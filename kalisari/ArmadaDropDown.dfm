object ArmadaDropDownFm: TArmadaDropDownFm
  Left = 271
  Top = 211
  Width = 1165
  Height = 588
  Caption = 'ArmadaDropDown'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 508
    Width = 1149
    Height = 41
    Align = alBottom
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1149
    Height = 508
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGridViewRepository1DBBandedTableView1
    end
  end
  object DataSource1: TDataSource
    DataSet = ArmadaQ
    Left = 328
    Top = 348
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 216
    Top = 376
    object cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView
      OnKeyDown = cxGridViewRepository1DBBandedTableView1KeyDown
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCellDblClick = cxGridViewRepository1DBBandedTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded, dcoImmediatePost, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsData.Editing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.BandCaptionsInColumnAlternateCaption = True
      OptionsView.BandHeaderEndEllipsis = True
      Bands = <
        item
        end>
      object cxGridViewRepository1DBBandedTableView1Kode: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Rekanan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Rekanan'
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KodeRekanan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KodeRekanan'
        Width = 108
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1PlatNo: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoBody: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoBody'
        Options.SortByDisplayText = isbtOn
        Width = 62
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NamaJenisKendaraan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NamaJenisKendaraan'
        Width = 180
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1TipeJenisKendaraan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'TipeJenisKendaraan'
        Width = 105
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1TahunPembuatan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'TahunPembuatan'
        Width = 94
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JumlahSeat: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JumlahSeat'
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JenisBBM: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JenisBBM'
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JenisAC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JenisAC'
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Toilet: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Toilet'
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1AirSuspension: TcxGridDBBandedColumn
        DataBinding.FieldName = 'AirSuspension'
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Sopir: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Sopir'
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoRangka: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoRangka'
        Width = 184
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoMesin: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoMesin'
        Width = 144
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KapasitasTangkiBBM: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KapasitasTangkiBBM'
        Width = 112
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1STNKPajakExpired: TcxGridDBBandedColumn
        DataBinding.FieldName = 'STNKPajakExpired'
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KirSelesai: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KirSelesai'
        Position.BandIndex = 0
        Position.ColIndex = 22
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1LevelArmada: TcxGridDBBandedColumn
        DataBinding.FieldName = 'LevelArmada'
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 23
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JumlahBan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JumlahBan'
        Position.BandIndex = 0
        Position.ColIndex = 24
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Keterangan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Keterangan'
        Position.BandIndex = 0
        Position.ColIndex = 26
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Aktif: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Aktif'
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1AC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'AC'
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KmSekarang: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KmSekarang'
        Width = 75
        Position.BandIndex = 0
        Position.ColIndex = 25
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1STNKPerpanjangExpired: TcxGridDBBandedColumn
        DataBinding.FieldName = 'STNKPerpanjangExpired'
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KirMulai: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KirMulai'
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada where rekanan=0')
    Left = 280
    Top = 336
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQNamaJenisKendaraan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaJenisKendaraan'
      LookupDataSet = JKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaJenis'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
    object ArmadaQTipeJenisKendaraan: TStringField
      FieldKind = fkLookup
      FieldName = 'TipeJenisKendaraan'
      LookupDataSet = JKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tipe'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
    object ArmadaQRekanan: TBooleanField
      FieldName = 'Rekanan'
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object ArmadaQKodeRekanan: TStringField
      FieldName = 'KodeRekanan'
      Size = 10
    end
  end
  object JKQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 568
    Top = 280
    object JKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JKQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JKQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JKQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object JKQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
