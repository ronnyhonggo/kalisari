unit Audit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxTextEdit, StdCtrls, Menus, cxButtons,
  cxMemo, cxMaskEdit, cxDropDownEdit;

type
  TAuditFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    AuditQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    Label1: TLabel;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaTabel: TcxGridDBColumn;
    cxGrid1DBTableView1KodeTabel: TcxGridDBColumn;
    cxGrid1DBTableView1DataLama: TcxGridDBColumn;
    cxGrid1DBTableView1DataBaru: TcxGridDBColumn;
    AuditQKode: TAutoIncField;
    AuditQNamaTabel: TStringField;
    AuditQKodeTabel: TStringField;
    AuditQDataLama: TMemoField;
    AuditQDataBaru: TMemoField;
    AuditQCreateBy: TStringField;
    AuditQCreateDate: TDateTimeField;
    cxComboForm: TcxComboBox;
    SearchQ: TSDQuery;
    SearchQnamatabel: TStringField;
    cxComboKode: TcxComboBox;
    Label2: TLabel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxSearchTextPropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButTambahClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxComboFormPropertiesChange(Sender: TObject);
    procedure cxComboKodePropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    OriSQL:string;
    SearchOriSQL:string;
  end;

var
  AuditFm: TAuditFm;

implementation

uses SysConst, MasterPelanggan;

{$R *.dfm}

{ TDropDownFm }

procedure TAuditFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TAuditFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=AuditQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TAuditFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=AuditQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TAuditFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=AuditQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TAuditFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=AuditQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TAuditFm.cxSearchTextPropertiesChange(
  Sender: TObject);
begin
{  AuditQ.Close;
  AuditQ.SQL.Text:='select * from pelanggan where namapt like '+QuotedStr('%'+cxSearchText.Text+'%');
  AuditQ.ExecSQL;
  AuditQ.Open;
  if AuditQ.RecordCount=0 then
    cxButTambah.Enabled:=True
  else
    cxButTambah.Enabled:=False;    }
end;

procedure TAuditFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  AuditQ.SQL.Text:=OriSQL;
end;

procedure TAuditFm.cxButTambahClick(Sender: TObject);
begin
  MasterPelangganFm := TMasterPelangganFm.create(self);
end;

procedure TAuditFm.FormShow(Sender: TObject);
var i:integer;
begin
  OriSQL:=AuditQ.SQL.Text;
  AuditQ.Open;
  SearchQ.Open;
  SearchQ.First;
  cxComboForm.Properties.Items.Clear;
  cxComboForm.Properties.Items.Add('ALL');
  while SearchQ.Eof=false do
  begin
    cxComboForm.Properties.Items.Add(SearchQnamatabel.AsString);
    SearchQ.Next;
  end;
  cxComboForm.Text:='ALL';
end;

procedure TAuditFm.cxComboFormPropertiesChange(Sender: TObject);
begin
  if cxComboForm.Text='ALL' then
    begin
      AuditQ.Close;
      AuditQ.SQL.Text:=OriSQL;
      AuditQ.Open;
      cxComboKode.Enabled:=False;
    end
  else
    begin
      SearchQ.Close;
      SearchQ.SQL.Text:='select distinct kodetabel as '+QuotedStr('namatabel')+' from audit where namatabel='+QuotedStr(cxComboForm.Text);
      SearchQ.Open;
      SearchQ.First;
      cxComboKode.Properties.Items.Clear;
      cxComboKode.Properties.Items.Add('ALL');
      while SearchQ.Eof=false do
      begin
        cxComboKode.Properties.Items.Add(SearchQnamatabel.AsString);
        SearchQ.Next;
      end;
      cxComboKode.Enabled:=True;
      cxComboKode.Text:='ALL';
    end;
end;

procedure TAuditFm.cxComboKodePropertiesChange(Sender: TObject);
begin
  if cxComboKode.Text='ALL' then
    begin
      AuditQ.Close;
      AuditQ.SQL.Text:='select * from audit where namatabel='+QuotedStr(cxComboForm.Text)+' order by kodetabel';
      AuditQ.Open;
    end
  else
    begin
      AuditQ.Close;
      AuditQ.SQL.Text:='select * from audit where namatabel='+QuotedStr(cxComboForm.Text)+' AND kodetabel='+QuotedStr(cxComboKode.Text)+' order by kode desc';
      AuditQ.Open;
    end;
end;

end.
