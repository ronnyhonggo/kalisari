unit BBMNonSO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, cxDropDownEdit, UCrpeClasses,
  UCrpe32, dxPrnDev, dxPrnDlg, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxSkinsdxBarPainter, dxPSCore, dxPScxCommon;

type
  TBBMNonSOFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;

    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    SaveBtn: TcxButton;
    SuratJalanQ: TSDQuery;
    ExitBtn: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    MasterQ: TSDQuery;
    ViewDs: TDataSource;
    ViewUS: TSDUpdateSQL;
    cxComboBox1: TcxComboBox;
    KodeQ: TSDQuery;
    Crpe1: TCrpe;
    Kode2Q: TSDQuery;
    StringField1: TStringField;
    SuratJalanQKodenota: TStringField;
    SuratJalanQTgl: TDateTimeField;
    SuratJalanQNoSO: TStringField;
    SuratJalanQSopir: TStringField;
    SuratJalanQSopir2: TStringField;
    SuratJalanQCrew: TStringField;
    SuratJalanQTitipKwitansi: TBooleanField;
    SuratJalanQNominalKwitansi: TCurrencyField;
    SuratJalanQNoKwitansi: TStringField;
    SuratJalanQKir: TBooleanField;
    SuratJalanQSTNK: TBooleanField;
    SuratJalanQPajak: TBooleanField;
    SuratJalanQTglKembali: TDateTimeField;
    SuratJalanQSPBUAYaniLiter: TFloatField;
    SuratJalanQSPBUAYaniUang: TCurrencyField;
    SuratJalanQSPBULuarLiter: TFloatField;
    SuratJalanQSPBULuarUang: TCurrencyField;
    SuratJalanQSPBULuarUangDiberi: TCurrencyField;
    SuratJalanQSPBULuarDetail: TMemoField;
    SuratJalanQStatus: TStringField;
    SuratJalanQCreateDate: TDateTimeField;
    SuratJalanQCreateBy: TStringField;
    SuratJalanQOperator: TStringField;
    SuratJalanQTglEntry: TDateTimeField;
    SuratJalanQLaporan: TMemoField;
    SuratJalanQTglRealisasi: TDateTimeField;
    SuratJalanQAwal: TStringField;
    SuratJalanQAkhir: TStringField;
    SuratJalanQTglCetak: TDateTimeField;
    SuratJalanQClaimSopir: TCurrencyField;
    SuratJalanQKeteranganClaimSopir: TStringField;
    SuratJalanQPremiSopir: TCurrencyField;
    SuratJalanQPremiSopir2: TCurrencyField;
    SuratJalanQPremiKernet: TCurrencyField;
    SuratJalanQTabunganSopir: TCurrencyField;
    SuratJalanQTabunganSopir2: TCurrencyField;
    SuratJalanQTol: TCurrencyField;
    SuratJalanQUangJalan: TCurrencyField;
    SuratJalanQBiayaLainLain: TCurrencyField;
    SuratJalanQUangMakan: TCurrencyField;
    SuratJalanQOther: TCurrencyField;
    SuratJalanQPendapatan: TCurrencyField;
    SuratJalanQPengeluaran: TCurrencyField;
    RuteQ: TSDQuery;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    SuratJalanQSisaDisetor: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ViewQ: TSDQuery;
    KodeEdit: TcxButtonEdit;
    deleteBtn: TcxButton;
    SuratJalanQKeteranganBiayaLainLain: TStringField;
    SuratJalanQUangInap: TCurrencyField;
    SuratJalanQUangParkir: TCurrencyField;
    cxButton1: TcxButton;
    SuratJalanQKeterangan: TMemoField;
    updateQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxButton2: TcxButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    SuratJalanQSPBUAYaniJam: TDateTimeField;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQArmada: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQJumlahLiter: TFloatField;
    MasterQJumlahDibayar: TCurrencyField;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQRekanan: TBooleanField;
    ArmadaQKodeRekanan: TStringField;
    MasterQPlatNo: TStringField;
    MasterQNoBody: TStringField;
    cxDBVerticalGrid1Armada: TcxDBEditorRow;
    cxDBVerticalGrid1Tanggal: TcxDBEditorRow;
    cxDBVerticalGrid1JumlahLiter: TcxDBEditorRow;
    cxDBVerticalGrid1JumlahDibayar: TcxDBEditorRow;
    cxDBVerticalGrid1PlatNo: TcxDBEditorRow;
    cxDBVerticalGrid1NoBody: TcxDBEditorRow;
    ViewQKode: TStringField;
    ViewQArmada: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQJumlahLiter: TFloatField;
    ViewQJumlahDibayar: TCurrencyField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahLiter: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahDibayar: TcxGridDBColumn;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    ViewQPlatNo: TStringField;
    ViewQNoBody: TStringField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    MasterQPengemudi: TStringField;
    MasterQKeterangan: TMemoField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    cxDBVerticalGrid1Pengemudi: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPengemudi: TcxDBEditorRow;
    MasterQJenisBBM: TStringField;
    HargaBBMQ: TSDQuery;
    HargaBBMQKode: TStringField;
    HargaBBMQHargaSolar: TCurrencyField;
    HargaBBMQHargaBensin: TCurrencyField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPengemudi: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure cxDBVerticalGrid1SuratJalanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1SuratJalanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SuratJalanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2SuratJalanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure BonBBMQAfterInsert(DataSet: TDataSet);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditKeyPress(Sender: TObject; var Key: Char);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure deleteBtnClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxDBVerticalGrid1ArmadaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1JumlahDibayarEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1JumlahLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PengemudiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  BBMNonSOFm: TBBMNonSOFm;

  MasterOriSQL,ViewOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, SJDropDown, ArmadaDropDown,
  PengemudiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TBBMNonSOFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TBBMNonSOFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TBBMNonSOFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TBBMNonSOFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;
end;

procedure TBBMNonSOFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TBBMNonSOFm.FormShow(Sender: TObject);
var i : integer;
begin
  KodeEdit.SetFocus;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
  ArmadaQ.Close;
  ArmadaQ.Open;
  BarangQ.Close;
  BarangQ.Open;
  PegawaiQ.Close;
  PegawaiQ.Open;
  HargaBBMQ.Close;
  HargaBBMQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKasBon.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBon.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBon.AsBoolean;
  KodeEditPropertiesButtonClick(sender,0);
end;

procedure TBBMNonSOFm.SaveBtnClick(Sender: TObject);
var kodeprint,kode : String;
begin
 if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;

  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    kode:=MasterQKode.AsString;
    MasterQ.ApplyUpdates;

    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');

    //cxButtonEdit1PropertiesButtonClick(sender,0);

  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  KodeEdit.SetFocus;
  ViewQ.Close;
  ViewQ.Open;
end;


procedure TBBMNonSOFm.cxComboBox1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.ItemIndex=0 then
  begin
    cxDBVerticalGrid1.Visible:=true;
    //cxDBVerticalGrid2.Visible:=false;
  end
  else if cxComboBox1.ItemIndex=1 then
  begin
    cxDBVerticalGrid1.Visible:=false;
   // cxDBVerticalGrid2.Visible:=true;
  end;
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1SuratJalanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{SuratJalanQ.Close;
SuratJalanQ.ExecSQL;
SuratJalanQ.Open;
SJDropDownFm:=TSJDropDownFm.Create(self);
if SJDropDownFm.ShowModal=MrOK then
begin
  MasterQ.Open;
  MasterQ.Edit;
  MasterQSuratJalan.AsString:=SJDropDownFm.kode;

end;
SJDropDownFm.Release;}
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1SuratJalanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SuratJalanQ.Close;
    SuratJalanQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SuratJalanQ.ExecSQL;
    SuratJalanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuratJalanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
    //  BonBBMQ.Open;
   //   BonBBMQ.Edit;
     // BonBBMQSuratJalan.AsString:=SuratJalanQKodeNota.AsString;
  //    BonBBMQmuat.AsString := suratjalanqmuat.AsString;
    //  bonbbmqbongkar.AsString := suratjalanqbongkar.AsString;
   //   bonbbmqborongan.AsString := suratjalanqborongan.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TBBMNonSOFm.cxDBVerticalGrid2SuratJalanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    SuratJalanQ.Close;
    SuratJalanQ.ParamByName('text').AsString:='%';
    SuratJalanQ.ExecSQL;
    SuratJalanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuratJalanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSuratJalan.AsString:=SuratJalanQKodeNota.AsString;
     // MasterQmuat.AsString := suratjalanqmuat.AsString;
     //MasterQbongkar.AsString := suratjalanqbongkar.AsString;
     // MasterQborongan.AsString := suratjalanqborongan.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;  }
end;

procedure TBBMNonSOFm.cxDBVerticalGrid2SuratJalanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  {  SuratJalanQ.Close;
    SuratJalanQ.ParamByName('text').AsString:=vartostr(displayvalue);
    SuratJalanQ.ExecSQL;
    SuratJalanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuratJalanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSuratJalan.AsString:=SuratJalanQKodeNota.AsString;
     // MasterQmuat.AsString := suratjalanqmuat.AsString;
     // MasterQbongkar.AsString := suratjalanqbongkar.AsString;
     // MasterQborongan.AsString := suratjalanqborongan.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release; }
end;

procedure TBBMNonSOFm.BonBBMQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //BonBBMQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
 // BonBBMQCreateBy.AsString:=User;
   MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TBBMNonSOFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TBBMNonSOFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin

    cxDBVerticalGrid1.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewQKode.AsString+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= ViewQKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBon.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBon.AsBoolean;
   cxDBVerticalGrid1.Enabled:=true;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);
  //  akhirnya:='';

end;

procedure TBBMNonSOFm.KodeEditEnter(Sender: TObject);
begin
 KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TBBMNonSOFm.KodeEditExit(Sender: TObject);
begin
 if KodeEdit.Text<>'' then
  begin

        MasterQ.SQL.Clear;
       MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kode like '+ QuotedStr('%'+kodeedit.Text+'%'));
        MasterQ.Open;
        if MasterQ.IsEmpty then
        begin
          StatusBar.Panels[0].Text:= 'Mode : Entry';
          MasterQ.Append;
          MasterQ.Edit;
        end
        else
        begin
          StatusBar.Panels[0].Text:= 'Mode : Edit';
          DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBon.AsBoolean;
        end;

    SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBon.AsBoolean;
    cxDBVerticalGrid1.Enabled:=true;
end;
end;

procedure TBBMNonSOFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TBBMNonSOFm.KodeEditKeyPress(Sender: TObject; var Key: Char);
begin
If Key = #13 Then Begin
      //MasterVGrid.Enabled:=true;
      cxDBVerticalGrid1.Enabled:=true;
     // cxButtonCetak.Enabled:=true;
      MasterQ.SQL.Clear;
      MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
      MasterQ.Open;
      if MasterQ.IsEmpty then
      begin
        StatusBar.Panels[0].Text:= 'Mode : Entry';
        MasterQ.Append;
        MasterQ.Edit;
      end
      else
      begin
        StatusBar.Panels[0].Text:= 'Mode : Edit';
        //KodeEdit.Text:= ViewKontrakQKodenota.AsString;
        MasterQ.Edit;

      end;
      SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBon.AsBoolean;
      cxDBVerticalGrid1.Enabled:=True;

  end;
end;

procedure TBBMNonSOFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  cxDBVerticalGrid1.SetFocus;
  DMFm.GetDateQ.Open;
  //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
end;

procedure TBBMNonSOFm.deleteBtnClick(Sender: TObject);
begin
 if MessageDlg('Hapus BBM Non SO '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('BBM Non SO telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('BBM Non SO pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     ViewQ.Close;
     ViewQ.Open;
     KodeEdit.SetFocus;
     KodeEditPropertiesButtonClick(sender,0);
  end;
end;

procedure TBBMNonSOFm.cxButton1Click(Sender: TObject);
begin
  updateQ.Close;
  updateQ.SQL.Clear;
  updateQ.SQL.Add('update BonSopir set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.asstring));
  updateQ.ExecSQL;
  updateQ.Close;

  Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonSopir.rpt';
  Crpe1.ParamFields[0].CurrentValue:=MasterQKode.asstring;
  Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
  Crpe1.Execute;
end;

procedure TBBMNonSOFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewQ.Open;
end;

procedure TBBMNonSOFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewQ.Open;
end;

procedure TBBMNonSOFm.cxButton2Click(Sender: TObject);
begin
  dxComponentPrinter1Link1.Preview();
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1ArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
if ArmadaDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:=ArmadaDropDownFm.Kode;
end;
ArmadaDropDownFm.Release;
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1JumlahDibayarEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQJumlahDibayar.AsCurrency:=DisplayValue;
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1JumlahLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQJumlahLiter.AsFloat:=DisplayValue;

if UpperCase(MasterQJenisBBM.AsString)='BENSIN' then
begin
  MasterQ.Edit;
  MasterQJumlahDibayar.AsCurrency:=MasterQJumlahLiter.AsFloat*HargaBBMQHargaBensin.AsCurrency;
end

else if UpperCase(MasterQJenisBBM.AsString)='SOLAR' then
begin
  MasterQ.Edit;
  MasterQJumlahDibayar.AsCurrency:=MasterQJumlahLiter.AsFloat*HargaBBMQHargaSolar.AsCurrency;
end;
end;

procedure TBBMNonSOFm.cxDBVerticalGrid1PengemudiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
PengemudiDropDownFm:=TPengemudiDropDownFm.Create(self);
if PengemudiDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPengemudi.AsString:=PengemudiDropDownFm.Kode;
end;
PengemudiDropDownFm.Release;
end;

end.
