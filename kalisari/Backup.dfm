object BackupFm: TBackupFm
  Left = 583
  Top = 262
  Width = 395
  Height = 310
  Caption = 'Backup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 64
    Width = 137
    Height = 25
    Caption = 'Backup Database'
    TabOrder = 0
    OnClick = Button1Click
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'BACKUP DATABASE BungaDaru TO DISK = :text')
    UpdateObject = MasterUS
    Left = 209
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 268
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PlatNo, Panjang, Kapasitas, TahunPembuatan, JumlahB' +
        'an, Aktif, KmSekarang, Keterangan, Sopir, Ekor, JenisKendaraan, ' +
        'CreateDate, CreateBy, Operator, TglEntry, STNKPajakExpired, STNK' +
        'PerpanjangExpired, KirMulai, KirSelesai, NoRangka, NoMesin'#13#10'from' +
        ' armada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update armada'
      'set'
      '  Kode = :Kode,'
      '  PlatNo = :PlatNo,'
      '  Panjang = :Panjang,'
      '  Kapasitas = :Kapasitas,'
      '  TahunPembuatan = :TahunPembuatan,'
      '  JumlahBan = :JumlahBan,'
      '  Aktif = :Aktif,'
      '  KmSekarang = :KmSekarang,'
      '  Keterangan = :Keterangan,'
      '  Sopir = :Sopir,'
      '  Ekor = :Ekor,'
      '  JenisKendaraan = :JenisKendaraan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  STNKPajakExpired = :STNKPajakExpired,'
      '  STNKPerpanjangExpired = :STNKPerpanjangExpired,'
      '  KirMulai = :KirMulai,'
      '  KirSelesai = :KirSelesai,'
      '  NoRangka = :NoRangka,'
      '  NoMesin = :NoMesin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into armada'
      
        '  (Kode, PlatNo, Panjang, Kapasitas, TahunPembuatan, JumlahBan, ' +
        'Aktif, KmSekarang, Keterangan, Sopir, Ekor, JenisKendaraan, Crea' +
        'teDate, CreateBy, Operator, TglEntry, STNKPajakExpired, STNKPerp' +
        'anjangExpired, KirMulai, KirSelesai, NoRangka, NoMesin)'
      'values'
      
        '  (:Kode, :PlatNo, :Panjang, :Kapasitas, :TahunPembuatan, :Jumla' +
        'hBan, :Aktif, :KmSekarang, :Keterangan, :Sopir, :Ekor, :JenisKen' +
        'daraan, :CreateDate, :CreateBy, :Operator, :TglEntry, :STNKPajak' +
        'Expired, :STNKPerpanjangExpired, :KirMulai, :KirSelesai, :NoRang' +
        'ka, :NoMesin)')
    DeleteSQL.Strings = (
      'delete from armada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 260
    Top = 42
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from armada order by kode desc')
    Left = 153
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from sopir where kode like '#39'%'#39' + :text + '#39'%'#39' or nama li' +
        'ke '#39'%'#39' + :text + '#39'%'#39
      '')
    Left = 73
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object SopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object SopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object SopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SopirQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object EkorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from ekor where kode like '#39'%'#39' + :text + '#39'%'#39' '
      '')
    Left = 113
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object EkorQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object EkorQPanjang: TStringField
      FieldName = 'Panjang'
      Size = 50
    end
    object EkorQBerat: TStringField
      FieldName = 'Berat'
      Size = 50
    end
    object EkorQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object EkorQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object EkorQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object EkorQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
  end
  object JenisKendaraanQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 32
    Top = 8
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 344
    Top = 24
  end
end
