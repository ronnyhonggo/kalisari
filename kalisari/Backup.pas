unit Backup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxClasses, cxMRUEdit, cxLabel;

type
  TBackupFm = class(TForm)
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    SopirQ: TSDQuery;
    EkorQ: TSDQuery;
    SopirQKode: TStringField;
    SopirQNama: TStringField;
    SopirQAlamat: TStringField;
    SopirQNotelp: TStringField;
    SopirQCreateDate: TDateTimeField;
    SopirQCreateBy: TStringField;
    SopirQOperator: TStringField;
    SopirQTglEntry: TDateTimeField;
    EkorQKode: TStringField;
    EkorQPanjang: TStringField;
    EkorQBerat: TStringField;
    EkorQCreateDate: TDateTimeField;
    EkorQCreateBy: TStringField;
    EkorQTglEntry: TDateTimeField;
    EkorQOperator: TStringField;
    JenisKendaraanQ: TSDQuery;
    JenisKendaraanQKode: TStringField;
    JenisKendaraanQNamaJenis: TStringField;
    JenisKendaraanQTipe: TMemoField;
    JenisKendaraanQKeterangan: TMemoField;
    Button1: TButton;
    SaveDialog1: TSaveDialog;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  BackupFm: TBackupFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TBackupFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TBackupFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TBackupFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TBackupFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TBackupFm.Button1Click(Sender: TObject);
begin
  SaveDialog1.Execute;
  //ShowMessage( SaveDialog1.FileName);
  MasterQ.Close;
  MasterQ.ParamByName('text').AsString:=SaveDialog1.FileName;
  MasterQ.Open;
end;

end.
