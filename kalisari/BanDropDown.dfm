object BanDropDownFm: TBanDropDownFm
  Left = 356
  Top = 336
  Width = 1043
  Height = 498
  Caption = 'BanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1027
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 98
      end
      object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 155
      end
      object cxGrid1DBTableView1KodeIDBan: TcxGridDBColumn
        DataBinding.FieldName = 'KodeIDBan'
        Width = 99
      end
      object cxGrid1DBTableView1Merk: TcxGridDBColumn
        DataBinding.FieldName = 'Merk'
        Width = 129
      end
      object cxGrid1DBTableView1Jenis: TcxGridDBColumn
        DataBinding.FieldName = 'Jenis'
        Width = 108
      end
      object cxGrid1DBTableView1UkuranBan: TcxGridDBColumn
        DataBinding.FieldName = 'UkuranBan'
        Width = 114
      end
      object cxGrid1DBTableView1StandardUmur: TcxGridDBColumn
        DataBinding.FieldName = 'StandardUmur'
        Width = 111
      end
      object cxGrid1DBTableView1StandardKm: TcxGridDBColumn
        DataBinding.FieldName = 'StandardKm'
        Width = 95
      end
      object cxGrid1DBTableView1Vulkanisir: TcxGridDBColumn
        DataBinding.FieldName = 'Vulkanisir'
        Width = 74
      end
      object cxGrid1DBTableView1TanggalVulkanisir: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalVulkanisir'
        Width = 131
      end
      object cxGrid1DBTableView1KedalamanAlur: TcxGridDBColumn
        DataBinding.FieldName = 'KedalamanAlur'
        Width = 116
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 83
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object BanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select b.*'
      'from Ban b'
      
        'where b.Status<>'#39'RUSAK'#39' and b.Kode not in (select KodeBan from D' +
        'etailArmadaBan)')
    Left = 216
    Top = 368
    object BanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BanQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object BanQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 50
      Lookup = True
    end
    object BanQKodeIDBan: TStringField
      FieldName = 'KodeIDBan'
      Size = 50
    end
    object BanQMerk: TStringField
      FieldName = 'Merk'
      Required = True
      Size = 50
    end
    object BanQJenis: TStringField
      FieldName = 'Jenis'
      Required = True
      Size = 50
    end
    object BanQUkuranBan: TStringField
      FieldName = 'UkuranBan'
      Size = 50
    end
    object BanQRing: TStringField
      FieldName = 'Ring'
      Size = 50
    end
    object BanQVelg: TStringField
      FieldName = 'Velg'
      Size = 50
    end
    object BanQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BanQStandardKm: TIntegerField
      FieldName = 'StandardKm'
    end
    object BanQVulkanisir: TBooleanField
      FieldName = 'Vulkanisir'
    end
    object BanQTanggalVulkanisir: TDateTimeField
      FieldName = 'TanggalVulkanisir'
    end
    object BanQKedalamanAlur: TFloatField
      FieldName = 'KedalamanAlur'
    end
    object BanQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object BanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object LPBDs: TDataSource
    DataSet = BanQ
    Left = 248
    Top = 368
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 280
    Top = 368
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
end
