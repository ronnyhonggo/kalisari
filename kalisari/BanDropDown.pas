unit BanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TBanDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BanQ: TSDQuery;
    LPBDs: TDataSource;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BanQKode: TStringField;
    BanQKodeBarang: TStringField;
    BanQKodeIDBan: TStringField;
    BanQMerk: TStringField;
    BanQJenis: TStringField;
    BanQUkuranBan: TStringField;
    BanQRing: TStringField;
    BanQVelg: TStringField;
    BanQStandardUmur: TIntegerField;
    BanQStandardKm: TIntegerField;
    BanQVulkanisir: TBooleanField;
    BanQTanggalVulkanisir: TDateTimeField;
    BanQKedalamanAlur: TFloatField;
    BanQStatus: TStringField;
    BanQCreateDate: TDateTimeField;
    BanQCreateBy: TStringField;
    BanQOperator: TStringField;
    BanQTglEntry: TDateTimeField;
    BanQNamaBarang: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1KodeIDBan: TcxGridDBColumn;
    cxGrid1DBTableView1Merk: TcxGridDBColumn;
    cxGrid1DBTableView1Jenis: TcxGridDBColumn;
    cxGrid1DBTableView1UkuranBan: TcxGridDBColumn;
    cxGrid1DBTableView1StandardUmur: TcxGridDBColumn;
    cxGrid1DBTableView1StandardKm: TcxGridDBColumn;
    cxGrid1DBTableView1Vulkanisir: TcxGridDBColumn;
    cxGrid1DBTableView1TanggalVulkanisir: TcxGridDBColumn;
    cxGrid1DBTableView1KedalamanAlur: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    BarangQJumlah: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  BanDropDownFm: TBanDropDownFm;

implementation

{$R *.dfm}

procedure TBanDropDownFm.FormCreate(Sender: TObject);
begin
  BarangQ.Open;
  BanQ.Open;
end;

procedure TBanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BanQKode.AsString;
  ModalResult:=mrOK;
end;

end.
