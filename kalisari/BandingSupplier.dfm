object BandingSupplierFm: TBandingSupplierFm
  Left = 358
  Top = 133
  Width = 633
  Height = 537
  Caption = 'BandingSupplierFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 16
    Top = 16
    Width = 425
    Height = 129
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 124
    ParentFont = False
    TabOrder = 0
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid1Supplier1: TcxDBEditorRow
      Properties.Caption = 'Supplier 1'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1Supplier1EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier1'
      ID = 40
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1nama1: TcxDBEditorRow
      Properties.Caption = 'Nama Toko 1'
      Properties.DataBinding.FieldName = 'nama1'
      Properties.Options.Editing = False
      ID = 52
      ParentID = 40
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Harga1: TcxDBEditorRow
      Properties.Caption = 'Harga'
      Properties.DataBinding.FieldName = 'Harga1'
      ID = 41
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Termin1: TcxDBEditorRow
      Properties.Caption = 'Termin'
      Properties.DataBinding.FieldName = 'Termin1'
      ID = 42
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1Keterangan1: TcxDBEditorRow
      Properties.Caption = 'Keterangan'
      Properties.DataBinding.FieldName = 'Keterangan1'
      ID = 43
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object cxDBVerticalGrid2: TcxDBVerticalGrid
    Left = 16
    Top = 160
    Width = 425
    Height = 129
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 124
    ParentFont = False
    TabOrder = 1
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid2Supplier2: TcxDBEditorRow
      Properties.Caption = 'Supplier 2'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2Supplier2EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier2'
      ID = 18
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2nama2: TcxDBEditorRow
      Properties.Caption = 'Nama Toko 2'
      Properties.DataBinding.FieldName = 'nama2'
      Properties.Options.Editing = False
      ID = 27
      ParentID = 18
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2Harga2: TcxDBEditorRow
      Properties.Caption = 'Harga'
      Properties.DataBinding.FieldName = 'Harga2'
      ID = 19
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid2Termin2: TcxDBEditorRow
      Properties.Caption = 'Termin'
      Properties.DataBinding.FieldName = 'Termin2'
      ID = 20
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid2Keterangan2: TcxDBEditorRow
      Properties.Caption = 'Keterangan'
      Properties.DataBinding.FieldName = 'Keterangan2'
      ID = 21
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object cxDBVerticalGrid3: TcxDBVerticalGrid
    Left = 16
    Top = 304
    Width = 425
    Height = 129
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 124
    ParentFont = False
    TabOrder = 2
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid3Supplier3: TcxDBEditorRow
      Properties.Caption = 'Supplier 3'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid3Supplier3EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier3'
      ID = 22
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid3nama3: TcxDBEditorRow
      Properties.Caption = 'Nama Toko 3'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.FieldName = 'nama3'
      Properties.Options.Editing = False
      ID = 28
      ParentID = 22
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid3Harga3: TcxDBEditorRow
      Properties.Caption = 'Harga'
      Properties.DataBinding.FieldName = 'Harga3'
      ID = 23
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid3Termin3: TcxDBEditorRow
      Properties.Caption = 'Termin'
      Properties.DataBinding.FieldName = 'Termin3'
      ID = 24
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid3Keterangan3: TcxDBEditorRow
      Properties.Caption = 'Keterangan'
      Properties.DataBinding.FieldName = 'Keterangan3'
      ID = 25
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object CheckBox1: TCheckBox
    Left = 448
    Top = 48
    Width = 145
    Height = 17
    Caption = 'Pilih Supplier 1'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    State = cbChecked
    TabOrder = 3
  end
  object CheckBox2: TCheckBox
    Left = 448
    Top = 192
    Width = 145
    Height = 17
    Caption = 'Pilih Supplier 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
  object CheckBox3: TCheckBox
    Left = 456
    Top = 352
    Width = 145
    Height = 17
    Caption = 'Pilih Supplier 3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object Button1: TButton
    Left = 240
    Top = 448
    Width = 137
    Height = 41
    Caption = 'Save All'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *, '#39#39' as nama1, '#39#39' as nama2, '#39#39' as nama3 from daftarbeli'
      '')
    UpdateObject = SDUpdateSQL1
    Left = 48
    Top = 456
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1Supplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object SDQuery1Harga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object SDQuery1Termin1: TIntegerField
      FieldName = 'Termin1'
    end
    object SDQuery1Keterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object SDQuery1Supplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object SDQuery1Harga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object SDQuery1Termin2: TIntegerField
      FieldName = 'Termin2'
    end
    object SDQuery1Keterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object SDQuery1Supplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object SDQuery1Harga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object SDQuery1Termin3: TIntegerField
      FieldName = 'Termin3'
    end
    object SDQuery1Keterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object SDQuery1nama1: TStringField
      FieldName = 'nama1'
      Required = True
      Size = 1
    end
    object SDQuery1nama2: TStringField
      FieldName = 'nama2'
      Required = True
      Size = 1
    end
    object SDQuery1nama3: TStringField
      FieldName = 'nama3'
      Required = True
      Size = 1
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 88
    Top = 456
  end
  object SDUpdateSQL1: TSDUpdateSQL
    Left = 128
    Top = 456
  end
end
