unit BandingSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxButtonEdit,
  cxVGrid, cxDBVGrid, StdCtrls, SDEngine, DB, cxInplaceContainer;

type
  TBandingSupplierFm = class(TForm)
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid3: TcxDBVerticalGrid;
    SDQuery1Kode: TStringField;
    SDQuery1Supplier1: TStringField;
    SDQuery1Harga1: TCurrencyField;
    SDQuery1Termin1: TIntegerField;
    SDQuery1Keterangan1: TMemoField;
    SDQuery1Supplier2: TStringField;
    SDQuery1Harga2: TCurrencyField;
    SDQuery1Termin2: TIntegerField;
    SDQuery1Keterangan2: TMemoField;
    SDQuery1Supplier3: TStringField;
    SDQuery1Harga3: TCurrencyField;
    SDQuery1Termin3: TIntegerField;
    SDQuery1Keterangan3: TMemoField;
    SDUpdateSQL1: TSDUpdateSQL;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Button1: TButton;
    SDQuery1nama1: TStringField;
    SDQuery1nama2: TStringField;
    SDQuery1nama3: TStringField;
    cxDBVerticalGrid1Supplier1: TcxDBEditorRow;
    cxDBVerticalGrid1Harga1: TcxDBEditorRow;
    cxDBVerticalGrid1Termin1: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan1: TcxDBEditorRow;
    cxDBVerticalGrid1nama1: TcxDBEditorRow;
    cxDBVerticalGrid2Supplier2: TcxDBEditorRow;
    cxDBVerticalGrid2Harga2: TcxDBEditorRow;
    cxDBVerticalGrid2Termin2: TcxDBEditorRow;
    cxDBVerticalGrid2Keterangan2: TcxDBEditorRow;
    cxDBVerticalGrid2nama2: TcxDBEditorRow;
    cxDBVerticalGrid3Supplier3: TcxDBEditorRow;
    cxDBVerticalGrid3Harga3: TcxDBEditorRow;
    cxDBVerticalGrid3Termin3: TcxDBEditorRow;
    cxDBVerticalGrid3Keterangan3: TcxDBEditorRow;
    cxDBVerticalGrid3nama3: TcxDBEditorRow;
    procedure cxDBVerticalGrid1Supplier1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2Supplier2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid3Supplier3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kodedb:String;
  end;

var
  BandingSupplierFm: TBandingSupplierFm;

implementation
uses menuutama, daftarbeli, BrowseSupplier;

{$R *.dfm}

procedure TBandingSupplierFm.cxDBVerticalGrid1Supplier1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    BrowseSupplierFm.ShowModal;
    { if MenuUtamaFm.param_kode<>'' then
    begin
      MasterQ.Edit;
      MasterQSupplier.AsString:=MenuUtamaFm.param_kode;
      MasterQsupplier_skr.AsString:=MenuUtamaFm.param_nama;
    end;   }
end;

procedure TBandingSupplierFm.cxDBVerticalGrid2Supplier2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BrowseSupplierFm.ShowModal;
end;

procedure TBandingSupplierFm.cxDBVerticalGrid3Supplier3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    BrowseSupplierFm.ShowModal;
end;

procedure TBandingSupplierFm.FormActivate(Sender: TObject);
begin
  SDQuery1.Close;
  SDQuery1.ParamByName('kodedb').AsString := kodedb;
  SDQuery1.ExecSQL;
  SDQuery1.Open;
end;

end.
