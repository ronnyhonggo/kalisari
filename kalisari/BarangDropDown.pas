unit BarangDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, Menus,
  cxContainer, cxTextEdit, StdCtrls, cxButtons, ExtCtrls;

type
  TBarangDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BarangQ: TSDQuery;
    LPBDs: TDataSource;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    cxGrid1DBTableView1MinimumStok: TcxGridDBColumn;
    cxGrid1DBTableView1MaximumStok: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    Panel1: TPanel;
    cxButTambah: TcxButton;
    cxSearchText: TcxTextEdit;
    Label1: TLabel;
    BarangQJumlah: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxSearchTextEnter(Sender: TObject);
    procedure cxSearchTextPropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButTambahClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    OriSQL:string;
    jumlah:double;
  end;

var
  BarangDropDownFm: TBarangDropDownFm;

implementation

uses MasterBarang;

{$R *.dfm}

procedure TBarangDropDownFm.FormCreate(Sender: TObject);
begin
//  BarangQ.Open;
end;

procedure TBarangDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BarangQKode.AsString;
  jumlah:=BarangQJumlah.AsFloat;
  ModalResult:=mrOK;
end;

procedure TBarangDropDownFm.cxSearchTextEnter(Sender: TObject);
begin
  cxSearchText.Text:='';
end;

procedure TBarangDropDownFm.cxSearchTextPropertiesChange(Sender: TObject);
begin
  BarangQ.Close;
  BarangQ.SQL.Text:='select * from barang where nama like '+QuotedStr('%'+cxSearchText.Text+'%');
  BarangQ.Open;
  if BarangQ.RecordCount=0 then
    cxButTambah.Enabled:=True
  else
    cxButTambah.Enabled:=False;
end;

procedure TBarangDropDownFm.FormShow(Sender: TObject);
begin
  OriSQL:=BarangQ.SQL.Text;
  cxButTambah.Enabled:=False;
  cxSearchText.SetFocus;
  BarangQ.Open;
end;

procedure TBarangDropDownFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  BarangQ.SQL.Text:=OriSQL;
end;

procedure TBarangDropDownFm.cxButTambahClick(Sender: TObject);
begin
  MasterBarangFm := TMasterBarangFm.create(self);
end;

end.
