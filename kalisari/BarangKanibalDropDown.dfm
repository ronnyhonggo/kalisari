object BarangKanibalDropDownFm: TBarangKanibalDropDownFm
  Left = 795
  Top = 205
  Width = 645
  Height = 498
  Caption = 'BarangKanibalDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 629
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1TglMasuk: TcxGridDBColumn
        DataBinding.FieldName = 'TglMasuk'
      end
      object cxGrid1DBTableView1IDBarang: TcxGridDBColumn
        DataBinding.FieldName = 'IDBarang'
        Width = 114
      end
      object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object BKQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from barangkanibal')
    Left = 216
    Top = 368
    object BKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BKQTglMasuk: TDateTimeField
      FieldName = 'TglMasuk'
    end
    object BKQIDBarang: TStringField
      FieldName = 'IDBarang'
      Size = 50
    end
    object BKQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object BKQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object BKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BKQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
  object LPBDs: TDataSource
    DataSet = BKQ
    Left = 280
    Top = 368
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 376
    Top = 368
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
end
