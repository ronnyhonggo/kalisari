unit BarangLPBDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TBarangLPBDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    BarangQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    cxGrid1DBTableView1MinimumStok: TcxGridDBColumn;
    cxGrid1DBTableView1MaximumStok: TcxGridDBColumn;
    cxGrid1DBTableView1StandardUmur: TcxGridDBColumn;
    cxGrid1DBTableView1Lokasi: TcxGridDBColumn;
    cxGrid1DBTableView1ClaimNWarranty: TcxGridDBColumn;
    cxGrid1DBTableView1DurasiClaimNWarranty: TcxGridDBColumn;
    cxGrid1DBTableView1Kategori: TcxGridDBColumn;
    cxGrid1DBTableView1SingleSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    BarangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    q:string;
  public
    { Public declarations }
    kode,nama:string;
    constructor Create(aOwner: TComponent;Query1:string);overload;
  end;

var
  BarangLPBDropDownFm: TBarangLPBDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }
constructor TBarangLPBDropDownFm.Create(aOwner: TComponent; Query1: string);
begin
  inherited Create(aOwner);
  q:=Query1;

end;

procedure TBarangLPBDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TBarangLPBDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=BarangQ.Fields[0].AsString;
  nama:=BarangQNama.AsString;
  ModalResult:=mrOk;
end;

procedure TBarangLPBDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BarangQ.Fields[0].AsString;
  nama:=BarangQNama.AsString;
  ModalResult:=mrOk;
end;

procedure TBarangLPBDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=BarangQ.Fields[0].AsString;
  nama:=BarangQNama.AsString;
  ModalResult:=mrOk;
end;
end;

procedure TBarangLPBDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TBarangLPBDropDownFm.FormCreate(Sender: TObject);
begin

  BarangQ.Close;
  BarangQ.SQL.Text:=q;
  BarangQ.Open;

end;

end.
