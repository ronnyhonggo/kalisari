unit BaseForm;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, ComCtrls,
{$IFDEF EXPRESSBARS}
  dxBar, dxStatusBar,
{$ENDIF}
  dxCore, cxClasses, cxStyles, cxGridTableView, cxLookAndFeels, cxGridCardView;

type
{$IFDEF EXPRESSBARS}
  TdxBaseMenuItem = TdxBarItem;
{$ELSE}
  TdxBaseMenuItem = TMenuItem;
{$ENDIF}

  TfmBaseForm = class(TForm)
    lbDescription: TLabel;
    miExit: TMenuItem;
    miFile: TMenuItem;
    mmMain: TMainMenu;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    GridTableViewStyleSheetDevExpress: TcxGridTableViewStyleSheet;
    GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet;
    sbMain: TStatusBar;

  private
    FLookAndFeel: TcxLookAndFeel;
  protected
 
  public
  {$IFDEF EXPRESSBARS}
    BarManager: TdxBarManager;
    StatusBar: TdxStatusBar;
  {$ELSE}
    function StatusBar: TStatusBar;
  {$ENDIF}

  end;

var
  fmBaseForm: TfmBaseForm;

implementation

{$R *.dfm}

uses
{$IFDEF EXPRESSSKINS}
  {$I dxSkins.inc}
  dxSkinsStrs, dxSkinsForm, dxSkinInfo,
{$IFDEF EXPRESSPAGECONTROL}
  dxSkinscxPCPainter,
{$ENDIF}
{$IFDEF EXPRESSSCHEDULER}
  dxSkinscxSchedulerPainter,
{$ENDIF}
{$IFDEF EXPRESSDOCKINGLIBRARY}
  dxSkinsdxDockControlPainter,
{$ENDIF}
{$IFDEF EXPRESSNAVBAR}
  dxSkinsdxNavBarPainter,
{$ENDIF}
{$IFDEF EXPRESSBARS}
  dxSkinsdxBarPainter,
  dxSkinsdxStatusBarPainter,
{$IFDEF EXPRESSPAGECONTROL}
  {$IFDEF EXPRESSEDITORS}
    dxBarSkinnedCustForm,
  {$ENDIF}
{$ENDIF}
{$ENDIF}
{$ENDIF}
  dxGDIPlusAPI, cxLookAndFeelPainters, AboutDemoForm;

{$IFDEF EXPRESSBARS}
const
  BarVisibility: array[Boolean] of TdxBarItemVisible = (ivNever, ivAlways);
{$ENDIF}

const
  LevelGroupIndex: Integer = 200;

end.
