unit Bayar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UCrpeClasses, UCrpe32, DB, SDEngine;

type
  TBayarFm = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Crpe1: TCrpe;
    Button2: TButton;
    SembarangQ: TSDQuery;
    LblKet: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    lblket2: TLabel;
    Edit1: TComboBox;
    ComboBox1: TComboBox;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BayarFm: TBayarFm;

implementation

uses MenuUtama, StrUtils;

{$R *.dfm}

procedure TBayarFm.Button1Click(Sender: TObject);
begin
if MessageDlg('Apakah anda yakin akan membayar PO dengan kode '+ edit1.Text +'?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
begin
   Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BayarPO.rpt';
  Crpe1.ParamFields[0].CurrentValue:=edit1.Text;

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


  Crpe1.Execute;
  try
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('update po set status='+QuotedStr('PAID')+' where kode='+QuotedStr(edit1.Text));
  SembarangQ.ExecSQL;

  edit1.Text:='';
  ComboBox1.Text:='';
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select po.kode from po where status='+QuotedStr('FINISHED'));
  SembarangQ.ExecSQL;
  SembarangQ.Open;

  edit1.Items.Clear;
  while not SembarangQ.Eof do
  begin
      edit1.Items.Add(SembarangQ.fieldbyname('kode').Value);
      SembarangQ.Next;
  end;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select distinct s.namatoko from po, supplier s where s.kode=po.supplier and po.status='+QuotedStr('FINISHED'));
  SembarangQ.ExecSQL;
  SembarangQ.Open;

  ComboBox1.Items.Clear;
  while not SembarangQ.Eof do
  begin
      ComboBox1.Items.Add(SembarangQ.fieldbyname('namatoko').Value);
      SembarangQ.Next;
  end;
  
  FormActivate(sender);
  except
  end;
end;


end;

procedure TBayarFm.Button2Click(Sender: TObject);
begin
Crpe1.Refresh;
Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BayarPO_Detail.rpt';
Crpe1.ParamFields[0].CurrentValue:=edit1.Text;

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


Crpe1.Execute;
end;

procedure TBayarFm.Edit1Change(Sender: TObject);
begin
SembarangQ.SQL.Clear;
SembarangQ.SQL.Add('select status from po where kode='+QuotedStr(edit1.Text));
SembarangQ.ExecSQL;
SembarangQ.Open;
LblKet.Caption:='-';
while not SembarangQ.Eof do
begin
  if SembarangQ.FieldByName('status').Value='PAID' THEN
    LblKet.Caption:='Sudah Pernah Bayar. ';
  SembarangQ.Next;
end;


SembarangQ.SQL.Clear;
SembarangQ.SQL.Add('select * from retur where kodepo='+QuotedStr(edit1.Text));
SembarangQ.ExecSQL;
SembarangQ.Open;
lblket2.Caption:='-';
while not SembarangQ.Eof do
begin
   LblKet2.Caption:='Ada Retur. ';
   SembarangQ.Next;
end;
end;

procedure TBayarFm.FormActivate(Sender: TObject);
begin
SembarangQ.SQL.Clear;
SembarangQ.SQL.Add('select po.kode from po where status='+QuotedStr('FINISHED'));
SembarangQ.ExecSQL;
SembarangQ.Open;

edit1.Items.Clear;
while not SembarangQ.Eof do
begin
    edit1.Items.Add(SembarangQ.fieldbyname('kode').Value);
    SembarangQ.Next;
end;

SembarangQ.SQL.Clear;
SembarangQ.SQL.Add('select distinct s.namatoko from po, supplier s where s.kode=po.supplier and po.status='+QuotedStr('FINISHED'));
SembarangQ.ExecSQL;
SembarangQ.Open;

ComboBox1.Items.Clear;
while not SembarangQ.Eof do
begin
    ComboBox1.Items.Add(SembarangQ.fieldbyname('namatoko').Value);
    SembarangQ.Next;
end;

end;

procedure TBayarFm.ComboBox1Change(Sender: TObject);
begin
SembarangQ.SQL.Clear;
SembarangQ.SQL.Add('select po.kode from po, supplier s where s.kode=po.supplier and s.namatoko='+QuotedStr(ComboBox1.Text)+' and po.status='+QuotedStr('FINISHED'));
SembarangQ.ExecSQL;
SembarangQ.Open;

edit1.Items.Clear;
while not SembarangQ.Eof do
begin
    edit1.Items.Add(SembarangQ.fieldbyname('kode').Value);
    SembarangQ.Next;
end;
Edit1.ItemIndex:=0;

end;

end.
