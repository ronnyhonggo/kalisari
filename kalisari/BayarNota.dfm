object BayarNotaFm: TBayarNotaFm
  Left = 303
  Top = 194
  Width = 905
  Height = 383
  BorderIcons = [biSystemMenu]
  Caption = 'Bayar Nota'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 889
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 291
    Width = 889
    Height = 35
    Align = alBottom
    AutoSize = True
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 100
      Top = 9
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 11
      Top = 9
      Width = 75
      Height = 25
      Caption = 'BAYAR'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 216
      Top = 9
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      Visible = False
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 892
      Top = 1
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 305
    Height = 243
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 108
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridKode: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKodeEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kode'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridSupplier: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Supplier'
      Properties.Options.Editing = False
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridTermin: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Termin'
      Properties.Options.Editing = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridTglKirim: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglKirim'
      Properties.Options.Editing = False
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridGrandTotal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GrandTotal'
      Properties.Options.Editing = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      Properties.Options.Editing = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridStatusKirim: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StatusKirim'
      Properties.Options.Editing = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridDiskon: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Diskon'
      Properties.Options.Editing = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridKirim: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kirim'
      Properties.Options.Editing = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridAmbil: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Ambil'
      Properties.Options.Editing = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 326
    Width = 889
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 307
    Top = 48
    Width = 582
    Height = 243
    Align = alRight
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column8: TcxGridDBColumn
        DataBinding.FieldName = 'KodeRetur'
        Width = 68
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        Width = 78
      end
      object cxGrid1DBTableView1Column9: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 72
      end
      object cxGrid1DBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'Jumlah'
        Width = 45
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Alasan'
        Width = 73
      end
      object cxGrid1DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'TglTargetTukar'
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'StatusRetur'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Width = 123
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from PO where Status = '#39'FINISHED'#39)
    UpdateObject = MasterUS
    Left = 209
    Top = 97
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object MasterQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object MasterQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object MasterQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object MasterQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object MasterQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 164
    Top = 94
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Supplier, Termin, TglKirim, GrandTotal, Status, Cre' +
        'ateDate, CreateBy, Operator, TglEntry, TglCetak, StatusKirim, Di' +
        'skon, Kirim, Ambil'
      'from PO'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update PO'
      'set'
      '  Kode = :Kode,'
      '  Supplier = :Supplier,'
      '  Termin = :Termin,'
      '  TglKirim = :TglKirim,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  StatusKirim = :StatusKirim,'
      '  Diskon = :Diskon,'
      '  Kirim = :Kirim,'
      '  Ambil = :Ambil'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into PO'
      
        '  (Kode, Supplier, Termin, TglKirim, GrandTotal, Status, CreateD' +
        'ate, CreateBy, Operator, TglEntry, TglCetak, StatusKirim, Diskon' +
        ', Kirim, Ambil)'
      'values'
      
        '  (:Kode, :Supplier, :Termin, :TglKirim, :GrandTotal, :Status, :' +
        'CreateDate, :CreateBy, :Operator, :TglEntry, :TglCetak, :StatusK' +
        'irim, :Diskon, :Kirim, :Ambil)')
    DeleteSQL.Strings = (
      'delete from PO'
      'where'
      '  Kode = :OLD_Kode')
    Left = 188
    Top = 138
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select top 1 kode from PO order by kode desc')
    Left = 249
    Top = 103
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      'select * from detailretur where PotongPO = '#39'true'#39)
    UpdateObject = SDUpdateSQL1
    Left = 497
    Top = 89
    object DetailQKodeRetur: TStringField
      FieldName = 'KodeRetur'
      Required = True
      Size = 10
    end
    object DetailQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailQAlasan: TMemoField
      FieldName = 'Alasan'
      Required = True
      BlobType = ftMemo
    end
    object DetailQTglTargetTukar: TDateTimeField
      FieldName = 'TglTargetTukar'
    end
    object DetailQPotongPO: TBooleanField
      FieldName = 'PotongPO'
    end
    object DetailQStatusRetur: TStringField
      FieldName = 'StatusRetur'
      Size = 50
    end
    object DetailQPesanUlang: TBooleanField
      FieldName = 'PesanUlang'
    end
    object DetailQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 10
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 452
    Top = 86
  end
  object JenisQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 569
    Top = 9
    object JenisQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DetailBarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    AfterDelete = DetailBarangQAfterDelete
    SQL.Strings = (
      'select * from detailbarang')
    Left = 601
    Top = 17
    object DetailBarangQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailBarangQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from detailbarang order by kode desc')
    Left = 377
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeJenisKendaraan'
      'from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    ModifySQL.Strings = (
      'update detailbarang'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeJenisKendaraan = :KodeJenisKendaraan'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    InsertSQL.Strings = (
      'insert into detailbarang'
      '  (KodeBarang, KodeJenisKendaraan)'
      'values'
      '  (:KodeBarang, :KodeJenisKendaraan)')
    DeleteSQL.Strings = (
      'delete from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    Left = 524
    Top = 2
  end
  object ListSupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = ListSupplierQAfterDelete
    SQL.Strings = (
      
        'select *, '#39#39' as NamaSupplier from listsupplier where kodebarang=' +
        ' :text')
    UpdateObject = SDUpdateSQL2
    Left = 689
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ListSupplierQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object ListSupplierQKodeSupplier: TStringField
      FieldName = 'KodeSupplier'
      Required = True
      Size = 10
    end
    object ListSupplierQNamaSupplier: TStringField
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'KodeSupplier'
      Required = True
      Size = 50
      Lookup = True
    end
  end
  object SDUpdateSQL2: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeSupplier'#13#10'from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    ModifySQL.Strings = (
      'update listsupplier'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeSupplier = :KodeSupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    InsertSQL.Strings = (
      'insert into listsupplier'
      '  (KodeBarang, KodeSupplier)'
      'values'
      '  (:KodeBarang, :KodeSupplier)')
    DeleteSQL.Strings = (
      'delete from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    Left = 732
    Top = 10
  end
  object DataSource2: TDataSource
    DataSet = ListSupplierQ
    Left = 652
    Top = 6
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from supplier')
    Left = 801
    Top = 7
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object KategoriQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KategoriBarang')
    Left = 768
    Top = 8
    object KategoriQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriQKategori: TStringField
      FieldName = 'Kategori'
      Required = True
      Size = 50
    end
    object KategoriQMinStok: TIntegerField
      FieldName = 'MinStok'
    end
    object KategoriQMaxStok: TIntegerField
      FieldName = 'MaxStok'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Barang')
    Left = 536
    Top = 88
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object POQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from PO where Status = '#39'FINISHED'#39)
    Left = 208
    Top = 64
    object POQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object POQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object POQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object POQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object POQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object POQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object POQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object POQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object POQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object POQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object POQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object POQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object POQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object POQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object POQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
end
