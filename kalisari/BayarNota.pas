unit BayarNota;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel;

type
  TBayarNotaFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    DetailQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    DataSource1: TDataSource;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    JenisQ: TSDQuery;
    JenisQKode: TStringField;
    JenisQNamaJenis: TStringField;
    JenisQTipe: TMemoField;
    JenisQKeterangan: TMemoField;
    DetailBarangQ: TSDQuery;
    Kode2Q: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    Kode2Qkode: TStringField;
    cxLabel1: TcxLabel;
    ListSupplierQ: TSDQuery;
    SDUpdateSQL2: TSDUpdateSQL;
    DataSource2: TDataSource;
    ListSupplierQKodeBarang: TStringField;
    ListSupplierQKodeSupplier: TStringField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQKategori: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    ListSupplierQNamaSupplier: TStringField;
    DetailBarangQKodeBarang: TStringField;
    DetailBarangQKodeJenisKendaraan: TStringField;
    KategoriQ: TSDQuery;
    KategoriQKode: TStringField;
    KategoriQKategori: TStringField;
    KategoriQMinStok: TIntegerField;
    KategoriQMaxStok: TIntegerField;
    MasterQKode: TStringField;
    MasterQSupplier: TStringField;
    MasterQTermin: TStringField;
    MasterQTglKirim: TDateTimeField;
    MasterQGrandTotal: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatusKirim: TStringField;
    MasterQDiskon: TCurrencyField;
    MasterQKirim: TBooleanField;
    MasterQAmbil: TBooleanField;
    DetailQKodeRetur: TStringField;
    DetailQBarang: TStringField;
    DetailQJumlah: TIntegerField;
    DetailQAlasan: TMemoField;
    DetailQTglTargetTukar: TDateTimeField;
    DetailQPotongPO: TBooleanField;
    DetailQStatusRetur: TStringField;
    DetailQPesanUlang: TBooleanField;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    KodeQkode: TStringField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TIntegerField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    DetailQNamaBarang: TStringField;
    cxGrid1DBTableView1Column9: TcxGridDBColumn;
    MasterVGridKode: TcxDBEditorRow;
    MasterVGridSupplier: TcxDBEditorRow;
    MasterVGridTermin: TcxDBEditorRow;
    MasterVGridTglKirim: TcxDBEditorRow;
    MasterVGridGrandTotal: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridStatusKirim: TcxDBEditorRow;
    MasterVGridDiskon: TcxDBEditorRow;
    MasterVGridKirim: TcxDBEditorRow;
    MasterVGridAmbil: TcxDBEditorRow;
    POQ: TSDQuery;
    POQKode: TStringField;
    POQSupplier: TStringField;
    POQTermin: TStringField;
    POQTglKirim: TDateTimeField;
    POQGrandTotal: TCurrencyField;
    POQStatus: TStringField;
    POQCreateDate: TDateTimeField;
    POQCreateBy: TStringField;
    POQOperator: TStringField;
    POQTglEntry: TDateTimeField;
    POQTglCetak: TDateTimeField;
    POQStatusKirim: TStringField;
    POQDiskon: TCurrencyField;
    POQKirim: TBooleanField;
    POQAmbil: TBooleanField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure DetailBarangQAfterDelete(DataSet: TDataSet);
    procedure ExitBtnClick(Sender: TObject);
    procedure DetailQAfterDelete(DataSet: TDataSet);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure ListSupplierQAfterDelete(DataSet: TDataSet);
    procedure MasterVGridKategoriEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridKodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  BayarNotaFm: TBayarNotaFm;

  MasterOriSQL, DetailOriSQL, ListSupplierOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TBayarNotaFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TBayarNotaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TBayarNotaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TBayarNotaFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailQ.SQL.Text;
  ListSupplierOriSQL:=ListSupplierQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  DetailQ.open;
end;

procedure TBayarNotaFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  Detailq.Close;
  ListSupplierQ.Close;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TBayarNotaFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  //KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  //MasterQ.Close;
end;

procedure TBayarNotaFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterBarang.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarang.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBarang.AsBoolean;
end;

procedure TBayarNotaFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      savebtn.enabled:=menuutamafm.UserQInsertMasterBarang.AsBoolean;
      //MasterQ.Edit;
      //MasterQAktif.AsBoolean :=false;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TBayarNotaFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TBayarNotaFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    //DetailQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    //DetailQ.CommitUpdates;
    ShowMessage('Pembayaran dengan Kode ' + KodeEdit.Text + ' telah dilakukan');
    KodeEdit.SetFocus;
  except
           on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
{
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    //DetailQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal'); }
  end;
{  DetailQ.Close;
  detailq.SQL.Text:=DetailOriSQL;
  detailq.ParamByName('text').AsString:=KodeEdit.Text;
  detailq.Open; }
end;

procedure TBayarNotaFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TBayarNotaFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TBayarNotaFm.DeleteBtnClick(Sender: TObject);
begin
{  if MessageDlg('Hapus Barang '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Barang telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Barang pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;}
  MasterQ.Edit;
  MasterQStatus.AsString:='PAID';
  SaveBtnClick(sender);

end;

procedure TBayarNotaFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
     { detailq.Close;
      detailq.ParamByName('text').AsString := MasterQKode.AsString;
      detailq.Open; }
    end;
    DropDownFm.Release;
end;

procedure TBayarNotaFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TBayarNotaFm.MasterVGridEnter(Sender: TObject);
begin
  //mastervgrid.FocusRow(MasterVGridNama);
end;

procedure TBayarNotaFm.cxButton1Click(Sender: TObject);
var i : integer;
begin
  for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
  begin
    showmessage(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[1])
  end;

end;

procedure TBayarNotaFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  //ShowMessage(inttostr(abuttonindex));
 {
  if abuttonindex=7 then
  begin
    DropDownFm:=TDropdownfm.Create(Self,JenisQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      detailq.Open;
      detailq.Insert;
      DetailQKodeBarang.AsString:=masterqkode.AsString;
      detailqkodejeniskendaraan.AsString:=jenisqkode.AsString;
      detailqnamajenis.AsString:=jenisqnamajenis.asstring;
      detailqtipe.asstring:=jenisqtipe.asstring;
      detailq.Post;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;

    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
 // end;
end;

procedure TBayarNotaFm.DetailBarangQAfterDelete(DataSet: TDataSet);
begin
    ShowMessage('a');
    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;
end;

procedure TBayarNotaFm.ExitBtnClick(Sender: TObject);
begin
  release;
end;

procedure TBayarNotaFm.DetailQAfterDelete(DataSet: TDataSet);
begin
MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
end;

procedure TBayarNotaFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=7 then
  begin
    DropDownFm:=TDropdownfm.Create(Self,SupplierQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      ListSupplierQ.Open;
      listsupplierq.Insert;
      ListSupplierQKodeBarang.AsString:=masterqkode.AsString;
      ListSupplierQKodeSupplier.AsString:=supplierqkode.AsString;
      listsupplierq.Post;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
      listsupplierq.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      listsupplierq.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      listsupplierq.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      listsupplierq.Close;
      listsupplierq.SQL.Text:=ListSupplierOriSQL;
      listsupplierq.ExecSQL;
      listsupplierq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;

    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TBayarNotaFm.ListSupplierQAfterDelete(DataSet: TDataSet);
begin
    MenuUtamaFm.Database1.StartTransaction;
    try
      ListSupplierQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      ListSupplierQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      ListSupplierQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      ListSupplierQ.Close;
      ListSupplierQ.SQL.Text:=ListSupplierOriSQL;
      ListSupplierQ.ExecSQL;
      ListSupplierQ.Open;
    end;
end;




procedure TBayarNotaFm.MasterVGridKategoriEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    {KategoriQ.Close;
    //KategoriQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    KategoriQ.ExecSQL;
    KategoriQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,KategoriQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKategori.AsString:=KategoriQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;     }
end;

procedure TBayarNotaFm.MasterVGridKodeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    POQ.Close;
    POQ.ExecSQL;
    POQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,POQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKode.AsString:=POQKode.AsString;
      MasterQSupplier.AsString:=POQSupplier.AsString;
      MasterQTermin.AsString:=POQTermin.AsString;
      MasterQTglKirim.AsString:=POQTglKirim.AsString;
      MasterQGrandTotal.AsString:=POQGrandTotal.AsString;
      MasterQStatus.AsString:=POQStatus.AsString;
      MasterQStatusKirim.AsString:=POQStatusKirim.AsString;
      MasterQDiskon.AsString:=POQDiskon.AsString;
      MasterQKirim.AsString:=POQKirim.AsString;
      MasterQAmbil.AsString:=POQAmbil.AsString;

    end;
    DropDownFm.Release;

end;

end.
