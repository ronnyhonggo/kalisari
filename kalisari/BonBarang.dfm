object BonBarangFm: TBonBarangFm
  Left = 157
  Top = 11
  BorderStyle = bsDialog
  Caption = 'Bon Barang'
  ClientHeight = 704
  ClientWidth = 1020
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 29
    Width = 106
    Height = 16
    Caption = 'Kode Bon Barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object LblGrandTotal: TLabel
    Left = 240
    Top = 288
    Width = 11
    Height = 20
    Alignment = taRightJustify
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label2: TLabel
    Left = 187
    Top = 288
    Width = 24
    Height = 20
    Alignment = taRightJustify
    Caption = 'Rp'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 16
    Top = 120
    Width = 313
    Height = 73
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 159
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 2
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid1Kode: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kode'
      Visible = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Peminta: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Peminta'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Penyetuju: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Penyetuju'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1LaporanPerbaikan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1LaporanPerbaikanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'LaporanPerbaikan'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1LaporanPerawatan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1LaporanPerawatanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'LaporanPerawatan'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1Status: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      Visible = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1CreateDate: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'CreateDate'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1CreateBy: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'CreateBy'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1Operator: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Operator'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1TglEntry: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglEntry'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1Armada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Armada'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1TglCetak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglCetak'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1Storing: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1StoringEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Storing'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
  end
  object cxGrid3: TcxGrid
    Left = 352
    Top = 24
    Width = 729
    Height = 433
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellClick = cxGridDBTableView1CellClick
      DataController.DataSource = DataSourceCoba
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1Urgent: TcxGridDBColumn
        DataBinding.FieldName = 'Urgent'
        Width = 53
      end
      object cxGridDBTableView1KodeBonBarang: TcxGridDBColumn
        DataBinding.FieldName = 'KodeBonBarang'
        Visible = False
        Width = 182
      end
      object cxGridDBTableView1nama: TcxGridDBColumn
        Caption = 'Nama Barang'
        DataBinding.FieldName = 'nama'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGridDBTableView1namaPropertiesButtonClick
        Width = 159
      end
      object cxGridDBTableView1KodeBarang: TcxGridDBColumn
        DataBinding.FieldName = 'KodeBarang'
        Visible = False
        Width = 92
      end
      object cxGridDBTableView1jumlah: TcxGridDBColumn
        Caption = 'Stok'
        DataBinding.FieldName = 'jumlah'
        Options.Editing = False
        Width = 46
      end
      object cxGridDBTableView1JumlahDiminta: TcxGridDBColumn
        Caption = 'Minta'
        DataBinding.FieldName = 'JumlahDiminta'
        Width = 45
      end
      object cxGridDBTableView1satuan: TcxGridDBColumn
        Caption = 'Satuan'
        DataBinding.FieldName = 'satuan'
        Options.Editing = False
        Width = 51
      end
      object cxGridDBTableView1StatusMinta: TcxGridDBColumn
        DataBinding.FieldName = 'StatusMinta'
        Visible = False
        Width = 96
      end
      object cxGridDBTableView1harga: TcxGridDBColumn
        DataBinding.FieldName = 'harga'
        Visible = False
      end
      object cxGridDBTableView1subtotal: TcxGridDBColumn
        DataBinding.FieldName = 'subtotal'
        Visible = False
      end
      object cxGridDBTableView1pendink_gudang: TcxGridDBColumn
        Caption = 'Total Minta'
        DataBinding.FieldName = 'pendink_gudang'
        PropertiesClassName = 'TcxCalcEditProperties'
        Options.Editing = False
        Width = 76
      end
      object cxGridDBTableView1pendink_db: TcxGridDBColumn
        Caption = 'Pending Beli'
        DataBinding.FieldName = 'pendink_db'
        Options.Editing = False
        Width = 84
      end
      object cxGridDBTableView1pendink_po: TcxGridDBColumn
        Caption = 'Pending PO'
        DataBinding.FieldName = 'pendink_po'
        Options.Editing = False
        Width = 80
      end
      object cxGridDBTableView1JumlahBeli: TcxGridDBColumn
        Caption = 'Beli'
        DataBinding.FieldName = 'JumlahBeli'
        Width = 43
      end
      object cxGridDBTableView1Keterangan: TcxGridDBColumn
        DataBinding.FieldName = 'Keterangan'
        Width = 141
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object KodeEdit: TcxButtonEdit
    Left = 128
    Top = 26
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Properties.OnButtonClick = KodeEditPropertiesButtonClick
    Properties.OnChange = KodeEditPropertiesChange
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 0
    Width = 201
  end
  object SearchBtn: TcxButton
    Left = 48
    Top = 34
    Width = 65
    Height = 23
    Caption = 'Search'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    TabStop = False
    Visible = False
    OnClick = SearchBtnClick
  end
  object BtnSave: TButton
    Left = 56
    Top = 288
    Width = 217
    Height = 33
    Caption = 'Save Mekanik'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = BtnSaveClick
  end
  object Panel1: TPanel
    Left = 16
    Top = 64
    Width = 313
    Height = 57
    Color = clBtnHighlight
    TabOrder = 4
    object RbtStok: TRadioButton
      Left = 8
      Top = 8
      Width = 113
      Height = 17
      Caption = 'Tambah Stok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = RbtStokClick
    end
    object RbtBaikan: TRadioButton
      Left = 168
      Top = 8
      Width = 113
      Height = 17
      Caption = 'Perbaikan'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      TabStop = True
      OnClick = RbtBaikanClick
    end
    object RbtRawat: TRadioButton
      Left = 168
      Top = 32
      Width = 113
      Height = 17
      Caption = 'Perawatan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = RbtRawatClick
    end
    object RbtStoring: TRadioButton
      Left = 8
      Top = 32
      Width = 113
      Height = 17
      Caption = 'Storing'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = RbtStoringClick
    end
  end
  object Panel2: TPanel
    Left = 16
    Top = 184
    Width = 313
    Height = 97
    Color = clBtnHighlight
    TabOrder = 8
    object LblBaik: TLabel
      Left = 16
      Top = 24
      Width = 97
      Height = 16
      Caption = 'Jenis Perbaikan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LblRawat: TLabel
      Left = 16
      Top = 24
      Width = 99
      Height = 16
      Caption = 'Jenis Perawatan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 0
      Width = 48
      Height = 16
      Caption = 'Armada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LblPlat: TLabel
      Left = 160
      Top = 0
      Width = 24
      Height = 16
      Caption = '        '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ListBox1: TListBox
      Left = 160
      Top = 24
      Width = 145
      Height = 65
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object Button1: TButton
    Left = 944
    Top = 552
    Width = 113
    Height = 33
    Caption = 'Save and Print'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    Visible = False
    OnClick = Button1Click
  end
  object BtnSetuju: TBitBtn
    Left = 56
    Top = 368
    Width = 217
    Height = 33
    Caption = 'Setuju dan Proses !'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = BtnSetujuClick
    Kind = bkAll
  end
  object cxGrid1: TcxGrid
    Left = 16
    Top = 464
    Width = 913
    Height = 225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellClick = cxGrid1DBTableView1CellClick
      DataController.DataSource = DataSource2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        Caption = 'Bon Barang'
        DataBinding.FieldName = 'Kode'
        Options.Editing = False
        Width = 99
      end
      object cxGrid1DBTableView1Peminta: TcxGridDBColumn
        DataBinding.FieldName = 'Peminta'
        Options.Editing = False
        Width = 139
      end
      object cxGrid1DBTableView1Penyetuju: TcxGridDBColumn
        DataBinding.FieldName = 'Penyetuju'
        Options.Editing = False
        Width = 123
      end
      object cxGrid1DBTableView1LaporanPerbaikan: TcxGridDBColumn
        DataBinding.FieldName = 'LaporanPerbaikan'
        Options.Editing = False
        Width = 118
      end
      object cxGrid1DBTableView1LaporanPerawatan: TcxGridDBColumn
        DataBinding.FieldName = 'LaporanPerawatan'
        Options.Editing = False
        Width = 122
      end
      object cxGrid1DBTableView1Storing: TcxGridDBColumn
        DataBinding.FieldName = 'Storing'
        Width = 80
      end
      object cxGrid1DBTableView1platno: TcxGridDBColumn
        DataBinding.FieldName = 'platno'
        Options.Editing = False
        Width = 97
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Options.Editing = False
        Width = 119
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Button2: TButton
    Left = 952
    Top = 480
    Width = 121
    Height = 41
    Caption = 'Refresh New '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = Button2Click
  end
  object BtnKepalaTeknik: TButton
    Left = 56
    Top = 328
    Width = 217
    Height = 33
    Caption = 'Approve Kepala Teknik'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    WordWrap = True
    OnClick = BtnKepalaTeknikClick
  end
  object BtnDelete: TButton
    Left = 56
    Top = 416
    Width = 217
    Height = 33
    Hint = 'tes'
    Caption = 'Delete Bon Barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnClick = BtnDeleteClick
  end
  object CobaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    BeforePost = CobaQBeforePost
    AfterPost = CobaQAfterPost
    SQL.Strings = (
      
        'select b.nama,dbb.*,0 as harga, 0 as subtotal, b.jumlah as jumla' +
        'h, 0 as pendink_gudang, 0 as pendink_db, 0 as pendink_po, b.satu' +
        'an'
      'from detailbonbarang dbb, barang b'
      'where dbb.kodebarang=b.kode'
      'and dbb.kodebonbarang=:kodebon')
    UpdateObject = SDUpdateSQL1
    Left = 464
    Top = 65528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodebon'
        ParamType = ptInput
      end>
    object CobaQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object CobaQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object CobaQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object CobaQJumlahDiminta: TIntegerField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object CobaQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object CobaQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CobaQStatusMinta: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object CobaQharga: TIntegerField
      FieldName = 'harga'
      Required = True
    end
    object CobaQsubtotal: TIntegerField
      FieldName = 'subtotal'
      Required = True
    end
    object CobaQjumlah: TIntegerField
      FieldName = 'jumlah'
      Required = True
    end
    object CobaQpendink_gudang: TIntegerField
      FieldName = 'pendink_gudang'
      Required = True
    end
    object CobaQpendink_db: TIntegerField
      FieldName = 'pendink_db'
      Required = True
    end
    object CobaQpendink_po: TIntegerField
      FieldName = 'pendink_po'
      Required = True
    end
    object CobaQStatusBeli: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object CobaQsatuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
    object CobaQUrgent: TBooleanField
      FieldName = 'Urgent'
    end
  end
  object DataSourceCoba: TDataSource
    DataSet = CobaQ
    Left = 392
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'#13#10'from detailbonbarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update detailbonbarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into detailbonbarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from detailbonbarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 416
  end
  object HeaderQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from bonbarang'
      'where kode=:kodebon')
    UpdateObject = SDUpdateSQLHeader
    Left = 16
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'kodebon'
        ParamType = ptInput
      end>
    object HeaderQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HeaderQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 50
    end
    object HeaderQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object HeaderQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object HeaderQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object HeaderQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object HeaderQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object HeaderQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object HeaderQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object HeaderQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object HeaderQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object HeaderQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object HeaderQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object HeaderQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 50
    end
  end
  object SDUpdateSQLHeader: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Peminta, Penyetuju, Penerima, LaporanPerbaikan, Lap' +
        'oranPerawatan, Storing, Status, CreateDate, CreateBy, Operator, ' +
        'TglEntry, Armada, TglCetak'#13#10'from bonbarang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update bonbarang'
      'set'
      '  Kode = :Kode,'
      '  Peminta = :Peminta,'
      '  Penyetuju = :Penyetuju,'
      '  Penerima = :Penerima,'
      '  LaporanPerbaikan = :LaporanPerbaikan,'
      '  LaporanPerawatan = :LaporanPerawatan,'
      '  Storing = :Storing,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Armada = :Armada,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into bonbarang'
      
        '  (Kode, Peminta, Penyetuju, Penerima, LaporanPerbaikan, Laporan' +
        'Perawatan, Storing, Status, CreateDate, CreateBy, Operator, TglE' +
        'ntry, Armada, TglCetak)'
      'values'
      
        '  (:Kode, :Peminta, :Penyetuju, :Penerima, :LaporanPerbaikan, :L' +
        'aporanPerawatan, :Storing, :Status, :CreateDate, :CreateBy, :Ope' +
        'rator, :TglEntry, :Armada, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from bonbarang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 40
    Top = 272
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from bonbarang order by kode desc')
    Left = 312
    Top = 432
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = HeaderQ
    Left = 72
    Top = 272
  end
  object SembarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      '')
    Left = 672
    Top = 112
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang where kode=:kodebarang_detail')
    UpdateObject = UpdateBarang
    Left = 312
    Top = 408
    ParamData = <
      item
        DataType = ftString
        Name = 'kodebarang_detail'
        ParamType = ptInput
      end>
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object UpdateBarang: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Sta' +
        'ndardUmur, Lokasi, CreateDate, CreateBy, Operator, TglEntry'#13#10'fro' +
        'm barang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  MinimumStok = :MinimumStok,'
      '  MaximumStok = :MaximumStok,'
      '  StandardUmur = :StandardUmur,'
      '  Lokasi = :Lokasi,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      
        '  (Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Standar' +
        'dUmur, Lokasi, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :Nama, :Jumlah, :Satuan, :MinimumStok, :MaximumStok, :' +
        'StandardUmur, :Lokasi, :CreateDate, :CreateBy, :Operator, :TglEn' +
        'try)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 312
    Top = 384
  end
  object DaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli')
    UpdateObject = UpdateDaftarBeli
    Left = 416
    Top = 192
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
  end
  object UpdateDaftarBeli: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status'#13#10'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 456
    Top = 192
  end
  object KodedbQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from daftarbeli order by kode desc')
    Left = 384
    Top = 192
    object KodedbQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\intan\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    PrintOptions.StartPage = 1
    PrintOptions.StopPage = 65535
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcMagnify
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcMagnify
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcMagnify
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 312
    Top = 360
  end
  object ViewQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select b.*,a.platno from bonbarang b, armada a'
      'where b.armada=a.kode and b.status<>'#39'FINISHED'#39
      'order by b.kode ')
    Left = 384
    Top = 400
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 50
    end
    object ViewQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object ViewQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object ViewQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object ViewQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewQplatno: TStringField
      FieldName = 'platno'
      Required = True
      Size = 10
    end
    object ViewQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
  end
  object DataSource2: TDataSource
    DataSet = ViewQ
    Left = 424
    Top = 400
  end
end
