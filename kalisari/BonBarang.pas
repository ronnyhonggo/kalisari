unit BonBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  SDEngine, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxButtonEdit,
  cxVGrid, cxDBVGrid, cxInplaceContainer, StdCtrls, cxContainer, Menus,
  cxButtons, cxTextEdit, cxMaskEdit, ComCtrls, cxDropDownEdit, ExtCtrls,
  cxCheckBox, UCrpeClasses, UCrpe32, Buttons, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxCalc;

type
  TBonBarangFm = class(TForm)
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    CobaQ: TSDQuery;
    DataSourceCoba: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    SearchBtn: TcxButton;
    BtnSave: TButton;
    HeaderQ: TSDQuery;
    SDUpdateSQLHeader: TSDUpdateSQL;
    HeaderQKode: TStringField;
    HeaderQPeminta: TStringField;
    HeaderQPenyetuju: TStringField;
    HeaderQLaporanPerbaikan: TStringField;
    HeaderQLaporanPerawatan: TStringField;
    HeaderQStatus: TStringField;
    HeaderQCreateDate: TDateTimeField;
    HeaderQCreateBy: TStringField;
    HeaderQOperator: TStringField;
    HeaderQTglEntry: TDateTimeField;
    HeaderQArmada: TStringField;
    HeaderQTglCetak: TDateTimeField;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    DataSource1: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    LblGrandTotal: TLabel;
    SembarangQ: TSDQuery;
    Label2: TLabel;
    BarangQ: TSDQuery;
    UpdateBarang: TSDUpdateSQL;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TIntegerField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    DaftarBeliQ: TSDQuery;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    UpdateDaftarBeli: TSDUpdateSQL;
    KodedbQ: TSDQuery;
    KodedbQkode: TStringField;
    CobaQnama: TStringField;
    CobaQKodeBonBarang: TStringField;
    CobaQKodeBarang: TStringField;
    CobaQJumlahDiminta: TIntegerField;
    CobaQJumlahBeli: TIntegerField;
    CobaQKeterangan: TMemoField;
    CobaQStatusMinta: TStringField;
    CobaQharga: TIntegerField;
    CobaQsubtotal: TIntegerField;
    CobaQjumlah: TIntegerField;
    cxGridDBTableView1nama: TcxGridDBColumn;
    cxGridDBTableView1KodeBonBarang: TcxGridDBColumn;
    cxGridDBTableView1KodeBarang: TcxGridDBColumn;
    cxGridDBTableView1JumlahDiminta: TcxGridDBColumn;
    cxGridDBTableView1JumlahBeli: TcxGridDBColumn;
    cxGridDBTableView1Keterangan: TcxGridDBColumn;
    cxGridDBTableView1StatusMinta: TcxGridDBColumn;
    cxGridDBTableView1harga: TcxGridDBColumn;
    cxGridDBTableView1subtotal: TcxGridDBColumn;
    cxGridDBTableView1jumlah: TcxGridDBColumn;
    CobaQpendink_gudang: TIntegerField;
    cxGridDBTableView1pendink_gudang: TcxGridDBColumn;
    CobaQpendink_db: TIntegerField;
    CobaQpendink_po: TIntegerField;
    cxGridDBTableView1pendink_db: TcxGridDBColumn;
    cxGridDBTableView1pendink_po: TcxGridDBColumn;
    Panel1: TPanel;
    RbtStok: TRadioButton;
    RbtBaikan: TRadioButton;
    RbtRawat: TRadioButton;
    Panel2: TPanel;
    LblBaik: TLabel;
    ListBox1: TListBox;
    LblRawat: TLabel;
    cxDBVerticalGrid1Kode: TcxDBEditorRow;
    cxDBVerticalGrid1Peminta: TcxDBEditorRow;
    cxDBVerticalGrid1Penyetuju: TcxDBEditorRow;
    cxDBVerticalGrid1LaporanPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1LaporanPerawatan: TcxDBEditorRow;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    cxDBVerticalGrid1CreateDate: TcxDBEditorRow;
    cxDBVerticalGrid1CreateBy: TcxDBEditorRow;
    cxDBVerticalGrid1Operator: TcxDBEditorRow;
    cxDBVerticalGrid1TglEntry: TcxDBEditorRow;
    cxDBVerticalGrid1Armada: TcxDBEditorRow;
    cxDBVerticalGrid1TglCetak: TcxDBEditorRow;
    Label1: TLabel;
    LblPlat: TLabel;
    CobaQStatusBeli: TStringField;
    Crpe1: TCrpe;
    Button1: TButton;
    CobaQsatuan: TStringField;
    cxGridDBTableView1satuan: TcxGridDBColumn;
    BtnSetuju: TBitBtn;
    CobaQUrgent: TBooleanField;
    cxGridDBTableView1Urgent: TcxGridDBColumn;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ViewQ: TSDQuery;
    DataSource2: TDataSource;
    ViewQKode: TStringField;
    ViewQPeminta: TStringField;
    ViewQPenyetuju: TStringField;
    ViewQLaporanPerbaikan: TStringField;
    ViewQLaporanPerawatan: TStringField;
    ViewQStatus: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQArmada: TStringField;
    ViewQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Peminta: TcxGridDBColumn;
    cxGrid1DBTableView1Penyetuju: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    ViewQplatno: TStringField;
    cxGrid1DBTableView1platno: TcxGridDBColumn;
    Button2: TButton;
    BtnKepalaTeknik: TButton;
    BtnDelete: TButton;
    HeaderQStoring: TStringField;
    cxDBVerticalGrid1Storing: TcxDBEditorRow;
    RbtStoring: TRadioButton;
    HeaderQPenerima: TStringField;
    ViewQStoring: TStringField;
    cxGrid1DBTableView1Storing: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxGridDBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure KodeEditPropertiesChange(Sender: TObject);
    procedure cxGridDBTableView1namaPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure CobaQBeforePost(DataSet: TDataSet);
    procedure cxDBVerticalGrid1LaporanPerbaikanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1LaporanPerawatanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure RbtStokClick(Sender: TObject);
    procedure RbtBaikanClick(Sender: TObject);
    procedure RbtRawatClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtnSetujuClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnKepalaTeknikClick(Sender: TObject);
    procedure CobaQAfterPost(DataSet: TDataSet);
    procedure BtnDeleteClick(Sender: TObject);
    procedure cxDBVerticalGrid1StoringEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure RbtStoringClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    print:Boolean;
  end;

var
  BonBarangFm: TBonBarangFm;
  grandtotal:Integer;
  ctr:integer;
  save_klik:boolean;
  cari:boolean;
  temp:String;

implementation
uses MenuUtama,browse,BrowseName, StrUtils;

{$R *.dfm}

procedure TBonBarangFm.FormCreate(Sender: TObject);
begin
//ShowMessage(MenuUtamaFm.param_user);
save_klik:=false;
cari:=false;

BtnSave.Enabled:=true;
BtnKepalaTeknik.Enabled:=false;
BtnSetuju.Enabled:=false;

grandtotal:=0;
KodeEdit.Text:='Insert Baru';
HeaderQ.Open;
HeaderQ.Insert;
HeaderQ.Edit;
HeaderQArmada.AsString:='0000000000';

CobaQ.Open;
CobaQ.Insert;

if RbtBaikan.Checked then
  begin
    cxDBVerticalGrid1Storing.Visible:=false;
    cxDBVerticalGrid1LaporanPerbaikan.Visible:=true;
    cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
    Panel2.Visible:=true;
    LblBaik.Visible:=true;
    LblRawat.Visible:=false ;

    LblPlat.Caption:='';
    ListBox1.Items.Clear;
    HeaderQLaporanPerbaikan.Clear;
    HeaderQLaporanPerawatan.Clear;
    HeaderQArmada.AsString:='';
  end;

MenuUtamaFm.param_sql_detail:=CobaQ.SQL.Text;

end;

procedure TBonBarangFm.cxGridDBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var i:Integer;
begin
//ShowMessage(CobaQPendingDaftarBeli.AsString);
{  try
    CobaQsubtotal.AsInteger:=CobaQjumlahbeli.AsInteger*CobaQharga.AsInteger;

    grandtotal:=0;
    for i:=0 to cxGridDBTableView1.DataController.RecordCount do
    begin
      //ShowMessage(cxGridDBTableView1.ViewData.Rows[i].DisplayTexts[7]);
      grandtotal:=grandtotal+StrToInt(cxGridDBTableView1.ViewData.Rows[i].DisplayTexts[7]);
      //ShowMessage (IntToStr(grandtotal));
//      cxGrid2DBTableView1.ViewData.Rows[i+1].DisplayTexts[1]
      LblGrandTotal.Caption:=IntToStr(grandtotal);
    end;
//    LblGrandTotal.Caption:=IntToStr(grandtotal);
  except
  end;

  if (KodeEdit.Text='') or (KodeEdit.Text='Insert Baru') then
  begin
    try
      CobaQ.Open;
      CobaQ.Edit;
      CobaQKodeBonBarang.AsString:='tes';
    except
    end;
  end; }
end;

procedure TBonBarangFm.Button1Click(Sender: TObject);
var kodebon:String;
begin
print:=true;
HeaderQ.Edit;
HeaderQTglCetak.AsDateTime:=Now;

BtnSaveClick(Sender);
Crpe1.Refresh;
Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonBarang.rpt';
Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
KodeEdit.Text:='';

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


Crpe1.Execute;
end;

procedure TBonBarangFm.SearchBtnClick(Sender: TObject);
var temp:String;
 totminta,totkasih,stok:integer;
begin

  KodeEdit.Text:=ViewQKode.AsString;
  cari:=true;

  CobaQ.Close;
  CobaQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;


  while not CobaQ.Eof do
  begin
    CobaQ.Edit;
    IF HeaderQStatus.AsString='NEW BB' then
    begin
      
      //HITUNG PERKIRAAN JUMLAHBELI
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select sum(jumlahdiminta-jumlahbeli) as totalsebelum from detailbonbarang db, barang b');
      SembarangQ.SQL.Add('where b.kode=db.kodebarang and db.statusminta='+QuotedStr('ONPROCESS')+' and db.kodebarang='+ QuotedStr(CobaQKodeBarang.AsString));
      SembarangQ.SQL.Add('and db.kodebonbarang<'+ QuotedStr(CobaQKodeBonBarang.AsString));
      SembarangQ.Open;

        totminta:=0;
        stok:=0;
        totminta:=SembarangQ.fieldbyname('totalsebelum').AsInteger;
        stok:=CobaQjumlah.AsInteger;
        SembarangQ.Close;

        //ShowMessage('total sebelum= '+IntToStr(totminta)+', jum minta skr= '+IntToStr(CobaQJumlahDiminta.AsInteger)+', stok= '+IntToStr(stok));
        CobaQJumlahBeli.AsInteger:=totminta+CobaQJumlahDiminta.AsInteger-stok;
        if CobaQJumlahBeli.AsInteger<0 then CobaQJumlahBeli.AsInteger:=0;
    end;

    //CARI HARGA TERAKHIR DARI KODEBARANG ITU ===================================
    CobaQharga.AsInteger:=0;
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select hargalast from daftarbeli db where barang=' + QuotedStr(CobaQKodeBarang.AsString) + ' and kode=(select max(kode) from daftarbeli where barang=db.barang)');
    SembarangQ.Open;
      CobaQharga.AsInteger:=SembarangQ.fieldbyname('hargalast').AsInteger;
    SembarangQ.Close;

  //CARI JUMLAH PENDINK GUDANG DARI KODEBARANG ITU ===================================
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select sum(jumlahdiminta) as pendink_gudang from detailbonbarang db where db.statusminta='+QuotedStr('ONPROCESS')+' and db.kodebarang=' + QuotedStr(CobaQKodeBarang.AsString));
  SembarangQ.Open;

    totminta:=0;
    totminta:=SembarangQ.fieldbyname('pendink_gudang').AsInteger;
    SembarangQ.Close;

    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select sum(jumlahkeluar) as totalkasih from bonkeluarbarang bkb, detailbonbarang dbb,detailbkb dbkb where bkb.kode=dbkb.kodebkb and dbkb.barang=' + QuotedStr(CobaQKodeBarang.AsString));
    SembarangQ.SQL.Add(' and dbb.kodebarang=dbkb.barang and dbb.kodebonbarang=bkb.bonbarang and dbb.statusminta='+QuotedStr('ONPROCESS'));
    SembarangQ.Open;
      totkasih:=0;
      totkasih:=SembarangQ.fieldbyname('totalkasih').AsInteger;
    SembarangQ.Close;
    CobaQpendink_gudang.AsInteger:=totminta-totkasih;

  //CARI JUMLAH PENDINK DAFTAR BELI DARI KODEBARANG ITU ===================================
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('SELECT sum(jumlahbeli) as pendink_db from daftarbeli where (status='+QuotedStr('ONPROCESS')+' or status='+QuotedStr('NEW DB')+') and barang='+ QuotedStr(CobaQKodeBarang.AsString));
    SembarangQ.Open;
      CobaQpendink_db.AsInteger:=SembarangQ.fieldbyname('pendink_db').AsInteger;
    SembarangQ.Close;
  //==========================================================================

  //CARI JUMLAH PENDINK PO DARI KODEBARANG ITU ===================================
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('SELECT sum(jumlahbeli) as pendink_po from detailpo dpo, daftarbeli db where dpo.kodedaftarbeli=db.kode and status='+QuotedStr('PO')+' and barang='+ QuotedStr(CobaQKodeBarang.AsString));
    SembarangQ.Open;
      CobaQpendink_po.AsInteger:=SembarangQ.fieldbyname('pendink_po').AsInteger;
    SembarangQ.Close;
  //==========================================================================

    CobaQ.Post;
    CobaQ.Next;
  end;

  CobaQ.Insert;

  Panel1.Visible:=false;
  RbtBaikan.Checked:=true;
  RbtStok.Checked:=true;
  RbtRawat.Checked:=true;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;

  HeaderQ.Edit;
  HeaderQKode.AsString:=KodeEdit.Text;

  if (HeaderQLaporanPerawatan.IsNull) and (HeaderQLaporanPerbaikan.IsNull) and (HeaderQStoring.IsNull) then
  begin
      cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
      cxDBVerticalGrid1Storing.Visible:=false;
      Panel2.Visible:=false;
  end
  else  if not HeaderQStoring.IsNull then
  begin
      cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
      cxDBVerticalGrid1Storing.Visible:=true;
      Panel2.Visible:=false;
  end
  else
  if not HeaderQLaporanPerbaikan.IsNull then
  begin
      cxDBVerticalGrid1LaporanPerbaikan.Visible:=true;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
      cxDBVerticalGrid1Storing.Visible:=false;

      Panel2.Visible:=true;
      LblBaik.Visible:=true;
      LblRawat.Visible:=false ;

      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select j.nama from laporanperbaikan lp, detailperbaikanjenis dp, jenisperbaikan j where lp.kode=dp.kodeperbaikan and j.kode=dp.jenisperbaikan and lp.kode='+QuotedStr(HeaderQLaporanPerbaikan.AsString));
      ListBox1.Items.Clear;
      SembarangQ.Open;
      while not SembarangQ.Eof do
      begin
        ListBox1.Items.Add(SembarangQ.fieldbyname('nama').AsString);
          SembarangQ.Next;
      end;
      SembarangQ.Close;

      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select pp.armada, a.platno from laporanperbaikan lp, permintaanperbaikan pp, armada a where lp.pp=pp.kode and pp.armada=a.kode and lp.kode='+QuotedStr(HeaderQLaporanPerbaikan.AsString));
      SembarangQ.Open;
      LblPlat.Caption:=SembarangQ.fieldbyname('platno').AsString ;
      HeaderQArmada.AsString:=SembarangQ.fieldbyname('armada').AsString ;
      SembarangQ.Close;

  end else
  if not HeaderQLaporanPerawatan.IsNull then
  begin
      cxDBVerticalGrid1Storing.Visible:=false;
     cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=true;

      LblBaik.Visible:=false;
      LblRawat.Visible:=true;
      Panel2.Visible:=true;

      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select s.tipeperawatan from laporanperawatan lp, detailperawatanjenis dp, standarperawatan s where lp.kode=dp.kodeperawatan and s.kode=dp.jenisperawatan and lp.kode='+QuotedStr(HeaderQLaporanPerawatan.AsString));
      ListBox1.Items.Clear;

      SembarangQ.Open;
      while not SembarangQ.Eof do
      begin
        ListBox1.Items.Add(SembarangQ.fieldbyname('tipeperawatan').AsString);
        SembarangQ.Next;
      end;
      SembarangQ.Close;

      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select a.kode,a.platno from laporanperawatan lp, armada a where lp.armada=a.kode and lp.kode='+QuotedStr(HeaderQLaporanPerawatan.AsString));
      SembarangQ.Open;
      LblPlat.Caption:=SembarangQ.fieldbyname('platno').AsString ;
      HeaderQArmada.AsString:=SembarangQ.fieldbyname('kode').AsString ;
      SembarangQ.Close;
  end;

  MenuUtamaFm.param_sql_detail:=CobaQ.SQL.Text;


  Panel1.Enabled:=false;
  cxGridDBTableView1JumlahBeli.Visible:=true;
  cxGridDBTableView1JumlahDiminta.Visible:=true;

  if HeaderQStatus.AsString='NEW BB' THEN
  begin
      BtnSave.Enabled:=true;
      BtnKepalaTeknik.Enabled:=true;
      BtnSetuju.Enabled:=false;
  end else
  if HeaderQStatus.AsString='APPROVED' THEN
  begin
      BtnSave.Enabled:=False;
      BtnKepalaTeknik.Enabled:=true;
      BtnSetuju.Enabled:=true;
  end else
  if HeaderQStatus.AsString='ONPROCESS' THEN
  begin
      BtnSave.Enabled:=False;
      BtnKepalaTeknik.Enabled:=False;
      BtnSetuju.Enabled:=true;
     // ShowMessage(HeaderQStatus.AsString);
  end else
  if HeaderQStatus.AsString='FINISHED' THEN
  begin
      BtnSave.Enabled:=False;
      BtnKepalaTeknik.Enabled:=False;
      BtnSetuju.Enabled:=False;
      //ShowMessage(HeaderQStatus.AsString);
  end;

end;

procedure TBonBarangFm.KodeEditPropertiesChange(Sender: TObject);
begin
  Panel1.Enabled:=true;
  BtnSetuju.Enabled:=false;
  {if user='admin' then
  begin
      cxGridDBTableView1JumlahBeli.Visible:=false;
      cxGridDBTableView1JumlahDiminta.Visible:=true;
  end
  else
  begin
      cxGridDBTableView1JumlahBeli.Visible:=true;
      cxGridDBTableView1JumlahDiminta.Visible:=false;
  end;}

  Panel1.Visible:=true;
  ListBox1.Items.Clear;
  LblPlat.Caption:='';

  //KodeEdit.Text:='Insert Baru';

  CobaQ.Close;
  CobaQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;

  LblGrandTotal.Caption:='0';

  cxGridDBTableView1.NewItemRow.Visible:=true;
  cxGrid3.Enabled:=true;
  cari:=false;
end;

procedure TBonBarangFm.cxGridDBTableView1namaPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var totminta,totkasih:integer;
begin
if RbtStok.Checked then
  BrowseNameFm.tipe:='bb'
  else
  BrowseNameFm.tipe:='';

BarangQ.Close;
BarangQ.Open;
BarangQ.Refresh;
BarangQ.Close;
BarangQ.Open;

BrowseNameFm.ShowModal;
CobaQ.Edit;
if CobaQUrgent.AsBoolean<>true then
    CobaQUrgent.AsBoolean:=false;

CobaQKodeBarang.AsString:=MenuUtamaFm.param_kode;
CobaQnama.AsString:=MenuUtamaFm.param_nama;
CobaQjumlah.AsInteger:=MenuUtamaFm.stok;
CobaQsatuan.AsString:=BrowseNameFm.satuan;

//CARI HARGA TERAKHIR DARI KODEBARANG ITU ===================================
  CobaQharga.AsInteger:=0;
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select hargalast from daftarbeli db where barang=' + QuotedStr(MenuUtamaFm.param_kode) + ' and kode=(select max(kode) from daftarbeli where barang=db.barang)');
  SembarangQ.Open;
    CobaQharga.AsInteger:=SembarangQ.fieldbyname('hargalast').AsInteger;
  SembarangQ.Close;

//CARI JUMLAH PENDINK GUDANG DARI KODEBARANG ITU ===================================
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select sum(jumlahdiminta) as pendink_gudang from detailbonbarang db where db.statusminta='+QuotedStr('ONPROCESS')+' and db.kodebarang=' + QuotedStr(MenuUtamaFm.param_kode));
  SembarangQ.Open;

    totminta:=0;
    totminta:=SembarangQ.fieldbyname('pendink_gudang').AsInteger;
  SembarangQ.Close;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select sum(jumlahkeluar) as totalkasih from bonkeluarbarang bkb, detailbonbarang dbb,detailbkb dbkb where bkb.kode=dbkb.kodebkb and dbkb.barang=' + QuotedStr(MenuUtamaFm.param_kode));
  SembarangQ.SQL.Add(' and dbb.kodebarang=dbkb.barang and dbb.kodebonbarang=bkb.bonbarang and dbb.statusminta='+QuotedStr('ONPROCESS'));
  SembarangQ.Open;
    totkasih:=0;
    totkasih:=SembarangQ.fieldbyname('totalkasih').AsInteger;
  SembarangQ.Close;
  CobaQpendink_gudang.AsInteger:=totminta-totkasih;

//CARI JUMLAH PENDINK DAFTAR BELI DARI KODEBARANG ITU ===================================
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('SELECT sum(jumlahbeli) as pendink_db from daftarbeli where (status='+QuotedStr('NEW DB')+' or status='+QuotedStr('ONPROCESS')+') and barang='+ QuotedStr(MenuUtamaFm.param_kode));
  SembarangQ.Open;
    CobaQpendink_db.AsInteger:=SembarangQ.fieldbyname('pendink_db').AsInteger;
  SembarangQ.Close;
//==========================================================================

//CARI JUMLAH PENDINK PO DARI KODEBARANG ITU ===================================
  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('SELECT sum(jumlahbeli) as pendink_po from detailpo dpo, daftarbeli db where dpo.kodedaftarbeli=db.kode and status='+QuotedStr('PO')+' and barang='+ QuotedStr(MenuUtamaFm.param_kode));
  SembarangQ.Open;
    CobaQpendink_po.AsInteger:=SembarangQ.fieldbyname('pendink_po').AsInteger;
  SembarangQ.Close;
//==========================================================================

CobaQJumlahDiminta.AsInteger:=0;
CobaQJumlahBeli.AsInteger:=0;

end;

procedure TBonBarangFm.BtnSaveClick(Sender: TObject);
var kodebon:String;
i,tambah:integer;
tdkcukup:boolean;
ada,lengkap:boolean;
begin
save_klik:=true;
lengkap:=true;

if HeaderQPeminta.IsNull then
  lengkap:=false;
if HeaderQPenyetuju.IsNull then
  lengkap:=false;

if lengkap=true then
begin
      if (KodeEdit.Text='Insert Baru') or (KodeEdit.Text='') then
      begin
        try
          KodeQ.Open;
          if KodeQ.IsEmpty then
          begin
            HeaderQKode.AsString:=FormatFloat('0000000000',1);
          end
          else
          begin
            HeaderQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+1);
          end;
          kodebon:=HeaderQKode.AsString;
          KodeQ.Close;

          HeaderQ.Edit;
          if HeaderQStatus.AsString<>'ONPROCESS' then
          begin
            if RbtStok.Checked then
                HeaderQStatus.AsString:='ONPROCESS'
            ELSE
                HeaderQStatus.AsString:='NEW BB';
          end;
              
          HeaderQCreateDate.AsDateTime:= Date;
          HeaderQPenerima.AsString:='';
          HeaderQ.Post;

            except
              //on E : Exception do
              //ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
              ShowMessage('Pencarian kode baru gagal, silahkan coba kembali');
              KodeEditPropertiesButtonClick(sender,1);
        end
      end;

       CobaQ.First;
        while not CobaQ.Eof do
        begin
           CobaQ.Edit;
           CobaQKodeBonBarang.AsString:=HeaderQKode.AsString;
           
           if CobaQJumlahDiminta.AsInteger>0 then
             CobaQStatusMinta.AsString:='ONPROCESS';

           if CobaQJumlahBeli.AsInteger>0 then
             CobaQStatusBeli.AsString:='ONPROCESS';

           CobaQ.Post;
          CobaQ.Next;
        end;//===========================================================

      HeaderQ.Edit;
      HeaderQTglEntry.AsDateTime:= Date;
      HeaderQ.Post;

      MenuUtamaFm.DataBase1.StartTransaction;
      try
        HeaderQ.ApplyUpdates;
      

        MenuUtamaFm.DataBase1.Commit;
        HeaderQ.CommitUpdates;

        MenuUtamaFm.DataBase1.StartTransaction;
        try
          CobaQ.ApplyUpdates;
          MenuUtamaFm.DataBase1.Commit;
          CobaQ.CommitUpdates;


         if (KodeEdit.Text='Insert Baru') or (KodeEdit.Text='') then
          begin
              CobaQ.First;
              while not CobaQ.Eof do
              begin
                  if (HeaderQStatus.AsString='ONPROCESS') and (CobaQJumlahBeli.AsInteger>0) THEN
                  BEGIN
                        //insert daftar beli ==================================================
                          DaftarBeliQ.Close;
                           DaftarBeliQ.Open;
                           DaftarBeliQ.Insert;
                           DaftarBeliQ.Edit;

                            KodedbQ.Open;
                            if KodedbQ.IsEmpty then
                            begin
                              DaftarBeliQKode.AsString:=FormatFloat('0000000000',1);
                            end
                            else
                            begin

                              DaftarBeliQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodedbQkode.AsString)+1);
                            end;
                            KodedbQ.Close;

                            DaftarBeliQBarang.AsString:=CobaQKodeBarang.AsString;
                            DaftarBeliQBonBarang.AsString:=CobaQKodeBonBarang.AsString;
                            DaftarBeliQJumlahBeli.AsInteger:=CobaQJumlahBeli.AsInteger;
                            DaftarBeliQGrandTotal.AsInteger:=0;
                            DaftarBeliQStatus.AsString:='NEW DB';

                          SembarangQ.SQL.Clear;
                          SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString));
                          SembarangQ.ExecSQL;
                          SembarangQ.Open;
                          DaftarBeliQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
                          SembarangQ.Close;

                          SembarangQ.SQL.Clear;
                          SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString));
                          SembarangQ.ExecSQL;
                          SembarangQ.Open;
                          DaftarBeliQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
                          SembarangQ.Close;

                          SembarangQ.SQL.Clear;
                          SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString)+'  and HargaSatuan is not null  order by kode desc');
                          SembarangQ.ExecSQL;
                          SembarangQ.Open;
                          try
                            tambah:=SembarangQ.fieldbyname('hargasatuan').AsInteger;
                            DaftarBeliQLastSupplier.AsString:=SembarangQ.fieldbyname('supplier').AsString;
                          except
                            tambah:=0;
                          end;
                          DaftarBeliQHargaLast.AsInteger:=tambah;
                          if DaftarBeliQLastSupplier.AsString='' then
                            DaftarBeliQLastSupplier.AsString:='0000000000';

                          DaftarBeliQSupplier.AsString:='0000000000';
                          SembarangQ.Close;

                           DaftarBeliQ.Post;

                           MenuUtamaFm.DataBase1.StartTransaction;
                              try
                                DaftarBeliQ.ApplyUpdates;
                                MenuUtamaFm.DataBase1.Commit;
                                DaftarBeliQ.CommitUpdates;
                              except
                                ShowMessage('insert daftar beli Gagal, silahkan coba kembali');
                                 MenuUtamaFm.DataBase1.Rollback;
                                 DaftarBeliQ.RollbackUpdates;
                                 BarangQ.RollbackUpdates;
                                 HeaderQ.RollbackUpdates;
                                 CobaQ.RollbackUpdates;
                                 KodeEditPropertiesButtonClick(sender,1);

                                //on E : Exception do
                                  //ShowMessage(E.ClassName+'insert daftar beli Gagal, with message : '+E.Message);

                              end;
                          DaftarBeliQ.Close;
                  //=================================================
                  END;
              CobaQ.Next;
              end;
          end
          ELSE
         begin

            CobaQ.First;
              while not CobaQ.Eof do
              begin
                  if ((CobaQJumlahBeli.AsInteger>0) or (CobaQStatusBeli.AsString<>'')) and (HeaderQStatus.AsString='ONPROCESS') then
                  begin
                      SembarangQ.SQL.Clear;
                      SembarangQ.SQL.Add('select * from daftarbeli where bonbarang='+QuotedStr(CobaQKodeBonBarang.AsString)+' and barang='+QuotedStr(CobaQKodeBarang.AsString));
                      SembarangQ.ExecSQL;
                      //ShowMessage(SembarangQ.SQL.Text);
                      SembarangQ.Open;


                      ada:=false;
                      while not  SembarangQ.Eof do
                      begin
                         ada:=true;
                         SembarangQ.Next;
                      end;
                      SembarangQ.Close;


                      if ada=true then
                      begin
                         SembarangQ.SQL.Clear;
                        SembarangQ.SQL.Add('update daftarbeli set jumlahbeli='+IntToStr(CobaQJumlahBeli.AsInteger)+' where bonbarang='+QuotedStr(CobaQKodeBonBarang.AsString)+' and barang='+QuotedStr(CobaQKodeBarang.AsString));

                        SembarangQ.ExecSQL;
                        SembarangQ.Close;
                      end
                      else
                      begin
                          //insert daftar beli ==================================================
                                  DaftarBeliQ.Close;
                                   DaftarBeliQ.Open;
                                   DaftarBeliQ.Insert;
                                   DaftarBeliQ.Edit;

                                    KodedbQ.Open;
                                    if KodedbQ.IsEmpty then
                                    begin
                                      DaftarBeliQKode.AsString:=FormatFloat('0000000000',1);
                                    end
                                    else
                                    begin

                                      DaftarBeliQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodedbQkode.AsString)+1);
                                    end;
                                    KodedbQ.Close;

                                    DaftarBeliQBarang.AsString:=CobaQKodeBarang.AsString;
                                    DaftarBeliQBonBarang.AsString:=CobaQKodeBonBarang.AsString;
                                    DaftarBeliQJumlahBeli.AsInteger:=CobaQJumlahBeli.AsInteger;
                                    DaftarBeliQGrandTotal.AsInteger:=0;
                                    DaftarBeliQStatus.AsString:='NEW DB';

                                  SembarangQ.SQL.Clear;
                                  SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString));
                                  SembarangQ.ExecSQL;
                                  SembarangQ.Open;
                                  DaftarBeliQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
                                  SembarangQ.Close;

                                  SembarangQ.SQL.Clear;
                                  SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString));
                                  SembarangQ.ExecSQL;
                                  SembarangQ.Open;
                                  DaftarBeliQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
                                  SembarangQ.Close;

                                  SembarangQ.SQL.Clear;
                                  SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString)+'  and HargaSatuan is not null  order by kode desc');
                                  SembarangQ.ExecSQL;
                                  SembarangQ.Open;
                                  try
                                    tambah:=SembarangQ.fieldbyname('hargasatuan').AsInteger;
                                    DaftarBeliQLastSupplier.AsString:=SembarangQ.fieldbyname('supplier').AsString;
                                  except
                                    tambah:=0;
                                  end;
                                  DaftarBeliQHargaLast.AsInteger:=tambah;
                                  if DaftarBeliQLastSupplier.AsString='' then
                                    DaftarBeliQLastSupplier.AsString:='0000000000';

                                  DaftarBeliQSupplier.AsString:='0000000000';
                                  SembarangQ.Close;

                                   DaftarBeliQ.Post;

                                   MenuUtamaFm.DataBase1.StartTransaction;
                                      try
                                        DaftarBeliQ.ApplyUpdates;
                                        MenuUtamaFm.DataBase1.Commit;
                                        DaftarBeliQ.CommitUpdates;
                                      except
                                        ShowMessage('insert daftar beli Gagal, silahkan coba kembali');
                                        MenuUtamaFm.DataBase1.Rollback;
                                        DaftarBeliQ.RollbackUpdates;
                                        BarangQ.RollbackUpdates;
                                        HeaderQ.RollbackUpdates;
                                        CobaQ.RollbackUpdates;
                                        KodeEditPropertiesButtonClick(sender,1);

//                                        on E : Exception do
  //                                        ShowMessage(E.ClassName+'insert daftar beli Gagal, with message : '+E.Message);

                                      end;
                                  DaftarBeliQ.Close;
                          //====================================================================
                          end;
                    end;
                    CobaQ.Next;
              end;
         end;

          if MessageDlg('Bon Barang dengan kode '+ HeaderQKode.AsString +' telah disimpan. Cetak Bon Barang ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
          begin
              SembarangQ.SQL.Clear;
              SembarangQ.SQL.Add('update bonbarang set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(HeaderQKode.AsString));
              SembarangQ.ExecSQL;

              Crpe1.Refresh;
              Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonBarang.rpt';
              Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
              KodeEdit.Text:='';

              Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
              Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
              Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


              Crpe1.Execute;
              Crpe1.WindowState:= wsMaximized;
          end;

          save_klik:=false;
          cari:=false;

          save_klik:=false;
          KodeEdit.Text:='Insert Baru';
          CobaQ.Close;
          CobaQ.ParamByName('kodebon').AsString := KodeEdit.Text;
          CobaQ.ExecSQL;
          CobaQ.Open;
          CobaQ.Append;

          HeaderQ.Close;
          HeaderQ.ParamByName('kodebon').AsString := KodeEdit.Text;
          HeaderQ.ExecSQL;
          HeaderQ.Open;
          HeaderQ.Append;

          KodeEditPropertiesButtonClick(sender,1);

        except
//            on E : Exception do
  //            ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
          ShowMessage('Penyimpanan detail Gagal, silahkan coba kembali');
          MenuUtamaFm.DataBase1.Rollback;
          HeaderQ.RollbackUpdates;
          CobaQ.RollbackUpdates;
          KodeEditPropertiesButtonClick(sender,1);

        end;

      except
            on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
{        ShowMessage('Penyimpanan header Gagal');
        MenuUtamaFm.DataBase1.Rollback;
        HeaderQ.RollbackUpdates;
        KodeEditPropertiesButtonClick(sender,1);}
        
      end;

end
else
ShowMessage('Isilah data dengan lengkap !');

end;



procedure TBonBarangFm.CobaQBeforePost(DataSet: TDataSet);
begin
  CobaQKodeBonBarang.AsString:=HeaderQKode.AsString;

  if (save_klik=false) and (cari=false) then
  begin
    CobaQ.Edit;
    temp:=CobaQnama.AsString;
    CobaQKodeBonBarang.AsString:='tes';
    CobaQsubtotal.AsInteger:=0;
  end;

  if CobaQsubtotal.IsNull then
  begin
      temp:=CobaQnama.AsString;
      CobaQsubtotal.AsInteger:=0;
      save_klik:=false;
      cari:=false;
  end;

end;

procedure TBonBarangFm.cxDBVerticalGrid1LaporanPerbaikanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    MenuUtamaFm.param_sql:='select * from laporanPERBAIKAN';
    BrowseFm.ShowModal;
    HeaderQ.Edit;
    HeaderQLaporanPerbaikan.AsString:=BrowseFm.kode;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select j.nama from laporanperbaikan lp, detailperbaikanjenis dp, jenisperbaikan j where lp.kode=dp.kodeperbaikan and j.kode=dp.jenisperbaikan and lp.kode='+QuotedStr(HeaderQLaporanPerbaikan.AsString));
  ListBox1.Items.Clear;
  SembarangQ.Open;
  while not SembarangQ.Eof do
  begin
    ListBox1.Items.Add(SembarangQ.fieldbyname('nama').AsString);
      SembarangQ.Next;
  end;
  SembarangQ.Close;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select pp.armada, a.platno from laporanperbaikan lp, permintaanperbaikan pp, armada a where lp.pp=pp.kode and pp.armada=a.kode and lp.kode='+QuotedStr(HeaderQLaporanPerbaikan.AsString));
  SembarangQ.Open;
//  ShowMessage(SembarangQ.SQL.Text);
  LblPlat.Caption:=SembarangQ.fieldbyname('platno').AsString ;
  HeaderQArmada.AsString:=SembarangQ.fieldbyname('armada').AsString ;
  SembarangQ.Close;

end;

procedure TBonBarangFm.cxDBVerticalGrid1LaporanPerawatanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin

    MenuUtamaFm.param_sql:='select * from laporanperawatan';
    BrowseFm.ShowModal;
    HeaderQ.Edit;
    HeaderQLaporanPerawatan.AsString:=BrowseFm.kode;

    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select s.tipeperawatan from laporanperawatan lp, detailperawatanjenis dp, standarperawatan s where lp.kode=dp.kodeperawatan and s.kode=dp.jenisperawatan and lp.kode='+QuotedStr(HeaderQLaporanPerawatan.AsString));
    ListBox1.Items.Clear;
    SembarangQ.Open;
    while not SembarangQ.Eof do
    begin
      ListBox1.Items.Add(SembarangQ.fieldbyname('tipeperawatan').AsString);
      SembarangQ.Next;
    end;
    SembarangQ.Close;

    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select a.kode,a.platno from laporanperawatan lp, armada a where lp.armada=a.kode and lp.kode='+QuotedStr(HeaderQLaporanPerawatan.AsString));
    SembarangQ.Open;
    LblPlat.Caption:=SembarangQ.fieldbyname('platno').AsString ;
    HeaderQArmada.AsString:=SembarangQ.fieldbyname('kode').AsString ;
    SembarangQ.Close;

end;

procedure TBonBarangFm.RbtStokClick(Sender: TObject);
begin
 if RbtStok.Checked then
  begin
    cxDBVerticalGrid1Storing.Visible:=false;
    cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
    cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
    Panel2.Visible:=false;

     LblPlat.Caption:='';
      ListBox1.Items.Clear;
      HeaderQLaporanPerbaikan.Clear;
      HeaderQLaporanPerawatan.Clear;
      HeaderQArmada.AsString:='0000000000';
  end;
end;

procedure TBonBarangFm.RbtBaikanClick(Sender: TObject);
begin
   if RbtBaikan.Checked then
    begin
      cxDBVerticalGrid1Storing.Visible:=false;
      cxDBVerticalGrid1LaporanPerbaikan.Visible:=true;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
      Panel2.Visible:=true;
      LblBaik.Visible:=true;
      LblRawat.Visible:=false ;

      LblPlat.Caption:='';
      ListBox1.Items.Clear;
      HeaderQLaporanPerbaikan.Clear;
      HeaderQLaporanPerawatan.Clear;
      HeaderQArmada.AsString:='';
    end;
end;

procedure TBonBarangFm.RbtRawatClick(Sender: TObject);
begin
    if RbtRawat.Checked then
    begin
      cxDBVerticalGrid1Storing.Visible:=false;
      cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
      cxDBVerticalGrid1LaporanPerawatan.Visible:=true;
      Panel2.Visible:=true;
      LblBaik.Visible:=false;
      LblRawat.Visible:=true;

       LblPlat.Caption:='';
      ListBox1.Items.Clear;
      HeaderQLaporanPerbaikan.Clear;
      HeaderQLaporanPerawatan.Clear;
      HeaderQArmada.AsString:='';
    end;
end;

procedure TBonBarangFm.FormActivate(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.Open;
  ViewQ.Refresh;

  cxGrid3.Enabled:=false;
  cxDBVerticalGrid1.Enabled:=false;
  BtnSave.Enabled:=false;
  BtnSetuju.Enabled:=false;
  BtnKepalaTeknik.Enabled:=false;
  BtnDelete.Enabled:=false;
  cxGridDBTableView1Urgent.Visible:=false;
  cxGridDBTableView1JumlahDiminta.Options.Editing:=false;

  cxGridDBTableView1JumlahBeli.Visible:=false;
  cxGridDBTableView1Urgent.Visible:=false;
  RbtBaikan.Enabled:=false;
  RbtRawat.Enabled:=false;
  RbtStok.Enabled:=false;

//  ShowMessage(MenuUtamaFm.UserQSetMintaBonBarang.AsString);

 if MenuUtamaFm.UserQUpdateBonBarang.AsBoolean=true then
  begin
    cxGrid3.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    if (MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=false) and (MenuUtamaFm.UserQProcessBonBarang.AsBoolean=false) then
        BtnSave.Enabled:=true;
  end;

  if MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=true then
  begin
    BtnKepalaTeknik.Enabled:=true;
  end;
  if MenuUtamaFm.UserQProcessBonBarang.AsBoolean=true then
  begin
    BtnSetuju.Enabled:=true;
  end;
  
  if MenuUtamaFm.UserQPerbaikanBonBarang.AsBoolean=true then
    RbtBaikan.Enabled:=true;
  if MenuUtamaFm.UserQPerawatanBonBarang.AsBoolean=true then
    RbtRawat.Enabled:=true;
  if MenuUtamaFm.UserQTambahStokBonBarang.AsBoolean=true then
    RbtStok.Enabled:=true;
   if MenuUtamaFm.UserQSetUrgentBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1Urgent.Visible:=true;
  end;
  if MenuUtamaFm.UserQSetMintaBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahDiminta.Options.editing:=true;
  end;
  if MenuUtamaFm.UserQSetBeliBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahBeli.Visible:=true;
  end;
  
end;

procedure TBonBarangFm.BtnSetujuClick(Sender: TObject);
var lolos:boolean;
duitmax:Currency;
begin
    lolos:=true;
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select * from [user] where kode='+QuotedStr(MenuUtamaFm.UserQKode.AsString));
    SembarangQ.ExecSQL;
    SembarangQ.Open;
    while not SembarangQ.Eof do
    begin
      duitmax:= SembarangQ.FieldByName('approvalmoneybonbarang').AsCurrency;
      SembarangQ.Next;
    end;
    //ShowMessage(SembarangQ.FieldByName('approvalmoneybonbarang').AsString);

    cobaQ.First;
    while not cobaQ.eof do
    begin
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString)+'  and HargaSatuan is not null  order by kode desc');
      SembarangQ.ExecSQL;
      SembarangQ.Open;
      while not SembarangQ.Eof do
      begin
        //ShowMessage(SembarangQ.FieldByName('hargasatuan').AsString);
        if SembarangQ.FieldByName('hargasatuan').AsCurrency > duitmax then
        begin
            lolos:=false;
            ShowMessage('Maaf, Harga '+CobaQnama.AsString+' Melebihi batas');
            //CobaQ.Delete;
        end;
        SembarangQ.Next;
      end;

      cobaQ.next;
    end;

    if lolos=true then
    begin
      HeaderQ.Edit;
      if HeaderQStatus.AsString<>'FINISHED' THEN
          HeaderQStatus.AsString:='ONPROCESS';

      BtnSaveClick(Sender)
    end;


end;

procedure TBonBarangFm.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
SearchBtnClick(sender);

  cxGrid3.Enabled:=false;
  cxDBVerticalGrid1.Enabled:=false;
  BtnSave.Enabled:=false;
  BtnSetuju.Enabled:=false;
  BtnKepalaTeknik.Enabled:=false;
  BtnDelete.Enabled:=false;
  cxGridDBTableView1Urgent.Visible:=false;
  cxGridDBTableView1JumlahDiminta.Options.Editing:=false;
  cxGridDBTableView1JumlahBeli.Visible:=false;
  cxGridDBTableView1Urgent.Visible:=false;

  if MenuUtamaFm.UserQDeleteBonBarang.AsBoolean=true then
    BtnDelete.Enabled:=true;
  if MenuUtamaFm.UserQUpdateBonBarang.AsBoolean=true then
  begin
    cxGrid3.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    if (MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=false) and (MenuUtamaFm.UserQProcessBonBarang.AsBoolean=false) then
        BtnSave.Enabled:=true;
  end;

  if MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=true then
  begin
    BtnKepalaTeknik.Enabled:=true;
  end;
  if MenuUtamaFm.UserQProcessBonBarang.AsBoolean=true then
  begin
    BtnSetuju.Enabled:=true;
  end;

  if MenuUtamaFm.UserQSetUrgentBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1Urgent.Visible:=true;
  end;
  if MenuUtamaFm.UserQSetMintaBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahDiminta.Options.Editing:=true;
  end;
  if MenuUtamaFm.UserQSetBeliBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahBeli.Visible:=true;
  end;
end;

procedure TBonBarangFm.Button2Click(Sender: TObject);
begin
ViewQ.Close;
ViewQ.Open;

ViewQ.Refresh;
end;

procedure TBonBarangFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  Panel1.Enabled:=true;
BtnSave.Enabled:=true;
BtnKepalaTeknik.Enabled:=false;
BtnSetuju.Enabled:=false;
  Panel1.Visible:=true;
  ListBox1.Items.Clear;
  LblPlat.Caption:='';
  
  KodeEdit.Text:='Insert Baru';
  CobaQ.Close;
  CobaQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodebon').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Append;

  viewQ.Refresh;

  RbtBaikan.Checked:=true;

  cxGridDBTableView1.NewItemRow.Visible:=true;
  cxGrid3.Enabled:=true;
  cari:=false;

  cxGrid3.Enabled:=false;
  cxDBVerticalGrid1.Enabled:=false;
  BtnSave.Enabled:=false;
  BtnSetuju.Enabled:=false;
  BtnKepalaTeknik.Enabled:=false;
  BtnDelete.Enabled:=false;
  cxGridDBTableView1Urgent.Visible:=false;
  cxGridDBTableView1JumlahDiminta.editing:=false;
  cxGridDBTableView1JumlahBeli.Visible:=false;
  cxGridDBTableView1Urgent.Visible:=false;
  RbtBaikan.Enabled:=false;
  RbtRawat.Enabled:=false;
  RbtStok.Enabled:=false;

  if MenuUtamaFm.UserQUpdateBonBarang.AsBoolean=true then
  begin
    cxGrid3.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    if (MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=false) and (MenuUtamaFm.UserQProcessBonBarang.AsBoolean=false) then
        BtnSave.Enabled:=true;
  end;

  if MenuUtamaFm.UserQApprovalBonBarang.AsBoolean=true then
  begin
    BtnKepalaTeknik.Enabled:=true;
  end;
  if MenuUtamaFm.UserQProcessBonBarang.AsBoolean=true then
  begin
    BtnSetuju.Enabled:=true;
  end;
  
  if MenuUtamaFm.UserQPerbaikanBonBarang.AsBoolean=true then
    RbtBaikan.Enabled:=true;
  if MenuUtamaFm.UserQPerawatanBonBarang.AsBoolean=true then
    RbtRawat.Enabled:=true;
  if MenuUtamaFm.UserQTambahStokBonBarang.AsBoolean=true then
    RbtStok.Enabled:=true;
   if MenuUtamaFm.UserQSetUrgentBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1Urgent.Visible:=true;
  end;
  if MenuUtamaFm.UserQSetMintaBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahDiminta.editing:=true;
  end;
  if MenuUtamaFm.UserQSetBeliBonBarang.AsBoolean=true then
  begin
    cxGridDBTableView1JumlahBeli.Visible:=true;
  end;
  
end;

procedure TBonBarangFm.BtnKepalaTeknikClick(Sender: TObject);
var lolos:boolean;
duitmax:Currency;
begin
      lolos:=true;
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select * from [user] where kode='+QuotedStr(MenuUtamaFm.UserQKode.AsString));
    SembarangQ.ExecSQL;
    SembarangQ.Open;
    while not SembarangQ.Eof do
    begin
      duitmax:= SembarangQ.FieldByName('approvalmoneybonbarang').AsCurrency;
      SembarangQ.Next;
    end;
    //ShowMessage(SembarangQ.FieldByName('approvalmoneybonbarang').AsString);

    cobaQ.First;
    while not cobaQ.eof do
    begin
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString)+'  and HargaSatuan is not null  order by kode desc');
      SembarangQ.ExecSQL;
      SembarangQ.Open;
      while not SembarangQ.Eof do
      begin
        //ShowMessage(SembarangQ.FieldByName('hargasatuan').AsString);
        if SembarangQ.FieldByName('hargasatuan').AsCurrency > duitmax then
        begin
            lolos:=false;
            ShowMessage('Maaf, Harga '+CobaQnama.AsString+' Melebihi batas');
            //CobaQ.Delete;
        end;
        SembarangQ.Next;
      end;

      cobaQ.next;
    end;

    if lolos=true then
    begin
      HeaderQ.Edit;
      if HeaderQStatus.AsString<>'FINISHED' THEN
          HeaderQStatus.AsString:='APPROVED';

      BtnSaveClick(Sender)
    end;

 { HeaderQ.Edit;
  if HeaderQStatus.AsString<>'FINISHED' THEN
      HeaderQStatus.AsString:='APPROVED';

  BtnSaveClick(Sender)}
end;

procedure TBonBarangFm.CobaQAfterPost(DataSet: TDataSet);
var hit,tampi_jum:integer;
duitmax:Currency;
begin
if (save_klik=false) and (cari=false) then
  begin
        {SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select * from [user] where kode='+QuotedStr(MenuUtamaFm.UserQKode.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        while not SembarangQ.Eof do
        begin
          duitmax:= SembarangQ.FieldByName('approvalmoneybonbarang').AsCurrency;
          SembarangQ.Next;
        end;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(CobaQKodeBarang.AsString)+'  and HargaSatuan is not null  order by kode desc');
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        while not SembarangQ.Eof do
        begin
          //ShowMessage(CurrToStr(SembarangQ.FieldByName('hargasatuan').AsCurrency));
          if SembarangQ.FieldByName('hargasatuan').AsCurrency > duitmax then
          begin
              ShowMessage('Maaf, Harga '+CobaQnama.AsString+' Melebihi batas');
              CobaQ.Delete;
          end;
          SembarangQ.Next;
        end;    }


        hit:=0;

        CobaQ.Last;
        while not CobaQ.Bof do
        begin
          //ShowMessage(CobaQnama.AsString);
          if CobaQnama.AsString=temp then
          begin
              hit:=hit+1;
              if hit>=2 then
              begin
                //ShowMessage('yang dihapus= '+CobaQJumlahDiminta.AsString);
                hit:=0;
                CobaQ.Edit;
                CobaQ.Delete;
              end
              else
              CobaQ.Prior
          end
          else
          CobaQ.Prior;
        end;
  end;  

end;

procedure TBonBarangFm.BtnDeleteClick(Sender: TObject);
begin
  if MessageDlg('Hapus Bon Barang '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try
        MenuUtamaFm.Database1.StartTransaction;
        CobaQ.First;
        while not cobaQ.eof do
        begin
          CobaQ.Delete;
        end;
        CobaQ.ApplyUpdates;

       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       MessageDlg('Bon Barang telah dihapus.',mtInformation,[mbOK],0);
       
     except
       MenuUtamaFm.Database1.Rollback;
       HeaderQ.RollbackUpdates;
       CobaQ.RollbackUpdates;

       MessageDlg('Bon Barang pernah/sedang proses bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;

     KodeEditPropertiesButtonClick(sender,1);
  end;
end;

procedure TBonBarangFm.cxDBVerticalGrid1StoringEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    MenuUtamaFm.param_sql:='select st.Kode, st.SuratJalan, a.PlatNo, sop.Nama, st.TglEntry ';
    MenuUtamaFm.param_sql:=MenuUtamaFm.param_sql+'from storing st, MasterSO so, MasterSJ sj, Armada a, Sopir sop ';
    MenuUtamaFm.param_sql:=MenuUtamaFm.param_sql+'where st.SuratJalan=sj.Kodenota and sj.NoSO=so.Kodenota ';
    MenuUtamaFm.param_sql:=MenuUtamaFm.param_sql+'and a.Kode=so.Armada and sop.Kode=sj.Sopir ';
    BrowseFm.ShowModal;
    HeaderQ.Edit;
    HeaderQStoring.AsString:=BrowseFm.kode;
    Panel2.Visible:=false;
end;

procedure TBonBarangFm.RbtStoringClick(Sender: TObject);
begin
 if RbtStoring.Checked then
  begin
    cxDBVerticalGrid1Storing.Visible:=true;
    cxDBVerticalGrid1LaporanPerbaikan.Visible:=false;
    cxDBVerticalGrid1LaporanPerawatan.Visible:=false;
    Panel2.Visible:=false;

     LblPlat.Caption:='';
      ListBox1.Items.Clear;
      HeaderQLaporanPerbaikan.Clear;
      HeaderQLaporanPerawatan.Clear;
      HeaderQArmada.AsString:='0000000000';
  end;
end;

end.
