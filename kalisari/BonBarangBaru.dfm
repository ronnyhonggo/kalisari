object BonBarangBaruFm: TBonBarangBaruFm
  Left = 202
  Top = 52
  Width = 1057
  Height = 710
  Caption = 'BonBarangBaruFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 313
    Height = 439
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 311
      Height = 88
      Align = alTop
      TabOrder = 0
      object lbl1: TLabel
        Left = 8
        Top = 33
        Width = 24
        Height = 13
        Caption = 'Kode'
      end
      object KodeEdit: TcxButtonEdit
        Left = 40
        Top = 33
        Align = alCustom
        Properties.Buttons = <
          item
            Caption = '+'
            Default = True
            Kind = bkText
          end>
        Properties.OnButtonClick = KodeEditPropertiesButtonClick
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebs3D
        Style.HotTrack = False
        Style.ButtonStyle = bts3D
        TabOrder = 0
        OnEnter = KodeEditEnter
        OnExit = KodeEditExit
        OnKeyDown = KodeEditKeyDown
        Width = 186
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 89
      Width = 311
      Height = 88
      Align = alTop
      Color = clBtnHighlight
      TabOrder = 1
      object RbtStok: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Tambah Stok'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = RbtStokClick
      end
      object RbtSPK: TRadioButton
        Left = 168
        Top = 8
        Width = 113
        Height = 17
        Caption = 'SPK'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        TabStop = True
        OnClick = RbtSPKClick
      end
      object RbtStoring: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'Verpal'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = RbtStoringClick
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 162
      Width = 311
      Height = 276
      Align = alBottom
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 143
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 2
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Peminta: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PemintaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Peminta'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NamaPeminta: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPeminta'
        Properties.Options.Editing = False
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPeminta: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPeminta'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Penyetuju: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penyetuju'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1NamaPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenyetuju'
        Properties.Options.Editing = False
        ID = 4
        ParentID = 3
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPenyetuju'
        Properties.Options.Editing = False
        ID = 5
        ParentID = 3
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Penerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penerima'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 6
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1LaporanPerbaikan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'LaporanPerbaikan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 7
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1LaporanPerawatan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'LaporanPerawatan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 8
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1Storing: TcxDBEditorRow
        Properties.Caption = 'Verpal'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1StoringEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Storing'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 9
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1SPK: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SPKEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'SPK'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 10
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1NoPlat: TcxDBEditorRow
        Properties.Caption = 'Plat Nomor'
        Properties.DataBinding.FieldName = 'NoPlat'
        Properties.Options.Editing = False
        ID = 11
        ParentID = 10
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NoBody: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBody'
        Properties.Options.Editing = False
        ID = 12
        ParentID = 10
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Status: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Status'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 13
        ParentID = -1
        Index = 7
        Version = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 439
    Width = 1041
    Height = 214
    Align = alBottom
    Caption = 'Panel2'
    TabOrder = 1
    object cxGrid3: TcxGrid
      Left = 1
      Top = 1
      Width = 1039
      Height = 151
      Align = alClient
      TabOrder = 3
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = ViewMasterDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGridDBColumn1: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid2DBTableView1NamaBarangBekas: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarangBekas'
          Options.SortByDisplayText = isbtOn
          Width = 124
        end
        object cxGrid2DBTableView1NamaPenerima: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenerima'
          Options.SortByDisplayText = isbtOn
          Width = 135
        end
        object cxGrid2DBTableView1JabatanPenerima: TcxGridDBColumn
          DataBinding.FieldName = 'JabatanPenerima'
          Width = 154
        end
        object cxGridDBColumn2: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenyetuju'
          Options.SortByDisplayText = isbtOn
          Width = 155
        end
        object cxGrid2DBTableView1JabatanPenyetuju: TcxGridDBColumn
          DataBinding.FieldName = 'JabatanPenyetuju'
          Width = 112
        end
        object cxGrid2DBTableView1Saldo: TcxGridDBColumn
          DataBinding.FieldName = 'Saldo'
        end
        object cxGrid2DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 75
        end
        object cxGrid2DBTableView1StatusApproved: TcxGridDBColumn
          DataBinding.FieldName = 'StatusApproved'
          Width = 103
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1039
      Height = 151
      Align = alClient
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        OnCellClick = cxGrid2DBTableView1CellClick
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid2DBTableView1TglEntry: TcxGridDBColumn
          DataBinding.FieldName = 'TglEntry'
          Width = 97
        end
        object cxGrid2DBTableView1NamaPeminta: TcxGridDBColumn
          Caption = 'Peminta'
          DataBinding.FieldName = 'NamaPeminta'
          Width = 125
        end
        object cxGrid2DBTableView1NamaPenyetuju: TcxGridDBColumn
          Caption = 'Penyetuju'
          DataBinding.FieldName = 'NamaPenyetuju'
          Width = 102
        end
        object cxGrid2DBTableView1LaporanPerbaikan: TcxGridDBColumn
          DataBinding.FieldName = 'LaporanPerbaikan'
          Width = 119
        end
        object cxGrid2DBTableView1LaporanPerawatan: TcxGridDBColumn
          DataBinding.FieldName = 'LaporanPerawatan'
          Width = 106
        end
        object cxGrid2DBTableView1Storing: TcxGridDBColumn
          DataBinding.FieldName = 'Storing'
        end
        object cxGrid2DBTableView1SPK: TcxGridDBColumn
          DataBinding.FieldName = 'SPK'
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 98
        end
        object cxGrid2DBTableView1Armada: TcxGridDBColumn
          DataBinding.FieldName = 'Armada'
          Width = 70
        end
        object cxGrid2DBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Width = 106
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object ExitBtn: TButton
      Left = 823
      Top = 165
      Width = 121
      Height = 41
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      OnClick = ExitBtnClick
    end
    object Panel4: TPanel
      Left = 1
      Top = 152
      Width = 1039
      Height = 61
      Align = alBottom
      Caption = 'Panel4'
      TabOrder = 2
      object BtnSave: TButton
        Left = 8
        Top = 8
        Width = 129
        Height = 45
        Caption = 'Simpan Permintaan Bon Barang'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        WordWrap = True
        OnClick = BtnSaveClick
      end
      object BtnKepalaTeknik: TButton
        Left = 144
        Top = 8
        Width = 121
        Height = 45
        Caption = 'Approve Kepala Teknik'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        WordWrap = True
        OnClick = BtnKepalaTeknikClick
      end
      object BtnSetuju: TBitBtn
        Left = 272
        Top = 8
        Width = 153
        Height = 45
        Caption = 'Verifikasi Gudang'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = BtnSetujuClick
        Kind = bkAll
      end
      object BtnDelete: TButton
        Left = 432
        Top = 8
        Width = 137
        Height = 45
        Hint = 'tes'
        Caption = 'Delete Bon Barang'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = BtnDeleteClick
      end
      object Button1: TButton
        Left = 959
        Top = -20
        Width = 121
        Height = 33
        Caption = 'Save and Print'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Visible = False
      end
      object Button2: TButton
        Left = 941
        Top = -24
        Width = 137
        Height = 45
        Caption = 'Refresh'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 741
        Top = 8
        Width = 121
        Height = 45
        Caption = 'Exit'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        Visible = False
        OnClick = Button3Click
      end
      object cxGroupBox1: TcxGroupBox
        Left = 741
        Top = 1
        Align = alRight
        Alignment = alTopRight
        Caption = 'Tanggal History'
        TabOrder = 7
        Height = 59
        Width = 297
        object cxDateEdit1: TcxDateEdit
          Left = 16
          Top = 16
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.OnChange = cxDateEdit1PropertiesChange
          TabOrder = 0
          Width = 121
        end
        object cxDateEdit2: TcxDateEdit
          Left = 168
          Top = 16
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.OnChange = cxDateEdit2PropertiesChange
          TabOrder = 1
          Width = 121
        end
        object cxLabel7: TcxLabel
          Left = 142
          Top = 18
          Caption = 's/d'
        end
      end
      object RadioGroup1: TRadioGroup
        Left = 576
        Top = 2
        Width = 161
        Height = 57
        ItemIndex = 0
        Items.Strings = (
          'BonBarang'
          'BarangBekas')
        TabOrder = 8
        OnClick = RadioGroup1Click
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 653
    Width = 1041
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel8: TPanel
    Left = 313
    Top = 0
    Width = 728
    Height = 439
    Align = alClient
    Caption = 'Panel8'
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 726
      Height = 437
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DetailBonBarangSource
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Urgent: TcxGridDBColumn
          DataBinding.FieldName = 'Urgent'
        end
        object cxGrid1DBTableView1KodeBarang: TcxGridDBColumn
          DataBinding.FieldName = 'KodeBarang'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KodeBarangPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Options.Editing = False
          Width = 110
        end
        object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
          Caption = 'Stok'
          DataBinding.FieldName = 'Jumlah'
          Options.Editing = False
        end
        object cxGrid1DBTableView1JumlahDiminta: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahDiminta'
          Width = 75
        end
        object cxGrid1DBTableView1Diminta: TcxGridDBColumn
          Caption = 'TotalDiminta'
          DataBinding.FieldName = 'Diminta'
          Options.Editing = False
        end
        object cxGrid1DBTableView1PendingBeli: TcxGridDBColumn
          DataBinding.FieldName = 'PendingBeli'
          Options.Editing = False
        end
        object cxGrid1DBTableView1PendingPO: TcxGridDBColumn
          DataBinding.FieldName = 'PendingPO'
          Options.Editing = False
        end
        object cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahBeli'
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 98
        end
        object cxGrid1DBTableView1CekPeminta: TcxGridDBColumn
          DataBinding.FieldName = 'CekPeminta'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1CekPemintaPropertiesButtonClick
          Width = 22
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BonBarang ')
    UpdateObject = MasterUpdate
    Left = 344
    Top = 160
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPeminta: TStringField
      FieldName = 'Peminta'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object MasterQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object MasterQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object MasterQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object MasterQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      LookupCache = True
      Size = 10
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object MasterQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Peminta, Penyetuju, Penerima, LaporanPerbaikan, Lap' +
        'oranPerawatan, Storing, SPK, Status, CreateDate, CreateBy, Opera' +
        'tor, TglEntry, Armada, TglCetak'
      'from BonBarang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update BonBarang'
      'set'
      '  Kode = :Kode,'
      '  Peminta = :Peminta,'
      '  Penyetuju = :Penyetuju,'
      '  Penerima = :Penerima,'
      '  LaporanPerbaikan = :LaporanPerbaikan,'
      '  LaporanPerawatan = :LaporanPerawatan,'
      '  Storing = :Storing,'
      '  SPK = :SPK,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Armada = :Armada,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into BonBarang'
      
        '  (Kode, Peminta, Penyetuju, Penerima, LaporanPerbaikan, Laporan' +
        'Perawatan, Storing, SPK, Status, CreateDate, CreateBy, Operator,' +
        ' TglEntry, Armada, TglCetak)'
      'values'
      
        '  (:Kode, :Peminta, :Penyetuju, :Penerima, :LaporanPerbaikan, :L' +
        'aporanPerawatan, :Storing, :SPK, :Status, :CreateDate, :CreateBy' +
        ', :Operator, :TglEntry, :Armada, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from BonBarang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 408
    Top = 168
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 376
    Top = 160
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from BonBarang order by kode desc')
    Left = 176
    Top = 8
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object viewBonBarang: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select * from BonBarang where tglentry>=:text1 and tglentry<=:te' +
        'xt2 order by CreateDate desc')
    Left = 464
    Top = 456
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewBonBarangKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewBonBarangPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object viewBonBarangPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object viewBonBarangPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object viewBonBarangLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object viewBonBarangLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object viewBonBarangStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object viewBonBarangSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object viewBonBarangStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object viewBonBarangCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewBonBarangCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewBonBarangOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewBonBarangTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewBonBarangArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewBonBarangNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object viewBonBarangTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewBonBarangNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object viewBonBarangNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object viewBonBarangNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
  end
  object DataSource2: TDataSource
    DataSet = viewBonBarang
    Left = 528
    Top = 456
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai')
    Left = 552
    Top = 48
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object StoringQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Storing')
    Left = 376
    Top = 344
    object StoringQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object StoringQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Size = 10
    end
    object StoringQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object StoringQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object StoringQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Visible = False
      Size = 10
    end
    object StoringQNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object StoringQKategoriRute: TStringField
      FieldName = 'KategoriRute'
      Size = 50
    end
    object StoringQBiaya: TCurrencyField
      FieldName = 'Biaya'
      Required = True
    end
    object StoringQPICJemput: TStringField
      FieldName = 'PICJemput'
      Required = True
      Visible = False
      Size = 10
    end
    object StoringQNamaPICJemput: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPICJemput'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PICJemput'
      Size = 50
      Lookup = True
    end
    object StoringQJenisStoring: TMemoField
      FieldName = 'JenisStoring'
      Required = True
      BlobType = ftMemo
    end
    object StoringQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object StoringQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object StoringQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object StoringQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object StoringQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object StoringQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object SuratPerintahKerjaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from SuratPerintahKerja')
    Left = 344
    Top = 344
    object SuratPerintahKerjaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object SuratPerintahKerjaQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object SuratPerintahKerjaQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object SuratPerintahKerjaQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object SuratPerintahKerjaQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object SuratPerintahKerjaQMekanik: TStringField
      FieldName = 'Mekanik'
      Visible = False
      Size = 10
    end
    object SuratPerintahKerjaQNamaMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 50
      Lookup = True
    end
    object SuratPerintahKerjaQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object SuratPerintahKerjaQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object SuratPerintahKerjaQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object SuratPerintahKerjaQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object SuratPerintahKerjaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object SuratPerintahKerjaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SuratPerintahKerjaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SuratPerintahKerjaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SuratPerintahKerjaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object DetailBonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select dbb.KodeBonBarang,dbb.KodeBarang,cast('#39#39' as integer) as D' +
        'iminta,'
      
        'b.Jumlah,dbb.JumlahDiminta,dbb.Urgent,dbb.JumlahBeli,dbb.Keteran' +
        'gan,dbb.StatusMinta,'
      'dbb.StatusBeli,cast('#39#39' as integer) as PendingBeli,'
      
        'cast('#39#39' as integer) as PendingPO,cast ('#39#39' as varchar(1)) as CekP' +
        'eminta'
      'from DetailBonBarang dbb, barang b'
      'where b.Kode=dbb.KodeBarang and dbb.KodeBonBarang=:text')
    UpdateObject = DetailBonBarangUpdate
    Left = 344
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailBonBarangQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object DetailBonBarangQUrgent: TBooleanField
      FieldName = 'Urgent'
      Required = True
    end
    object DetailBonBarangQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
    end
    object DetailBonBarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailBonBarangQStatusMinta: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object DetailBonBarangQStatusBeli: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object DetailBonBarangQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      LookupCache = True
      Required = True
      Size = 10
    end
    object DetailBonBarangQPendingBeli: TIntegerField
      FieldName = 'PendingBeli'
    end
    object DetailBonBarangQPendingPO: TIntegerField
      FieldName = 'PendingPO'
    end
    object DetailBonBarangQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 50
      Lookup = True
    end
    object DetailBonBarangQCekPeminta: TStringField
      FieldName = 'CekPeminta'
      Size = 1
    end
    object DetailBonBarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailBonBarangQJumlahDiminta: TFloatField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object DetailBonBarangQDiminta: TIntegerField
      FieldName = 'Diminta'
    end
  end
  object DetailBonBarangSource: TDataSource
    DataSet = DetailBonBarangQ
    Left = 384
    Top = 128
  end
  object DetailBonBarangUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'
      'from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update DetailBonBarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into DetailBonBarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 416
    Top = 120
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Barang')
    Left = 464
    Top = 48
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Visible = False
      Size = 10
    end
    object BarangQNamaKategori: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKategori'
      LookupDataSet = KategoriBarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'Kategori'
      Size = 50
      Lookup = True
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object CobaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @Diminta numeric(18,4);'
      'declare @BarangKeluar numeric(18,4);'
      'declare @JumlahDiminta numeric(18,4);'
      'declare @PendingBeli int;'
      'declare @PendingPO int;'
      'declare @KodeBonBarang varchar(10);'
      'declare @JumKeluar numeric(18,4);'
      'declare @Tambah numeric(18,4);'
      
        'set @Diminta=(select sum(JumlahDiminta) from DetailBonBarang whe' +
        're KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39');'
      'set @JumKeluar=0;'
      
        'DECLARE MyCursor CURSOR for  (select KodeBonBarang from DetailBo' +
        'nBarang where KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39')  '
      'open MyCursor  '
      'Fetch Next from MyCursor  '
      'into @KodeBonBarang '
      'while @@FETCH_STATUS=0'
      'begin'
      'set @Tambah=(select sum(JumlahKeluar) from DetailBKB dbkb '
      'left join BonKeluarBarang bkb on dbkb.KodeBKB=bkb.Kode'
      'where bkb.BonBarang=@KodeBonBarang and dbkb.Barang=:text)'
      'if @Tambah is NULL'
      'begin'
      'set @Tambah=0'
      'end'
      'set @JumKeluar=@JumKeluar+@Tambah'
      ' Fetch Next from MyCursor  '
      ' into @KodeBonBarang  '
      ' end  '
      ' Close MyCursor  '
      ' Deallocate MyCursor  '
      'set @JumlahDiminta=@Diminta-@JumKeluar;'
      
        'set @PendingBeli=(select sum(JumlahBeli) from DaftarBeli where B' +
        'arang=:text and Status<>'#39'PO'#39' and Status<>'#39'CNC'#39' and Status<>'#39'FINI' +
        'SHED'#39');'
      'if @PendingBeli is NULL'
      'begin'
      'set @PendingBeli=0;'
      'end;'
      
        'set @PendingPO=(select sum(JumlahBeli) from DaftarBeli where Bar' +
        'ang=:text and Status='#39'PO'#39' and Status='#39'CNC'#39' and Status<>'#39'FINISHED' +
        #39');'
      'if @PendingPO is NULL'
      'begin'
      'set @PendingPO=0;'
      'end;'
      'select distinct Diminta=@JumlahDiminta,'
      'PendingBeli=@PendingBeli,'
      'PendingPO=@PendingPO'
      'from DetailBonBarang dbb, barang b')
    Left = 480
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CobaQPendingBeli: TIntegerField
      FieldName = 'PendingBeli'
    end
    object CobaQPendingPO: TIntegerField
      FieldName = 'PendingPO'
    end
    object CobaQDiminta: TFloatField
      FieldName = 'Diminta'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @Diminta int;'
      'declare @BarangKeluar int;'
      'declare @JumlahDiminta int;'
      
        'set @Diminta=(select sum(JumlahDiminta) from DetailBonBarang whe' +
        're KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39');'
      
        'set @BarangKeluar=(select sum(JumlahKeluar) from DetailBKB dbkb,' +
        'BonKeluarBarang bkb where dbkb.KodeBKB=bkb.Kode and dbkb.Barang=' +
        ':text);'
      'set @JumlahDiminta=@Diminta-@BarangKeluar;'
      'select dbb.KodeBonBarang,dbb.KodeBarang,Diminta=@JumlahDiminta,'
      
        'b.Jumlah,dbb.JumlahDiminta,dbb.Urgent,dbb.JumlahBeli,dbb.Keteran' +
        'gan,dbb.StatusMinta,'
      
        'dbb.StatusBeli,PendingBeli=(select sum(JumlahBeli) from DaftarBe' +
        'li where Barang=:text and Status='#39'ON PROCESS'#39' or Status='#39'NEWDB'#39')' +
        ','
      
        'PendingPO=(select sum(JumlahBeli) from DaftarBeli where Barang=:' +
        'text and Status='#39'PO'#39' or Status='#39'CNC'#39') '
      'from DetailBonBarang dbb, barang b'
      'where b.Kode=dbb.KodeBarang and dbb.KodeBonBarang=:text')
    Left = 280
    Top = 40
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StringField1: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'Diminta'
    end
    object IntegerField2: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object BooleanField1: TBooleanField
      FieldName = 'Urgent'
      Required = True
    end
    object IntegerField4: TIntegerField
      FieldName = 'JumlahBeli'
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object StringField3: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object IntegerField5: TIntegerField
      FieldName = 'PendingBeli'
    end
    object IntegerField6: TIntegerField
      FieldName = 'PendingPO'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 520
    Top = 48
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object SugestiBeliQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @KodeBarang varchar(10)'
      'declare @JumlahMinta int;'
      'declare @Stok numeric(18,4);'
      'declare @PendingPO int;'
      'declare @PendingBeli int;'
      'declare @Total numeric(18,4);'
      'declare @Diminta numeric(18,4);'
      'declare @BarangKeluar numeric(18,4);'
      'declare @JumlahDiminta numeric(18,4);'
      'set @KodeBarang=:text;'
      'set @Stok=(select Jumlah from Barang where Kode=@KodeBarang);'
      
        'set @Diminta=(select sum(JumlahDiminta) from DetailBonBarang whe' +
        're KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39');'
      
        'set @BarangKeluar=(select sum(JumlahKeluar) from DetailBKB dbkb,' +
        'BonKeluarBarang bkb where dbkb.KodeBKB=bkb.Kode and dbkb.Barang=' +
        ':text);'
      'if @BarangKeluar is NULL'
      'begin'
      'set @BarangKeluar=0;'
      'end;'
      'set @JumlahDiminta=@Diminta-@BarangKeluar;'
      
        'set @PendingBeli=(select sum(JumlahBeli) from DaftarBeli where B' +
        'arang=:text and Status<>'#39'PO'#39' and Status<>'#39'CNC'#39' and Status<>'#39'FINI' +
        'SHED'#39');'
      'if @PendingBeli is NULL'
      'begin'
      'set @PendingBeli=0;'
      'end'
      
        'set @PendingPO=(select sum(JumlahBeli) from DaftarBeli where Bar' +
        'ang=:text and Status='#39'PO'#39' and Status='#39'CNC'#39' and Status<>'#39'FINISHED' +
        #39');'
      'if @PendingPO is NULL'
      'begin'
      'set @PendingPO=0;'
      'end'
      'set @Total=@JumlahDiminta-@PendingPO-@PendingBeli-@Stok;'
      'select cast(@Total as numeric(18,4)) as Total')
    UpdateObject = UpdateSugestiBeli
    Left = 552
    Top = 384
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SugestiBeliQTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object UpdateSugestiBeli: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Sta' +
        'ndardUmur, Lokasi, ClaimNWarranty, DurasiClaimNWarranty, Kategor' +
        'i, SingleSupplier, Harga, Keterangan, CreateDate, CreateBy, Oper' +
        'ator, TglEntry'
      'from Barang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  MinimumStok = :MinimumStok,'
      '  MaximumStok = :MaximumStok,'
      '  StandardUmur = :StandardUmur,'
      '  Lokasi = :Lokasi,'
      '  ClaimNWarranty = :ClaimNWarranty,'
      '  DurasiClaimNWarranty = :DurasiClaimNWarranty,'
      '  Kategori = :Kategori,'
      '  SingleSupplier = :SingleSupplier,'
      '  Harga = :Harga,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Barang'
      
        '  (Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Standar' +
        'dUmur, Lokasi, ClaimNWarranty, DurasiClaimNWarranty, Kategori, S' +
        'ingleSupplier, Harga, Keterangan, CreateDate, CreateBy, Operator' +
        ', TglEntry)'
      'values'
      
        '  (:Kode, :Nama, :Jumlah, :Satuan, :MinimumStok, :MaximumStok, :' +
        'StandardUmur, :Lokasi, :ClaimNWarranty, :DurasiClaimNWarranty, :' +
        'Kategori, :SingleSupplier, :Harga, :Keterangan, :CreateDate, :Cr' +
        'eateBy, :Operator, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from Barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 584
    Top = 384
  end
  object ArmadaPerbaikanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select armada from PermintaanPerbaikan pp,LaporanPerbaikan lp, S' +
        'uratPerintahKerja spk where lp.Kode=spk.LaporanPerbaikan and pp.' +
        'Kode=lp.PP and spk.Kode=:text')
    Left = 224
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaPerbaikanQarmada: TStringField
      FieldName = 'armada'
      Size = 10
    end
  end
  object LaporanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select LaporanPerbaikan, LaporanPerawatan,Rebuild from SuratPeri' +
        'ntahKerja '
      'where Kode=:text')
    Left = 464
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object LaporanQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object LaporanQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object LaporanQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
  end
  object ArmadaPerawatanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select Armada from LaporanPerawatan where Kode=:text')
    Left = 224
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaPerawatanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
  object ArmadaRebuildQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select DariArmada from Rebuild where kode=:text')
    Left = 120
    Top = 104
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaRebuildQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
  end
  object KategoriBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from KategoriBarang')
    Left = 488
    Top = 48
    object KategoriBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriBarangQKategori: TStringField
      FieldName = 'Kategori'
      Required = True
      Size = 50
    end
    object KategoriBarangQMinStok: TIntegerField
      FieldName = 'MinStok'
    end
    object KategoriBarangQMaxStok: TIntegerField
      FieldName = 'MaxStok'
    end
  end
  object CekPemintaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select dbb.KodeBonBarang,dbb.KodeBarang,b.Nama,bb.Peminta,p.Nama' +
        ' as NamaPeminta, dbb.JumlahDiminta from DetailBonBarang dbb '
      'left join BonBarang bb on dbb.KodeBonBarang=bb.Kode'
      'left join Barang b on dbb.KodeBarang=b.Kode'
      'left join Pegawai p on bb.Peminta=p.Kode'
      'where dbb.StatusMinta='#39'ON PROCESS'#39' and dbb.KodeBarang=:text')
    UpdateObject = CekPemintaUs
    Left = 672
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekPemintaQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object CekPemintaQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object CekPemintaQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object CekPemintaQPeminta: TStringField
      FieldName = 'Peminta'
      Size = 10
    end
    object CekPemintaQNamaPeminta: TStringField
      FieldName = 'NamaPeminta'
      Size = 50
    end
    object CekPemintaQJumlahDiminta: TIntegerField
      FieldName = 'JumlahDiminta'
      Required = True
    end
  end
  object CekPemintaUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'
      'from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update DetailBonBarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into DetailBonBarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 704
    Top = 128
  end
  object CekIdPemintaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select distinct bb.Peminta from DetailBonBarang dbb '
      'left join BonBarang bb on dbb.KodeBonBarang=bb.Kode'
      'left join Barang b on dbb.KodeBarang=b.Kode'
      'left join Pegawai p on bb.Peminta=p.Kode'
      'where dbb.StatusMinta='#39'ON PROCESS'#39' and dbb.KodeBarang=:text')
    UpdateObject = CekIdUs
    Left = 672
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekIdPemintaQPeminta: TStringField
      FieldName = 'Peminta'
      Size = 10
    end
    object CekIdPemintaQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
  end
  object CekIdUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'
      'from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update DetailBonBarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into DetailBonBarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 704
    Top = 168
  end
  object CekJumlahQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select sum(dbb.JumlahDiminta) as Jumlah from DetailBonBarang dbb' +
        ' '
      'left join BonBarang bb on dbb.KodeBonBarang=bb.Kode'
      'left join Barang b on dbb.KodeBarang=b.Kode'
      'left join Pegawai p on bb.Peminta=p.Kode '
      'where dbb.StatusMinta='#39'ON PROCESS'#39' and dbb.KodeBarang=:teks'
      'and bb.Peminta=:teks2')
    Left = 632
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'teks'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'teks2'
        ParamType = ptInput
      end>
    object CekJumlahQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
  object SDQuery2: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Left = 760
    Top = 216
  end
  object CekDaftarBeliQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select count(*) as CekDB from DaftarBeli where BonBarang=:kode')
    Left = 696
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'kode'
        ParamType = ptInput
      end>
    object CekDaftarBeliQCekDB: TIntegerField
      FieldName = 'CekDB'
    end
  end
  object CekBKBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(*) as CekBKB from BonKeluarBarang where BonBarang=:' +
        'text ')
    Left = 744
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekBKBQCekBKB: TIntegerField
      FieldName = 'CekBKB'
    end
  end
  object DeleteDetailBonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailBonBarang where KodeBonBarang=:text')
    UpdateObject = DeleteUs
    Left = 624
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailBonBarangQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object DeleteDetailBonBarangQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DeleteDetailBonBarangQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
    end
    object DeleteDetailBonBarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DeleteDetailBonBarangQStatusMinta: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object DeleteDetailBonBarangQStatusBeli: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object DeleteDetailBonBarangQUrgent: TBooleanField
      FieldName = 'Urgent'
      Required = True
    end
    object DeleteDetailBonBarangQJumlahDiminta: TFloatField
      FieldName = 'JumlahDiminta'
      Required = True
    end
  end
  object DeleteUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'
      'from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update DetailBonBarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into DetailBonBarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 632
    Top = 296
  end
  object DaftarBonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select distinct DetailBonBarang.KodeBonBarang '
      'from DetailBonBarang where KodeBarang=:text'
      'and StatusMinta='#39'ON PROCESS'#39)
    UpdateObject = DetailBonBarangUs
    Left = 728
    Top = 336
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DaftarBonBarangQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
  end
  object DetailBonBarangUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Ket' +
        'erangan, StatusMinta, StatusBeli, Urgent'
      'from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update DetailBonBarang'
      'set'
      '  KodeBonBarang = :KodeBonBarang,'
      '  KodeBarang = :KodeBarang,'
      '  JumlahDiminta = :JumlahDiminta,'
      '  JumlahBeli = :JumlahBeli,'
      '  Keterangan = :Keterangan,'
      '  StatusMinta = :StatusMinta,'
      '  StatusBeli = :StatusBeli,'
      '  Urgent = :Urgent'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into DetailBonBarang'
      
        '  (KodeBonBarang, KodeBarang, JumlahDiminta, JumlahBeli, Keteran' +
        'gan, StatusMinta, StatusBeli, Urgent)'
      'values'
      
        '  (:KodeBonBarang, :KodeBarang, :JumlahDiminta, :JumlahBeli, :Ke' +
        'terangan, :StatusMinta, :StatusBeli, :Urgent)')
    DeleteSQL.Strings = (
      'delete from DetailBonBarang'
      'where'
      '  KodeBonBarang = :OLD_KodeBonBarang and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 768
    Top = 336
  end
  object JumlahKeluarQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select sum(dbkb.JumlahKeluar) as JumKeluar from DetailBKB dbkb'
      'left join BonKeluarBarang bkb on dbkb.KodeBKB=bkb.Kode'
      'left join BonBarang bb on bkb.BonBarang=bb.Kode'
      'left join DetailBonBarang dbb on dbb.KodeBonBarang=bb.Kode'
      'left join Barang b on dbb.KodeBarang=b.Kode'
      'left join Pegawai p on bb.Peminta=p.Kode '
      'where dbb.StatusMinta='#39'ON PROCESS'#39' and dbb.KodeBarang=:teks'
      'and bb.Peminta=:teks2')
    Left = 632
    Top = 200
    ParamData = <
      item
        DataType = ftString
        Name = 'teks'
        ParamType = ptInput
        Value = ' '
      end
      item
        DataType = ftString
        Name = 'teks2'
        ParamType = ptInput
      end>
    object JumlahKeluarQJumKeluar: TFloatField
      FieldName = 'JumKeluar'
    end
  end
  object cariDaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli'
      'where bonbarang=:text and barang=:text2')
    Left = 240
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object cariDaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object cariDaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object cariDaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object cariDaftarBeliQUrgent: TBooleanField
      FieldName = 'Urgent'
    end
    object cariDaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object cariDaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object cariDaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object cariDaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object cariDaftarBeliQTglLast: TDateTimeField
      FieldName = 'TglLast'
    end
    object cariDaftarBeliQHargaLast2: TCurrencyField
      FieldName = 'HargaLast2'
    end
    object cariDaftarBeliQHargaLast3: TCurrencyField
      FieldName = 'HargaLast3'
    end
    object cariDaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object cariDaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object cariDaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object cariDaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object cariDaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object cariDaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object cariDaftarBeliQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object cariDaftarBeliQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object cariDaftarBeliQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object cariDaftarBeliQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object cariDaftarBeliQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object cariDaftarBeliQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object cariDaftarBeliQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object cariDaftarBeliQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object cariDaftarBeliQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object cariDaftarBeliQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object cariDaftarBeliQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object cariDaftarBeliQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object cariDaftarBeliQBeli1: TIntegerField
      FieldName = 'Beli1'
    end
    object cariDaftarBeliQBeli2: TIntegerField
      FieldName = 'Beli2'
    end
    object cariDaftarBeliQBeli3: TIntegerField
      FieldName = 'Beli3'
    end
    object cariDaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
  object updDaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'update daftarbeli set jumlahbeli=:text'
      'where kode=:text2')
    Left = 272
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
  end
  object genDaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'insert into '
      'DaftarBeli '
      '(Kode,Barang,BonBarang,JumlahBeli, HargaSatuan,Supplier,'
      'CashNCarry,GrandTotal,Status,Urgent) '
      'values '
      '(:text,:text2,:text3,:text4,0,NULL,'
      #39'FALSE'#39',0,'#39'NEW DB'#39',:text5) ')
    Left = 304
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text4'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text5'
        ParamType = ptInput
      end>
  end
  object KodeDaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from DaftarBeli order by kode desc')
    Left = 336
    Top = 544
    object KodeDaftarBeliQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ViewMaster: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KeluarMasukBarangBekas'
      'where StatusApproved='#39'ON PROCESS'#39)
    Left = 816
    Top = 472
    object ViewMasterKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewMasterBarangBekas: TStringField
      FieldName = 'BarangBekas'
      Required = True
      Size = 10
    end
    object ViewMasterPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object ViewMasterPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Size = 10
    end
    object ViewMasterSaldo: TIntegerField
      FieldName = 'Saldo'
    end
    object ViewMasterKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewMasterNamaBarangBekas: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarangBekas'
      LookupDataSet = BarangBekasQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangBekas'
      Size = 50
      Lookup = True
    end
    object ViewMasterNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object ViewMasterNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object ViewMasterJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object ViewMasterJabatanPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object ViewMasterStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object ViewMasterStatusApproved: TStringField
      FieldName = 'StatusApproved'
      Size = 50
    end
  end
  object ViewMasterDs: TDataSource
    DataSet = ViewMaster
    Left = 848
    Top = 472
  end
  object BarangBekasQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from BarangBekas')
    Left = 784
    Top = 472
    object BarangBekasQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangBekasQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object BarangBekasQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object BarangBekasQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangBekasQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object BarangBekasQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangBekasQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangBekasQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangBekasQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangBekasQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangBekasQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object SDQuery3: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 432
    Top = 360
  end
  object TStokVeriQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 96
    Top = 56
  end
end
