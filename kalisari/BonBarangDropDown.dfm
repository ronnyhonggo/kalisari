�
 TBONBARANGDROPDOWNFM 0�`  TPF0TBonBarangDropDownFmBonBarangDropDownFmLeft� Top� Width�HeightLCaptionBonBarangDropDownColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TPanelpnl1Left Top�Width}Height)AlignalBottomTabOrder   TcxGridcxGrid1Left Top Width}Height�AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TcxGridDBTableViewcxGrid1DBTableView1Navigator.Buttons.First.Visible#Navigator.Buttons.PriorPage.VisibleNavigator.Buttons.Prior.VisibleNavigator.Buttons.Next.Visible"Navigator.Buttons.NextPage.VisibleNavigator.Buttons.Last.Visible Navigator.Buttons.Append.VisibleNavigator.Buttons.Edit.Visible	Navigator.Buttons.Post.Visible	&Navigator.Buttons.SaveBookmark.Visible&Navigator.Buttons.GotoBookmark.Visible Navigator.Buttons.Filter.VisibleNavigator.Visible	OnCellDblClickcxGrid1DBTableView1CellDblClickOnFocusedRecordChanged'cxGrid1DBTableView1FocusedRecordChangedDataController.DataSourceDataSource1/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups OptionsBehavior.FocusCellOnTab	OptionsBehavior.IncSearch	)OptionsBehavior.ExpandMasterRowOnDblClickOptionsData.EditingOptionsView.GroupByBoxPreview.Visible	 TcxGridDBColumncxGrid1DBTableView1KodeDataBinding.FieldNameKodeWidthk  TcxGridDBColumncxGrid1DBTableView1CreateDateCaptionTglBonBarangDataBinding.FieldName
CreateDate  TcxGridDBColumncxGrid1DBTableView1TglSPKDataBinding.FieldNameTglSPK  TcxGridDBColumncxGrid1DBTableView1SPKDataBinding.FieldNameSPK  TcxGridDBColumn%cxGrid1DBTableView1NoBodyArmadaVerpalDataBinding.FieldNameNoBodyArmadaVerpalOptions.SortByDisplayTextisbtOnWidth�   TcxGridDBColumn"cxGrid1DBTableView1NoBodyPerawatanCaptionNoBodyPerawatanDataBinding.FieldNameNoBodyPerawatan2Options.SortByDisplayTextisbtOnWidth�   TcxGridDBColumn#cxGrid1DBTableView1NoBodyDiperbaikiDataBinding.FieldNameNoBodyPerbaikanOptions.SortByDisplayTextisbtOnWidth�   TcxGridDBColumn cxGrid1DBTableView1NoBodyRebuildDataBinding.FieldNameNoBodyRebuildOptions.SortByDisplayTextisbtOnWidth|  TcxGridDBColumncxGrid1DBTableView1StatusDataBinding.FieldNameStatus  TcxGridDBColumncxGrid1DBTableView1NamaPemintaDataBinding.FieldNameNamaPeminta  TcxGridDBColumn cxGrid1DBTableView1NamaPenyetujuDataBinding.FieldNameNamaPenyetuju  TcxGridDBColumn,cxGrid1DBTableView1NamaPengemudiArmadaVerpalDataBinding.FieldNameNamaPengemudiArmadaVerpal  TcxGridDBColumncxGrid1DBTableView1TglSJDataBinding.FieldNameTglSJ  TcxGridDBColumncxGrid1DBTableView1TglPerawatanDataBinding.FieldNameTglPerawatan  TcxGridDBColumn&cxGrid1DBTableView1TglLaporanPerbaikanDataBinding.FieldNameTglLaporanPerbaikan   TcxGridDBTableViewcxGrid1DBTableView2DataController.DataSourceDataSource2"DataController.DetailKeyFieldNamesKodeBonBarang"DataController.MasterKeyFieldNamesKode/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups )OptionsBehavior.ExpandMasterRowOnDblClickOptionsView.GroupByBox TcxGridDBColumn cxGrid1DBTableView2KodeBonBarangDataBinding.FieldNameKodeBonBarang  TcxGridDBColumncxGrid1DBTableView2KodeBarangDataBinding.FieldName
KodeBarang  TcxGridDBColumncxGrid1DBTableView2namaDataBinding.FieldNamenamaWidth�   TcxGridDBColumn cxGrid1DBTableView2JumlahDimintaDataBinding.FieldNameJumlahDiminta  TcxGridDBColumncxGrid1DBTableView2JumlahBeliDataBinding.FieldName
JumlahBeli  TcxGridDBColumncxGrid1DBTableView2KeteranganDataBinding.FieldName
Keterangan  TcxGridDBColumncxGrid1DBTableView2StatusMintaDataBinding.FieldNameStatusMintaWidth�   TcxGridDBColumncxGrid1DBTableView2StatusBeliDataBinding.FieldName
StatusBeliWidth�   TcxGridDBColumncxGrid1DBTableView2UrgentDataBinding.FieldNameUrgent   TcxGridLevelcxGrid1Level1GridViewcxGrid1DBTableView1   TDataSourceDataSource1DataSet
BonBarangQLeftXTop<  TSDQuery
BonBarangQDatabaseNameDataOptions SQL.Strings2select * from BonBarang  where status='ON PROCESS'order by tglentry desc LeftXTop TStringFieldBonBarangQKode	FieldNameKodeRequired	Size
  TStringFieldBonBarangQPeminta	FieldNamePemintaRequired	Size
  TStringFieldBonBarangQPenyetuju	FieldName	PenyetujuRequired	Size
  TStringFieldBonBarangQPenerima	FieldNamePenerimaSize
  TStringFieldBonBarangQStoring	FieldNameStoringSize
  TStringFieldBonBarangQSPK	FieldNameSPKSize
  TStringFieldBonBarangQStatus	FieldNameStatusRequired	Size2  TDateTimeFieldBonBarangQCreateDate	FieldName
CreateDate  TStringFieldBonBarangQCreateBy	FieldNameCreateBySize
  TStringFieldBonBarangQOperator	FieldNameOperatorSize
  TDateTimeFieldBonBarangQTglEntry	FieldNameTglEntry  TStringFieldBonBarangQArmada	FieldNameArmadaSize
  TDateTimeFieldBonBarangQTglCetak	FieldNameTglCetak  TStringFieldBonBarangQNamaPeminta	FieldKindfkLookup	FieldNameNamaPemintaLookupDataSetPegawaiQLookupKeyFieldsKodeLookupResultFieldNama	KeyFieldsPemintaSize2Lookup	  TStringFieldBonBarangQNamaPenyetuju	FieldKindfkLookup	FieldNameNamaPenyetujuLookupDataSetPegawaiQLookupKeyFieldsKodeLookupResultFieldNama	KeyFields	PenyetujuSize2Lookup	  TDateTimeFieldBonBarangQTglLaporanPerbaikan	FieldKindfkLookup	FieldNameTglLaporanPerbaikanLookupDataSetLaporanPerbaikanQLookupKeyFieldsKodeLookupResultFieldTanggal	KeyFieldsLaporanPerbaikanLookup	  TStringField!BonBarangQKodePermintaanPerbaikan	FieldKindfkLookup	FieldNameKodePermintaanPerbaikanLookupDataSetLaporanPerbaikanQLookupKeyFieldsKodeLookupResultFieldPP	KeyFieldsLaporanPerbaikanSize
Lookup	  TStringFieldBonBarangQKodeArmadaPerbaikan	FieldKindfkLookup	FieldNameKodeArmadaPerbaikanLookupDataSetPermintaanPerbaikanQLookupKeyFieldsKodeLookupResultFieldArmada	KeyFieldsKodePermintaanPerbaikanSize
Lookup	  TStringFieldBonBarangQNoBodyDiperbaiki	FieldKindfkLookup	FieldNameNoBodyDiperbaikiLookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsKodeArmadaPerbaikanSize2Lookup	  TDateTimeFieldBonBarangQTglPerawatan	FieldKindfkLookup	FieldNameTglPerawatanLookupDataSetLaporanPerawatanQLookupKeyFieldsKodeLookupResultFieldTanggal	KeyFieldsLaporanPerawatanLookup	  TDateTimeFieldBonBarangQKodeArmadaPerawatan	FieldKindfkLookup	FieldNameKodeArmadaPerawatanLookupDataSetLaporanPerawatanQLookupKeyFieldsKodeLookupResultFieldArmada	KeyFieldsLaporanPerawatanLookup	  TStringFieldBonBarangQNoBodyPerawatan	FieldKindfkLookup	FieldNameNoBodyPerawatanLookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsKodeArmadaPerawatanSize2Lookup	  TStringFieldBonBarangQKodeArmadaStoring	FieldKindfkLookup	FieldNameKodeArmadaStoringLookupDataSetStoringQLookupKeyFieldsKodeLookupResultFieldArmada	KeyFieldsStoringSize
Lookup	  TStringFieldBonBarangQNoBodyArmadaVerpal	FieldKindfkLookup	FieldNameNoBodyArmadaVerpalLookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsKodeArmadaStoringSize2Lookup	  TStringField#BonBarangQKodePengemudiArmadaVerpal	FieldKindfkLookup	FieldNameKodePengemudiArmadaVerpalLookupDataSetStoringQLookupKeyFieldsKodeLookupResultField	Pengemudi	KeyFieldsStoringSize
Lookup	  TStringField#BonBarangQNamaPengemudiArmadaVerpal	FieldKindfkLookup	FieldNameNamaPengemudiArmadaVerpalLookupDataSetPegawaiQLookupKeyFieldsKodeLookupResultFieldNama	KeyFieldsKodePengemudiArmadaVerpalSize2Lookup	  TStringFieldBonBarangQKodeSJ	FieldKindfkLookup	FieldNameKodeSJLookupDataSetStoringQLookupKeyFieldsKodeLookupResultField
SuratJalan	KeyFieldsStoringSize
Lookup	  TDateTimeFieldBonBarangQTglSJ	FieldKindfkLookup	FieldNameTglSJLookupDataSetSJQLookupKeyFieldsKodenotaLookupResultFieldTgl	KeyFieldsKodeSJLookup	  TDateTimeFieldBonBarangQTglSPK	FieldKindfkLookup	FieldNameTglSPKLookupDataSetSPKQLookupKeyFieldsKodeLookupResultFieldTanggal	KeyFieldsSPKLookup	  TStringFieldBonBarangQLaporanPerbaikan	FieldKindfkLookup	FieldNameLaporanPerbaikanLookupDataSetSPKQLookupKeyFieldsKodeLookupResultFieldLaporanPerbaikan	KeyFieldsSPKSize
Lookup	  TStringFieldBonBarangQLaporanPerawatan	FieldKindfkLookup	FieldNameLaporanPerawatanLookupDataSetSPKQLookupKeyFieldsKodeLookupResultFieldLaporanPerawatan	KeyFieldsSPKSize
Lookup	  TStringFieldBonBarangQKodePP	FieldKindfkLookup	FieldNameKodePPLookupDataSetLaporanPerbaikanQLookupKeyFieldsKodeLookupResultFieldPP	KeyFieldsLaporanPerbaikanSize
Lookup	  TStringFieldBonBarangQArmadaPerbaikan	FieldKindfkLookup	FieldNameArmadaPerbaikanLookupDataSetPPQLookupKeyFieldsKodeLookupResultFieldArmada	KeyFieldsKodePPSize
Lookup	  TStringFieldBonBarangQNoBodyPerbaikan	FieldKindfkLookup	FieldNameNoBodyPerbaikanLookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsArmadaPerbaikanSize2Lookup	  TStringFieldBonBarangQArmadaPerawatan	FieldKindfkLookup	FieldNameArmadaPerawatanLookupDataSetLaporanPerawatanQLookupKeyFieldsKodeLookupResultFieldArmada	KeyFieldsLaporanPerawatanSize
Lookup	  TStringFieldBonBarangQNoBodyPerawatan2	FieldKindfkLookup	FieldNameNoBodyPerawatan2LookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsArmadaPerawatanSize2Lookup	  TStringFieldBonBarangQRebuild	FieldKindfkLookup	FieldNameRebuildLookupDataSetSPKQLookupKeyFieldsKodeLookupResultFieldRebuild	KeyFieldsSPKSize
Lookup	  TStringFieldBonBarangQArmadaRebuild	FieldKindfkLookup	FieldNameArmadaRebuildLookupDataSetRebuildQLookupKeyFieldsKodeLookupResultField
DariArmada	KeyFieldsRebuildLookup	  TStringFieldBonBarangQNoBodyRebuild	FieldKindfkLookup	FieldNameNoBodyRebuildLookupDataSetArmadaQLookupKeyFieldsKodeLookupResultFieldNoBody	KeyFieldsArmadaRebuildSize2Lookup	   TSDQueryArmadaQDatabaseNameDataOptions Active	SQL.Stringsselect * from Armada Left�Top TStringFieldArmadaQKode	FieldNameKodeRequired	Size
  TStringFieldArmadaQPlatNo	FieldNamePlatNoSize
  TStringFieldArmadaQNoBody	FieldNameNoBodySize2  TStringFieldArmadaQNoRangka	FieldNameNoRangkaSize2  TStringFieldArmadaQNoMesin	FieldNameNoMesinSize2  TStringFieldArmadaQJenisKendaraan	FieldNameJenisKendaraanSize
  TIntegerFieldArmadaQJumlahSeat	FieldName
JumlahSeat  TStringFieldArmadaQJenisBBM	FieldNameJenisBBMSize2  TStringFieldArmadaQTahunPembuatan	FieldNameTahunPembuatanSize  TStringFieldArmadaQJenisAC	FieldNameJenisACSize
  TBooleanFieldArmadaQToilet	FieldNameToilet  TBooleanFieldArmadaQAirSuspension	FieldNameAirSuspension  TStringFieldArmadaQSopir	FieldNameSopirSize
  TIntegerFieldArmadaQKapasitasTangkiBBM	FieldNameKapasitasTangkiBBM  TDateTimeFieldArmadaQSTNKPajakExpired	FieldNameSTNKPajakExpired  TDateTimeFieldArmadaQKirSelesai	FieldName
KirSelesai  TStringFieldArmadaQLevelArmada	FieldNameLevelArmadaSize2  TIntegerFieldArmadaQJumlahBan	FieldName	JumlahBan  TStringFieldArmadaQKeterangan	FieldName
KeteranganSize2  TBooleanFieldArmadaQAktif	FieldNameAktif  TBooleanField	ArmadaQAC	FieldNameAC  TIntegerFieldArmadaQKmSekarang	FieldName
KmSekarang  TDateTimeFieldArmadaQCreateDate	FieldName
CreateDate  TStringFieldArmadaQCreateBy	FieldNameCreateBySize
  TStringFieldArmadaQOperator	FieldNameOperatorSize
  TDateTimeFieldArmadaQTglEntry	FieldNameTglEntry  TDateTimeFieldArmadaQSTNKPerpanjangExpired	FieldNameSTNKPerpanjangExpired  TDateTimeFieldArmadaQKirMulai	FieldNameKirMulai   TSDQueryLaporanPerbaikanQDatabaseNameDataOptions Active	SQL.Stringsselect * from LaporanPerbaikan Left�Top8 TStringFieldLaporanPerbaikanQKode	FieldNameKodeRequired	Size
  TStringFieldLaporanPerbaikanQPP	FieldNamePPRequired	Size
  TDateTimeFieldLaporanPerbaikanQTanggal	FieldNameTanggal  
TMemoFieldLaporanPerbaikanQAnalisaMasalah	FieldNameAnalisaMasalahRequired	BlobTypeftMemo  
TMemoField"LaporanPerbaikanQTindakanPerbaikan	FieldNameTindakanPerbaikanRequired	BlobTypeftMemo  TDateTimeFieldLaporanPerbaikanQWaktuMulai	FieldName
WaktuMulai  TDateTimeFieldLaporanPerbaikanQWaktuSelesai	FieldNameWaktuSelesai  TStringFieldLaporanPerbaikanQVerifikator	FieldNameVerifikatorSize
  TDateTimeFieldLaporanPerbaikanQTglSerahTerima	FieldNameTglSerahTerima  TStringFieldLaporanPerbaikanQPICSerahTerima	FieldNamePICSerahTerimaSize
  TIntegerField!LaporanPerbaikanQKmArmadaSekarang	FieldNameKmArmadaSekarang  TStringFieldLaporanPerbaikanQStatus	FieldNameStatusRequired	Size2  
TMemoFieldLaporanPerbaikanQKeterangan	FieldName
KeteranganBlobTypeftMemo  TCurrencyFieldLaporanPerbaikanQClaimSopir	FieldName
ClaimSopir  TStringFieldLaporanPerbaikanQSopirClaim	FieldName
SopirClaimSize
  TStringField%LaporanPerbaikanQKeteranganClaimSopir	FieldNameKeteranganClaimSopirSize2  TDateTimeFieldLaporanPerbaikanQCreateDate	FieldName
CreateDate  TStringFieldLaporanPerbaikanQCreateBy	FieldNameCreateBySize
  TStringFieldLaporanPerbaikanQOperator	FieldNameOperatorSize
  TDateTimeFieldLaporanPerbaikanQTglEntry	FieldNameTglEntry  TDateTimeFieldLaporanPerbaikanQTglCetak	FieldNameTglCetak   TSDQueryPegawaiQDatabaseNameDataOptions Active	SQL.Stringsselect * from Pegawai Left�Top8 TStringFieldPegawaiQKode	FieldNameKodeRequired	Size
  TStringFieldPegawaiQNama	FieldNameNamaSize2  TStringFieldPegawaiQAlamat	FieldNameAlamatSize2  TStringFieldPegawaiQKota	FieldNameKotaSize2  TStringFieldPegawaiQNoTelp	FieldNameNoTelpSize2  TStringFieldPegawaiQNoHP	FieldNameNoHPSize2  TDateTimeFieldPegawaiQTglLahir	FieldNameTglLahir  TCurrencyFieldPegawaiQGaji	FieldNameGaji  TStringFieldPegawaiQJabatan	FieldNameJabatanSize2  TDateTimeFieldPegawaiQMulaiBekerja	FieldNameMulaiBekerja  TStringFieldPegawaiQNomorSIM	FieldNameNomorSIMSize2  TDateTimeFieldPegawaiQExpiredSIM	FieldName
ExpiredSIM  TBooleanFieldPegawaiQAktif	FieldNameAktif  
TMemoFieldPegawaiQKeterangan	FieldName
KeteranganBlobTypeftMemo  TStringFieldPegawaiQNoKTP	FieldNameNoKTPSize2   TSDQueryStoringQDatabaseNameDataOptions Active	SQL.Stringsselect * from Storing Left�Top TStringFieldStoringQKode	FieldNameKodeRequired	Size
  TStringFieldStoringQSuratJalan	FieldName
SuratJalanSize
  TStringFieldStoringQArmada	FieldNameArmadaSize
  TStringFieldStoringQPengemudi	FieldName	PengemudiSize
  TStringFieldStoringQKategoriRute	FieldNameKategoriRuteSize2  TCurrencyFieldStoringQBiaya	FieldNameBiayaRequired	  TStringFieldStoringQPICJemput	FieldName	PICJemputRequired	Size
  
TMemoFieldStoringQJenisStoring	FieldNameJenisStoringRequired	BlobTypeftMemo  TDateTimeFieldStoringQCreateDate	FieldName
CreateDate  TStringFieldStoringQCreateBy	FieldNameCreateBySize
  TStringFieldStoringQOperator	FieldNameOperatorSize
  TDateTimeFieldStoringQTglEntry	FieldNameTglEntry  
TMemoFieldStoringQTindakanPerbaikan	FieldNameTindakanPerbaikanBlobTypeftMemo  TDateTimeFieldStoringQTglCetak	FieldNameTglCetak   TSDQueryLaporanPerawatanQDatabaseNameDataOptions Active	SQL.Stringsselect * from LaporanPerawatan Left�Top TStringFieldLaporanPerawatanQKode	FieldNameKodeRequired	Size
  TDateTimeFieldLaporanPerawatanQTanggal	FieldNameTanggal  TDateTimeFieldLaporanPerawatanQWaktuMulai	FieldName
WaktuMulai  TDateTimeFieldLaporanPerawatanQWaktuSelesai	FieldNameWaktuSelesai  TStringFieldLaporanPerawatanQVerifikator	FieldNameVerifikatorSize
  TBooleanFieldLaporanPerawatanQButuhPerbaikan	FieldNameButuhPerbaikan  TIntegerField!LaporanPerawatanQKmArmadaSekarang	FieldNameKmArmadaSekarangRequired	  TStringFieldLaporanPerawatanQStatus	FieldNameStatusRequired	Size2  
TMemoFieldLaporanPerawatanQKeterangan	FieldName
KeteranganBlobTypeftMemo  TDateTimeFieldLaporanPerawatanQCreateDate	FieldName
CreateDate  TStringFieldLaporanPerawatanQCreateBy	FieldNameCreateBySize
  TStringFieldLaporanPerawatanQOperator	FieldNameOperatorSize
  TDateTimeFieldLaporanPerawatanQTglEntry	FieldNameTglEntry  TDateTimeFieldLaporanPerawatanQTglCetak	FieldNameTglCetak  TStringFieldLaporanPerawatanQArmada	FieldNameArmadaSize
   TSDQueryPermintaanPerbaikanQDatabaseNameDataOptions Active	SQL.Strings!select * from PermintaanPerbaikan Left�Top8 TStringFieldPermintaanPerbaikanQKode	FieldNameKodeRequired	Size
  TDateTimeFieldPermintaanPerbaikanQTanggal	FieldNameTanggal  TStringFieldPermintaanPerbaikanQArmada	FieldNameArmadaSize
  TStringFieldPermintaanPerbaikanQPeminta	FieldNamePemintaRequired	Size
  
TMemoFieldPermintaanPerbaikanQKeluhan	FieldNameKeluhanRequired	BlobTypeftMemo  TDateTimeFieldPermintaanPerbaikanQCreateDate	FieldName
CreateDate  TStringFieldPermintaanPerbaikanQCreateBy	FieldNameCreateBySize
  TStringFieldPermintaanPerbaikanQOperator	FieldNameOperatorSize
  TDateTimeFieldPermintaanPerbaikanQTglEntry	FieldNameTglEntry  TDateTimeFieldPermintaanPerbaikanQTglCetak	FieldNameTglCetak   TSDQuerySJQDatabaseNameDataOptions SQL.Stringsselect * from MasterSJ Left�Top TStringFieldSJQKodenota	FieldNameKodenotaRequired	Size
  TDateTimeFieldSJQTgl	FieldNameTglRequired	  TStringFieldSJQNoSO	FieldNameNoSORequired	Size
  TStringFieldSJQSopir	FieldNameSopirSize
  TStringField	SJQSopir2	FieldNameSopir2Size
  TStringFieldSJQCrew	FieldNameCrewSize2  TBooleanFieldSJQTitipKwitansi	FieldNameTitipKwitansi  TCurrencyFieldSJQNominalKwitansi	FieldNameNominalKwitansi  TStringFieldSJQNoKwitansi	FieldName
NoKwitansiSize2  TBooleanFieldSJQKir	FieldNameKirRequired	  TBooleanFieldSJQSTNK	FieldNameSTNKRequired	  TBooleanFieldSJQPajak	FieldNamePajakRequired	  TDateTimeFieldSJQTglKembali	FieldName
TglKembali  TCurrencyFieldSJQPendapatan	FieldName
Pendapatan  TCurrencyFieldSJQPengeluaran	FieldNamePengeluaran  TCurrencyFieldSJQSisaDisetor	FieldNameSisaDisetor  TFloatFieldSJQSPBUAYaniLiter	FieldNameSPBUAYaniLiter  TCurrencyFieldSJQSPBUAYaniUang	FieldNameSPBUAYaniUang  TFloatFieldSJQSPBULuarLiter	FieldNameSPBULuarLiter  TCurrencyFieldSJQSPBULuarUang	FieldNameSPBULuarUang  TCurrencyFieldSJQSPBULuarUangDiberi	FieldNameSPBULuarUangDiberi  
TMemoFieldSJQSPBULuarDetail	FieldNameSPBULuarDetailBlobTypeftMemo  TStringField	SJQStatus	FieldNameStatusRequired	Size2  TDateTimeFieldSJQCreateDate	FieldName
CreateDate  TStringFieldSJQCreateBy	FieldNameCreateBySize
  TStringFieldSJQOperator	FieldNameOperatorSize
  TDateTimeFieldSJQTglEntry	FieldNameTglEntry  
TMemoField
SJQLaporan	FieldNameLaporanBlobTypeftMemo  TDateTimeFieldSJQTglRealisasi	FieldNameTglRealisasi  TStringFieldSJQAwal	FieldNameAwalSize
  TStringFieldSJQAkhir	FieldNameAkhirSize
  TDateTimeFieldSJQTglCetak	FieldNameTglCetak  TCurrencyFieldSJQClaimSopir	FieldName
ClaimSopir  TStringFieldSJQKeteranganClaimSopir	FieldNameKeteranganClaimSopirSize2  TCurrencyFieldSJQPremiSopir	FieldName
PremiSopir  TCurrencyFieldSJQPremiSopir2	FieldNamePremiSopir2  TCurrencyFieldSJQPremiKernet	FieldNamePremiKernet  TCurrencyFieldSJQTabunganSopir	FieldNameTabunganSopir  TCurrencyFieldSJQTabunganSopir2	FieldNameTabunganSopir2  TCurrencyFieldSJQTol	FieldNameTol  TCurrencyFieldSJQUangJalan	FieldName	UangJalan  TCurrencyFieldSJQBiayaLainLain	FieldNameBiayaLainLain  TStringFieldSJQKeteranganBiayaLainLain	FieldNameKeteranganBiayaLainLainSize2  TCurrencyFieldSJQUangMakan	FieldName	UangMakan  TCurrencyFieldSJQUangInap	FieldNameUangInap  TCurrencyFieldSJQUangParkir	FieldName
UangParkir  TCurrencyFieldSJQOther	FieldNameOther  
TMemoFieldSJQKeterangan	FieldName
KeteranganBlobTypeftMemo  TDateTimeFieldSJQSPBUAYaniJam	FieldNameSPBUAYaniJam   TSDQuerySPKQDatabaseNameDataOptions Active	SQL.Strings select * from SuratPerintahKerja Left�Top8 TStringFieldSPKQKode	FieldNameKodeRequired	Size
  TDateTimeFieldSPKQTanggal	FieldNameTanggal  TStringFieldSPKQLaporanPerbaikan	FieldNameLaporanPerbaikanSize
  TStringFieldSPKQLaporanPerawatan	FieldNameLaporanPerawatanSize
  TStringFieldSPKQRebuild	FieldNameRebuildSize
  TStringFieldSPKQMekanik	FieldNameMekanikSize
  
TMemoFieldSPKQDetailTindakan	FieldNameDetailTindakanBlobTypeftMemo  TDateTimeFieldSPKQWaktuMulai	FieldName
WaktuMulai  TDateTimeFieldSPKQWaktuSelesai	FieldNameWaktuSelesai  TStringField
SPKQStatus	FieldNameStatusSize2  TStringFieldSPKQKeterangan	FieldName
KeteranganSize2  TDateTimeFieldSPKQCreateDate	FieldName
CreateDate  TStringFieldSPKQCreateBy	FieldNameCreateBySize
  TStringFieldSPKQOperator	FieldNameOperatorSize
  TDateTimeFieldSPKQTglEntry	FieldNameTglEntry   TSDQueryPPQDatabaseNameDataOptions Active	SQL.Strings!select * from PermintaanPerbaikan Left8Top TStringFieldPPQKode	FieldNameKodeRequired	Size
  TDateTimeField
PPQTanggal	FieldNameTanggal  TStringField	PPQArmada	FieldNameArmadaSize
  TStringField
PPQPeminta	FieldNamePemintaRequired	Size
  
TMemoField
PPQKeluhan	FieldNameKeluhanRequired	BlobTypeftMemo  TDateTimeFieldPPQCreateDate	FieldName
CreateDate  TStringFieldPPQCreateBy	FieldNameCreateBySize
  TStringFieldPPQOperator	FieldNameOperatorSize
  TDateTimeFieldPPQTglEntry	FieldNameTglEntry  TDateTimeFieldPPQTglCetak	FieldNameTglCetak   TSDQueryRebuildQDatabaseNameDataOptions Active	SQL.Stringsselect * from Rebuild LeftTop TStringFieldRebuildQKode	FieldNameKodeRequired	Size
  TStringFieldRebuildQBarang	FieldNameBarangSize
  TStringFieldRebuildQDariArmada	FieldName
DariArmadaSize
  TDateTimeFieldRebuildQTanggalMasuk	FieldNameTanggalMasuk  TStringFieldRebuildQKeArmada	FieldNameKeArmadaSize
  TDateTimeFieldRebuildQTanggalKeluar	FieldNameTanggalKeluar  
TMemoFieldRebuildQAnalisaMasalah	FieldNameAnalisaMasalahBlobTypeftMemo  TBooleanFieldRebuildQJasaLuar	FieldNameJasaLuar  TStringFieldRebuildQSupplier	FieldNameSupplierSize
  TCurrencyFieldRebuildQHarga	FieldNameHarga  TDateTimeFieldRebuildQTanggalKirim	FieldNameTanggalKirim  TStringFieldRebuildQPICKirim	FieldNamePICKirimSize
  TDateTimeFieldRebuildQTanggalKembali	FieldNameTanggalKembali  TStringFieldRebuildQPenerima	FieldNamePenerimaSize
  TBooleanFieldRebuildQPerbaikanInternal	FieldNamePerbaikanInternal  TStringFieldRebuildQVerifikator	FieldNameVerifikatorSize
  TDateTimeFieldRebuildQTglVerifikasi	FieldNameTglVerifikasi  TBooleanFieldRebuildQKanibal	FieldNameKanibal  TFloatFieldRebuildQPersentaseCosting	FieldNamePersentaseCosting  TStringFieldRebuildQStatus	FieldNameStatusSize2  TDateTimeFieldRebuildQCreateDate	FieldName
CreateDate  TDateTimeFieldRebuildQTglEntry	FieldNameTglEntry  TStringFieldRebuildQOperator	FieldNameOperatorSize
  TStringFieldRebuildQCreateBy	FieldNameCreateBySize
   TSDQueryDetailBonBarangQDatabaseNameDataOptions SQL.Stringsselect dbb.*,b.nama!from detailbonbarang dbb,barang bwhere dbb.kodebarang=b.kode Left8TopP TStringFieldDetailBonBarangQKodeBonBarang	FieldNameKodeBonBarangRequired	Size
  TStringFieldDetailBonBarangQKodeBarang	FieldName
KodeBarangRequired	Size
  TIntegerFieldDetailBonBarangQJumlahDiminta	FieldNameJumlahDimintaRequired	  TIntegerFieldDetailBonBarangQJumlahBeli	FieldName
JumlahBeli  
TMemoFieldDetailBonBarangQKeterangan	FieldName
KeteranganBlobTypeftMemo  TStringFieldDetailBonBarangQStatusMinta	FieldNameStatusMintaSize2  TStringFieldDetailBonBarangQStatusBeli	FieldName
StatusBeliSize2  TBooleanFieldDetailBonBarangQUrgent	FieldNameUrgentRequired	  TStringFieldDetailBonBarangQnama	FieldNamenamaRequired	Size2   TDataSourceDataSource2DataSetDetailBonBarangQLeftTopT   