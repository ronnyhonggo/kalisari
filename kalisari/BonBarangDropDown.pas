unit BonBarangDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TBonBarangDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    BonBarangQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    BonBarangQKode: TStringField;
    BonBarangQPeminta: TStringField;
    BonBarangQPenyetuju: TStringField;
    BonBarangQPenerima: TStringField;
    BonBarangQStoring: TStringField;
    BonBarangQSPK: TStringField;
    BonBarangQStatus: TStringField;
    BonBarangQCreateDate: TDateTimeField;
    BonBarangQCreateBy: TStringField;
    BonBarangQOperator: TStringField;
    BonBarangQTglEntry: TDateTimeField;
    BonBarangQArmada: TStringField;
    BonBarangQTglCetak: TDateTimeField;
    ArmadaQ: TSDQuery;
    LaporanPerbaikanQ: TSDQuery;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    BonBarangQNamaPeminta: TStringField;
    BonBarangQNamaPenyetuju: TStringField;
    LaporanPerbaikanQKode: TStringField;
    LaporanPerbaikanQPP: TStringField;
    LaporanPerbaikanQTanggal: TDateTimeField;
    LaporanPerbaikanQAnalisaMasalah: TMemoField;
    LaporanPerbaikanQTindakanPerbaikan: TMemoField;
    LaporanPerbaikanQWaktuMulai: TDateTimeField;
    LaporanPerbaikanQWaktuSelesai: TDateTimeField;
    LaporanPerbaikanQVerifikator: TStringField;
    LaporanPerbaikanQTglSerahTerima: TDateTimeField;
    LaporanPerbaikanQPICSerahTerima: TStringField;
    LaporanPerbaikanQKmArmadaSekarang: TIntegerField;
    LaporanPerbaikanQStatus: TStringField;
    LaporanPerbaikanQKeterangan: TMemoField;
    LaporanPerbaikanQClaimSopir: TCurrencyField;
    LaporanPerbaikanQSopirClaim: TStringField;
    LaporanPerbaikanQKeteranganClaimSopir: TStringField;
    LaporanPerbaikanQCreateDate: TDateTimeField;
    LaporanPerbaikanQCreateBy: TStringField;
    LaporanPerbaikanQOperator: TStringField;
    LaporanPerbaikanQTglEntry: TDateTimeField;
    LaporanPerbaikanQTglCetak: TDateTimeField;
    BonBarangQTglLaporanPerbaikan: TDateTimeField;
    BonBarangQKodePermintaanPerbaikan: TStringField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    StoringQ: TSDQuery;
    LaporanPerawatanQ: TSDQuery;
    PermintaanPerbaikanQ: TSDQuery;
    PermintaanPerbaikanQKode: TStringField;
    PermintaanPerbaikanQTanggal: TDateTimeField;
    PermintaanPerbaikanQArmada: TStringField;
    PermintaanPerbaikanQPeminta: TStringField;
    PermintaanPerbaikanQKeluhan: TMemoField;
    PermintaanPerbaikanQCreateDate: TDateTimeField;
    PermintaanPerbaikanQCreateBy: TStringField;
    PermintaanPerbaikanQOperator: TStringField;
    PermintaanPerbaikanQTglEntry: TDateTimeField;
    PermintaanPerbaikanQTglCetak: TDateTimeField;
    BonBarangQKodeArmadaPerbaikan: TStringField;
    BonBarangQNoBodyDiperbaiki: TStringField;
    LaporanPerawatanQKode: TStringField;
    LaporanPerawatanQTanggal: TDateTimeField;
    LaporanPerawatanQWaktuMulai: TDateTimeField;
    LaporanPerawatanQWaktuSelesai: TDateTimeField;
    LaporanPerawatanQVerifikator: TStringField;
    LaporanPerawatanQButuhPerbaikan: TBooleanField;
    LaporanPerawatanQKmArmadaSekarang: TIntegerField;
    LaporanPerawatanQStatus: TStringField;
    LaporanPerawatanQKeterangan: TMemoField;
    LaporanPerawatanQCreateDate: TDateTimeField;
    LaporanPerawatanQCreateBy: TStringField;
    LaporanPerawatanQOperator: TStringField;
    LaporanPerawatanQTglEntry: TDateTimeField;
    LaporanPerawatanQTglCetak: TDateTimeField;
    LaporanPerawatanQArmada: TStringField;
    BonBarangQTglPerawatan: TDateTimeField;
    BonBarangQKodeArmadaPerawatan: TDateTimeField;
    BonBarangQNoBodyPerawatan: TStringField;
    StoringQKode: TStringField;
    StoringQSuratJalan: TStringField;
    StoringQArmada: TStringField;
    StoringQPengemudi: TStringField;
    StoringQKategoriRute: TStringField;
    StoringQBiaya: TCurrencyField;
    StoringQPICJemput: TStringField;
    StoringQJenisStoring: TMemoField;
    StoringQCreateDate: TDateTimeField;
    StoringQCreateBy: TStringField;
    StoringQOperator: TStringField;
    StoringQTglEntry: TDateTimeField;
    StoringQTindakanPerbaikan: TMemoField;
    StoringQTglCetak: TDateTimeField;
    BonBarangQKodeArmadaStoring: TStringField;
    BonBarangQNoBodyArmadaVerpal: TStringField;
    BonBarangQKodePengemudiArmadaVerpal: TStringField;
    BonBarangQNamaPengemudiArmadaVerpal: TStringField;
    SJQ: TSDQuery;
    SJQKodenota: TStringField;
    SJQTgl: TDateTimeField;
    SJQNoSO: TStringField;
    SJQSopir: TStringField;
    SJQSopir2: TStringField;
    SJQCrew: TStringField;
    SJQTitipKwitansi: TBooleanField;
    SJQNominalKwitansi: TCurrencyField;
    SJQNoKwitansi: TStringField;
    SJQKir: TBooleanField;
    SJQSTNK: TBooleanField;
    SJQPajak: TBooleanField;
    SJQTglKembali: TDateTimeField;
    SJQPendapatan: TCurrencyField;
    SJQPengeluaran: TCurrencyField;
    SJQSisaDisetor: TCurrencyField;
    SJQSPBUAYaniLiter: TFloatField;
    SJQSPBUAYaniUang: TCurrencyField;
    SJQSPBULuarLiter: TFloatField;
    SJQSPBULuarUang: TCurrencyField;
    SJQSPBULuarUangDiberi: TCurrencyField;
    SJQSPBULuarDetail: TMemoField;
    SJQStatus: TStringField;
    SJQCreateDate: TDateTimeField;
    SJQCreateBy: TStringField;
    SJQOperator: TStringField;
    SJQTglEntry: TDateTimeField;
    SJQLaporan: TMemoField;
    SJQTglRealisasi: TDateTimeField;
    SJQAwal: TStringField;
    SJQAkhir: TStringField;
    SJQTglCetak: TDateTimeField;
    SJQClaimSopir: TCurrencyField;
    SJQKeteranganClaimSopir: TStringField;
    SJQPremiSopir: TCurrencyField;
    SJQPremiSopir2: TCurrencyField;
    SJQPremiKernet: TCurrencyField;
    SJQTabunganSopir: TCurrencyField;
    SJQTabunganSopir2: TCurrencyField;
    SJQTol: TCurrencyField;
    SJQUangJalan: TCurrencyField;
    SJQBiayaLainLain: TCurrencyField;
    SJQKeteranganBiayaLainLain: TStringField;
    SJQUangMakan: TCurrencyField;
    SJQUangInap: TCurrencyField;
    SJQUangParkir: TCurrencyField;
    SJQOther: TCurrencyField;
    BonBarangQKodeSJ: TStringField;
    BonBarangQTglSJ: TDateTimeField;
    SPKQ: TSDQuery;
    SPKQKode: TStringField;
    SPKQTanggal: TDateTimeField;
    SPKQLaporanPerbaikan: TStringField;
    SPKQLaporanPerawatan: TStringField;
    SPKQRebuild: TStringField;
    SPKQMekanik: TStringField;
    SPKQDetailTindakan: TMemoField;
    SPKQWaktuMulai: TDateTimeField;
    SPKQWaktuSelesai: TDateTimeField;
    SPKQStatus: TStringField;
    SPKQKeterangan: TStringField;
    SPKQCreateDate: TDateTimeField;
    SPKQCreateBy: TStringField;
    SPKQOperator: TStringField;
    SPKQTglEntry: TDateTimeField;
    BonBarangQTglSPK: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPeminta: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenyetuju: TcxGridDBColumn;
    cxGrid1DBTableView1TglLaporanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1NoBodyDiperbaiki: TcxGridDBColumn;
    cxGrid1DBTableView1TglPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1NoBodyPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1NoBodyArmadaVerpal: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPengemudiArmadaVerpal: TcxGridDBColumn;
    cxGrid1DBTableView1TglSJ: TcxGridDBColumn;
    cxGrid1DBTableView1TglSPK: TcxGridDBColumn;
    SJQKeterangan: TMemoField;
    cxGrid1DBTableView1SPK: TcxGridDBColumn;
    BonBarangQLaporanPerbaikan: TStringField;
    BonBarangQLaporanPerawatan: TStringField;
    BonBarangQKodePP: TStringField;
    PPQ: TSDQuery;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQArmada: TStringField;
    PPQPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    BonBarangQArmadaPerbaikan: TStringField;
    BonBarangQNoBodyPerbaikan: TStringField;
    BonBarangQArmadaPerawatan: TStringField;
    BonBarangQNoBodyPerawatan2: TStringField;
    BonBarangQRebuild: TStringField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    BonBarangQArmadaRebuild: TStringField;
    BonBarangQNoBodyRebuild: TStringField;
    cxGrid1DBTableView1NoBodyRebuild: TcxGridDBColumn;
    cxGrid1DBTableView1CreateDate: TcxGridDBColumn;
    DetailBonBarangQ: TSDQuery;
    cxGrid1DBTableView2: TcxGridDBTableView;
    DataSource2: TDataSource;
    DetailBonBarangQKodeBonBarang: TStringField;
    DetailBonBarangQKodeBarang: TStringField;
    DetailBonBarangQJumlahDiminta: TIntegerField;
    DetailBonBarangQJumlahBeli: TIntegerField;
    DetailBonBarangQKeterangan: TMemoField;
    DetailBonBarangQStatusMinta: TStringField;
    DetailBonBarangQStatusBeli: TStringField;
    DetailBonBarangQUrgent: TBooleanField;
    cxGrid1DBTableView2KodeBarang: TcxGridDBColumn;
    cxGrid1DBTableView2JumlahDiminta: TcxGridDBColumn;
    cxGrid1DBTableView2JumlahBeli: TcxGridDBColumn;
    cxGrid1DBTableView2Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView2StatusMinta: TcxGridDBColumn;
    cxGrid1DBTableView2StatusBeli: TcxGridDBColumn;
    cxGrid1DBTableView2Urgent: TcxGridDBColumn;
    cxGrid1DBTableView2KodeBonBarang: TcxGridDBColumn;
    DetailBonBarangQnama: TStringField;
    cxGrid1DBTableView2nama: TcxGridDBColumn;
    SJQSPBUAYaniJam: TDateTimeField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  BonBarangDropDownFm: TBonBarangDropDownFm;

implementation

uses Math;

{$R *.dfm}

{ TDropDownFm }

procedure TBonBarangDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TBonBarangDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=BonBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TBonBarangDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BonBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TBonBarangDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=BonBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TBonBarangDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
//  cxGrid1DBTableView1.ViewData.Records[Sender.DataController.GetFocusedRecordIndex].Expanded:=true;
//  Ahandled:=true;
  
  kode:=BonBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TBonBarangDropDownFm.FormCreate(Sender: TObject);
var arr : array [1..100] of string;
begin
  PegawaiQ.Open;
  ArmadaQ.Open;
  StoringQ.Open;
  PPQ.Open;
  RebuildQ.Open;
  SJQ.Open;
  LaporanPerawatanQ.Open;
  PermintaanPerbaikanQ.Open;
  LaporanPerbaikanQ.Open;
  SPKQ.Open;
  BonBarangQ.Open;
  DetailBonBarangQ.Open;

end;

procedure TBonBarangDropDownFm.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
//  cxGrid1DBTableView1.Controller.FocusedRow.Expand(true);
end;

end.
