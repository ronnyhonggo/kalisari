object BonKeluarBarangFm: TBonKeluarBarangFm
  Left = 214
  Top = 221
  Width = 939
  Height = 555
  Caption = 'BonKeluarBarangFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 921
    Height = 321
    Caption = 'Panel1'
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 8
      Top = 40
      Width = 265
      Height = 89
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      DataController.DataSource = MasterSource
      Version = 1
      object cxDBVerticalGrid1BonBarang: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1BonBarangEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'BonBarang'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Penerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penerima'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1TglKeluar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglKeluar'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 375
      Top = 1
      Width = 545
      Height = 319
      Align = alRight
      TabOrder = 2
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1KodeBKB: TcxGridDBColumn
          DataBinding.FieldName = 'KodeBKB'
        end
        object cxGrid1DBTableView1Barang: TcxGridDBColumn
          DataBinding.FieldName = 'Barang'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1BarangPropertiesButtonClick
        end
        object cxGrid1DBTableView1Diminta: TcxGridDBColumn
          DataBinding.FieldName = 'Diminta'
        end
        object cxGrid1DBTableView1JumlahKeluar: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahKeluar'
          Width = 70
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
        end
        object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 321
    Width = 912
    Height = 168
    Caption = 'Panel2'
    TabOrder = 1
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 910
      Height = 112
      Align = alCustom
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid2DBTableView1BonBarang: TcxGridDBColumn
          DataBinding.FieldName = 'BonBarang'
        end
        object cxGrid2DBTableView1Penerima: TcxGridDBColumn
          DataBinding.FieldName = 'Penerima'
        end
        object cxGrid2DBTableView1TglKeluar: TcxGridDBColumn
          DataBinding.FieldName = 'TglKeluar'
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object BtnSave: TButton
      Left = 24
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BtnSaveClick
    end
    object BtnDelete: TButton
      Left = 136
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Delete BKB'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtnDeleteClick
    end
    object Button2: TButton
      Left = 247
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object ExitBtn: TButton
      Left = 359
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = ExitBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 498
    Width = 923
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BonKeluarBarang')
    UpdateObject = MasterUpdate
    Left = 176
    Top = 8
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object MasterQTglKeluar: TDateTimeField
      FieldName = 'TglKeluar'
      Required = True
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
  end
  object MasterSource: TDataSource
    DataSet = MasterQ
    Left = 208
    Top = 8
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, BonBarang, Penerima, TglKeluar, CreateDate, CreateB' +
        'y, TglEntry, Operator'
      'from BonKeluarBarang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update BonKeluarBarang'
      'set'
      '  Kode = :Kode,'
      '  BonBarang = :BonBarang,'
      '  Penerima = :Penerima,'
      '  TglKeluar = :TglKeluar,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into BonKeluarBarang'
      
        '  (Kode, BonBarang, Penerima, TglKeluar, CreateDate, CreateBy, T' +
        'glEntry, Operator)'
      'values'
      
        '  (:Kode, :BonBarang, :Penerima, :TglKeluar, :CreateDate, :Creat' +
        'eBy, :TglEntry, :Operator)')
    DeleteSQL.Strings = (
      'delete from BonKeluarBarang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 240
    Top = 8
  end
  object viewBonKeluarBarang: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BonKeluarBarang ')
    Left = 792
    Top = 440
    object viewBonKeluarBarangKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangTglKeluar: TDateTimeField
      FieldName = 'TglKeluar'
      Required = True
    end
    object viewBonKeluarBarangCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewBonKeluarBarangCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewBonKeluarBarangTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewBonKeluarBarangOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = viewBonKeluarBarang
    Left = 824
    Top = 448
  end
  object DetailBonKeluarBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select dbkb.*, '
      'Diminta=(select sum(JumlahDiminta) from DetailBonBarang dbb '
      'where KodeBarang=dbkb.Barang and StatusMinta='#39'ON PROCESS'#39')'
      'from detailBKB dbkb, BonKeluarBarang bkb'
      'where dbkb.KodeBKB=bkb.Kode and dbkb.KodeBKB=:text')
    UpdateObject = DetailBonKeluarBarangUpdate
    Left = 576
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailBonKeluarBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQKodeBKB: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQJumlahKeluar: TIntegerField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object DetailBonKeluarBarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailBonKeluarBarangQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object DetailBonKeluarBarangQDiminta: TIntegerField
      FieldName = 'Diminta'
    end
  end
  object DataSource2: TDataSource
    DataSet = DetailBonKeluarBarangQ
    Left = 608
    Top = 288
  end
  object DetailBonKeluarBarangUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, KodeBKB, Barang, JumlahKeluar, Keterangan'
      'from detailBKB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update detailBKB'
      'set'
      '  Kode = :Kode,'
      '  KodeBKB = :KodeBKB,'
      '  Barang = :Barang,'
      '  JumlahKeluar = :JumlahKeluar,'
      '  Keterangan = :Keterangan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into detailBKB'
      '  (Kode, KodeBKB, Barang, JumlahKeluar, Keterangan)'
      'values'
      '  (:Kode, :KodeBKB, :Barang, :JumlahKeluar, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from detailBKB'
      'where'
      '  Kode = :OLD_Kode')
    Left = 640
    Top = 280
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from BonKeluarBarang order by kode desc')
    Left = 296
    Top = 16
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object BonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    Filtered = True
    SQL.Strings = (
      'select * from BonBarang')
    Left = 296
    Top = 64
    object BonBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BonBarangQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object BonBarangQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object BonBarangQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object BonBarangQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object BonBarangQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object BonBarangQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object BonBarangQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object BonBarangQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object BonBarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BonBarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BonBarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BonBarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BonBarangQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object BonBarangQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 296
    Top = 104
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    Filtered = True
    SQL.Strings = (
      'select * from Barang')
    Left = 304
    Top = 152
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object KodeDetailBKB: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from detailBKB order by kode desc')
    Left = 528
    Top = 280
    object KodeDetailBKBkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DeleteDBKBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailBKB dbkb where dbkb.KodeBKB=:text')
    Left = 184
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDBKBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DeleteDBKBQKodeBKB: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object DeleteDBKBQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DeleteDBKBQJumlahKeluar: TIntegerField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object DeleteDBKBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DeleteDBKBUpd: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, KodeBKB, Barang, JumlahKeluar, Keterangan'
      'from DetailBKB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update DetailBKB'
      'set'
      '  Kode = :Kode,'
      '  KodeBKB = :KodeBKB,'
      '  Barang = :Barang,'
      '  JumlahKeluar = :JumlahKeluar,'
      '  Keterangan = :Keterangan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into DetailBKB'
      '  (Kode, KodeBKB, Barang, JumlahKeluar, Keterangan)'
      'values'
      '  (:Kode, :KodeBKB, :Barang, :JumlahKeluar, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailBKB'
      'where'
      '  KodeBKB = :OLD_KodeBKB'
      ' ')
    Left = 224
    Top = 248
  end
  object CobaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select distinct Diminta=(select sum(JumlahDiminta) from DetailBo' +
        'nBarang where KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39'),'
      
        'PendingBeli=(select sum(JumlahBeli) from DetailBonBarang where K' +
        'odeBarang=:text and StatusBeli='#39'ON PROCESS'#39') '
      'from DetailBonBarang dbb, barang b')
    Left = 456
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CobaQDiminta: TIntegerField
      FieldName = 'Diminta'
    end
    object CobaQPendingBeli: TIntegerField
      FieldName = 'PendingBeli'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailBKB dbkb where dbkb.KodeBKB=:text')
    UpdateObject = DeleteDBKBUpd
    Left = 208
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
