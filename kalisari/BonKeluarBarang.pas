unit BonKeluarBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, cxVGrid, cxDBVGrid, cxInplaceContainer,
  SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit, ComCtrls, ExtCtrls;

type
  TBonKeluarBarangFm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar: TStatusBar;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQBonBarang: TStringField;
    MasterQPenerima: TStringField;
    MasterQTglKeluar: TDateTimeField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterSource: TDataSource;
    MasterUpdate: TSDUpdateSQL;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1BonBarang: TcxDBEditorRow;
    cxDBVerticalGrid1Penerima: TcxDBEditorRow;
    cxDBVerticalGrid1TglKeluar: TcxDBEditorRow;
    viewBonKeluarBarang: TSDQuery;
    DataSource1: TDataSource;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    BtnSave: TButton;
    BtnDelete: TButton;
    Button2: TButton;
    DetailBonKeluarBarangQ: TSDQuery;
    DataSource2: TDataSource;
    DetailBonKeluarBarangUpdate: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TButton;
    BonBarangQ: TSDQuery;
    BonBarangQKode: TStringField;
    BonBarangQPeminta: TStringField;
    BonBarangQPenyetuju: TStringField;
    BonBarangQPenerima: TStringField;
    BonBarangQLaporanPerbaikan: TStringField;
    BonBarangQLaporanPerawatan: TStringField;
    BonBarangQStoring: TStringField;
    BonBarangQSPK: TStringField;
    BonBarangQStatus: TStringField;
    BonBarangQCreateDate: TDateTimeField;
    BonBarangQCreateBy: TStringField;
    BonBarangQOperator: TStringField;
    BonBarangQTglEntry: TDateTimeField;
    BonBarangQArmada: TStringField;
    BonBarangQTglCetak: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    viewBonKeluarBarangKode: TStringField;
    viewBonKeluarBarangBonBarang: TStringField;
    viewBonKeluarBarangPenerima: TStringField;
    viewBonKeluarBarangTglKeluar: TDateTimeField;
    viewBonKeluarBarangCreateDate: TDateTimeField;
    viewBonKeluarBarangCreateBy: TStringField;
    viewBonKeluarBarangTglEntry: TDateTimeField;
    viewBonKeluarBarangOperator: TStringField;
    DetailBonKeluarBarangQKode: TStringField;
    DetailBonKeluarBarangQKodeBKB: TStringField;
    DetailBonKeluarBarangQBarang: TStringField;
    DetailBonKeluarBarangQJumlahKeluar: TIntegerField;
    DetailBonKeluarBarangQKeterangan: TMemoField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid2DBTableView1Penerima: TcxGridDBColumn;
    cxGrid2DBTableView1TglKeluar: TcxGridDBColumn;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TIntegerField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    cxGrid1DBTableView1KodeBKB: TcxGridDBColumn;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahKeluar: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    KodeDetailBKB: TSDQuery;
    KodeDetailBKBkode: TStringField;
    DetailBonKeluarBarangQNamaBarang: TStringField;
    DetailBonKeluarBarangQDiminta: TIntegerField;
    cxGrid1DBTableView1Diminta: TcxGridDBColumn;
    DeleteDBKBQ: TSDQuery;
    DeleteDBKBUpd: TSDUpdateSQL;
    DeleteDBKBQKode: TStringField;
    DeleteDBKBQKodeBKB: TStringField;
    DeleteDBKBQBarang: TStringField;
    DeleteDBKBQJumlahKeluar: TIntegerField;
    DeleteDBKBQKeterangan: TMemoField;
    CobaQ: TSDQuery;
    CobaQDiminta: TIntegerField;
    CobaQPendingBeli: TIntegerField;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField1: TIntegerField;
    MemoField1: TMemoField;
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxDBVerticalGrid1BonBarangEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1DBTableView1BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BonKeluarBarangFm: TBonKeluarBarangFm;
  MasterOriSQL: string;
  paramkode :string;
  DetailBonKeluarBarangOriSQL: string;

implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

procedure TBonKeluarBarangFm.KodeEditEnter(Sender: TObject);
begin
 KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  BtnSave.Enabled:=false;
 // BtnKepalaTeknik.Enabled:=false;
  //BtnSetuju.Enabled:=false;
 // BtnDelete.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TBonKeluarBarangFm.KodeEditExit(Sender: TObject);
begin
if KodeEdit.Text<>'' then
  begin
   MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //DMFm.GetDateQ.Open;
      //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;

      //DMFm.GetDateQ.Close;
      btnsave.Enabled:=menuutamafm.UserQInsertBonKeluarBarang.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    BtnSave.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
  end;

end;

procedure TBonKeluarBarangFm.FormCreate(Sender: TObject);
begin
 MasterOriSQL:=MasterQ.SQL.Text;
 DetailBonKeluarBarangOriSQL:=DetailBonKeluarBarangQ.SQL.Text;
  //DetailBonBarangOriSQL:=DetailBonBarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TBonKeluarBarangFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    release;
end;

procedure TBonKeluarBarangFm.FormShow(Sender: TObject);
begin
KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  ViewBonKeluarBarang.Active:=TRUE;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertBonKeluarBarang.AsBoolean;
  BtnSave.Enabled:=menuutamafm.UserQUpdateBonKeluarBarang.AsBoolean;
  BtnDelete.Enabled:=menuutamafm.UserQDeleteBonKeluarBarang.AsBoolean;
  //RbtSPK.Checked:=TRUE;

end;

procedure TBonKeluarBarangFm.KodeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TBonKeluarBarangFm.KodeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  //cxDBVerticalGrid1.SetFocus;
end;

procedure TBonKeluarBarangFm.ExitBtnClick(Sender: TObject);
begin
Release;
end;

procedure TBonKeluarBarangFm.cxDBVerticalGrid1BonBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
BonBarangQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  BonBarangQ.ExecSQL;

  BonBarangQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,BonBarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBonBarang.AsString:=BonBarangQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TBonKeluarBarangFm.cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TBonKeluarBarangFm.BtnSaveClick(Sender: TObject);
begin
try

  KodeQ.Open;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;


    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage('a');
    MasterQ.Post;
  end;
  MenuUtamaFm.Database1.StartTransaction;
except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
end;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    FormShow(self);
    //cxButtonEdit1PropertiesButtonClick(sender,0);
  except
    on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
  end;
    {MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;}
  ViewBonKeluarBarang.refresh;
  KodeQ.Refresh;
  //masterq.Refresh;
end;

procedure TBonKeluarBarangFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
   MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewBonKeluarBarangKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;

    end;


    BtnSave.Enabled:=True;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    cxDBVerticalGrid1.Enabled:=True;
    DetailBonKeluarBarangQ.Close;
    DetailBonKeluarBarangQ.ParamByName('text').AsString := MasterQKode.AsString;
    DetailBonKeluarBarangQ.Open;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TBonKeluarBarangFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);

begin
if abuttonindex=10 then
  begin
  DetailBonKeluarBarangQ.Edit;
  KodeDetailBKB.Close;
  KodeDetailBKB.Open;
  DetailBonKeluarBarangQKode.AsString:= DMFm.Fill(InttoStr(StrtoInt(KodeDetailBKBkode.AsString)+1),10,'0',True);;
  ShowMessage(KodeDetailBKBKode.AsString);
  DetailBonKeluarBarangQ.Post;

    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailBonKeluarBarangQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailBonKeluarBarangQ.CommitUpdates;
      except
      MenuUtamaFm.Database1.Rollback;
      DetailBonKeluarBarangQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
     DetailBonKeluarBarangQ.Close;
      DetailBonKeluarBarangQ.SQL.Text:=DetailBonKeluarBarangOriSQL;
      DetailBonKeluarBarangQ.ExecSQL;
      DetailBonKeluarBarangQ.Open;
    end;

    cxGrid1DBTableView1.Focused:=false;
  viewBonKeluarBarang.Refresh;
    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TBonKeluarBarangFm.cxGrid1DBTableView1BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL : string;
begin
BarangQ.Close;
tempSQL:=BarangQ.SQL.Text;
BarangQ.SQL.Clear;
BarangQ.SQL.Add('select distinct Barang.* from Barang,DetailBonBarang where barang.Kode=DetailBonBarang.KodeBarang and DetailBonBarang.KodeBonBarang='+QuotedStr(MasterQBonBarang.AsString));
//BarangQ.SQL.Text:='select distinct Barang.* from Barang,DetailBonBarang where barang.Kode=DetailBonBarang.KodeBarang and DetailBonBarang.KodeBonBarang=';
BarangQ.Open;
DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
    DetailBonKeluarBarangQ.Close;
    DetailBonKeluarBarangQ.Open;
    DetailBonKeluarBarangQ.Insert;
     CobaQ.Close;
      CobaQ.ParamByName('text').AsString := BarangQKode.AsString;
      //CobaQ.ParamByName('text').AsString := DetailBonBarangQKodeBarang.AsString;
      //ShowMessage(CobaQ.ParamByName('text').AsString );
      //CobaQ.ParamByName('text2').AsString:=DetailBonBarangQKodeBonBarang.AsString;
     CobaQ.Open;
 
    DetailBonKeluarBarangQBarang.AsString:=BarangQKode.AsString;
    DetailBonKeluarBarangQKodeBKB.AsString:=MasterQBonBarang.AsString;
    DetailBonKeluarBarangQDiminta.AsInteger:= CobaQDiminta.AsInteger;

    end;
        DropDownFm.Release;
    BarangQ.SQL.Text:=tempSQL;
end;

procedure TBonKeluarBarangFm.BtnDeleteClick(Sender: TObject);
begin
  if MessageDlg('Hapus BKB '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
     DeleteDBKBQ.Close;
     DeleteDBKBQ.ParamByName('text').AsString := MasterQKode.AsString;
     DeleteDBKBQ.Open;
     DeleteDBKBQ.First;
     while DeleteDBKBQ.Eof = FALSE
     do
     begin
       SDQuery1.SQL.Text:=('delete from DetailBKB where kodeBKB='+QuotedStr(KodeEdit.Text)+' and barang='+QuotedStr(DeleteDBKBQBarang.AsString));
       SDQuery1.ExecSQL;
       DeleteDBKBQ.Next;
     end;
     //DeleteDBKBQ.Delete;
     //DeleteDBKBQ.ApplyUpdates;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('BKB telah dihapus.',mtInformation,[mbOK],0);
       DeleteDBKBQ.Close;
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('BKB pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     ViewBonKeluarBarang.Close;
  ViewBonKeluarBarang.ExecSQL;
  ViewBonKeluarBarang.Open;
  DetailBonKeluarBarangQ.Close;
  end;
end;

end.
