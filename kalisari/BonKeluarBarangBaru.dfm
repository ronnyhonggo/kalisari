object BonKeluarBarangBaruFm: TBonKeluarBarangBaruFm
  Left = 250
  Top = 73
  Width = 913
  Height = 555
  Caption = 'BonKeluarBarangBaruFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 330
    Width = 897
    Height = 168
    Align = alBottom
    Caption = 'Panel2'
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 910
      Height = 112
      Align = alCustom
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 88
        end
        object cxGrid2DBTableView1BonBarang: TcxGridDBColumn
          DataBinding.FieldName = 'BonBarang'
          Width = 87
        end
        object cxGrid2DBTableView1NamaPenerima: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenerima'
          Options.SortByDisplayText = isbtOn
        end
        object cxGrid2DBTableView1TglKeluar: TcxGridDBColumn
          DataBinding.FieldName = 'TglKeluar'
          Width = 214
        end
        object cxGrid2DBTableView1Rekanan: TcxGridDBColumn
          DataBinding.FieldName = 'Rekanan'
          Width = 75
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object BtnSave: TButton
      Left = 24
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BtnSaveClick
    end
    object BtnDelete: TButton
      Left = 136
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Delete BKB'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtnDeleteClick
    end
    object Button2: TButton
      Left = 247
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = Button2Click
    end
    object ExitBtn: TButton
      Left = 359
      Top = 119
      Width = 105
      Height = 41
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = ExitBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 599
      Top = 112
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 55
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 498
    Width = 897
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 377
    Height = 330
    Align = alLeft
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 375
      Height = 41
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 104
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Kode'
      end
      object Label2: TLabel
        Left = 8
        Top = 12
        Width = 24
        Height = 13
        Caption = 'Kode'
      end
      object KodeEdit: TcxButtonEdit
        Left = 40
        Top = 10
        Properties.Buttons = <
          item
            Caption = '+'
            Default = True
            Kind = bkText
          end>
        Properties.OnButtonClick = KodeEditPropertiesButtonClick
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebs3D
        Style.HotTrack = False
        Style.ButtonStyle = bts3D
        TabOrder = 0
        OnEnter = KodeEditEnter
        OnExit = KodeEditExit
        OnKeyDown = KodeEditKeyDown
        Width = 121
      end
      object cxCheckBox1: TcxCheckBox
        Left = 207
        Top = 10
        Caption = 'Permintaan Barang Umum'
        Enabled = False
        TabOrder = 1
        Transparent = True
        OnClick = cxCheckBox1Click
        Width = 144
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 42
      Width = 375
      Height = 287
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 138
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 1
      DataController.DataSource = MasterSource
      Version = 1
      object cxDBVerticalGrid1Rekanan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1RekananEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Rekanan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NamaRekanan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaRekanan'
        Properties.Options.Editing = False
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1BonBarang: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1BonBarangEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'BonBarang'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Penerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penerima'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1NamaPenerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenerima'
        Properties.Options.Editing = False
        ID = 4
        ParentID = 3
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPenerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPenerima'
        Properties.Options.Editing = False
        ID = 5
        ParentID = 3
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1TglKeluar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglKeluar'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 6
        ParentID = -1
        Index = 3
        Version = 1
      end
    end
  end
  object Panel4: TPanel
    Left = 377
    Top = 0
    Width = 520
    Height = 330
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 518
      Height = 328
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.Appending = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Barang: TcxGridDBColumn
          DataBinding.FieldName = 'Barang'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1BarangPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Options.Editing = False
          Width = 153
        end
        object cxGrid1DBTableView1TotalDiminta: TcxGridDBColumn
          DataBinding.FieldName = 'TotalDiminta'
          Options.Editing = False
        end
        object cxGrid1DBTableView1SisaDiminta: TcxGridDBColumn
          DataBinding.FieldName = 'SisaDiminta'
          Options.Editing = False
        end
        object cxGrid1DBTableView1JumlahKeluar: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahKeluar'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.OnValidate = cxGrid1DBTableView1JumlahKeluarPropertiesValidate
          Width = 69
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 155
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BonKeluarBarang')
    UpdateObject = MasterUpdate
    Left = 176
    Top = 80
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBonBarang: TStringField
      FieldName = 'BonBarang'
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQTglKeluar: TDateTimeField
      FieldName = 'TglKeluar'
      Required = True
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQRekanan: TStringField
      FieldName = 'Rekanan'
      Size = 10
    end
    object MasterQNamaRekanan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaRekanan'
      LookupDataSet = RekananQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Rekanan'
      Lookup = True
    end
  end
  object MasterSource: TDataSource
    DataSet = MasterQ
    Left = 208
    Top = 48
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Rekanan, BonBarang, Penerima, TglKeluar, CreateDate' +
        ', CreateBy, TglEntry, Operator'
      'from BonKeluarBarang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update BonKeluarBarang'
      'set'
      '  Kode = :Kode,'
      '  Rekanan = :Rekanan,'
      '  BonBarang = :BonBarang,'
      '  Penerima = :Penerima,'
      '  TglKeluar = :TglKeluar,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into BonKeluarBarang'
      
        '  (Kode, Rekanan, BonBarang, Penerima, TglKeluar, CreateDate, Cr' +
        'eateBy, TglEntry, Operator)'
      'values'
      
        '  (:Kode, :Rekanan, :BonBarang, :Penerima, :TglKeluar, :CreateDa' +
        'te, :CreateBy, :TglEntry, :Operator)')
    DeleteSQL.Strings = (
      'delete from BonKeluarBarang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 240
    Top = 48
  end
  object viewBonKeluarBarang: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select cast(TglKeluar as date) as Tanggal,* '
      'from bonkeluarbarang'
      
        'where (TglKeluar>=:text1 and TglKeluar<=:text2) or TglKeluar is ' +
        'null'
      'order by tglentry desc')
    Left = 792
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewBonKeluarBarangKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object viewBonKeluarBarangTglKeluar: TDateTimeField
      FieldName = 'TglKeluar'
      Required = True
    end
    object viewBonKeluarBarangCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewBonKeluarBarangCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewBonKeluarBarangTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewBonKeluarBarangOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewBonKeluarBarangNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object viewBonKeluarBarangRekanan: TStringField
      FieldName = 'Rekanan'
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = viewBonKeluarBarang
    Left = 824
    Top = 448
  end
  object DetailBonKeluarBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *, cast(0 as numeric(18, 4)) as TotalDiminta,cast(0 as nu' +
        'meric(18, 4)) as SisaDiminta'
      'from DetailBKB where KodeBKB=:text')
    UpdateObject = DetailBonKeluarBarangUpdate
    Left = 576
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailBonKeluarBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQKodeBKB: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQBarang: TStringField
      FieldName = 'Barang'
      LookupCache = True
      Required = True
      Size = 10
    end
    object DetailBonKeluarBarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailBonKeluarBarangQDiminta: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Diminta'
      Calculated = True
    end
    object DetailBonKeluarBarangQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object DetailBonKeluarBarangQJumlahKeluar: TFloatField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object DetailBonKeluarBarangQTotalDiminta: TFloatField
      FieldName = 'TotalDiminta'
    end
    object DetailBonKeluarBarangQSisaDiminta: TFloatField
      FieldName = 'SisaDiminta'
    end
  end
  object DataSource2: TDataSource
    DataSet = DetailBonKeluarBarangQ
    Left = 608
    Top = 288
  end
  object DetailBonKeluarBarangUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, KodeBKB, Barang, JumlahKeluar, Keterangan'#13#10'from Det' +
        'ailBKB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update DetailBKB'
      'set'
      '  Kode = :Kode,'
      '  KodeBKB = :KodeBKB,'
      '  Barang = :Barang,'
      '  JumlahKeluar = :JumlahKeluar,'
      '  Keterangan = :Keterangan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into DetailBKB'
      '  (Kode, KodeBKB, Barang, JumlahKeluar, Keterangan)'
      'values'
      '  (:Kode, :KodeBKB, :Barang, :JumlahKeluar, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailBKB'
      'where'
      '  Kode = :OLD_Kode')
    Left = 640
    Top = 280
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from BonKeluarBarang order by kode desc')
    Left = 296
    Top = 56
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object BonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Filtered = True
    SQL.Strings = (
      'select * from BonBarang  where status='#39'ON PROCESS'#39)
    Left = 296
    Top = 88
    object BonBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BonBarangQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Visible = False
      Size = 10
    end
    object BonBarangQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object BonBarangQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Visible = False
      Size = 10
    end
    object BonBarangQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object BonBarangQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object BonBarangQPenerima: TStringField
      FieldName = 'Penerima'
      Visible = False
      Size = 10
    end
    object BonBarangQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object BonBarangQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object BonBarangQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object BonBarangQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object BonBarangQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object BonBarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object BonBarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object BonBarangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object BonBarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object BonBarangQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object BonBarangQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 296
    Top = 120
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Filtered = True
    SQL.Strings = (
      'select * from Barang')
    Left = 296
    Top = 152
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object KodeDetailBKB: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from detailBKB order by kode desc')
    Left = 528
    Top = 280
    object KodeDetailBKBkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DeleteDBKBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailBKB dbkb where dbkb.KodeBKB=:text')
    Left = 408
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDBKBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DeleteDBKBQKodeBKB: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object DeleteDBKBQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DeleteDBKBQJumlahKeluar: TIntegerField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object DeleteDBKBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DeleteDBKBUpd: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, KodeBKB, Barang, JumlahKeluar, Keterangan'
      'from DetailBKB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update DetailBKB'
      'set'
      '  Kode = :Kode,'
      '  KodeBKB = :KodeBKB,'
      '  Barang = :Barang,'
      '  JumlahKeluar = :JumlahKeluar,'
      '  Keterangan = :Keterangan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into DetailBKB'
      '  (Kode, KodeBKB, Barang, JumlahKeluar, Keterangan)'
      'values'
      '  (:Kode, :KodeBKB, :Barang, :JumlahKeluar, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailBKB'
      'where'
      '  KodeBKB = :OLD_KodeBKB'
      ' ')
    Left = 336
    Top = 48
  end
  object CobaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select distinct Diminta=(select sum(JumlahDiminta) from DetailBo' +
        'nBarang where KodeBarang=:text and StatusMinta='#39'ON PROCESS'#39'),'
      
        'PendingBeli=(select sum(JumlahBeli) from DetailBonBarang where K' +
        'odeBarang=:text and StatusBeli='#39'ON PROCESS'#39') '
      'from DetailBonBarang dbb, barang b')
    Left = 456
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CobaQPendingBeli: TIntegerField
      FieldName = 'PendingBeli'
    end
    object CobaQDiminta: TFloatField
      FieldName = 'Diminta'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailBKB dbkb where dbkb.KodeBKB=:text')
    UpdateObject = DeleteDBKBUpd
    Left = 408
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'KodeBKB'
      Required = True
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object HitungQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @TotalDiminta numeric(18,4);'
      'declare @TotalKeluar numeric(18,4);'
      'declare @SisaDiminta numeric(18,4);'
      
        'set @TotalDiminta=(select SUM(JumlahDiminta) from DetailBonBaran' +
        'g dbb '
      'where KodeBonBarang=:text and KodeBarang=:text2);'
      
        'set @TotalKeluar=(select SUM(JumlahKeluar) from DetailBKB dbkb, ' +
        'BonKeluarBarang bkb'
      
        'where dbkb.KodeBKB=bkb.Kode and bkb.BonBarang=:text and dbkb.Bar' +
        'ang=:text2)'
      'if @TotalKeluar is NULL'
      'begin'
      'set @TotalKeluar=0;'
      'end;'
      'set @SisaDiminta=@TotalDiminta-@TotalKeluar;'
      'select TotalDiminta=@TotalDiminta, SisaDiminta=@SisaDiminta')
    Left = 312
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object HitungQTotalDiminta: TFloatField
      FieldName = 'TotalDiminta'
    end
    object HitungQSisaDiminta: TFloatField
      FieldName = 'SisaDiminta'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 320
    Top = 264
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object KodeDBKBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select kode from DetailBKB'
      'where KodeBKB=:text ')
    Left = 208
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KodeDBKBQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object RekananQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from masterrekanan')
    Left = 176
    Top = 48
    object RekananQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RekananQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object RekananQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object RekananQNoTelp: TStringField
      FieldName = 'NoTelp'
    end
    object RekananQNamaPIC: TStringField
      FieldName = 'NamaPIC'
      Size = 50
    end
    object RekananQNoTelpPIC: TStringField
      FieldName = 'NoTelpPIC'
    end
  end
end
