unit BonKeluarBarangBaru;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, cxVGrid, cxDBVGrid, cxInplaceContainer,
  SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit, ComCtrls, ExtCtrls,
  cxCheckBox, cxLabel, cxDropDownEdit, cxCalendar, cxGroupBox;

type
  TBonKeluarBarangBaruFm = class(TForm)
    Panel2: TPanel;
    StatusBar: TStatusBar;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQBonBarang: TStringField;
    MasterQPenerima: TStringField;
    MasterQTglKeluar: TDateTimeField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterSource: TDataSource;
    MasterUpdate: TSDUpdateSQL;
    viewBonKeluarBarang: TSDQuery;
    DataSource1: TDataSource;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    BtnSave: TButton;
    BtnDelete: TButton;
    Button2: TButton;
    DetailBonKeluarBarangQ: TSDQuery;
    DataSource2: TDataSource;
    DetailBonKeluarBarangUpdate: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TButton;
    BonBarangQ: TSDQuery;
    BonBarangQKode: TStringField;
    BonBarangQPeminta: TStringField;
    BonBarangQPenyetuju: TStringField;
    BonBarangQPenerima: TStringField;
    BonBarangQLaporanPerbaikan: TStringField;
    BonBarangQLaporanPerawatan: TStringField;
    BonBarangQStoring: TStringField;
    BonBarangQSPK: TStringField;
    BonBarangQStatus: TStringField;
    BonBarangQCreateDate: TDateTimeField;
    BonBarangQCreateBy: TStringField;
    BonBarangQOperator: TStringField;
    BonBarangQTglEntry: TDateTimeField;
    BonBarangQArmada: TStringField;
    BonBarangQTglCetak: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    viewBonKeluarBarangKode: TStringField;
    viewBonKeluarBarangBonBarang: TStringField;
    viewBonKeluarBarangPenerima: TStringField;
    viewBonKeluarBarangTglKeluar: TDateTimeField;
    viewBonKeluarBarangCreateDate: TDateTimeField;
    viewBonKeluarBarangCreateBy: TStringField;
    viewBonKeluarBarangTglEntry: TDateTimeField;
    viewBonKeluarBarangOperator: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid2DBTableView1TglKeluar: TcxGridDBColumn;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    KodeDetailBKB: TSDQuery;
    KodeDetailBKBkode: TStringField;
    DeleteDBKBQ: TSDQuery;
    DeleteDBKBUpd: TSDUpdateSQL;
    DeleteDBKBQKode: TStringField;
    DeleteDBKBQKodeBKB: TStringField;
    DeleteDBKBQBarang: TStringField;
    DeleteDBKBQJumlahKeluar: TIntegerField;
    DeleteDBKBQKeterangan: TMemoField;
    CobaQ: TSDQuery;
    CobaQPendingBeli: TIntegerField;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField1: TIntegerField;
    MemoField1: TMemoField;
    DetailBonKeluarBarangQKode: TStringField;
    DetailBonKeluarBarangQKodeBKB: TStringField;
    DetailBonKeluarBarangQBarang: TStringField;
    DetailBonKeluarBarangQKeterangan: TMemoField;
    DetailBonKeluarBarangQDiminta: TIntegerField;
    HitungQ: TSDQuery;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    DetailBonKeluarBarangQNamaBarang: TStringField;
    viewBonKeluarBarangNamaPenerima: TStringField;
    cxGrid2DBTableView1NamaPenerima: TcxGridDBColumn;
    BonBarangQNamaPeminta: TStringField;
    BonBarangQNamaPenyetuju: TStringField;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    BonBarangQNoBody: TStringField;
    KodeDBKBQ: TSDQuery;
    KodeDBKBQkode: TStringField;
    lbl1: TLabel;
    Panel1: TPanel;
    Panel3: TPanel;
    KodeEdit: TcxButtonEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1BonBarang: TcxDBEditorRow;
    cxDBVerticalGrid1Penerima: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPenerima: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPenerima: TcxDBEditorRow;
    cxDBVerticalGrid1TglKeluar: TcxDBEditorRow;
    Panel4: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1TotalDiminta: TcxGridDBColumn;
    cxGrid1DBTableView1SisaDiminta: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    MasterQRekanan: TStringField;
    RekananQ: TSDQuery;
    RekananQKode: TStringField;
    RekananQNama: TStringField;
    RekananQAlamat: TStringField;
    RekananQNoTelp: TStringField;
    RekananQNamaPIC: TStringField;
    RekananQNoTelpPIC: TStringField;
    MasterQNamaRekanan: TStringField;
    cxDBVerticalGrid1Rekanan: TcxDBEditorRow;
    cxDBVerticalGrid1NamaRekanan: TcxDBEditorRow;
    cxCheckBox1: TcxCheckBox;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    viewBonKeluarBarangRekanan: TStringField;
    cxGrid2DBTableView1Rekanan: TcxGridDBColumn;
    BarangQJumlah: TFloatField;
    DetailBonKeluarBarangQJumlahKeluar: TFloatField;
    HitungQTotalDiminta: TFloatField;
    HitungQSisaDiminta: TFloatField;
    DetailBonKeluarBarangQTotalDiminta: TFloatField;
    DetailBonKeluarBarangQSisaDiminta: TFloatField;
    cxGrid1DBTableView1JumlahKeluar: TcxGridDBColumn;
    CobaQDiminta: TFloatField;
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxDBVerticalGrid1BonBarangEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1DBTableView1BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnDeleteClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cxGrid1DBTableView1JumlahKeluarPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1RekananEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BonKeluarBarangBaruFm: TBonKeluarBarangBaruFm;
  MasterOriSQL: string;
  paramkode :string;
  DetailBonKeluarBarangOriSQL: string;
  IsiBaru: integer;

implementation

uses MenuUtama, DropDown, DM, PegawaiDropDown, BarangLPBDropDown, BonBarangDropDown,
  LookupRekanan;

{$R *.dfm}

procedure TBonKeluarBarangBaruFm.KodeEditEnter(Sender: TObject);
begin
 KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  BtnSave.Enabled:=false;
 // BtnKepalaTeknik.Enabled:=false;
  //BtnSetuju.Enabled:=false;
  BtnDelete.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
  DetailBonKeluarBarangQ.Close;
end;

procedure TBonKeluarBarangBaruFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
   MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      BtnDelete.Enabled:=menuutamafm.UserQDeleteBonKeluarBarang.AsBoolean;
    end;
    BtnSave.Enabled:=menuutamafm.UserQUpdateBonKeluarBarang.AsBoolean;
    cxDBVerticalGrid1.Enabled:=true;
    DetailBonKeluarBarangQ.Close;
    DetailBonKeluarBarangQ.SQL.Text:=DetailBonKeluarBarangOriSQL;
    DetailBonKeluarBarangQ.ParamByName('text').AsString:='';
    DetailBonKeluarBarangQ.Open;
    DetailBonKeluarBarangQ.Close;
    IsiBaru:=1;
  end;
  cxCheckBox1.Enabled:=true;
  cxDBVerticalGrid1BonBarang.Visible:=true;
end;

procedure TBonKeluarBarangBaruFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailBonKeluarBarangOriSQL:=DetailBonKeluarBarangQ.SQL.Text;
  //DetailBonBarangOriSQL:=DetailBonBarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TBonKeluarBarangBaruFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    release;
end;

procedure TBonKeluarBarangBaruFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewBonKeluarBarang.Close;
  viewBonKeluarBarang.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewBonKeluarBarang.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewBonKeluarBarang.Open;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertBonKeluarBarang.AsBoolean;
  BtnSave.Enabled:=menuutamafm.UserQUpdateBonKeluarBarang.AsBoolean;
  BtnDelete.Enabled:=menuutamafm.UserQDeleteBonKeluarBarang.AsBoolean;
  //RbtSPK.Checked:=TRUE;

end;

procedure TBonKeluarBarangBaruFm.KodeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TBonKeluarBarangBaruFm.KodeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  //cxDBVerticalGrid1.SetFocus;
end;

procedure TBonKeluarBarangBaruFm.ExitBtnClick(Sender: TObject);
begin
Release;
end;

procedure TBonKeluarBarangBaruFm.cxDBVerticalGrid1BonBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BonBarangDropDownFm:=TBonBarangDropdownfm.Create(Self);
    if BonBarangDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBonBarang.AsString:=BonBarangDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    BonBarangDropDownFm.Release;
end;

procedure TBonKeluarBarangBaruFm.cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open; }
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TBonKeluarBarangBaruFm.BtnSaveClick(Sender: TObject);
var kode : integer;
var kodes : string;
begin
kode:=-1;
try
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
  MasterQ.Edit;
  KodeQ.Open;

    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage('a');
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end; //untuk if mode:entry

except
  on E : Exception do
  ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailBonKeluarBarangQ.Open;
    DetailBonKeluarBarangQ.Edit;
    DetailBonKeluarBarangQ.First;

    while DetailBonKeluarBarangQ.Eof=FALSE do
    begin

           KodeDetailBKB.Close;
        KodeDetailBKB.Open;
        if kode=-1 then
        begin
         if IsiBaru=1 then
         begin
            if KodeDetailBKBkode.AsString <> '' then
            begin
              kode:=strtoint(KodeDetailBKBkode.AsString);
            end

            else
            begin
              kode:=0;
            end;
         end  //end insert baru

         else
         begin
          KodeDBKBQ.Close;
          KodeDBKBQ.ParamByName('text').AsString:=KodeEdit.Text;
          //KodeDBKBQ.ParamByName('text2').AsString:=DetailBonKeluarBarangQBarang.AsString;
          KodeDBKBQ.Open;
          //ShowMessage(inttostr(IsiBaru));
             kode:=strtoint(KodeDBKBQkode.AsString)-1;
         end;

    end

        else
        begin
            kode:=kode+1;
            //kode:=kode;
        end;

        kodes:=inttostr(kode);
        KodeDetailBKB.Close;
        DetailBonKeluarBarangQ.Edit;
        DetailBonKeluarBarangQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(kodes)+1),10,'0',True);
        DetailBonKeluarBarangQKodeBKB.AsString:=KodeEdit.Text;
        DetailBonKeluarBarangQ.Post;
        DetailBonKeluarBarangQ.Next;
    end;

    DetailBonKeluarBarangQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailBonKeluarBarangQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    //FormShow(self);
    //cxButtonEdit1PropertiesButtonClick(sender,0);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailBonKeluarBarangQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
    //MenuUtamaFm.Database1.Rollback;
   // MasterQ.RollbackUpdates;
    //ShowMessage('Penyimpanan Gagal');
  //end;
  KodeEdit.SetFocus;
  viewBonKeluarBarang.Close;
  viewBonKeluarBarang.Open;
  DetailBonKeluarBarangQ.Close;
  DetailBonKeluarBarangQ.SQL.Text:=DetailBonKeluarBarangOriSQL;
  DetailBonKeluarBarangQ.ParamByName('text').AsString:='';
  DetailBonKeluarBarangQ.Open;
  DetailBonKeluarBarangQ.Close;
  HitungQ.Close;

end;

procedure TBonKeluarBarangBaruFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  IsiBaru:=0;
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewBonKeluarBarangKode.AsString+'%'));
  MasterQ.Open;
  if MasterQ.IsEmpty then
  begin
    StatusBar.Panels[0].Text:= 'Mode : Entry';
    MasterQ.Append;
    MasterQ.Edit;
  end
  else
  begin
    StatusBar.Panels[0].Text:= 'Mode : Edit';
    MasterQ.Edit;
    BtnDelete.Enabled:=menuutamafm.UserQDeleteBonKeluarBarang.AsBoolean;
  end;
  BtnSave.Enabled:=menuutamafm.UserQUpdateBonKeluarBarang.AsBoolean;
  cxDBVerticalGrid1.Enabled:=True;

  DetailBonKeluarBarangQ.Close;
  BonBarangQ.Open;
  PegawaiQ.Open;
  BarangQ.Open;
  DeleteDBKBQ.Close;
  SDQuery1.Close;
  CobaQ.Close;
  HitungQ.Close;
  DetailBonKeluarBarangQ.ParamByName('text').AsString:=MasterQKode.AsString;
  DetailBonKeluarBarangQ.Open;
  DetailBonKeluarBarangQ.Edit;
  DetailBonKeluarBarangQ.First;
  while DetailBonKeluarBarangQ.Eof= false do
  begin
    HitungQ.Close;
    HitungQ.ParamByName('text').AsString:=MasterQBonBarang.AsString;
    HitungQ.ParamByName('text2').AsString:=DetailBonKeluarBarangQBarang.AsString;
    HitungQ.Open;

    DetailBonKeluarBarangQ.Edit;
    if cxCheckBox1.Checked=true then begin
      DetailBonKeluarBarangQTotalDiminta.AsFloat:=999;
      DetailBonKeluarBarangQSisaDiminta.AsFloat:=999;
    end
    else begin
      DetailBonKeluarBarangQTotalDiminta.AsFloat:=HitungQTotalDiminta.AsFloat;
      DetailBonKeluarBarangQSisaDiminta.AsFloat:=HitungQSisaDiminta.AsFloat;
    end;
    DetailBonKeluarBarangQ.Next;
  end;
  KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TBonKeluarBarangBaruFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);

begin
if abuttonindex=10 then
  begin
  DetailBonKeluarBarangQ.Edit;
  {KodeDetailBKB.Close;
  KodeDetailBKB.Open;
  DetailBonKeluarBarangQKode.AsString:= DMFm.Fill(InttoStr(StrtoInt(KodeDetailBKBkode.AsString)+1),10,'0',True);;
  ShowMessage(KodeDetailBKBKode.AsString);  }
  DetailBonKeluarBarangQKode.AsString:='0';
  DetailBonKeluarBarangQ.Post;

   { MenuUtamaFm.Database1.StartTransaction;
    try
      DetailBonKeluarBarangQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailBonKeluarBarangQ.CommitUpdates;
      except
      MenuUtamaFm.Database1.Rollback;
      DetailBonKeluarBarangQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
     DetailBonKeluarBarangQ.Close;
      DetailBonKeluarBarangQ.SQL.Text:=DetailBonKeluarBarangOriSQL;
      DetailBonKeluarBarangQ.ExecSQL;
      DetailBonKeluarBarangQ.Open;
    end;  }

    cxGrid1DBTableView1.Focused:=false;
  //viewBonKeluarBarang.Refresh;
    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TBonKeluarBarangBaruFm.cxGrid1DBTableView1BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL,q : string;
begin
  if cxCheckBox1.Checked=true then begin
    q:='select Barang.* from Barang,KategoriBarang where barang.kategori=KategoriBarang.Kode and upper(KategoriBarang.Kategori)=' + QuotedStr('UMUM');
  end
  else begin
    q:='select Barang.* from Barang,DetailBonBarang where barang.Kode=DetailBonBarang.KodeBarang and DetailBonBarang.KodeBonBarang='+QuotedStr(MasterQBonBarang.AsString);
  end;

  BarangLPBDropDownFm:=TBarangLPBDropDownFm.Create(Self,q);
  if BarangLPBDropDownFm.ShowModal=MrOK then
  begin
    DetailBonKeluarBarangQ.Open;
    DetailBonKeluarBarangQ.Insert;
    DetailBonKeluarBarangQ.Edit;
    DetailBonKeluarBarangQBarang.AsString:=BarangLPBDropDownFm.kode;

    HitungQ.Close;
    HitungQ.ParamByName('text').AsString:=MasterQBonBarang.AsString;
    HitungQ.ParamByName('text2').AsString:=BarangLPBDropDownFm.kode;
    HitungQ.Open;

    DetailBonKeluarBarangQKodeBKB.AsString:=MasterQBonBarang.AsString;
    if cxCheckBox1.Checked=true then begin
      DetailBonKeluarBarangQTotalDiminta.AsFloat:=999;
      DetailBonKeluarBarangQSisaDiminta.AsFloat:=999;
    end
    else begin
      DetailBonKeluarBarangQTotalDiminta.AsFloat:=HitungQTotalDiminta.AsFloat;
      DetailBonKeluarBarangQSisaDiminta.AsFloat:=HitungQSisaDiminta.AsFloat;
    end;
  end;
  BarangLPBDropDownFm.Release;
end;

procedure TBonKeluarBarangBaruFm.BtnDeleteClick(Sender: TObject);
begin
  if MessageDlg('Hapus BKB '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
     DeleteDBKBQ.Close;
     DeleteDBKBQ.ParamByName('text').AsString := MasterQKode.AsString;
     DeleteDBKBQ.Open;
     DeleteDBKBQ.First;
     while DeleteDBKBQ.Eof = FALSE
     do
     begin
       SDQuery1.SQL.Text:=('delete from DetailBKB where kodeBKB='+QuotedStr(KodeEdit.Text)+' and barang='+QuotedStr(DeleteDBKBQBarang.AsString));
       SDQuery1.ExecSQL;
       DeleteDBKBQ.Next;
     end;
     //DeleteDBKBQ.Delete;
     //DeleteDBKBQ.ApplyUpdates;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('BKB telah dihapus.',mtInformation,[mbOK],0);
       DeleteDBKBQ.Close;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('BKB pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     ViewBonKeluarBarang.Close;
  ViewBonKeluarBarang.ExecSQL;
  ViewBonKeluarBarang.Open;
  DetailBonKeluarBarangQ.Close;
  end;
end;

procedure TBonKeluarBarangBaruFm.Button2Click(Sender: TObject);
begin
  viewBonKeluarBarang.Close;
  viewBonKeluarBarang.Open;
end;

procedure TBonKeluarBarangBaruFm.cxGrid1DBTableView1JumlahKeluarPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailBonKeluarBarangQJumlahKeluar.AsFloat:=DisplayValue;
  if DetailBonKeluarBarangQJumlahKeluar.AsFloat>DetailBonKeluarBarangQSisaDiminta.AsFloat then
  begin
     ShowMessage('Jumlah keluar harus lebih kecil dari sisa diminta!');
     DetailBonKeluarBarangQJumlahKeluar.AsFloat:=DetailBonKeluarBarangQSisaDiminta.AsFloat;
  end;
end;

procedure TBonKeluarBarangBaruFm.cxDBVerticalGrid1RekananEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LookupRekananFm:=TLookupRekananFm.Create(self);
  if LookupRekananFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQRekanan.AsString:= LookupRekananFm.kode;
    //cxDBVerticalGrid1BonBarang.Visible:=false;
    MasterQBonBarang.AsVariant:=null;
    cxGrid1DBTableView1Barang.Options.Editing:=true;
  end;
  LookupRekananFm.Release;
end;

procedure TBonKeluarBarangBaruFm.cxCheckBox1Click(Sender: TObject);
begin
  if cxCheckBox1.Checked=true then begin
    cxDBVerticalGrid1Rekanan.Visible:=false;
    cxDBVerticalGrid1BonBarang.Visible:=false;
    MasterQRekanan.AsVariant:=null;
    MasterQBonBarang.AsVariant:=null;
    //cxGrid1DBTableView1Barang.Options.Editing:=true;
  end
  else begin
    cxDBVerticalGrid1Rekanan.Visible:=true;
    cxDBVerticalGrid1BonBarang.Visible:=true;
    MasterQRekanan.AsVariant:=null;
    MasterQBonBarang.AsVariant:=null;
    //cxGrid1DBTableView1Barang.Options.Editing:=false;
  end;
end;

procedure TBonKeluarBarangBaruFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  viewBonKeluarBarang.Close;
  viewBonKeluarBarang.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewBonKeluarBarang.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewBonKeluarBarang.Open;
end;

procedure TBonKeluarBarangBaruFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  viewBonKeluarBarang.Close;
  viewBonKeluarBarang.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewBonKeluarBarang.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewBonKeluarBarang.Open;
end;

end.
