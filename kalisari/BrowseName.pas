unit BrowseName;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, SDEngine,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, StdCtrls, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TBrowseNameFm = class(TForm)
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    BrowseQ: TSDQuery;
    DataSourceBrowse: TDataSource;
    Edit1: TEdit;
    Button1: TButton;
    Label1: TLabel;
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    satuan:String;
    tipe:String;
  end;

var
  BrowseNameFm: TBrowseNameFm;

implementation
uses bonbarang,menuutama;

{$R *.dfm}

procedure TBrowseNameFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin

  MenuUtamaFm.param_kode:= BrowseQ.Fields[0].AsString;
  MenuUtamaFm.param_nama:= BrowseQ.Fields[1].AsString;
  MenuUtamaFm.stok:=BrowseQ.Fields[2].AsInteger;
  satuan:=BrowseQ.Fields[3].AsString;
  Close;
end;

procedure TBrowseNameFm.FormActivate(Sender: TObject);
var i:Integer;
    temp: String;
begin
  MenuUtamaFm.param_kodebon:='';
    MenuUtamaFm.param_kode:='';
    MenuUtamaFm.param_nama:='';
   
  try
    if tipe='' then
      temp:='select b.kode,b.nama, b.jumlah as SisaStok, satuan,minimumstok, maximumstok,lokasi from barang b  order by b.nama'
      else
      temp:='select b.kode,b.nama, b.jumlah as SisaStok, satuan,minimumstok, maximumstok,lokasi from barang b where b.jumlah<=b.minimumstok order by b.nama';

    BrowseQ.SQL.Text:=temp;
    DataSourceBrowse.DataSet:=BrowseQ;
    cxGridDBTableView1.DataController.DataSource:=DataSourceBrowse;

    BrowseQ.Open;
    cxGridDBTableView1.ClearItems;
    for i:=0 to DataSourceBrowse.DataSet.FieldCount-1 do
    begin
      cxGridDBTableView1.CreateColumn.DataBinding.FieldName:=DataSourceBrowse.DataSet.Fields[i].FieldName;
      cxGridDBTableView1.GetColumnByFieldName(DataSourceBrowse.DataSet.Fields[i].FieldName).Options.Editing:=true;
    end;

  except
    ShowMessage('gagal');
  end
end;

procedure TBrowseNameFm.Button1Click(Sender: TObject);
var temp:String;
i:integer;
begin
  if tipe='' then
    temp:='select b.kode,b.nama, b.jumlah as SisaStok, satuan,minimumstok, maximumstok,lokasi from barang b where b.nama like '+QuotedStr('%'+Edit1.Text+'%') +' order by b.nama'
    else
    temp:='select b.kode,b.nama, b.jumlah as SisaStok, satuan,minimumstok, maximumstok,lokasi from barang b where b.jumlah<=b.minimumstok and b.nama like '+QuotedStr('%'+Edit1.Text+'%') +' order by b.nama';

    BrowseQ.SQL.Text:=temp;
    DataSourceBrowse.DataSet:=BrowseQ;
    cxGridDBTableView1.DataController.DataSource:=DataSourceBrowse;

    BrowseQ.Open;
    cxGridDBTableView1.ClearItems;
    for i:=0 to DataSourceBrowse.DataSet.FieldCount-1 do
    begin
      cxGridDBTableView1.CreateColumn.DataBinding.FieldName:=DataSourceBrowse.DataSet.Fields[i].FieldName;
      cxGridDBTableView1.GetColumnByFieldName(DataSourceBrowse.DataSet.Fields[i].FieldName).Options.Editing:=true;
    end;
end;

end.
