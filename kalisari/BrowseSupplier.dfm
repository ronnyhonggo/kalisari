object BrowseSupplierFm: TBrowseSupplierFm
  Left = 250
  Top = 31
  Width = 865
  Height = 705
  Caption = 'BrowseSupplierFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 243
    Height = 16
    Caption = 'Daftar Supplier yang direkomendasikan :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 216
    Width = 145
    Height = 16
    Caption = 'Daftar Supplier Lainnya :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 32
    Width = 849
    Height = 169
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsSelection.CellSelect = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 74
      end
      object cxGrid1DBTableView1NamaToko: TcxGridDBColumn
        DataBinding.FieldName = 'NamaToko'
        Width = 119
      end
      object cxGrid1DBTableView1Kategori: TcxGridDBColumn
        DataBinding.FieldName = 'Kategori'
        Width = 77
      end
      object cxGrid1DBTableView1Alamat: TcxGridDBColumn
        DataBinding.FieldName = 'Alamat'
        Width = 164
      end
      object cxGrid1DBTableView1NoTelp: TcxGridDBColumn
        DataBinding.FieldName = 'NoTelp'
        Width = 119
      end
      object cxGrid1DBTableView1NamaPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPIC1'
        Width = 93
      end
      object cxGrid1DBTableView1TelpPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'TelpPIC1'
      end
      object cxGrid1DBTableView1JabatanPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'JabatanPIC1'
        Width = 77
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 249
    Width = 849
    Height = 417
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGridDBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGridDBTableView1CellDblClick
      DataController.DataSource = DataSource2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsSelection.CellSelect = False
      object cxGridDBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 78
      end
      object cxGridDBTableView1NamaToko: TcxGridDBColumn
        DataBinding.FieldName = 'NamaToko'
        Width = 116
      end
      object cxGridDBTableView1Kategori: TcxGridDBColumn
        DataBinding.FieldName = 'Kategori'
        Width = 82
      end
      object cxGridDBTableView1Alamat: TcxGridDBColumn
        DataBinding.FieldName = 'Alamat'
        Width = 163
      end
      object cxGridDBTableView1NoTelp: TcxGridDBColumn
        DataBinding.FieldName = 'NoTelp'
        Width = 120
      end
      object cxGridDBTableView1NamaPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPIC1'
        Width = 91
      end
      object cxGridDBTableView1TelpPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'TelpPIC1'
      end
      object cxGridDBTableView1JabatanPIC1: TcxGridDBColumn
        DataBinding.FieldName = 'JabatanPIC1'
        Width = 74
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object Edit1: TEdit
    Left = 168
    Top = 216
    Width = 145
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 320
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Cari'
    TabOrder = 3
    OnClick = Button1Click
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select s.*, '#39'Rekomendasi'#39' as status'
      'from supplier s, listsupplier l'
      'where s.kode=l.kodesupplier and l.kodebarang=:kodebarang')
    Left = 224
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodebarang'
        ParamType = ptInput
      end>
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1NamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SDQuery1Alamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SDQuery1NoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SDQuery1Kategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SDQuery1NamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SDQuery1TelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SDQuery1JabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SDQuery1NamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SDQuery1TelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SDQuery1JabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SDQuery1NamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SDQuery1TelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SDQuery1JabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SDQuery1CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery1CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery1Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery1TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery1status: TStringField
      FieldName = 'status'
      Required = True
      Size = 11
    end
  end
  object SDQuery2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *'
      'from supplier '
      'where kode not in('
      'select kode'
      'from supplier s, listsupplier l'
      'where s.kode=l.kodesupplier and l.kodebarang=:kodebarang'
      ')')
    Left = 240
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodebarang'
        ParamType = ptInput
      end>
    object SDQuery2Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery2NamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SDQuery2Alamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SDQuery2NoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SDQuery2Kategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SDQuery2NamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SDQuery2TelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SDQuery2JabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SDQuery2NamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SDQuery2TelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SDQuery2JabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SDQuery2NamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SDQuery2TelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SDQuery2JabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SDQuery2CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery2CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery2Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery2TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 256
    Top = 80
  end
  object DataSource2: TDataSource
    DataSet = SDQuery2
    Left = 272
    Top = 360
  end
end
