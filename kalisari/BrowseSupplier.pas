unit BrowseSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  SDEngine;

type
  TBrowseSupplierFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    SDQuery1: TSDQuery;
    SDQuery2: TSDQuery;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    SDQuery1Kode: TStringField;
    SDQuery1NamaToko: TStringField;
    SDQuery1Alamat: TStringField;
    SDQuery1NoTelp: TStringField;
    SDQuery1Kategori: TStringField;
    SDQuery1NamaPIC1: TStringField;
    SDQuery1TelpPIC1: TStringField;
    SDQuery1JabatanPIC1: TStringField;
    SDQuery1NamaPIC2: TStringField;
    SDQuery1TelpPIC2: TStringField;
    SDQuery1JabatanPIC2: TStringField;
    SDQuery1NamaPIC3: TStringField;
    SDQuery1TelpPIC3: TStringField;
    SDQuery1JabatanPIC3: TStringField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1status: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaToko: TcxGridDBColumn;
    cxGrid1DBTableView1Alamat: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelp: TcxGridDBColumn;
    cxGrid1DBTableView1Kategori: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPIC1: TcxGridDBColumn;
    cxGrid1DBTableView1TelpPIC1: TcxGridDBColumn;
    cxGrid1DBTableView1JabatanPIC1: TcxGridDBColumn;
    SDQuery2Kode: TStringField;
    SDQuery2NamaToko: TStringField;
    SDQuery2Alamat: TStringField;
    SDQuery2NoTelp: TStringField;
    SDQuery2Kategori: TStringField;
    SDQuery2NamaPIC1: TStringField;
    SDQuery2TelpPIC1: TStringField;
    SDQuery2JabatanPIC1: TStringField;
    SDQuery2NamaPIC2: TStringField;
    SDQuery2TelpPIC2: TStringField;
    SDQuery2JabatanPIC2: TStringField;
    SDQuery2NamaPIC3: TStringField;
    SDQuery2TelpPIC3: TStringField;
    SDQuery2JabatanPIC3: TStringField;
    SDQuery2CreateDate: TDateTimeField;
    SDQuery2CreateBy: TStringField;
    SDQuery2Operator: TStringField;
    SDQuery2TglEntry: TDateTimeField;
    cxGridDBTableView1Kode: TcxGridDBColumn;
    cxGridDBTableView1NamaToko: TcxGridDBColumn;
    cxGridDBTableView1Alamat: TcxGridDBColumn;
    cxGridDBTableView1NoTelp: TcxGridDBColumn;
    cxGridDBTableView1Kategori: TcxGridDBColumn;
    cxGridDBTableView1NamaPIC1: TcxGridDBColumn;
    cxGridDBTableView1TelpPIC1: TcxGridDBColumn;
    cxGridDBTableView1JabatanPIC1: TcxGridDBColumn;
    procedure FormActivate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kodebar:String;
  end;

var
  BrowseSupplierFm: TBrowseSupplierFm;

implementation
uses MenuUtama, daftarbeli;

{$R *.dfm}

procedure TBrowseSupplierFm.FormActivate(Sender: TObject);
begin
  MenuUtamaFm.param_kode:='';
  MenuUtamaFm.param_nama:='';
  MenuUtamaFm.param_kodebon:='';
    MenuUtamaFm.param_kode:='';
    MenuUtamaFm.param_nama:='';

  SDQuery1.Close;
  SDQuery1.ParamByName('kodebarang').AsString := kodebar;
  SDQuery1.ExecSQL;
  SDQuery1.Open;

  SDQuery2.Close;
  SDQuery2.ParamByName('kodebarang').AsString := kodebar;
  SDQuery2.ExecSQL;
  SDQuery2.Open;
end;

procedure TBrowseSupplierFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
 MenuUtamaFm.param_kode:= SDQuery1.Fields[0].AsString;
    MenuUtamaFm.param_nama:=SDQuery1.Fields[1].AsString;
    Close;
end;

procedure TBrowseSupplierFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    MenuUtamaFm.param_kode:= SDQuery2.Fields[0].AsString;
    MenuUtamaFm.param_nama:=SDQuery2.Fields[1].AsString;
    Close;
end;

procedure TBrowseSupplierFm.Button1Click(Sender: TObject);
var temp:String;
begin
SDQuery2.SQL.Clear;
temp:='select * from supplier where namatoko like '+ QuotedStr('%'+Edit1.Text+'%');
temp:=temp+' and kode not in(select kode from supplier s, listsupplier l where s.kode=l.kodesupplier and l.kodebarang='+kodebar+')';

SDQuery2.SQL.Add(temp);

SDQuery2.ExecSQL;
SDQuery2.Close;
SDQuery2.Open;
end;

end.
