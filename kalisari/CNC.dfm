object CNCFm: TCNCFm
  Left = 45
  Top = 57
  BorderStyle = bsDialog
  Caption = 'Cash and Carry'
  ClientHeight = 678
  ClientWidth = 1116
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SearchBtn: TcxButton
    Left = 424
    Top = 530
    Width = 57
    Height = 21
    Caption = 'Search'
    TabOrder = 0
    TabStop = False
    Visible = False
    OnClick = SearchBtnClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 659
    Width = 1116
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object pnl2: TPanel
    Left = 0
    Top = 608
    Width = 1116
    Height = 51
    Align = alBottom
    TabOrder = 8
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 976
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object cxGroupBox1: TcxGroupBox
      Left = 818
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1116
    Height = 49
    Align = alTop
    TabOrder = 9
    object Label1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object lblAudit: TLabel
      Left = 40
      Top = 32
      Width = 3
      Height = 13
    end
  end
  object KodeEdit: TcxButtonEdit
    Left = 40
    Top = 10
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Properties.OnButtonClick = KodeEditPropertiesButtonClick
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 10
    OnEnter = KodeEditEnter
    OnExit = KodeEditExit
    OnKeyDown = KodeEditKeyDown
    Width = 121
  end
  object cxButton1: TcxButton
    Left = 168
    Top = 10
    Width = 57
    Height = 21
    Caption = 'Search'
    TabOrder = 11
    TabStop = False
    Visible = False
    OnClick = SearchBtnClick
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 792
    Top = 98
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Properties.OnChange = cxButtonEdit1PropertiesChange
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 7
    Visible = False
    OnEnter = cxButtonEdit1Enter
    OnExit = cxButtonEdit1Exit
    OnKeyDown = cxButtonEdit1KeyDown
    Width = 121
  end
  object Panel1: TPanel
    Left = 369
    Top = 49
    Width = 747
    Height = 559
    Align = alClient
    TabOrder = 13
    object cxGrid3: TcxGrid
      Left = 1
      Top = 1
      Width = 745
      Height = 336
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGridDBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = True
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        OnCellClick = cxGridDBTableView1CellClick
        OnCellDblClick = cxGridDBTableView1CellDblClick
        DataController.DataSource = DataSourceCoba
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGridDBTableView1KodeDaftarBeli: TcxGridDBColumn
          Caption = 'Daftar Beli'
          DataBinding.FieldName = 'KodeDaftarBeli'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBTableView1bonbarang: TcxGridDBColumn
          Caption = 'Bon Barang'
          DataBinding.FieldName = 'bonbarang'
          Options.Editing = False
          Width = 81
        end
        object cxGridDBTableView1nama: TcxGridDBColumn
          Caption = 'Nama Barang'
          DataBinding.FieldName = 'nama'
          Options.Editing = False
          Width = 104
        end
        object cxGridDBTableView1jumlahbeli: TcxGridDBColumn
          Caption = 'Permintaan Beli'
          DataBinding.FieldName = 'jumlahbeli'
          Options.Editing = False
          Width = 103
        end
        object cxGridDBTableView1RealisasiJumlah: TcxGridDBColumn
          Caption = 'Realisasi Beli'
          DataBinding.FieldName = 'RealisasiJumlah'
          Width = 93
        end
        object cxGridDBTableView1satuan: TcxGridDBColumn
          Caption = 'Satuan'
          DataBinding.FieldName = 'satuan'
          Options.Editing = False
          Width = 55
        end
        object cxGridDBTableView1RealisasiHarga: TcxGridDBColumn
          Caption = 'Harga'
          DataBinding.FieldName = 'RealisasiHarga'
          Width = 76
        end
        object cxGridDBTableView1TglBeli: TcxGridDBColumn
          DataBinding.FieldName = 'TglBeli'
          Width = 90
        end
        object cxGridDBTableView1supplier: TcxGridDBColumn
          DataBinding.FieldName = 'supplier'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Visible = False
          Options.Editing = False
          Width = 112
        end
        object cxGridDBTableView1namatoko: TcxGridDBColumn
          Caption = 'Supplier'
          DataBinding.FieldName = 'namatoko'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGridDBTableView1namatokoPropertiesButtonClick
          Width = 118
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 337
      Width = 745
      Height = 221
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellClick = cxGrid2DBTableView1CellClick
        DataController.DataSource = DataSourceView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 85
        end
        object cxGrid2DBTableView1Tanggal: TcxGridDBColumn
          DataBinding.FieldName = 'Tanggal'
        end
        object cxGrid2DBTableView1KasBon: TcxGridDBColumn
          DataBinding.FieldName = 'KasBon'
          Width = 267
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 119
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object ListBox1: TListBox
    Left = 816
    Top = 200
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 2
    Visible = False
  end
  object ListBox3: TListBox
    Left = 656
    Top = 224
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 12
    Visible = False
  end
  object ListBox2: TListBox
    Left = 520
    Top = 224
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 4
    Visible = False
  end
  object BtnDelete: TButton
    Left = 400
    Top = 472
    Width = 113
    Height = 33
    Caption = 'Delete CNC ini'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = False
    OnClick = BtnDeleteClick
  end
  object BtnSave: TButton
    Left = 240
    Top = 328
    Width = 97
    Height = 33
    Caption = 'Save'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    OnClick = BtnSaveClick
  end
  object Button1: TButton
    Left = 608
    Top = 424
    Width = 97
    Height = 33
    Caption = 'Save and Print'
    TabOrder = 1
    Visible = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 369
    Height = 559
    Align = alLeft
    TabOrder = 14
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 367
      Height = 96
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 140
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Tanggal: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tanggal'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1KasBon: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KasBon'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Penyetuju: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penyetuju'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1DetailPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'DetailPenyetuju'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 2
        Index = 0
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 97
      Width = 367
      Height = 461
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellClick = cxGrid1DBTableView1CellClick
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Visible = False
        end
        object cxGrid1DBTableView1BonBarang: TcxGridDBColumn
          DataBinding.FieldName = 'BonBarang'
          Width = 79
        end
        object cxGrid1DBTableView1nama: TcxGridDBColumn
          DataBinding.FieldName = 'nama'
          Width = 164
          IsCaptionAssigned = True
        end
        object cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn
          Caption = 'Beli'
          DataBinding.FieldName = 'JumlahBeli'
          Width = 43
        end
        object cxGrid1DBTableView1satuan: TcxGridDBColumn
          Caption = 'Satuan'
          DataBinding.FieldName = 'satuan'
          Width = 51
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object BtnKiri: TButton
    Left = 347
    Top = 214
    Width = 41
    Height = 33
    Caption = '<<'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    OnClick = BtnKiriClick
  end
  object BtnKanan: TButton
    Left = 348
    Top = 176
    Width = 41
    Height = 33
    Caption = '>>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    OnClick = BtnKananClick
  end
  object CobaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    BeforeDelete = CobaQBeforeDelete
    SQL.Strings = (
      
        'select distinct dpo.*,b.nama, db.jumlahbeli, db.supplier, db.gra' +
        'ndtotal, 0 as temp, '#39#39' as kodebar, db.bonbarang as bonbarang,b.s' +
        'atuan'
      'from detailcashncarry dpo, daftarbeli db, barang b'
      
        'where dpo.kodedaftarbeli=db.kode and db.barang=b.kode and dpo.Ko' +
        'decnc=:kodecnc')
    UpdateObject = SDUpdateSQL1
    Left = 536
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'kodecnc'
        ParamType = ptInput
      end>
    object CobaQKodeCNC: TStringField
      FieldName = 'KodeCNC'
      Required = True
      Size = 10
    end
    object CobaQKodeDaftarBeli: TStringField
      FieldName = 'KodeDaftarBeli'
      Required = True
      Size = 10
    end
    object CobaQRealisasiHarga: TCurrencyField
      FieldName = 'RealisasiHarga'
    end
    object CobaQTglBeli: TDateTimeField
      FieldName = 'TglBeli'
    end
    object CobaQRealisasiJumlah: TFloatField
      FieldName = 'RealisasiJumlah'
    end
    object CobaQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object CobaQjumlahbeli: TIntegerField
      FieldName = 'jumlahbeli'
      Required = True
    end
    object CobaQsupplier: TStringField
      FieldName = 'supplier'
      Size = 10
    end
    object CobaQgrandtotal: TCurrencyField
      FieldName = 'grandtotal'
      Required = True
    end
    object CobaQtemp: TIntegerField
      FieldName = 'temp'
      Required = True
    end
    object CobaQkodebar: TStringField
      FieldName = 'kodebar'
      Required = True
      Size = 1
    end
    object CobaQbonbarang: TStringField
      FieldName = 'bonbarang'
      Required = True
      Size = 10
    end
    object CobaQStatusCNC: TStringField
      FieldName = 'StatusCNC'
      Size = 50
    end
    object CobaQsatuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
    object CobaQnamatoko: TStringField
      FieldKind = fkLookup
      FieldName = 'namatoko'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'supplier'
      Size = 50
      Lookup = True
    end
  end
  object DataSourceCoba: TDataSource
    DataSet = CobaQ
    Left = 464
    Top = 112
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeCNC, KodeDaftarBeli, RealisasiHarga, TglBeli, Realisa' +
        'siJumlah, StatusCNC'#13#10'from detailcashncarry'
      'where'
      '  KodeCNC = :OLD_KodeCNC and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    ModifySQL.Strings = (
      'update detailcashncarry'
      'set'
      '  KodeCNC = :KodeCNC,'
      '  KodeDaftarBeli = :KodeDaftarBeli,'
      '  RealisasiHarga = :RealisasiHarga,'
      '  TglBeli = :TglBeli,'
      '  RealisasiJumlah = :RealisasiJumlah,'
      '  StatusCNC = :StatusCNC'
      'where'
      '  KodeCNC = :OLD_KodeCNC and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    InsertSQL.Strings = (
      'insert into detailcashncarry'
      
        '  (KodeCNC, KodeDaftarBeli, RealisasiHarga, TglBeli, RealisasiJu' +
        'mlah, StatusCNC)'
      'values'
      
        '  (:KodeCNC, :KodeDaftarBeli, :RealisasiHarga, :TglBeli, :Realis' +
        'asiJumlah, :StatusCNC)')
    DeleteSQL.Strings = (
      'delete from detailcashncarry'
      'where'
      '  KodeCNC = :OLD_KodeCNC and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    Left = 504
    Top = 120
  end
  object UpdateHeaderPO: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, KasBon, Penyetuju, CreateDate, CreateBy, O' +
        'perator, TglEntry, TglCetak, Status'
      'from cashncarry'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update cashncarry'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  KasBon = :KasBon,'
      '  Penyetuju = :Penyetuju,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into cashncarry'
      
        '  (Kode, Tanggal, KasBon, Penyetuju, CreateDate, CreateBy, Opera' +
        'tor, TglEntry, TglCetak, Status)'
      'values'
      
        '  (:Kode, :Tanggal, :KasBon, :Penyetuju, :CreateDate, :CreateBy,' +
        ' :Operator, :TglEntry, :TglCetak, :Status)')
    DeleteSQL.Strings = (
      'delete from cashncarry'
      'where'
      '  Kode = :OLD_Kode')
    Left = 168
    Top = 328
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select top 1 kode from cashncarry order by kode desc')
    Left = 104
    Top = 328
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = HeaderQ
    Left = 200
    Top = 328
  end
  object HeaderQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from cashncarry'
      'where kode=:kodecnc')
    UpdateObject = UpdateHeaderPO
    Left = 136
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'kodecnc'
        ParamType = ptInput
      end>
    object HeaderQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HeaderQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object HeaderQKasBon: TCurrencyField
      FieldName = 'KasBon'
      Required = True
    end
    object HeaderQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 50
    end
    object HeaderQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object HeaderQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object HeaderQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object HeaderQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object HeaderQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object HeaderQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object HeaderQDetailPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 10
      Lookup = True
    end
  end
  object DaftarBeliQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select db.*,b.nama,b.satuan'
      'from daftarbeli db, barang b'
      'where db.barang=b.kode'
      'and db.status='#39'ON PROCESS'#39' and db.cashncarry=1'
      'order by db.bonbarang desc')
    UpdateObject = UpdateDaftarBeli
    Left = 32
    Top = 216
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DaftarBeliQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object DaftarBeliQsatuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
  end
  object JumlahQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select sum(grandtotal) as total'
      'from detailpo dpo, daftarbeli db '
      'where dpo.kodedaftarbeli=db.kode'
      'and dpo.kodepo=:kodepo')
    Left = 488
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodepo'
        ParamType = ptInput
      end>
    object JumlahQtotal: TCurrencyField
      FieldName = 'total'
    end
  end
  object DataSource2: TDataSource
    DataSet = DaftarBeliQ
    Left = 72
    Top = 216
  end
  object UpdateDaftarBeli: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplie' +
        'r2, Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ke' +
        'terangan3'#13#10'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Harga1 = :Harga1,'
      '  Termin1 = :Termin1,'
      '  Keterangan1 = :Keterangan1,'
      '  Supplier2 = :Supplier2,'
      '  Harga2 = :Harga2,'
      '  Termin2 = :Termin2,'
      '  Keterangan2 = :Keterangan2,'
      '  Supplier3 = :Supplier3,'
      '  Harga3 = :Harga3,'
      '  Termin3 = :Termin3,'
      '  Keterangan3 = :Keterangan3'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplier2, ' +
        'Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ketera' +
        'ngan3)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Harga1, :Termin1, :Keteran' +
        'gan1, :Supplier2, :Harga2, :Termin2, :Keterangan2, :Supplier3, :' +
        'Harga3, :Termin3, :Keterangan3)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 112
    Top = 216
  end
  object SembarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 544
    Top = 352
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7601'
    TempPath = 'C:\Users\intan\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    PrintOptions.StartPage = 1
    PrintOptions.StopPage = 65535
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcMagnify
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcMagnify
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcMagnify
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 744
    Top = 344
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select cast(tanggal as date) as Tanggal,* '
      'from cashncarry'
      'where (Tanggal>=:text1 and Tanggal<=:text2) or tanggal is null'
      'order by tglentry desc')
    Left = 296
    Top = 416
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQKasBon: TCurrencyField
      FieldName = 'KasBon'
      Required = True
    end
    object ViewQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object ViewQTanggal: TStringField
      FieldName = 'Tanggal'
    end
    object ViewQTanggal_1: TDateTimeField
      FieldName = 'Tanggal_1'
    end
  end
  object DataSourceView: TDataSource
    DataSet = ViewQ
    Left = 360
    Top = 624
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 616
    Top = 280
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object SuppQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from supplier')
    Left = 520
    Top = 200
    object SuppQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SuppQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SuppQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SuppQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SuppQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SuppQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SuppQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SuppQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SuppQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SuppQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SuppQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SuppQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SuppQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SuppQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SuppQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SuppQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SuppQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SuppQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SuppQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SuppQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SuppQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SuppQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SuppQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object cekStatusDaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli where kode=:text')
    Left = 568
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object cekStatusDaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object cekStatusDaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object cekStatusDaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object cekStatusDaftarBeliQUrgent: TBooleanField
      FieldName = 'Urgent'
    end
    object cekStatusDaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object cekStatusDaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object cekStatusDaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object cekStatusDaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object cekStatusDaftarBeliQTglLast: TDateTimeField
      FieldName = 'TglLast'
    end
    object cekStatusDaftarBeliQHargaLast2: TCurrencyField
      FieldName = 'HargaLast2'
    end
    object cekStatusDaftarBeliQHargaLast3: TCurrencyField
      FieldName = 'HargaLast3'
    end
    object cekStatusDaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object cekStatusDaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object cekStatusDaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object cekStatusDaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object cekStatusDaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object cekStatusDaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object cekStatusDaftarBeliQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object cekStatusDaftarBeliQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object cekStatusDaftarBeliQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object cekStatusDaftarBeliQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object cekStatusDaftarBeliQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object cekStatusDaftarBeliQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object cekStatusDaftarBeliQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object cekStatusDaftarBeliQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object cekStatusDaftarBeliQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object cekStatusDaftarBeliQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object cekStatusDaftarBeliQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object cekStatusDaftarBeliQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object cekStatusDaftarBeliQBeli1: TIntegerField
      FieldName = 'Beli1'
    end
    object cekStatusDaftarBeliQBeli2: TIntegerField
      FieldName = 'Beli2'
    end
    object cekStatusDaftarBeliQBeli3: TIntegerField
      FieldName = 'Beli3'
    end
    object cekStatusDaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
end
