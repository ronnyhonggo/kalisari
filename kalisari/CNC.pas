unit CNC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  SDEngine, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxButtonEdit,
  cxVGrid, cxDBVGrid, cxInplaceContainer, StdCtrls, cxContainer, Menus,
  cxButtons, cxTextEdit, cxMaskEdit, ComCtrls, cxDropDownEdit, ExtCtrls,
  cxCheckBox, UCrpeClasses, UCrpe32, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLabel,
  cxCalendar, cxGroupBox;

type
  TCNCFm = class(TForm)
    CobaQ: TSDQuery;
    DataSourceCoba: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    SearchBtn: TcxButton;
    UpdateHeaderPO: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    DataSource1: TDataSource;
    HeaderQ: TSDQuery;
    DaftarBeliQ: TSDQuery;
    JumlahQ: TSDQuery;
    JumlahQtotal: TCurrencyField;
    DataSource2: TDataSource;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQnama: TStringField;
    UpdateDaftarBeli: TSDUpdateSQL;
    SembarangQ: TSDQuery;
    HeaderQKode: TStringField;
    HeaderQKasBon: TCurrencyField;
    HeaderQPenyetuju: TStringField;
    HeaderQCreateDate: TDateTimeField;
    HeaderQCreateBy: TStringField;
    HeaderQOperator: TStringField;
    HeaderQTglEntry: TDateTimeField;
    HeaderQTglCetak: TDateTimeField;
    CobaQKodeCNC: TStringField;
    CobaQKodeDaftarBeli: TStringField;
    CobaQRealisasiHarga: TCurrencyField;
    CobaQTglBeli: TDateTimeField;
    CobaQRealisasiJumlah: TFloatField;
    CobaQnama: TStringField;
    CobaQjumlahbeli: TIntegerField;
    CobaQsupplier: TStringField;
    CobaQgrandtotal: TCurrencyField;
    CobaQtemp: TIntegerField;
    CobaQkodebar: TStringField;
    CobaQbonbarang: TStringField;
    CobaQStatusCNC: TStringField;
    HeaderQStatus: TStringField;
    Button1: TButton;
    Crpe1: TCrpe;
    CobaQsatuan: TStringField;
    DaftarBeliQsatuan: TStringField;
    ViewQ: TSDQuery;
    DataSourceView: TDataSource;
    ViewQKode: TStringField;
    ViewQKasBon: TCurrencyField;
    ViewQPenyetuju: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQTglCetak: TDateTimeField;
    ViewQStatus: TStringField;
    ListBox1: TListBox;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    HeaderQDetailPenyetuju: TStringField;
    SuppQ: TSDQuery;
    SuppQKode: TStringField;
    SuppQNamaToko: TStringField;
    SuppQAlamat: TStringField;
    SuppQNoTelp: TStringField;
    SuppQFax: TStringField;
    SuppQEmail: TStringField;
    SuppQKategori: TStringField;
    SuppQStandarTermOfPayment: TStringField;
    SuppQCashNCarry: TBooleanField;
    SuppQNPWP: TStringField;
    SuppQNamaPIC1: TStringField;
    SuppQTelpPIC1: TStringField;
    SuppQJabatanPIC1: TStringField;
    SuppQNamaPIC2: TStringField;
    SuppQTelpPIC2: TStringField;
    SuppQJabatanPIC2: TStringField;
    SuppQNamaPIC3: TStringField;
    SuppQTelpPIC3: TStringField;
    SuppQJabatanPIC3: TStringField;
    SuppQCreateDate: TDateTimeField;
    SuppQCreateBy: TStringField;
    SuppQOperator: TStringField;
    SuppQTglEntry: TDateTimeField;
    StatusBar: TStatusBar;
    ListBox2: TListBox;
    BtnSave: TButton;
    BtnDelete: TButton;
    cxButtonEdit1: TcxButtonEdit;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    cxLabel1: TcxLabel;
    pnl1: TPanel;
    Label1: TLabel;
    KodeEdit: TcxButtonEdit;
    cxButton1: TcxButton;
    ListBox3: TListBox;
    Panel1: TPanel;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1KodeDaftarBeli: TcxGridDBColumn;
    cxGridDBTableView1bonbarang: TcxGridDBColumn;
    cxGridDBTableView1nama: TcxGridDBColumn;
    cxGridDBTableView1jumlahbeli: TcxGridDBColumn;
    cxGridDBTableView1RealisasiJumlah: TcxGridDBColumn;
    cxGridDBTableView1satuan: TcxGridDBColumn;
    cxGridDBTableView1RealisasiHarga: TcxGridDBColumn;
    cxGridDBTableView1TglBeli: TcxGridDBColumn;
    cxGridDBTableView1supplier: TcxGridDBColumn;
    cxGridDBTableView1namatoko: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Panel2: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1KasBon: TcxDBEditorRow;
    cxDBVerticalGrid1Penyetuju: TcxDBEditorRow;
    cxDBVerticalGrid1DetailPenyetuju: TcxDBEditorRow;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1nama: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn;
    cxGrid1DBTableView1satuan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    BtnKiri: TButton;
    BtnKanan: TButton;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1KasBon: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    CobaQnamatoko: TStringField;
    HeaderQTanggal: TDateTimeField;
    cxDBVerticalGrid1Tanggal: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    lblAudit: TLabel;
    cekStatusDaftarBeliQ: TSDQuery;
    cekStatusDaftarBeliQKode: TStringField;
    cekStatusDaftarBeliQBarang: TStringField;
    cekStatusDaftarBeliQBonBarang: TStringField;
    cekStatusDaftarBeliQUrgent: TBooleanField;
    cekStatusDaftarBeliQHargaMin: TCurrencyField;
    cekStatusDaftarBeliQHargaMax: TCurrencyField;
    cekStatusDaftarBeliQHargaLast: TCurrencyField;
    cekStatusDaftarBeliQLastSupplier: TStringField;
    cekStatusDaftarBeliQTglLast: TDateTimeField;
    cekStatusDaftarBeliQHargaLast2: TCurrencyField;
    cekStatusDaftarBeliQHargaLast3: TCurrencyField;
    cekStatusDaftarBeliQJumlahBeli: TIntegerField;
    cekStatusDaftarBeliQHargaSatuan: TCurrencyField;
    cekStatusDaftarBeliQSupplier: TStringField;
    cekStatusDaftarBeliQCashNCarry: TBooleanField;
    cekStatusDaftarBeliQGrandTotal: TCurrencyField;
    cekStatusDaftarBeliQStatus: TStringField;
    cekStatusDaftarBeliQSupplier1: TStringField;
    cekStatusDaftarBeliQSupplier2: TStringField;
    cekStatusDaftarBeliQSupplier3: TStringField;
    cekStatusDaftarBeliQHarga1: TCurrencyField;
    cekStatusDaftarBeliQHarga2: TCurrencyField;
    cekStatusDaftarBeliQHarga3: TCurrencyField;
    cekStatusDaftarBeliQTermin1: TIntegerField;
    cekStatusDaftarBeliQTermin2: TIntegerField;
    cekStatusDaftarBeliQTermin3: TIntegerField;
    cekStatusDaftarBeliQKeterangan1: TMemoField;
    cekStatusDaftarBeliQKeterangan2: TMemoField;
    cekStatusDaftarBeliQKeterangan3: TMemoField;
    cekStatusDaftarBeliQBeli1: TIntegerField;
    cekStatusDaftarBeliQBeli2: TIntegerField;
    cekStatusDaftarBeliQBeli3: TIntegerField;
    cekStatusDaftarBeliQTermin: TIntegerField;
    ViewQTanggal: TStringField;
    ViewQTanggal_1: TDateTimeField;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    procedure cxGridDBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGridDBTableView1namaPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CobaQBeforeDelete(DataSet: TDataSet);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure cxGridDBTableView1namatokoPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid2DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnKananClick(Sender: TObject);
    procedure BtnKiriClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxButtonEdit1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButtonEdit1Exit(Sender: TObject);
    procedure cxButtonEdit1Enter(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
     print:Boolean;
  end;

var
  CNCFm: TCNCFm;
  grandtotal:Integer;
  ctr:integer;
  HeaderOriSQL: string;

implementation
uses MenuUtama,browse,BrowseName, StrUtils, DropDown, SupplierDropDown, DM;

{$R *.dfm}

procedure TCNCFm.cxGridDBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var i:Integer;
begin
end;

procedure TCNCFm.SearchBtnClick(Sender: TObject);
var i,j:integer;
begin

  {MenuUtamaFm.param_sql:='select kode as kode_CNC, kasbon,status from cashncarry order by kode desc';
  BrowseFm.ShowModal;
  KodeEdit.Text:= MenuUtamaFm.param_kodebon; }
  KodeEdit.Text:= ViewQKode.AsString;

  SuppQ.Open;
  PegawaiQ.Open;

  CobaQ.Close;
  CobaQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Edit;

  while not CobaQ.Eof do
  begin
      CobaQ.Edit;
      CobaQtemp.AsInteger:= CobaQgrandtotal.AsInteger;
      CobaQ.Post;
      CobaQ.Next;
  end;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;

end;

procedure TCNCFm.cxGridDBTableView1namaPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BrowseNameFm.ShowModal;
  CobaQ.Edit;
  CobaQ.Fields[16].AsString:=MenuUtamaFm.param_nama;
  CobaQ.Fields[4].AsString:=MenuUtamaFm.param_kode;
end;

procedure TCNCFm.BtnSaveClick(Sender: TObject);
var kodebon:String;
i:integer;
tdkcukup,belum:boolean;
tambahan:String;
begin
      if (KodeEdit.Text='Insert Baru') or (KodeEdit.Text='') then
      begin
        try
          KodeQ.Open;
          if KodeQ.IsEmpty then
          begin
            HeaderQKode.AsString:=FormatFloat('0000000000',1);
          end
          else
          begin

            HeaderQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+1);
          end;
          KodeQ.Close;

          kodebon:=HeaderQKode.AsString;

          HeaderQ.Edit;
          HeaderQStatus.AsString:='ON PROCESS';
          HeaderQ.Post;

          //ubah kodedetail di detail==================================
          CobaQ.First;
          while not CobaQ.Eof do
          begin
             CobaQ.Edit;
             CobaQKodeCNC.AsString:=HeaderQKode.AsString;
             CobaQ.Post;

            CobaQ.Next;
          end;//===========================================================


            except
            on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
        end
      end;

      belum:=false;
      CobaQ.First;
      while not CobaQ.Eof do
      begin
          CobaQ.Edit;
          if CobaQKodeCNC.AsString='tes' then CobaQKodeCNC.AsString:=HeaderQKode.AsString;

         if (CobaQRealisasiJumlah.AsInteger>0) and (CobaQRealisasiHarga.AsInteger>0) then
         begin
            if (CobaQStatusCNC.AsString='') then
                CobaQStatusCNC.AsString:='COMPLETED';
            CobaQgrandtotal.AsInteger:=CobaQRealisasiJumlah.AsInteger*CobaQRealisasiHarga.AsInteger;
         end
         else
            CobaQStatusCNC.AsString:='';

//         if (CobaQStatusCNC.AsString<>'COMPLETED') or (CobaQStatusCNC.AsString<>'FINISHED') then
         if (CobaQStatusCNC.AsString='') then
            belum:=true;

         CobaQ.Post;
         CobaQ.Next;
      end;

      if belum=false then
      begin
        HeaderQ.Edit;
        HeaderQStatus.AsString:='COMPLETED';
        HeaderQ.Post;
      end;

      MenuUtamaFm.DataBase1.StartTransaction;
      try
        HeaderQ.ApplyUpdates;
        MenuUtamaFm.DataBase1.Commit;
        HeaderQ.CommitUpdates;

        MenuUtamaFm.DataBase1.StartTransaction;
        try
          CobaQ.ApplyUpdates;
          MenuUtamaFm.DataBase1.Commit;
          CobaQ.CommitUpdates;

          CobaQ.First;
          while not CobaQ.Eof do
          begin
            SembarangQ.Close;
            SembarangQ.SQL.Clear;

            if (CobaQRealisasiJumlah.AsInteger>0) and (CobaQRealisasiHarga.AsInteger>0) then
                //tambahan:= 'hargasatuan='+IntToStr(CobaQRealisasiHarga.AsInteger)+', jumlahbeli='+IntToStr(CobaQRealisasiJumlah.AsInteger)+','
                tambahan:= 'hargasatuan='+IntToStr(CobaQRealisasiHarga.AsInteger)+','
                else
                tambahan:='';


            SembarangQ.SQL.Add('update daftarbeli set '+tambahan+' status ='+QuotedStr('CNC')+',grandtotal='+IntToStr(CobaQgrandtotal.AsInteger)+', supplier='+QuotedStr(CobaQsupplier.AsString)+' where kode='+QuotedStr(CobaQKodeDaftarBeli.AsString));
            SembarangQ.ExecSQL;
            CobaQ.Next;
           end;

          //ShowMessage('CNC dengan kode '+ HeaderQKode.AsString +' telah disimpan');
          ViewQ.Refresh;

          if MessageDlg('CNC dengan kode '+ HeaderQKode.AsString +' telah disimpan. Cetak CNC ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
          begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update cashncarry set tglcetak='+DateToStr(Now)+' where kode='+QuotedStr(HeaderQKode.AsString));
            SembarangQ.ExecSQL;

            Crpe1.Refresh;
            Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'CNC.rpt';

            Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
            KodeEdit.Text:='';
            print:=false;
          Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
            Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
            Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


            Crpe1.Execute;
            Crpe1.WindowState:= wsMaximized;
          end;
          KodeEdit.Text:='';

          DaftarBeliQ.First;
          while not DaftarBeliQ.Eof do
          begin
              SembarangQ.SQL.Clear;
              SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ON PROCESS')+' where kode='+QuotedStr(DaftarBeliQKode.AsString));
              SembarangQ.ExecSQL;

              DaftarBeliQ.Next;
          end;

        except
           MenuUtamaFm.DataBase1.Rollback;
          HeaderQ.RollbackUpdates;
          CobaQ.RollbackUpdates;
          ShowMessage('Penyimpanan detail Gagal, silahkan coba kembali');
          KodeEditPropertiesButtonClick(sender,1);

        end;

      except
        MenuUtamaFm.DataBase1.Rollback;
        HeaderQ.RollbackUpdates;
        ShowMessage('Penyimpanan header Gagal, silahkan coba kembali');
        KodeEditPropertiesButtonClick(sender,1);
      end;

end;

procedure TCNCFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
var i,t1,t2,hasil:integer;
begin

end;

procedure TCNCFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  KodeEdit.Text:='';
  KodeEdit.Properties.MaxLength:=HeaderQKode.Size;

end;

procedure TCNCFm.CobaQBeforeDelete(DataSet: TDataSet);
var i:integer;
begin

end;

procedure TCNCFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  CobaQ.Insert;
   CobaQ.Append;
   CobaQKodeDaftarBeli.AsString:=DaftarBeliQKode.AsString;
//   CobaQKodeLPB.AsString:=HeaderQKode.AsString;
   CobaQnama.AsString:=DaftarBeliQnama.AsString;
   CobaQjumlahbeli.AsInteger:=DaftarBeliQJumlahBeli.AsInteger;
//  CobaQnamatoko.AsString:=DaftarBeliQnamatoko.AsString;
  CobaQsupplier.AsString:=DaftarBeliQSupplier.AsString;

  CobaQgrandtotal.AsString:=DaftarBeliQGrandTotal.AsString;
  CobaQKodeCNC.AsString:='tes';
  CobaQtemp.AsInteger:= DaftarBeliQGrandTotal.AsInteger;
  CobaQkodebar.AsString:=DaftarBeliQBarang.AsString;
  CobaQbonbarang.AsString:=DaftarBeliQBonBarang.AsString;
  CobaQsatuan.AsString:=DaftarBeliQsatuan.AsString;

   CobaQ.Post;

   DaftarBeliQ.Delete;
end;

procedure TCNCFm.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var i:integer;
begin

end;

procedure TCNCFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  DaftarBeliQ.Open;
  DaftarBeliQ.Insert;
   DaftarBeliQnama.AsString:=CobaQnama.AsString;
   DaftarBeliQJumlahBeli.AsInteger:=CobaQjumlahbeli.AsInteger;
  DaftarBeliQHargaSatuan.AsInteger:=CobaQRealisasiHarga.AsInteger;
  DaftarBeliQSupplier.AsString:=CobaQsupplier.AsString;
  DaftarBeliQGrandTotal.AsString:=CobaQgrandtotal.AsString;
  DaftarBeliQGrandTotal.AsInteger:= CobaQtemp.AsInteger;

  DaftarBeliQKode.AsString:=CobaQKodeDaftarBeli.AsString;
  DaftarBeliQBarang.AsString:=CobaQkodebar.AsString;
  DaftarBeliQBonBarang.AsString:=CobaQbonbarang.AsString;
  DaftarBeliQStatus.AsString:='ON PROCESS';
  DaftarBeliQsatuan.AsString:=CobaQsatuan.AsString;
//  DaftarBeliQnamatoko.AsString:='';
   DaftarBeliQ.Post;

   CobaQ.Delete;
end;

procedure TCNCFm.FormActivate(Sender: TObject);
begin
  DaftarBeliQ.Close;
  DaftarBeliQ.Open;

  cxDBVerticalGrid1.Enabled:=false;
  cxGrid3.Enabled:=false;
  //cxGrid2.Enabled:=false;
  cxGrid1.Enabled:=false;
  btnkanan.Enabled:=false;
  BtnKiri.Enabled:=false;
  BtnSave.Enabled:=false;
  BtnDelete.Enabled:=false;

  if MenuUtamaFm.UserQInsertCashNCarry.AsBoolean=true then
  begin
    cxDBVerticalGrid1.Enabled:=true;
    cxGrid3.Enabled:=true;
    cxGrid2.Enabled:=true;
    cxGrid1.Enabled:=true;
    btnkanan.Enabled:=true;
    BtnKiri.Enabled:=true;
    BtnSave.Enabled:=true;
  end;
end;

procedure TCNCFm.cxGridDBTableView1namatokoPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      CobaQ.Edit;
      CobaQSupplier.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TCNCFm.cxGrid2DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var i,j:integer;  
begin
  KodeEdit.Text:=ViewQKode.AsString;
  SearchBtnClick(sender);
  cxDBVerticalGrid1.Enabled:=false;
  cxGrid3.Enabled:=false;
  //cxGrid2.Enabled:=false;
  cxGrid1.Enabled:=false;
  btnkanan.Enabled:=false;
  BtnKiri.Enabled:=false;
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;

  if MenuUtamaFm.UserQUpdateCashNCarry.AsBoolean=true then
  begin
    cxDBVerticalGrid1.Enabled:=true;
    cxGrid3.Enabled:=true;
    cxGrid2.Enabled:=true;
    cxGrid1.Enabled:=true;
    btnkanan.Enabled:=true;
    BtnKiri.Enabled:=true;
    SaveBtn.Enabled:=true;
  end;

  if MenuUtamaFm.UserQDeleteCashNCarry.AsBoolean=true then
  begin
    DeleteBtn.Enabled:=true;
  end;

  CobaQ.Close;
  CobaQ.ParamByName('kodecnc').AsString:=ViewQKode.AsString;
  CobaQ.Open;
end;

procedure TCNCFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  CobaQ.Close;
  CobaQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
//  CobaQ.Insert;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Append;
  DaftarBeliQ.Refresh;
  ViewQ.Refresh;

//  KodeEditExit(sender);
  cxDBVerticalGrid1.SetFocus;
  {cxGridDBTableView1.NewItemRow.Visible:=true;
  cxGrid3.Enabled:=true; }

end;

procedure TCNCFm.BtnKananClick(Sender: TObject);
var ada:Boolean;
i:Integer;
begin
 ListBox1.Items.Clear;
    for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
    begin
        ListBox1.Items.Add(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[0]);
    end;

  DaftarBeliQ.First;
    while not DaftarBeliQ.Eof do
    begin
        for i:=0 to ListBox1.Items.Count-1 do
        begin
            ada:=false;
            if ListBox1.Items[i] = DaftarBeliQKode.AsString then
            begin
                ada:=true;
                CobaQ.Insert;
                CobaQ.Append;
                CobaQKodeDaftarBeli.AsString:=DaftarBeliQKode.AsString;
                //   CobaQKodeLPB.AsString:=HeaderQKode.AsString;
                CobaQnama.AsString:=DaftarBeliQnama.AsString;
                CobaQjumlahbeli.AsInteger:=DaftarBeliQJumlahBeli.AsInteger;
//                CobaQnamatoko.AsString:=DaftarBeliQnamatoko.AsString;
                CobaQsupplier.AsString:=DaftarBeliQSupplier.AsString;

                CobaQgrandtotal.AsString:=DaftarBeliQGrandTotal.AsString;
                CobaQKodeCNC.AsString:='tes';
                CobaQtemp.AsInteger:= DaftarBeliQGrandTotal.AsInteger;
                CobaQkodebar.AsString:=DaftarBeliQBarang.AsString;
                CobaQbonbarang.AsString:=DaftarBeliQBonBarang.AsString;
                CobaQsatuan.AsString:=DaftarBeliQsatuan.AsString;
                CobaQ.Post;

                DaftarBeliQ.Delete;
            end;
        end;
         if ada=false then
          DaftarBeliQ.Next;
    end;
end;

procedure TCNCFm.BtnKiriClick(Sender: TObject);
var ada:Boolean;
    i:Integer;
begin
  ListBox1.Items.Clear;
    for i:=1 to cxGridDBTableView1.Controller.SelectedRecordCount do
    begin
        ListBox1.Items.Add(cxGridDBTableView1.Controller.SelectedRecords[i-1].Values[0]);
    end;

    CobaQ.First;
    while not CobaQ.Eof do
    begin
        for i:=0 to ListBox1.Items.Count-1 do
        begin
            ada:=false;
            if ListBox1.Items[i] = CobaQKodeDaftarBeli.AsString then
            begin
              DaftarBeliQ.Open;

              DaftarBeliQ.Insert;
               DaftarBeliQnama.AsString:=CobaQnama.AsString;
               DaftarBeliQJumlahBeli.AsInteger:=CobaQjumlahbeli.AsInteger;
              DaftarBeliQHargaSatuan.AsInteger:=CobaQRealisasiHarga.AsInteger;
              DaftarBeliQSupplier.AsString:=CobaQsupplier.AsString;
              DaftarBeliQGrandTotal.AsString:=CobaQgrandtotal.AsString;
              DaftarBeliQGrandTotal.AsInteger:= CobaQtemp.AsInteger;
//              DaftarBeliQnamatoko.AsString:='';

              DaftarBeliQKode.AsString:=CobaQKodeDaftarBeli.AsString;
              DaftarBeliQBarang.AsString:=CobaQkodebar.AsString;
              DaftarBeliQBonBarang.AsString:=CobaQbonbarang.AsString;
              DaftarBeliQStatus.AsString:='ON PROCESS';
              DaftarBeliQsatuan.AsString:=CobaQsatuan.AsString;
               DaftarBeliQ.Post;

               CobaQ.Delete;
            end;
        end;
        if ada=false then
          CobaQ.Next;
    end;
    
end;

procedure TCNCFm.BtnDeleteClick(Sender: TObject);
var i:integer;
begin
  if MessageDlg('Hapus CNC '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try
        ListBox1.Items.Clear;
       CobaQ.First;
        while not CobaQ.Eof do
        begin
            ListBox1.Items.Add(CobaQKodeDaftarBeli.AsString);
            CobaQ.Next;
        end;

        MenuUtamaFm.Database1.StartTransaction;
        CobaQ.First;
        while not cobaQ.eof do
        begin
          CobaQ.Delete;
        end;
        CobaQ.ApplyUpdates;

       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       for i:=0 to ListBox1.Items.Count-1 do
        begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ON PROCESS')+' where kode='+QuotedStr(ListBox1.Items[i]));
            SembarangQ.ExecSQL;
        end;
       MessageDlg('CNC telah dihapus.',mtInformation,[mbOK],0);
       
     except
       MenuUtamaFm.Database1.Rollback;
       HeaderQ.RollbackUpdates;
       CobaQ.RollbackUpdates;

       MessageDlg('CNC pernah/sedang proses bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;

     KodeEditPropertiesButtonClick(sender,1);
     end;
end;

procedure TCNCFm.cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PegawaiQ.Close;
    PegawaiQ.ExecSQL;
    PegawaiQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      HeaderQ.Open;
      HeaderQ.Edit;
      HeaderQPenyetuju.AsString:=PegawaiQKode.AsString;
      HeaderQDetailPenyetuju.AsString:=PegawaiQNama.AsString;
    end;
    DropDownFm.Release;

end;

procedure TCNCFm.cxButtonEdit1PropertiesChange(Sender: TObject);
begin
//  KodeEdit.Text:='Insert Baru';
  CobaQ.Close;
  CobaQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Insert;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodecnc').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Append;

end;

procedure TCNCFm.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
   KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  HeaderQ.Open;
  HeaderQ.Append;
//  KodeEditExit(sender);
//  MasterVGrid.SetFocus;
end;

procedure TCNCFm.cxButtonEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key=VK_F5) then
  begin
//    HeaderQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,HeaderQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
//      KodeEditExit(Sender);
//      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
//    ExitBtn.Click;
  end;
end;

procedure TCNCFm.cxButtonEdit1Exit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    HeaderQ.SQL.Clear;
//    HeaderQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    HeaderQ.Open;
    if HeaderQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      HeaderQ.Append;
      HeaderQ.Edit;
      //MasterQAktif.AsBoolean :=false;
      //savebtn.Enabled:=menuutamafm.UserQInsertMasterArmada.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
//      DeleteBtn.Enabled:=True;
    end;
//    SaveBtn.Enabled:=True;
//    MasterVGrid.Enabled:=True;
  end;

end;

procedure TCNCFm.cxButtonEdit1Enter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
//  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
//  SaveBtn.Enabled:=false;
//  DeleteBtn.Enabled:=false;
  HeaderQ.Close;
end;

procedure TCNCFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TCNCFm.DeleteBtnClick(Sender: TObject);
var i,j:integer;
    tkode,tlama,tbaru:string;
begin
  if MessageDlg('Hapus CNC '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try

       ListBox1.Items.Clear;
       CobaQ.First;
       while not CobaQ.Eof do
       begin
           ListBox1.Items.Add(CobaQKodeDaftarBeli.AsString);
           CobaQ.Next;
       end;

       MenuUtamaFm.Database1.StartTransaction;
       CobaQ.First;
       while not cobaQ.eof do
       begin
         CobaQ.Delete;
       end;
       CobaQ.ApplyUpdates;

       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       for i:=0 to ListBox1.Items.Count-1 do
        begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ON PROCESS')+' where kode='+QuotedStr(ListBox1.Items[i]));
            SembarangQ.ExecSQL;
        end;

       MessageDlg('CNC telah dihapus.',mtInformation,[mbOK],0);

     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      HeaderQ.RollbackUpdates;
      CobaQ.RollbackUpdates;
      MessageDlg('CNC pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;

     KodeEditPropertiesButtonClick(sender,1);
     end;
end;

procedure TCNCFm.SaveBtnClick(Sender: TObject);
var kodebon:String;
  i,j:integer;
  tdkcukup,belum:boolean;
  tambahan:String;
  tlama,tbaru:string;
  flagTambahan:integer;
begin
  if (KodeEdit.Text='INSERT BARU') or (KodeEdit.Text='') then begin
    try
      KodeQ.Open;
      if KodeQ.IsEmpty then begin
        HeaderQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else begin
//        HeaderQKode.AsString:=DMFm.FillCheck(KodeQkode.AsString,'Cash And Carry');
        HeaderQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;

      KodeQ.Close;
      kodebon:=HeaderQKode.AsString;
      HeaderQ.Edit;
      HeaderQStatus.AsString:='ON PROCESS';
      HeaderQ.Post;

      //ubah kodedetail di detail==================================
      CobaQ.First;
      while not CobaQ.Eof do begin
        CobaQ.Edit;
        CobaQKodeCNC.AsString:=HeaderQKode.AsString;
        CobaQ.Post;
        CobaQ.Next;
      end;//===========================================================
    except
      on E : Exception do
        ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
    end;
  end
  else begin
    HeaderQKode.AsString:=KodeEdit.Text;
  end;

  belum:=false;
  CobaQ.First;
  while not CobaQ.Eof do
  begin
    CobaQ.Edit;
    if CobaQKodeCNC.AsString='tes' then
      CobaQKodeCNC.AsString:=HeaderQKode.AsString;
    if (CobaQRealisasiJumlah.AsInteger>0) and (CobaQRealisasiHarga.AsInteger>0) and (CobaQStatusCNC.AsString='FINISHED') then begin
      CobaQStatusCNC.AsString:='COMPLETED';
      CobaQgrandtotal.AsInteger:=CobaQRealisasiJumlah.AsInteger*CobaQRealisasiHarga.AsInteger;
    end
    else if (CobaQStatusCNC.AsString='') then begin
      CobaQStatusCNC.AsString:='ON PROCESS';
    end;
    if HeaderQStatus.AsString='' then begin
      HeaderQ.edit;
      HeaderQStatus.asString:='ON PROCESS';
      HeaderQ.Post;
    end;
    if (CobaQStatusCNC.AsString='ON PROCESS') then
      belum:=true;
    CobaQ.Post;
    CobaQ.Next;
  end;
  //ShowMessage(BoolToStr(belum));
  //ShowMessage(HeaderQKode.AsString);
  if belum=false then
  begin
    //ShowMessage(BoolToStr(belum));
    HeaderQ.Edit;
    HeaderQStatus.AsString:='COMPLETED';
    HeaderQ.Post;
  end;
  //ShowMessage(HeaderQStatus.AsString);
  MenuUtamaFm.DataBase1.StartTransaction;
  try
    CobaQ.Open;
    CobaQ.First;
    while not CobaQ.Eof do
    begin
      flagTambahan:=0;
      cekStatusDaftarBeliQ.Close;
      cekStatusDaftarBeliQ.ParamByName('text').AsString:=CobaQKodeDaftarBeli.AsString;
      cekStatusDaftarBeliQ.Open;
      if(CobaQRealisasiJumlah.AsInteger>0) and (CobaQRealisasiHarga.AsInteger>0) then begin
        flagTambahan:=flagTambahan+1;
      end;
      if(CobaQRealisasiJumlah.AsInteger>0) and (CobaQsupplier.AsString<>'') then begin
        flagTambahan:=flagTambahan+2;
      end;
      tambahan:='';
      if flagTambahan=1 then begin
        tambahan:= 'hargasatuan='+IntToStr(CobaQRealisasiHarga.AsInteger)+',';
      end
      else if flagTambahan=2 then begin
        tambahan:= 'supplier='+QuotedStr(CobaQsupplier.AsString)+',';
      end
      else if flagTambahan=3 then begin
        tambahan:= 'hargasatuan='+IntToStr(CobaQRealisasiHarga.AsInteger)+',';
        tambahan:= tambahan + 'supplier='+QuotedStr(CobaQsupplier.AsString)+',';
      end;
      SembarangQ.Close;
      SembarangQ.SQL.Clear;
      if cekStatusDaftarBeliQStatus.AsString='ON PROCESS' then begin
        SembarangQ.SQL.Add('update daftarbeli set status ='+QuotedStr('CNC')+' where kode='+QuotedStr(CobaQKodeDaftarBeli.AsString));
      end
      else begin
        SembarangQ.SQL.Add('update daftarbeli set ' + tambahan + 'grandtotal=' + IntToStr(CobaQRealisasiHarga.AsInteger*CobaQRealisasiJumlah.AsInteger) + ' where kode='+QuotedStr(CobaQKodeDaftarBeli.AsString));
      end;
      SembarangQ.ExecSQL;
      CobaQ.Next;
    end;
    DaftarBeliQ.Open;
    DaftarBeliQ.First;
    while not DaftarBeliQ.Eof do
    begin
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ON PROCESS')+' where kode='+QuotedStr(DaftarBeliQKode.AsString));
      SembarangQ.ExecSQL;
      DaftarBeliQ.Next;
    end;
    //ShowMessage(HeaderQStatus.AsString);
    HeaderQ.ApplyUpdates;
    CobaQ.ApplyUpdates;
    MenuUtamaFm.DataBase1.Commit;
    HeaderQ.CommitUpdates;
    CobaQ.CommitUpdates;
    //ShowMessage(HeaderQStatus.AsString);

    if MessageDlg('CNC dengan kode '+ HeaderQKode.AsString +' telah disimpan. Cetak CNC ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('update cashncarry set tglcetak=getdate() where kode='+QuotedStr(HeaderQKode.AsString));
      SembarangQ.ExecSQL;
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'CNC.rpt';
      Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
      KodeEdit.Text:='';
      print:=false;
      Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
      Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
      Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
      Crpe1.Execute;
      Crpe1.WindowState:= wsMaximized;
    end;
    KodeEdit.Text:='';
    ViewQ.Refresh;

  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      HeaderQ.RollbackUpdates;
      CobaQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  ViewQ.Close;
  ViewQ.Open;
end;

procedure TCNCFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  //cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  //SetujuBtn.Enabled:=false;
  HeaderQ.Close;
end;

procedure TCNCFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    HeaderQ.Close;
//    HeaderQ.SQL.Add('select po.*,s.namatoko from PO,supplier s where po.supplier=s.kode and PO.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    HeaderQ.ParamByName('kodecnc').AsString:=KodeEdit.Text;
    //HeaderQ.SQL.Add('select * from ('+ HeaderOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    HeaderQ.Open;
    if HeaderQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      HeaderQ.Append;
      HeaderQ.Edit;
      CobaQ.Close;
      CobaQ.ParamByName('kodecnc').AsString:=KodeEdit.Text;
      CobaQ.Open;
      //MasterQAktif.AsBoolean :=false;
      savebtn.Enabled:=MenuUtamaFm.UserQInsertCashNCarry.AsBoolean;
      //Audit
      lblAudit.Caption:='';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    //SetujuBtn.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=True;
  end;


end;

procedure TCNCFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    HeaderQ.SQL.Text:=HeaderOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,HeaderQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;

end;

procedure TCNCFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertCashNCarry.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateCashNCarry.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteCashNCarry.AsBoolean;
end;

procedure TCNCFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
end;

procedure TCNCFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
end;

end.




