unit CNCRetur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox;

type
  TCNCReturFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    BarangQ: TSDQuery;
    DCNCQ: TSDQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewCNCRQ: TSDQuery;
    Crpe1: TCrpe;
    KodeQkode: TStringField;
    ViewDs: TDataSource;
    cxButton1: TcxButton;
    MasterQKode: TStringField;
    MasterQKodeCNC: TStringField;
    MasterQJumlahRetur: TIntegerField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    viewCNCRQKode: TStringField;
    viewCNCRQKodeCNC: TStringField;
    viewCNCRQJumlahRetur: TIntegerField;
    viewCNCRQKeterangan: TMemoField;
    viewCNCRQCreateDate: TDateTimeField;
    viewCNCRQCreateBy: TStringField;
    viewCNCRQTglEntry: TDateTimeField;
    DBQ: TSDQuery;
    CNCQ: TSDQuery;
    DCNCQKodeCNC: TStringField;
    DCNCQKodeDaftarBeli: TStringField;
    DCNCQRealisasiHarga: TCurrencyField;
    DCNCQTglBeli: TDateTimeField;
    DCNCQRealisasiJumlah: TFloatField;
    DCNCQStatusCNC: TStringField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQFoto: TBlobField;
    DBQKode: TStringField;
    DBQBarang: TStringField;
    DBQBonBarang: TStringField;
    DBQUrgent: TBooleanField;
    DBQHargaMin: TCurrencyField;
    DBQHargaMax: TCurrencyField;
    DBQHargaLast: TCurrencyField;
    DBQLastSupplier: TStringField;
    DBQTglLast: TDateTimeField;
    DBQHargaLast2: TCurrencyField;
    DBQHargaLast3: TCurrencyField;
    DBQJumlahBeli: TIntegerField;
    DBQHargaSatuan: TCurrencyField;
    DBQSupplier: TStringField;
    DBQCashNCarry: TBooleanField;
    DBQGrandTotal: TCurrencyField;
    DBQStatus: TStringField;
    DBQSupplier1: TStringField;
    DBQSupplier2: TStringField;
    DBQSupplier3: TStringField;
    DBQHarga1: TCurrencyField;
    DBQHarga2: TCurrencyField;
    DBQHarga3: TCurrencyField;
    DBQTermin1: TIntegerField;
    DBQTermin2: TIntegerField;
    DBQTermin3: TIntegerField;
    DBQKeterangan1: TMemoField;
    DBQKeterangan2: TMemoField;
    DBQKeterangan3: TMemoField;
    DBQBeli1: TIntegerField;
    DBQBeli2: TIntegerField;
    DBQBeli3: TIntegerField;
    DBQTermin: TIntegerField;
    MasterVGridKodeCNC: TcxDBEditorRow;
    MasterVGridJumlahRetur: TcxDBEditorRow;
    CNCQKode: TStringField;
    CNCQTanggal: TDateTimeField;
    CNCQKasBon: TCurrencyField;
    CNCQPenyetuju: TStringField;
    CNCQCreateDate: TDateTimeField;
    CNCQCreateBy: TStringField;
    CNCQOperator: TStringField;
    CNCQTglEntry: TDateTimeField;
    CNCQTglCetak: TDateTimeField;
    CNCQStatus: TStringField;
    MasterQDaftarBeli: TStringField;
    MasterQBarang: TStringField;
    MasterQNamaBarang: TStringField;
    MasterQRealisasiJumlah: TIntegerField;
    MasterQRealisasiHarga: TCurrencyField;
    MasterQTanggalBeli: TDateTimeField;
    MasterVGridNamaBarang: TcxDBEditorRow;
    MasterVGridRealisasiJumlah: TcxDBEditorRow;
    MasterVGridRealisasiHarga: TcxDBEditorRow;
    MasterVGridTanggalBeli: TcxDBEditorRow;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1KodeCNC: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahRetur: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView1TglEntry: TcxGridDBColumn;
    MasterVGridKeterangan: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    MasterQStokBarang: TFloatField;
    MasterVGridStokBarang: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure MasterVGridKodeCNCEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridJumlahReturEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  CNCReturFm: TCNCReturFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, ArmadaDropDown,
  PegawaiDropDown, CashNCarryDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TCNCReturFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TCNCReturFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TCNCReturFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TCNCReturFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  CNCQ.Open;
  DCNCQ.Open;
  DBQ.Open;
  BarangQ.Open;
end;

procedure TCNCReturFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //MasterQTanggal.Value:=Trunc(Now());
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TCNCReturFm.KodeEditEnter(Sender: TObject);
begin
  //buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  cxButton1.Enabled:=false;
  MasterQ.Close;
end;

procedure TCNCReturFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewCNCRQ.Close;
  ViewCNCRQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewCNCRQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewCNCRQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertCNCRetur.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateCNCRetur.AsBoolean;
  cxButton1.Enabled:=menuutamafm.UserQDeleteCNCRetur.AsBoolean;
end;

procedure TCNCReturFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxButton1.Enabled:=menuutamafm.UserQDeleteCNCRetur.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateCNCRetur.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TCNCReturFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TCNCReturFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Retur CashNCarry dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    viewCNCRQ.Close;
    viewCNCRQ.Open;
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TCNCReturFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TCNCReturFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=MenuUtamaFm.UserQKodePegawai.AsString;
  //DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TCNCReturFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus CNC Retur '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('CNC Retur telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('CNC Retur pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TCNCReturFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TCNCReturFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    //buttonCetak.Enabled:=true;
    kodeedit.Text:=viewCNCRQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdateCNCRetur.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

procedure TCNCReturFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus Retur CashNCarry '+KodeEdit.Text+' - '+MasterQKodeCNC.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Retur CashNCarry telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Retur CashNCarry pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewCNCRQ.Refresh;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TCNCReturFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TCNCReturFm.MasterVGridKodeCNCEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  CashNCarryDropDownFm:=TCashNCarryDropdownfm.Create(Self,'Retur');
  if CashNCarryDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKodeCNC.AsString:=CashNCarryDropDownFm.kode;
  end;
  CashNCarryDropDownFm.Release;
end;

procedure TCNCReturFm.MasterVGridJumlahReturEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if MasterQJumlahRetur.AsInteger > MasterQStokBarang.AsInteger then
  begin
    ShowMessage('Jumlah retur tidak boleh melebihi jumlah stok barang yang ada');
    MasterQJumlahRetur.Clear;
  end;
end;

procedure TCNCReturFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewCNCRQ.Close;
  ViewCNCRQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewCNCRQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewCNCRQ.Open;
end;

procedure TCNCReturFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewCNCRQ.Close;
  ViewCNCRQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewCNCRQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewCNCRQ.Open;
end;

end.
