unit CashNCarryDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TCashNCarryDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    CNCQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    CNCQKode: TStringField;
    CNCQKasBon: TCurrencyField;
    CNCQPenyetuju: TStringField;
    CNCQCreateDate: TDateTimeField;
    CNCQCreateBy: TStringField;
    CNCQOperator: TStringField;
    CNCQTglEntry: TDateTimeField;
    CNCQTglCetak: TDateTimeField;
    CNCQStatus: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    CNCQNamaPenyetuju: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1KasBon: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenyetuju: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  CashNCarryDropDownFm: TCashNCarryDropDownFm;
  tipe,SQLOri:string;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TCashNCarryDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  tipe:=kd;
end;

constructor TCashNCarryDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TCashNCarryDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TCashNCarryDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=CNCQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TCashNCarryDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=CNCQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TCashNCarryDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=CNCQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TCashNCarryDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=CNCQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TCashNCarryDropDownFm.FormCreate(Sender: TObject);
begin
{PegawaiQ.Open;
CNCQ.Open;}
end;

procedure TCashNCarryDropDownFm.FormShow(Sender: TObject);
begin
  PegawaiQ.Open;
  SQLOri:=CNCQ.SQL.Text;
  if tipe='Retur' then
    begin
      CNCQ.Close;
      CNCQ.SQL.Text:='select cnc.* from CashNCarry cnc, LPB where cnc.Kode=LPB.CashNCarry order by TglEntry desc';
      CNCQ.Open;
    end
  else
    CNCQ.Open;
end;

end.
