unit CetakanKuitansi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsDefaultPainters, Menus,
  StdCtrls, cxButtons, ComCtrls, cxTextEdit, cxMaskEdit, cxButtonEdit,
  ExtCtrls, DB, SDEngine, cxStyles, cxVGrid, cxDBVGrid, cxInplaceContainer,
  UCrpeClasses, UCrpe32, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TCetakanKuitansiFm = class(TForm)
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    StatusBar: TStatusBar;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    Panel1: TPanel;
    MasterDs: TDataSource;
    MasterUs: TSDUpdateSQL;
    MasterVGrid: TcxDBVerticalGrid;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    Crpe1: TCrpe;
    DeleteBtn: TcxButton;
    MasterVGridTerbilang: TcxDBEditorRow;
    MasterVGridGrandTotal: TcxDBEditorRow;
    MasterVGridTujuan: TcxDBEditorRow;
    MasterVGridTempat: TcxDBEditorRow;
    MasterVGridPemakaian: TcxDBEditorRow;
    MasterVGridTersisa: TcxDBEditorRow;
    MasterVGridNominal: TcxDBEditorRow;
    MasterVGridTglPemakaian: TcxDBEditorRow;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQTerbilang: TMemoField;
    MasterQGrandTotal: TMemoField;
    MasterQTujuan: TMemoField;
    MasterQJamMulai: TMemoField;
    MasterQTempat: TMemoField;
    MasterQPemakaian: TMemoField;
    MasterQNominal: TMemoField;
    MasterQTglPemakaian: TMemoField;
    MasterQNamaTtd: TMemoField;
    MasterVGridNamaTtd: TcxDBEditorRow;
    MasterQPIC: TMemoField;
    MasterVGridPIC: TcxDBEditorRow;
    CekPrintQ: TSDQuery;
    MasterQKomisiPelanggan: TMemoField;
    MasterQKomisiPelangganNominal: TMemoField;
    MasterVGridKomisiPelanggan: TcxDBEditorRow;
    MasterVGridKomisiPelangganNominal: TcxDBEditorRow;
    MasterQDiskon: TMemoField;
    MasterVGridDiskon: TcxDBEditorRow;
    MasterQTersisa: TMemoField;
    UpdateQ: TSDQuery;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  CetakanKuitansiFm: TCetakanKuitansiFm;
  MasterOriSQL, paramkode,NamaTtd: string;

implementation

uses DM, MenuUtama, DropDown, StrUtils;

{$R *.dfm}

constructor TCetakanKuitansiFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TCetakanKuitansiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;



procedure TCetakanKuitansiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TCetakanKuitansiFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TCetakanKuitansiFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
   KodeEdit.Text:=paramkode;
   KodeEditExit(sender);
  end;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  //kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKontrak.AsBoolean;
  SaveBtn.Enabled:=true;
  //DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
  KodeEdit.Enabled:=false;
  //CetakanKuitansiFm.Visible:=true;
end;

procedure TCetakanKuitansiFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
    end
    else
    begin
      Masterq.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
    end;
    SaveBtn.Enabled:=true;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TCetakanKuitansiFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TCetakanKuitansiFm.KodeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TCetakanKuitansiFm.SaveBtnClick(Sender: TObject);
var NominalPrint,GrandTotalPrint: currency;
begin
 NamaTtd:='Dwi Kurniastuti';
 if MasterQKomisiPelanggan.AsString = '' then
 begin
     MasterQ.Edit;
     MasterQKomisiPelanggan.AsString:='0';
 end;
 if MasterQKomisiPelangganNominal.AsString = '' then
 begin
     MasterQ.Edit;
     MasterQKomisiPelangganNominal.AsString:='0';
 end;
 if MasterQDiskon.AsString='' then
 begin
  masterq.edit;
  masterqdiskon.AsString:='0';
 end;
 NominalPrint:=strtocurr(MasterQNominal.AsString)+strtocurr(MasterQKomisiPelangganNominal.AsString);
 GrandTotalPrint:=strtocurr(MasterQGrandTotal.AsString)+strtocurr(MasterQKomisiPelanggan.AsString)-strtocurr(MasterQDiskon.AsString);
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MasterQ.Post;
  //MenuUtamaFm.Database1.StartTransaction;
  try
   // MasterQ.ApplyUpdates;
    //MenuUtamaFm.Database1.Commit;
   // MasterQ.CommitUpdates;
   UpdateQ.Close;
   UpdateQ.SQL.Text:='update CetakanKuitansi set Terbilang='+QuotedStr(MasterQTerbilang.AsString)+', GrandTotal='+QuotedStr(MasterQGrandTotal.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+',Tujuan='+QuotedStr(MasterQTujuan.AsString)+',JamMulai='+QuotedStr(MasterQJamMulai.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+',Tempat='+QuotedStr(MasterQTempat.AsString)+',Pemakaian='+QuotedStr(MasterQPemakaian.AsString)+',Tersisa='+QuotedStr(MasterQTersisa.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+',Nominal='+QuotedStr(MasterQNominal.AsString)+',TglPemakaian='+QuotedStr(MasterQTglPemakaian.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+',NamaTTD='+QuotedStr(MasterQNamaTtd.AsString)+',PIC='+QuotedStr(MasterQPIC.AsString)+',KomisiPelanggan='+QuotedStr(MasterQKomisiPelanggan.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+',KomisiPelangganNominal='+QuotedStr(MasterQKomisiPelangganNominal.AsString)+',Diskon='+QuotedStr(MasterQDiskon.AsString);
   UpdateQ.SQL.Text:=UpdateQ.SQL.Text+' where Kode='+QuotedStr(MasterQKode.AsString);
   UpdateQ.ExecSQL;
    ShowMessage('Simpan berhasil');
    //KodeEdit.SetFocus;
    CekPrintQ.SQL.Text:='update Kwitansi set CekPrint=1 where Kode='+QuotedStr(MasterQKode.AsString);
    CekPrintQ.ExecSQL;
    Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiSO.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
      Crpe1.ParamFields[1].CurrentValue:=MasterQTerbilang.AsString;
      Crpe1.ParamFields[2].CurrentValue:=currtostr(GrandTotalPrint);
      Crpe1.ParamFields[3].CurrentValue:=MasterQTujuan.AsString;
      Crpe1.ParamFields[4].CurrentValue:=MasterQJamMulai.AsString;
      //Crpe1.ParamFields[5].CurrentValue:=jam;
      Crpe1.ParamFields[5].CurrentValue:=MasterQTempat.AsString;
      Crpe1.ParamFields[6].CurrentValue:=MasterQPemakaian.AsString;
      Crpe1.ParamFields[7].CurrentValue:=MasterQTersisa.AsString;
      Crpe1.ParamFields[8].CurrentValue:=currtostr(NominalPrint);
      Crpe1.ParamFields[9].CurrentValue:=MasterQTglPemakaian.AsString;
      Crpe1.ParamFields[10].CurrentValue:=MasterQNamaTtd.AsString;
      Crpe1.ParamFields[11].CurrentValue:=MasterQPIC.AsString;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      close;
      release;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      {updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update masterso set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;  }
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TCetakanKuitansiFm.ExitBtnClick(Sender: TObject);
begin
close;
release;
end;

end.
