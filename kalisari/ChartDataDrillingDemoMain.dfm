inherited frmMain: TfrmMain
  Left = 296
  Top = 94
  Caption = 'Riwayat Harga Barang'
  ClientHeight = 566
  ClientWidth = 842
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescription: TLabel
    Width = 842
    Caption = 
      '  This demo shows the chart'#39's data drilling capabilities. Click ' +
      #39'About this demo'#39' for more information.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Top = 547
    Width = 842
  end
  object Grid: TcxGrid [2]
    Left = 0
    Top = 16
    Width = 842
    Height = 531
    Align = alClient
    TabOrder = 0
    LevelTabs.Style = 9
    LookAndFeel.NativeStyle = True
    RootLevelOptions.DetailTabsPosition = dtpTop
    OnActiveTabChanged = GridActiveTabChanged
    object ChartView: TcxGridDBChartView
      Categories.DataBinding.FieldName = 'ID'
      Categories.DisplayText = 'Order'
      DataController.DataSource = dsOrders
      DiagramColumn.Active = True
      DiagramPie.Legend.Position = cppRight
      Legend.Position = cppNone
      OptionsView.CategoriesPerPage = 10
      ToolBox.CustomizeButton = True
      ToolBox.DataLevelsInfoVisible = dlivAlways
      ToolBox.DiagramSelector = True
      object ChartViewDataGroup3: TcxGridDBChartDataGroup
        DataBinding.FieldName = 'Tahun'
      end
      object ChartViewDataGroup4: TcxGridDBChartDataGroup
        DataBinding.FieldName = 'Bulan'
      end
      object ChartViewDataGroup1: TcxGridDBChartDataGroup
        DataBinding.FieldName = 'NamaBarang'
      end
      object ChartViewDataGroup2: TcxGridDBChartDataGroup
        DataBinding.FieldName = 'NamaSupplier'
      end
      object ChartViewSeries1: TcxGridDBChartSeries
        DataBinding.FieldName = 'HargaSatuan'
      end
    end
    object TableView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = True
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = True
      Navigator.Buttons.GotoBookmark.Visible = True
      Navigator.Buttons.Filter.Visible = True
      DataController.DataSource = dsOrders
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '$0,'
          Kind = skSum
        end
        item
          Format = '0'
          Kind = skSum
        end
        item
          Format = '0'
          Kind = skCount
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupSummaryLayout = gslAlignWithColumns
      Styles.OnGetGroupStyle = TableViewStylesGetGroupStyle
      object TableViewTanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object TableViewNamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 213
      end
      object TableViewhargasatuan: TcxGridDBColumn
        DataBinding.FieldName = 'hargasatuan'
      end
      object TableViewNamaSupplier: TcxGridDBColumn
        DataBinding.FieldName = 'NamaSupplier'
        Width = 195
      end
      object TableViewTahun: TcxGridDBColumn
        DataBinding.FieldName = 'Tahun'
        Width = 78
      end
      object TableViewBulan: TcxGridDBColumn
        DataBinding.FieldName = 'Bulan'
        Width = 95
      end
    end
    object GridLevelChart: TcxGridLevel
      Caption = '  Chart  '
      GridView = ChartView
    end
    object GridLevelTable: TcxGridLevel
      Caption = '  Table  '
      GridView = TableView
    end
  end
  inherited mmMain: TMainMenu
    Left = 160
    inherited miFile: TMenuItem
      inherited miExit: TMenuItem
        OnClick = miExitClick
      end
    end
    object miAbout: TMenuItem
      Visible = False
    end
  end
  inherited StyleRepository: TcxStyleRepository
    Left = 128
    PixelsPerInch = 96
    object styleActiveGroup: TcxStyle [24]
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    inherited GridTableViewStyleSheetDevExpress: TcxGridTableViewStyleSheet
      BuiltIn = True
    end
    inherited GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet
      BuiltIn = True
    end
  end
  object dsOrders: TDataSource
    DataSet = DaftarBeliQ
    Left = 232
    Top = 156
  end
  object DaftarBeliQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select db.barang,db.hargasatuan,db.supplier, '
      'po.createdate as Tanggal, '
      'DATENAME(MONTH,po.createdate) as Bulan, '
      'DATENAME(YEAR,po.createdate) as Tahun'
      'from daftarbeli db, detailPO dp, po'
      'where dp.kodePO=po.kode and db.kode=dp.kodedaftarbeli')
    Left = 264
    Top = 152
    object DaftarBeliQbarang: TStringField
      FieldName = 'barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQhargasatuan: TCurrencyField
      FieldName = 'hargasatuan'
    end
    object DaftarBeliQsupplier: TStringField
      FieldName = 'supplier'
      Size = 10
    end
    object DaftarBeliQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'kode'
      LookupResultField = 'nama'
      KeyFields = 'barang'
      Size = 50
      Lookup = True
    end
    object DaftarBeliQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'supplier'
      Size = 50
      Lookup = True
    end
    object DaftarBeliQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object DaftarBeliQBulan: TStringField
      FieldName = 'Bulan'
      Size = 60
    end
    object DaftarBeliQTahun: TStringField
      FieldName = 'Tahun'
      Size = 60
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from supplier')
    Left = 328
    Top = 152
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select kode,nama from barang')
    Left = 296
    Top = 152
    object BarangQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object BarangQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
