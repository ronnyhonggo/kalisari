unit ChartDataDrillingDemoMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBTables, Menus, StdCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGridChartView, cxGridDBChartView, cxTextEdit,
  cxLookAndFeels, cxLookAndFeelPainters, BaseForm, cxGridCardView, ComCtrls,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, SDEngine;

type
  TfrmMain = class(TfmBaseForm)
    dsOrders: TDataSource;
    GridLevelChart: TcxGridLevel;
    Grid: TcxGrid;
    ChartView: TcxGridDBChartView;
    GridLevelTable: TcxGridLevel;
    TableView: TcxGridDBTableView;
    styleActiveGroup: TcxStyle;
    DaftarBeliQ: TSDQuery;
    ChartViewSeries1: TcxGridDBChartSeries;
    DaftarBeliQbarang: TStringField;
    DaftarBeliQhargasatuan: TCurrencyField;
    DaftarBeliQsupplier: TStringField;
    SupplierQ: TSDQuery;
    BarangQ: TSDQuery;
    BarangQnama: TStringField;
    BarangQkode: TStringField;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    DaftarBeliQNamaBarang: TStringField;
    DaftarBeliQNamaSupplier: TStringField;
    ChartViewDataGroup1: TcxGridDBChartDataGroup;
    ChartViewDataGroup2: TcxGridDBChartDataGroup;
    DaftarBeliQTanggal: TDateTimeField;
    TableViewhargasatuan: TcxGridDBColumn;
    TableViewNamaBarang: TcxGridDBColumn;
    TableViewNamaSupplier: TcxGridDBColumn;
    TableViewTanggal: TcxGridDBColumn;
    DaftarBeliQBulan: TStringField;
    DaftarBeliQTahun: TStringField;
    ChartViewDataGroup3: TcxGridDBChartDataGroup;
    ChartViewDataGroup4: TcxGridDBChartDataGroup;
    TableViewBulan: TcxGridDBColumn;
    TableViewTahun: TcxGridDBColumn;
    procedure GridActiveTabChanged(Sender: TcxCustomGrid; ALevel: TcxGridLevel);
    procedure TableViewStylesGetGroupStyle(Sender: TcxGridTableView;
      ARecord: TcxCustomGridRecord; ALevel: Integer; out AStyle: TcxStyle);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure miExitClick(Sender: TObject);
  protected
    ActiveDataGroups: array of Integer;
    function GetColumnByChartItem(AChartItem: TcxGridChartItem): TcxGridDBColumn;
    procedure ShowTableActiveGroup;
    procedure SynchronizeTableWithChart;
    procedure UpdateTableGroupingAndColumnVisibility;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  AboutDemoForm;

function TfrmMain.GetColumnByChartItem(AChartItem: TcxGridChartItem): TcxGridDBColumn;
begin
  Result := TableView.GetColumnByFieldName(TcxGridDBChartItemDataBinding(AChartItem.DataBinding).FieldName);
end;

procedure TfrmMain.ShowTableActiveGroup;
var
  I, ADataGroupIndex, AGroupRowIndex: Integer;
  ARow: TcxCustomGridRow;
begin
  // expand group rows so that the group with the active chart data is made visible
  SetLength(ActiveDataGroups, ChartView.ActiveDataLevel);
  TableView.BeginUpdate;
  try
    TableView.DataController.Groups.FullCollapse;
    ADataGroupIndex := -1;
    for I := 0 to ChartView.ActiveDataLevel - 1 do
    begin
      ADataGroupIndex := TableView.DataController.Groups.GetDataGroupIndexByGroupValue(
        ADataGroupIndex, ChartView.VisibleDataGroups[I].ActiveValue);
      ActiveDataGroups[I] := ADataGroupIndex;
        
      AGroupRowIndex := TableView.DataController.DataControllerInfo.DataGroups[ADataGroupIndex].RowIndex;
      TableView.DataController.Groups.ChangeExpanding(AGroupRowIndex, True, False);
      TableView.Controller.FocusedRowIndex := AGroupRowIndex;
    end;
  finally
    TableView.EndUpdate;
  end;
  // select rows with the data used for the active chart
  TableView.BeginUpdate;
  try
    if ADataGroupIndex = -1 then
      TableView.Controller.SelectAll
    else
    begin
      TableView.Controller.ClearSelection;
      for I := TableView.Controller.FocusedRowIndex + 1 to TableView.ViewData.RowCount - 1 do
      begin
        ARow := TableView.ViewData.Rows[I];
        if ARow.Level = ChartView.ActiveDataLevel then
        begin
          ARow.Focused := True;
          ARow.Selected := True;
        end
        else
          Break;
      end;
    end;
  finally
    TableView.EndUpdate;
  end;
end;

procedure TfrmMain.SynchronizeTableWithChart;
begin
  UpdateTableGroupingAndColumnVisibility;
  ShowTableActiveGroup;
end;

procedure TfrmMain.TableViewStylesGetGroupStyle(Sender: TcxGridTableView;
  ARecord: TcxCustomGridRecord; ALevel: Integer; out AStyle: TcxStyle);
var
  ADataGroupIndex, I: Integer;
begin
  if ARecord = nil then Exit;
  ADataGroupIndex := TableView.DataController.Groups.DataGroupIndexByRowIndex[ARecord.Index];
  for I := 0 to Length(ActiveDataGroups) - 1 do
    if ADataGroupIndex = ActiveDataGroups[I] then
    begin
      AStyle := styleActiveGroup;
      Break;
    end;
end;

procedure TfrmMain.UpdateTableGroupingAndColumnVisibility;
var
  I: Integer;
begin
  TableView.BeginUpdate;
  try
    TableView.Controller.ClearGrouping;
    for I := 0 to ChartView.VisibleDataGroupCount - 1 do
      GetColumnByChartItem(ChartView.VisibleDataGroups[I]).GroupIndex := I;

    for I := 0 to ChartView.DataGroupCount - 1 do
      GetColumnByChartItem(ChartView.DataGroups[I]).Visible := ChartView.DataGroups[I].Visible;
    for I := 0 to ChartView.SeriesCount - 1 do
      GetColumnByChartItem(ChartView.Series[I]).Visible := ChartView.Series[I].Visible;
  finally
    TableView.EndUpdate;
  end;
end;

procedure TfrmMain.GridActiveTabChanged(Sender: TcxCustomGrid;
  ALevel: TcxGridLevel);
begin
  if ALevel = GridLevelTable then
    SynchronizeTableWithChart;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  inherited;
  SupplierQ.open;
  BarangQ.Open;
  DaftarBeliQ.Open;
end;
procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  release;
end;

procedure TfrmMain.miExitClick(Sender: TObject);
begin
  close;
  release;

end;

end.
