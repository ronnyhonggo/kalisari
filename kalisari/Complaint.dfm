object ComplaintFm: TComplaintFm
  Left = 160
  Top = 147
  BorderStyle = bsDialog
  Caption = 'Komplain dan Retur'
  ClientHeight = 430
  ClientWidth = 1114
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 24
    Top = 13
    Width = 67
    Height = 16
    Caption = 'Kode Retur'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 16
    Top = 80
    Width = 241
    Height = 137
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 0
    DataController.DataSource = DataSource1
    Version = 1
    object cxDBVerticalGrid1TglRetur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglRetur'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Pelaksana: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pelaksana'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1KodePO: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KodePO'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1Retur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Retur'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1Complaint: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Complaint'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object cxGrid1: TcxGrid
    Tag = 1
    Left = 272
    Top = 8
    Width = 825
    Height = 225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Barang: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        Visible = False
        Width = 164
      end
      object cxGrid1DBTableView1nama: TcxGridDBColumn
        Caption = 'Nama Barang'
        DataBinding.FieldName = 'nama'
        Options.Editing = False
        Width = 183
      end
      object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
        Caption = 'Jumlah Retur/Complaint'
        DataBinding.FieldName = 'Jumlah'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnValidate = cxGrid1DBTableView1JumlahPropertiesValidate
        Width = 155
      end
      object cxGrid1DBTableView1PotongPO: TcxGridDBColumn
        Caption = 'Potong Nota'
        DataBinding.FieldName = 'PotongPO'
        Width = 85
      end
      object cxGrid1DBTableView1PesanUlang: TcxGridDBColumn
        Caption = 'Pesan Beda Supplier'
        DataBinding.FieldName = 'PesanUlang'
        Width = 138
      end
      object cxGrid1DBTableView1TglTargetTukar: TcxGridDBColumn
        DataBinding.FieldName = 'TglTargetTukar'
        Width = 107
      end
      object cxGrid1DBTableView1Alasan: TcxGridDBColumn
        DataBinding.FieldName = 'Alasan'
        Width = 147
      end
      object cxGrid1DBTableView1beda: TcxGridDBColumn
        DataBinding.FieldName = 'beda'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object KodeEdit: TcxButtonEdit
    Left = 112
    Top = 10
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Properties.OnButtonClick = KodeEditPropertiesButtonClick
    Properties.OnChange = KodeEditPropertiesChange
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 2
    Text = 'Insert Baru'
    OnDblClick = KodeEditDblClick
    Width = 145
  end
  object SearchBtn: TcxButton
    Left = 112
    Top = 34
    Width = 57
    Height = 21
    Caption = 'History'
    TabOrder = 3
    TabStop = False
    OnClick = SearchBtnClick
  end
  object Button1: TButton
    Tag = 2
    Left = 16
    Top = 224
    Width = 105
    Height = 33
    Caption = 'Save'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = Button1Click
  end
  object cxGrid2: TcxGrid
    Left = 272
    Top = 240
    Width = 825
    Height = 177
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object cxGrid2DBTableView1: TcxGridDBTableView
      OnCellClick = cxGrid2DBTableView1CellClick
      DataController.DataSource = DataSourceView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid2DBTableView1kode_po: TcxGridDBColumn
        DataBinding.FieldName = 'kode_po'
        Options.Editing = False
        Width = 105
      end
      object cxGrid2DBTableView1tglkirim: TcxGridDBColumn
        DataBinding.FieldName = 'tglkirim'
        Options.Editing = False
      end
      object cxGrid2DBTableView1namatoko: TcxGridDBColumn
        DataBinding.FieldName = 'namatoko'
        Options.Editing = False
        Width = 302
      end
      object cxGrid2DBTableView1status: TcxGridDBColumn
        DataBinding.FieldName = 'status'
        Options.Editing = False
        Width = 151
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object BtnDelete: TButton
    Left = 136
    Top = 224
    Width = 97
    Height = 33
    Caption = 'Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = BtnDeleteClick
  end
  object ListBox1: TListBox
    Left = 64
    Top = 296
    Width = 121
    Height = 33
    ItemHeight = 13
    TabOrder = 7
    Visible = False
  end
  object ListBox2: TListBox
    Left = 64
    Top = 344
    Width = 121
    Height = 41
    ItemHeight = 13
    TabOrder = 8
    Visible = False
  end
  object HeaderQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from retur'
      'where kode=:koderetur'
      '')
    UpdateObject = UpdateHeader
    Left = 104
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'koderetur'
        ParamType = ptInput
      end>
    object HeaderQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HeaderQTglRetur: TDateTimeField
      FieldName = 'TglRetur'
      Required = True
    end
    object HeaderQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Required = True
      Size = 50
    end
    object HeaderQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object HeaderQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object HeaderQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object HeaderQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object HeaderQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object HeaderQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object HeaderQKodePO: TStringField
      FieldName = 'KodePO'
      Size = 10
    end
    object HeaderQStatus: TStringField
      FieldName = 'Status'
    end
    object HeaderQRetur: TBooleanField
      FieldName = 'Retur'
    end
    object HeaderQComplaint: TBooleanField
      FieldName = 'Complaint'
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select dr.* , b.nama, 0 as beda'
      'from detailretur dr, barang b'
      'where dr.barang=b.kode'
      'and koderetur=:koderetur')
    UpdateObject = UpdateDetail
    Left = 560
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'koderetur'
        ParamType = ptInput
        Value = ' '
      end>
    object DetailQKodeRetur: TStringField
      FieldName = 'KodeRetur'
      Required = True
      Size = 10
    end
    object DetailQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailQAlasan: TMemoField
      FieldName = 'Alasan'
      Required = True
      BlobType = ftMemo
    end
    object DetailQPotongPO: TBooleanField
      FieldName = 'PotongPO'
    end
    object DetailQPesanUlang: TBooleanField
      FieldName = 'PesanUlang'
    end
    object DetailQTglTargetTukar: TDateTimeField
      FieldName = 'TglTargetTukar'
    end
    object DetailQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object DetailQStatusRetur: TStringField
      FieldName = 'StatusRetur'
      Size = 50
    end
    object DetailQbeda: TIntegerField
      FieldName = 'beda'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = HeaderQ
    Left = 168
    Top = 216
  end
  object DataSource2: TDataSource
    DataSet = DetailQ
    Left = 600
    Top = 192
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from retur order by kode desc')
    Left = 40
    Top = 216
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailPOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select udpo.kodepo,ub.kode barang,ub.nama, sum(udb.jumlahbeli) a' +
        's jumlahbeli,udb.status,ub.satuan,'
      'isnull('
      'sum(udb.jumlahbeli)-('
      'select sum(dlpb.jumlah)'
      'from detaillpb dlpb, barang b, lpb, po'
      
        'where dlpb.barang=b.kode and lpb.kode=dlpb.kodelpb and lpb.po=po' +
        '.kode '
      'and po.kode=udpo.kodepo and dlpb.barang=ub.kode'
      '),sum(udb.jumlahbeli)) as sisa_pesan'
      'from detailpo udpo,daftarbeli udb, barang ub'
      'where udpo.kodedaftarbeli=udb.kode and udb.barang=ub.kode'
      ''
      'and udpo.kodepo=:kodepo'
      'group by udpo.kodepo,ub.kode ,ub.nama, udb.status,ub.satuan'
      '')
    Left = 216
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodepo'
        ParamType = ptInput
      end>
    object DetailPOQkodepo: TStringField
      FieldName = 'kodepo'
      Required = True
      Size = 10
    end
    object DetailPOQbarang: TStringField
      FieldName = 'barang'
      Required = True
      Size = 10
    end
    object DetailPOQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object DetailPOQjumlahbeli: TIntegerField
      FieldName = 'jumlahbeli'
    end
    object DetailPOQstatus: TStringField
      FieldName = 'status'
      Required = True
      Size = 50
    end
    object DetailPOQsatuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
    object DetailPOQsisa_pesan: TIntegerField
      FieldName = 'sisa_pesan'
    end
  end
  object UpdateHeader: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, TglRetur, Pelaksana, Keterangan, CreateDate, Create' +
        'By, Operator, TglEntry, TglCetak, KodePO, Status, Retur, Complai' +
        'nt'#13#10'from retur'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update retur'
      'set'
      '  Kode = :Kode,'
      '  TglRetur = :TglRetur,'
      '  Pelaksana = :Pelaksana,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KodePO = :KodePO,'
      '  Status = :Status,'
      '  Retur = :Retur,'
      '  Complaint = :Complaint'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into retur'
      
        '  (Kode, TglRetur, Pelaksana, Keterangan, CreateDate, CreateBy, ' +
        'Operator, TglEntry, TglCetak, KodePO, Status, Retur, Complaint)'
      'values'
      
        '  (:Kode, :TglRetur, :Pelaksana, :Keterangan, :CreateDate, :Crea' +
        'teBy, :Operator, :TglEntry, :TglCetak, :KodePO, :Status, :Retur,' +
        ' :Complaint)')
    DeleteSQL.Strings = (
      'delete from retur'
      'where'
      '  Kode = :OLD_Kode')
    Left = 136
    Top = 224
  end
  object UpdateDetail: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeRetur, Barang, Jumlah, Alasan, PotongPO, PesanUlang, ' +
        'TglTargetTukar, StatusRetur'#13#10'from detailretur'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update detailretur'
      'set'
      '  KodeRetur = :KodeRetur,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  Alasan = :Alasan,'
      '  PotongPO = :PotongPO,'
      '  PesanUlang = :PesanUlang,'
      '  TglTargetTukar = :TglTargetTukar,'
      '  StatusRetur = :StatusRetur'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into detailretur'
      
        '  (KodeRetur, Barang, Jumlah, Alasan, PotongPO, PesanUlang, TglT' +
        'argetTukar, StatusRetur)'
      'values'
      
        '  (:KodeRetur, :Barang, :Jumlah, :Alasan, :PotongPO, :PesanUlang' +
        ', :TglTargetTukar, :StatusRetur)')
    DeleteSQL.Strings = (
      'delete from detailretur'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    Left = 640
    Top = 192
  end
  object SembarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 392
    Top = 144
  end
  object SembarangQ_2: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 424
    Top = 144
  end
  object KodeDbQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from daftarbeli order by kode desc')
    Left = 232
    Top = 248
    object KodeDbQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli')
    UpdateObject = UpdateDaftarBeli
    Left = 272
    Top = 248
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DaftarBeliQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object DaftarBeliQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object DaftarBeliQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object DaftarBeliQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object DaftarBeliQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object DaftarBeliQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object DaftarBeliQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object DaftarBeliQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object DaftarBeliQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object DaftarBeliQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object DaftarBeliQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object DaftarBeliQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object DaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
  object UpdateDaftarBeli: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplie' +
        'r2, Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ke' +
        'terangan3, Termin'#13#10'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Harga1 = :Harga1,'
      '  Termin1 = :Termin1,'
      '  Keterangan1 = :Keterangan1,'
      '  Supplier2 = :Supplier2,'
      '  Harga2 = :Harga2,'
      '  Termin2 = :Termin2,'
      '  Keterangan2 = :Keterangan2,'
      '  Supplier3 = :Supplier3,'
      '  Harga3 = :Harga3,'
      '  Termin3 = :Termin3,'
      '  Keterangan3 = :Keterangan3,'
      '  Termin = :Termin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplier2, ' +
        'Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ketera' +
        'ngan3, Termin)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Harga1, :Termin1, :Keteran' +
        'gan1, :Supplier2, :Harga2, :Termin2, :Keterangan2, :Supplier3, :' +
        'Harga3, :Termin3, :Keterangan3, :Termin)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 304
    Top = 248
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select po.kode as kode_po, po.tglkirim, s.namatoko, po.status '
      'from po, supplier s '
      'where po.supplier=s.kode and status<>'#39'NEW PO'#39' and status<>'#39'PAID'#39)
    Left = 296
    Top = 320
    object ViewQkode_po: TStringField
      FieldName = 'kode_po'
      Required = True
      Size = 10
    end
    object ViewQtglkirim: TDateTimeField
      FieldName = 'tglkirim'
      Required = True
    end
    object ViewQnamatoko: TStringField
      FieldName = 'namatoko'
      Required = True
      Size = 50
    end
    object ViewQstatus: TStringField
      FieldName = 'status'
      Required = True
      Size = 50
    end
  end
  object DataSourceView: TDataSource
    DataSet = ViewQ
    Left = 344
    Top = 320
  end
end
