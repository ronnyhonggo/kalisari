unit Complaint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxVGrid, cxDBVGrid, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxInplaceContainer, SDEngine, cxContainer, Menus, StdCtrls, cxButtons,
  cxTextEdit, cxMaskEdit, cxButtonEdit, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TComplaintFm = class(TForm)
    HeaderQ: TSDQuery;
    DetailQ: TSDQuery;
    HeaderQKode: TStringField;
    HeaderQTglRetur: TDateTimeField;
    HeaderQPelaksana: TStringField;
    HeaderQKeterangan: TMemoField;
    HeaderQCreateDate: TDateTimeField;
    HeaderQCreateBy: TStringField;
    HeaderQOperator: TStringField;
    HeaderQTglEntry: TDateTimeField;
    HeaderQTglCetak: TDateTimeField;
    HeaderQKodePO: TStringField;
    DetailQKodeRetur: TStringField;
    DetailQBarang: TStringField;
    DetailQJumlah: TIntegerField;
    DetailQAlasan: TMemoField;
    DetailQPotongPO: TBooleanField;
    DetailQPesanUlang: TBooleanField;
    DetailQTglTargetTukar: TDateTimeField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1Alasan: TcxGridDBColumn;
    cxGrid1DBTableView1PotongPO: TcxGridDBColumn;
    cxGrid1DBTableView1PesanUlang: TcxGridDBColumn;
    cxGrid1DBTableView1TglTargetTukar: TcxGridDBColumn;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    SearchBtn: TcxButton;
    Button1: TButton;
    KodeQ: TSDQuery;
    DetailPOQ: TSDQuery;
    DetailPOQkodepo: TStringField;
    DetailPOQbarang: TStringField;
    DetailPOQnama: TStringField;
    DetailPOQjumlahbeli: TIntegerField;
    DetailPOQstatus: TStringField;
    DetailPOQsatuan: TStringField;
    DetailPOQsisa_pesan: TIntegerField;
    DetailQnama: TStringField;
    cxGrid1DBTableView1nama: TcxGridDBColumn;
    UpdateHeader: TSDUpdateSQL;
    UpdateDetail: TSDUpdateSQL;
    KodeQkode: TStringField;
    SembarangQ: TSDQuery;
    DetailQStatusRetur: TStringField;
    SembarangQ_2: TSDQuery;
    HeaderQStatus: TStringField;
    HeaderQRetur: TBooleanField;
    HeaderQComplaint: TBooleanField;
    cxDBVerticalGrid1TglRetur: TcxDBEditorRow;
    cxDBVerticalGrid1Pelaksana: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxDBVerticalGrid1KodePO: TcxDBEditorRow;
    cxDBVerticalGrid1Retur: TcxDBEditorRow;
    cxDBVerticalGrid1Complaint: TcxDBEditorRow;
    KodeDbQ: TSDQuery;
    KodeDbQkode: TStringField;
    DaftarBeliQ: TSDQuery;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQSupplier1: TStringField;
    DaftarBeliQHarga1: TCurrencyField;
    DaftarBeliQTermin1: TIntegerField;
    DaftarBeliQKeterangan1: TMemoField;
    DaftarBeliQSupplier2: TStringField;
    DaftarBeliQHarga2: TCurrencyField;
    DaftarBeliQTermin2: TIntegerField;
    DaftarBeliQKeterangan2: TMemoField;
    DaftarBeliQSupplier3: TStringField;
    DaftarBeliQHarga3: TCurrencyField;
    DaftarBeliQTermin3: TIntegerField;
    DaftarBeliQKeterangan3: TMemoField;
    DaftarBeliQTermin: TIntegerField;
    UpdateDaftarBeli: TSDUpdateSQL;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    ViewQ: TSDQuery;
    DataSourceView: TDataSource;
    cxGrid2DBTableView1kode_po: TcxGridDBColumn;
    cxGrid2DBTableView1tglkirim: TcxGridDBColumn;
    cxGrid2DBTableView1namatoko: TcxGridDBColumn;
    cxGrid2DBTableView1status: TcxGridDBColumn;
    ViewQkode_po: TStringField;
    ViewQtglkirim: TDateTimeField;
    ViewQnamatoko: TStringField;
    ViewQstatus: TStringField;
    BtnDelete: TButton;
    ListBox1: TListBox;
    ListBox2: TListBox;
    DetailQbeda: TIntegerField;
    cxGrid1DBTableView1beda: TcxGridDBColumn;
    procedure cxDBVerticalGrid1KodePOEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure KodeEditPropertiesChange(Sender: TObject);
    procedure KodeEditDblClick(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormActivate(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure cxGrid1DBTableView1JumlahPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ComplaintFm: TComplaintFm;

implementation
uses menuutama, Browse;

{$R *.dfm}

procedure TComplaintFm.cxDBVerticalGrid1KodePOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
MenuUtamaFm.param_sql:='select po.kode as kode_po, po.tglkirim, s.namatoko, po.status from po, supplier s where po.supplier=s.kode and status<>'+QuotedStr('NEW PO')+' and status<>'+QuotedStr('PAID')+'';
//ShowMessage(MenuUtamaFm.param_sql);
BrowseFm.ShowModal;

HeaderQ.Open;
HeaderQ.Edit;
HeaderQKodePO.AsString:=BrowseFm.kode;

DetailPOQ.Close;
DetailPOQ.ParamByName('kodepo').AsString := BrowseFm.kode;
DetailPOQ.ExecSQL;
DetailPOQ.Open;

DetailQ.Close;
DetailQ.ParamByName('koderetur').AsString := KodeEdit.Text;
DetailQ.ExecSQL;
DetailQ.Open;

while not DetailPOQ.Eof do
  begin
     DetailQ.Insert;
     DetailQ.Append;
     DetailQnama.AsString:=DetailPOQnama.AsString;
     DetailQKodeRetur.AsString:=KodeEdit.Text;
     DetailQBarang.AsString:=DetailPOQbarang.AsString;
     //CobaQPesan.AsInteger:=DetailPOQjumlahbeli.AsInteger;
//     CobaQJumlah.AsInteger:=0;
  //   CobaQsatuan.AsString:=DetailPOQsatuan.AsString;
     DetailQJumlah.AsInteger:=DetailPOQsisa_pesan.AsInteger;
     DetailQPotongPO.AsBoolean:=false;
     DetailQPesanUlang.AsBoolean:=false;
     DetailQAlasan.AsString:=' ';


     DetailQ.Post;
    DetailPOQ.Next;
  end;//=========
end;

procedure TComplaintFm.FormCreate(Sender: TObject);
begin
  KodeEdit.Text:='Insert Baru';

  HeaderQ.Close;
  HeaderQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Append;
  HeaderQTglRetur.AsDateTime:=now;

  DetailQ.Close;
  DetailQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  DetailQ.ExecSQL;
  DetailQ.Open;
  DetailQ.Insert;
  DetailQ.Append;
end;

procedure TComplaintFm.Button1Click(Sender: TObject);
var belum,belum_2:boolean;
tambah,kurang,jumlah_kumulatif:integer;
begin
  if (KodeEdit.Text='Insert Baru') or (KodeEdit.Text='') then
  begin
    try
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        HeaderQKode.AsString:=FormatFloat('0000000000',1);
      end
      else
      begin
        HeaderQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+1);
      end;
      KodeQ.Close;
      HeaderQ.Edit;
      HeaderQStatus.AsString:='ONPROCESS';
      HeaderQ.Post;

      DetailQ.Open;
      DetailQ.First;
      while not DetailQ.Eof do
      begin
        DetailQ.Edit;
        if (DetailQJumlah.AsInteger<=0) then
            DetailQ.Delete
        else
        begin
           DetailQKodeRetur.AsString:=HeaderQKode.AsString;
           DetailQ.Post;
            DetailQ.Next;
        end;
      end;//=====

      DetailQ.First;
      //ubah kodebonbarang di detail==================================
      while not DetailQ.Eof do
      begin
        DetailQ.Edit;
         DetailQKodeRetur.AsString:=HeaderQKode.AsString;
         if DetailQPotongPO.AsBoolean=true then
            DetailQStatusRetur.AsString:='FINISHED'
            else
            DetailQStatusRetur.AsString:='ONPROCESS';

         DetailQ.Post;
         DetailQ.Next;
      end;//===========================================================


        except
        on E : Exception do
          ShowMessage(E.ClassName+' error raised 1, with message : '+E.Message);
    end
  end;

  MenuUtamaFm.DataBase1.StartTransaction;
  try
    HeaderQ.ApplyUpdates;
    MenuUtamaFm.DataBase1.Commit;
    HeaderQ.CommitUpdates;

    MenuUtamaFm.DataBase1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.DataBase1.Commit;
      DetailQ.CommitUpdates;

      //update stok barang==========================================================
      //if ((KodeEdit.Text='Insert Baru') or (KodeEdit.Text='')) and (HeaderQRetur.AsBoolean=true) then
      if (HeaderQRetur.AsBoolean=true) then
      begin
          DetailQ.First;
          while not DetailQ.Eof do
          begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update barang set jumlah=jumlah-('+IntToStr(DetailQbeda.AsInteger)+') where kode='+QuotedStr(DetailQBarang.AsString));
            //ShowMessage(SembarangQ.SQL.Text);
            SembarangQ.ExecSQL;
            DetailQ.Next;
          end;
      end;
      //================================================================================

      //FINISHKAN DETAIL-DETAIL
      DetailQ.First;
      while not DetailQ.Eof do
      begin
        if DetailQPotongPO.AsBoolean=true then
        begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('select dpo.*, db.barang from detailpo dpo, daftarbeli db where dpo.kodedaftarbeli=db.kode and dpo.kodepo='+QuotedStr(HeaderQKodePO.AsString)+' and barang='+QuotedStr(DetailQBarang.AsString));
            SembarangQ.ExecSQL;
            SembarangQ.Open;

            while not SembarangQ.Eof do
            begin
                SembarangQ_2.SQL.Clear;
                SembarangQ_2.SQL.Add('update detailpo set statuspo='+QuotedStr('FINISHED')+' where kodepo='+QuotedStr(HeaderQKodePO.AsString) +' and kodedaftarbeli='+QuotedStr(SembarangQ.fieldbyname('kodedaftarbeli').AsString));
                SembarangQ_2.ExecSQL;

                SembarangQ_2.SQL.Clear;
                SembarangQ_2.SQL.Add('update daftarbeli set status='+QuotedStr('FINISHED')+' where kode='+QuotedStr(SembarangQ.fieldbyname('kodedaftarbeli').AsString));
                SembarangQ_2.ExecSQL;

                SembarangQ.Next;
            end;
        end;


        if (DetailQPesanUlang.AsBoolean=true) and ((KodeEdit.Text='Insert Baru') or (KodeEdit.Text='')) then
        begin
            SembarangQ_2.SQL.Clear;
            SembarangQ_2.SQL.Add('select dpo.kodepo,db.barang, db.bonbarang, db.kode as beli, db.jumlahbeli from detailpo dpo, daftarbeli db where dpo.kodedaftarbeli=db.kode and dpo.kodepo='+QuotedStr(HeaderQKodePO.AsString)+'  and db.barang='+QuotedStr(DetailQBarang.AsString)+' order by db.bonbarang desc');
            SembarangQ_2.ExecSQL;
            SembarangQ_2.Open;

            kurang:=DetailQJumlah.AsInteger;
            while not SembarangQ_2.Eof do
            begin
                if kurang > 0 then
                begin
                    //insert daftar beli ==================================================
                    if (kurang-StrToInt(SembarangQ_2.fieldbyname('jumlahbeli').Value))>=0 then
                    begin
                        jumlah_kumulatif:=SembarangQ_2.fieldbyname('jumlahbeli').Value;
                        kurang:=kurang-StrToInt(SembarangQ_2.fieldbyname('jumlahbeli').Value)
                    end
                    else
                    begin
                        jumlah_kumulatif:=kurang;
                        kurang:=-1;
                    end;

                    DaftarBeliQ.Close;
                     DaftarBeliQ.Open;
                     DaftarBeliQ.Insert;
                     DaftarBeliQ.Edit;

                      KodedbQ.Open;
                      if KodedbQ.IsEmpty then
                      begin
                        DaftarBeliQKode.AsString:=FormatFloat('0000000000',1);
                      end
                      else
                      begin

                        DaftarBeliQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodedbQkode.AsString)+1);
                      end;
                      KodedbQ.Close;

                      DaftarBeliQBarang.AsString:=DetailQBarang.AsString;
                      DaftarBeliQBonBarang.AsString:=SembarangQ_2.fieldbyname('bonbarang').Value;
                      DaftarBeliQJumlahBeli.AsInteger:=jumlah_kumulatif;

                      DaftarBeliQGrandTotal.AsInteger:=0;
                      DaftarBeliQStatus.AsString:='NEW DB';

                    SembarangQ.SQL.Clear;
                    SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(DetailQBarang.AsString));
                    SembarangQ.ExecSQL;
                    SembarangQ.Open;
                    DaftarBeliQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
                    SembarangQ.Close;

                    SembarangQ.SQL.Clear;
                    SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(DetailQBarang.AsString));
                    SembarangQ.ExecSQL;
                    SembarangQ.Open;
                    DaftarBeliQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
                    SembarangQ.Close;

                    SembarangQ.SQL.Clear;
                    SembarangQ.SQL.Add('select top 1 hargasatuan,supplier from daftarbeli where barang='+QuotedStr(DetailQBarang.AsString)+' order by kode desc');
                    SembarangQ.ExecSQL;
                    SembarangQ.Open;
                    try
                      tambah:=SembarangQ.fieldbyname('hargasatuan').AsInteger;
                      DaftarBeliQLastSupplier.AsString:=SembarangQ.fieldbyname('supplier').AsString;
                    except
                      tambah:=0;
                    end;
                    DaftarBeliQHargaLast.AsInteger:=tambah;
                    if DaftarBeliQLastSupplier.AsString='' then
                      DaftarBeliQLastSupplier.AsString:='0000000000';

                    DaftarBeliQSupplier.AsString:='0000000000';
                    SembarangQ.Close;

                     DaftarBeliQ.Post;

                     MenuUtamaFm.DataBase1.StartTransaction;
                        try
                          DaftarBeliQ.ApplyUpdates;
                          MenuUtamaFm.DataBase1.Commit;
                          DaftarBeliQ.CommitUpdates;
                        except
                          on E : Exception do
                            ShowMessage(E.ClassName+'insert daftar beli Gagal, with message : '+E.Message);
                          { MenuUtamaFm.DataBase1.Rollback;
                           DaftarBeliQ.RollbackUpdates;
                            BarangQ.RollbackUpdates;
                            HeaderQ.RollbackUpdates;
                            CobaQ.RollbackUpdates; }
                        end;
                    DaftarBeliQ.Close;
                    //====================================================================


                end;
                SembarangQ_2.Next;

            end;
        end;
        DetailQ.Next;
      end;

      //FINISHKAN HEADER PO ============================================================
      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select * from detailpo where kodepo='+HeaderQKodePO.AsString);
      SembarangQ.Open;
      belum:=false;
      while not SembarangQ.Eof do
      begin
         if SembarangQ.FieldByName('statuspo').Value<>'FINISHED' then
            belum:=true;
         SembarangQ.Next;
      end;
      if belum=false then
      begin
        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('update po set status='+QuotedStr('FINISHED')+' where kode='+HeaderQKodePO.AsString);
        SembarangQ.ExecSQL;
      end;

      //FINISHKAN HEADER retur ============================================================

      SembarangQ.SQL.Clear;
      SembarangQ.SQL.Add('select * from detailretur where koderetur='+QuotedStr(HeaderQKode.AsString));
      SembarangQ.Open;
      belum_2:=false;
      while not SembarangQ.Eof do
      begin
         if SembarangQ.FieldByName('statusretur').Value<>'FINISHED' then
            belum_2:=true;
         SembarangQ.Next;
      end;
      if belum_2=false then
      begin
        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('update retur set status='+QuotedStr('FINISHED')+' where kode='+HeaderQKode.AsString);
        SembarangQ.ExecSQL;
      end;

      ShowMessage('Retur/complaint dengan kode '+ HeaderQKode.AsString +' telah disimpan');
      ViewQ.Refresh;
       HeaderQ.Edit;
      HeaderQTglRetur.AsDateTime:=now;

      KodeEditPropertiesButtonClick(sender,1);
    except
        ShowMessage('Penyimpanan detail Gagal, silahkan coba kembali');
        MenuUtamaFm.DataBase1.Rollback;
        DetailQ.RollbackUpdates;
        HeaderQ.RollbackUpdates;
        KodeEditPropertiesButtonClick(sender,1);

    end;

  except
       ShowMessage('Penyimpanan header Gagal, silahkan coba kembali');
        MenuUtamaFm.DataBase1.Rollback;
        DetailQ.RollbackUpdates;
        HeaderQ.RollbackUpdates;
        KodeEditPropertiesButtonClick(sender,1);
  end;
end;

procedure TComplaintFm.SearchBtnClick(Sender: TObject);
begin
  MenuUtamaFm.param_sql:='select * from retur order by kode desc';
  BrowseFm.ShowModal;
  KodeEdit.Text:= MenuUtamaFm.param_kodebon;
//  KodeEdit.Text:=ViewQKode.AsString;

  HeaderQ.Close;
  HeaderQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;

  DetailQ.Close;
  DetailQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  DetailQ.ExecSQL;
  DetailQ.Open;
  DetailQ.Edit;
end;

procedure TComplaintFm.cxGrid2DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
Button1.Enabled:=false;
cxGrid1.Enabled:=false;
cxgrid2.Enabled:=false;
cxDBVerticalGrid1.Enabled:=false;

if MenuUtamaFm.UserQUpdateKomplainRetur.AsBoolean=true then
begin
  Button1.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  cxGrid1.Enabled:=true;
  cxgrid2.Enabled:=true;
end;

HeaderQKodePO.AsString:=ViewQkode_po.AsString;
HeaderQ.Open;
HeaderQ.Edit;
HeaderQRetur.AsBoolean:=false;
HeaderQComplaint.AsBoolean:=false;

DetailPOQ.Close;
DetailPOQ.ParamByName('kodepo').AsString := HeaderQKodePO.AsString;
DetailPOQ.ExecSQL;
DetailPOQ.Open;


DetailQ.Close;
DetailQ.Open;
while not DetailPOQ.Eof do
  begin

     DetailQ.Insert;
     DetailQ.Append;
     DetailQnama.AsString:=DetailPOQnama.AsString;
     DetailQKodeRetur.AsString:=KodeEdit.Text;
     DetailQBarang.AsString:=DetailPOQbarang.AsString;
     DetailQbeda.AsInteger:=0;
     
     if DetailPOQsisa_pesan.AsInteger<0 then
      DetailQJumlah.AsInteger:=0
      else
     DetailQJumlah.AsInteger:=DetailPOQsisa_pesan.AsInteger;

     DetailQPotongPO.AsBoolean:=false;
     DetailQPesanUlang.AsBoolean:=false;
     DetailQAlasan.AsString:=' ';


     DetailQ.Post;
    DetailPOQ.Next;
  end;//=========
end;

procedure TComplaintFm.KodeEditPropertiesChange(Sender: TObject);
begin
  HeaderQ.Close;
  HeaderQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.edit;

  DetailQ.Close;
  DetailQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  DetailQ.ExecSQL;
  DetailQ.Open;
  
end;

procedure TComplaintFm.KodeEditDblClick(Sender: TObject);
begin
  KodeEdit.Text:='Insert Baru';

  HeaderQ.Close;
  HeaderQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Append;

  DetailQ.Close;
  DetailQ.ParamByName('koderetur').AsString := KodeEdit.Text;
  DetailQ.ExecSQL;
  DetailQ.Open;
  DetailQ.Insert;
  DetailQ.Append;
end;

procedure TComplaintFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='Insert Baru';

  HeaderQ.Close;
  HeaderQ.ParamByName('koderetur').AsString := '';
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.edit;
  HeaderQTglRetur.AsDateTime:=now;
  HeaderQRetur.AsBoolean:=false;
  HeaderQComplaint.AsBoolean:=false;

  DetailQ.Close;
  DetailQ.ParamByName('koderetur').AsString := '';
  DetailQ.ExecSQL;
  DetailQ.Open;

  ViewQ.Close;
  ViewQ.Open;
end;

procedure TComplaintFm.FormActivate(Sender: TObject);
begin
ViewQ.Close;
ViewQ.Open;
ViewQ.Refresh;

Button1.Enabled:=false;
cxGrid1.Enabled:=false;
cxgrid2.Enabled:=false;
cxDBVerticalGrid1.Enabled:=false;

if MenuUtamaFm.UserQInsertKomplainNRetur.AsBoolean=true then
begin
  Button1.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  cxGrid1.Enabled:=true;
end;

if MenuUtamaFm.UserQUpdateKomplainRetur.AsBoolean=true then
  cxgrid2.Enabled:=true;

end;

procedure TComplaintFm.BtnDeleteClick(Sender: TObject);
var i:integer;
returkah:boolean;
begin
  if MessageDlg('Hapus Retur/Complaint '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try
        if not HeaderQRetur.IsNull then
          returkah:=true
          else
          returkah:=false;

        ListBox1.Items.Clear;
        ListBox2.Items.Clear;
        
        MenuUtamaFm.Database1.StartTransaction;
        DetailQ.First;
        while not DetailQ.eof do
        begin
          ListBox1.Items.Add(DetailQBarang.AsString);
          ListBox2.Items.Add(DetailQJumlah.AsString);
          ShowMessage('a');
          DetailQ.Delete;
        end;
        DetailQ.ApplyUpdates;

       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       if returkah=true then
       begin
          for i:=0 to ListBox1.Items.Count-1 do
          begin
              SembarangQ.SQL.Clear;
              SembarangQ.SQL.Add('update barang set jumlah=jumlah+'+ ListBox2.Items[i]+' where kode='+ListBox1.Items[i]);
              ShowMessage(SembarangQ.SQL.Text);
              SembarangQ.ExecSQL;
          end;
      end;

       MessageDlg('Retur/Complaint telah dihapus.',mtInformation,[mbOK],0);
       
     except
       MenuUtamaFm.Database1.Rollback;
       HeaderQ.RollbackUpdates;
       DetailQ.RollbackUpdates;

       MessageDlg('Retur/Complaint pernah/sedang proses bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;

     KodeEditPropertiesButtonClick(sender,1);
  end;
end;

procedure TComplaintFm.cxGrid1DBTableView1JumlahPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
DetailQBEDA.AsInteger:=DetailQBEDA.AsInteger+DisplayValue-DetailQJumlah.AsInteger;
DetailQJumlah.AsInteger:=DisplayValue;
end;

end.
