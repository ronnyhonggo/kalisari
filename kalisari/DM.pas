unit DM;

interface

uses
  SysUtils, Classes, DB, SDEngine;

type
  TDMFm = class(TDataModule)
    GetDateQ: TSDQuery;
    GetDateQNow: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
    function Fill(Str: string; Size: integer; CharToFill: char; FromLeft: boolean): string;
  end;

var
  DMFm: TDMFm;

implementation

{$R *.dfm}

{ TDMFm }

function TDMFm.Fill(Str: string; Size: integer; CharToFill: char;
  FromLeft: boolean): string;
var t: string;
    i: integer;
begin
  t:=Str;
  for i:=length(Str)+1 to Size do begin
    if FromLeft then t:=CharToFill+t
    else t:=t+CharToFill;
  end;
  result:=t;

end;

end.
