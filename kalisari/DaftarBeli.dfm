object DaftarBeliFm: TDaftarBeliFm
  Left = 20
  Top = 105
  BorderStyle = bsDialog
  Caption = 'Daftar Beli'
  ClientHeight = 685
  ClientWidth = 1263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 464
    Top = 32
    Width = 84
    Height = 16
    Caption = 'Nama Barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 32
    Width = 62
    Height = 16
    Caption = 'Daftar Beli'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 240
    Top = 32
    Width = 71
    Height = 16
    Caption = 'Bon Barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 696
    Top = 32
    Width = 102
    Height = 16
    Caption = 'Status Daftar Beli'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 56
    Top = 576
    Width = 204
    Height = 16
    Caption = 'Centang Supplier yang dipilih'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = False
  end
  object TxtBarang: TEdit
    Left = 560
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Txtbon: TEdit
    Left = 320
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Button2: TButton
    Left = 944
    Top = 16
    Width = 129
    Height = 33
    Caption = 'Search'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = Button2Click
  end
  object TxtBeli: TEdit
    Left = 96
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object ComboBox1: TComboBox
    Left = 808
    Top = 24
    Width = 105
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = 'ONPROCESS'
    Items.Strings = (
      'ALL'
      'NEW DB'
      'ONPROCESS'
      'PO'
      'CNC'
      'FINISHED')
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1263
    Height = 521
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.First.Enabled = False
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Enabled = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellClick = tr
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsSelection.MultiSelect = True
      object cxGrid1DBTableView1urgent: TcxGridDBColumn
        Caption = 'Urgent'
        DataBinding.FieldName = 'urgent'
        Visible = False
        Width = 51
      end
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        Caption = 'Kode Beli'
        DataBinding.FieldName = 'Kode'
        Visible = False
        Options.Editing = False
        Width = 78
      end
      object cxGrid1DBTableView1BonBarang: TcxGridDBColumn
        Caption = 'Bon Barang'
        DataBinding.FieldName = 'BonBarang'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1BonBarangPropertiesButtonClick
        Options.Editing = False
        Width = 84
      end
      object cxGrid1DBTableView1Barang: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        Visible = False
      end
      object cxGrid1DBTableView1nama: TcxGridDBColumn
        Caption = 'Nama Barang'
        DataBinding.FieldName = 'nama'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Options.Editing = False
        Width = 118
      end
      object cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn
        Caption = 'Beli'
        DataBinding.FieldName = 'JumlahBeli'
        Options.Editing = False
        Width = 41
      end
      object cxGrid1DBTableView1satuan: TcxGridDBColumn
        Caption = 'Satuan'
        DataBinding.FieldName = 'satuan'
        Options.Editing = False
        Width = 52
      end
      object cxGrid1DBTableView1Supplier: TcxGridDBColumn
        DataBinding.FieldName = 'Supplier'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Visible = False
        Options.Editing = False
      end
      object cxGrid1DBTableView1Supp_skr_R: TcxGridDBColumn
        Caption = 'Supplier Skr'
        DataBinding.FieldName = 'DetailSupplier'
        Width = 104
      end
      object cxGrid1DBTableView1Termin: TcxGridDBColumn
        DataBinding.FieldName = 'Termin'
        Width = 50
      end
      object cxGrid1DBTableView1HargaSatuan: TcxGridDBColumn
        Caption = 'Harga Satuan'
        DataBinding.FieldName = 'HargaSatuan'
        Width = 92
      end
      object cxGrid1DBTableView1CashNCarry: TcxGridDBColumn
        Caption = 'CNC'
        DataBinding.FieldName = 'CashNCarry'
        Width = 36
      end
      object cxGrid1DBTableView1GrandTotal: TcxGridDBColumn
        DataBinding.FieldName = 'GrandTotal'
        Options.Editing = False
        Width = 86
      end
      object cxGrid1DBTableView1HargaMin: TcxGridDBColumn
        Caption = 'Harga Last 2'
        DataBinding.FieldName = 'HargaLast2'
        Options.Editing = False
        Width = 82
      end
      object cxGrid1DBTableView1HargaMax: TcxGridDBColumn
        Caption = 'Harga Last 3'
        DataBinding.FieldName = 'HargaLast3'
        Options.Editing = False
        Width = 87
      end
      object cxGrid1DBTableView1HargaLast: TcxGridDBColumn
        Caption = 'Harga Last'
        DataBinding.FieldName = 'HargaLast'
        Options.Editing = False
        Width = 81
      end
      object cxGrid1DBTableView1Last_Sup_R: TcxGridDBColumn
        Caption = 'Last Supplier'
        DataBinding.FieldName = 'Detaillastsupp'
        Options.Editing = False
        Width = 94
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalLast'
        Width = 93
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableViewSingleSupplier: TcxGridDBColumn
        Caption = 'Single'
        DataBinding.FieldName = 'singlesupplier'
        Width = 62
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ListBox1: TListBox
    Left = 1032
    Top = 656
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 6
    Visible = False
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 666
    Width = 1263
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object RbtSingle: TRadioButton
    Left = 24
    Top = 552
    Width = 137
    Height = 17
    Caption = 'Single Supplier'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    TabStop = True
    OnClick = RbtSingleClick
  end
  object RbtMUlti: TRadioButton
    Left = 168
    Top = 552
    Width = 137
    Height = 17
    Caption = 'Multiple Supplier'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnClick = RbtMUltiClick
  end
  object BtnSupSama: TButton
    Left = 24
    Top = 600
    Width = 121
    Height = 57
    Caption = 'Generate Supplier Sama'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    WordWrap = True
    OnClick = BtnSupSamaClick
  end
  object BtnSupBanyak: TButton
    Left = 168
    Top = 600
    Width = 137
    Height = 57
    Caption = 'Generate Banyak Supplier'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    WordWrap = True
    OnClick = BtnSupBanyakClick
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 320
    Top = 544
    Width = 225
    Height = 137
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 88
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 12
    DataController.DataSource = DataSource2
    Version = 1
    object cxDBVerticalGrid1NamaSup1: TcxDBEditorRow
      Properties.Caption = 'Supplier 1'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1namasup1EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier1'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DetailSupplier1: TcxDBEditorRow
      Properties.Caption = 'Detail'
      Properties.DataBinding.FieldName = 'DetailSupplier1'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Harga1: TcxDBEditorRow
      Properties.Caption = 'Harga'
      Properties.DataBinding.FieldName = 'Harga1'
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Termin1: TcxDBEditorRow
      Properties.Caption = 'Termin'
      Properties.DataBinding.FieldName = 'Termin1'
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1beli1: TcxDBEditorRow
      Properties.Caption = 'beli'
      Properties.DataBinding.FieldName = 'beli1'
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1Keterangan1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan1'
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object cxDBVerticalGrid2: TcxDBVerticalGrid
    Left = 552
    Top = 544
    Width = 217
    Height = 137
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 91
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 13
    DataController.DataSource = DataSource2
    Version = 1
    object cxDBVerticalGrid2Namasup2_r: TcxDBEditorRow
      Properties.Caption = 'Supplier 2'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2Namasup2_rEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier2'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2DetailSupplier2: TcxDBEditorRow
      Properties.Caption = 'Detail'
      Properties.DataBinding.FieldName = 'DetailSupplier2'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2Harga2: TcxDBEditorRow
      Properties.Caption = 'Harga'
      Properties.DataBinding.FieldName = 'Harga2'
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid2Termin2: TcxDBEditorRow
      Properties.Caption = 'Termin'
      Properties.DataBinding.FieldName = 'Termin2'
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid2beli2: TcxDBEditorRow
      Properties.Caption = 'beli'
      Properties.DataBinding.FieldName = 'beli2'
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid2Keterangan2: TcxDBEditorRow
      Properties.Caption = 'Keterangan'
      Properties.DataBinding.FieldName = 'Keterangan2'
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object cxDBVerticalGrid3: TcxDBVerticalGrid
    Left = 784
    Top = 544
    Width = 225
    Height = 137
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 89
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 14
    DataController.DataSource = DataSource2
    Version = 1
    object cxDBVerticalGrid3NamaSup3: TcxDBEditorRow
      Properties.Caption = 'Supplier 3'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid3namasup3EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier3'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid3DetailSupplier3: TcxDBEditorRow
      Properties.Caption = 'Detail'
      Properties.DataBinding.FieldName = 'DetailSupplier3'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid3Harga3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Harga3'
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid3Termin3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Termin3'
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid3beli3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'beli3'
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid3Keterangan3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan3'
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object BtnSave: TButton
    Left = 1048
    Top = 568
    Width = 201
    Height = 73
    Caption = 'Save Daftar Beli'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    OnClick = BtnSaveClick
  end
  object Panel1: TPanel
    Left = -3
    Top = 520
    Width = 1369
    Height = 9
    Color = clGrayText
    TabOrder = 16
  end
  object CheckBox1: TcxCheckBox
    Left = 352
    Top = 512
    Caption = 'Pilih Supplier 1'
    ParentFont = False
    Properties.OnValidate = CheckBox1PropertiesValidate
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 17
    OnClick = CheckBox1Click
    Width = 129
  end
  object CheckBox2: TcxCheckBox
    Left = 576
    Top = 512
    Caption = 'Pilih Supplier 2'
    ParentFont = False
    Properties.OnValidate = CheckBox2PropertiesValidate
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 18
    OnClick = CheckBox2Click
    Width = 129
  end
  object CheckBox3: TcxCheckBox
    Left = 808
    Top = 512
    Caption = 'Pilih Supplier 3'
    ParentFont = False
    Properties.OnValidate = CheckBox3PropertiesValidate
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 19
    OnClick = CheckBox3Click
    Width = 129
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select db.*, b.nama, dbb.jumlahbeli as bbbeli, b.satuan'
      
        ',0 as beli1, 0 as beli2, 0 as beli3, dbb.urgent, b.singlesupplie' +
        'r '
      'from daftarbeli db,barang b,detailbonbarang dbb, bonbarang bb'
      
        'where db.barang=b.kode and dbb.kodebonbarang=db.bonbarang and bb' +
        '.kode=dbb.kodebonbarang'
      
        'and dbb.kodebarang=db.barang and (db.status='#39'ONPROCESS'#39' or db.st' +
        'atus='#39'NEW DB'#39') and bb.status<>'#39'NEW BB'#39
      'order by db.bonbarang, b.nama,db.kode desc')
    UpdateObject = SDUpdateSQL1
    Left = 320
    Top = 432
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object MasterQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object MasterQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object MasterQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object MasterQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object MasterQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object MasterQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object MasterQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object MasterQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object MasterQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object MasterQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object MasterQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object MasterQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object MasterQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object MasterQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object MasterQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object MasterQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object MasterQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object MasterQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object MasterQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object MasterQTermin: TIntegerField
      FieldName = 'Termin'
    end
    object MasterQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object MasterQbbbeli: TIntegerField
      FieldName = 'bbbeli'
    end
    object MasterQsatuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
    object MasterQbeli1: TIntegerField
      FieldName = 'beli1'
      Required = True
    end
    object MasterQbeli2: TIntegerField
      FieldName = 'beli2'
      Required = True
    end
    object MasterQbeli3: TIntegerField
      FieldName = 'beli3'
      Required = True
    end
    object MasterQurgent: TBooleanField
      FieldName = 'urgent'
      Required = True
    end
    object MasterQsinglesupplier: TBooleanField
      FieldName = 'singlesupplier'
    end
    object MasterQDetailSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 15
      Lookup = True
    end
    object MasterQDetailSupplier1: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier1'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier1'
      Lookup = True
    end
    object MasterQDetailSupplier2: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier2'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier2'
      Lookup = True
    end
    object MasterQDetailSupplier3: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier3'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier3'
      Lookup = True
    end
    object MasterQDetaillastsupp: TStringField
      FieldKind = fkLookup
      FieldName = 'Detaillastsupp'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'LastSupplier'
      Size = 15
      Lookup = True
    end
    object MasterQHargaLast2: TCurrencyField
      FieldName = 'HargaLast2'
    end
    object MasterQHargaLast3: TCurrencyField
      FieldName = 'HargaLast3'
    end
    object MasterQTanggalLast: TDateTimeField
      FieldName = 'TanggalLast'
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Supplier2, Supplier3, Harga1, Harga2,' +
        ' Harga3, Termin1, Termin2, Termin3, Keterangan1, Keterangan2, Ke' +
        'terangan3, Termin'
      'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Supplier2 = :Supplier2,'
      '  Supplier3 = :Supplier3,'
      '  Harga1 = :Harga1,'
      '  Harga2 = :Harga2,'
      '  Harga3 = :Harga3,'
      '  Termin1 = :Termin1,'
      '  Termin2 = :Termin2,'
      '  Termin3 = :Termin3,'
      '  Keterangan1 = :Keterangan1,'
      '  Keterangan2 = :Keterangan2,'
      '  Keterangan3 = :Keterangan3,'
      '  Termin = :Termin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Supplier2, Supplier3, Harga1, Harga2, Har' +
        'ga3, Termin1, Termin2, Termin3, Keterangan1, Keterangan2, Ketera' +
        'ngan3, Termin)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Supplier2, :Supplier3, :Ha' +
        'rga1, :Harga2, :Harga3, :Termin1, :Termin2, :Termin3, :Keteranga' +
        'n1, :Keterangan2, :Keterangan3, :Termin)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 384
    Top = 448
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 352
    Top = 440
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select top 1 kode from daftarbeli order by kode desc')
    Left = 192
    Top = 432
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object HargaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      '')
    Left = 160
    Top = 432
  end
  object SembarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select max(kode) from daftarbeli where barang=daftarbeli.barang ' +
        'and Status='#39'FINISH'#39)
    Left = 168
    Top = 408
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *, '#39#39' as nama1, '#39#39' as nama2, '#39#39' as nama3 from daftarbeli'
      '')
    UpdateObject = SDUpdateSQL2
    Left = 1128
    Top = 528
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1Supplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object SDQuery1Harga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object SDQuery1Termin1: TIntegerField
      FieldName = 'Termin1'
    end
    object SDQuery1Keterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object SDQuery1Supplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object SDQuery1Harga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object SDQuery1Termin2: TIntegerField
      FieldName = 'Termin2'
    end
    object SDQuery1Keterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object SDQuery1Supplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object SDQuery1Harga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object SDQuery1Termin3: TIntegerField
      FieldName = 'Termin3'
    end
    object SDQuery1Keterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object SDQuery1nama1: TStringField
      FieldName = 'nama1'
      Required = True
      Size = 1
    end
    object SDQuery1nama2: TStringField
      FieldName = 'nama2'
      Required = True
      Size = 1
    end
    object SDQuery1nama3: TStringField
      FieldName = 'nama3'
      Required = True
      Size = 1
    end
  end
  object SDUpdateSQL2: TSDUpdateSQL
    Left = 1168
    Top = 528
  end
  object GenerateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli'
      'where kode=:kodebeli')
    UpdateObject = GenerateUpdate
    Left = 696
    Top = 624
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodebeli'
        ParamType = ptInput
      end>
    object GenerateQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object GenerateQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object GenerateQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object GenerateQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object GenerateQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object GenerateQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object GenerateQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object GenerateQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object GenerateQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object GenerateQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object GenerateQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object GenerateQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object GenerateQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object GenerateQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object GenerateQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object GenerateQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object GenerateQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object GenerateQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object GenerateQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object GenerateQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object GenerateQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object GenerateQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object GenerateQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object GenerateQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object GenerateQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object GenerateQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
  object GenerateUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplie' +
        'r2, Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ke' +
        'terangan3, Termin'#13#10'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Harga1 = :Harga1,'
      '  Termin1 = :Termin1,'
      '  Keterangan1 = :Keterangan1,'
      '  Supplier2 = :Supplier2,'
      '  Harga2 = :Harga2,'
      '  Termin2 = :Termin2,'
      '  Keterangan2 = :Keterangan2,'
      '  Supplier3 = :Supplier3,'
      '  Harga3 = :Harga3,'
      '  Termin3 = :Termin3,'
      '  Keterangan3 = :Keterangan3,'
      '  Termin = :Termin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplier2, ' +
        'Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ketera' +
        'ngan3, Termin)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Harga1, :Termin1, :Keteran' +
        'gan1, :Supplier2, :Harga2, :Termin2, :Keterangan2, :Supplier3, :' +
        'Harga3, :Termin3, :Keterangan3, :Termin)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 728
    Top = 624
  end
  object SuppQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from supplier')
    Left = 624
    Top = 336
    object SuppQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SuppQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SuppQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SuppQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SuppQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SuppQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SuppQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SuppQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SuppQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SuppQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SuppQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SuppQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SuppQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SuppQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SuppQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SuppQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SuppQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SuppQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object DataSourceSupp: TDataSource
    DataSet = SuppQ
    Left = 664
    Top = 336
  end
  object DetailBonBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from DetailBonBarang')
    Left = 640
    Top = 400
    object DetailBonBarangQKodeBonBarang: TStringField
      FieldName = 'KodeBonBarang'
      Required = True
      Size = 10
    end
    object DetailBonBarangQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailBonBarangQJumlahDiminta: TIntegerField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object DetailBonBarangQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
    end
    object DetailBonBarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailBonBarangQStatusMinta: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object DetailBonBarangQStatusBeli: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object DetailBonBarangQUrgent: TBooleanField
      FieldName = 'Urgent'
      Required = True
    end
  end
  object MasterQ2: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select db.*, b.nama, dbb.jumlahbeli as bbbeli, b.satuan'
      
        ',0 as beli1, 0 as beli2, 0 as beli3, dbb.urgent, b.singlesupplie' +
        'r '
      'from daftarbeli db,barang b,detailbonbarang dbb, bonbarang bb'
      
        'where db.barang=b.kode and dbb.kodebonbarang=db.bonbarang and bb' +
        '.kode=dbb.kodebonbarang'
      
        'and dbb.kodebarang=db.barang and (db.status='#39'ONPROCESS'#39' or db.st' +
        'atus='#39'NEW DB'#39') and bb.status<>'#39'NEW BB'#39
      'order by db.bonbarang, b.nama,db.kode desc')
    UpdateObject = SDUpdateSQL3
    Left = 448
    Top = 432
    object MasterQ2Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQ2Barang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object MasterQ2BonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object MasterQ2HargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object MasterQ2HargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object MasterQ2HargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object MasterQ2LastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object MasterQ2JumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object MasterQ2HargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object MasterQ2Supplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object MasterQ2CashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object MasterQ2GrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object MasterQ2Status: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQ2Supplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object MasterQ2Supplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object MasterQ2Supplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object MasterQ2Harga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object MasterQ2Harga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object MasterQ2Harga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object MasterQ2Termin1: TIntegerField
      FieldName = 'Termin1'
    end
    object MasterQ2Termin2: TIntegerField
      FieldName = 'Termin2'
    end
    object MasterQ2Termin3: TIntegerField
      FieldName = 'Termin3'
    end
    object MasterQ2Keterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object MasterQ2Keterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object MasterQ2Keterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object MasterQ2Termin: TIntegerField
      FieldName = 'Termin'
    end
    object MasterQ2nama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object MasterQ2bbbeli: TIntegerField
      FieldName = 'bbbeli'
    end
    object MasterQ2satuan: TStringField
      FieldName = 'satuan'
      Required = True
      Size = 10
    end
    object MasterQ2beli1: TIntegerField
      FieldName = 'beli1'
      Required = True
    end
    object MasterQ2beli2: TIntegerField
      FieldName = 'beli2'
      Required = True
    end
    object MasterQ2beli3: TIntegerField
      FieldName = 'beli3'
      Required = True
    end
    object MasterQ2urgent: TBooleanField
      FieldName = 'urgent'
      Required = True
    end
    object MasterQ2singlesupplier: TBooleanField
      FieldName = 'singlesupplier'
    end
    object MasterQ2DetailSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 10
      Lookup = True
    end
    object MasterQ2DetailSupplier1: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier1'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier1'
      Lookup = True
    end
    object MasterQ2DetailSupplier3: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier3'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier3'
      Lookup = True
    end
    object MasterQ2DetailLastSupp: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailLastSupp'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'LastSupplier'
      Lookup = True
    end
    object MasterQ2DetailSupplier2: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailSupplier2'
      LookupDataSet = SuppQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier2'
      Lookup = True
    end
  end
  object DataSource2: TDataSource
    DataSet = MasterQ2
    Left = 488
    Top = 432
  end
  object SDUpdateSQL3: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Supplier2, Supplier3, Harga1, Harga2,' +
        ' Harga3, Termin1, Termin2, Termin3, Keterangan1, Keterangan2, Ke' +
        'terangan3, Termin'
      'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Supplier2 = :Supplier2,'
      '  Supplier3 = :Supplier3,'
      '  Harga1 = :Harga1,'
      '  Harga2 = :Harga2,'
      '  Harga3 = :Harga3,'
      '  Termin1 = :Termin1,'
      '  Termin2 = :Termin2,'
      '  Termin3 = :Termin3,'
      '  Keterangan1 = :Keterangan1,'
      '  Keterangan2 = :Keterangan2,'
      '  Keterangan3 = :Keterangan3,'
      '  Termin = :Termin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Supplier2, Supplier3, Harga1, Harga2, Har' +
        'ga3, Termin1, Termin2, Termin3, Keterangan1, Keterangan2, Ketera' +
        'ngan3, Termin)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Supplier2, :Supplier3, :Ha' +
        'rga1, :Harga2, :Harga3, :Termin1, :Termin2, :Termin3, :Keteranga' +
        'n1, :Keterangan2, :Keterangan3, :Termin)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 480
    Top = 400
  end
  object SDQuery2: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select max(kode) from daftarbeli '
      #9'where barang=daftarbeli.barang')
    Left = 384
    Top = 296
  end
end
