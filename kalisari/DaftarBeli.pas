unit DaftarBeli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, SDEngine,
  StdCtrls, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxButtonEdit,
  ExtCtrls, cxVGrid, cxDBVGrid, cxInplaceContainer, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, ComCtrls,
  cxContainer, cxCheckBox;

type
  TDaftarBeliFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    MasterQ: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    DataSource1: TDataSource;
    TxtBarang: TEdit;
    Txtbon: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button2: TButton;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1HargaMin: TcxGridDBColumn;
    cxGrid1DBTableView1HargaMax: TcxGridDBColumn;
    cxGrid1DBTableView1HargaLast: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn;
    cxGrid1DBTableView1HargaSatuan: TcxGridDBColumn;
    cxGrid1DBTableView1Supplier: TcxGridDBColumn;
    cxGrid1DBTableView1CashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1nama: TcxGridDBColumn;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    HargaQ: TSDQuery;
    SembarangQ: TSDQuery;
    cxGrid1DBTableView1satuan: TcxGridDBColumn;
    Label3: TLabel;
    TxtBeli: TEdit;
    ComboBox1: TComboBox;
    Label4: TLabel;
    SDQuery1: TSDQuery;
    SDQuery1Kode: TStringField;
    SDQuery1Supplier1: TStringField;
    SDQuery1Harga1: TCurrencyField;
    SDQuery1Termin1: TIntegerField;
    SDQuery1Keterangan1: TMemoField;
    SDQuery1Supplier2: TStringField;
    SDQuery1Harga2: TCurrencyField;
    SDQuery1Termin2: TIntegerField;
    SDQuery1Keterangan2: TMemoField;
    SDQuery1Supplier3: TStringField;
    SDQuery1Harga3: TCurrencyField;
    SDQuery1Termin3: TIntegerField;
    SDQuery1Keterangan3: TMemoField;
    SDQuery1nama1: TStringField;
    SDQuery1nama2: TStringField;
    SDQuery1nama3: TStringField;
    SDUpdateSQL2: TSDUpdateSQL;
    cxGrid1DBTableView1Termin: TcxGridDBColumn;
    GenerateQ: TSDQuery;
    GenerateQKode: TStringField;
    GenerateQBarang: TStringField;
    GenerateQBonBarang: TStringField;
    GenerateQHargaMin: TCurrencyField;
    GenerateQHargaMax: TCurrencyField;
    GenerateQHargaLast: TCurrencyField;
    GenerateQLastSupplier: TStringField;
    GenerateQJumlahBeli: TIntegerField;
    GenerateQHargaSatuan: TCurrencyField;
    GenerateQSupplier: TStringField;
    GenerateQCashNCarry: TBooleanField;
    GenerateQGrandTotal: TCurrencyField;
    GenerateQStatus: TStringField;
    GenerateQSupplier1: TStringField;
    GenerateQHarga1: TCurrencyField;
    GenerateQTermin1: TIntegerField;
    GenerateQKeterangan1: TMemoField;
    GenerateQSupplier2: TStringField;
    GenerateQHarga2: TCurrencyField;
    GenerateQTermin2: TIntegerField;
    GenerateQKeterangan2: TMemoField;
    GenerateQSupplier3: TStringField;
    GenerateQHarga3: TCurrencyField;
    GenerateQTermin3: TIntegerField;
    GenerateQKeterangan3: TMemoField;
    GenerateQTermin: TIntegerField;
    GenerateUpdate: TSDUpdateSQL;
    ListBox1: TListBox;
    cxGrid1DBTableView1urgent: TcxGridDBColumn;
    SuppQ: TSDQuery;
    DataSourceSupp: TDataSource;
    SuppQKode: TStringField;
    SuppQNamaToko: TStringField;
    SuppQAlamat: TStringField;
    SuppQNoTelp: TStringField;
    SuppQKategori: TStringField;
    SuppQNamaPIC1: TStringField;
    SuppQTelpPIC1: TStringField;
    SuppQJabatanPIC1: TStringField;
    SuppQNamaPIC2: TStringField;
    SuppQTelpPIC2: TStringField;
    SuppQJabatanPIC2: TStringField;
    SuppQNamaPIC3: TStringField;
    SuppQTelpPIC3: TStringField;
    SuppQJabatanPIC3: TStringField;
    SuppQCreateDate: TDateTimeField;
    SuppQCreateBy: TStringField;
    SuppQOperator: TStringField;
    SuppQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Supp_skr_R: TcxGridDBColumn;
    cxGrid1DBTableView1Last_Sup_R: TcxGridDBColumn;
    cxGrid1DBTableViewSingleSupplier: TcxGridDBColumn;
    MasterQKode: TStringField;
    MasterQBarang: TStringField;
    MasterQBonBarang: TStringField;
    MasterQHargaMin: TCurrencyField;
    MasterQHargaMax: TCurrencyField;
    MasterQHargaLast: TCurrencyField;
    MasterQLastSupplier: TStringField;
    MasterQJumlahBeli: TIntegerField;
    MasterQHargaSatuan: TCurrencyField;
    MasterQSupplier: TStringField;
    MasterQCashNCarry: TBooleanField;
    MasterQGrandTotal: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQSupplier1: TStringField;
    MasterQSupplier2: TStringField;
    MasterQSupplier3: TStringField;
    MasterQHarga1: TCurrencyField;
    MasterQHarga2: TCurrencyField;
    MasterQHarga3: TCurrencyField;
    MasterQTermin1: TIntegerField;
    MasterQTermin2: TIntegerField;
    MasterQTermin3: TIntegerField;
    MasterQKeterangan1: TMemoField;
    MasterQKeterangan2: TMemoField;
    MasterQKeterangan3: TMemoField;
    MasterQTermin: TIntegerField;
    MasterQnama: TStringField;
    MasterQbbbeli: TIntegerField;
    MasterQsatuan: TStringField;
    MasterQbeli1: TIntegerField;
    MasterQbeli2: TIntegerField;
    MasterQbeli3: TIntegerField;
    MasterQurgent: TBooleanField;
    MasterQsinglesupplier: TBooleanField;
    MasterQDetailSupplier: TStringField;
    MasterQDetailSupplier1: TStringField;
    MasterQDetailSupplier2: TStringField;
    MasterQDetailSupplier3: TStringField;
    DetailBonBarangQ: TSDQuery;
    DetailBonBarangQKodeBonBarang: TStringField;
    DetailBonBarangQKodeBarang: TStringField;
    DetailBonBarangQJumlahDiminta: TIntegerField;
    DetailBonBarangQJumlahBeli: TIntegerField;
    DetailBonBarangQKeterangan: TMemoField;
    DetailBonBarangQStatusMinta: TStringField;
    DetailBonBarangQStatusBeli: TStringField;
    DetailBonBarangQUrgent: TBooleanField;
    StatusBar: TStatusBar;
    RbtSingle: TRadioButton;
    RbtMUlti: TRadioButton;
    BtnSupSama: TButton;
    BtnSupBanyak: TButton;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1NamaSup1: TcxDBEditorRow;
    cxDBVerticalGrid1DetailSupplier1: TcxDBEditorRow;
    cxDBVerticalGrid1Harga1: TcxDBEditorRow;
    cxDBVerticalGrid1Termin1: TcxDBEditorRow;
    cxDBVerticalGrid1beli1: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan1: TcxDBEditorRow;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2Namasup2_r: TcxDBEditorRow;
    cxDBVerticalGrid2DetailSupplier2: TcxDBEditorRow;
    cxDBVerticalGrid2Harga2: TcxDBEditorRow;
    cxDBVerticalGrid2Termin2: TcxDBEditorRow;
    cxDBVerticalGrid2beli2: TcxDBEditorRow;
    cxDBVerticalGrid2Keterangan2: TcxDBEditorRow;
    cxDBVerticalGrid3: TcxDBVerticalGrid;
    cxDBVerticalGrid3NamaSup3: TcxDBEditorRow;
    cxDBVerticalGrid3DetailSupplier3: TcxDBEditorRow;
    cxDBVerticalGrid3Harga3: TcxDBEditorRow;
    cxDBVerticalGrid3Termin3: TcxDBEditorRow;
    cxDBVerticalGrid3beli3: TcxDBEditorRow;
    cxDBVerticalGrid3Keterangan3: TcxDBEditorRow;
    BtnSave: TButton;
    Panel1: TPanel;
    Label5: TLabel;
    MasterQDetaillastsupp: TStringField;
    MasterQ2: TSDQuery;
    MasterQ2Kode: TStringField;
    MasterQ2Barang: TStringField;
    MasterQ2BonBarang: TStringField;
    MasterQ2HargaMin: TCurrencyField;
    MasterQ2HargaMax: TCurrencyField;
    MasterQ2HargaLast: TCurrencyField;
    MasterQ2LastSupplier: TStringField;
    MasterQ2JumlahBeli: TIntegerField;
    MasterQ2HargaSatuan: TCurrencyField;
    MasterQ2Supplier: TStringField;
    MasterQ2CashNCarry: TBooleanField;
    MasterQ2GrandTotal: TCurrencyField;
    MasterQ2Status: TStringField;
    MasterQ2Supplier1: TStringField;
    MasterQ2Supplier2: TStringField;
    MasterQ2Supplier3: TStringField;
    MasterQ2Harga1: TCurrencyField;
    MasterQ2Harga2: TCurrencyField;
    MasterQ2Harga3: TCurrencyField;
    MasterQ2Termin1: TIntegerField;
    MasterQ2Termin2: TIntegerField;
    MasterQ2Termin3: TIntegerField;
    MasterQ2Keterangan1: TMemoField;
    MasterQ2Keterangan2: TMemoField;
    MasterQ2Keterangan3: TMemoField;
    MasterQ2Termin: TIntegerField;
    MasterQ2nama: TStringField;
    MasterQ2bbbeli: TIntegerField;
    MasterQ2satuan: TStringField;
    MasterQ2beli1: TIntegerField;
    MasterQ2beli2: TIntegerField;
    MasterQ2beli3: TIntegerField;
    MasterQ2urgent: TBooleanField;
    MasterQ2singlesupplier: TBooleanField;
    DataSource2: TDataSource;
    MasterQ2DetailSupplier: TStringField;
    MasterQ2DetailSupplier1: TStringField;
    MasterQ2DetailSupplier3: TStringField;
    MasterQ2DetailLastSupp: TStringField;
    MasterQ2DetailSupplier2: TStringField;
    SDUpdateSQL3: TSDUpdateSQL;
    CheckBox1: TcxCheckBox;
    CheckBox2: TcxCheckBox;
    CheckBox3: TcxCheckBox;
    SDQuery2: TSDQuery;
    MasterQHargaLast2: TCurrencyField;
    MasterQHargaLast3: TCurrencyField;
    MasterQTanggalLast: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cxGrid1DBTableView1BonBarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1LastSupplierPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1supplier_skrPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Supplier1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2Supplier2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid3Supplier3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnSupSamaClick(Sender: TObject);
    procedure BtnSupBanyakClick(Sender: TObject);
    procedure cxDBVerticalGrid1namasup1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid3namasup3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure tr(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid2Namasup2_rEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FormActivate(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RbtMUltiClick(Sender: TObject);
    procedure RbtSingleClick(Sender: TObject);
    procedure CheckBox1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure CheckBox2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure CheckBox3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DaftarBeliFm: TDaftarBeliFm;
  ctr:integer;

implementation
uses menuutama, BrowseName, Browse,BrowseSupplier, StrUtils,BandingSupplier,
  DropDown;

{$R *.dfm}

procedure TDaftarBeliFm.FormCreate(Sender: TObject);
var str:String;

begin
ctr:=1;
  MasterQ2.open;
  MasterQ2.edit;
  MasterQ2Supplier.AsString:='';
  MasterQ2HargaSatuan.AsInteger:=0;
  MasterQ2Termin.AsInteger:=0;
  MasterQ2Supplier1.AsString:='';
  MasterQ2Harga1.AsInteger:=0;
  MasterQ2Termin1.AsInteger:=0;
  MasterQ2Supplier2.AsString:='';
  MasterQ2Harga2.AsInteger:=0;
  MasterQ2Termin2.AsInteger:=0;
  MasterQ2Supplier3.AsString:='';
  MasterQ2Harga3.AsInteger:=0;
  MasterQ2Termin3.AsInteger:=0;
  MasterQ2.Close;
end;

procedure TDaftarBeliFm.BtnSaveClick(Sender: TObject);
begin
  MasterQ.Open;
  if checkbox1.Checked then
  begin
  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier1.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga1.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin1.AsInteger;
  MasterQ.Post;
  end
  else if checkbox2.Checked then
  begin
  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier2.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga2.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin2.AsInteger;
  MasterQ.Post;
  end
  else if checkbox2.Checked then
  begin
  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier3.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga3.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin3.AsInteger;
  MasterQ.Post;
  end;

      MasterQ.Edit;
      MasterQSupplier1.AsString:=MasterQ2Supplier1.AsString;
      MasterQDetailSupplier1.AsString:=MasterQ2DetailSupplier1.AsString;
      MasterQHarga1.AsString:=MasterQ2Harga1.AsString;
      MasterQTermin1.AsString:=MasterQ2Termin1.AsString;
      MasterQKeterangan1.AsString:=MasterQ2Keterangan1.AsString;
      MasterQSupplier2.AsString:=MasterQ2Supplier2.AsString;
      MasterQDetailSupplier2.AsString:=MasterQ2DetailSupplier2.AsString;
      MasterQHarga2.AsString:=MasterQ2Harga2.AsString;
      MasterQTermin2.AsString:=MasterQ2Termin2.AsString;
      MasterQKeterangan2.AsString:=MasterQ2Keterangan2.AsString;
      MasterQSupplier3.AsString:=MasterQ2Supplier3.AsString;
      MasterQDetailSupplier3.AsString:=MasterQ2DetailSupplier3.AsString;
      MasterQHarga3.AsString:=MasterQ2Harga3.AsString;
      MasterQTermin3.AsString:=MasterQ2Termin3.AsString;
      MasterQKeterangan3.AsString:=MasterQ2Keterangan3.AsString;
      MasterQTanggalLast.asdatetime:=now;

  MasterQ.First;
  ctr:=1;
  while not MasterQ.Eof do
  begin
      MasterQ.Edit;
      if MasterQKode.AsString='tes' then
      begin
        KodeQ.Open;
          if KodeQ.IsEmpty then
          begin
            MasterQKode.AsString:=FormatFloat('0000000000',1);
          end
          else
          begin
            MasterQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+ctr);
            ctr:=ctr+1;
          end;
          KodeQ.Close;
     end;
     //MasterQ.edit;
     MasterQGrandTotal.AsInteger:=MasterQJumlahBeli.AsInteger*MasterQHargaSatuan.AsInteger;
     if MasterQSupplier.AsString<>'0000000000' then
        MasterQStatus.AsString:='ONPROCESS';

     if MasterQCashNCarry.AsBoolean=true then
        MasterQStatus.AsString:='ONPROCESS';

     MasterQ.Post;
     MasterQ.Next;

     {MenuUtamaFm.DataBase1.StartTransaction;
      MasterQ.ApplyUpdates;
       MenuUtamaFm.DataBase1.Commit;
      MasterQ.CommitUpdates;}
  end;

  MenuUtamaFm.DataBase1.StartTransaction;
  MasterQ.ApplyUpdates;
   MenuUtamaFm.DataBase1.Commit;
  MasterQ.CommitUpdates;
//  MasterQ.close;

  ShowMessage('Daftar Beli Telah Tersimpan');
  MasterQ2.Open;
  MasterQ2.edit;
  MasterQ2Supplier1.AsString:='';
  MasterQ2Harga1.AsString:='';
  MasterQ2Termin1.AsString:='';
  MasterQ2Keterangan1.AsString:='';

  MasterQ2Supplier2.AsString:='';
  MasterQ2Harga2.AsString:='';
  MasterQ2Termin2.AsString:='';
  MasterQ2Keterangan2.AsString:='';

  MasterQ2Supplier3.AsString:='';
  MasterQ2Harga3.AsString:='';
  MasterQ2Termin3.AsString:='';
  MasterQ2Keterangan3.AsString:='';

  checkbox1.checked:=false;
  checkbox2.checked:=false;
  checkbox3.checked:=false;

end;

procedure TDaftarBeliFm.Button2Click(Sender: TObject);
var str:String;
begin
str:='';

if (TxtBeli.Text<>'') then
begin
str:=str+' and db.kode='+TxtBeli.Text;
end;
if (Txtbon.Text<>'') then
begin
str:=str+' and dbb.kodebonbarang='+QuotedStr(Txtbon.Text);
end;
if (TxtBarang.Text<>'') then
begin

str:=str+' and b.nama like'+QuotedStr('%'+TxtBarang.Text+'%');
end;
if ComboBox1.ItemIndex>0 then
    str:=str+' and db.status='+QuotedStr(ComboBox1.Text);

MasterQ.SQL.Clear;

MasterQ.SQL.Add('select db.*, b.nama,'+QuotedStr('tes')+' as last_supplier, '+QuotedStr('tes')+' as supplier_skr , dbb.jumlahbeli as bbbeli, b.satuan');
MasterQ.SQL.Add('from daftarbeli db,barang b,detailbonbarang dbb');
MasterQ.SQL.Add('where db.barang=b.kode and dbb.kodebonbarang=db.bonbarang');
MasterQ.SQL.Add('and dbb.kodebarang=db.barang');
MasterQ.SQL.Add(str);
MasterQ.SQL.Add('order by db.bonbarang, b.nama,db.kode desc');
TxtBarang.Text:=MasterQ.SQL.Text;
MasterQ.Open;

MasterQ.Close;
MasterQ.Open;

MasterQ.First;
while not MasterQ.Eof do
begin
  MasterQ.Edit;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select namatoko from supplier where kode='+QuotedStr(MasterQLastSupplier.AsString));
  SembarangQ.ExecSQL;
  SembarangQ.Open;

  SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select namatoko from supplier where kode='+QuotedStr(MasterQSupplier.AsString));
  SembarangQ.ExecSQL;
  SembarangQ.Open;

  MasterQ.Post;
  MasterQ.Next;
end;

end;

procedure TDaftarBeliFm.cxGrid1DBTableView1BonBarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  MenuUtamaFm.param_sql:='select bb.kode BonBarang, b.nama, b.jumlah, b.kode as barang from bonbarang bb, barang b, detailbonbarang dbb where bb.kode=dbb.kodebonbarang and dbb.kodebarang=b.kode order by bb.kode desc';
  BrowseFm.dari:='po';
  BrowseFm.ShowModal;
  MasterQ.Edit;

  MasterQKode.AsString:='tes'+IntToStr(ctr);
  ctr:=ctr+1;
  
  MasterQBonBarang.AsString:=MenuUtamaFm.param_kodebon;
  MasterQBarang.AsString:=BrowseFm.dari;
  MasterQnama.AsString:=MenuUtamaFm.param_nama;
  MasterQStatus.AsString:='ONPROCESS';
  MasterQGrandTotal.AsInteger:=0;
  MasterQbbbeli.AsInteger:=0;

  HargaQ.SQL.Clear;
  HargaQ.SQL.Add('select db.*, s.namatoko from daftarbeli db, supplier s where barang=' + QuotedStr(MasterQBarang.AsString) + ' and db.supplier=s.kode and db.kode=(select max(kode) from daftarbeli where barang=db.barang)');
  HargaQ.Open;
    MasterQHargaLast.AsInteger:=HargaQ.fieldbyname('hargasatuan').AsInteger;
    MasterQLastSupplier.AsString:=HargaQ.fieldbyname('supplier').AsString;
    MasterQSupplier.AsString:='0000000000';
  HargaQ.Close;

    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
    SembarangQ.ExecSQL;
    SembarangQ.Open;
    MasterQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
    SembarangQ.Close;

    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
    SembarangQ.ExecSQL;
    SembarangQ.Open;
    MasterQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
    SembarangQ.Close;


end;

procedure TDaftarBeliFm.cxGrid1DBTableView1LastSupplierPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseFm.ShowModal;
  MasterQ.Fields[6].AsString:=MenuUtamaFm.param_kodebon;

end;

procedure TDaftarBeliFm.cxGrid1DBTableView1supplier_skrPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    BrowseSupplierFm.kodebar:=MasterQBarang.AsString;
    BandingSupplierFm.kodedb:=MasterQKode.AsString;
    BandingSupplierFm.ShowModal;
    {BrowseSupplierFm.ShowModal;

    if MenuUtamaFm.param_kode<>'' then
    begin
      MasterQ.Edit;
      MasterQSupplier.AsString:=MenuUtamaFm.param_kode;
      MasterQsupplier_skr.AsString:=MenuUtamaFm.param_nama;
    end;  }
end;

procedure TDaftarBeliFm.cxDBVerticalGrid1Supplier1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseFm.ShowModal;

  MasterQ.Edit;
  MasterQSupplier1.AsString:=MenuUtamaFm.param_kodebon;
end;

procedure TDaftarBeliFm.cxDBVerticalGrid2Supplier2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseFm.ShowModal;

  MasterQ.Edit;
  MasterQSupplier2.AsString:=MenuUtamaFm.param_kodebon;
end;

procedure TDaftarBeliFm.cxDBVerticalGrid3Supplier3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseFm.ShowModal;

  MasterQ.Edit;
  MasterQSupplier3.AsString:=MenuUtamaFm.param_kodebon;
end;

procedure TDaftarBeliFm.BtnSupSamaClick(Sender: TObject);
var i,termin,harga:Integer;
  kodesup,namasup:String;
  oke:Boolean;
  asup1, asup2, asup3, aket1, aket2, aket3:String;
  atermin1,atermin2,atermin3,aharga1,aharga2,aharga3:Integer;
begin
   asup1:=MasterQ2Supplier1.AsString;
    atermin1:=MasterQ2Termin1.AsInteger;
    aharga1:=MasterQ2Harga1.AsInteger;
    aket1:=MasterQ2Keterangan1.AsString;

    asup2:=MasterQ2Supplier2.AsString;
    atermin2:=MasterQ2Termin2.AsInteger;
    aharga2:=MasterQ2Harga2.AsInteger;
    aket2:=MasterQ2Keterangan2.AsString;

    asup3:=MasterQ2Supplier3.AsString;
    atermin3:=MasterQ2Termin3.AsInteger;
    aharga3:=MasterQ2Harga3.AsInteger;
    aket3:=MasterQ2Keterangan3.AsString;

   oke:=false;
   if CheckBox1.Checked then
    begin
      kodesup:=MasterQ2Supplier1.AsString;
      termin:=MasterQ2Termin1.AsInteger;
      harga:=MasterQ2Harga1.AsInteger;
      oke:=true;
    end
   else if CheckBox2.Checked then
   begin
      kodesup:=MasterQ2Supplier2.AsString;
      termin:=MasterQ2Termin2.AsInteger;
      harga:=MasterQ2Harga2.AsInteger;
       oke:=true;
   end
   else if CheckBox3.Checked then
   begin
      kodesup:=MasterQ2Supplier3.AsString;
      termin:=MasterQ2Termin3.AsInteger;
      harga:=MasterQ2Harga3.AsInteger;
       oke:=true;
   end
   else
   ShowMessage('Tidak ada supplier yang dipilih');

   if oke=true then
   begin
        ListBox1.Items.Clear;
        for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
        begin
            ListBox1.Items.Add(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[0]);
        end;

        MasterQ.First;
        while not MasterQ.Eof do
        begin
            for i:=0 to ListBox1.Items.Count-1 do
            begin
                if ListBox1.Items[i] = MasterQKode.AsString then
                begin
                    MasterQ.Edit;
                    MasterQSupplier.AsString:=kodesup;
                    MasterQHargaSatuan.AsInteger:=harga;
                    MasterQTermin.AsInteger:=termin;

                    MasterQSupplier1.AsString:=asup1;
                    MasterQTermin1.AsInteger:=atermin1;
                    MasterQHarga1.AsInteger:=aharga1;
                    MasterQKeterangan1.AsString:=aket1;

                    MasterQSupplier2.AsString:=asup2;
                    MasterQTermin2.AsInteger:=atermin2;
                    MasterQHarga2.AsInteger:=aharga2;
                    MasterQKeterangan2.AsString:=aket2;

                    MasterQSupplier3.AsString:=asup3;
                    MasterQTermin3.AsInteger:=atermin3;
                    MasterQHarga3.AsInteger:=aharga3;
                    MasterQKeterangan3.AsString:=aket3;

                    //ShowMessage(MasterQsupplier_skr.AsString);
                    //ShowMessage(namasup);

                    MasterQ.Post;
                end;
            end;
          MasterQ.Next;
        end;
   end;

end;

procedure generate();
var kodebb, kodebarang, nama,satuan:String;
    jumbeli,i,beli1,beli2,beli3:Integer;
begin

end;

procedure TDaftarBeliFm.BtnSupBanyakClick(Sender: TObject);
var kodebb, kodebarang, nama,satuan,sup1,sup2,sup3, asup1,asup2,asup3, aket1, aket2, aket3:String;
    jumbeli,i,beli1,beli2,beli3,termin1,termin2,termin3,harga1,harga2,harga3:Integer;
    abeli1,abeli2,abeli3,atermin1,atermin2,atermin3,aharga1,aharga2,aharga3:Integer;
    oke,single:boolean;
begin
    asup1:=MasterQ2Supplier1.AsString;
    atermin1:=MasterQ2Termin1.AsInteger;
    aharga1:=MasterQ2Harga1.AsInteger;
    aket1:=MasterQ2Keterangan1.AsString;

    asup2:=MasterQ2Supplier2.AsString;
    atermin2:=MasterQ2Termin2.AsInteger;
    aharga2:=MasterQ2Harga2.AsInteger;
    aket2:=MasterQ2Keterangan2.AsString;

    asup3:=MasterQ2Supplier3.AsString;
    atermin3:=MasterQ2Termin3.AsInteger;
    aharga3:=MasterQ2Harga3.AsInteger;
    aket3:=MasterQ2Keterangan3.AsString;

    oke:=true;
    if CheckBox1.Checked then
    begin
        beli1:=MasterQ2beli1.AsInteger;
        sup1:=MasterQ2Supplier1.AsString;
        termin1:=MasterQ2Termin1.AsInteger;
        harga1:=MasterQ2Harga1.AsInteger;
        if beli1<=0 then oke:=false;
    end;
    if CheckBox2.Checked then
    begin
        beli2:=MasterQ2beli2.AsInteger;
        sup2:=MasterQ2Supplier2.AsString;
        termin2:=MasterQ2Termin2.AsInteger;
        harga2:=MasterQ2Harga2.AsInteger;
        if beli2<=0 then oke:=false;
    end;
    if CheckBox3.Checked then
    begin
        beli3:=MasterQ2beli3.AsInteger;
        sup3:=MasterQ2Supplier3.AsString;
        termin3:=MasterQ2Termin3.AsInteger;
        harga3:=MasterQ2Harga3.AsInteger;
        if beli3<=0 then oke:=false;
    end;

if beli1+beli2+beli3 <> MasterQJumlahBeli.AsInteger then
 ShowMessage('Total Beli tidak sesuai dengan Jumlah beli pada Bon Barang')
else
 if oke=true then
 begin
     kodebb:=MasterQBonBarang.AsString;
     kodebarang:=MasterQBarang.AsString;
     nama:=MasterQnama.AsString;
     satuan:=MasterQsatuan.AsString;
     single:=MasterQSingleSupplier.AsBoolean;

     MasterQ.Delete;

     if CheckBox1.Checked then
     begin
         MasterQ.Insert;

        MasterQKode.AsString:='tes';
         MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnama.AsString:=nama;
        MasterQStatus.AsString:='ONPROCESS';
        MasterQGrandTotal.AsInteger:=0;
        MasterQbbbeli.AsInteger:=0;

        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQurgent.AsBoolean:=DetailBonBarangQurgent.AsBoolean;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;

        MasterQJumlahBeli.AsInteger:=beli1;
        MasterQSupplier.AsString:=sup1;

        MasterQTermin.AsInteger:=termin1;
        MasterQHargaSatuan.AsInteger:=harga1;

        MasterQSingleSupplier.AsBoolean:=single;


        HargaQ.SQL.Clear;
        HargaQ.SQL.Add('select db.*, s.namatoko from daftarbeli db, supplier s where barang=' + QuotedStr(MasterQBarang.AsString) + ' and db.lastsupplier=s.kode and db.kode=(select max(kode) from daftarbeli where barang=db.barang)');
        //HargaQ.ExecSQL;
        HargaQ.Open;
        if HargaQ.fieldbyname('hargasatuan').Asstring='' then MasterQHargaLast.asvariant:=NULL
        else MasterQHargaLast.AsInteger:=HargaQ.fieldbyname('hargasatuan').AsInteger;
        if HargaQ.fieldbyname('lastsupplier').asstring='' then MasterQLastSupplier.asvariant:=NULL
        else MasterQLastSupplier.AsString:=HargaQ.fieldbyname('lastsupplier').AsString;
        HargaQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
        SembarangQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
        SembarangQ.Close;
     end;
     if CheckBox2.Checked then
     begin
         MasterQ.Insert;
          MasterQKode.AsString:='tes';

         MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnama.AsString:=nama;
        MasterQStatus.AsString:='ONPROCESS';
        MasterQGrandTotal.AsInteger:=0;
        MasterQbbbeli.AsInteger:=0;

        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQurgent.AsBoolean:=DetailBonBarangQurgent.AsBoolean;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;

        MasterQJumlahBeli.AsInteger:=beli2;
        MasterQSupplier.AsString:=sup2;


        MasterQTermin.AsInteger:=termin2;
        MasterQHargaSatuan.AsInteger:=harga2;

        MasterQSingleSupplier.AsBoolean:=single;

        HargaQ.SQL.Clear;
        HargaQ.SQL.Add('select db.*, s.namatoko from daftarbeli db, supplier s where barang=' + QuotedStr(MasterQBarang.AsString) + ' and db.lastsupplier=s.kode and db.kode=(select max(kode) from daftarbeli where barang=db.barang)');
        HargaQ.Open;
        if HargaQ.fieldbyname('hargasatuan').Asstring='' then MasterQHargaLast.asvariant:=NULL
        else MasterQHargaLast.AsInteger:=HargaQ.fieldbyname('hargasatuan').AsInteger;
        if HargaQ.fieldbyname('lastsupplier').asstring='' then MasterQLastSupplier.asvariant:=NULL
        else MasterQLastSupplier.AsString:=HargaQ.fieldbyname('lastsupplier').AsString;
        HargaQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
        SembarangQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
        SembarangQ.Close;
     end;
     if CheckBox3.Checked then
     begin
         MasterQ.Insert;
         MasterQKode.AsString:='tes';

         MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnama.AsString:=nama;
        MasterQStatus.AsString:='ONPROCESS';
        MasterQGrandTotal.AsInteger:=0;
        MasterQbbbeli.AsInteger:=0;

        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQurgent.AsBoolean:=DetailBonBarangQurgent.AsBoolean;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;

        MasterQJumlahBeli.AsInteger:=beli3;
        MasterQSupplier.AsString:=sup3;
        MasterQStatus.AsString:='ON PROCESS';

        MasterQTermin.AsInteger:=termin3;
        MasterQHargaSatuan.AsInteger:=harga3;

        MasterQSingleSupplier.AsBoolean:=single;

        HargaQ.SQL.Clear;
        HargaQ.SQL.Add('select db.*, s.namatoko from daftarbeli db, supplier s where barang=' + QuotedStr(MasterQBarang.AsString) + ' and db.lastsupplier=s.kode and db.kode=(select max(kode) from daftarbeli where barang=db.barang)');
        HargaQ.Open;
        if HargaQ.fieldbyname('hargasatuan').Asstring='' then MasterQHargaLast.asvariant:=NULL
        else MasterQHargaLast.AsInteger:=HargaQ.fieldbyname('hargasatuan').AsInteger;
        if HargaQ.fieldbyname('lastsupplier').asstring='' then MasterQLastSupplier.asvariant:=NULL
        else MasterQLastSupplier.AsString:=HargaQ.fieldbyname('lastsupplier').AsString;
        HargaQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(min(hargasatuan),0) as min from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMin.AsInteger:=SembarangQ.fieldbyname('min').AsInteger;
        SembarangQ.Close;

        SembarangQ.SQL.Clear;
        SembarangQ.SQL.Add('select isnull(max(hargasatuan),0) as max from daftarbeli where barang='+QuotedStr(MasterQBarang.AsString));
        SembarangQ.ExecSQL;
        SembarangQ.Open;
        MasterQHargaMax.AsInteger:=SembarangQ.fieldbyname('max').AsInteger;
        SembarangQ.Close;
     end;

    MasterQ.Post;
 end
 else
 ShowMessage('Jumlah Beli tidak lengkap');

end;

procedure TDaftarBeliFm.cxDBVerticalGrid1namasup1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SuppQ.Close;
//    ACQ.ParamByName('text').AsString:='';
    SuppQ.ExecSQL;
    SuppQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuppQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ2.Open;
      MasterQ2.Edit;
      MasterQ2Supplier1.AsString:=SuppQKode.AsString;
      MasterQ2DetailSupplier1.AsString:=SuppQNamaToko.AsString;
      MasterQ2Harga1.AsString:=MasterQHarga1.AsString;
      MasterQ2Termin1.AsString:=MasterQTermin1.AsString;
      MasterQ2Keterangan1.AsString:=MasterQKeterangan1.AsString;

     end;
    DropDownFm.Release;




{  MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseSupplierFm.kodebar:=MasterQBarang.AsString;
  BrowseSupplierFm.ShowModal;
//BrowseSupplierFm.Show;

  MasterQ.Edit;
  MasterQSupplier1.AsString:=MenuUtamaFm.param_kode;
}
end;

procedure TDaftarBeliFm.cxDBVerticalGrid3namasup3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SuppQ.Close;
//    ACQ.ParamByName('text').AsString:='';
    SuppQ.ExecSQL;
    SuppQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuppQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ2.Open;
      MasterQ2.Edit;
      MasterQ2Supplier3.AsString:=SuppQKode.AsString;
      MasterQ2DetailSupplier3.AsString:=SuppQNamaToko.AsString;
      MasterQ2Harga3.AsString:=MasterQHarga3.AsString;
      MasterQ2Termin3.AsString:=MasterQTermin3.AsString;
      MasterQ2Keterangan3.AsString:=MasterQKeterangan3.AsString;

     end;
    DropDownFm.Release;


{  MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseSupplierFm.kodebar:=MasterQBarang.AsString;
  BrowseSupplierFm.ShowModal;

  MasterQ.Edit;
  MasterQSupplier3.AsString:=MenuUtamaFm.param_kode;
 }
end;

procedure TDaftarBeliFm.tr(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  BtnSupBanyak.Enabled:=false;
  BtnSupSama.Enabled:=false;
  RbtSingle.Enabled:=false;
  RbtMUlti.Enabled:=false;
  cxDBVerticalGrid1.Enabled:=false;
  cxDBVerticalGrid2.Enabled:=false;
  cxDBVerticalGrid3.Enabled:=false;
//  CheckBox1.Enabled:=false;
//  CheckBox2.Enabled:=false;
//  CheckBox3.Enabled:=false;

    if MasterQSupplier.AsString = '' then
    begin
    CheckBox1.Checked:= false;
    CheckBox2.Checked:= false;
    CheckBox3.Checked:= false;
    end
    else If MasterQSupplier.AsString = MasterQSupplier1.AsString then
    begin
    CheckBox1.Checked:= true;
    CheckBox2.Checked:= false;
    CheckBox3.Checked:= false;
    end
    else if MasterQSupplier.AsString = MasterQSupplier2.AsString then
    begin
    CheckBox1.Checked:= false;
    CheckBox2.Checked:= true;
    CheckBox3.Checked:= false;
    end
    else if MasterQSupplier.AsString = MasterQSupplier3.AsString then
    begin
    CheckBox1.Checked:= false;
    CheckBox2.Checked:= false;
    CheckBox3.Checked:= true;
    end;


  if MenuUtamaFm.UserQPilihSupplierDaftarBeli.AsBoolean=true and MasterQsinglesupplier.AsBoolean=true then
  //if MasterQsinglesupplier.AsBoolean=true and MenuUtamaFm.UserQPilihSupplierDaftarBeli.AsBoolean=true then
  begin
    BtnSupBanyak.Enabled:=false;
    BtnSupSama.Enabled:=true;
    RbtSingle.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    cxDBVerticalGrid2.Enabled:=false;
    cxDBVerticalGrid3.Enabled:=false;
    CheckBox1.Enabled:=true;
    CheckBox2.Enabled:=false;
    CheckBox3.Enabled:=false;
    MasterQ2.Open;
    MasterQ2.Edit;
    MasterQ2Supplier1.AsString:=MasterQSupplier1.AsString;
    MasterQ2DetailSupplier1.AsString:=MasterQDetailSupplier1.AsString;
    MasterQ2Harga1.AsString:=MasterQHarga1.AsString;
    MasterQ2Termin1.AsInteger:=MasterQTermin1.AsInteger;
    MasterQ2Supplier2.AsString:=MasterQSupplier2.AsString;
    MasterQ2DetailSupplier2.AsString:=MasterQDetailSupplier2.AsString;
    MasterQ2Harga2.AsString:=MasterQHarga2.AsString;
    MasterQ2Termin2.AsInteger:=MasterQTermin2.AsInteger;
    MasterQ2Supplier3.AsString:=MasterQSupplier3.AsString;
    MasterQ2DetailSupplier3.AsString:=MasterQDetailSupplier3.AsString;
    MasterQ2Harga3.AsString:=MasterQHarga3.AsString;
    MasterQ2Termin3.AsInteger:=MasterQTermin3.AsInteger;
{
    If MasterQ2Supplier1.AsString = MasterQ2Supplier.AsString then
    begin
    CheckBox4.Checked:= true;
    CheckBox5.Checked:= false;
    CheckBox6.Checked:= false
    end
    else if MasterQ2Supplier2.AsString = MasterQ2Supplier.AsString then
    begin
    CheckBox4.Checked:= false;
    CheckBox5.Checked:= true;
    CheckBox6.Checked:= false
    end
    else
    CheckBox4.Checked:= false;
    CheckBox5.Checked:= false;
    CheckBox6.Checked := true;
 }

  end
  else if MenuUtamaFm.UserQPilihSupplierDaftarBeli.AsBoolean=true and MasterQSingleSupplier.AsBoolean=false then
  begin
    BtnSupBanyak.Enabled:=true;
    BtnSupSama.Enabled:=true;
    RbtSingle.Enabled:=true;
    RbtMUlti.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    cxDBVerticalGrid2.Enabled:=true;
    cxDBVerticalGrid3.Enabled:=true;
    CheckBox1.Enabled:=true;
    CheckBox2.Enabled:=true;
    CheckBox3.Enabled:=true;
    MasterQ2.Open;
    MasterQ2.Edit;
    MasterQ2Supplier1.AsString:=MasterQSupplier1.AsString;
    MasterQ2DetailSupplier1.AsString:=MasterQDetailSupplier1.AsString;
    MasterQ2Harga1.AsString:=MasterQHarga1.AsString;
    MasterQ2Termin1.AsInteger:=MasterQTermin1.AsInteger;
    MasterQ2Supplier2.AsString:=MasterQSupplier2.AsString;
    MasterQ2DetailSupplier2.AsString:=MasterQDetailSupplier2.AsString;
    MasterQ2Harga2.AsString:=MasterQHarga2.AsString;
    MasterQ2Termin2.AsInteger:=MasterQTermin2.AsInteger;
    MasterQ2Supplier3.AsString:=MasterQSupplier3.AsString;
    MasterQ2DetailSupplier3.AsString:=MasterQDetailSupplier3.AsString;
    MasterQ2Harga3.AsString:=MasterQHarga3.AsString;
    MasterQ2Termin3.AsInteger:=MasterQTermin3.AsInteger;
{
    If MasterQ2Supplier1.AsString = MasterQ2Supplier.AsString then
    begin
    CheckBox4.Checked:= true;
    CheckBox5.Checked:= false;
    CheckBox6.Checked:= false
    end
    else if MasterQ2Supplier2.AsString = MasterQ2Supplier.AsString then
    begin
    CheckBox4.Checked:= false;
    CheckBox5.Checked:= true;
    CheckBox6.Checked:= false
    end
    else
    CheckBox4.Checked:= false;
    CheckBox5.Checked:= false;
    CheckBox6.Checked := true;
 }
  end;

 // CheckBox1.Checked:=false;
 // CheckBox2.Checked:=false;
 // CheckBox3.Checked:=false;


  {SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select * from supplier where kode='+QuotedStr(MasterQSupplier1.AsString));
  SembarangQ.ExecSQL;
  SembarangQ.Open;
  try
  //  LblSup1.Caption:=SembarangQ.fieldbyname('namatoko').AsString;
  except
  end;

   SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select * from supplier where kode='+QuotedStr(MasterQSupplier2.AsString));
  SembarangQ.ExecSQL;
  SembarangQ.Open;
  try
  //  LblSup2.Caption:=SembarangQ.fieldbyname('namatoko').AsString;
  except
  end;

   SembarangQ.SQL.Clear;
  SembarangQ.SQL.Add('select * from supplier where kode='+QuotedStr(MasterQSupplier3.AsString));
  SembarangQ.ExecSQL;
  SembarangQ.Open;
  try
  //  LblSup3.Caption:=SembarangQ.fieldbyname('namatoko').AsString;
  except
  end;}

end;

procedure TDaftarBeliFm.cxDBVerticalGrid2Namasup2_rEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SuppQ.Close;
//    ACQ.ParamByName('text').AsString:='';
    SuppQ.ExecSQL;
    SuppQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuppQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ2.Open;
      MasterQ2.Edit;
      MasterQ2Supplier2.AsString:=SuppQKode.AsString;
      MasterQ2DetailSupplier2.AsString:=SuppQNamaToko.AsString;
      MasterQ2Harga2.AsString:=MasterQHarga2.AsString;
      MasterQ2Termin2.AsString:=MasterQTermin2.AsString;
      MasterQ2Keterangan2.AsString:=MasterQKeterangan2.AsString;

     end;
    DropDownFm.Release;


{  MenuUtamaFm.param_sql:='select kode,namatoko,namapic1 from supplier';
  BrowseSupplierFm.kodebar:=MasterQBarang.AsString;
  BrowseSupplierFm.ShowModal;

  MasterQ.Edit;
  MasterQSupplier2.AsString:=MenuUtamaFm.param_kode;
}
end;

procedure TDaftarBeliFm.FormActivate(Sender: TObject);
begin
MasterQ.Close;
MasterQ.Open;

BtnSave.Enabled:=false;
cxGrid1DBTableView1Supp_skr_R.Options.Editing:=false;
cxDBVerticalGrid1beli1.Visible:=false;
cxDBVerticalGrid2beli2.Visible:=false;
cxDBVerticalGrid3beli3.Visible:=false;

if MenuUtamaFm.UserQUpdateDaftarBeli.AsBoolean=true then
  BtnSave.Enabled:=true;

{if MenuUtamaFm.UserQPilihSupplierDaftarBeli.AsBoolean=true then
begin
  cxGrid1DBTableView1Supp_skr_R.Options.Editing:=true;
end;}


end;

procedure TDaftarBeliFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  //showmessage(aviewinfo.GridRecord.Values[1]);
 if(AViewInfo.GridRecord.Values[1] = true) then
 begin
    ACanvas.Brush.Color := clRed;
 end
 else
 begin
  //ACanvas.Brush.Color := clWhite;
 end;

  if(AViewInfo.Selected) and (Screen.ActiveControl = Sender.Site) then begin
    ACanvas.Brush.Color := clBlack;
    ACanvas.Font.Color := clWhite;
  end;

end;

procedure TDaftarBeliFm.RbtMUltiClick(Sender: TObject);
begin
    cxDBVerticalGrid1beli1.Visible:=true;
    cxDBVerticalGrid2beli2.Visible:=true;
    cxDBVerticalGrid3beli3.Visible:=true;
end;

procedure TDaftarBeliFm.RbtSingleClick(Sender: TObject);
begin
    cxDBVerticalGrid1beli1.Visible:=false;
    cxDBVerticalGrid2beli2.Visible:=false;
    cxDBVerticalGrid3beli3.Visible:=false;
    //CheckBox1.Checked:=true;
end;

procedure TDaftarBeliFm.CheckBox1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin

  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier1.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga1.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin1.AsInteger;
  MasterQSupplier1.AsString:=MasterQ2Supplier1.AsString;
  MasterQHarga1.AsInteger:=MasterQ2Harga1.AsInteger;
  MasterQTermin1.AsInteger:=MasterQ2Termin1.AsInteger;

 // MasterQsupplier_skr.AsString:=LblSup1.Caption;
  MasterQ.Post;
end;

procedure TDaftarBeliFm.CheckBox2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
{if CheckBox2.Checked then
begin
  if RbtSingle.Checked then
  begin
      CheckBox1.Checked:=false;
      CheckBox3.Checked:=false;
  end;  }
  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier2.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga2.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin2.AsInteger;
  MasterQSupplier2.AsString:=MasterQ2Supplier2.AsString;
  MasterQHarga2.AsInteger:=MasterQ2Harga2.AsInteger;
  MasterQTermin2.AsInteger:=MasterQ2Termin2.AsInteger;

  MasterQ.Post;
//  end;
end;

procedure TDaftarBeliFm.CheckBox3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
{ if CheckBox3.Checked then
begin
  if RbtSingle.Checked then
  begin
      CheckBox1.Checked:=false;
      CheckBox2.Checked:=false;
  end;
 }
  MasterQ.Edit;
  MasterQSupplier.AsString:=MasterQ2Supplier3.AsString;
  MasterQHargaSatuan.AsInteger:=MasterQ2Harga3.AsInteger;
  MasterQTermin.AsInteger:=MasterQ2Termin3.AsInteger;
  MasterQSupplier3.AsString:=MasterQ2Supplier3.AsString;
  MasterQHarga3.AsInteger:=MasterQ2Harga3.AsInteger;
  MasterQTermin3.AsInteger:=MasterQ2Termin3.AsInteger;

  MasterQ.Post;
//end;
end;

procedure TDaftarBeliFm.CheckBox1Click(Sender: TObject);
begin
if CheckBox1.Checked then
begin
  if RbtSingle.Checked then
  begin
      CheckBox2.Checked:=false;
      CheckBox3.Checked:=false;
  end;
end;
end;

procedure TDaftarBeliFm.CheckBox2Click(Sender: TObject);
begin
if CheckBox2.Checked then
begin
  if RbtSingle.Checked then
  begin
      CheckBox1.Checked:=false;
      CheckBox3.Checked:=false;
  end;
end;
end;

procedure TDaftarBeliFm.CheckBox3Click(Sender: TObject);
begin
 if CheckBox3.Checked then
begin
  if RbtSingle.Checked then
  begin
      CheckBox1.Checked:=false;
      CheckBox2.Checked:=false;
  end;
end;
end;

end.
