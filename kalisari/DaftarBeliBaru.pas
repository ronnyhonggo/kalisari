unit DaftarBeliBaru;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxLabel, Buttons, cxCheckBox;

type
  TDaftarBeliBaruFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeQkode: TStringField;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    BarangQ: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    SDQuery1Barang: TStringField;
    SDQuery1Keterangan: TMemoField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1Operator: TStringField;
    SupplierQ: TSDQuery;
    SDQuery1Kode: TStringField;
    SDQuery1NamaBarang: TStringField;
    SDQuery3: TSDQuery;
    StringField7: TStringField;
    StringField8: TStringField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    MemoField1: TMemoField;
    DateTimeField3: TDateTimeField;
    StringField9: TStringField;
    DateTimeField4: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    SDQuery1Saldo: TIntegerField;
    SDQuery1Status: TStringField;
    Panel1: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1HargaLast: TcxGridDBColumn;
    cxGrid1DBTableView1LastSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn;
    cxGrid1DBTableView1HargaSatuan: TcxGridDBColumn;
    cxGrid1DBTableView1CashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1Termin: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    cxGrid1DBTableView1TglLast: TcxGridDBColumn;
    cxGrid1DBTableView1HargaLast2: TcxGridDBColumn;
    cxGrid1DBTableView1HargaLast3: TcxGridDBColumn;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    cxGrid1DBTableView1Single: TcxGridDBColumn;
    RbtSingle: TRadioButton;
    RbtMUlti: TRadioButton;
    Label5: TLabel;
    BtnSupSama: TButton;
    BtnSupBanyak: TButton;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1NamaSup1: TcxDBEditorRow;
    cxDBVerticalGrid1DetailSupplier1: TcxDBEditorRow;
    cxDBVerticalGrid1Harga1: TcxDBEditorRow;
    cxDBVerticalGrid1Termin1: TcxDBEditorRow;
    cxDBVerticalGrid1beli1: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan1: TcxDBEditorRow;
    CheckBox1: TcxCheckBox;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2Namasup2_r: TcxDBEditorRow;
    cxDBVerticalGrid2DetailSupplier2: TcxDBEditorRow;
    cxDBVerticalGrid2Harga2: TcxDBEditorRow;
    cxDBVerticalGrid2Termin2: TcxDBEditorRow;
    cxDBVerticalGrid2beli2: TcxDBEditorRow;
    cxDBVerticalGrid2Keterangan2: TcxDBEditorRow;
    CheckBox2: TcxCheckBox;
    cxDBVerticalGrid3: TcxDBVerticalGrid;
    cxDBVerticalGrid3NamaSup3: TcxDBEditorRow;
    cxDBVerticalGrid3DetailSupplier3: TcxDBEditorRow;
    cxDBVerticalGrid3Harga3: TcxDBEditorRow;
    cxDBVerticalGrid3Termin3: TcxDBEditorRow;
    cxDBVerticalGrid3beli3: TcxDBEditorRow;
    cxDBVerticalGrid3Keterangan3: TcxDBEditorRow;
    CheckBox3: TcxCheckBox;
    ListBox1: TListBox;
    MasterQKode: TStringField;
    MasterQBarang: TStringField;
    MasterQBonBarang: TStringField;
    MasterQHargaMin: TCurrencyField;
    MasterQHargaMax: TCurrencyField;
    MasterQHargaLast: TCurrencyField;
    MasterQLastSupplier: TStringField;
    MasterQTglLast: TDateTimeField;
    MasterQHargaLast2: TCurrencyField;
    MasterQHargaLast3: TCurrencyField;
    MasterQJumlahBeli: TIntegerField;
    MasterQHargaSatuan: TCurrencyField;
    MasterQSupplier: TStringField;
    MasterQCashNCarry: TBooleanField;
    MasterQGrandTotal: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQSupplier1: TStringField;
    MasterQSupplier2: TStringField;
    MasterQSupplier3: TStringField;
    MasterQHarga1: TCurrencyField;
    MasterQHarga2: TCurrencyField;
    MasterQHarga3: TCurrencyField;
    MasterQTermin1: TIntegerField;
    MasterQTermin2: TIntegerField;
    MasterQTermin3: TIntegerField;
    MasterQBeli1: TIntegerField;
    MasterQBeli2: TIntegerField;
    MasterQBeli3: TIntegerField;
    MasterQKeterangan1: TMemoField;
    MasterQKeterangan2: TMemoField;
    MasterQKeterangan3: TMemoField;
    MasterQTermin: TIntegerField;
    MasterQUrgent: TBooleanField;
    MasterQNamaBarang: TStringField;
    MasterQSatuan: TStringField;
    MasterQSingle: TBooleanField;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    MasterQDetailSupplier1: TStringField;
    MasterQDetailSupplier2: TStringField;
    MasterQDetailSupplier3: TStringField;
    MasterQNamaSupplier: TStringField;
    cxGrid1DBTableView1NamaSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1Urgent: TcxGridDBColumn;
    cxButton1: TcxButton;
    DeleteDBQ: TSDQuery;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    BarangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure BtnSupSamaClick(Sender: TObject);
    procedure BtnSupBanyakClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxDBVerticalGrid1NamaSup1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2Namasup2_rEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid3NamaSup3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cxGrid1DBTableView1CashNCarryPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  DaftarBeliBaruFm: TDaftarBeliBaruFm;

  MasterOriSQL: string;
  BarangOriSQL : string;
implementation

uses MenuUtama, DropDown, DM, Math, SupplierDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TDaftarBeliBaruFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TDaftarBeliBaruFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDaftarBeliBaruFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TDaftarBeliBaruFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TDaftarBeliBaruFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
end;

procedure TDaftarBeliBaruFm.SaveBtnClick(Sender: TObject);
var temp : integer;
begin
  MasterQ.First;
  KodeQ.Open;
  temp:=KodeQkode.AsInteger;
  While MasterQ.Eof=false do
  begin
    If MasterQKode.AsString='tes' then
    begin
      MasterQ.Edit;
      MasterQKode.AsString:=DMFm.Fill(InttoStr(temp+1),10,'0',True);
      Inc(temp);
    end;
    MasterQ.Next;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Penyimpanan Berhasil');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TDaftarBeliBaruFm.BtnSupSamaClick(Sender: TObject);
var i,termin,harga:Integer;
  kodesup,namasup:String;
  oke:Boolean;
  asup1, asup2, asup3, aket1, aket2, aket3:String;
  atermin1,atermin2,atermin3,aharga1,aharga2,aharga3:Integer;
begin

    asup1:=MasterQSupplier1.AsString;
    atermin1:=MasterQTermin1.AsInteger;
    aharga1:=MasterQHarga1.AsInteger;
    aket1:=MasterQKeterangan1.AsString;

    asup2:=MasterQSupplier2.AsString;
    atermin2:=MasterQTermin2.AsInteger;
    aharga2:=MasterQHarga2.AsInteger;
    aket2:=MasterQKeterangan2.AsString;

    asup3:=MasterQSupplier3.AsString;
    atermin3:=MasterQTermin3.AsInteger;
    aharga3:=MasterQHarga3.AsInteger;
    aket3:=MasterQKeterangan3.AsString;
   oke:=false;
   if CheckBox1.Checked then
    begin
      kodesup:=MasterQSupplier1.AsString;
      termin:=MasterQTermin1.AsInteger;
      harga:=MasterQHarga1.AsInteger;
      oke:=true;
    end
   else if CheckBox2.Checked then
   begin
      kodesup:=MasterQSupplier2.AsString;
      termin:=MasterQTermin2.AsInteger;
      harga:=MasterQHarga2.AsInteger;
       oke:=true;
   end
   else if CheckBox3.Checked then
   begin
      kodesup:=MasterQSupplier3.AsString;
      termin:=MasterQTermin3.AsInteger;
      harga:=MasterQHarga3.AsInteger;
       oke:=true;
   end
   else
   ShowMessage('Tidak ada supplier yang dipilih');

   if oke=true then
   begin
        ListBox1.Items.Clear;
        for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
        begin
            ListBox1.Items.Add(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[1]);
          //ShowMessage(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[0]);
        end;

        MasterQ.First;
        while not MasterQ.Eof do
        begin
            for i:=0 to ListBox1.Items.Count-1 do
            begin
                if ListBox1.Items[i] = MasterQKode.AsString then
                begin
                    MasterQ.Edit;
                    MasterQSupplier.AsString:=kodesup;
                    MasterQHargaSatuan.AsInteger:=harga;
                    MasterQTermin.AsInteger:=termin;

                    MasterQSupplier1.AsString:=asup1;
                    MasterQTermin1.AsInteger:=atermin1;
                    MasterQHarga1.AsInteger:=aharga1;
                    MasterQKeterangan1.AsString:=aket1;

                    MasterQSupplier2.AsString:=asup2;
                    MasterQTermin2.AsInteger:=atermin2;
                    MasterQHarga2.AsInteger:=aharga2;
                    MasterQKeterangan2.AsString:=aket2;

                    MasterQSupplier3.AsString:=asup3;
                    MasterQTermin3.AsInteger:=atermin3;
                    MasterQHarga3.AsInteger:=aharga3;
                    MasterQKeterangan3.AsString:=aket3;

                    MasterQ.Post;
                end;
            end;
          MasterQ.Next;
        end;
   end;

end;


procedure TDaftarBeliBaruFm.BtnSupBanyakClick(Sender: TObject);
var kodebb, kodebarang, nama,satuan,sup1,sup2,sup3, asup1,asup2,asup3, aket1, aket2, aket3:String;
    jumbeli,i,beli1,beli2,beli3,termin1,termin2,termin3,harga1,harga2,harga3:Integer;
    abeli1,abeli2,abeli3,atermin1,atermin2,atermin3,aharga1,aharga2,aharga3:Integer;
    urgent,oke,single:boolean;
begin
    asup1:=MasterQSupplier1.AsString;
    atermin1:=MasterQTermin1.AsInteger;
    aharga1:=MasterQHarga1.AsInteger;
    aket1:=MasterQKeterangan1.AsString;

    asup2:=MasterQSupplier2.AsString;
    atermin2:=MasterQTermin2.AsInteger;
    aharga2:=MasterQHarga2.AsInteger;
    aket2:=MasterQKeterangan2.AsString;

    asup3:=MasterQSupplier3.AsString;
    atermin3:=MasterQTermin3.AsInteger;
    aharga3:=MasterQHarga3.AsInteger;
    aket3:=MasterQKeterangan3.AsString;

    oke:=true;
    if CheckBox1.Checked then
    begin
        beli1:=MasterQbeli1.AsInteger;
        sup1:=MasterQSupplier1.AsString;
        termin1:=MasterQTermin1.AsInteger;
        harga1:=MasterQHarga1.AsInteger;
        if beli1<=0 then oke:=false;
    end;
    if CheckBox2.Checked then
    begin
        beli2:=MasterQbeli2.AsInteger;
        sup2:=MasterQSupplier2.AsString;
        termin2:=MasterQTermin2.AsInteger;
        harga2:=MasterQHarga2.AsInteger;
        if beli2<=0 then oke:=false;
    end;
    if CheckBox3.Checked then
    begin
        beli3:=MasterQbeli3.AsInteger;
        sup3:=MasterQSupplier3.AsString;
        termin3:=MasterQTermin3.AsInteger;
        harga3:=MasterQHarga3.AsInteger;
        if beli3<=0 then oke:=false;
    end;

if beli1+beli2+beli3 <> MasterQJumlahBeli.AsInteger then
 ShowMessage('Total Beli tidak sesuai dengan Jumlah beli pada Bon Barang')
else
 if oke=true then
 begin
     kodebb:=MasterQBonBarang.AsString;
     kodebarang:=MasterQBarang.AsString;
     nama:=MasterQnamabarang.AsString;
     satuan:=MasterQsatuan.AsString;
     single:=MasterQSingle.AsBoolean;
     urgent:=MasterQUrgent.AsBoolean;
     MasterQ.Delete;

     if CheckBox1.Checked then
     begin
        MasterQ.Insert;
        MasterQKode.AsString:='tes';
        MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnamabarang.AsString:=nama;
        MasterQGrandTotal.AsInteger:=0;
        MasterQjumlahbeli.AsInteger:=0;
        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;
        MasterQUrgent.AsBoolean:=urgent;
        MasterQJumlahBeli.AsInteger:=beli1;
        MasterQSupplier.AsString:=sup1;
        MasterQStatus.AsString:='ON PROCESS';
        MasterQTermin.AsInteger:=termin1;
        MasterQHargaSatuan.AsInteger:=harga1;

        MasterQHargaSatuan.AsString:=MasterQHarga1.AsString;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;

        MasterQSingle.AsBoolean:=single;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
     end;
     if CheckBox2.Checked then
     begin
        MasterQ.Insert;
        MasterQKode.AsString:='tes';
        MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnamabarang.AsString:=nama;
        MasterQGrandTotal.AsInteger:=0;
        MasterQjumlahbeli.AsInteger:=0;
        MasterQUrgent.AsBoolean:=urgent;
        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;
        MasterQStatus.AsString:='ON PROCESS';
        MasterQJumlahBeli.AsInteger:=beli2;
        MasterQSupplier.AsString:=sup2;


        MasterQTermin.AsInteger:=termin2;
        MasterQHargaSatuan.AsInteger:=harga2;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;

        MasterQSingle.AsBoolean:=single;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
     end;
     if CheckBox3.Checked then
     begin
         MasterQ.Insert;
         MasterQKode.AsString:='tes';

         MasterQBonBarang.AsString:=kodebb;
        MasterQBarang.AsString:=kodebarang;
        MasterQnamabarang.AsString:=nama;
        MasterQGrandTotal.AsInteger:=0;
        MasterQjumlahbeli.AsInteger:=0;
        MasterQUrgent.AsBoolean:=urgent;
        MasterQsatuan.AsString:=satuan;
        MasterQbeli1.AsInteger:=0;
        MasterQbeli2.AsInteger:=0;
        MasterQbeli3.AsInteger:=0;
        MasterQSupplier1.AsString:=asup1;
        MasterQSupplier2.AsString:=asup2;
        MasterQSupplier3.AsString:=asup3;
        MasterQHarga1.AsInteger:=aharga1;
        MasterQHarga2.AsInteger:=aharga2;
        MasterQHarga3.AsInteger:=aharga3;
        MasterQTermin1.AsInteger:=atermin1;
        MasterQTermin2.AsInteger:=atermin2;
        MasterQTermin3.AsInteger:=atermin3;
        MasterQKeterangan1.AsString:=aket1;
        MasterQKeterangan2.AsString:=aket2;
        MasterQKeterangan3.AsString:=aket3;
        MasterQCashNCarry.AsBoolean:=false;
        MasterQStatus.AsString:='ON PROCESS';
        MasterQJumlahBeli.AsInteger:=beli3;
        MasterQSupplier.AsString:=sup3;


        MasterQTermin.AsInteger:=termin3;
        MasterQHargaSatuan.AsInteger:=harga3;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;
        
        MasterQSingle.AsBoolean:=single;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
     end;

    MasterQ.Post;
 end
 else
 ShowMessage('Jumlah Beli tidak lengkap');
end;

procedure TDaftarBeliBaruFm.CheckBox1Click(Sender: TObject);
begin
if CheckBox1.Checked then
begin
  if (MasterQSupplier1.AsString='') OR (MasterQHarga1.AsString='') OR (MasterQTermin1.AsString='') then
  begin
    ShowMessage('Lengkapi data supplier terlebih dahulu');
    CheckBox1.Checked:=False;
  end
  else
  begin
    if RbtSingle.Checked then
    begin
        CheckBox2.Checked:=false;
        CheckBox3.Checked:=false;
        MasterQ.Edit;
        MasterQSupplier.AsString:=MasterQSupplier1.AsString;
        MasterQHargaSatuan.AsString:=MasterQHarga1.AsString;
        MasterQTermin.AsString:=MasterQTermin1.AsString;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
    end;
  end;
end;
end;

procedure TDaftarBeliBaruFm.CheckBox2Click(Sender: TObject);
begin
if CheckBox2.Checked then
begin
  if (MasterQSupplier2.AsString='') OR (MasterQHarga2.AsString='') OR (MasterQTermin2.AsString='') then
  begin
    ShowMessage('Lengkapi data supplier terlebih dahulu');
    CheckBox2.Checked:=False;
  end
  else
  begin
    if RbtSingle.Checked then
    begin
        CheckBox1.Checked:=false;
        CheckBox3.Checked:=false;
        MasterQ.Edit;
        MasterQSupplier.AsString:=MasterQSupplier2.AsString;
        MasterQHargaSatuan.AsString:=MasterQHarga2.AsString;
        MasterQTermin.AsString:=MasterQTermin2.AsString;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
    end;
  end;
end;
end;

procedure TDaftarBeliBaruFm.CheckBox3Click(Sender: TObject);
begin
if CheckBox3.Checked then
begin
  if (MasterQSupplier3.AsString='') OR (MasterQHarga3.AsString='') OR (MasterQTermin3.AsString='') then
  begin
    ShowMessage('Lengkapi data supplier terlebih dahulu');
    CheckBox3.Checked:=False;
  end
  else
  begin
  if RbtSingle.Checked then
    begin
        CheckBox1.Checked:=false;
        CheckBox2.Checked:=false;
        MasterQ.Edit;
        MasterQSupplier.AsString:=MasterQSupplier3.AsString;
        MasterQHargaSatuan.AsString:=MasterQHarga3.AsString;
        MasterQTermin.AsString:=MasterQTermin3.AsString;
        MasterQGrandTotal.AsFloat:=MasterQHargaSatuan.AsFloat*MasterQJumlahBeli.AsFloat;
        if MasterQStatus.AsString='NEW DB' then MasterQStatus.AsString:='ON PROCESS';
    end;
  end;
end;
end;

procedure TDaftarBeliBaruFm.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin 
if MasterQSingle.AsBoolean then
begin
  CheckBox2.Visible:=false;
  CheckBox3.Visible:=false;
end
else
begin
  CheckBox2.Visible:=true;
  CheckBox3.Visible:=true;
end;
end;

procedure TDaftarBeliBaruFm.FormShow(Sender: TObject);
begin
  BarangQ.Open;
  SupplierQ.Open;
  MasterQ.Open;
  MasterQ.Edit;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateDaftarBeli.AsBoolean;
end;

procedure TDaftarBeliBaruFm.cxDBVerticalGrid1NamaSup1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Edit;
      MasterQSupplier1.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TDaftarBeliBaruFm.cxDBVerticalGrid2Namasup2_rEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Edit;
      MasterQSupplier2.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TDaftarBeliBaruFm.cxDBVerticalGrid3NamaSup3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Edit;
      MasterQSupplier3.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TDaftarBeliBaruFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
 if(AViewInfo.GridRecord.Values[0] = true) then
 begin
    ACanvas.Brush.Color := clRed;
 end
 else
 begin
  //ACanvas.Brush.Color := clWhite;
 end;

  if(AViewInfo.Selected) and (Screen.ActiveControl = Sender.Site) then begin
    ACanvas.Brush.Color := clBlack;
    ACanvas.Font.Color := clWhite;
  end;
end;

procedure TDaftarBeliBaruFm.cxGrid1DBTableView1CashNCarryPropertiesEditValueChanged(
  Sender: TObject);
begin
  if MasterQCashNCarry.AsBoolean then
    MasterQStatus.AsString:='ON PROCESS'
  else
    begin
      if MasterQSupplier.AsString<>'' then
        MasterQStatus.AsString:='ON PROCESS'
      else
        MasterQStatus.AsString:='NEW DB'
    end;
end;

procedure TDaftarBeliBaruFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus Daftar Beli '+MasterQKode.Asstring+ ', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
    try
      DeleteDBQ.Close;
      DeleteDBQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DeleteDBQ.ExecSQL;
      ShowMessage('Daftar Beli telah dihapus');
      MasterQ.Refresh;
    except
      MessageDlg('Daftar Beli pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
  end;
end;

end.
