unit DaftarMinStok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit;

type
  TDaftarMinStokFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1Level1: TcxGridLevel;
    MasterQ: TSDQuery;
    Panel1: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxComboBox1: TcxComboBox;
    cxGridViewRepository1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQSatuan: TStringField;
    MasterQMinimumStok: TIntegerField;
    MasterQMaximumStok: TIntegerField;
    MasterQStandardUmur: TIntegerField;
    MasterQLokasi: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    cxGridViewRepository1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column5: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column6: TcxGridDBBandedColumn;
    MasterQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    q :TSDQuery;
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
  end;

var
  DaftarMinStokFm: TDaftarMinStokFm;
  masterorisql:string;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TDaftarMinStokFm.Create(aOwner: TComponent; Query1: TSDQuery);
begin
  inherited Create(aOwner);
  q:=Query1;
end;

procedure TDaftarMinStokFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDaftarMinStokFm.FormCreate(Sender: TObject);
var i : integer;
begin
  MasterQ.Open;
  masterorisql:=masterq.sql.text;
end;

procedure TDaftarMinStokFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDaftarMinStokFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TDaftarMinStokFm.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.ItemIndex=0 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.nama like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end;
end;

procedure TDaftarMinStokFm.cxComboBox1PropertiesChange(Sender: TObject);
begin
  cxTextEdit1.Clear;
end;

procedure TDaftarMinStokFm.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
end;

end.
