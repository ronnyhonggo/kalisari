object DaftarMinStokKategoriFm: TDaftarMinStokKategoriFm
  Left = 450
  Top = 205
  Width = 959
  Height = 616
  Caption = 'Daftar Minimum Stok Kategori'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poPrintToFit
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 548
    Width = 943
    Height = 29
    Align = alBottom
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 41
    Width = 943
    Height = 507
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGridViewRepository1DBBandedTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 943
    Height = 41
    Align = alTop
    TabOrder = 2
    object cxTextEdit1: TcxTextEdit
      Left = 16
      Top = 8
      Properties.OnChange = cxTextEdit1PropertiesChange
      TabOrder = 0
      Width = 121
    end
    object cxComboBox1: TcxComboBox
      Left = 152
      Top = 8
      Properties.Items.Strings = (
        'Nama')
      Properties.OnChange = cxComboBox1PropertiesChange
      TabOrder = 1
      Text = 'Nama'
      Width = 121
    end
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 296
    Top = 372
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 432
    Top = 384
    object cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView
      DragMode = dmAutomatic
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = DataSource1
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded, dcoImmediatePost, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsBehavior.ShowLockedStateImageOptions.BestFit = lsimImmediate
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsData.Editing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.CellEndEllipsis = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.BandCaptionsInColumnAlternateCaption = True
      OptionsView.BandHeaderEndEllipsis = True
      Styles.Selection = cxStyle1
      Bands = <
        item
          Width = 90
        end>
      object cxGridViewRepository1DBBandedTableView1Column1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kode'
        Width = 95
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kategori'
        Options.SortByDisplayText = isbtOn
        Width = 359
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Jumlah'
        Width = 118
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Satuan'
        Width = 120
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column5: TcxGridDBBandedColumn
        DataBinding.FieldName = 'MinStok'
        Width = 119
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'MaxStok'
        Width = 118
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 552
    Top = 400
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 640
    Top = 392
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clHighlight
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select k.*, (select sum(jumlah) as jum from barang b, kategoriba' +
        'rang kb where kb.kode=b.kategori and kb.kode=k.kode group by b.k' +
        'ategori) as Jumlah, (select satuan from barang where kategori=k.' +
        'kode group by satuan) as Satuan from kategoribarang k where k.mi' +
        'nstok is not null and k.minstok > 0')
    Left = 345
    Top = 313
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Required = True
      Size = 50
    end
    object MasterQMinStok: TIntegerField
      FieldName = 'MinStok'
    end
    object MasterQMaxStok: TIntegerField
      FieldName = 'MaxStok'
    end
    object MasterQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 10
    end
    object MasterQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
end
