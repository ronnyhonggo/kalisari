object DaftarPesertaFm: TDaftarPesertaFm
  Left = 292
  Top = 76
  Width = 837
  Height = 638
  BorderIcons = [biSystemMenu]
  Caption = 'Daftar Pelanggan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl2: TPanel
    Left = 353
    Top = 0
    Width = 468
    Height = 600
    Align = alClient
    TabOrder = 0
    object StatusBar: TStatusBar
      Left = 1
      Top = 574
      Width = 466
      Height = 25
      Panels = <
        item
          Width = 50
        end>
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 353
    Height = 600
    Align = alLeft
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 41
      Width = 351
      Height = 558
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1NamaPT: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPT'
          Width = 349
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 351
      Height = 40
      Align = alTop
      Caption = 'Panel2'
      TabOrder = 1
      object lbl1: TLabel
        Left = 6
        Top = 13
        Width = 43
        Height = 16
        Caption = 'Nama :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxTextEdit1: TcxTextEdit
        Left = 56
        Top = 12
        TabOrder = 0
        OnKeyUp = cxTextEdit1KeyUp
        Width = 137
      end
    end
  end
  object Panel1: TPanel
    Left = 353
    Top = 0
    Width = 468
    Height = 600
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 2
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 466
      Height = 598
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 209
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Kode: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Kode'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NamaPT: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPT'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Alamat: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Alamat'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1Kota: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Kota'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1NoTelp: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoTelp'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1Email: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Email'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1NoFax: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoFax'
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1NamaPIC1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPIC1'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object cxDBVerticalGrid1TelpPIC1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TelpPIC1'
        ID = 8
        ParentID = -1
        Index = 8
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPIC1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPIC1'
        ID = 9
        ParentID = -1
        Index = 9
        Version = 1
      end
      object cxDBVerticalGrid1NamaPIC2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPIC2'
        ID = 10
        ParentID = -1
        Index = 10
        Version = 1
      end
      object cxDBVerticalGrid1TelpPIC2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TelpPIC2'
        ID = 11
        ParentID = -1
        Index = 11
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPIC2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPIC2'
        ID = 12
        ParentID = -1
        Index = 12
        Version = 1
      end
      object cxDBVerticalGrid1NamaPIC3: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPIC3'
        ID = 13
        ParentID = -1
        Index = 13
        Version = 1
      end
      object cxDBVerticalGrid1TelpPIC3: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TelpPIC3'
        ID = 14
        ParentID = -1
        Index = 14
        Version = 1
      end
      object cxDBVerticalGrid1JabatanPIC3: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPIC3'
        ID = 15
        ParentID = -1
        Index = 15
        Version = 1
      end
    end
  end
  object ViewPelangganQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Pelanggan')
    Left = 265
    Top = 1
    object ViewPelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewPelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object ViewPelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object ViewPelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object ViewPelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object ViewPelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object ViewPelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object ViewPelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewPelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewPelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewPelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewPelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object ViewPelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object ViewPelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object ViewPelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object ViewPelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object ViewPelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object ViewPelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object ViewPelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object ViewPelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewPelangganQ
    Left = 300
    Top = 6
  end
  object cekpanjangQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select Isnull(max(len(NamaPT)),1)as panjang1,Isnull(max(len(Alam' +
        'at)),1)as panjang2,Isnull(max(len(NoTelp)),1)as panjang3,Isnull(' +
        'max(len(NamaPIC1)),1)as panjang4,Isnull(max(len(TelpPIC1)),1)as ' +
        'panjang5,Isnull(max(len(JabatanPIC1)),1)as panjang6 from Pelangg' +
        'an')
    Left = 233
    Top = 1
    object cekpanjangQpanjang1: TIntegerField
      FieldName = 'panjang1'
    end
    object cekpanjangQpanjang2: TIntegerField
      FieldName = 'panjang2'
    end
    object cekpanjangQpanjang3: TIntegerField
      FieldName = 'panjang3'
    end
    object cekpanjangQpanjang4: TIntegerField
      FieldName = 'panjang4'
    end
    object cekpanjangQpanjang5: TIntegerField
      FieldName = 'panjang5'
    end
    object cekpanjangQpanjang6: TIntegerField
      FieldName = 'panjang6'
    end
  end
  object DataSource1: TDataSource
    DataSet = masterQ
    Left = 544
    Top = 8
  end
  object masterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Pelanggan')
    Left = 504
    Top = 8
    object masterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object masterQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object masterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object masterQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object masterQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object masterQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object masterQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object masterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object masterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object masterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object masterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object masterQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object masterQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object masterQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object masterQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object masterQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object masterQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object masterQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object masterQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object masterQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
end
