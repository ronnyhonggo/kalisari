unit DaftarPeserta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo;

type
  TDaftarPesertaFm = class(TForm)
    pnl2: TPanel;
    ViewPelangganQ: TSDQuery;
    ViewDs: TDataSource;
    StatusBar: TStatusBar;
    pnl1: TPanel;
    cekpanjangQ: TSDQuery;
    cekpanjangQpanjang1: TIntegerField;
    cekpanjangQpanjang2: TIntegerField;
    cekpanjangQpanjang3: TIntegerField;
    cekpanjangQpanjang4: TIntegerField;
    cekpanjangQpanjang5: TIntegerField;
    cekpanjangQpanjang6: TIntegerField;
    DataSource1: TDataSource;
    masterQ: TSDQuery;
    masterQKode: TStringField;
    masterQNamaPT: TStringField;
    masterQAlamat: TStringField;
    masterQKota: TStringField;
    masterQNoTelp: TStringField;
    masterQEmail: TStringField;
    masterQNoFax: TStringField;
    masterQCreateDate: TDateTimeField;
    masterQCreateBy: TStringField;
    masterQOperator: TStringField;
    masterQTglEntry: TDateTimeField;
    masterQNamaPIC1: TStringField;
    masterQTelpPIC1: TStringField;
    masterQJabatanPIC1: TStringField;
    masterQNamaPIC2: TStringField;
    masterQTelpPIC2: TStringField;
    masterQJabatanPIC2: TStringField;
    masterQNamaPIC3: TStringField;
    masterQTelpPIC3: TStringField;
    masterQJabatanPIC3: TStringField;
    ViewPelangganQKode: TStringField;
    ViewPelangganQNamaPT: TStringField;
    ViewPelangganQAlamat: TStringField;
    ViewPelangganQKota: TStringField;
    ViewPelangganQNoTelp: TStringField;
    ViewPelangganQEmail: TStringField;
    ViewPelangganQNoFax: TStringField;
    ViewPelangganQCreateDate: TDateTimeField;
    ViewPelangganQCreateBy: TStringField;
    ViewPelangganQOperator: TStringField;
    ViewPelangganQTglEntry: TDateTimeField;
    ViewPelangganQNamaPIC1: TStringField;
    ViewPelangganQTelpPIC1: TStringField;
    ViewPelangganQJabatanPIC1: TStringField;
    ViewPelangganQNamaPIC2: TStringField;
    ViewPelangganQTelpPIC2: TStringField;
    ViewPelangganQJabatanPIC2: TStringField;
    ViewPelangganQNamaPIC3: TStringField;
    ViewPelangganQTelpPIC3: TStringField;
    ViewPelangganQJabatanPIC3: TStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1NamaPT: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1Kode: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPT: TcxDBEditorRow;
    cxDBVerticalGrid1Alamat: TcxDBEditorRow;
    cxDBVerticalGrid1Kota: TcxDBEditorRow;
    cxDBVerticalGrid1NoTelp: TcxDBEditorRow;
    cxDBVerticalGrid1Email: TcxDBEditorRow;
    cxDBVerticalGrid1NoFax: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPIC1: TcxDBEditorRow;
    cxDBVerticalGrid1TelpPIC1: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPIC1: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPIC2: TcxDBEditorRow;
    cxDBVerticalGrid1TelpPIC2: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPIC2: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPIC3: TcxDBEditorRow;
    cxDBVerticalGrid1TelpPIC3: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPIC3: TcxDBEditorRow;
    Panel2: TPanel;
    lbl1: TLabel;
    cxTextEdit1: TcxTextEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxTextEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  DaftarPesertaFm: TDaftarPesertaFm;
  paramkode :string;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TDaftarPesertaFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
  cxTextEdit1.Text:=paramkode;
      ViewPelangganQ.Close;
    ViewPelangganQ.SQL.Clear;
    ViewPelangganQ.SQL.Add('select * from pelanggan where namapt like '+ QuotedStr('%'+cxTextEdit1.Text+'%'));
    ViewPelangganQ.ExecSQL;
    ViewPelangganQ.Open;
    
end;

procedure TDaftarPesertaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDaftarPesertaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TDaftarPesertaFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TDaftarPesertaFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=ViewPelangganQ.SQL.Text;
  ViewPelangganQ.Open;
  cekpanjangQ.Close;
  cekpanjangQ.Open;
  cekpanjangQ.First;
  //cxGrid1DBTableView1NamaPT.Width:= cekpanjangQpanjang1.AsInteger*8;
  {cxGrid1DBTableView1Alamat.Width:= cekpanjangQpanjang2.AsInteger*8;
  cxGrid1DBTableView1NoTelp.Width:= cekpanjangQpanjang3.AsInteger*8;
  cxGrid1DBTableView1NamaPIC1.Width := cekpanjangQpanjang4.AsInteger*8;
  cxGrid1DBTableView1TelpPIC1.Width := cekpanjangQpanjang5.AsInteger*8;
  cxGrid1DBTableView1JabatanPIC1.Width := cekpanjangQpanjang6.AsInteger*8; }
end;


procedure TDaftarPesertaFm.cxTextEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ViewPelangganQ.Close;
    ViewPelangganQ.SQL.Clear;
    ViewPelangganQ.SQL.Add('select * from pelanggan where namapt like '+ QuotedStr('%'+cxTextEdit1.Text+'%'));
    ViewPelangganQ.ExecSQL;
    ViewPelangganQ.Open;
end;

procedure TDaftarPesertaFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewPelangganQkode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    //MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

end.
