unit DropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    q :TSDQuery;
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
  end;

var
  DropDownFm: TDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TDropDownFm.Create(aOwner: TComponent; Query1: TSDQuery);
begin
  inherited Create(aOwner);
  q:=Query1;
end;

procedure TDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDropDownFm.FormCreate(Sender: TObject);
var i,fieldcount : integer;
begin
  fieldcount:=0;
  DataSource1.DataSet:=q;
  cxGridViewRepository1DBBandedTableView1.DataController.DataSource:=DataSource1;
  q.Open;
  for i:=0 to DataSource1.DataSet.FieldCount-1 do
  begin
    if DataSource1.DataSet.Fields[i].Visible=true then
    begin
      fieldcount:=fieldcount+1;
      cxGridViewRepository1DBBandedTableView1.CreateColumn.DataBinding.FieldName:=DataSource1.DataSet.Fields[i].FieldName;
      cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Options.Editing:=true;
      if DataSource1.DataSet.Fields[i].FieldName='AC' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='Toilet' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='AirSuspension' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='Kode' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='KodeBarang' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=100
      else if DataSource1.DataSet.Fields[i].FieldName='KodeIDBan' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=100
      else if DataSource1.DataSet.Fields[i].FieldName='Ring' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Velg' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Tol' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Mel' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Poin' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Jarak' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Waktu' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=50
      else if DataSource1.DataSet.Fields[i].FieldName='Asal' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Tujuan' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='StandardUmur' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='StandardKM' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='JumlahSeat' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='NoBody' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='PlatNo' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='JenisAC' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='MerkAC' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='TipeAC' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='JenisBBM' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='LevelArmada' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Aktif' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='KirMulai' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='KirSelesai' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Status' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Jenis' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Jumlah' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='Satuan' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60
      else if DataSource1.DataSet.Fields[i].FieldName='ClaimNWarranty' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='SingleSupplier' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Kategori' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=80
      else if DataSource1.DataSet.Fields[i].FieldName='Harga' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=60

      else cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=120;
    end;
  end;
  cxGrid1Level1.GridView:=cxGridViewRepository1DBBandedTableView1;
  if fieldcount<10 then cxGridViewRepository1DBBandedTableView1.OptionsView.ColumnAutoWidth:=true;
end;

procedure TDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

end.
