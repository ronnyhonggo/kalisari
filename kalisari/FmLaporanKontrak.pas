unit FmLaporanKontrak;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinsDefaultPainters, DB, ADODB, UCrpeClasses, UCrpe32,
  StdCtrls, cxButtons, SDEngine, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxControls,
  cxContainer, cxEdit, cxDropDownEdit, cxTextEdit, cxMaskEdit, cxCalendar;

type
  TFrmKontrak = class(TForm)
    Label2: TLabel;
    lblstatus: TLabel;
    cxButton1: TcxButton;
    Crpe1: TCrpe;
    Label1: TLabel;
    masterq: TSDQuery;
    masterds: TDataSource;
    ok: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxComboBox1: TcxComboBox;
    cxDateEdit2: TcxDateEdit;
    Label3: TLabel;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmKontrak: TFrmKontrak;

implementation
uses MenuUtama, StrUtils;
{$R *.dfm}

procedure TFrmKontrak.cxButton1Click(Sender: TObject);
begin
  lblstatus.Visible:=True;
  Crpe1.Refresh;

  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Kontrak.rpt';
  if cxComboBox1.ItemIndex=0 then
  begin
    Crpe1.ParamFields[0].CurrentValue:='semua';
  end
  else
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from pelanggan where namaPT='+QuotedStr(cxComboBox1.Text));
    MasterQ.Open;
    while not MasterQ.Eof do
    begin
      Crpe1.ParamFields[0].CurrentValue:=MasterQ.fieldbyname('kode').AsString;
      MasterQ.Next;
    end;
    MasterQ.Close;
  end;
  Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', cxDateEdit1.Date);
  Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', cxDateEdit2.Date);
  Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

  Crpe1.Execute;
  Crpe1.WindowState:= wsMaximized;
  lblstatus.Visible:=False;
end;

procedure TFrmKontrak.FormCreate(Sender: TObject);
begin
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from pelanggan');
  MasterQ.Open;
  while not MasterQ.Eof do
  begin
    cxComboBox1.Properties.Items.Add(MasterQ.fieldbyname('namaPT').AsString);
    MasterQ.Next;
  end;
  MasterQ.Close;
end;

end.
