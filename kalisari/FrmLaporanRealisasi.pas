unit FrmLaporanRealisasi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UCrpeClasses, UCrpe32, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ComCtrls, Menus, cxButtons, DB, SDEngine, ADODB, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TFrmRealisasi = class(TForm)
    ComboBox1: TComboBox;
    Label2: TLabel;
    DateTimePickerDay: TDateTimePicker;
    Label3: TLabel;
    RadioButtonTgl: TRadioButton;
    RadioButtonBln: TRadioButton;
    DateTimePickerDay2: TDateTimePicker;
    Label4: TLabel;
    DateTimePickerMonth: TDateTimePicker;
    DateTimePickerMonth2: TDateTimePicker;
    Label5: TLabel;
    cxButton1: TcxButton;
    Crpe1: TCrpe;
    lblstatus: TLabel;
    Label6: TLabel;
    ComboBox2: TComboBox;
    ok: TLabel;
    masterq: TSDQuery;
    masterds: TDataSource;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  FrmRealisasi: TFrmRealisasi;

implementation

uses DateUtils, MenuUtama, StrUtils;

{$R *.dfm}


procedure TFrmRealisasi.cxButton1Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
  lblstatus.Visible:=True;
  Crpe1.Refresh;

  if ComboBox1.ItemIndex=0 then
  begin
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'R0_All.rpt';
  end
  else if ComboBox1.ItemIndex=1 then
  begin
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'R0_Armada.rpt';

  end
  else if ComboBox1.ItemIndex=2 then
  begin
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'R0_Ekor.rpt';

  end
    else if ComboBox1.ItemIndex=3 then
  begin
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'R0_Sopir.rpt';

  end;

  Crpe1.ParamFields[3].CurrentValue:=ComboBox2.Text;
  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='date';
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay2.Date);
    end
    else
    begin
      Crpe1.ParamFields[2].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

  Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

  //ShowMessage(Crpe1.ReportName);

    Crpe1.Execute;
    Crpe1.WindowState:= wsMaximized;
    lblstatus.Visible:=False;
    //self.Visible:=false;
end;

procedure TFrmRealisasi.FormShow(Sender: TObject);
begin
lblstatus.Visible:=False;
  DateTimePickerDay.DateTime:=Today;
  DateTimePickerDay2.DateTime:=Today;
end;

constructor TFrmRealisasi.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TFrmRealisasi.FormCreate(Sender: TObject);
begin
DateTimePickerDay.Format:='';
DateTimePickerDay2.Format:='';

DateTimePickerDay.Date:=now;
  DateTimePickerDay2.Date:=now;
  DateTimePickerMonth.Date:=now;
  DateTimePickerMonth2.Date:=now;

  

  ComboBox2.Items.Clear;
  ComboBox2.Items.Add('semua');

  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select distinct status from mastersj');
  MasterQ.Open;
  while not MasterQ.Eof do
  begin
    ComboBox2.Items.Add(MasterQ.fieldbyname('status').AsString);
    MasterQ.Next;
  end;
  MasterQ.Close;
  ComboBox2.ItemIndex:=0;
end;

end.
