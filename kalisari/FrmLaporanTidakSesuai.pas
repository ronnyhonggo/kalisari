unit frmlaporantidaksesuai;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinsDefaultPainters, UCrpeClasses, UCrpe32, StdCtrls,
  cxButtons, ComCtrls, DB, SDEngine, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TFmLaporanTidakSesuai = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblstatus: TLabel;
    RadioButtonBln: TRadioButton;
    RadioButtonTgl: TRadioButton;
    DateTimePickerDay: TDateTimePicker;
    DateTimePickerDay2: TDateTimePicker;
    DateTimePickerMonth: TDateTimePicker;
    DateTimePickerMonth2: TDateTimePicker;
    cxButton1: TcxButton;
    masterds: TDataSource;
    masterq: TSDQuery;
    Crpe1: TCrpe;
    Label1: TLabel;
    ok: TLabel;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmLaporanTidakSesuai: TFmLaporanTidakSesuai;

implementation

uses MenuUtama, StrUtils, DateUtils;

{$R *.dfm}

procedure TFmLaporanTidakSesuai.cxButton1Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
    lblstatus.Visible:=True;
    Crpe1.Refresh;

    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Order Tidak Sesuai.rpt';
    if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='date';
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay2.Date);
    end
    else
    begin
      Crpe1.ParamFields[2].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


    Crpe1.Execute;
    lblstatus.Visible:=False;
     Crpe1.WindowState:= wsMaximized;
    //self.Visible:=false
end;

procedure TFmLaporanTidakSesuai.FormShow(Sender: TObject);
begin
//  DateTimePickerDay.DateTime:=Today;
  //DateTimePickerDay2.DateTime:=Today;
  
end;

procedure TFmLaporanTidakSesuai.FormCreate(Sender: TObject);
begin
DateTimePickerDay.Format:='';
DateTimePickerDay2.Format:='';

DateTimePickerDay.Date:=now;
  DateTimePickerDay2.Date:=now;
  DateTimePickerMonth.Date:=now;
  DateTimePickerMonth2.Date:=now;
end;

end.
