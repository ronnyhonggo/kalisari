object IsianPremiFm: TIsianPremiFm
  Left = 437
  Top = 212
  AutoScroll = False
  Caption = 'IsianPremiFm'
  ClientHeight = 231
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 331
    Height = 231
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 101
      Height = 16
      Caption = 'Jenis Kendaraan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 32
      Width = 69
      Height = 16
      Caption = 'Dalam kota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 56
      Width = 32
      Height = 16
      Caption = 'Jatim'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 80
      Width = 68
      Height = 16
      Caption = 'Jateng/Bali'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 104
      Width = 84
      Height = 16
      Caption = 'Jabar/Jakarta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 128
      Width = 59
      Height = 16
      Caption = 'Anjem PP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 152
      Width = 71
      Height = 16
      Caption = 'Anjem Drop'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Panel2: TPanel
      Left = 1
      Top = 189
      Width = 329
      Height = 41
      Align = alBottom
      TabOrder = 0
      object Button1: TButton
        Left = 16
        Top = 8
        Width = 75
        Height = 25
        Caption = 'HitungPremi'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
    object ComboBox1: TComboBox
      Left = 136
      Top = 8
      Width = 121
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = 'Bis besar'
      OnChange = ComboBox1Change
      Items.Strings = (
        'Bis besar'
        'Mini bis'
        'ELF')
    end
    object Edit1: TEdit
      Left = 136
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '0'
    end
    object Edit2: TEdit
      Left = 136
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 3
      Text = '0'
    end
    object Edit3: TEdit
      Left = 136
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 4
      Text = '0'
    end
    object Edit4: TEdit
      Left = 136
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 5
      Text = '0'
    end
    object Edit5: TEdit
      Left = 136
      Top = 128
      Width = 121
      Height = 21
      TabOrder = 6
      Text = '0'
    end
    object Edit6: TEdit
      Left = 136
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 7
      Text = '0'
    end
  end
  object PremiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select cast('#39#39' as varchar(20)) as JenisKendaraan,'
      'cast(0 as numeric(18,4)) as BisBesarDalamKota,'
      'cast(0 as numeric(18,4)) as BisBesarJatim,'
      'cast(0 as numeric(18,4)) as BisBesarJatengBali,'
      'cast(0 as numeric(18,4)) as BisBesarJabarJkt,'
      'cast(0 as numeric(18,4)) as BisBesarAnjemPP,'
      'cast(0 as numeric(18,4)) as BisBesarAnjemDrop,'
      'cast(0 as numeric(18,4)) as BisKecilDalamKota,'
      'cast(0 as numeric(18,4)) as BisKecilJatim,'
      'cast(0 as numeric(18,4)) as BisKecilJatengBali,'
      'cast(0 as numeric(18,4)) as BisKecilJabarJkt,'
      'cast(0 as numeric(18,4)) as BisKecilAnjemPP,'
      'cast(0 as numeric(18,4)) as BisKecilAnjemDrop,'
      'cast(0 as numeric(18,4)) as ELFDalamKota,'
      'cast(0 as numeric(18,4)) as ELFJatim,'
      'cast(0 as numeric(18,4)) as ELFJatengBali,'
      'cast(0 as numeric(18,4)) as ELFJabarJkt'
      'from MasterSJ'
      ''
      ''
      ''
      '')
    UpdateObject = SDUpdateSQL1
    Left = 400
    Top = 56
    object PremiQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
    end
    object PremiQBisBesarDalamKota: TFloatField
      FieldName = 'BisBesarDalamKota'
    end
    object PremiQBisBesarJatim: TFloatField
      FieldName = 'BisBesarJatim'
    end
    object PremiQBisBesarJatengBali: TFloatField
      FieldName = 'BisBesarJatengBali'
    end
    object PremiQBisBesarJabarJkt: TFloatField
      FieldName = 'BisBesarJabarJkt'
    end
    object PremiQBisBesarAnjemPP: TFloatField
      FieldName = 'BisBesarAnjemPP'
    end
    object PremiQBisBesarAnjemDrop: TFloatField
      FieldName = 'BisBesarAnjemDrop'
    end
    object PremiQBisKecilDalamKota: TFloatField
      FieldName = 'BisKecilDalamKota'
    end
    object PremiQBisKecilJatim: TFloatField
      FieldName = 'BisKecilJatim'
    end
    object PremiQBisKecilJatengBali: TFloatField
      FieldName = 'BisKecilJatengBali'
    end
    object PremiQBisKecilJabarJkt: TFloatField
      FieldName = 'BisKecilJabarJkt'
    end
    object PremiQBisKecilAnjemPP: TFloatField
      FieldName = 'BisKecilAnjemPP'
    end
    object PremiQBisKecilAnjemDrop: TFloatField
      FieldName = 'BisKecilAnjemDrop'
    end
    object PremiQELFDalamKota: TFloatField
      FieldName = 'ELFDalamKota'
    end
    object PremiQELFJatim: TFloatField
      FieldName = 'ELFJatim'
    end
    object PremiQELFJatengBali: TFloatField
      FieldName = 'ELFJatengBali'
    end
    object PremiQELFJabarJkt: TFloatField
      FieldName = 'ELFJabarJkt'
    end
  end
  object DataSource1: TDataSource
    DataSet = PremiQ
    Left = 432
    Top = 56
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other' +
        ', SPBUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, Keteran' +
        'ganVerifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang'
      'from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSJ'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other,'
      '  SPBUAYaniDetail = :SPBUAYaniDetail,'
      '  TitipTagihan = :TitipTagihan,'
      '  SudahPrint = :SudahPrint,'
      '  Verifikasi = :Verifikasi,'
      '  KeteranganVerifikasi = :KeteranganVerifikasi,'
      '  DanaKebersihan = :DanaKebersihan,'
      '  VerifikasiStatus = :VerifikasiStatus,'
      '  KmSekarang = :KmSekarang'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSJ'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other, SP' +
        'BUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, KeteranganV' +
        'erifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other, :SPBUAYaniDetail,' +
        ' :TitipTagihan, :SudahPrint, :Verifikasi, :KeteranganVerifikasi,' +
        ' :DanaKebersihan, :VerifikasiStatus, :KmSekarang)')
    DeleteSQL.Strings = (
      'delete from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 464
    Top = 56
  end
end
