unit IsianPremi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxStyles,
  cxEdit, cxDropDownEdit, cxVGrid, cxDBVGrid, DB, cxInplaceContainer,
  SDEngine, cxCurrencyEdit, cxTextEdit, cxCalc, cxCalendar, cxMemo,
  cxRadioGroup, cxButtonEdit, cxCheckBox, StdCtrls;

type
  TIsianPremiFm = class(TForm)
    Panel1: TPanel;
    PremiQ: TSDQuery;
    DataSource1: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    PremiQJenisKendaraan: TStringField;
    Panel2: TPanel;
    PremiQBisBesarDalamKota: TFloatField;
    PremiQBisBesarJatim: TFloatField;
    PremiQBisBesarJatengBali: TFloatField;
    PremiQBisBesarJabarJkt: TFloatField;
    PremiQBisBesarAnjemPP: TFloatField;
    PremiQBisBesarAnjemDrop: TFloatField;
    PremiQBisKecilDalamKota: TFloatField;
    PremiQBisKecilJatim: TFloatField;
    PremiQBisKecilJatengBali: TFloatField;
    PremiQBisKecilJabarJkt: TFloatField;
    PremiQBisKecilAnjemPP: TFloatField;
    PremiQBisKecilAnjemDrop: TFloatField;
    PremiQELFDalamKota: TFloatField;
    PremiQELFJatim: TFloatField;
    PremiQELFJatengBali: TFloatField;
    PremiQELFJabarJkt: TFloatField;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IsianPremiFm: TIsianPremiFm;

implementation

uses MenuUtama, DropDown, DM, StrUtils,RealisasiSuratJalan;

{$R *.dfm}

procedure TIsianPremiFm.Button1Click(Sender: TObject);
begin
if ComboBox1.ItemIndex=0 then
begin
   RealisasiSuratJalanFm.MasterQ.Edit;
   RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency:=(strtofloat(Edit1.Text)*135000)+(strtofloat(Edit2.Text)*160000)+(strtofloat(Edit3.Text)*170000)+(strtofloat(Edit4.Text)*180000)+(strtofloat(Edit5.Text)*130000)+(strtofloat(Edit6.Text)*105000);
   RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency:=(strtofloat(Edit1.Text)*70000)+(strtofloat(Edit2.Text)*80000)+(strtofloat(Edit3.Text)*85000)+(strtofloat(Edit4.Text)*90000)+(strtofloat(Edit5.Text)*60000)+(strtofloat(Edit6.Text)*57000);
   RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency:=RealisasiSuratJalanFm.MasterQSPBULuarUang.AsCurrency+RealisasiSuratJalanFm.MasterQtol.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir2.AsCurrency+RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency+RealisasiSuratJalanFm.MasterQClaimSopir.AsCurrency+RealisasiSuratJalanFm.MasterQBiayaLainLain.AsCurrency+RealisasiSuratJalanFm.MasterQOther.AsCurrency+RealisasiSuratJalanFm.MasterQUangInap.AsCurrency+RealisasiSuratJalanFm.MasterQUangParkir.AsCurrency+RealisasiSuratJalanFm.MasterQDanaKebersihan.AsCurrency;
   RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency:=RealisasiSuratJalanFm.MasterQPendapatan.AsCurrency-RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency;
      if RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency<0 then
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
end
else if ComboBox1.ItemIndex=1 then
begin
   RealisasiSuratJalanFm.MasterQ.Edit;
   RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency:=(strtofloat(Edit1.Text)*125000)+(strtofloat(Edit2.Text)*150000)+(strtofloat(Edit3.Text)*160000)+(strtofloat(Edit4.Text)*175000)+(strtofloat(Edit5.Text)*120000)+(strtofloat(Edit6.Text)*100000);
   RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency:=(strtofloat(Edit1.Text)*65000)+(strtofloat(Edit2.Text)*75000)+(strtofloat(Edit3.Text)*80000)+(strtofloat(Edit4.Text)*85000)+(strtofloat(Edit5.Text)*55000)+(strtofloat(Edit6.Text)*55000);
   RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency:=RealisasiSuratJalanFm.MasterQSPBULuarUang.AsCurrency+RealisasiSuratJalanFm.MasterQtol.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir2.AsCurrency+RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency+RealisasiSuratJalanFm.MasterQClaimSopir.AsCurrency+RealisasiSuratJalanFm.MasterQBiayaLainLain.AsCurrency+RealisasiSuratJalanFm.MasterQOther.AsCurrency+RealisasiSuratJalanFm.MasterQUangInap.AsCurrency+RealisasiSuratJalanFm.MasterQUangParkir.AsCurrency+RealisasiSuratJalanFm.MasterQDanaKebersihan.AsCurrency;
   RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency:=RealisasiSuratJalanFm.MasterQPendapatan.AsCurrency-RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency;
      if RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency<0 then
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
end

else if ComboBox1.ItemIndex=2 then
begin
   RealisasiSuratJalanFm.MasterQ.Edit;
   RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency:=(strtofloat(Edit1.Text)*70000)+(strtofloat(Edit2.Text)*105000)+(strtofloat(Edit3.Text)*130000)+(strtofloat(Edit4.Text)*140000)+(strtofloat(Edit5.Text)*150000);
   RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency:=0;
   RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency:=RealisasiSuratJalanFm.MasterQSPBULuarUang.AsCurrency+RealisasiSuratJalanFm.MasterQtol.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir.AsCurrency+RealisasiSuratJalanFm.MasterQPremiSopir2.AsCurrency+RealisasiSuratJalanFm.MasterQPremiKernet.AsCurrency+RealisasiSuratJalanFm.MasterQClaimSopir.AsCurrency+RealisasiSuratJalanFm.MasterQBiayaLainLain.AsCurrency+RealisasiSuratJalanFm.MasterQOther.AsCurrency+RealisasiSuratJalanFm.MasterQUangInap.AsCurrency+RealisasiSuratJalanFm.MasterQUangParkir.AsCurrency+RealisasiSuratJalanFm.MasterQDanaKebersihan.AsCurrency;
   RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency:=RealisasiSuratJalanFm.MasterQPendapatan.AsCurrency-RealisasiSuratJalanFm.MasterQPengeluaran.AsCurrency;
      if RealisasiSuratJalanFm.MasterQSisaDisetor.AsCurrency<0 then
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          RealisasiSuratJalanFm.cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
end;

end;

procedure TIsianPremiFm.ComboBox1Change(Sender: TObject);
begin
if ComboBox1.ItemIndex=0 then
begin
   Label2.Caption:='Dalam kota';
   Label3.Caption:='Jatim';
   Label4.Caption:='Jateng/Bali';
   Label5.Caption:='Jabar/Jakarta';
   Label6.Caption:='Anjem PP';
   Label7.Visible:=true;
   Edit6.Visible:=true;
   Label7.Caption:='Anjem Drop';
end

else if ComboBox1.ItemIndex=1 then
begin
   Label2.Caption:='Dalam kota';
   Label3.Caption:='Jatim';
   Label4.Caption:='Jateng/Bali';
   Label5.Caption:='Jabar/Jakarta';
   Label6.Caption:='Anjem PP';
   Label7.Visible:=true;
   Edit6.Visible:=true;
   Label7.Caption:='Anjem Drop';
end

else
begin
   Label2.Caption:='Setengah hari';
   Label3.Caption:='Dalam kota';
   Label4.Caption:='Jatim';
   Label5.Caption:='Jateng/Bali';
   Label6.Caption:='Jabar/Jakarta';
   Label7.Visible:=false;
   Edit6.Visible:=false;
end;
end;

end.
