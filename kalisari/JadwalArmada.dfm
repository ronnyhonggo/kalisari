object JadwalArmadaFm: TJadwalArmadaFm
  Left = 81
  Top = 46
  Width = 1120
  Height = 715
  Caption = 'Jadwal Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1104
    Height = 452
    Align = alClient
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 1
      Top = 145
      Width = 1102
      Height = 306
      Align = alClient
      ColCount = 10
      DefaultColWidth = 70
      RowCount = 10
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNone
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnDrawCell = StringGrid1DrawCell
      OnSelectCell = StringGrid1SelectCell
      ColWidths = (
        70
        78
        72
        76
        83
        82
        70
        70
        70
        70)
    end
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 1102
      Height = 144
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object cxComboBox1: TcxComboBox
        Left = 16
        Top = 8
        Properties.Items.Strings = (
          'Normal'
          'Bulan'
          'Range')
        Properties.OnChange = cxComboBox1PropertiesChange
        TabOrder = 0
        Text = 'Normal'
        Width = 121
      end
      object DateTimePicker1: TDateTimePicker
        Left = 144
        Top = 8
        Width = 89
        Height = 21
        Date = 41264.032495659730000000
        Format = 'MMM-yyy'
        Time = 41264.032495659730000000
        TabOrder = 1
        Visible = False
      end
      object BBulan: TButton
        Left = 248
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Tampilkan'
        TabOrder = 2
        Visible = False
        OnClick = BBulanClick
      end
      object DateTimePicker2: TDateTimePicker
        Left = 144
        Top = 9
        Width = 89
        Height = 21
        Date = 41264.032495659730000000
        Format = 'dd-MMM-yyy'
        Time = 41264.032495659730000000
        TabOrder = 3
        Visible = False
      end
      object DateTimePicker3: TDateTimePicker
        Left = 239
        Top = 7
        Width = 89
        Height = 21
        Date = 41264.032495659730000000
        Format = 'dd-MMM-yyy'
        Time = 41264.032495659730000000
        TabOrder = 4
        Visible = False
      end
      object BRange: TButton
        Left = 340
        Top = 6
        Width = 75
        Height = 25
        Caption = 'Tampilkan'
        TabOrder = 5
        Visible = False
        OnClick = BRangeClick
      end
      object cxLabel1: TcxLabel
        Left = 456
        Top = 8
        Caption = 'Tabel Jadwal Armada'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -32
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
      object BNormal: TButton
        Left = 148
        Top = 6
        Width = 75
        Height = 25
        Caption = 'Tampilkan'
        TabOrder = 7
        OnClick = BNormalClick
      end
      object cxCheckBoxToilet: TcxCheckBox
        Left = 16
        Top = 40
        Caption = 'Toilet'
        TabOrder = 8
        Visible = False
        Width = 121
      end
      object cxCheckBoxAirSuspension: TcxCheckBox
        Left = 16
        Top = 64
        Caption = 'Air Suspension'
        TabOrder = 9
        Visible = False
        Width = 121
      end
      object cxCheckBoxAktif: TcxCheckBox
        Left = 16
        Top = 88
        Caption = 'Aktif'
        TabOrder = 10
        Visible = False
        Width = 121
      end
      object cxCheckBoxAC: TcxCheckBox
        Left = 16
        Top = 112
        Caption = 'AC'
        TabOrder = 11
        Visible = False
        Width = 121
      end
      object cxLabelPlat: TcxLabel
        Left = 163
        Top = 40
        Caption = 'PlatNo/BadanMesin:'
      end
      object cxLabeltahunpembuatan: TcxLabel
        Left = 168
        Top = 68
        Caption = 'Tahun Pembuatan :'
      end
      object cxTextEditPlatno: TcxTextEdit
        Left = 280
        Top = 40
        TabOrder = 14
        Width = 121
      end
      object cxTextEditTahun: TcxTextEdit
        Left = 280
        Top = 67
        TabOrder = 15
        Width = 121
      end
      object cxLabeljumlahseat: TcxLabel
        Left = 198
        Top = 97
        Caption = 'Jumlah Seat :'
      end
      object ComboBox1: TComboBox
        Left = 280
        Top = 96
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 17
      end
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 452
    Width = 1104
    Height = 225
    Align = alBottom
    TabOrder = 1
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 121
      Width = 1102
      Height = 103
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsData.CancelOnExit = False
      OptionsData.Editing = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DetailJadwalDs
      Version = 1
      object MasterVGridRute: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Rute'
        Visible = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridNoSJ: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoSJ'
        Visible = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Pelanggan'
        Visible = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridNoSO: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoSO'
        Visible = False
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridSopir: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Sopir'
        Visible = False
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridLama: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Lama'
        Visible = False
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridTgl: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tgl'
        Visible = False
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridMekanik: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Mekanik'
        Visible = False
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Keterangan'
        Visible = False
        ID = 8
        ParentID = -1
        Index = 8
        Version = 1
      end
      object MasterVGridMasalah: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Masalah'
        Visible = False
        ID = 9
        ParentID = -1
        Index = 9
        Version = 1
      end
      object MasterVGridTindakan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tindakan'
        Visible = False
        ID = 10
        ParentID = -1
        Index = 10
        Version = 1
      end
      object MasterVGridPlatNo: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNo'
        Visible = False
        ID = 11
        ParentID = -1
        Index = 11
        Version = 1
      end
      object MasterVGridBongkar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Bongkar'
        Visible = False
        ID = 12
        ParentID = -1
        Index = 12
        Version = 1
      end
      object MasterVGridMuat: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Muat'
        Visible = False
        ID = 13
        ParentID = -1
        Index = 13
        Version = 1
      end
      object MasterVGridJenisPerawatan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JenisPerawatan'
        Visible = False
        ID = 14
        ParentID = -1
        Index = 14
        Version = 1
      end
      object MasterVGridJenisPerbaikan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JenisPerbaikan'
        Visible = False
        ID = 15
        ParentID = -1
        Index = 15
        Version = 1
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1102
      Height = 120
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 88
        Height = 24
        Caption = 'Legends:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold, fsUnderline]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 88
        Top = 43
        Width = 81
        Height = 20
        Caption = 'Perbaikan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 88
        Top = 76
        Width = 86
        Height = 20
        Caption = 'Perawatan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 443
        Top = 41
        Width = 122
        Height = 20
        Caption = 'Ready no Crew'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 443
        Top = 74
        Width = 52
        Height = 20
        Caption = 'Ready'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 894
        Top = 43
        Width = 44
        Height = 20
        Caption = 'Jalan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lperbaikan: TLabel
        Left = 176
        Top = 43
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lperawatan: TLabel
        Left = 181
        Top = 74
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lreadynocrew: TLabel
        Left = 572
        Top = 41
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lready: TLabel
        Left = 499
        Top = 74
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ljalan: TLabel
        Left = 947
        Top = 42
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 894
        Top = 74
        Width = 23
        Height = 20
        Caption = 'So'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LSO: TLabel
        Left = 921
        Top = 74
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 446
        Top = 11
        Width = 90
        Height = 20
        Caption = 'Plot Trayek'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ltrayek: TLabel
        Left = 555
        Top = 11
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Shape2: TShape
        Left = 16
        Top = 40
        Width = 57
        Height = 25
        Brush.Color = clRed
      end
      object Shape1: TShape
        Left = 16
        Top = 72
        Width = 57
        Height = 25
        Brush.Color = clYellow
      end
      object Shape3: TShape
        Left = 371
        Top = 8
        Width = 58
        Height = 25
        Brush.Color = clMedGray
      end
      object Shape4: TShape
        Left = 371
        Top = 40
        Width = 58
        Height = 25
        Brush.Color = 2396927
      end
      object Shape5: TShape
        Left = 371
        Top = 72
        Width = 58
        Height = 25
        Brush.Color = clLime
      end
      object Shape7: TShape
        Left = 822
        Top = 40
        Width = 57
        Height = 25
        Brush.Color = clBlue
      end
      object Shape8: TShape
        Left = 822
        Top = 72
        Width = 57
        Height = 25
        Brush.Color = clAqua
      end
    end
  end
  object DetailJadwalQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select '#39#39' as Rute, '#39#39' as NoSJ, '#39#39' as Pelanggan, '#39#39' as NoSO , '
      
        #39#39' as Sopir, '#39#39' as Lama , '#39#39' as Tgl, '#39#39' as Mekanik, '#39#39' as Ketera' +
        'ngan,'
      
        #39#39' as Masalah, '#39#39' as Tindakan ,'#39#39' as PlatNo, '#39#39' as Bongkar , '#39#39' ' +
        'as Muat'
      ','#39#39' as JenisPerawatan,'#39#39' as JenisPerbaikan')
    UpdateObject = SDUpdateSQL1
    Left = 689
    Top = 94
    object DetailJadwalQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 255
    end
    object DetailJadwalQNoSJ: TStringField
      FieldName = 'NoSJ'
      Required = True
      Size = 255
    end
    object DetailJadwalQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 255
    end
    object DetailJadwalQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 255
    end
    object DetailJadwalQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 255
    end
    object DetailJadwalQLama: TStringField
      FieldName = 'Lama'
      Required = True
      Size = 255
    end
    object DetailJadwalQTgl: TStringField
      FieldName = 'Tgl'
      Required = True
      Size = 255
    end
    object DetailJadwalQMekanik: TStringField
      FieldName = 'Mekanik'
      Required = True
      Size = 255
    end
    object DetailJadwalQKeterangan: TStringField
      FieldName = 'Keterangan'
      Required = True
      Size = 255
    end
    object DetailJadwalQMasalah: TStringField
      FieldName = 'Masalah'
      Required = True
      Size = 255
    end
    object DetailJadwalQTindakan: TStringField
      FieldName = 'Tindakan'
      Required = True
      Size = 255
    end
    object DetailJadwalQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 255
    end
    object DetailJadwalQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 255
    end
    object DetailJadwalQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 255
    end
    object DetailJadwalQJenisPerawatan: TStringField
      DisplayWidth = 255
      FieldName = 'JenisPerawatan'
      Required = True
      Size = 255
    end
    object DetailJadwalQJenisPerbaikan: TStringField
      DisplayWidth = 255
      FieldName = 'JenisPerbaikan'
      Required = True
      Size = 255
    end
  end
  object DetailJadwalDs: TDataSource
    DataSet = DetailJadwalQ
    Left = 763
    Top = 50
  end
  object armadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select kode,PlatNo,NoBody,sopir from armada where '
      'PlatNo = :t1 '
      'or NoBody = :t1 '
      'or TahunPembuatan = :t2'
      'or JumlahSeat =:t3')
    Left = 712
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't2'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 't3'
        ParamType = ptInput
        Value = 0
      end>
    object armadaQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object armadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object armadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object armadaQsopir: TStringField
      FieldName = 'sopir'
      Size = 10
    end
  end
  object statusQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 800
    Top = 16
  end
  object perbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 816
    Top = 296
  end
  object perawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 816
    Top = 256
  end
  object showDetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,sj.Tgl) as tgl,p.NamaPT as pelanggan,s.Nama ' +
        'as sopir,r.Muat as muat,r.Bongkar as bongkar,r.Waktu as lama fro' +
        'm MasterSJ sj,MasterSO so ,Rute r,Sopir s, Pelanggan p where sj.' +
        'sopir=s.Kode and so.Pelanggan=p.Kode and r.Kode=so.Rute and sj.N' +
        'oSO=so.Kodenota and so.Armada=:text and convert(date,sj.Tgl)<=co' +
        'nvert(date,CURRENT_TIMESTAMP+ :col) and convert(date,(sj.Tgl+r.w' +
        'aktu))>=convert(date,CURRENT_TIMESTAMP+ :col)')
    Left = 888
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object SDUpdateSQL1: TSDUpdateSQL
    Left = 824
    Top = 16
  end
  object detailPerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.kode as kode,j.nama as tipe ,l.AnalisaMasalah as masala' +
        'h, l.TindakanPerbaikan as tindakan, l.Keterangan as keterangan, ' +
        'j.LamaPengerjaan as lama, l.waktumulai as tgl from PermintaanPer' +
        'baikan p, LaporanPerbaikan l,jenisperbaikan j,DetailPerbaikanJen' +
        'is d where l.Kode=d.KodePerbaikan  and j.kode=d.JenisPerbaikan a' +
        'nd p.Kode=l.PP and p.Armada=:text and convert(date,l.WaktuMulai)' +
        '<=convert(date,CURRENT_TIMESTAMP+:col) and isnull(l.WaktuSelesai' +
        ',convert(date,(l.WaktuMulai+(select MAX(JenisPerbaikan.lamapenge' +
        'rjaan) from JenisPerbaikan where Kode in (d.JenisPerbaikan) ))))' +
        '>=convert(date,CURRENT_TIMESTAMP+:col)')
    Left = 888
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.kode as kode,s.tipeperawatan as tipe ,l.WaktuMulai as t' +
        'gl, s.StandardWaktu as lama, l.Keterangan as keterangan from Lap' +
        'oranPerawatan l, StandarPerawatan s, DetailPerawatanJenis d  whe' +
        're d.KodePerawatan=l.Kode and d.JenisPerawatan=s.Kode and l.Arma' +
        'da=:text and convert(date,l.WaktuMulai)<=convert(date,CURRENT_TI' +
        'MESTAMP+:col) and isnull(l.WaktuSelesai,convert(date,(l.WaktuMul' +
        'ai+(select MAX(StandarPerawatan.StandardWaktu) from StandarPeraw' +
        'atan where Kode in (d.JenisPerawatan) ))))>=convert(date,CURRENT' +
        '_TIMESTAMP+:col)')
    Left = 888
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object showDetail2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,sj.Tgl) as tgl,p.NamaPT as pelanggan,s.Nama ' +
        'as sopir,r.Muat as muat,r.Bongkar as bongkar,r.Waktu as lama fro' +
        'm MasterSJ sj,MasterSO so ,Rute r,Sopir s, Pelanggan p where sj.' +
        'sopir=s.Kode and so.Pelanggan=p.Kode and r.Kode=so.Rute and sj.N' +
        'oSO=so.Kodenota and so.Armada=:text and convert(date,sj.Tgl)<=co' +
        'nvert(date,:col ) and convert(date,(sj.Tgl+r.waktu))>=convert(da' +
        'te, :col )')
    Left = 936
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerawatanQ2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.kode as kode,s.tipeperawatan as tipe,l.WaktuMulai as tg' +
        'l, s.StandardWaktu as lama, l.Keterangan as keterangan from Lapo' +
        'ranPerawatan l, StandarPerawatan s, DetailPerawatanJenis d where' +
        ' d.KodePerawatan=l.Kode and d.JenisPerawatan=s.Kode and l.Armada' +
        '=:text and convert(date,l.WaktuMulai)<=convert(date,:col) and is' +
        'null(l.WaktuSelesai,convert(date,(l.WaktuMulai+(select MAX(Stand' +
        'arPerawatan.StandardWaktu) from StandarPerawatan where Kode in (' +
        'd.JenisPerawatan) ))))>=convert(date,:col)')
    Left = 936
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerbaikanQ2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.kode as kode,j.nama as tipe, l.AnalisaMasalah as masala' +
        'h, l.TindakanPerbaikan as tindakan, l.Keterangan as keterangan, ' +
        'j.LamaPengerjaan as lama, l.waktumulai as tgl from PermintaanPer' +
        'baikan p, LaporanPerbaikan l,jenisperbaikan j,DetailPerbaikanJen' +
        'is d where l.Kode=d.KodePerbaikan  and j.kode=d.JenisPerbaikan a' +
        'nd p.Kode=l.PP  and p.Armada=:text and convert(date,l.WaktuMulai' +
        ')<=convert(date,:col) and isnull(l.WaktuSelesai,convert(date,(l.' +
        'WaktuMulai+(select MAX(JenisPerbaikan.lamapengerjaan) from Jenis' +
        'Perbaikan where Kode in (d.JenisPerbaikan) ))))>=convert(date,:c' +
        'ol)')
    Left = 936
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object isiDetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 640
    Top = 96
  end
  object ceksoQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 as ok from masterso m where armada= :a1 and convert(dat' +
        'e,m.berangkat)<=convert(date,CURRENT_TIMESTAMP+ :t1) and convert' +
        '(date,m.tiba)>=convert(date,CURRENT_TIMESTAMP+ :t2)')
    Left = 784
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'a1'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftString
        Name = 't2'
        ParamType = ptInput
        Value = 0
      end>
  end
  object cekso2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 as ok from masterso m where armada= :a1 and convert(dat' +
        'e,m.berangkat)<=convert(datetime,:s1) + :t1 and convert(date,m.t' +
        'iba)>=convert(datetime,:s2 )+ :t2')
    Left = 824
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'a1'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftUnknown
        Name = 's1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftUnknown
        Name = 's2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't2'
        ParamType = ptInput
        Value = 0
      end>
  end
  object detailSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,so.Tgl) as tgl,p.NamaPT as pelanggan,r.Muat ' +
        'as muat,r.Bongkar as bongkar,r.Waktu as lama from MasterSO so ,R' +
        'ute r, Pelanggan p where so.Pelanggan=p.Kode and r.Kode=so.Rute ' +
        'and so.Armada=:text and convert(date,so.berangkat)<=convert(date' +
        ',CURRENT_TIMESTAMP+ :col) and convert(date,(so.tiba))>=convert(d' +
        'ate,CURRENT_TIMESTAMP+ :col)')
    Left = 888
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailso2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,so.Tgl) as tgl,p.NamaPT as pelanggan,r.Muat ' +
        'as muat,r.Bongkar as bongkar,r.Waktu as lama from MasterSO so ,R' +
        'ute r, Pelanggan p where so.Pelanggan=p.Kode and r.Kode=so.Rute ' +
        'and so.Armada=:text and convert(date,so.berangkat)<=convert(date' +
        ',:col ) and convert(date,(so.tiba))>=convert(date, :col )')
    Left = 936
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object cekTrayekQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'SELECT 0 AS OK FROM KONTRAK K, ARMADAKONTRAK A WHERE A.KONTRAK=K' +
        '.KODE AND K.STATUS='#39'ONGOING'#39' AND A.PLATNO=:TEXT AND CONVERT(DATE' +
        ',K.TGLMULAI)<=CONVERT(DATE,CURRENT_TIMESTAMP+ :col) AND CONVERT(' +
        'DATE, A.TGLEXPIRED)>=CONVERT(DATE,CURRENT_TIMESTAMP+ :col)')
    Left = 784
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftInteger
        Name = 'col'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object cekTrayek2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'SELECT 0 AS OK FROM KONTRAK K, ARMADAKONTRAK A WHERE A.KONTRAK=K' +
        '.KODE AND K.STATUS='#39'ONGOING'#39' AND A.PLATNO=:TEXT AND CONVERT(DATE' +
        ',K.TGLMULAI)<=CONVERT(DATETIME,:S1)+ :T1 AND CONVERT(DATE, A.TGL' +
        'EXPIRED)>=CONVERT(DATETIME,:S2)+ :T2;')
    Left = 824
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'S1'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'T1'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'S2'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'T2'
        ParamType = ptInput
        Value = 0
      end>
  end
  object cekAnjemQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 AS OK from realisasianjem ra where ra.armada=:TEXT and ' +
        'CONVERT(DATE,ra.TanggalMulai)<=CONVERT(DATE,CURRENT_TIMESTAMP+ :' +
        'COL) AND CONVERT(DATE, ra.TanggalSelesai)>=CONVERT(DATE,CURRENT_' +
        'TIMESTAMP+ :COL);')
    Left = 784
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
      end>
  end
  object cekAnjem2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 AS OK from realisasianjem ra where ra.armada=:TEXT and ' +
        'CONVERT(DATE,ra.TanggalMulai)<=CONVERT(DATETIME,:S1)+ :T1 AND CO' +
        'NVERT(DATE, ra.TanggalSelesai)>=CONVERT(DATETIME,:S2)+ :T2;')
    Left = 824
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'S1'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'T1'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'S2'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'T2'
        ParamType = ptInput
        Value = 0
      end>
  end
  object crTrayekQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 as ok from realisasitrayek rt where rt.armada=:text and' +
        ' CONVERT(DATE,rt.tanggal)=CONVERT(DATE,CURRENT_TIMESTAMP+ :COL);')
    Left = 784
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
        Value = 0
      end>
  end
  object crTrayek2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select 0 as ok from realisasitrayek rt where rt.armada=:text and' +
        ' CONVERT(DATE,rt.tanggal)=CONVERT(DATETIME,:S1)+ :T1;')
    Left = 824
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'S1'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'T1'
        ParamType = ptInput
        Value = 0
      end>
  end
  object dplotQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'SELECT k.rute as rute, p.namapt as pelanggan,k.tglselesai as tgl' +
        ' FROM KONTRAK K, ARMADAKONTRAK A,pelanggan p WHERE p.kode=k.pela' +
        'nggan and A.KONTRAK=K.KODE AND K.STATUS='#39'ONGOING'#39' AND A.PLATNO=:' +
        'TEXT AND CONVERT(DATE,K.TGLMULAI)<=CONVERT(DATE,CURRENT_TIMESTAM' +
        'P+ :col) AND CONVERT(DATE, A.TGLEXPIRED)>=CONVERT(DATE,CURRENT_T' +
        'IMESTAMP+ :col)')
    Left = 888
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'col'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'col'
        ParamType = ptInput
      end>
    object dplotQrute: TStringField
      FieldName = 'rute'
      Size = 10
    end
    object dplotQpelanggan: TStringField
      FieldName = 'pelanggan'
      Required = True
      Size = 50
    end
    object dplotQtgl: TDateTimeField
      FieldName = 'tgl'
    end
  end
  object dplot2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'SELECT k.rute as rute, p.namapt as pelanggan,k.tglselesai as tgl' +
        ' FROM KONTRAK K, ARMADAKONTRAK A,pelanggan p WHERE p.kode=k.pela' +
        'nggan and  A.KONTRAK=K.KODE AND K.STATUS='#39'ONGOING'#39' AND A.PLATNO=' +
        ':TEXT AND CONVERT(DATE,K.TGLMULAI)<=CONVERT(DATETIME,:col) AND C' +
        'ONVERT(DATE, A.TGLEXPIRED)>=CONVERT(DATETIME,:col);')
    Left = 928
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'col'
        ParamType = ptInput
        Value = '0'
      end
      item
        DataType = ftSmallint
        Name = 'col'
        ParamType = ptInput
      end>
    object dplot2Qrute: TStringField
      FieldName = 'rute'
      Size = 10
    end
    object dplot2Qpelanggan: TStringField
      FieldName = 'pelanggan'
      Required = True
      Size = 50
    end
    object dplot2Qtgl: TDateTimeField
      FieldName = 'tgl'
    end
  end
  object dAnjemQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select ra.tanggalselesai as tgl, p.nama AS sopir from realisasia' +
        'njem ra,pegawai p where p.kode=ra.pengemudi and ra.armada=:TEXT ' +
        'and CONVERT(DATE,ra.TanggalMulai)<=CONVERT(DATE,CURRENT_TIMESTAM' +
        'P+ :COL) AND CONVERT(DATE, ra.TanggalSelesai)>=CONVERT(DATE,CURR' +
        'ENT_TIMESTAMP+ :COL);')
    Left = 888
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
      end>
    object dAnjemQtgl: TDateTimeField
      FieldName = 'tgl'
    end
    object dAnjemQsopir: TStringField
      FieldName = 'sopir'
      Size = 50
    end
  end
  object dAnjem2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select ra.tanggalselesai as tgl, p.nama AS sopir from realisasia' +
        'njem ra,pegawai p where p.kode=ra.pengemudi and ra.armada=:TEXT ' +
        'and CONVERT(DATE,ra.TanggalMulai)<=CONVERT(DATETIME,:col) AND CO' +
        'NVERT(DATE, ra.TanggalSelesai)>=CONVERT(DATETIME,:col);')
    Left = 928
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'TEXT'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'col'
        ParamType = ptInput
      end>
    object dAnjem2Qtgl: TDateTimeField
      FieldName = 'tgl'
    end
    object dAnjem2Qsopir: TStringField
      FieldName = 'sopir'
      Size = 50
    end
  end
  object dRTrayekQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select rt.tanggal as tgl, p.nama AS sopir from realisasitrayek r' +
        't,pegawai p where rt.pengemudi=p.kode and rt.armada=:text and CO' +
        'NVERT(DATE,rt.tanggal)=CONVERT(DATE,CURRENT_TIMESTAMP+ :COL);')
    Left = 888
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftSmallint
        Name = 'COL'
        ParamType = ptInput
        Value = 0
      end>
    object dRTrayekQtgl: TDateTimeField
      FieldName = 'tgl'
    end
    object dRTrayekQsopir: TStringField
      FieldName = 'sopir'
      Size = 50
    end
  end
  object dRTrayek2Q: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select rt.tanggal as tgl, p.nama AS sopir from realisasitrayek r' +
        't,pegawai p where rt.pengemudi=p.kode and rt.armada=:text and CO' +
        'NVERT(DATE,rt.tanggal)=CONVERT(DATETIME,:col);')
    Left = 928
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'col'
        ParamType = ptInput
        Value = ''
      end>
    object dRTrayek2Qtgl: TDateTimeField
      FieldName = 'tgl'
    end
    object dRTrayek2Qsopir: TStringField
      FieldName = 'sopir'
      Size = 50
    end
  end
  object tempQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select platno from armada where kode=:text')
    Left = 656
    Top = 56
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end>
    object tempQplatno: TStringField
      FieldName = 'platno'
      Required = True
      Size = 10
    end
  end
  object JumlahSeatQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select distinct JumlahSeat from Armada where jumlahseat<>null')
    Left = 408
    Top = 64
    object JumlahSeatQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
  end
end
