unit JadwalArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, Grids,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxDropDownEdit, cxCalendar, cxLabel, cxSpinEdit,
  cxCheckBox;

type
  TJadwalArmadaFm = class(TForm)
    pnl1: TPanel;
    StringGrid1: TStringGrid;
    pnl2: TPanel;
    pnl3: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    DetailJadwalQ: TSDQuery;
    DetailJadwalQRute: TStringField;
    DetailJadwalQNoSJ: TStringField;
    DetailJadwalQPelanggan: TStringField;
    DetailJadwalQNoSO: TStringField;
    DetailJadwalDs: TDataSource;
    armadaQ: TSDQuery;
    statusQ: TSDQuery;
    perbaikanQ: TSDQuery;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    perawatanQ: TSDQuery;
    DetailJadwalQSopir: TStringField;
    DetailJadwalQLama: TStringField;
    DetailJadwalQTgl: TStringField;
    DetailJadwalQMekanik: TStringField;
    DetailJadwalQKeterangan: TStringField;
    DetailJadwalQMasalah: TStringField;
    DetailJadwalQTindakan: TStringField;
    DetailJadwalQPlatNo: TStringField;
    showDetailQ: TSDQuery;
    DetailJadwalQBongkar: TStringField;
    DetailJadwalQMuat: TStringField;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridNoSJ: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridNoSO: TcxDBEditorRow;
    MasterVGridSopir: TcxDBEditorRow;
    MasterVGridLama: TcxDBEditorRow;
    MasterVGridTgl: TcxDBEditorRow;
    MasterVGridMekanik: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridMasalah: TcxDBEditorRow;
    MasterVGridTindakan: TcxDBEditorRow;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridBongkar: TcxDBEditorRow;
    MasterVGridMuat: TcxDBEditorRow;
    SDUpdateSQL1: TSDUpdateSQL;
    detailPerbaikanQ: TSDQuery;
    detailPerawatanQ: TSDQuery;
    cxComboBox1: TcxComboBox;
    DateTimePicker1: TDateTimePicker;
    BBulan: TButton;
    showDetail2Q: TSDQuery;
    detailPerawatanQ2: TSDQuery;
    detailPerbaikanQ2: TSDQuery;
    DateTimePicker2: TDateTimePicker;
    DateTimePicker3: TDateTimePicker;
    BRange: TButton;
    lperbaikan: TLabel;
    lperawatan: TLabel;
    lreadynocrew: TLabel;
    lready: TLabel;
    ljalan: TLabel;
    cxLabel1: TcxLabel;
    isiDetailQ: TSDQuery;
    DetailJadwalQJenisPerawatan: TStringField;
    DetailJadwalQJenisPerbaikan: TStringField;
    MasterVGridJenisPerawatan: TcxDBEditorRow;
    MasterVGridJenisPerbaikan: TcxDBEditorRow;
    ceksoQ: TSDQuery;
    cekso2Q: TSDQuery;
    detailSOQ: TSDQuery;
    detailso2Q: TSDQuery;
    Label7: TLabel;
    LSO: TLabel;
    Label8: TLabel;
    ltrayek: TLabel;
    cekTrayekQ: TSDQuery;
    cekTrayek2Q: TSDQuery;
    cekAnjemQ: TSDQuery;
    cekAnjem2Q: TSDQuery;
    crTrayekQ: TSDQuery;
    crTrayek2Q: TSDQuery;
    dplotQ: TSDQuery;
    dplot2Q: TSDQuery;
    dAnjemQ: TSDQuery;
    dAnjem2Q: TSDQuery;
    dRTrayekQ: TSDQuery;
    dRTrayek2Q: TSDQuery;
    dplotQrute: TStringField;
    dplotQpelanggan: TStringField;
    dplotQtgl: TDateTimeField;
    dAnjemQtgl: TDateTimeField;
    dAnjemQsopir: TStringField;
    dRTrayekQtgl: TDateTimeField;
    dRTrayekQsopir: TStringField;
    tempQ: TSDQuery;
    tempQplatno: TStringField;
    dplot2Qrute: TStringField;
    dplot2Qpelanggan: TStringField;
    dplot2Qtgl: TDateTimeField;
    dAnjem2Qtgl: TDateTimeField;
    dAnjem2Qsopir: TStringField;
    dRTrayek2Qtgl: TDateTimeField;
    dRTrayek2Qsopir: TStringField;
    BNormal: TButton;
    cxCheckBoxToilet: TcxCheckBox;
    cxCheckBoxAirSuspension: TcxCheckBox;
    cxCheckBoxAktif: TcxCheckBox;
    cxCheckBoxAC: TcxCheckBox;
    cxLabelPlat: TcxLabel;
    cxLabeltahunpembuatan: TcxLabel;
    cxTextEditPlatno: TcxTextEdit;
    cxTextEditTahun: TcxTextEdit;
    armadaQkode: TStringField;
    armadaQPlatNo: TStringField;
    armadaQNoBody: TStringField;
    armadaQsopir: TStringField;
    cxLabeljumlahseat: TcxLabel;
    Shape2: TShape;
    Shape1: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape7: TShape;
    Shape8: TShape;
    ComboBox1: TComboBox;
    JumlahSeatQ: TSDQuery;
    JumlahSeatQJumlahSeat: TIntegerField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure BRangeClick(Sender: TObject);
    procedure BBulanClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BNormalClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  JadwalArmadaFm: TJadwalArmadaFm;
  kondisi :array of array of String;
  kodearmada : array of String;
  cektglq : array[1..50] of String;
  gambar: Boolean;
  MasterOriSQL: string;
  DaysInYear: array[1..12] of Integer =(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  
implementation

uses MenuUtama, DropDown, DM, Math, DateUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TJadwalArmadaFm.Create(aOwner: TComponent);

begin

    inherited Create(aOwner);


end;

procedure TJadwalArmadaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TJadwalArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TJadwalArmadaFm.StringGrid1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  pbaris  :integer;
  i,j     :integer;
begin
  //Dapatkan Banyaknya Baris
	armadaQ.Open;
	pbaris:=armadaQ.RecordCount;
	armadaQ.Close;
	if cxComboBox1.ItemIndex=0 then
	begin
		if (ACol = 3) and (ARow = 0) then
		begin
			with TStringGrid(Sender) do
			begin
			  Canvas.Brush.Color := clFuchsia;
			  //Canvas.Font.Color:= clRed;
			  Canvas.FillRect(Rect);
			  Canvas.TextOut(Rect.Left+2,Rect.Top+2,FormatDateTime('dd-mmm-yyyy', now));
			end;
		end;
	end;

  for i:=StringGrid1.ColCount-7 to StringGrid1.ColCount do
  begin
		if (ACol = i) and (ARow = 0) then
		begin
			with TStringGrid(Sender) do
			begin
        if i=StringGrid1.ColCount-7 then
        begin
          Canvas.Brush.Color := clRed;
        end
        else if i=StringGrid1.ColCount-6 then
        begin
          Canvas.Brush.Color := clYellow;
        end
        else if i=StringGrid1.ColCount-5 then
        begin
          Canvas.Brush.Color := RGB(255, 146, 36);
        end
        else if i=StringGrid1.ColCount-4 then
        begin
          Canvas.Brush.Color := clLime;
        end
        else if i=StringGrid1.ColCount-3 then
        begin
          Canvas.Brush.Color := clBlue;
        end
        else if i=StringGrid1.ColCount-2 then
        begin
          Canvas.Brush.Color := clAqua;
        end
        else if i=StringGrid1.ColCount-1 then
        begin
          Canvas.Brush.Color := clMedGray;
        end;
			  Canvas.FillRect(Rect);
			  //Canvas.TextOut(Rect.Left+2,Rect.Top+2,'');
			end;
		end;
  end;


	for i:=0 to pbaris do
	begin
		for j:=1 to StringGrid1.ColCount do
		begin
			if (ACol = j) and (ARow = i) then
				if (kondisi[i,j]='jalan') or (kondisi[i,j]='trayek') or (kondisi[i,j]='ancem') then
				begin
				  with TStringGrid(Sender) do
				  begin
					Canvas.Brush.Color := clBlue;
					Canvas.FillRect(Rect);
					Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
				  end;
				end
			else if kondisi[i,j]='ready' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := clLime;
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end
			else if kondisi[i,j]='readynocrew' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := RGB(255, 146, 36);
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end
			else if kondisi[i,j]='perawatan' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := clYellow;
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end
      else if kondisi[i,j]='so' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := clAqua;
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end
			else if kondisi[i,j]='perbaikan' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := clRed;
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end
      else if kondisi[i,j]='plot' then
			begin
			  with TStringGrid(Sender) do
			  begin
				Canvas.Brush.Color := clMedGray;
				Canvas.FillRect(Rect);
				Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
			  end;
			end;
		end;
	end;
end;

procedure TJadwalArmadaFm.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  myquery :String;
  mydate: TDateTime;
  temp,temp2  :String;
begin
  //Sembunyikan semua field;
  MasterVGridNoSJ.Visible:=false;
  MasterVGridNoSJ.Visible:=false;
  MasterVGridPelanggan.Visible:=false;
  MasterVGridNoSO.Visible:=false;
  MasterVGridSopir.Visible:=false;
  MasterVGridLama.Visible:=false;
  MasterVGridTgl.Visible:=false;
  MasterVGridMekanik.Visible:=false;
  MasterVGridKeterangan.Visible:=false;
  MasterVGridMasalah.Visible:=false;
  MasterVGridTindakan.Visible:=false;
  MasterVGridPlatNo.Visible:=false;
  MasterVGridBongkar.Visible:=false;
  MasterVGridMuat.Visible:=false;
  MasterVGridJenisPerawatan.Visible:=False;
  MasterVGridJenisPerbaikan.Visible:=False;

  if kondisi[ARow][ACol]='jalan' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      showDetailQ.ParamByName('text').AsString:=kodearmada[arow];
      showDetailQ.ParamByName('col').AsInteger:= (acol-3);
      showDetailQ.ExecSQL;
      showDetailQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=showDetailQ.FieldByName('pelanggan').AsString ;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.asstring :=showDetailQ.FieldByName('sopir').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= showDetailQ.fieldbyname('tgl').AsString;
        MasterVGridMuat.Visible:=true;
        DetailJadwalQMuat.AsString:=showDetailQ.fieldByName('muat').AsString;
        MasterVGridBongkar.Visible:=true;
        DetailJadwalQBongkar.AsString:=showDetailQ.fieldByName('bongkar').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=showDetailQ.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      showDetailQ.Close;
    end
    else
    begin
      showDetail2Q.ParamByName('text').AsString:=kodearmada[arow];
      showDetail2Q.ParamByName('col').AsString:=  cektglq[ACol];
      showDetail2Q.ExecSQL;
      showDetail2Q.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=showDetail2Q.FieldByName('pelanggan').AsString ;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.asstring :=showDetail2Q.FieldByName('sopir').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= showDetail2Q.fieldbyname('tgl').AsString;
        MasterVGridMuat.Visible:=true;
        DetailJadwalQMuat.AsString:=showDetail2Q.fieldByName('muat').AsString;
        MasterVGridBongkar.Visible:=true;
        DetailJadwalQBongkar.AsString:=showDetail2Q.fieldByName('bongkar').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=showDetail2Q.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      showDetail2Q.Close;
    end;
  end
  else if kondisi[ARow][ACol]='ready' then
  begin
  end
  else if kondisi[ARow][ACol]='readynocrew' then
  begin
  end
  else if kondisi[ARow][ACol]='perawatan' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      detailPerawatanQ.ParamByName('text').AsString:=kodearmada[arow];
      detailPerawatanQ.ParamByName('col').AsInteger:= (acol-3);
      //detailPerawatanQ.ExecSQL;
      detailPerawatanQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;


        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.asstring :=detailPerawatanQ.FieldByName('tgl').AsString ;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:= detailPerawatanQ.fieldbyname('lama').AsString;
        MasterVGridKeterangan.Visible:=true;
        DetailJadwalQKeterangan.AsString:=detailPerawatanQ.fieldByName('keterangan').AsString;
        //Isi Untuk mekanik
        MasterVGridMekanik.Visible:=true;
        isiDetailQ.Close;
        isiDetailQ.SQL.Clear;
        isiDetailQ.SQL.Add('select m.nama as nama from DetailPerawatanMekanik dpm, Mekanik m where m.Kode=dpm.Mekanik and dpm.KodePerawatan='+QuotedStr(detailPerawatanQ.fieldByName('kode').AsString));
        isiDetailQ.Open;
        isiDetailQ.First;
        DetailJadwalQMekanik.AsString:='';
        while not isiDetailQ.Eof do
        begin
          DetailJadwalQMekanik.AsString:=DetailJadwalQMekanik.AsString+isiDetailQ.fieldByName('nama').AsString+',';
          isiDetailQ.Next;
        end;
        // Isi Untuk Jenis Perawatan
        detailPerawatanQ.First;
        DetailJadwalQJenisPerawatan.AsString:='';
        MasterVGridJenisPerawatan.Visible:=true;
        while not detailPerawatanQ.Eof do
        begin
          DetailJadwalQJenisPerawatan.AsString:=DetailJadwalQJenisPerawatan.AsString  + detailPerawatanQ.FieldByName('tipe').AsString+',';
          detailPerawatanQ.Next;
        end;

      detailPerawatanQ.Close;
    end
    else
    begin
      detailPerawatanQ2.ParamByName('text').AsString:=kodearmada[arow];
      detailPerawatanQ2.ParamByName('col').AsString:= cektglq[ACol];
      detailPerawatanQ2.ExecSQL;
      detailPerawatanQ2.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridJenisPerawatan.Visible:=true;
        //MasterVGridMekanik.Visible:=true;
        //DetailJadwalQMekanik.asstring :=detailPerawatanQ2.FieldByName('mekanik').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.asstring :=detailPerawatanQ2.FieldByName('tgl').AsString ;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:= detailPerawatanQ2.fieldbyname('lama').AsString;
        MasterVGridKeterangan.Visible:=true;
        DetailJadwalQKeterangan.AsString:=detailPerawatanQ2.fieldByName('keterangan').AsString;
        //Isi Untuk mekanik
        MasterVGridMekanik.Visible:=true;
        isiDetailQ.Close;
        isiDetailQ.SQL.Clear;
        isiDetailQ.SQL.Add('select m.nama as nama from DetailPerawatanMekanik dpm, Mekanik m where m.Kode=dpm.Mekanik and dpm.KodePerawatan='+QuotedStr(detailPerawatanQ2.fieldByName('kode').AsString));
        isiDetailQ.Open;
        isiDetailQ.First;
        DetailJadwalQMekanik.AsString:='';
        while not isiDetailQ.Eof do
        begin
          DetailJadwalQMekanik.AsString:=DetailJadwalQMekanik.AsString+isiDetailQ.fieldByName('nama').AsString+',';
          isiDetailQ.Next;
        end;
        // Isi Untuk Jenis Perawatan
        detailPerawatanQ2.First;
        DetailJadwalQJenisPerawatan.AsString:='';
        MasterVGridJenisPerawatan.Visible:=true;
        while not detailPerawatanQ.Eof do
        begin
          DetailJadwalQJenisPerawatan.AsString:=DetailJadwalQJenisPerawatan.AsString  + detailPerawatanQ2.FieldByName('tipe').AsString+',';
          detailPerawatanQ2.Next;
        end;
      detailPerawatanQ2.Close;
    end;
  end
  else if kondisi[arow][acol]='so' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      detailSOQ.ParamByName('text').AsString:=kodearmada[arow];
      detailSOQ.ParamByName('col').AsInteger:= (acol-3);
      //detailPerawatanQ.ExecSQL;
      detailSOQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=detailSOQ.FieldByName('pelanggan').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= detailSOQ.fieldbyname('tgl').AsString;
        MasterVGridMuat.Visible:=true;
        DetailJadwalQMuat.AsString:=detailSOQ.fieldByName('muat').AsString;
        MasterVGridBongkar.Visible:=true;
        DetailJadwalQBongkar.AsString:=detailSOQ.fieldByName('bongkar').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=detailSOQ.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      detailSOQ.Close;
    end
    else
    begin
      detailSO2Q.ParamByName('text').AsString:=kodearmada[arow];
      detailSO2Q.ParamByName('col').AsString:= cektglq[ACol];
      detailSO2Q.ExecSQL;
      detailSO2Q.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=detailSO2Q.FieldByName('pelanggan').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= detailSO2Q.fieldbyname('tgl').AsString;
        MasterVGridMuat.Visible:=true;
        DetailJadwalQMuat.AsString:=detailSO2Q.fieldByName('muat').AsString;
        MasterVGridBongkar.Visible:=true;
        DetailJadwalQBongkar.AsString:=detailSO2Q.fieldByName('bongkar').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=detailSO2Q.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      detailSO2Q.Close;
    end;
  end
  else if kondisi[arow][acol]='plot' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      tempQ.Close;
      tempQ.ParamByName('text').AsString:= kodearmada[arow];
      tempQ.Open;
      dplotQ.ParamByName('text').AsString:=tempQplatno.AsString;
      dplotQ.ParamByName('col').AsInteger:= (acol-3);
      dplotQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=dplotQ.FieldByName('pelanggan').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dplotQ.fieldbyname('tgl').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dplotQ.Close;
    end
    else
    begin
      tempQ.Close;
      tempQ.ParamByName('text').AsString:= kodearmada[arow];
      tempQ.Open;
      dplot2Q.ParamByName('text').AsString:=tempQplatno.AsString;
      dplot2Q.ParamByName('col').AsString:= cektglq[ACol];
      dplot2Q.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridPelanggan.Visible:=true;
        DetailJadwalQPelanggan.asstring :=dplot2Q.FieldByName('pelanggan').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dplot2Q.fieldbyname('tgl').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dplot2Q.Close;
    end;
  end
  else if kondisi[arow][acol]='trayek' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      dRTrayekQ.ParamByName('text').AsString:=kodearmada[arow];
      dRTrayekQ.ParamByName('col').AsInteger:= (acol-3);
      //detailPerawatanQ.ExecSQL;
      dRTrayekQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dRTrayekQ.fieldbyname('tgl').AsString;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.AsString:= dRTrayekQ.fieldbyname('sopir').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dRTrayekQ.Close;
    end
    else
    begin
      dRTrayek2Q.ParamByName('text').AsString:=kodearmada[arow];
      dRTrayek2Q.ParamByName('col').AsString:= cektglq[ACol];
      dRTrayek2Q.ExecSQL;
      dRTrayek2Q.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dRTrayek2Q.fieldbyname('tgl').AsString;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.AsString:= dRTrayek2Q.fieldbyname('sopir').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dRTrayek2Q.Close;
    end;
  end
  else if kondisi[arow][acol]='ancem' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      dAnjemQ.ParamByName('text').AsString:=kodearmada[arow];
      dAnjemQ.ParamByName('col').AsInteger:= (acol-3);
      //detailPerawatanQ.ExecSQL;
      dAnjemQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dAnjemQ.fieldbyname('tgl').AsString;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.AsString:= dAnjemQ.fieldbyname('sopir').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dAnjemQ.Close;
    end
    else
    begin
      dAnjem2Q.ParamByName('text').AsString:=kodearmada[arow];
      dAnjem2Q.ParamByName('col').AsString:= cektglq[ACol];
      dAnjem2Q.ExecSQL;
      dAnjem2Q.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= dAnjem2Q.fieldbyname('tgl').AsString;
        MasterVGridSopir.Visible:=true;
        DetailJadwalQSopir.AsString:= dAnjem2Q.fieldbyname('sopir').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
      dAnjem2Q.Close;
    end;
  end 
  else if kondisi[ARow][ACol]='perbaikan' then
  begin
    if cxComboBox1.ItemIndex=0 then
    begin
      detailPerbaikanQ.ParamByName('text').AsString:=kodearmada[arow];
      detailPerbaikanQ.ParamByName('col').AsInteger:= (acol-3);
      detailPerbaikanQ.ExecSQL;
      detailPerbaikanQ.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridMekanik.Visible:=true;
        //DetailJadwalQMekanik.asstring :=detailPerbaikanQ.FieldByName('mekanik').AsString ;
        MasterVGridMasalah.Visible:=true;
        DetailJadwalQMasalah.asstring :=detailPerbaikanQ.FieldByName('masalah').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= detailPerbaikanQ.fieldbyname('tgl').AsString;
        MasterVGridTindakan.Visible:=true;
        DetailJadwalQTindakan.AsString:=detailPerbaikanQ.fieldByName('tindakan').AsString;
        MasterVGridKeterangan.Visible:=true;
        DetailJadwalQKeterangan.AsString:=detailPerbaikanQ.fieldByName('keterangan').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=detailPerbaikanQ.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
        //Isi Untuk mekanik
        MasterVGridMekanik.Visible:=true;
        isiDetailQ.Close;
        isiDetailQ.SQL.Clear;
        isiDetailQ.SQL.Add('select m.nama as nama from DetailPerbaikanMekanik dpm, Mekanik m where m.Kode=dpm.Mekanik and dpm.KodePerbaikan='+QuotedStr(detailPerbaikanQ.fieldByName('kode').AsString));
        isiDetailQ.Open;
        isiDetailQ.First;
        DetailJadwalQMekanik.AsString:='';
        while not isiDetailQ.Eof do
        begin
          DetailJadwalQMekanik.AsString:=DetailJadwalQMekanik.AsString+isiDetailQ.fieldByName('nama').AsString+',';
          isiDetailQ.Next;
        end;
        // Isi Untuk Jenis Perawatan
        detailPerbaikanQ.First;
        DetailJadwalQJenisPerbaikan.AsString:='';
        MasterVGridJenisPerbaikan.Visible:=true;
        while not detailPerbaikanQ.Eof do
        begin
          DetailJadwalQJenisPerbaikan.AsString:=DetailJadwalQJenisPerbaikan.AsString  + detailPerbaikanQ.FieldByName('tipe').AsString+',';
          detailPerbaikanQ.Next;
        end;
      detailPerbaikanQ.Close;
    end
    else
    begin
      detailPerbaikanQ2.ParamByName('text').AsString:=kodearmada[arow];
      detailPerbaikanQ2.ParamByName('col').AsString:= cektglq[ACol];
      detailPerbaikanQ2.ExecSQL;
      detailPerbaikanQ2.Open;
        DetailJadwalQ.Close;
        DetailJadwalQ.Open;
        DetailJadwalQ.Edit;
        MasterVGridMekanik.Visible:=true;
        //DetailJadwalQMekanik.asstring :=detailPerbaikanQ2.FieldByName('mekanik').AsString ;
        MasterVGridMasalah.Visible:=true;
        DetailJadwalQMasalah.asstring :=detailPerbaikanQ2.FieldByName('masalah').AsString ;
        MasterVGridTgl.Visible:=true;
        DetailJadwalQTgl.AsString:= detailPerbaikanQ2.fieldbyname('tgl').AsString;
        MasterVGridTindakan.Visible:=true;
        DetailJadwalQTindakan.AsString:=detailPerbaikanQ2.fieldByName('tindakan').AsString;
        MasterVGridKeterangan.Visible:=true;
        DetailJadwalQKeterangan.AsString:=detailPerbaikanQ2.fieldByName('keterangan').AsString;
        MasterVGridLama.Visible:=true;
        DetailJadwalQLama.AsString:=detailPerbaikanQ2.fieldByName('lama').AsString;
        MasterVGridPlatNo.Visible:=true;
        DetailJadwalQPlatNo.AsString:=StringGrid1.Cells[0,arow];
        //Isi Untuk mekanik
        MasterVGridMekanik.Visible:=true;
        isiDetailQ.Close;
        isiDetailQ.SQL.Clear;
        isiDetailQ.SQL.Add('select m.nama as nama from DetailPerbaikanMekanik dpm, Mekanik m where m.Kode=dpm.Mekanik and dpm.KodePerbaikan='+QuotedStr(detailPerbaikanQ2.fieldByName('kode').AsString));
        isiDetailQ.Open;
        isiDetailQ.First;
        DetailJadwalQMekanik.AsString:='';
        while not isiDetailQ.Eof do
        begin
          DetailJadwalQMekanik.AsString:=DetailJadwalQMekanik.AsString+isiDetailQ.fieldByName('nama').AsString+',';
          isiDetailQ.Next;
        end;
        // Isi Untuk Jenis Perawatan
        detailPerbaikanQ2.First;
        DetailJadwalQJenisPerbaikan.AsString:='';
        MasterVGridJenisPerbaikan.Visible:=true;
        while not detailPerbaikanQ2.Eof do
        begin
          DetailJadwalQJenisPerbaikan.AsString:=DetailJadwalQJenisPerbaikan.AsString  + detailPerbaikanQ2.FieldByName('tipe').AsString+',';
          detailPerbaikanQ2.Next;
        end;
      detailPerbaikanQ2.Close;
    end;

  end;


end;

procedure TJadwalArmadaFm.cxComboBox1PropertiesChange(Sender: TObject);
var i,j,countery :integer;
  mydate : TDateTime;
  temp:String;
  temp2:String;
  rperbaikan,rperawatan,rreadynocrew,rready,rjalan,rso,rplot:integer;
  trperbaikan,trperawatan,trreadynocrew,trready,trjalan,trso,trplot:integer;
  alltotal:integer;
  cjalan,ctrayek,cancem:Boolean;
begin
DateTimePicker1.Visible:=false;
DateTimePicker2.Visible:=false;
DateTimePicker3.Visible:=false;
BBulan.Visible:=false;
BRange.Visible:=false;
BNormal.Visible:=false;
  if cxComboBox1.ItemIndex=1 then
  begin
    DateTimePicker1.Visible:=true;
    BBulan.Visible:=true;
  end
  else if cxComboBox1.ItemIndex=0 then
  begin
    BNormal.Visible:=true;
    
  end
  else if cxComboBox1.ItemIndex=2 then
  begin
    DateTimePicker2.Visible:=true;
    DateTimePicker3.Visible:=true;
    BRange.Visible:=true;
  end;
 StringGrid1.Repaint;
end;

procedure TJadwalArmadaFm.BRangeClick(Sender: TObject);
var i,j,countery :integer;
  mydate : TDateTime;
  temp:String;
  temp2:String;
  cek :Extended;
  masuk :Boolean;
  rperbaikan,rperawatan,rreadynocrew,rready,rjalan,rso,rplot:integer;
  trperbaikan,trperawatan,trreadynocrew,trready,trjalan,trso,trplot:integer;
  alltotal:integer;
  cjalan,ctrayek,cancem:Boolean;
begin
    StringGrid1.Visible:=true;
    masuk:=true;
    if DateTimePicker2.DateTime>DateTimePicker3.DateTime then
    begin
      ShowMessage('Yang kedua g boleh lebih kecil');
      masuk:=false;
    end;
    if (DateTimePicker3.DateTime-DateTimePicker2.DateTime)>31 then
    begin
      ShowMessage('Range Tidak boleh lebih dari 31');
      masuk:=false;
    end;

    if masuk=true then
    begin
        StringGrid1.ColCount:=0;
        for i := 0 to StringGrid1.RowCount - 1 do StringGrid1.Rows[i].Clear;
        cek:=DateTimePicker3.DateTime-DateTimePicker2.DateTime;
        StringGrid1.ColCount:= round(cek)+2+7;
        temp:= FormatDateTime('mm',DateTimePicker2.Date);
        mydate:=EncodeDateTime(StrToInt (FormatDateTime('yyyy',DateTimePicker2.Date)),StrToInt( FormatDateTime('mm',DateTimePicker2.Date)),StrToInt( FormatDateTime('dd',DateTimePicker2.Date)),0,0,0,0);
        //ShowMessage(''+DateToStr(MonthOf(DateTimePicker1.Date)));
        for i:=1 to StringGrid1.ColCount do
        begin
          StringGrid1.Cells[i,0]:=FormatDateTime('dd-mmm-yyyy', mydate+i-1);
        end;
        armadaQ.Close;
        {armadaQ.ParamByName('b1').AsBoolean := cxCheckBoxToilet.Checked;
        armadaQ.ParamByName('b2').AsBoolean := cxCheckBoxAirSuspension.Checked ;
        armadaQ.ParamByName('b3').AsBoolean := cxCheckBoxAktif.Checked   ;
        armadaQ.ParamByName('b4').AsBoolean := cxCheckBoxAC.Checked; }
        armadaQ.ParamByName('t1').AsString :='%'+cxTextEditPlatno.Text+'%';
        armadaQ.ParamByName('t2').AsString  := '%'+cxTextEditTahun.Text+'%' ;
        armadaQ.ParamByName('t3').AsInteger  := strtoint(ComboBox1.text);
        armadaQ.open;

        if armadaQ.RecordCount <1 then
        begin
          ShowMessage('No Record Found'); 
        end
        else
        begin
          StringGrid1.RowCount:=  armadaQ.RecordCount+1;
          armadaQ.First;
          countery:=1;
          temp2:=FormatDateTime('mm/dd/yyyy',mydate);

          trperbaikan:=0;
          trperawatan:=0;
          trreadynocrew:=0;
          trready:=0;
          trjalan:=0;
          trso:=0;
          trplot:=0;
          alltotal:=0;
          //Bersihkan Isi Yang Lama
          SetLength(kondisi,(armadaQ.RecordCount+1));
          SetLength(kodearmada,(armadaQ.RecordCount+1));
          for i:=0 to armadaQ.RecordCount do
          begin
            SetLength(kondisi[i],45);
            kodearmada[i]:='';
          end;

          for i:=0 to armadaQ.RecordCount do
          begin
            for j:=0 to 34 do
            begin
              kondisi[i][j]:='';
            end;
          end;
          //Bersihkan Isi Yang Lama
          while not armadaQ.Eof do
          begin
              rperbaikan:=0;
              rperawatan:=0;
              rreadynocrew:=0;
              rready:=0;
              rjalan:=0;
              rso:=0;
              rplot:=0;
              kodearmada[countery]:=armadaQ.FieldByName('kode').AsString;
              StringGrid1.Cells[0,countery] := armadaQ.FieldByName('platno').AsString + '('+armadaQNoBody.AsString+')' ;
              for i:=0 to StringGrid1.ColCount-9 do
              begin
                cektglq[i+1]:=FormatDateTime('mm/dd/yyyy',mydate+i);
                statusQ.Close;
                statusQ.SQL.Clear;
                statusQ.SQL.Add('select 0 as ok from MasterSJ sj,MasterSO so ,Rute r where r.Kode=so.Rute and sj.NoSO=so.Kodenota and so.Armada=' +QuotedStr(armadaQ.FieldByName('kode').AsString)+ ' and convert(date,sj.Tgl)<=(convert(datetime,'+ QuotedStr(temp2) +')+'+ IntToStr(i) +') and convert(date,(sj.Tgl+r.waktu))>=(convert(datetime,'+ QuotedStr(temp2) +')+'+ IntToStr(i) + ')');
                statusQ.Open;
                  cjalan := true;
                  ctrayek :=true;
                  cancem:= true;
                  if statusQ.RecordCount<1 then cjalan:=false;
                  cekAnjem2Q.Close;
                  cekAnjem2Q.ParamByName('text').AsString:=armadaQKode.AsString;
                  cekAnjem2Q.ParamByName('t1').AsInteger := i;
                  cekAnjem2Q.ParamByName('t2').AsInteger := i;
                  cekAnjem2Q.ParamByName('s1').AsString := temp2;
                  cekAnjem2Q.ParamByName('s2').AsString := temp2;
                  cekAnjem2Q.Open;
                  if cekAnjem2Q.RecordCount < 1 then cancem:=false;
                  crTrayek2Q.Close;
                  crTrayek2Q.ParamByName('text').AsString:=armadaQKode.AsString;
                  crTrayek2Q.ParamByName('t1').AsInteger := i;
                  //crTrayek2Q.ParamByName('t2').AsInteger := i;
                  crTrayek2Q.ParamByName('s1').AsString := temp2;
                  //crTrayek2Q.ParamByName('s2').AsString := temp2;
                  crTrayek2Q.Open;
                  if crTrayek2Q.RecordCount < 1 then ctrayek :=false;

                  if (ctrayek = false) and (cjalan =false) and (cancem = false) then
                  begin
                    cekso2Q.Close;
                    cekso2Q.ParamByName('a1').AsString:=armadaQ.FieldByName('kode').AsString;
                    cekso2Q.ParamByName('s1').AsString:=temp2;
                    cekso2Q.ParamByName('s2').AsString:=temp2;
                    cekso2Q.ParamByName('t1').AsInteger:=i;
                    cekso2Q.ParamByName('t2').AsInteger:=i;
                    cekso2Q.Open;

                    if cekso2Q.RecordCount<1 then
                    begin
                      cekTrayek2Q.Close;
                      cekTrayek2Q.ParamByName('text').AsString:=armadaQPlatNo.AsString;
                      cekTrayek2Q.ParamByName('t1').AsInteger := i;
                      cekTrayek2Q.ParamByName('t2').AsInteger := i;
                      cekTrayek2Q.ParamByName('s1').AsString := temp2;
                      cekTrayek2Q.ParamByName('s2').AsString := temp2;
                      cekTrayek2Q.Open;
                      if cekTrayek2Q.RecordCount < 1 then
                      begin
                        perawatanQ.Close;
                        perawatanQ.SQL.Clear;
                        perawatanQ.SQL.Add('select 0 as ok from LaporanPerawatan l,detailperawatanjenis d, standarperawatan s where s.Kode=d.JenisPerawatan and d.KodePerawatan=l.Kode and l.Armada=' + QuotedStr(armadaQ.FieldByName('kode').AsString) + ' and convert(date,l.WaktuMulai)<=(convert(datetime,' + QuotedStr(temp2) + ')+' + IntToStr(i) + ') and isnull(l.WaktuSelesai,convert(date,(l.WaktuMulai+(select MAX(StandarPerawatan.StandardWaktu) from StandarPerawatan where Kode in (d.JenisPerawatan) ))))>=(convert(datetime,' + QuotedStr(temp2) +')+' + IntToStr(i) + ')');
                        //perawatanQ.ExecSQL;
                        perawatanQ.Open;
                          if perawatanQ.RecordCount < 1 then
                          begin
                            perbaikanQ.Close;
                            perbaikanQ.SQL.Clear;
                            perbaikanQ.SQL.Add('select 0 as ok from PermintaanPerbaikan p, LaporanPerbaikan l, DetailPerbaikanJenis d , JenisPerbaikan j where j.Kode=d.JenisPerbaikan and d.KodePerbaikan=l.Kode and  p.Kode=l.PP and p.Armada='+ QuotedStr(armadaQ.FieldByName('kode').AsString) +' and convert(date,l.WaktuMulai)<=(convert(datetime,' + QuotedStr(temp2) + ')+' + IntToStr(i) +') and isnull(l.WaktuSelesai,convert(date,(l.WaktuMulai+(select MAX(JenisPerbaikan.lamapengerjaan) from JenisPerbaikan where Kode in (d.JenisPerbaikan) ))))>=(convert(datetime,' + QuotedStr(temp2) + ')+' + IntToStr(i) + ')');
                            //perbaikanQ.ExecSQL;
                            perbaikanQ.Open;
                              if perbaikanQ.RecordCount < 1 then
                              begin
                                if (armadaQ.FieldByName('sopir').AsString = null) or (armadaQ.FieldByName('sopir').AsString = '') then
                                begin
                                  kondisi[countery][i+1] := 'readynocrew';
                                  rreadynocrew:=rreadynocrew+1;
                                end
                                else
                                begin
                                  kondisi[countery][i+1] := 'ready';
                                  rready:=rready+1;
                                end;
                              end
                              else
                              begin
                                kondisi[countery][i+1] := 'perbaikan';
                                rperbaikan:=rperbaikan+1;
                              end;
                            perbaikanQ.Close;
                          end
                          else
                          begin
                            kondisi[countery][i+1] := 'perawatan';
                            rperawatan:=rperawatan+1;
                          end;
                        perawatanQ.Close;
                      end
                      else
                      begin
                        kondisi[countery][i+1] := 'plot';
                        rplot:=rplot+1;
                      end;
                      cekTrayek2Q.Close;
                    end
                    else
                    begin
                      kondisi[countery][i+1] := 'so';
                      rso:=rso+1;
                    end;
                  end
                  else
                  begin
                    if cjalan = true then
                    begin
                      kondisi[countery][i+1] := 'jalan';
                      rjalan:=rjalan+1;
                    end
                    else if ctrayek = true then
                    begin
                      kondisi[countery][i+1] := 'trayek';
                      rjalan:=rjalan+1;
                    end
                    else if cancem = true then
                    begin
                      kondisi[countery][i+1] := 'ancem';
                      rjalan:=rjalan+1;
                    end;
                  end;
                statusQ.Close;
              end;
              armadaQ.Next;
              StringGrid1.Cells[StringGrid1.ColCount-7,countery]:=IntToStr(rperbaikan) + '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rperbaikan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-6,countery]:=IntToStr(rperawatan)+ '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rperawatan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-5,countery]:=IntToStr(rreadynocrew)+ '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rreadynocrew/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-4,countery]:=IntToStr(rready)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rready/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-3,countery]:=IntToStr(rjalan)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rjalan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-2,countery]:=IntToStr(rso)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rso/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              StringGrid1.Cells[StringGrid1.ColCount-1,countery]:=IntToStr(rplot) + '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((+rplot/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
              countery:=countery+1 ;
              trperbaikan:=trperbaikan+rperbaikan;
              trperawatan:=trperawatan+rperawatan;
              trreadynocrew:=trreadynocrew+rreadynocrew;
              trready:=trready+rready;
              trjalan:=trjalan+rjalan;
              trso:=trso+rso;
              trplot:=trplot +rplot;
          end;
          alltotal:=alltotal+trperbaikan+trperawatan+trreadynocrew+trready+trjalan+trso+trplot;
          lperbaikan.Caption:= IntToStr( trperbaikan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trperbaikan/alltotal*100) +'%)';
          lperawatan.Caption:= IntToStr(trperawatan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trperawatan/alltotal*100) +'%)';
          lreadynocrew.Caption:= IntToStr(trreadynocrew)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trreadynocrew/alltotal*100) +'%)';
          lready.Caption:= IntToStr( trready)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trready/alltotal*100) +'%)';
          ljalan.Caption:= IntToStr( trjalan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trjalan/alltotal*100) +'%)';
          LSO.Caption:=IntToStr(trso)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trso/alltotal*100) +'%)';
          ltrayek.Caption:=IntToStr(trplot)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trplot/alltotal*100) +'%)';
          armadaQ.Close;
          gambar:=false;

        end;


    end;
     StringGrid1.Repaint;
end;

procedure TJadwalArmadaFm.BBulanClick(Sender: TObject);
var mydate,awal,akhir : TDateTime;
    bulan : word;
begin
  mydate:=DateTimePicker2.Date;
  //ShowMessage(DateToStr(mydate));
  //awal:=StartOfTheMonth(mydate);
  //ShowMessage(DateToStr(awal));
  //akhir:=EndOfTheMonth(mydate);
  //ShowMessage(DateToStr(akhir));
  //bulan:= MonthOf(DateTimePicker2.date);
  //ShowMessage(inttostr(bulan));
  DateTimePicker2.Date:=StartOfTheMonth(mydate);
  DateTimePicker3.Date:=EndOfTheMonth(mydate);
  BRangeClick(sender);
end;

procedure TJadwalArmadaFm.FormCreate(Sender: TObject);
var counterx :integer;
    countery :integer;
    i,j :integer;
    rperbaikan,rperawatan,rreadynocrew,rready,rjalan,rso,rplot:integer;
    trperbaikan,trperawatan,trreadynocrew,trready,trjalan,trso,trplot:integer;
    alltotal:integer;
    cjalan,ctrayek,cancem:Boolean;
begin
    StringGrid1.ColWidths[0]:=100;
    DateTimePicker1.Date:=Date;
    DateTimePicker2.Date:=Date;
    DateTimePicker3.Date:=Date;
    StringGrid1.ColCount:= StringGrid1.ColCount + 7;
    StringGrid1.Cells[10,0]:='perbaikan';
    StringGrid1.Cells[11,0]:='perawatan';
    StringGrid1.Cells[12,0]:='readynocrew';
    StringGrid1.Cells[13,0]:='ready';
    StringGrid1.Cells[14,0]:='jalan';
    StringGrid1.Cells[15,0]:='so';
    StringGrid1.Cells[15,0]:='plot';
    StringGrid1.Cells[1,0]:=FormatDateTime('dd-mmm-yyyy', now-2);
    StringGrid1.Cells[2,0]:=FormatDateTime('dd-mmm-yyyy', now-1);
    StringGrid1.Cells[3,0]:=FormatDateTime('dd-mmm-yyyy', now);
    StringGrid1.Cells[4,0]:=FormatDateTime('dd-mmm-yyyy', now+1);
    StringGrid1.Cells[5,0]:=FormatDateTime('dd-mmm-yyyy', now+2);
    StringGrid1.Cells[6,0]:=FormatDateTime('dd-mmm-yyyy', now+3);
    StringGrid1.Cells[7,0]:=FormatDateTime('dd-mmm-yyyy', now+4);
    StringGrid1.Cells[8,0]:=FormatDateTime('dd-mmm-yyyy', now+5);
    StringGrid1.Cells[9,0]:=FormatDateTime('dd-mmm-yyyy', now+6);

    armadaQ.open;
    SetLength(kondisi,(armadaQ.RecordCount+1));
    SetLength(kodearmada,(armadaQ.RecordCount+1));
    for i:=0 to armadaQ.RecordCount do
    begin
      SetLength(kondisi[i],45);
      kodearmada[i]:='';
    end;

    for i:=0 to armadaQ.RecordCount do
    begin
      for j:=0 to 34 do
      begin
        kondisi[i][j]:='';
      end;
    end;
    JumlahSeatQ.Open;
    JumlahSeatQ.First;
    while JumlahSeatQ.Eof=False do begin
      ComboBox1.Items.Add(JumlahSeatQJumlahSeat.AsString);
      JumlahSeatQ.Next;
    end;
    ComboBox1.ItemIndex:=0;
    BNormalClick(sender);
end;

procedure TJadwalArmadaFm.BNormalClick(Sender: TObject);
var i,j,countery :integer;
  mydate : TDateTime;
  temp:String;
  temp2:String;
  rperbaikan,rperawatan,rreadynocrew,rready,rjalan,rso,rplot:integer;
  trperbaikan,trperawatan,trreadynocrew,trready,trjalan,trso,trplot:integer;
  alltotal:integer;
  cjalan,ctrayek,cancem:Boolean;
begin
StringGrid1.Visible:=true;
for i := 0 to StringGrid1.RowCount - 1 do StringGrid1.Rows[i].Clear;
    StringGrid1.ColCount:=10;
    StringGrid1.ColCount:= StringGrid1.ColCount + 7;
    StringGrid1.Cells[10,0]:='perbaikan';
    StringGrid1.Cells[11,0]:='perawatan';
    StringGrid1.Cells[12,0]:='readynocrew';
    StringGrid1.Cells[13,0]:='ready';
    StringGrid1.Cells[14,0]:='jalan';
    StringGrid1.Cells[15,0]:='so';
    StringGrid1.Cells[15,0]:='plot';
    StringGrid1.Cells[1,0]:=FormatDateTime('dd-mmm-yyyy', now-2);
    StringGrid1.Cells[2,0]:=FormatDateTime('dd-mmm-yyyy', now-1);
    StringGrid1.Cells[3,0]:=FormatDateTime('dd-mmm-yyyy', now);
    StringGrid1.Cells[4,0]:=FormatDateTime('dd-mmm-yyyy', now+1);
    StringGrid1.Cells[5,0]:=FormatDateTime('dd-mmm-yyyy', now+2);
    StringGrid1.Cells[6,0]:=FormatDateTime('dd-mmm-yyyy', now+3);
    StringGrid1.Cells[7,0]:=FormatDateTime('dd-mmm-yyyy', now+4);
    StringGrid1.Cells[8,0]:=FormatDateTime('dd-mmm-yyyy', now+5);
    StringGrid1.Cells[9,0]:=FormatDateTime('dd-mmm-yyyy', now+6);

    armadaQ.Close ;
    armadaQ.ParamByName('t1').AsString := cxTextEditPlatno.Text;
    armadaQ.ParamByName('t2').AsString := cxTextEditTahun.Text;
    armadaQ.ParamByName('t3').AsInteger  := strtoint(ComboBox1.Text) ;
    armadaQ.open;
    if armadaQ.RecordCount <1  then
    begin
      ShowMessage('No Record Found');
    end
    else
    begin
      StringGrid1.RowCount:=  armadaQ.RecordCount+1;
      SetLength(kondisi,(armadaQ.RecordCount+1));
      SetLength(kodearmada,(armadaQ.RecordCount+1));
      for i:=0 to armadaQ.RecordCount do
      begin
        SetLength(kondisi[i],45);
        kodearmada[i]:='';
      end;

      for i:=0 to armadaQ.RecordCount do
      begin
        for j:=0 to 34 do
        begin
          kondisi[i][j]:='';
        end;
      end;
      trperbaikan:=0;
      trperawatan:=0;
      trreadynocrew:=0;
      trready:=0;
      trjalan:=0;
      trso:=0;
      trplot:=0;
      //ShowMessage(IntToStr(armadaQ.RecordCount));
      armadaQ.First;
      countery:=1;
      alltotal:=0;
      while not armadaQ.Eof do
      begin
          rperbaikan:=0;
          rperawatan:=0;
          rreadynocrew:=0;
          rready:=0;
          rjalan:=0;
          rso:=0;
          rplot:=0;
          kodearmada[countery]:=armadaQ.FieldByName('kode').AsString;
          StringGrid1.Cells[0,countery] := armadaQ.FieldByName('platno').AsString + '('+armadaQNoBody.AsString+')' ;
          for i:=0 to 8 do
          begin
            statusQ.Close;
            statusQ.SQL.Clear;
            statusQ.SQL.Add('select 0 as ok from MasterSJ sj,MasterSO so ,Rute r where r.Kode=so.Rute and sj.NoSO=so.Kodenota and so.Armada=' +QuotedStr(armadaQ.FieldByName('kode').AsString)+ ' and convert(date,sj.Tgl)<=convert(date,CURRENT_TIMESTAMP+'+ IntToStr(i-2) +') and convert(date,(sj.Tgl+r.waktu))>=convert(date,CURRENT_TIMESTAMP+'+ IntToStr(i-2) + ')');
            statusQ.Open;
              cjalan := true;
              ctrayek :=true;
              cancem:= true;
              if statusQ.RecordCount<1 then cjalan:=false;
              cekAnjemQ.Close;
              cekAnjemQ.ParamByName('text').AsString:=armadaQKode.AsString;
              cekAnjemQ.ParamByName('col').AsInteger := i-2;
              cekAnjemQ.Open;
              if cekAnjemQ.RecordCount < 1 then cancem:=false;
              crTrayekQ.Close;
              crTrayekQ.ParamByName('text').AsString :=armadaQKode.AsString;
              crTrayekQ.ParamByName('col').AsInteger:=i-2;
              crTrayekQ.Open;
              if crTrayekQ.RecordCount < 1 then ctrayek :=false;

              if (ctrayek = false) and (cjalan =false) and (cancem = false) then
              begin
                ceksoQ.Close;
                ceksoQ.ParamByName('a1').AsString:=armadaQ.FieldByName('kode').AsString;
                ceksoQ.ParamByName('t1').AsInteger:=i-2;
                ceksoQ.ParamByName('t2').AsInteger:=i-2;
                ceksoQ.Open;
                if ceksoQ.RecordCount <1 then
                begin
                  cekTrayekQ.Close;
                  cekTrayekQ.ParamByName('text').AsString:=armadaQPlatNo.AsString;
                  cekTrayekQ.ParamByName('col').AsInteger:=i-2;
                  cekTrayekQ.Open;
                  if cekTrayekQ.RecordCount < 1 then
                  begin
                    perawatanQ.Close;
                    perawatanQ.SQL.Clear;
                    perawatanQ.SQL.Add('select 0 as ok from LaporanPerawatan l,detailperawatanjenis d, standarperawatan s where s.Kode=d.JenisPerawatan and d.KodePerawatan=l.Kode and l.Armada=' + QuotedStr(armadaQ.FieldByName('kode').AsString) + ' and convert(date,l.WaktuMulai)<=convert(date,CURRENT_TIMESTAMP+' + IntToStr(i-2) + ') and isnull(l.WaktuSelesai,convert(date,(l.WaktuMulai+(select MAX(StandarPerawatan.StandardWaktu) from StandarPerawatan where Kode in (d.JenisPerawatan) ))))>=convert(date,CURRENT_TIMESTAMP+' + IntToStr(i-2) + ')');
                    //perawatanQ.ExecSQL;
                    perawatanQ.Open;
                      if perawatanQ.RecordCount < 1 then
                      begin
                        perbaikanQ.Close;
                        perbaikanQ.SQL.Clear;
                        perbaikanQ.SQL.Add('select 0 as ok from PermintaanPerbaikan p, LaporanPerbaikan l, DetailPerbaikanJenis d , JenisPerbaikan j where j.Kode=d.JenisPerbaikan and d.KodePerbaikan=l.Kode and  p.Kode=l.PP and p.Armada='+ QuotedStr(armadaQ.FieldByName('kode').AsString) +' and convert(date,l.WaktuMulai)<=convert(date,CURRENT_TIMESTAMP+' + IntToStr(i-2) +') and isnull(l.WaktuSelesai,convert(date,(l.WaktuMulai+(select MAX(JenisPerbaikan.lamapengerjaan) from JenisPerbaikan where Kode in (d.JenisPerbaikan) ))))>=convert(date,CURRENT_TIMESTAMP+' + IntToStr(i-2) + ')');
                        //perbaikanQ.ExecSQL;
                        perbaikanQ.Open;
                          if perbaikanQ.RecordCount < 1 then
                          begin
                            if (armadaQ.FieldByName('sopir').AsString = null) or (armadaQ.FieldByName('sopir').AsString = '') then
                            begin
                              kondisi[countery][i+1] := 'readynocrew';
                              rreadynocrew:=rreadynocrew+1;
                            end
                            else
                            begin
                              kondisi[countery][i+1] := 'ready';
                              rready:=rready+1;
                            end;
                          end
                          else
                          begin
                            kondisi[countery][i+1] := 'perbaikan';
                            rperbaikan:=rperbaikan+1;
                          end;
                        perbaikanQ.Close;
                      end
                      else
                      begin
                        kondisi[countery][i+1] := 'perawatan';
                        rperawatan:=rperawatan+1;
                      end;
                    perawatanQ.Close;
                  end
                  else
                  begin
                    kondisi[countery][i+1] := 'plot';
                    rplot:=rplot+1;
                    //ShowMessage('plot')
                  end;
                end
                else
                begin
                  kondisi[countery][i+1] := 'so';
                  rso:=rso+1;
                end;
              end
              else
              begin
                if cjalan = true then
                begin
                  kondisi[countery][i+1] := 'jalan';
                  rjalan:=rjalan+1;
                end
                else if ctrayek = true then
                begin
                  kondisi[countery][i+1] := 'trayek';
                  rjalan:=rjalan+1;
                end
                else if cancem = true then
                begin
                  kondisi[countery][i+1] := 'ancem';
                  rjalan:=rjalan+1;
                end;
              end;
            statusQ.Close;
            cekAnjemQ.Close;
            crTrayekQ.Close;
          end;
          armadaQ.Next;
          StringGrid1.Cells[10,countery]:=IntToStr(rperbaikan) + '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rperbaikan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[11,countery]:=IntToStr(rperawatan)+ '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rperawatan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[12,countery]:=IntToStr(rreadynocrew)+ '/' + IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rreadynocrew/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[13,countery]:=IntToStr(rready)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rready/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[14,countery]:=IntToStr(rjalan)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rjalan/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[15,countery]:=IntToStr(rso)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((rso/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          StringGrid1.Cells[16,countery]:=IntToStr(rplot)+ '/'+ IntToStr(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot) + ' (' + FormatFloat('#.##',((+rplot/(rperbaikan+rperawatan+rreadynocrew+rready+rjalan+rso+rplot)))*100) + '%)';
          trperbaikan:=trperbaikan+rperbaikan;
          trperawatan:=trperawatan+rperawatan;
          trreadynocrew:=trreadynocrew+rreadynocrew;
          trready:=trready+rready;
          trjalan:=trjalan+rjalan;
          trso:=trso+rso;
          trplot:=trplot+rplot;
          countery:=countery+1;
      end;
        alltotal:=alltotal+trperbaikan+trperawatan+trreadynocrew+trready+trjalan+trso+trplot;
        lperbaikan.Caption:= IntToStr( trperbaikan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trperbaikan/alltotal*100) +'%)';
        lperawatan.Caption:= IntToStr(trperawatan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trperawatan/alltotal*100) +'%)';
        lreadynocrew.Caption:= IntToStr(trreadynocrew)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trreadynocrew/alltotal*100) +'%)';
        lready.Caption:= IntToStr( trready)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trready/alltotal*100) +'%)';
        ljalan.Caption:= IntToStr( trjalan)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trjalan/alltotal*100) +'%)';
        LSO.Caption:=IntToStr(trso)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trso/alltotal*100) +'%)';
        ltrayek.Caption:=IntToStr(trplot)+'/'+IntToStr(alltotal) +'('+ FormatFloat('#.##',trplot/alltotal*100) +'%)';
      armadaQ.Close;
      gambar:=false;
      StringGrid1.Repaint;
    end;

end;

end.
