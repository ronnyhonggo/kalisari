unit JenisKendaraanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TJenisKendaraanDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    JenisKendaraanQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    JenisKendaraanQKode: TStringField;
    JenisKendaraanQNamaJenis: TStringField;
    JenisKendaraanQTipe: TMemoField;
    JenisKendaraanQKategori: TStringField;
    JenisKendaraanQKeterangan: TMemoField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaJenis: TcxGridDBColumn;
    cxGrid1DBTableView1Tipe: TcxGridDBColumn;
    cxGrid1DBTableView1Kategori: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode,nama,tipe:string;
  end;

var
  JenisKendaraanDropDownFm: TJenisKendaraanDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TJenisKendaraanDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TJenisKendaraanDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=JenisKendaraanQ.Fields[0].AsString;
  nama:=JenisKendaraanQNamaJenis.AsString;
  tipe:=JenisKendaraanQTipe.AsString;
  ModalResult:=mrOk;
end;

procedure TJenisKendaraanDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=JenisKendaraanQ.Fields[0].AsString;
  nama:=JenisKendaraanQNamaJenis.AsString;
  tipe:=JenisKendaraanQTipe.AsString;
  ModalResult:=mrOk;
end;

procedure TJenisKendaraanDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=JenisKendaraanQ.Fields[0].AsString;
  nama:=JenisKendaraanQNamaJenis.AsString;
  tipe:=JenisKendaraanQTipe.AsString;
  ModalResult:=mrOk;
end;
end;

procedure TJenisKendaraanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=JenisKendaraanQ.Fields[0].AsString;
  nama:=JenisKendaraanQNamaJenis.AsString;
  tipe:=JenisKendaraanQTipe.AsString;
  ModalResult:=mrOk;
end;

end.
