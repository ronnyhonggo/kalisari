unit JenisPerbaikanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TJenisPerbaikanDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    JenisPerbaikanQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    JenisPerbaikanQKode: TStringField;
    JenisPerbaikanQNama: TStringField;
    JenisPerbaikanQLamaPengerjaan: TIntegerField;
    JenisPerbaikanQLamaGaransi: TIntegerField;
    JenisPerbaikanQCreateDate: TDateTimeField;
    JenisPerbaikanQCreateBy: TStringField;
    JenisPerbaikanQOperator: TStringField;
    JenisPerbaikanQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1LamaPengerjaan: TcxGridDBColumn;
    cxGrid1DBTableView1LamaGaransi: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  JenisPerbaikanDropDownFm: TJenisPerbaikanDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TJenisPerbaikanDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TJenisPerbaikanDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=JenisPerbaikanQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TJenisPerbaikanDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=JenisPerbaikanQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TJenisPerbaikanDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=JenisPerbaikanQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TJenisPerbaikanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=JenisPerbaikanQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

end.
