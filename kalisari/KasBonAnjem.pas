unit KasBonAnjem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxStyles,
  cxEdit, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxContainer, StdCtrls, cxTextEdit,
  cxMaskEdit, cxButtonEdit, ComCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxInplaceContainer, cxVGrid, cxDBVGrid,
  ExtCtrls, SDEngine, Menus, cxButtons, cxCurrencyEdit, UCrpeClasses,
  UCrpe32, cxLabel, cxDropDownEdit, cxCalendar, cxGroupBox, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxSkinsdxBarPainter,
  dxPSCore, dxPScxCommon;

type
  TKasBonAnjemFm = class(TForm)
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel2: TPanel;
    StatusBar: TStatusBar;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    MasterQ: TSDQuery;
    MasterUpdate: TSDUpdateSQL;
    DataSource1: TDataSource;
    MasterQKode: TStringField;
    MasterQKontrak: TStringField;
    MasterQSopir: TStringField;
    MasterQNominal: TCurrencyField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    deleteBtn: TcxButton;
    ExitBtn: TcxButton;
    viewKasBonAnjem: TSDQuery;
    DataSource2: TDataSource;
    cxDBVerticalGrid1Kontrak: TcxDBEditorRow;
    cxDBVerticalGrid1Sopir: TcxDBEditorRow;
    cxDBVerticalGrid1Nominal: TcxDBEditorRow;
    KontrakQ: TSDQuery;
    PegawaiQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    KodeQkode: TStringField;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    MasterQKodePelanggan: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQNoTelp: TStringField;
    MasterQNamaPengemudi: TStringField;
    cxDBVerticalGrid1NamaPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1AlamatPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1NoTelp: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPengemudi: TcxDBEditorRow;
    viewKasBonAnjemKode: TStringField;
    viewKasBonAnjemKontrak: TStringField;
    viewKasBonAnjemSopir: TStringField;
    viewKasBonAnjemNominal: TCurrencyField;
    viewKasBonAnjemCreateDate: TDateTimeField;
    viewKasBonAnjemCreateBy: TStringField;
    viewKasBonAnjemOperator: TStringField;
    viewKasBonAnjemTglEntry: TDateTimeField;
    viewKasBonAnjemTglCetak: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Kontrak: TcxGridDBColumn;
    cxGrid1DBTableView1Nominal: TcxGridDBColumn;
    viewKasBonAnjemKodePelanggan: TStringField;
    viewKasBonAnjemNamaPelanggan: TStringField;
    viewKasBonAnjemNamaPengemudi: TStringField;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPengemudi: TcxGridDBColumn;
    MasterQStatus: TStringField;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    updateQ: TSDQuery;
    Crpe1: TCrpe;
    cetakBtn: TcxButton;
    viewKasBonAnjemStatus: TStringField;
    viewKasBonAnjemnamaPT: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQTanggal: TDateTimeField;
    cxDBVerticalGrid1Tanggal: TcxDBEditorRow;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxButton1: TcxButton;
    viewKasBonAnjemTanggal: TStringField;
    viewKasBonAnjemTanggal_1: TDateTimeField;
    cxGrid1DBTableView1Tanggal_1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1SopirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure deleteBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cetakBtnClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KasBonAnjemFm: TKasBonAnjemFm;
  MasterOriSQL:string;

implementation

uses DM, MenuUtama, StrUtils, DropDown, KontrakDropDown, PengemudiDropDown;

{$R *.dfm}

procedure TKasBonAnjemFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TKasBonAnjemFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TKasBonAnjemFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewKasBonAnjem.Close;
  viewKasBonAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKasBonAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewKasBonAnjem.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKasBonAnjem.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBonAnjem.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBonAnjem.AsBoolean;
end;

procedure TKasBonAnjemFm.KodeEditEnter(Sender: TObject);
begin
 KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKasBonAnjemFm.KodeEditExit(Sender: TObject);
begin
if KodeEdit.Text<>'' then
  begin
    //cxDBVerticalGrid1.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
   MasterQ.SQL.Clear;
   MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kode like '+ QuotedStr('%'+kodeedit.Text+'%'));
   MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQTanggal.AsDateTime:=date;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBonAnjem.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKasBonAnjem.AsBoolean;
    cxDBVerticalGrid1.Enabled:=true;
    MasterQStatus.AsString:='ON PROCESS';
  end;
end;

procedure TKasBonAnjemFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;


procedure TKasBonAnjemFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  //cxDBVerticalGrid1.SetFocus;
end;

procedure TKasBonAnjemFm.cxDBVerticalGrid1SopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open; }
  PengemudiDropDownFm:=TPengemudiDropdownfm.Create(Self);
    if PengemudiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSopir.AsString:=PengemudiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PengemudiDropDownFm.Release;
end;

procedure TKasBonAnjemFm.cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL,a:string;
begin
{KontrakQ.Close;
tempSQL:=KontrakQ.SQL.Text;
KontrakQ.SQL.Text:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"';

 //PegawaiQ.ParamByName('text').AsString:='';
  //KontrakQ.ExecSQL;
  KontrakQ.Open; }
  a:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"';
  KontrakDropDownFm:=TKontrakDropdownfm.Create(Self,a);
    if KontrakDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKontrak.AsString:=KontrakDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    KontrakDropDownFm.Release;
    //KontrakQ.SQL.Text:=tempSQL;
end;

procedure TKasBonAnjemFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TKasBonAnjemFm.SaveBtnClick(Sender: TObject);
var kode:string;
begin
if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
    kode:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
  kode:=MasterQKode.AsString;
    MasterQ.ApplyUpdates;

    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');

    //cxButtonEdit1PropertiesButtonClick(sender,0);

  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  KodeEdit.SetFocus;
  KontrakQ.Open;
  PegawaiQ.Open;
  PelangganQ.Open;
  viewKasBonAnjem.Close;
  //viewKasBonAnjem.ExecSQL;
  viewKasBonAnjem.Open;

   //cetak Kas Bon anjem
    if MessageDlg('Cetak Kas Bon Anjem '+kode+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
           updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update KasBonAnjem set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(kode));
      updateQ.ExecSQL;
      updateQ.Close;

      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonSopirAnjem.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kode;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
    //  Crpe1.Print;
     // Crpe1.CloseWindow;
    end;
    //end Cetak Kas Bon Anjem
end;

procedure TKasBonAnjemFm.deleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Kas Bon Anjem '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Kas Bon Anjem telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Kas Bon Anjem pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
      viewKasBonAnjem.Close;
  //viewKasBonAnjem.ExecSQL;
  viewKasBonAnjem.Open;
  end;
end;

procedure TKasBonAnjemFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
cxDBVerticalGrid1.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewKasBonAnjemKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= viewKasBonAnjemKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBonAnjem.AsBoolean;
    end;
    SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateKasBonAnjem.AsBoolean;
   cxDBVerticalGrid1.Enabled:=true;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);
  //  akhirnya:='';
end;

procedure TKasBonAnjemFm.cetakBtnClick(Sender: TObject);
begin
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update KasBonAnjem set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;

      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonSopirAnjem.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
    //  Crpe1.Print;
     // Crpe1.CloseWindow;
end;

procedure TKasBonAnjemFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewKasBonAnjem.Close;
  viewKasBonAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKasBonAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewKasBonAnjem.Open;
end;

procedure TKasBonAnjemFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewKasBonAnjem.Close;
  viewKasBonAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKasBonAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewKasBonAnjem.Open;
end;

procedure TKasBonAnjemFm.cxButton1Click(Sender: TObject);
begin
  dxComponentPrinter1Link1.Preview();
end;

procedure TKasBonAnjemFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  MasterQCreateBy.AsString:=User;
end;

end.
