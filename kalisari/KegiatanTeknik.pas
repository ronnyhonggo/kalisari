unit KegiatanTeknik;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinsDefaultPainters, DB, SDEngine, UCrpeClasses, UCrpe32,
  StdCtrls, cxButtons, ComCtrls, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TKegiatanTeknikFm = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    RadioButtonBln: TRadioButton;
    RadioButtonTgl: TRadioButton;
    ComboBox1: TComboBox;
    DateTimePickerDay: TDateTimePicker;
    DateTimePickerDay2: TDateTimePicker;
    DateTimePickerMonth: TDateTimePicker;
    DateTimePickerMonth2: TDateTimePicker;
    cxButton1: TcxButton;
    Crpe1: TCrpe;
    masterq: TSDQuery;
    masterds: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KegiatanTeknikFm: TKegiatanTeknikFm;

implementation
uses DateUtils, MenuUtama, StrUtils;
{$R *.dfm}

procedure TKegiatanTeknikFm.FormCreate(Sender: TObject);
begin
DateTimePickerDay.Date:=now;
  DateTimePickerDay2.Date:=now;
  DateTimePickerMonth.Date:=now;
  DateTimePickerMonth2.Date:=now;
  
  ComboBox1.Items.Clear;
  ComboBox1.Items.Add('semua');
  ComboBox1.Items.Add('Perbaikan');
  ComboBox1.Items.Add('Perawatan');
  ComboBox1.ItemIndex:=0;
end;

procedure TKegiatanTeknikFm.cxButton1Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
  Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Laporan Kegiatan Teknik.rpt';
  MasterQ.SQL.Clear;

  Crpe1.ParamFields[3].CurrentValue:=ComboBox1.Text;


  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='date';
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay2.Date);
    end
    else
    begin
      Crpe1.ParamFields[2].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

    Crpe1.Execute;
    Crpe1.WindowState:= wsMaximized;
end;

procedure TKegiatanTeknikFm.FormShow(Sender: TObject);
begin
  DateTimePickerDay.DateTime:=today;
  DateTimePickerDay2.DateTime:=today;
end;

end.
