object KeluarMasukBarangBekasFm: TKeluarMasukBarangBekasFm
  Left = 220
  Top = 60
  Width = 1047
  Height = 597
  Caption = 'Keluar Masuk Barang Bekas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 539
    Width = 1031
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object pnl2: TPanel
    Left = 0
    Top = 488
    Width = 1031
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxButton1: TcxButton
      Left = 256
      Top = 10
      Width = 75
      Height = 25
      Caption = 'APPROVE'
      TabOrder = 3
      OnClick = cxButton1Click
    end
    object cxGroupBox1: TcxGroupBox
      Left = 733
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 48
    Align = alTop
    TabOrder = 2
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 364
    Width = 1031
    Height = 124
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1029
      Height = 122
      Align = alClient
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = ViewMasterDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid2DBTableView1Tanggal: TcxGridDBColumn
          DataBinding.FieldName = 'Tanggal'
        end
        object cxGrid2DBTableView1NamaBarangBekas: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarangBekas'
          Options.SortByDisplayText = isbtOn
          Width = 124
        end
        object cxGrid2DBTableView1NamaPenerima: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenerima'
          Options.SortByDisplayText = isbtOn
          Width = 135
        end
        object cxGrid2DBTableView1NamaPenyetuju: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenyetuju'
          Options.SortByDisplayText = isbtOn
          Width = 155
        end
        object cxGrid2DBTableView1Saldo: TcxGridDBColumn
          DataBinding.FieldName = 'Saldo'
        end
        object cxGrid2DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 75
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
        end
        object cxGrid2DBTableView1StatusApproved: TcxGridDBColumn
          DataBinding.FieldName = 'StatusApproved'
          Width = 103
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 401
    Height = 316
    Align = alLeft
    Caption = 'Panel3'
    TabOrder = 4
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 399
      Height = 314
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 165
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = MasterDS
      Version = 1
      object MasterVGridStatus: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Columns = 2
        Properties.EditProperties.Items = <
          item
            Caption = 'MASUK'
            Value = 'MASUK'
          end
          item
            Caption = 'KELUAR'
            Value = 'KELUAR'
          end>
        Properties.DataBinding.FieldName = 'Status'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridTanggal: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tanggal'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridBarangBekas: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1BarangBekasEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'BarangBekas'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridNamaBarangBekas: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaBarangBekas'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 2
        Index = 0
        Version = 1
      end
      object MasterVGridPenerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penerima'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 4
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridNamaPenerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenerima'
        Properties.Options.Editing = False
        ID = 5
        ParentID = 4
        Index = 0
        Version = 1
      end
      object MasterVGridJabatanPenerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPenerima'
        Properties.Options.Editing = False
        ID = 6
        ParentID = 4
        Index = 1
        Version = 1
      end
      object MasterVGridPenyetuju: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penyetuju'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 7
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridNamaPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenyetuju'
        Properties.Options.Editing = False
        ID = 8
        ParentID = 7
        Index = 0
        Version = 1
      end
      object MasterVGridJabatanPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanPenyetuju'
        Properties.Options.Editing = False
        ID = 9
        ParentID = 7
        Index = 1
        Version = 1
      end
      object MasterVGridSaldo: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.DataBinding.FieldName = 'Saldo'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 10
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Height = 47
        Properties.DataBinding.FieldName = 'Keterangan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 11
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridStatusApproved: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'StatusApproved'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 12
        ParentID = -1
        Index = 7
        Version = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 401
    Top = 48
    Width = 630
    Height = 316
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 5
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 628
      Height = 314
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Nama: TcxGridDBColumn
          DataBinding.FieldName = 'Nama'
          Options.SortByDisplayText = isbtOn
          Width = 93
        end
        object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
          DataBinding.FieldName = 'Jumlah'
        end
        object cxGrid1DBTableView1StandardUmur: TcxGridDBColumn
          DataBinding.FieldName = 'StandardUmur'
          Width = 82
        end
        object cxGrid1DBTableView1Lokasi: TcxGridDBColumn
          DataBinding.FieldName = 'Lokasi'
          Width = 87
        end
        object cxGrid1DBTableView1Harga: TcxGridDBColumn
          DataBinding.FieldName = 'Harga'
          Width = 66
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          Caption = 'Ket.'
          DataBinding.FieldName = 'Keterangan'
          Width = 191
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KeluarMasukBarangBekas')
    UpdateObject = MasterUs
    Left = 368
    Top = 16
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBarangBekas: TStringField
      FieldName = 'BarangBekas'
      Required = True
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object MasterQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Size = 10
    end
    object MasterQSaldo: TIntegerField
      FieldName = 'Saldo'
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQNamaBarangBekas: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarangBekas'
      LookupDataSet = BarangBekasQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangBekas'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object MasterQStatusApproved: TStringField
      FieldName = 'StatusApproved'
      Size = 50
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, BarangBekas, Penerima, Penyetuju, Saldo, Keterangan' +
        ', Status, StatusApproved, Tanggal'#13#10'from KeluarMasukBarangBekas'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update KeluarMasukBarangBekas'
      'set'
      '  Kode = :Kode,'
      '  BarangBekas = :BarangBekas,'
      '  Penerima = :Penerima,'
      '  Penyetuju = :Penyetuju,'
      '  Saldo = :Saldo,'
      '  Keterangan = :Keterangan,'
      '  Status = :Status,'
      '  StatusApproved = :StatusApproved,'
      '  Tanggal = :Tanggal'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into KeluarMasukBarangBekas'
      
        '  (Kode, BarangBekas, Penerima, Penyetuju, Saldo, Keterangan, St' +
        'atus, StatusApproved, Tanggal)'
      'values'
      
        '  (:Kode, :BarangBekas, :Penerima, :Penyetuju, :Saldo, :Keterang' +
        'an, :Status, :StatusApproved, :Tanggal)')
    DeleteSQL.Strings = (
      'delete from KeluarMasukBarangBekas'
      'where'
      '  Kode = :OLD_Kode')
    Left = 432
    Top = 16
  end
  object MasterDS: TDataSource
    DataSet = MasterQ
    Left = 400
    Top = 16
  end
  object ViewBBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BarangBekas')
    Left = 544
    Top = 8
    object ViewBBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewBBQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object ViewBBQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object ViewBBQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object ViewBBQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object ViewBBQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object ViewBBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewBBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewBBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewBBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewBBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewBBQ
    Left = 576
    Top = 8
  end
  object ViewMaster: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KeluarMasukBarangBekas'
      
        'where (tanggal >= :text1 and tanggal <= :text2) or tanggal is nu' +
        'll'
      'order by tanggal desc')
    Left = 528
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewMasterKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewMasterBarangBekas: TStringField
      FieldName = 'BarangBekas'
      Required = True
      Size = 10
    end
    object ViewMasterPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object ViewMasterPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Size = 10
    end
    object ViewMasterSaldo: TIntegerField
      FieldName = 'Saldo'
    end
    object ViewMasterKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewMasterNamaBarangBekas: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarangBekas'
      LookupDataSet = BarangBekasQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangBekas'
      Size = 50
      Lookup = True
    end
    object ViewMasterNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object ViewMasterNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object ViewMasterJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object ViewMasterJabatanPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object ViewMasterStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object ViewMasterStatusApproved: TStringField
      FieldName = 'StatusApproved'
      Size = 50
    end
    object ViewMasterTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object ViewMasterDs: TDataSource
    DataSet = ViewMaster
    Left = 568
    Top = 392
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from KeluarMasukBarangBekas order by kode desc')
    Left = 257
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object BarangBekasQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from BarangBekas')
    Left = 656
    Top = 24
    object BarangBekasQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangBekasQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object BarangBekasQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object BarangBekasQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangBekasQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object BarangBekasQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangBekasQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangBekasQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangBekasQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangBekasQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangBekasQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 688
    Top = 24
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KeluarMasukBarangBekas')
    Left = 536
    Top = 264
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'BarangBekas'
      Required = True
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object StringField4: TStringField
      FieldName = 'Penyetuju'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'Saldo'
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object StringField5: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarangBekas'
      LookupDataSet = BarangBekasQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangBekas'
      Size = 50
      Lookup = True
    end
    object StringField6: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object StringField7: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object StringField8: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object StringField9: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object StringField10: TStringField
      FieldName = 'Status'
      Size = 10
    end
  end
end
