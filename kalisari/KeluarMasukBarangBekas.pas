unit KeluarMasukBarangBekas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxControls,
  cxContainer, cxEdit, DB, SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit,
  StdCtrls, cxLabel, cxButtons, ExtCtrls, ComCtrls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxDBData, cxVGrid, cxDBVGrid, cxInplaceContainer, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, cxRadioGroup, cxDropDownEdit, cxCalendar,
  cxGroupBox;

type
  TKeluarMasukBarangBekasFm = class(TForm)
    StatusBar: TStatusBar;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    Panel1: TPanel;
    MasterQ: TSDQuery;
    MasterUs: TSDUpdateSQL;
    MasterDS: TDataSource;
    MasterQKode: TStringField;
    MasterQBarangBekas: TStringField;
    MasterQPenerima: TStringField;
    MasterQPenyetuju: TStringField;
    MasterQSaldo: TIntegerField;
    MasterQKeterangan: TMemoField;
    ViewBBQ: TSDQuery;
    ViewDs: TDataSource;
    ViewBBQKode: TStringField;
    ViewBBQNama: TStringField;
    ViewBBQJumlah: TIntegerField;
    ViewBBQStandardUmur: TIntegerField;
    ViewBBQLokasi: TStringField;
    ViewBBQHarga: TCurrencyField;
    ViewBBQKeterangan: TMemoField;
    ViewBBQCreateDate: TDateTimeField;
    ViewBBQCreateBy: TStringField;
    ViewBBQOperator: TStringField;
    ViewBBQTglEntry: TDateTimeField;
    Panel3: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridBarangBekas: TcxDBEditorRow;
    MasterVGridPenerima: TcxDBEditorRow;
    MasterVGridPenyetuju: TcxDBEditorRow;
    MasterVGridSaldo: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    ViewMaster: TSDQuery;
    ViewMasterDs: TDataSource;
    ViewMasterKode: TStringField;
    ViewMasterBarangBekas: TStringField;
    ViewMasterPenerima: TStringField;
    ViewMasterPenyetuju: TStringField;
    ViewMasterSaldo: TIntegerField;
    ViewMasterKeterangan: TMemoField;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1Saldo: TcxGridDBColumn;
    cxGrid2DBTableView1Keterangan: TcxGridDBColumn;
    BarangBekasQ: TSDQuery;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    BarangBekasQKode: TStringField;
    BarangBekasQNama: TStringField;
    BarangBekasQJumlah: TIntegerField;
    BarangBekasQStandardUmur: TIntegerField;
    BarangBekasQLokasi: TStringField;
    BarangBekasQHarga: TCurrencyField;
    BarangBekasQKeterangan: TMemoField;
    BarangBekasQCreateDate: TDateTimeField;
    BarangBekasQCreateBy: TStringField;
    BarangBekasQOperator: TStringField;
    BarangBekasQTglEntry: TDateTimeField;
    MasterQNamaBarangBekas: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    MasterQNamaPenyetuju: TStringField;
    MasterQJabatanPenyetuju: TStringField;
    MasterVGridNamaBarangBekas: TcxDBEditorRow;
    MasterVGridNamaPenerima: TcxDBEditorRow;
    MasterVGridJabatanPenerima: TcxDBEditorRow;
    MasterVGridNamaPenyetuju: TcxDBEditorRow;
    MasterVGridJabatanPenyetuju: TcxDBEditorRow;
    ViewMasterNamaBarangBekas: TStringField;
    ViewMasterNamaPenerima: TStringField;
    ViewMasterNamaPenyetuju: TStringField;
    ViewMasterJabatanPenerima: TStringField;
    ViewMasterJabatanPenyetuju: TStringField;
    cxGrid2DBTableView1NamaBarangBekas: TcxGridDBColumn;
    cxGrid2DBTableView1NamaPenerima: TcxGridDBColumn;
    cxGrid2DBTableView1NamaPenyetuju: TcxGridDBColumn;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1StandardUmur: TcxGridDBColumn;
    cxGrid1DBTableView1Lokasi: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    MasterQStatus: TStringField;
    ViewMasterStatus: TStringField;
    MasterQStatusApproved: TStringField;
    cxButton1: TcxButton;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    IntegerField1: TIntegerField;
    MemoField1: TMemoField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    ViewMasterStatusApproved: TStringField;
    cxGrid2DBTableView1StatusApproved: TcxGridDBColumn;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridStatusApproved: TcxDBEditorRow;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQTanggal: TDateTimeField;
    MasterVGridTanggal: TcxDBEditorRow;
    ViewMasterTanggal: TDateTimeField;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1BarangBekasEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure MasterVGridSaldoEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KeluarMasukBarangBekasFm: TKeluarMasukBarangBekasFm;
  MasterOriSQL:string;

implementation

uses DM, MenuUtama, DropDown, PegawaiDropDown, BarangBekasDropDown;

{$R *.dfm}

procedure TKeluarMasukBarangBekasFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
release;
end;

procedure TKeluarMasukBarangBekasFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TKeluarMasukBarangBekasFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKeluarMasukBarangBekas.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluarMasukBarangBekas.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKeluarMasukBarangBekas.AsBoolean;
  KodeEditPropertiesButtonClick(sender,0);
  ViewBBQ.Open;
  MasterQ.Edit;
  MasterQStatus.AsString:='MASUK';
  cxDateEdit1.Date:=Now-1;
  cxDateEdit2.Date:=now;
  ViewMaster.Close;
  viewmaster.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewmaster.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
  viewmaster.Open;
end;

procedure TKeluarMasukBarangBekasFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKeluarMasukBarangBekasFm.KodeEditExit(Sender: TObject);
begin
if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterqStatus.asstring:='MASUK';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKeluarMasukBarangBekas.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluarMasukBarangBekas.AsBoolean;
    MasterVGrid.Enabled:=True;
    
  end;
end;

procedure TKeluarMasukBarangBekasFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TKeluarMasukBarangBekasFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TKeluarMasukBarangBekasFm.cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TKeluarMasukBarangBekasFm.cxDBVerticalGrid1PenyetujuEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenyetuju.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TKeluarMasukBarangBekasFm.cxDBVerticalGrid1BarangBekasEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 BarangBekasDropDownFm:=TBarangBekasDropdownfm.Create(Self);
    if BarangBekasDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBarangBekas.AsString:=BarangBekasDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    BarangBekasDropDownFm.Release;
end;

procedure TKeluarMasukBarangBekasFm.SaveBtnClick(Sender: TObject);
begin
  MasterQ.Edit;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      MasterqStatusApproved.asstring:='ON PROCESS';
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;

    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Data Keluar Masuk Barang Bekas dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
    ViewBBQ.Close;
    ViewBBQ.Open;
    ViewMaster.Close;
  ViewMaster.Open;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  KodeEditPropertiesButtonClick(sender,0);
end;

procedure TKeluarMasukBarangBekasFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Data Keluar Masuk Barang Bekas '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Data Keluar Masuk Barang Bekas telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Data Keluar Masuk Barang Bekas pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     ViewBBQ.Close;
      ViewBBQ.Open;
     ViewMaster.Close;
     ViewMaster.Open;
     KodeEdit.SetFocus;
     KodeEditPropertiesButtonClick(sender,0);
  end;
end;

procedure TKeluarMasukBarangBekasFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TKeluarMasukBarangBekasFm.MasterVGridSaldoEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
  var a:integer;
begin
if MasterQStatus.AsString='KELUAR' then
begin
    a:=MasterQSaldo.AsInteger*-1;
    MasterQ.Edit;
    MasterQSaldo.AsInteger:=a;
    showmessage(inttostr(a));
end;
MasterQSaldo.AsInteger:=DisplayValue;
end;

procedure TKeluarMasukBarangBekasFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewMasterKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= viewMasterKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKeluarMasukBarangBekas.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluarMasukBarangBekas.AsBoolean;
    MasterVGrid.Enabled:=True;
end;

procedure TKeluarMasukBarangBekasFm.cxButton1Click(Sender: TObject);
begin
if StatusBar.Panels[0].Text= 'Mode : Edit' then
begin
   try
     SDQuery1.Close;
     SDQuery1.SQL.Text:='Update keluarmasukbarangbekas set statusapproved="APPROVED" where kode="'+kodeedit.Text+'"';
    SDQuery1.ExecSQL;
    ViewMaster.Refresh;
    ViewBBQ.Refresh;
   except
     on E : Exception do begin
    ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
    showmessage('Update Gagal');
  end;
  end;
end;
end;

procedure TKeluarMasukBarangBekasFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
ViewMaster.Close;
  viewmaster.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewmaster.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
  viewmaster.Open;
end;

procedure TKeluarMasukBarangBekasFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
ViewMaster.Close;
  viewmaster.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewmaster.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
  viewmaster.Open;
end;

end.
