object KeluhanPelangganFm: TKeluhanPelangganFm
  Left = 153
  Top = 60
  BorderStyle = bsDialog
  Caption = 'Keluhan Pelanggan'
  ClientHeight = 602
  ClientWidth = 809
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 809
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 532
    Width = 809
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 188
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object deleteBtn: TcxButton
      Left = 100
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = deleteBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 720
      Top = 18
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object cxGroupBox1: TcxGroupBox
      Left = 511
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel2: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 393
    Height = 272
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 166
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPelangganEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pelanggan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNamaPT: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPelanggan'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGridSuratJalan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridSuratJalanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'SuratJalan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Armada'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 3
      Index = 0
      Version = 1
    end
    object MasterVGridPlatNo: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo'
      Properties.Options.Editing = False
      ID = 5
      ParentID = 4
      Index = 0
      Version = 1
    end
    object MasterVGridSopir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Sopir'
      Properties.Options.Editing = False
      ID = 6
      ParentID = 3
      Index = 1
      Version = 1
    end
    object MasterVGridNamaPengemudi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPengemudi'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 583
    Width = 809
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 393
    Top = 48
    Width = 416
    Height = 272
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 202
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.FocusFirstCellOnNewRecord = True
    ParentFont = False
    TabOrder = 4
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBVerticalGrid1TglKejadian: TcxDBEditorRow
      Properties.Caption = 'TglKejadian*'
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.DataBinding.FieldName = 'TglKejadian'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1LokasiKejadian: TcxDBEditorRow
      Properties.Caption = 'LokasiKejadian*'
      Properties.DataBinding.FieldName = 'LokasiKejadian'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Perihal: TcxDBEditorRow
      Height = 90
      Properties.Caption = 'Perihal*'
      Properties.EditPropertiesClassName = 'TcxMemoProperties'
      Properties.DataBinding.FieldName = 'Perihal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1Analisa: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Analisa'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1TindakanPerbaikan: TcxDBEditorRow
      Height = 70
      Properties.EditPropertiesClassName = 'TcxMemoProperties'
      Properties.DataBinding.FieldName = 'TindakanPerbaikan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1PelakuPerbaikan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PelakuPerbaikanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PelakuPerbaikan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1NamaPelakuPerbaikan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPelakuPerbaikan'
      Properties.Options.Editing = False
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1JabatanPelakuPerbaikan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'JabatanPelakuPerbaikan'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 5
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1TglPerbaikan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.DataBinding.FieldName = 'TglPerbaikan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1Verifikasi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Verifikasi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1Verifikator: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1VerifikatorEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Verifikator'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1NamaVerifikator: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaVerifikator'
      Properties.Options.Editing = False
      ID = 11
      ParentID = 10
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1JabatanVerifikator: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'JabatanVerifikator'
      Properties.Options.Editing = False
      ID = 12
      ParentID = 10
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1TglVerifikasi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.DataBinding.FieldName = 'TglVerifikasi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1Pencegahan: TcxDBEditorRow
      Height = 70
      Properties.EditPropertiesClassName = 'TcxMemoProperties'
      Properties.DataBinding.FieldName = 'Pencegahan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1PelakuPencegahan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PelakuPencegahanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PelakuPencegahan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1NamaPelakuPencegahan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPelakuPencegahan'
      Properties.Options.Editing = False
      ID = 16
      ParentID = 15
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1JabatanPelakuPencegahan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'JabatanPelakuPencegahan'
      Properties.Options.Editing = False
      ID = 17
      ParentID = 15
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1TglPencegahan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.DataBinding.FieldName = 'TglPencegahan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 18
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1Status: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.DefaultValue = 'ON PROGRESS'
      Properties.EditProperties.Items = <
        item
          Caption = 'ON PROGRESS'
          Value = 'ON PROGRESS'
        end
        item
          Caption = 'FINISHED'
          Value = 'FINISHED'
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 19
      ParentID = -1
      Index = 13
      Version = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 320
    Width = 809
    Height = 212
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 88
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object cxGrid1DBTableView1SuratJalan: TcxGridDBColumn
        DataBinding.FieldName = 'SuratJalan'
        Width = 81
      end
      object cxGrid1DBTableView1Perihal: TcxGridDBColumn
        DataBinding.FieldName = 'Perihal'
        Width = 154
      end
      object cxGrid1DBTableView1namaPT: TcxGridDBColumn
        Caption = 'NamaPT'
        DataBinding.FieldName = 'namaPT'
        Options.SortByDisplayText = isbtOn
        Width = 234
      end
      object cxGrid1DBTableView1nama_sopir: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPengemudi'
        Width = 117
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 130
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select *, '#39#39' as NamaPelanggan, '#39#39' as Alamat, '#39#39' as PlatNo, '#39#39' as' +
        ' NamaPengemudi, '#39#39' as NamaPelakuPerbaikan, '#39#39' as JabatanPelakuPe' +
        'rbaikan, '#39#39' as NamaVerifikator, '#39#39' as JabatanVerifikator, '#39#39' as ' +
        'NamaPelakuPencegahan, '#39#39' as JabatanPelakuPencegahan'
      'from KeluhanPelanggan')
    UpdateObject = MasterUS
    Left = 257
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object MasterQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Required = True
      Size = 10
    end
    object MasterQTglKejadian: TDateTimeField
      FieldName = 'TglKejadian'
      Required = True
    end
    object MasterQLokasiKejadian: TStringField
      FieldName = 'LokasiKejadian'
      Required = True
      Size = 50
    end
    object MasterQPerihal: TMemoField
      FieldName = 'Perihal'
      Required = True
      BlobType = ftMemo
    end
    object MasterQAnalisa: TMemoField
      FieldName = 'Analisa'
      BlobType = ftMemo
    end
    object MasterQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object MasterQPelakuPerbaikan: TStringField
      FieldName = 'PelakuPerbaikan'
      Size = 10
    end
    object MasterQTglPerbaikan: TDateTimeField
      FieldName = 'TglPerbaikan'
    end
    object MasterQVerifikasi: TMemoField
      FieldName = 'Verifikasi'
      BlobType = ftMemo
    end
    object MasterQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object MasterQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object MasterQPencegahan: TMemoField
      FieldName = 'Pencegahan'
      BlobType = ftMemo
    end
    object MasterQPelakuPencegahan: TStringField
      FieldName = 'PelakuPencegahan'
      Size = 10
    end
    object MasterQTglPencegahan: TDateTimeField
      FieldName = 'TglPencegahan'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Required = True
      Size = 100
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 100
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
    end
    object MasterQNamaPengemudi: TStringField
      FieldName = 'NamaPengemudi'
      Required = True
      Size = 50
    end
    object MasterQNamaPelakuPerbaikan: TStringField
      FieldName = 'NamaPelakuPerbaikan'
      Required = True
      Size = 50
    end
    object MasterQNamaVerifikator: TStringField
      FieldName = 'NamaVerifikator'
      Required = True
      Size = 50
    end
    object MasterQJabatanVerifikator: TStringField
      FieldName = 'JabatanVerifikator'
      Required = True
      Size = 30
    end
    object MasterQNamaPelakuPencegahan: TStringField
      FieldName = 'NamaPelakuPencegahan'
      Required = True
      Size = 50
    end
    object MasterQJabatanPelakuPencegahan: TStringField
      FieldName = 'JabatanPelakuPencegahan'
      Required = True
      Size = 30
    end
    object MasterQJabatanPelakuPerbaikan: TStringField
      FieldName = 'JabatanPelakuPerbaikan'
      Required = True
      Size = 1
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 340
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Pelanggan, Armada, Sopir, SuratJalan, TglKejadian, ' +
        'LokasiKejadian, Perihal, Analisa, TindakanPerbaikan, PelakuPerba' +
        'ikan, TglPerbaikan, Verifikasi, Verifikator, TglVerifikasi, Penc' +
        'egahan, PelakuPencegahan, TglPencegahan, Status, CreateDate, Cre' +
        'ateBy, Operator, TglEntry, TglCetak'#13#10'from KeluhanPelanggan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update KeluhanPelanggan'
      'set'
      '  Kode = :Kode,'
      '  Pelanggan = :Pelanggan,'
      '  Armada = :Armada,'
      '  Sopir = :Sopir,'
      '  SuratJalan = :SuratJalan,'
      '  TglKejadian = :TglKejadian,'
      '  LokasiKejadian = :LokasiKejadian,'
      '  Perihal = :Perihal,'
      '  Analisa = :Analisa,'
      '  TindakanPerbaikan = :TindakanPerbaikan,'
      '  PelakuPerbaikan = :PelakuPerbaikan,'
      '  TglPerbaikan = :TglPerbaikan,'
      '  Verifikasi = :Verifikasi,'
      '  Verifikator = :Verifikator,'
      '  TglVerifikasi = :TglVerifikasi,'
      '  Pencegahan = :Pencegahan,'
      '  PelakuPencegahan = :PelakuPencegahan,'
      '  TglPencegahan = :TglPencegahan,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into KeluhanPelanggan'
      
        '  (Kode, Pelanggan, Armada, Sopir, SuratJalan, TglKejadian, Loka' +
        'siKejadian, Perihal, Analisa, TindakanPerbaikan, PelakuPerbaikan' +
        ', TglPerbaikan, Verifikasi, Verifikator, TglVerifikasi, Pencegah' +
        'an, PelakuPencegahan, TglPencegahan, Status, CreateDate, CreateB' +
        'y, Operator, TglEntry, TglCetak)'
      'values'
      
        '  (:Kode, :Pelanggan, :Armada, :Sopir, :SuratJalan, :TglKejadian' +
        ', :LokasiKejadian, :Perihal, :Analisa, :TindakanPerbaikan, :Pela' +
        'kuPerbaikan, :TglPerbaikan, :Verifikasi, :Verifikator, :TglVerif' +
        'ikasi, :Pencegahan, :PelakuPencegahan, :TglPencegahan, :Status, ' +
        ':CreateDate, :CreateBy, :Operator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from KeluhanPelanggan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 380
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from keluhanpelanggan order by kode desc')
    Left = 297
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterso where kodenota=:text')
    Left = 425
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Armada where kode=:text')
    Left = 465
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan where kode=:text')
    Left = 513
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
  end
  object sjQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from MasterSJ where kodenota=:text'
      '')
    Left = 609
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object sjQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Visible = False
      Size = 10
    end
    object sjQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object sjQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 10
    end
    object sjQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object sjQSopir2: TStringField
      FieldName = 'Sopir2'
      Size = 10
    end
    object sjQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object sjQTitipKwitansi: TBooleanField
      FieldName = 'TitipKwitansi'
    end
    object sjQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object sjQKir: TBooleanField
      FieldName = 'Kir'
      Required = True
    end
    object sjQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object sjQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object sjQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object sjQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object sjQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object sjQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object sjQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object sjQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object sjQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object sjQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object sjQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object sjQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object sjQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object sjQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object sjQLaporan: TMemoField
      FieldName = 'Laporan'
      BlobType = ftMemo
    end
    object sjQTglRealisasi: TDateTimeField
      FieldName = 'TglRealisasi'
    end
    object sjQAwal: TStringField
      FieldName = 'Awal'
      Size = 10
    end
    object sjQAkhir: TStringField
      FieldName = 'Akhir'
      Size = 10
    end
    object sjQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object sjQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object sjQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object sjQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object sjQPremiSopir2: TCurrencyField
      FieldName = 'PremiSopir2'
    end
    object sjQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object sjQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object sjQTabunganSopir2: TCurrencyField
      FieldName = 'TabunganSopir2'
    end
    object sjQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object sjQUangJalan: TCurrencyField
      FieldName = 'UangJalan'
    end
    object sjQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
      LookupDataSet = KodeQ
    end
    object sjQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object sjQOther: TCurrencyField
      FieldName = 'Other'
    end
    object sjQNominalKwitansi: TCurrencyField
      FieldName = 'NominalKwitansi'
    end
    object sjQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object sjQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object sjQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object sjQKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Pelanggan'
      KeyFields = 'NoSO'
      Size = 10
      Lookup = True
    end
    object sjQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object sjQKeteranganBiayaLainLain: TStringField
      FieldName = 'KeteranganBiayaLainLain'
      Size = 50
    end
    object sjQUangInap: TCurrencyField
      FieldName = 'UangInap'
    end
    object sjQUangParkir: TCurrencyField
      FieldName = 'UangParkir'
    end
    object sjQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object sjQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object sjQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object sjQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object sjQSudahPrint: TBooleanField
      FieldName = 'SudahPrint'
    end
    object sjQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object sjQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object sjQDanaKebersihan: TCurrencyField
      FieldName = 'DanaKebersihan'
    end
  end
  object DataSource1: TDataSource
    DataSet = viewQuery
    Left = 680
    Top = 8
  end
  object viewQuery: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select cast(kp.createdate as date) as Tanggal, kp.*,p.namaPT fro' +
        'm KeluhanPelanggan kp left join pelanggan p'
      'on kp.Pelanggan=p.kode'
      'where kp.createdate >= :text1 and kp.createdate <= :text2'
      'order by tglentry desc')
    Left = 720
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewQueryKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewQuerySuratJalan: TStringField
      FieldName = 'SuratJalan'
      Required = True
      Size = 10
    end
    object viewQueryTglKejadian: TDateTimeField
      FieldName = 'TglKejadian'
      Required = True
    end
    object viewQueryLokasiKejadian: TStringField
      FieldName = 'LokasiKejadian'
      Required = True
      Size = 50
    end
    object viewQueryPerihal: TMemoField
      FieldName = 'Perihal'
      Required = True
      BlobType = ftMemo
    end
    object viewQueryAnalisa: TMemoField
      FieldName = 'Analisa'
      BlobType = ftMemo
    end
    object viewQueryTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object viewQueryPelakuPerbaikan: TStringField
      FieldName = 'PelakuPerbaikan'
      Size = 10
    end
    object viewQueryTglPerbaikan: TDateTimeField
      FieldName = 'TglPerbaikan'
    end
    object viewQueryVerifikasi: TMemoField
      FieldName = 'Verifikasi'
      BlobType = ftMemo
    end
    object viewQueryVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object viewQueryTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object viewQueryPencegahan: TMemoField
      FieldName = 'Pencegahan'
      BlobType = ftMemo
    end
    object viewQueryPelakuPencegahan: TStringField
      FieldName = 'PelakuPencegahan'
      Size = 10
    end
    object viewQueryTglPencegahan: TDateTimeField
      FieldName = 'TglPencegahan'
    end
    object viewQueryStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object viewQueryCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewQueryCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewQueryOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewQueryTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewQueryTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewQueryKodeNoSO: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeNoSO'
      LookupDataSet = sjQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'NoSO'
      KeyFields = 'SuratJalan'
      Size = 10
      Lookup = True
    end
    object viewQueryKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Pelanggan'
      KeyFields = 'KodeNoSO'
      Size = 10
      Lookup = True
    end
    object viewQueryKodePengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePengemudi'
      LookupDataSet = sjQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Sopir'
      KeyFields = 'SuratJalan'
      Size = 10
      Lookup = True
    end
    object viewQueryNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodePengemudi'
      Size = 50
      Lookup = True
    end
    object viewQueryPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object viewQueryArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object viewQuerySopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object viewQuerynamaPT: TStringField
      FieldName = 'namaPT'
      Size = 100
    end
    object viewQueryTanggal: TStringField
      FieldName = 'Tanggal'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai where kode=:text')
    Left = 648
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
