unit KeluhanPelanggan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, cxMemo,
  dxSkinscxPCPainter, cxDropDownEdit, cxGroupBox;

type
  TKeluhanPelangganFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQHarga: TCurrencyField;
    SOQRute: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQStatus: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    sjQ: TSDQuery;
    KodeQkode: TStringField;
    cxDBVerticalGrid1TglKejadian: TcxDBEditorRow;
    cxDBVerticalGrid1LokasiKejadian: TcxDBEditorRow;
    cxDBVerticalGrid1Perihal: TcxDBEditorRow;
    cxDBVerticalGrid1Analisa: TcxDBEditorRow;
    cxDBVerticalGrid1TindakanPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1PelakuPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1TglPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1Verifikasi: TcxDBEditorRow;
    cxDBVerticalGrid1Verifikator: TcxDBEditorRow;
    cxDBVerticalGrid1TglVerifikasi: TcxDBEditorRow;
    cxDBVerticalGrid1Pencegahan: TcxDBEditorRow;
    cxDBVerticalGrid1PelakuPencegahan: TcxDBEditorRow;
    cxDBVerticalGrid1TglPencegahan: TcxDBEditorRow;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    viewQuery: TSDQuery;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1SuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1Perihal: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1namaPT: TcxGridDBColumn;
    cxGrid1DBTableView1nama_sopir: TcxGridDBColumn;
    deleteBtn: TcxButton;
    cxLabel1: TcxLabel;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    sjQKodenota: TStringField;
    sjQTgl: TDateTimeField;
    sjQNoSO: TStringField;
    sjQSopir: TStringField;
    sjQSopir2: TStringField;
    sjQCrew: TStringField;
    sjQTitipKwitansi: TBooleanField;
    sjQNoKwitansi: TStringField;
    sjQKir: TBooleanField;
    sjQSTNK: TBooleanField;
    sjQPajak: TBooleanField;
    sjQTglKembali: TDateTimeField;
    sjQSPBUAYaniLiter: TFloatField;
    sjQSPBUAYaniUang: TCurrencyField;
    sjQSPBULuarLiter: TFloatField;
    sjQSPBULuarUang: TCurrencyField;
    sjQSPBULuarUangDiberi: TCurrencyField;
    sjQSPBULuarDetail: TMemoField;
    sjQStatus: TStringField;
    sjQCreateDate: TDateTimeField;
    sjQCreateBy: TStringField;
    sjQOperator: TStringField;
    sjQTglEntry: TDateTimeField;
    sjQLaporan: TMemoField;
    sjQTglRealisasi: TDateTimeField;
    sjQAwal: TStringField;
    sjQAkhir: TStringField;
    sjQTglCetak: TDateTimeField;
    sjQClaimSopir: TCurrencyField;
    sjQKeteranganClaimSopir: TStringField;
    sjQPremiSopir: TCurrencyField;
    sjQPremiSopir2: TCurrencyField;
    sjQPremiKernet: TCurrencyField;
    sjQTabunganSopir: TCurrencyField;
    sjQTabunganSopir2: TCurrencyField;
    sjQTol: TCurrencyField;
    sjQUangJalan: TCurrencyField;
    sjQBiayaLainLain: TCurrencyField;
    sjQUangMakan: TCurrencyField;
    sjQOther: TCurrencyField;
    viewQueryKode: TStringField;
    viewQuerySuratJalan: TStringField;
    viewQueryTglKejadian: TDateTimeField;
    viewQueryLokasiKejadian: TStringField;
    viewQueryPerihal: TMemoField;
    viewQueryAnalisa: TMemoField;
    viewQueryTindakanPerbaikan: TMemoField;
    viewQueryPelakuPerbaikan: TStringField;
    viewQueryTglPerbaikan: TDateTimeField;
    viewQueryVerifikasi: TMemoField;
    viewQueryVerifikator: TStringField;
    viewQueryTglVerifikasi: TDateTimeField;
    viewQueryPencegahan: TMemoField;
    viewQueryPelakuPencegahan: TStringField;
    viewQueryTglPencegahan: TDateTimeField;
    viewQueryStatus: TStringField;
    viewQueryCreateDate: TDateTimeField;
    viewQueryCreateBy: TStringField;
    viewQueryOperator: TStringField;
    viewQueryTglEntry: TDateTimeField;
    viewQueryTglCetak: TDateTimeField;
    cxDBVerticalGrid1NamaPelakuPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPelakuPerbaikan: TcxDBEditorRow;
    cxDBVerticalGrid1NamaVerifikator: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanVerifikator: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPelakuPencegahan: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPelakuPencegahan: TcxDBEditorRow;
    viewQueryKodeNoSO: TStringField;
    viewQueryKodePelanggan: TStringField;
    viewQueryKodePengemudi: TStringField;
    viewQueryNamaPengemudi: TStringField;
    sjQNominalKwitansi: TCurrencyField;
    sjQPendapatan: TCurrencyField;
    sjQPengeluaran: TCurrencyField;
    sjQSisaDisetor: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    sjQKodePelanggan: TStringField;
    sjQNamaPelanggan: TStringField;
    sjQKeteranganBiayaLainLain: TStringField;
    sjQUangInap: TCurrencyField;
    sjQUangParkir: TCurrencyField;
    sjQKeterangan: TMemoField;
    viewQueryPelanggan: TStringField;
    viewQueryArmada: TStringField;
    viewQuerySopir: TStringField;
    viewQuerynamaPT: TStringField;
    viewQueryTanggal: TStringField;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel2: TcxLabel;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridSopir: TcxDBEditorRow;
    MasterVGridSuratJalan: TcxDBEditorRow;
    MasterVGridNamaPT: TcxDBEditorRow;
    MasterVGridAlamat: TcxDBEditorRow;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridNamaPengemudi: TcxDBEditorRow;
    MasterQKode: TStringField;
    MasterQPelanggan: TStringField;
    MasterQArmada: TStringField;
    MasterQSopir: TStringField;
    MasterQSuratJalan: TStringField;
    MasterQTglKejadian: TDateTimeField;
    MasterQLokasiKejadian: TStringField;
    MasterQPerihal: TMemoField;
    MasterQAnalisa: TMemoField;
    MasterQTindakanPerbaikan: TMemoField;
    MasterQPelakuPerbaikan: TStringField;
    MasterQTglPerbaikan: TDateTimeField;
    MasterQVerifikasi: TMemoField;
    MasterQVerifikator: TStringField;
    MasterQTglVerifikasi: TDateTimeField;
    MasterQPencegahan: TMemoField;
    MasterQPelakuPencegahan: TStringField;
    MasterQTglPencegahan: TDateTimeField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamat: TStringField;
    MasterQPlatNo: TStringField;
    MasterQNamaPengemudi: TStringField;
    MasterQNamaPelakuPerbaikan: TStringField;
    MasterQNamaVerifikator: TStringField;
    MasterQJabatanVerifikator: TStringField;
    MasterQNamaPelakuPencegahan: TStringField;
    MasterQJabatanPelakuPencegahan: TStringField;
    MasterQJabatanPelakuPerbaikan: TStringField;
    sjQSPBUAYaniJam: TDateTimeField;
    sjQSPBUAYaniDetail: TStringField;
    sjQTitipTagihan: TBooleanField;
    sjQSudahPrint: TBooleanField;
    sjQVerifikasi: TCurrencyField;
    sjQKeteranganVerifikasi: TMemoField;
    sjQDanaKebersihan: TCurrencyField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSuratJalanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid1PelakuPerbaikanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1VerifikatorEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PelakuPencegahanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  KeluhanPelangganFm: TKeluhanPelangganFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, SJDropDown, PegawaiDropDown,
  PelangganDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TKeluhanPelangganFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TKeluhanPelangganFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TKeluhanPelangganFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TKeluhanPelangganFm.ExitBtnClick(Sender: TObject);
begin
  Close;
  Release;
end;

procedure TKeluhanPelangganFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  //KodeEdit.Properties.MaxLength:=MasterQKodenota.Size;
end;

procedure TKeluhanPelangganFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
//  KodeEditExit(sender);
  MasterVGrid.Enabled:=True;
  MasterVGrid.SetFocus;
  cxDBVerticalGrid1.Enabled:=True;
end;

procedure TKeluhanPelangganFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKeluhanPelangganFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKeluhanPelanggan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluhanPelanggan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKeluhanPelanggan.AsBoolean;
  ArmadaQ.Open;
  PegawaiQ.Open;
  PelangganQ.Open;
  Soq.Open;
  sjQ.Open;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-3;
  viewQuery.Close;
  viewQuery.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQuery.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewQuery.Open;
//  cxButtonEdit1PropertiesButtonClick(Sender,0);
end;

procedure TKeluhanPelangganFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKeluhanPelanggan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluhanPelanggan.AsBoolean;
  end;
end;

procedure TKeluhanPelangganFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TKeluhanPelangganFm.SaveBtnClick(Sender: TObject);
begin
//if (cxDBVerticalGrid1Verifikasi.Properties.Value<>null) and (cxDBVerticalGrid1Verifikator.Properties.Value<>null) and (cxDBVerticalGrid1TglVerifikasi.Properties.Value<>null) then
//begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQStatus.AsString :='ON PROGRESS';
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Keluhan Pelanggan telah disimpan');
    KodeEdit.SetFocus;
    viewQuery.Close;
    viewQuery.Open;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
//end
//else
//begin
//  ShowMessage('Field Verifikasi, Verifikator dan TglVerifikasi Harus Diisi');
//end;
end;

procedure TKeluhanPelangganFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TKeluhanPelangganFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TKeluhanPelangganFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Keluhan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Keluhan telah dihapus.',mtInformation,[mbOK],0);
       viewQuery.Close;
       viewQuery.Open;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Keluhan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TKeluhanPelangganFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKeluhanPelangganFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQKodenota.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKeluhanPelangganFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:='';
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQKodenota.AsString;
      PelangganQ.ParamByName('text').AsString:=SOQPelanggan.AsString;
      PelangganQ.Open;
      PelangganQ.Close;
      ArmadaQ.ParamByName('text').AsString:=SOQArmada.AsString;
      ArmadaQ.Open;
      ArmadaQ.Close;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKeluhanPelangganFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PelangganDropDownFm:=TPelangganDropdownfm.Create(Self);
  if PelangganDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=PelangganDropDownFm.kode;
    PelangganQ.Open;
    MasterQPelanggan.AsString:=PelangganQKode.AsString;
    MasterQNamaPelanggan.AsString:=PelangganQNamaPT.AsString;
    MasterQAlamat.AsString:=PelangganQAlamat.AsString;
  end;
  PelangganDropDownFm.Release;
end;

procedure TKeluhanPelangganFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQArmada.AsString:=ArmadaQKode.AsString;

    end;
    DropDownFm.Release;
end;


procedure TKeluhanPelangganFm.MasterVGridSuratJalanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SJDropDownFm:=TSJDropdownfm.Create(Self,MasterQPelanggan.AsString);
  if SJDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    sjQ.Close;
    sjQ.ParamByName('text').AsString:=SJDropDownFm.kode;
    sjQ.Open;
    MasterQSuratJalan.AsString:=sjQKodenota.AsString;
    MasterQSopir.AsString:=sjQSopir.AsString;
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=sjQNoSO.AsString;
    SOQ.Open;
    MasterQArmada.AsString:=SOQArmada.AsString;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=SOQArmada.AsString;
    ArmadaQ.Open;
    MasterQPlatNo.AsString:=ArmadaQPlatNo.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopir.AsString;
    PegawaiQ.Open;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  end;
  SJDropDownFm.Release;
end;

procedure TKeluhanPelangganFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  KodeEdit.Text:=viewQueryKode.AsString;
  KodeEditExit(self);
  PelangganQ.Close;
  PelangganQ.ParamByName('text').AsString:=MasterQPelanggan.AsString;
  PelangganQ.Open;
  MasterQNamaPelanggan.AsString:=PelangganQNamaPT.AsString;
  MasterQAlamat.AsString:=PelangganQAlamat.AsString;
  sjQ.Close;
  sjQ.ParamByName('text').AsString:=MasterQSuratJalan.AsString;
  sjQ.Open;
  MasterQSopir.AsString:=sjQSopir.AsString;
  SOQ.Close;
  SOQ.ParamByName('text').AsString:=sjQNoSO.AsString;
  SOQ.Open;
  MasterQArmada.AsString:=SOQArmada.AsString;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:=SOQArmada.AsString;
  ArmadaQ.Open;
  MasterQPlatNo.AsString:=ArmadaQPlatNo.AsString;
  PegawaiQ.Close;
  PegawaiQ.ParamByName('text').AsString:=MasterQSopir.AsString;
  PegawaiQ.Open;
  MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  PegawaiQ.Close;
  PegawaiQ.ParamByName('text').AsString:=MasterQPelakuPerbaikan.AsString;
  PegawaiQ.Open;
  MasterQNamaPelakuPerbaikan.AsString:=PegawaiQNama.AsString;
  MasterQJabatanPelakuPerbaikan.AsString:=PegawaiQJabatan.AsString;
  PegawaiQ.Close;
  PegawaiQ.ParamByName('text').AsString:=MasterQVerifikator.AsString;
  PegawaiQ.Open;
  MasterQNamaVerifikator.AsString:=PegawaiQNama.AsString;
  MasterQJabatanVerifikator.AsString:=PegawaiQJabatan.AsString;
  PegawaiQ.Close;
  PegawaiQ.ParamByName('text').AsString:=MasterQPelakuPencegahan.AsString;
  PegawaiQ.Open;
  MasterQNamaPelakuPencegahan.AsString:=PegawaiQNama.AsString;
  MasterQJabatanPelakuPencegahan.AsString:=PegawaiQJabatan.AsString;

  SaveBtn.Enabled:=menuutamafm.UserQUpdateKeluhanPelanggan.AsBoolean;
  MasterVGrid.Enabled:=True;
  cxDBVerticalGrid1.Enabled:=true;
  KodeEdit.Text:=MasterQKode.AsString;
end;


procedure TKeluhanPelangganFm.cxDBVerticalGrid1PelakuPerbaikanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
      PegawaiQ.Open;
      MasterQPelakuPerbaikan.AsString:=PegawaiDropDownFm.kode;
      MasterQNamaPelakuPerbaikan.AsString:=PegawaiQNama.AsString;
      MasterQJabatanPelakuPerbaikan.AsString:=PegawaiQJabatan.AsString;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TKeluhanPelangganFm.cxDBVerticalGrid1VerifikatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
      PegawaiQ.Open;
      MasterQVerifikator.AsString:=PegawaiDropDownFm.kode;
      MasterQNamaVerifikator.AsString:=PegawaiQNama.AsString;
      MasterQJabatanVerifikator.AsString:=PegawaiQJabatan.AsString;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TKeluhanPelangganFm.cxDBVerticalGrid1PelakuPencegahanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
      PegawaiQ.Open;
      MasterQPelakuPencegahan.AsString:=PegawaiDropDownFm.kode;
      MasterQNamaPelakuPencegahan.AsString:=PegawaiQNama.AsString;
      MasterQJabatanPelakuPencegahan.AsString:=PegawaiQJabatan.AsString;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TKeluhanPelangganFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewQuery.Close;
  viewQuery.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQuery.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewQuery.Open;
end;

procedure TKeluhanPelangganFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewQuery.Close;
  viewQuery.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQuery.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewQuery.Open;
end;

end.
