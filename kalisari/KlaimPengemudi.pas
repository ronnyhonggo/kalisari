unit KlaimPengemudi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxControls,
  cxContainer, cxEdit, cxStyles, cxVGrid, cxDBVGrid, cxInplaceContainer,
  DB, SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit, StdCtrls, cxLabel,
  cxButtons, ExtCtrls, ComCtrls, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxCalendar, cxGroupBox,
  cxCurrencyEdit, UCrpeClasses, UCrpe32;

type
  TKlaimPengemudiFm = class(TForm)
    StatusBar: TStatusBar;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    cxLabel1: TcxLabel;
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    MasterQ: TSDQuery;
    MasterUs: TSDUpdateSQL;
    MAsterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DetailKlaimPengemudiQ: TSDQuery;
    DetailPORebuildDs: TDataSource;
    DetailPORebuildUs: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQKategori: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    KodeDetailQ: TSDQuery;
    KodePORQ: TSDQuery;
    DeletePORQ: TSDQuery;
    SDQuery1: TSDQuery;
    DeleteDBKBUpd: TSDUpdateSQL;
    SDQuery1Kode: TStringField;
    SDQuery1PORebuild: TStringField;
    SDQuery1Kegiatan: TMemoField;
    SDQuery1Harga: TCurrencyField;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    ViewKlaimPengemudiQ: TSDQuery;
    DataSource1: TDataSource;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQSingleSupplier: TBooleanField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQPengemudi: TStringField;
    MasterQGrandTotal: TCurrencyField;
    MasterQKeterangan: TMemoField;
    ViewKlaimPengemudiQTgl: TStringField;
    ViewKlaimPengemudiQKode: TStringField;
    ViewKlaimPengemudiQTanggal: TDateTimeField;
    ViewKlaimPengemudiQArmada: TStringField;
    ViewKlaimPengemudiQPengemudi: TStringField;
    ViewKlaimPengemudiQGrandTotal: TCurrencyField;
    ViewKlaimPengemudiQKeterangan: TMemoField;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridPengemudi: TcxDBEditorRow;
    MasterVGridGrandTotal: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterQPlatNo: TStringField;
    MasterQNoBody: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPengemudi: TStringField;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridNoBody: TcxDBEditorRow;
    MasterVGridNamaPengemudi: TcxDBEditorRow;
    KodeDetailQkodeDetail: TStringField;
    KodePORQkodeDetail: TStringField;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView1Total: TcxGridDBColumn;
    ViewKlaimPengemudiQPlatNo: TStringField;
    ViewKlaimPengemudiQNoBody: TStringField;
    ViewKlaimPengemudiQNamaPengemudi: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid2DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid2DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid2DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid2DBTableView1NoBody: TcxGridDBColumn;
    cxGrid2DBTableView1NamaPengemudi: TcxGridDBColumn;
    DeletePORQKodeKlaim: TStringField;
    DeletePORQKodeDetail: TStringField;
    DeletePORQKeterangan: TMemoField;
    DeletePORQTotal: TCurrencyField;
    DetailKlaimPengemudiQKodeKlaim: TStringField;
    DetailKlaimPengemudiQKodeDetail: TStringField;
    DetailKlaimPengemudiQKeterangan: TMemoField;
    DetailKlaimPengemudiQTotal: TCurrencyField;
    PrintBtn: TcxButton;
    Crpe1: TCrpe;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRebuildEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSupplierEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPengemudiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1TotalPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure PrintBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KlaimPengemudiFm: TKlaimPengemudiFm;
  MAsterOriSQL,DetailPORebuildOriSQL:string;
  IsiBaru:integer;
implementation

uses RebuildDropDown, DM, MenuUtama, DropDown, SupplierDropDown,
  ArmadaDropDown, PengemudiDropDown,StrUtils;

{$R *.dfm}

procedure TKlaimPengemudiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TKlaimPengemudiFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailPORebuildOriSQL:=DetailKlaimPengemudiQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TKlaimPengemudiFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPOUmum.AsBoolean;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewKlaimPengemudiQ.Close;
  ViewKlaimPengemudiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKlaimPengemudiQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewKlaimPengemudiQ.Open;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeletePOUmum.AsBoolean;
  BarangQ.Open;
  RebuildQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  ViewKlaimPengemudiQ.Open;
end;

procedure TKlaimPengemudiFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKlaimPengemudiFm.KodeEditExit(Sender: TObject);
begin
 if KodeEdit.Text<>'' then
  begin
    Masterq.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    DetailKlaimPengemudiQ.Close;
    DetailKlaimPengemudiQ.SQL.Text:=DetailPORebuildOriSQL;
    DetailKlaimPengemudiQ.ParamByName('text').AsString:=KodeEdit.Text;
    DetailKlaimPengemudiQ.Open;
    DetailKlaimPengemudiQ.Close;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQGrandTotal.AsCurrency:=0;
      DetailKlaimPengemudiQ.Open;
      DetailKlaimPengemudiQ.Append;
      DetailKlaimPengemudiQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DetailKlaimPengemudiQ.Open;
      DetailKlaimPengemudiQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeletePOUmum.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TKlaimPengemudiFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TKlaimPengemudiFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  MasterVGrid.Enabled:=True;
  IsiBaru:=1;
  KodeEditExit(sender);
end;

procedure TKlaimPengemudiFm.MasterVGridRebuildEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{ RebuildDropDownFm:=TRebuildDropdownfm.Create(Self);
    if RebuildDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRebuild.AsString:=RebuildDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    RebuildDropDownFm.Release; }
end;

procedure TKlaimPengemudiFm.MasterVGridSupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{ SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
    if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSupplier.AsString:=SupplierDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    SupplierDropDownFm.Release; }
end;

procedure TKlaimPengemudiFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
  var GrandTot:currency;
begin
if abuttonindex=10 then
  begin
  GrandTot:=0;
  DetailKlaimPengemudiQ.Edit;
  DetailKlaimPengemudiQKodeKlaim.AsString:=KodeEdit.Text;
  DetailKlaimPengemudiQKodeDetail.AsString:=DetailKlaimPengemudiQKodeDetail.AsString;
  DetailKlaimPengemudiQ.Post;

  DetailKlaimPengemudiQ.Edit;
  DetailKlaimPengemudiQ.First;
  while DetailKlaimPengemudiQ.Eof=false do
  begin
    GrandTot:=GrandTot+DetailKlaimPengemudiQTotal.AsCurrency;
    DetailKlaimPengemudiQ.Next;
  end;
  MasterQ.Edit;
  MasterQGrandTotal.AsCurrency:=GrandTot;
  end;
end;

procedure TKlaimPengemudiFm.SaveBtnClick(Sender: TObject);
var kode:integer;
var kodes:string;
begin
kode:=-1;
try

  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
  MasterQ.Edit;
  KodeQ.Open;

    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage('a');
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end; //untuk if mode:entry
  MenuUtamaFm.Database1.StartTransaction;
except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
end;
  try
    MasterQ.ApplyUpdates;
    DetailKlaimPengemudiQ.Open;
    DetailKlaimPengemudiQ.Edit;
    DetailKlaimPengemudiQ.First;

    while DetailKlaimPengemudiQ.Eof=FALSE do
    begin
           KodeDetailQ.Close;
        KodeDetailQ.Open;
        if kode=-1 then
        begin
        
         if IsiBaru=1 then
         begin
            if KodeDetailQkodeDetail.AsString <> '' then
            begin
              kode:=strtoint(KodeDetailQkodeDetail.AsString);
            end

            else
            begin
              kode:=0;
            end;
         end  //end insert baru

         //kalau bukan isi baru
         else
         begin
          if DetailKlaimPengemudiQKodeDetail.AsString='' then
          begin
            KodePORQ.Close;
            //KodePORQ.ParamByName('text').AsString:=KodeEdit.Text;
            KodePORQ.Open;
            kode:=strtoint(KodePORQkodeDetail.AsString);
          end;
         end;

    end

        else
        begin
          if DetailKlaimPengemudiQKodeDetail.AsString='' then
          begin
            kode:=kode+1;
          end;
            //kode:=kode;
        end;

        kodes:=inttostr(kode);
        KodeDetailQ.Close;
        DetailKlaimPengemudiQ.Edit;
        if DetailKlaimPengemudiQKodeDetail.AsString='' then
        begin
          DetailKlaimPengemudiQKodeDetail.AsString:=DMFm.Fill(InttoStr(StrtoInt(kodes)+1),10,'0',True);
        end;
        DetailKlaimPengemudiQKodeKlaim.AsString:=KodeEdit.Text;
        DetailKlaimPengemudiQ.Post;
        DetailKlaimPengemudiQ.Next;
    end;

    DetailKlaimPengemudiQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailKlaimPengemudiQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    //FormShow(self);
    //cxButtonEdit1PropertiesButtonClick(sender,0);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailKlaimPengemudiQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
    //MenuUtamaFm.Database1.Rollback;
   // MasterQ.RollbackUpdates;
    //ShowMessage('Penyimpanan Gagal');
  //end;
  KodeEdit.SetFocus;
  ViewKlaimPengemudiQ.Close;
  ViewKlaimPengemudiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKlaimPengemudiQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewKlaimPengemudiQ.Open;
  DetailKlaimPengemudiQ.Close;
  DetailKlaimPengemudiQ.SQL.Text:=DetailPORebuildOriSQL;
  DetailKlaimPengemudiQ.ParamByName('text').AsString:='';
  DetailKlaimPengemudiQ.Open;
  DetailKlaimPengemudiQ.Close;


end;

procedure TKlaimPengemudiFm.DeleteBtnClick(Sender: TObject);
begin
 if MessageDlg('Hapus Klaim Pengemudi '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
     DeletePORQ.Close;
     DeletePORQ.ParamByName('text').AsString := MasterQKode.AsString;
     DeletePORQ.Open;
     DeletePORQ.First;
     while DeletePORQ.Eof = FALSE
     do
     begin
       SDQuery1.SQL.Text:=('delete from DetailKlaimPengemudi where KodeKlaim='+QuotedStr(KodeEdit.Text));
       SDQuery1.ExecSQL;
       DeletePORQ.Next;
     end;
     //DeleteDBKBQ.Delete;
     //DeleteDBKBQ.ApplyUpdates;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Klaim Pengemudi telah dihapus.',mtInformation,[mbOK],0);
       DeletePORQ.Close;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Klaim Pengemudi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  ViewKlaimPengemudiQ.Close;
  ViewKlaimPengemudiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKlaimPengemudiQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewKlaimPengemudiQ.Open;
  DetailKlaimPengemudiQ.Close;
  end;
end;

procedure TKlaimPengemudiFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
IsiBaru:=0;
SupplierQ.Open;
   MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewKlaimPengemudiQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;

    end;


    SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
    DeleteBtn.Enabled:=true;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    MasterVGrid.Enabled:=True;
    DetailKlaimPengemudiQ.Close;
    SupplierQ.Open;
    DetailKlaimPengemudiQ.ParamByName('text').AsString:=MasterQKode.AsString;
    DetailKlaimPengemudiQ.Open;
    DetailKlaimPengemudiQ.Edit;
   DetailKlaimPengemudiQ.First;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TKlaimPengemudiFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewKlaimPengemudiQ.Close;
  ViewKlaimPengemudiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKlaimPengemudiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewKlaimPengemudiQ.Open;
end;

procedure TKlaimPengemudiFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewKlaimPengemudiQ.Close;
  ViewKlaimPengemudiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKlaimPengemudiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewKlaimPengemudiQ.Open;
end;

procedure TKlaimPengemudiFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   ArmadaDropDownFm:=TArmadaDropdownfm.Create(Self);
    if ArmadaDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    ArmadaDropDownFm.Release;
end;

procedure TKlaimPengemudiFm.MasterVGridPengemudiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   PengemudiDropDownFm:=TPengemudiDropdownfm.Create(Self);
    if PengemudiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPengemudi.AsString:=PengemudiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PengemudiDropDownFm.Release;
end;

procedure TKlaimPengemudiFm.ExitBtnClick(Sender: TObject);
begin
Release;
end;

procedure TKlaimPengemudiFm.cxGrid1DBTableView1TotalPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
DetailKlaimPengemudiQTotal.AsCurrency:=DisplayValue;
end;

procedure TKlaimPengemudiFm.PrintBtnClick(Sender: TObject);
begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KlaimPengemudi.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
      Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
      Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
      Crpe1.Execute;
end;

end.
