object KomplainDanReturFm: TKomplainDanReturFm
  Left = 201
  Top = 144
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Komplain dan Retur'
  ClientHeight = 503
  ClientWidth = 1054
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1054
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 16
      Caption = 'Kode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 56
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.MaxLength = 0
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 270
    Width = 1054
    Height = 233
    Align = alBottom
    TabOrder = 2
    object SaveBtn: TcxButton
      Left = 8
      Top = 186
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 180
      Top = 186
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 3
      OnClick = ExitBtnClick
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1052
      Height = 168
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 81
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'Pelaksana'
          Width = 98
        end
        object cxGrid1DBTableView1Column8: TcxGridDBColumn
          DataBinding.FieldName = 'DetailPelaksana'
          Options.SortByDisplayText = isbtOn
          Width = 118
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 222
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          DataBinding.FieldName = 'KodePO'
          Width = 105
        end
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 118
        end
        object cxGrid1DBTableView1Column6: TcxGridDBColumn
          DataBinding.FieldName = 'Retur'
          Width = 137
        end
        object cxGrid1DBTableView1Column7: TcxGridDBColumn
          DataBinding.FieldName = 'Complaint'
          Width = 147
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 215
      Width = 1052
      Height = 17
      Panels = <
        item
          Width = 50
        end>
    end
    object DeleteBtn: TcxButton
      Left = 92
      Top = 186
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 4
      OnClick = DeleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 756
      Top = 169
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 46
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 377
    Height = 222
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    LookAndFeel.SkinName = 'Darkroom'
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.GridLineColor = clBtnFace
    OptionsView.RowHeaderWidth = 139
    OptionsView.RowHeight = 12
    OptionsView.ShowEmptyRowImage = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTglRetur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglRetur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPelaksana: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPelaksanaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pelaksana'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridDetailPelaksana: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPelaksana'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 47
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridKodePO: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKodePOEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'KodePO'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 2
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.Items = <
        item
          Caption = 'ON PROCESS'
          Value = 'ON PROCESS'
        end
        item
          Caption = 'FINISHED'
          Value = 'FINISHED'
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridRetur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Retur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridComplaint: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Complaint'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 6
      Version = 1
    end
  end
  object cxGrid2: TcxGrid
    Left = 377
    Top = 48
    Width = 677
    Height = 222
    Align = alClient
    TabOrder = 3
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGridDBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DetailDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGridDBColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGridDBColumn1PropertiesButtonClick
        Width = 87
      end
      object cxGridDBColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Options.Editing = False
        Width = 81
      end
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Jumlah'
        Width = 49
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'Alasan'
        PropertiesClassName = 'TcxMemoProperties'
        Width = 125
      end
      object cxGridDBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'PesanUlang'
        Width = 75
      end
      object cxGridDBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'PotongPO'
        Width = 66
      end
      object cxGridDBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'TglTargetTukar'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.InputKind = ikRegExpr
        Width = 90
      end
      object cxGridDBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'StatusRetur'
        Options.Editing = False
        Width = 80
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object MasterQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = [doFetchAllOnOpen]
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from retur')
    UpdateObject = MasterUS
    Left = 201
    Top = 113
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTglRetur: TDateTimeField
      FieldName = 'TglRetur'
      Required = True
    end
    object MasterQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Required = True
      Size = 10
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQKodePO: TStringField
      FieldName = 'KodePO'
      Size = 10
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object MasterQRetur: TBooleanField
      FieldName = 'Retur'
    end
    object MasterQComplaint: TBooleanField
      FieldName = 'Complaint'
    end
    object MasterQNamaPelaksana: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelaksana'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelaksana'
      Size = 50
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 239
    Top = 113
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, TglRetur, Pelaksana, Keterangan, CreateDate, Create' +
        'By, Operator, TglEntry, TglCetak, KodePO, Status, Retur, Complai' +
        'nt'#13#10'from retur'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update retur'
      'set'
      '  Kode = :Kode,'
      '  TglRetur = :TglRetur,'
      '  Pelaksana = :Pelaksana,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KodePO = :KodePO,'
      '  Status = :Status,'
      '  Retur = :Retur,'
      '  Complaint = :Complaint'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into retur'
      
        '  (Kode, TglRetur, Pelaksana, Keterangan, CreateDate, CreateBy, ' +
        'Operator, TglEntry, TglCetak, KodePO, Status, Retur, Complaint)'
      'values'
      
        '  (:Kode, :TglRetur, :Pelaksana, :Keterangan, :CreateDate, :Crea' +
        'teBy, :Operator, :TglEntry, :TglCetak, :KodePO, :Status, :Retur,' +
        ' :Complaint)')
    DeleteSQL.Strings = (
      'delete from retur'
      'where'
      '  Kode = :OLD_Kode')
    Left = 280
    Top = 114
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select top 1 kode from retur order by kode desc')
    Left = 169
    Top = 119
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39)
    Left = 505
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      ''
      '')
    Left = 553
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 593
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewKontrakQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(tglretur as date) as Tanggal,* '
      'from retur'
      
        'where (Tglretur>=:text1 and Tglretur<=:text2) or tglretur is nul' +
        'l'
      'order by tglentry desc')
    Left = 625
    Top = 313
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewKontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewKontrakQTglRetur: TDateTimeField
      FieldName = 'TglRetur'
      Required = True
    end
    object ViewKontrakQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Required = True
      Size = 10
    end
    object ViewKontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewKontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewKontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewKontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewKontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewKontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewKontrakQKodePO: TStringField
      FieldName = 'KodePO'
      Size = 10
    end
    object ViewKontrakQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object ViewKontrakQRetur: TBooleanField
      FieldName = 'Retur'
    end
    object ViewKontrakQComplaint: TBooleanField
      FieldName = 'Complaint'
    end
    object ViewKontrakQDetailPelaksana: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPelaksana'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelaksana'
      Size = 10
      Lookup = True
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewKontrakQ
    Left = 676
    Top = 310
  end
  object DetailQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = [doFetchAllOnOpen]
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from detailretur where koderetur = :text')
    UpdateObject = DetailUS
    Left = 689
    Top = 161
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ' '
      end>
    object DetailQKodeRetur: TStringField
      FieldName = 'KodeRetur'
      Required = True
      Size = 10
    end
    object DetailQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 55
      Lookup = True
    end
    object DetailQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailQAlasan: TMemoField
      FieldName = 'Alasan'
      Required = True
      BlobType = ftMemo
    end
    object DetailQTglTargetTukar: TDateTimeField
      FieldName = 'TglTargetTukar'
    end
    object DetailQPotongPO: TBooleanField
      FieldName = 'PotongPO'
    end
    object DetailQStatusRetur: TStringField
      FieldName = 'StatusRetur'
      Size = 50
    end
    object DetailQPesanUlang: TBooleanField
      FieldName = 'PesanUlang'
    end
  end
  object DetailDs: TDataSource
    DataSet = DetailQ
    Left = 759
    Top = 161
  end
  object DetailUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeRetur, Barang, Jumlah, Alasan, TglTargetTukar, Potong' +
        'PO, StatusRetur, PesanUlang'#13#10'from detailretur'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update detailretur'
      'set'
      '  KodeRetur = :KodeRetur,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  Alasan = :Alasan,'
      '  TglTargetTukar = :TglTargetTukar,'
      '  PotongPO = :PotongPO,'
      '  StatusRetur = :StatusRetur,'
      '  PesanUlang = :PesanUlang'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into detailretur'
      
        '  (KodeRetur, Barang, Jumlah, Alasan, TglTargetTukar, PotongPO, ' +
        'StatusRetur, PesanUlang)'
      'values'
      
        '  (:KodeRetur, :Barang, :Jumlah, :Alasan, :TglTargetTukar, :Poto' +
        'ngPO, :StatusRetur, :PesanUlang)')
    DeleteSQL.Strings = (
      'delete from detailretur'
      'where'
      '  KodeRetur = :OLD_KodeRetur and'
      '  Barang = :OLD_Barang')
    Left = 728
    Top = 162
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 64
    Top = 72
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object POQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from PO')
    Left = 48
    Top = 136
    object POQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object POQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object POQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object POQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object POQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object POQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object POQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object POQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object POQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object POQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object POQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object POQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object POQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object POQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object POQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 656
    Top = 168
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object TempQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailRetur')
    Left = 672
    Top = 128
    object TempQKodeRetur: TStringField
      FieldName = 'KodeRetur'
      Required = True
      Size = 10
    end
    object TempQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object TempQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object TempQAlasan: TMemoField
      FieldName = 'Alasan'
      Required = True
      BlobType = ftMemo
    end
    object TempQTglTargetTukar: TDateTimeField
      FieldName = 'TglTargetTukar'
    end
    object TempQPotongPO: TBooleanField
      FieldName = 'PotongPO'
    end
    object TempQStatusRetur: TStringField
      FieldName = 'StatusRetur'
      Size = 50
    end
    object TempQPesanUlang: TBooleanField
      FieldName = 'PesanUlang'
    end
    object TempQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 10
      Lookup = True
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Left = 520
    Top = 256
  end
end
