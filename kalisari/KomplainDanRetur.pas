unit KomplainDanRetur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo, cxDropDownEdit, cxGroupBox;

type
  TKomplainDanReturFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQKeterangan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSopir: TStringField;
    ExitBtn: TcxButton;
    KodeQkode: TStringField;
    ViewKontrakQ: TSDQuery;
    ViewDs: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar: TStatusBar;
    DeleteBtn: TcxButton;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQKode: TStringField;
    MasterQTglRetur: TDateTimeField;
    MasterQPelaksana: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQKodePO: TStringField;
    MasterQStatus: TStringField;
    MasterQRetur: TBooleanField;
    MasterQComplaint: TBooleanField;
    MasterVGridTglRetur: TcxDBEditorRow;
    MasterVGridPelaksana: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridKodePO: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridRetur: TcxDBEditorRow;
    MasterVGridComplaint: TcxDBEditorRow;
    DetailQ: TSDQuery;
    DetailDs: TDataSource;
    DetailUS: TSDUpdateSQL;
    DetailQKodeRetur: TStringField;
    DetailQBarang: TStringField;
    DetailQJumlah: TIntegerField;
    DetailQAlasan: TMemoField;
    DetailQTglTargetTukar: TDateTimeField;
    DetailQPotongPO: TBooleanField;
    DetailQStatusRetur: TStringField;
    DetailQPesanUlang: TBooleanField;
    DetailQNamaBarang: TStringField;
    PegawaiQ: TSDQuery;
    MasterVGridDetailPelaksana: TcxDBEditorRow;
    ViewKontrakQKode: TStringField;
    ViewKontrakQTglRetur: TDateTimeField;
    ViewKontrakQPelaksana: TStringField;
    ViewKontrakQKeterangan: TMemoField;
    ViewKontrakQCreateDate: TDateTimeField;
    ViewKontrakQCreateBy: TStringField;
    ViewKontrakQOperator: TStringField;
    ViewKontrakQTglEntry: TDateTimeField;
    ViewKontrakQTglCetak: TDateTimeField;
    ViewKontrakQKodePO: TStringField;
    ViewKontrakQStatus: TStringField;
    ViewKontrakQRetur: TBooleanField;
    ViewKontrakQComplaint: TBooleanField;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    ViewKontrakQDetailPelaksana: TStringField;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    cxGridDBTableView1Column4: TcxGridDBColumn;
    cxGridDBTableView1Column5: TcxGridDBColumn;
    cxGridDBTableView1Column6: TcxGridDBColumn;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    POQ: TSDQuery;
    POQKode: TStringField;
    POQSupplier: TStringField;
    POQTermin: TStringField;
    POQTglKirim: TDateTimeField;
    POQGrandTotal: TCurrencyField;
    POQStatus: TStringField;
    POQCreateDate: TDateTimeField;
    POQCreateBy: TStringField;
    POQOperator: TStringField;
    POQTglEntry: TDateTimeField;
    POQTglCetak: TDateTimeField;
    POQStatusKirim: TStringField;
    POQDiskon: TCurrencyField;
    POQKirim: TBooleanField;
    POQAmbil: TBooleanField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    TempQ: TSDQuery;
    TempQKodeRetur: TStringField;
    TempQBarang: TStringField;
    TempQJumlah: TIntegerField;
    TempQAlasan: TMemoField;
    TempQTglTargetTukar: TDateTimeField;
    TempQPotongPO: TBooleanField;
    TempQStatusRetur: TStringField;
    TempQPesanUlang: TBooleanField;
    TempQNamaBarang: TStringField;
    MasterQNamaPelaksana: TStringField;
    SDQuery1: TSDQuery;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxDBVerticalGrid1Exit(Sender: TObject);
    procedure MasterVGridPelaksanaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKodePOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGridDBColumn1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  KomplainDanReturFm: TKomplainDanReturFm;

  MasterOriSQL: string;
  DetailOriSQL: string;

  implementation

uses MenuUtama, DropDown, DM, MasterPelanggan, PegawaiDropDown, PODropDown, BarangLPBDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TKomplainDanReturFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TKomplainDanReturFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TKomplainDanReturFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TKomplainDanReturFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TKomplainDanReturFm.FormCreate(Sender: TObject);
begin
  //MenuUtamaFm.cxListBox1.Visible:=false;
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  DetailQ.Open;
end;

procedure TKomplainDanReturFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Close;
  MasterQ.Open;
  KodeEditExit(sender);
  //mastervgrid.Enabled:=true;
  //MasterVGrid.SetFocus;
  //DMFm.GetDateQ.Open;
  //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //DMFm.GetDateQ.Close;
end;

procedure TKomplainDanReturFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKomplainDanReturFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewKontrakQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKomplainNRetur.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKomplainRetur.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKomplainRetur.AsBoolean;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
end;

procedure TKomplainDanReturFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
      MasterQRetur.Value:=False;
      MasterQComplaint.Value:=False;
      MasterQTglRetur.Value:=Trunc(Now());
      MasterQStatus.AsString:='ON PROCESS';
    end
    else
    begin
      Masterq.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKomplainRetur.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKomplainRetur.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
    DetailQ.Close;
  detailq.SQL.Text:=DetailOriSQL;
  detailq.ParamByName('text').AsString:='';
  detailq.Open;
  DetailQ.Close;
end;

procedure TKomplainDanReturFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TKomplainDanReturFm.SaveBtnClick(Sender: TObject);
begin
  KodeQ.Open;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
      DetailQ.First;
      While DetailQ.Eof = false do
      begin
        DetailQ.Edit;
        DetailQKodeRetur.AsString:=MasterQKode.AsString;
        DetailQ.Post;
        DetailQ.Next;
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
  end;

  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailQ.Open;
    DetailQ.Edit;
    DetailQ.First;
    while DetailQ.Eof=false do
    begin
        DetailQ.Edit;
        DetailQKodeRetur.AsString:=MasterQKode.AsString;
        DetailQ.Post;
        DetailQ.Next;
    end;
    DetailQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailQ.CommitUpdates;
    ShowMessage('Komplain dan Retur dengan kode '+ MasterQKode.AsString +' telah disimpan');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
    {MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;

    except
    on E : Exception do
              //ShowMessage('a');
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
    end; }

  ViewKontrakQ.refresh;
  //KodeQ.Refresh;
  //masterq.Refresh;
  DetailQ.Close;
  detailq.SQL.Text:=DetailOriSQL;
  detailq.ParamByName('text').AsString:='';
  detailq.Open;
  DetailQ.Close;
  KodeEdit.SetFocus;


end;

procedure TKomplainDanReturFm.MasterQAfterInsert(DataSet: TDataSet);
begin
      //DMFm.GetDateQ.Open;
      //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
      MasterQ.Edit;
      MasterQCreateBy.AsString:=User;
 // DMFm.GetDateQ.Close;
end;

procedure TKomplainDanReturFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQ.Edit;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TKomplainDanReturFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Komplain dan Retur '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
      SDQuery1.SQL.Text:='delete from detailretur where koderetur='+quotedstr(MasterQKode.AsString);
      SDQuery1.ExecSQL;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Komplain dan Retur telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Komplain dan Retur pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     ViewKontrakQ.Refresh;
  end;
end;

procedure TKomplainDanReturFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKomplainDanReturFm.MasterVGridExit(Sender: TObject);
begin
  //cxDBVerticalGrid1.SetFocus
end;

procedure TKomplainDanReturFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if Abuttonindex=0 then
  begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:='';
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      //MasterQPelanggan.AsString:=PelangganQKode.AsString;
    end;
    DropDownFm.Release;
  end
  else if abuttonindex=1 then
  begin
    MasterPelangganFm := Tmasterpelangganfm.create(MenuUtamaFm);
  end;

end;

procedure TKomplainDanReturFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQPelanggan.AsString:=PelangganQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKomplainDanReturFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='';
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQRute.AsString:=RuteQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TKomplainDanReturFm.MasterVGridRuteEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQRute.AsString:=RuteQKode.AsString;
      //cxlabel4.Caption:='Muat : ' + RuteQMuat.AsString;
      //cxlabel5.Caption:='Bongkar : ' + RuteQBongkar.AsString;
      //cxlabel6.Caption:='Standard Harga : ' + RuteQHarga.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKomplainDanReturFm.MasterVGridArmadaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
   { ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;   }
end;

procedure TKomplainDanReturFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release; }
end;

procedure TKomplainDanReturFm.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
    MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kode like '+ QuotedStr('%'+ViewKontrakQKode.AsString+'%'));
    DetailQ.Close;
    DetailQ.SQL.Clear;
    //DetailQ.SQL.Add('select * from ('+ DetailOriSQL +') x where x.kode like '+ QuotedStr('%'+ViewKontrakQKode.AsString+'%'));
    //DetailQ.ParamByName('koderetur').AsString:=KodeEdit.Text;
    DetailQ.SQL.Add('select * from detailretur where koderetur like '+ QuotedStr('%'+ViewKontrakQKode.AsString+'%'));

    try
      MasterQ.Open;
      DetailQ.open;
    except
        on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

     // ShowMessage('gagal');
    end;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      MasterQ.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    if MenuUtamaFm.UserQUpdateKomplainRetur.AsBoolean then SaveBtn.Enabled:=True else savebtn.Enabled:=false;
    MasterVGrid.Enabled:=True;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKomplainRetur.AsBoolean;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TKomplainDanReturFm.cxDBVerticalGrid1Exit(Sender: TObject);
begin
SaveBtn.SetFocus;
end;

procedure TKomplainDanReturFm.MasterVGridPelaksanaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    PegawaiQ.Close;
    PegawaiQ.ExecSQL;
    PegawaiQ.Open;   }
    PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelaksana.AsString:=PegawaiDropDownFm.kode;
      //MasterQDetailPelaksana.AsString:=PegawaiQNama.AsString;
    end;
    PegawaiDropDownFm.Release;


end;

procedure TKomplainDanReturFm.MasterVGridKodePOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    POQ.Close;
    POQ.ExecSQL;
    POQ.Open; }

    PODropDownFm:=TPODropdownfm.Create(Self);
    if PODropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKodePO.AsString:=PODropDownFm.kode;
    end;
    PODropDownFm.Release;


end;

procedure TKomplainDanReturFm.cxGridDBColumn1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var q:string;
begin
   {BarangQ.Close;
    BarangQ.ExecSQL;
    BarangQ.Open; }
    q:='select b.* from barang b, DetailPO dp, DaftarBeli db where dp.KodeDaftarBeli=db.kode and b.kode=db.barang and dp.KodePO=' +QuotedStr(MasterQKodePO.AsString);
    BarangLPBDropDownFm:=TBarangLPBDropdownfm.Create(Self,q);
    if BarangLPBDropDownFm.ShowModal=MrOK then
    begin
      DetailQ.Open;
      DetailQ.Edit;
      DetailQBarang.AsString:=BarangLPBDropDownFm.kode;
      //DetailQNamaBarang.AsString:=BarangQNama.AsString;
    end;
    BarangLPBDropDownFm.Release;

end;

procedure TKomplainDanReturFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  //ShowMessage(inttostr(abuttonindex));
  if abuttonindex=10 then
  begin
    DetailQKodeRetur.AsString:=KodeEdit.Text;
    DetailQ.Post;
  end;
  if abuttonindex=8 then
  begin
      detailq.Open;
      DetailQ.edit;
      DetailQ.Delete;
      DetailQ.Refresh;
    cxGrid1DBTableView1.Focused:=false;
  end
{
    else if abuttonindex=9 then
  begin
      TempQ.Open;
      DetailQ.Open;
      TempQ.Edit;
      TempQkoderetur.AsString:=DetailQkoderetur.asString;
      TempQBarang.AsString:=DetailQbarang.AsString;
      TempQNamaBarang.AsString:=DetailQNamaBarang.AsString;
      TempQJumlah.AsInteger:=DetailQJumlah.AsInteger;
      TempQAlasan.AsString:=DetailQAlasan.AsString;
      TempQPesanUlang.AsBoolean:=DetailQPesanUlang.AsBoolean;
      TempQPotongPO.AsBoolean:=DetailQPotongPO.AsBoolean;
      TempQTglTargetTukar.AsString:=DetailQTglTargetTukar.AsString;
      Tempq.Post;

    cxGrid1DBTableView1.Focused:=false;
  end;
 }
end;

procedure TKomplainDanReturFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewKontrakQ.Open;
end;

procedure TKomplainDanReturFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewKontrakQ.Open;
end;

end.
