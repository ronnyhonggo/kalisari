object KontrakFm: TKontrakFm
  Left = 193
  Top = 45
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Kontrak'
  ClientHeight = 629
  ClientWidth = 922
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 922
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 16
      Caption = 'Kode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 56
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.MaxLength = 0
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 341
    Width = 922
    Height = 288
    Align = alBottom
    TabOrder = 2
    object SaveBtn: TcxButton
      Left = 8
      Top = 226
      Width = 75
      Height = 25
      Caption = 'SAVE'
      Enabled = False
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 180
      Top = 226
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 3
      OnClick = ExitBtnClick
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 920
      Height = 216
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPelanggan'
          Options.SortByDisplayText = isbtOn
          Width = 313
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'TglMulai'
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          DataBinding.FieldName = 'TglSelesai'
        end
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 254
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 262
      Width = 920
      Height = 25
      Panels = <
        item
          Width = 50
        end>
    end
    object DeleteBtn: TcxButton
      Left = 92
      Top = 226
      Width = 75
      Height = 25
      Caption = 'DELETE'
      Enabled = False
      TabOrder = 4
      OnClick = DeleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 624
      Top = 217
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 45
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel1: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 337
    Height = 293
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    LookAndFeel.SkinName = 'Darkroom'
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.GridLineColor = clBtnFace
    OptionsView.RowHeaderWidth = 155
    OptionsView.RowHeight = 12
    OptionsView.ShowEmptyRowImage = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPelangganEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pelanggan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNamaPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPelanggan'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridAlamatPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'AlamatPelanggan'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGridTelpPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'TelpPelanggan'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object MasterVGridTglMulai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglMulai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridTglSelesai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglSelesai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridRuteEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Rute'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridDariRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'DariRute'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object MasterVGridKeRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'KeRute'
      Properties.Options.Editing = False
      ID = 8
      ParentID = 6
      Index = 1
      Version = 1
    end
    object MasterVGridKategoriRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'KategoriRute'
      Properties.Options.Editing = False
      ID = 9
      ParentID = 6
      Index = 2
      Version = 1
    end
    object MasterVGridKeteranganRute: TcxDBEditorRow
      Height = 50
      Properties.DataBinding.FieldName = 'KeteranganRute'
      ID = 10
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridIntervalPenagihan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IntervalPenagihan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object Panel1: TPanel
    Left = 337
    Top = 48
    Width = 585
    Height = 293
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 3
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 361
      Height = 291
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      LookAndFeel.SkinName = 'Darkroom'
      OptionsView.ShowEditButtons = ecsbFocused
      OptionsView.GridLineColor = clBtnFace
      OptionsView.RowHeaderWidth = 136
      OptionsView.RowHeight = 12
      OptionsView.ShowEmptyRowImage = True
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      OnExit = cxDBVerticalGrid1Exit
      DataController.DataSource = MasterDs
      Version = 1
      object cxDBVerticalGrid1JumlahUnit: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JumlahUnit'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object ACTes: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'AC'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Toilet: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Toilet'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1AirSuspension: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'AirSuspension'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1KapasitasSeat: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KapasitasSeat'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1Harga: TcxDBEditorRow
        Properties.Caption = 'Harga/Unit'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = cxDBVerticalGrid1HargaEditPropertiesValidate
        Properties.DataBinding.FieldName = 'Harga'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1PPN: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.OnChange = cxDBVerticalGrid1PPNEditPropertiesChange
        Properties.DataBinding.FieldName = 'PPN'
        ID = 38
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1POEksternal: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'POEksternal'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 6
        ParentID = -1
        Index = 7
        Version = 1
      end
      object cxDBVerticalGrid1Status: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = 'ONGOING'
            Value = 'ONGOING'
          end
          item
            Caption = 'FINISHED'
            Value = 'FINISHED'
          end
          item
            Caption = 'CANCEL'
            Value = 'CANCEL'
          end>
        Properties.DataBinding.FieldName = 'Status'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 7
        ParentID = -1
        Index = 8
        Version = 1
      end
      object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
        Height = 81
        Properties.DataBinding.FieldName = 'Keterangan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 8
        ParentID = -1
        Index = 9
        Version = 1
      end
    end
    object cxGrid2: TcxGrid
      Left = 362
      Top = 1
      Width = 222
      Height = 291
      Align = alRight
      TabOrder = 1
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid2DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2PlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'PlatNo'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView2PlatNoPropertiesButtonClick
          Styles.Content = MenuUtamaFm.cxStyle5
          Width = 100
        end
        object cxGrid1DBTableView2TglExpired: TcxGridDBColumn
          DataBinding.FieldName = 'TglExpired'
          Styles.Content = MenuUtamaFm.cxStyle5
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid1DBTableView2
      end
    end
  end
  object MasterQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = [doFetchAllOnOpen]
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from kontrak')
    UpdateObject = MasterUS
    Left = 321
    Top = 1
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQTelpPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'TelpPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object MasterQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object MasterQDariRute: TStringField
      FieldKind = fkLookup
      FieldName = 'DariRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQKeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KeRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQKategoriRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KategoriRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQAC: TBooleanField
      FieldName = 'AC'
    end
    object MasterQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object MasterQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object MasterQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object MasterQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object MasterQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 200
    end
    object MasterQJumlahUnit: TIntegerField
      FieldName = 'JumlahUnit'
    end
    object MasterQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object MasterQPPN: TBooleanField
      FieldName = 'PPN'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 372
    Top = 65534
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, ' +
        'AC, Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, St' +
        'atus, Keterangan, IntervalPenagihan, CreateDate, CreateBy, Opera' +
        'tor, TglEntry, TglCetak, PlatNo, JumlahUnit, KeteranganRute, PPN'
      'from kontrak'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update kontrak'
      'set'
      '  Kode = :Kode,'
      '  Pelanggan = :Pelanggan,'
      '  TglMulai = :TglMulai,'
      '  TglSelesai = :TglSelesai,'
      '  StatusRute = :StatusRute,'
      '  Rute = :Rute,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  Harga = :Harga,'
      '  POEksternal = :POEksternal,'
      '  Status = :Status,'
      '  Keterangan = :Keterangan,'
      '  IntervalPenagihan = :IntervalPenagihan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  PlatNo = :PlatNo,'
      '  JumlahUnit = :JumlahUnit,'
      '  KeteranganRute = :KeteranganRute,'
      '  PPN = :PPN'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into kontrak'
      
        '  (Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, AC, ' +
        'Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, Status' +
        ', Keterangan, IntervalPenagihan, CreateDate, CreateBy, Operator,' +
        ' TglEntry, TglCetak, PlatNo, JumlahUnit, KeteranganRute, PPN)'
      'values'
      
        '  (:Kode, :Pelanggan, :TglMulai, :TglSelesai, :StatusRute, :Rute' +
        ', :AC, :Toilet, :AirSuspension, :KapasitasSeat, :Harga, :POEkste' +
        'rnal, :Status, :Keterangan, :IntervalPenagihan, :CreateDate, :Cr' +
        'eateBy, :Operator, :TglEntry, :TglCetak, :PlatNo, :JumlahUnit, :' +
        'KeteranganRute, :PPN)')
    DeleteSQL.Strings = (
      'delete from kontrak'
      'where'
      '  Kode = :OLD_Kode')
    Left = 420
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from kontrak order by kode desc')
    Left = 281
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39)
    Left = 473
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      ''
      '')
    Left = 529
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 609
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewKontrakQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select k.pelanggan,k.tglentry, k.Kode, cast(k.tglmulai as date) ' +
        'as TglMulai, cast(k.tglselesai as date) as TglSelesai, k.Status,' +
        ' p.namapt as NamaPelanggan from kontrak k, pelanggan p where k.p' +
        'elanggan=p.kode'
      
        'and ((k.TglMulai >= :text1 and k.TglMulai <= :text2) or k.tglmul' +
        'ai is null) order by k.tglentry desc')
    Left = 657
    Top = 1
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewKontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewKontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object ViewKontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewKontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewKontrakQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Required = True
      Size = 100
    end
    object ViewKontrakQTglMulai: TStringField
      FieldName = 'TglMulai'
    end
    object ViewKontrakQTglSelesai: TStringField
      FieldName = 'TglSelesai'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewKontrakQ
    Left = 708
    Top = 6
  end
  object ArmadaKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from ArmadaKontrak where Kontrak=:text')
    UpdateObject = ArmadaKontrakUpdate
    Left = 936
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaKontrakQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object ArmadaKontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 50
    end
    object ArmadaKontrakQTglExpired: TDateTimeField
      FieldName = 'TglExpired'
    end
  end
  object DataSource1: TDataSource
    DataSet = ArmadaKontrakQ
    Left = 968
    Top = 8
  end
  object ArmadaKontrakUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kontrak, PlatNo, TglExpired'
      'from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    ModifySQL.Strings = (
      'update ArmadaKontrak'
      'set'
      '  Kontrak = :Kontrak,'
      '  PlatNo = :PlatNo,'
      '  TglExpired = :TglExpired'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    InsertSQL.Strings = (
      'insert into ArmadaKontrak'
      '  (Kontrak, PlatNo, TglExpired)'
      'values'
      '  (:Kontrak, :PlatNo, :TglExpired)')
    DeleteSQL.Strings = (
      'delete from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    Left = 1008
    Top = 8
  end
  object DeleteArmadaKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from ArmadaKontrak where Kontrak=:text')
    UpdateObject = DeleteArmadaKontrakUpdate
    Left = 1048
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteArmadaKontrakQKontrak: TStringField
      FieldName = 'Kontrak'
      Required = True
      Size = 10
    end
    object DeleteArmadaKontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 50
    end
    object DeleteArmadaKontrakQTglExpired: TDateTimeField
      FieldName = 'TglExpired'
    end
  end
  object DeleteArmadaKontrakUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kontrak, PlatNo, TglExpired'
      'from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    ModifySQL.Strings = (
      'update ArmadaKontrak'
      'set'
      '  Kontrak = :Kontrak,'
      '  PlatNo = :PlatNo,'
      '  TglExpired = :TglExpired'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    InsertSQL.Strings = (
      'insert into ArmadaKontrak'
      '  (Kontrak, PlatNo, TglExpired)'
      'values'
      '  (:Kontrak, :PlatNo, :TglExpired)')
    DeleteSQL.Strings = (
      'delete from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    Left = 1080
    Top = 16
  end
  object UpdatePlatQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'update Kontrak set PlatNo=:text where kode=:text2')
    Left = 512
    Top = 296
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
  end
end
