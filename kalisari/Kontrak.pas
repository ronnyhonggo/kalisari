unit Kontrak;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo, cxDropDownEdit, cxGroupBox, cxCheckBox;

type
  TKontrakFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQKeterangan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSopir: TStringField;
    ExitBtn: TcxButton;
    KodeQkode: TStringField;
    ViewKontrakQ: TSDQuery;
    ViewDs: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar: TStatusBar;
    DeleteBtn: TcxButton;
    MasterQKode: TStringField;
    MasterQPelanggan: TStringField;
    MasterQTglMulai: TDateTimeField;
    MasterQTglSelesai: TDateTimeField;
    MasterQRute: TStringField;
    MasterQAC: TBooleanField;
    MasterQToilet: TBooleanField;
    MasterQAirSuspension: TBooleanField;
    MasterQKapasitasSeat: TIntegerField;
    MasterQHarga: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQTelpPelanggan: TStringField;
    MasterQDariRute: TStringField;
    MasterQKeRute: TStringField;
    MasterQKategoriRute: TStringField;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridAlamatPelanggan: TcxDBEditorRow;
    MasterVGridTelpPelanggan: TcxDBEditorRow;
    MasterVGridTglMulai: TcxDBEditorRow;
    MasterVGridTglSelesai: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridDariRute: TcxDBEditorRow;
    MasterVGridKeRute: TcxDBEditorRow;
    MasterVGridKategoriRute: TcxDBEditorRow;
    MasterQStatusRute: TStringField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    ViewKontrakQKode: TStringField;
    ViewKontrakQPelanggan: TStringField;
    ViewKontrakQStatus: TStringField;
    ViewKontrakQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    MasterQIntervalPenagihan: TStringField;
    MasterVGridIntervalPenagihan: TcxDBEditorRow;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    ACTes: TcxDBEditorRow;
    cxDBVerticalGrid1Toilet: TcxDBEditorRow;
    cxDBVerticalGrid1AirSuspension: TcxDBEditorRow;
    cxDBVerticalGrid1KapasitasSeat: TcxDBEditorRow;
    cxDBVerticalGrid1Harga: TcxDBEditorRow;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    ArmadaKontrakQ: TSDQuery;
    DataSource1: TDataSource;
    ArmadaKontrakQKontrak: TStringField;
    ArmadaKontrakQPlatNo: TStringField;
    ArmadaKontrakQTglExpired: TDateTimeField;
    cxGrid1DBTableView2PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView2TglExpired: TcxGridDBColumn;
    ArmadaKontrakUpdate: TSDUpdateSQL;
    cxGrid1DBTableView2: TcxGridDBTableView;
    DeleteArmadaKontrakQ: TSDQuery;
    DeleteArmadaKontrakUpdate: TSDUpdateSQL;
    DeleteArmadaKontrakQKontrak: TStringField;
    DeleteArmadaKontrakQPlatNo: TStringField;
    DeleteArmadaKontrakQTglExpired: TDateTimeField;
    MasterQPOEksternal: TStringField;
    cxDBVerticalGrid1POEksternal: TcxDBEditorRow;
    MasterQPlatNo: TStringField;
    UpdatePlatQ: TSDQuery;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel1: TcxLabel;
    ViewKontrakQNamaPelanggan: TStringField;
    ViewKontrakQTglMulai: TStringField;
    ViewKontrakQTglSelesai: TStringField;
    MasterQJumlahUnit: TIntegerField;
    cxDBVerticalGrid1JumlahUnit: TcxDBEditorRow;
    MasterQKeteranganRute: TMemoField;
    MasterVGridKeteranganRute: TcxDBEditorRow;
    MasterQPPN: TBooleanField;
    cxDBVerticalGrid1PPN: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxDBVerticalGrid1Exit(Sender: TObject);
    procedure MasterVGridDBEditorRow1EditPropertiesEditValueChanged(
      Sender: TObject);
    procedure MasterVGridDBEditorRow1EditPropertiesChange(Sender: TObject);
    procedure cxGrid2DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1DBTableView2PlatNoPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxDBVerticalGrid1HargaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PPNEditPropertiesChange(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  KontrakFm: TKontrakFm;

  MasterOriSQL, ArmadaKontrakOriSQL, paramkode: string;
  HargaUnit:currency;
implementation

uses MenuUtama, DropDown, DM, MasterPelanggan, PelangganDropDown, RuteDropDown, ArmadaDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TKontrakFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TKontrakFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TKontrakFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TKontrakFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TKontrakFm.FormCreate(Sender: TObject);
begin
  //MenuUtamaFm.cxListBox1.Visible:=false;
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  ArmadaKontrakOriSQL:=ArmadaKontrakQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
  ViewKontrakQ.Open;
end;

procedure TKontrakFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Close;
  MasterQ.Open;
   ArmadaKontrakQ.Close;
   ArmadaKontrakQ.ParamByName('text').AsString:='';
    ArmadaKontrakQ.Open;
  KodeEditExit(sender);
end;

procedure TKontrakFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKontrakFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
   KodeEdit.Text:=paramkode;
   KodeEditExit(sender);
  end;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKontrak.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
end;

procedure TKontrakFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
      if MasterQPPN.AsBoolean=NULL then
      begin
          MasterQ.Edit;
          MasterQPPN.AsBoolean:=false;
      end;
      if MasterQPPN.AsBoolean=true then
      begin
         HargaUnit:=MasterQHarga.AsCurrency*100/110;
      end
      else
      begin
         HargaUnit:=MasterQHarga.AsCurrency;
      end;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
      MasterQStatus.AsString:='CANCEL';
      MasterQStatusRute.AsString:='SINGLE';
      MasterQAC.AsBoolean:=FALSE;
      MasterQToilet.AsBoolean:=FALSE;
      MasterQAirSuspension.AsBoolean:=FALSE;
      MasterQPPN.AsBoolean:=false;
    end
    else
    begin
      Masterq.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TKontrakFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TKontrakFm.SaveBtnClick(Sender: TObject);
var plat,kode:string;
begin
  try
  plat:='';
    KodeQ.Open;
    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      if KodeEdit.Text = 'INSERT BARU' then
      begin
        if KodeQ.IsEmpty then
        begin
          MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
        end
        else
        begin
          MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
        end;
      end
      else
      begin
        MasterQKode.AsString:=KodeEdit.Text;
      end;
      MasterQ.Post;
      KodeQ.Close;
      KodeEdit.Text:=MasterQKode.AsString;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
    kode:=MasterQKode.AsString;
      MasterQ.ApplyUpdates;
      ArmadaKontrakQ.Open;
      ArmadaKontrakQ.Edit;
      ArmadaKontrakQ.First;
      while ArmadaKontrakQ.Eof=FALSE do
      begin
          ArmadaKontrakQ.Edit;
          ArmadaKontrakQKontrak.AsString:=KodeEdit.Text;
          plat:=plat+ArmadaKontrakQPlatNo.AsString+'. ';
          ArmadaKontrakQ.Post;
          ArmadaKontrakQ.Next;
      end;
     { MasterQ.Edit;
      MasterQPlatNo.AsString:=plat;
      showmessage(MasterQPlatNo.AsString);
      MasterQ.Post;    }
      ArmadaKontrakQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      MasterQ.CommitUpdates;
      ShowMessage('Simpan Berhasil');
      FormShow(self);
    except
      on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ArmadaKontrakQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
    end;
  except
  on E : Exception do begin
      ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
      end;
  end;
  ViewKontrakQ.refresh;
  ArmadaKontrakQ.Close;
  UpdatePlatQ.Close;
  UpdatePlatQ.ParamByName('text').AsString:=plat;
  UpdatePlatQ.ParamByName('text2').AsString:=kode;
  UpdatePlatQ.Open;
end;

procedure TKontrakFm.MasterQAfterInsert(DataSet: TDataSet);
begin
      //DMFm.GetDateQ.Open;
      //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
      MasterQCreateBy.AsString:=User;
 // DMFm.GetDateQ.Close;
end;

procedure TKontrakFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TKontrakFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Kontrak '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       ArmadaKontrakQ.Close;
       ArmadaKontrakQ.ParamByName('text').AsString:=MasterQKode.AsString;
       ArmadaKontrakQ.Open;
       ArmadaKontrakQ.First;
       while ArmadaKontrakQ.Eof=FALSE
       do
       begin
          DeleteArmadaKontrakQ.SQL.Clear;
          DeleteArmadaKontrakQ.SQL.Text:='delete from ArmadaKontrak where Kontrak='+QuotedStr(KodeEdit.Text)+'';
          DeleteArmadaKontrakQ.ExecSQL;
          ArmadaKontrakQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Kontrak telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DeleteArmadaKontrakQ.RollbackUpdates;
      MessageDlg('Kontrak pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     viewKontrakQ.Close;
     viewKontrakQ.Open;
     ArmadaKontrakQ.Close;
     cxButtonEdit1PropertiesButtonClick(Sender,0);
  end;
end;

procedure TKontrakFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKontrakFm.MasterVGridExit(Sender: TObject);
begin
  cxDBVerticalGrid1.SetFocus
end;

procedure TKontrakFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if Abuttonindex=0 then
  begin
    PelangganDropDownFm:=TPelangganDropdownfm.Create(Self);
    if PelangganDropDownFm.ShowModal=MrOK then
    begin
      PelangganQ.Close;
      PelangganQ.open;
      MasterQPelanggan.AsString:=PelangganDropDownFm.kode;
    end;
    PelangganDropDownFm.Release;
  end
  else if abuttonindex=1 then
  begin
    MasterPelangganFm := Tmasterpelangganfm.create(MenuUtamaFm);
  end;
end;

procedure TKontrakFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=PelangganQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TKontrakFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='';
    RuteQ.ExecSQL;
    RuteQ.Open;}
    RuteDropDownFm:=TRuteDropdownfm.Create(Self);
    if RuteDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteDropDownFm.kode;
    end;
    RuteDropDownFm.Release;
end;

procedure TKontrakFm.MasterVGridRuteEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteQKode.AsString;
      //cxlabel4.Caption:='Muat : ' + RuteQMuat.AsString;
      //cxlabel5.Caption:='Bongkar : ' + RuteQBongkar.AsString;
      //cxlabel6.Caption:='Standard Harga : ' + RuteQHarga.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
      cxDBVerticalGrid1.SetFocus;
end;

procedure TKontrakFm.MasterVGridArmadaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
   { ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;   }
end;

procedure TKontrakFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release; }
end;

procedure TKontrakFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridPelanggan);
end;

procedure TKontrakFm.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
    MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kode like '+ QuotedStr('%'+ViewKontrakQKode.AsString+'%'));
    try
      MasterQ.Open;
      if MasterQPPN.AsBoolean=NULL then
      begin
          MasterQ.Edit;
          MasterQPPN.AsBoolean:=false;
      end;
      if MasterQPPN.AsBoolean=true then
      begin
         HargaUnit:=MasterQHarga.AsCurrency*100/110;
      end
      else
      begin
         HargaUnit:=MasterQHarga.AsCurrency;
      end;
    except
      ShowMessage('gagal');
    end;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      MasterQ.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
    MasterVGrid.Enabled:=True;
    ArmadaKontrakQ.Close;
    ArmadaKontrakQ.ParamByName('text').AsString := MasterQKode.AsString;
    ArmadaKontrakQ.Open;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TKontrakFm.cxDBVerticalGrid1Exit(Sender: TObject);
begin
SaveBtn.SetFocus;

end;

procedure TKontrakFm.MasterVGridDBEditorRow1EditPropertiesEditValueChanged(
  Sender: TObject);
begin
  if MasterQStatusRute.AsString='SINGLE' then
  begin

  end
  else
  begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='0000000000';
    ruteq.ExecSQL;
    ruteq.Open;
    masterQ.Open;
    masterq.Edit;
    MasterQRute.AsString:=RuteQKode.AsString;
  end;
end;

procedure TKontrakFm.MasterVGridDBEditorRow1EditPropertiesChange(
  Sender: TObject);
begin
  if MasterQStatusRute.AsString='SINGLE' then
  begin
    //MasterVGridRute.Visible:=true;
  end
  else
  begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='0000000000';
    ruteq.ExecSQL;
    ruteq.Open;
    masterQ.Open;
    masterq.Edit;
    MasterQRute.AsString:=RuteQKode.AsString;
  end;
end;


procedure TKontrakFm.cxGrid2DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
  ArmadaKontrakQ.Edit;

  //KodeDetailLPBQ.Close;
  //KodeDetailLPBQ.Open;

  {if KodeDetailLPBQkodeLPB.AsString='' then
  begin
      KodeDetailLPBQKodeLPB.AsString:='0000000000';
  end;}

  //DetailLPBQKodeLPB.AsString:= DMFm.Fill(InttoStr(StrtoInt(KodeDetailLPBQkodeLPB.AsString)+1),10,'0',True);;


 // ShowMessage(DetailLPBQKodeLPB.AsString);
    //DetailLPBQKodeLPB.AsString:='0';
    ArmadaKontrakQ.Post;

   { MenuUtamaFm.Database1.StartTransaction;
    try
      DetailLPBQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailLPBQ.CommitUpdates;
      except
      MenuUtamaFm.Database1.Rollback;
      DetailLPBQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    DetailLPBQ.Close;
    DetailLPBQ.SQL.Text:=DetailLPBQOriSQL;
   //DetailLPBQ.ExecSQL;
    DetailLPBQ.Open;
    end;  }

    cxGrid1DBTableView1.Focused:=false;
  ViewKontrakQ.Refresh;
    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TKontrakFm.cxGrid1DBTableView2PlatNoPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
     ArmadaDropDownFm:=TArmadaDropdownfm.Create(Self);
    if ArmadaDropDownFm.ShowModal=MrOK then
    begin
      //ArmadaKontrakQ.Open;
      //ArmadaKontrakQ.Insert;
      ArmadaKontrakQ.Edit;
      ArmadaKontrakQKontrak.AsString:=ArmadaDropDownFm.kode;
      ArmadaKontrakQPlatNo.AsString:=ArmadaDropDownFm.plat;
      //ArmadaKontrakQ.Post;
    end;
    ArmadaDropDownFm.Release;
end;

procedure TKontrakFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date;
  ViewKontrakQ.Open;
end;

procedure TKontrakFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date;
  ViewKontrakQ.Open;
end;

procedure TKontrakFm.cxDBVerticalGrid1HargaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQHarga.AsCurrency:=DisplayValue;
HargaUnit:=MasterQHarga.AsCurrency;
if MasterQPPN.AsBoolean=true then
begin
    MasterQ.Edit;
    MasterQHarga.AsCurrency:=HargaUnit*110/100;
end;
end;

procedure TKontrakFm.cxDBVerticalGrid1PPNEditPropertiesChange(
  Sender: TObject);
begin
if MasterQPPN.AsBoolean=true then
begin
   MasterQ.Edit;
   MasterQHarga.AsCurrency:=HargaUnit*110/100;
end

else
begin
   MasterQ.Edit;
   MasterQHarga.AsCurrency:=HargaUnit;
end;

end;

end.
