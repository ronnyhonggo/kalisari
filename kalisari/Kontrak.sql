/*
   22 April 201310:24:19
   User: sa
   Server: TOSHIBA-PC\SQLEXPRESS
   Database: Kalisari
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Kontrak
	DROP CONSTRAINT FK_Kontrak_Pelanggan
GO
ALTER TABLE dbo.Pelanggan SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Pelanggan', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Pelanggan', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Pelanggan', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Kontrak
	DROP CONSTRAINT FK_Kontrak_User
GO
ALTER TABLE dbo.Kontrak
	DROP CONSTRAINT FK_Kontrak_User1
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[User]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Kontrak
	DROP CONSTRAINT FK_Kontrak_Rute
GO
ALTER TABLE dbo.Rute SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Rute', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Rute', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Rute', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Kontrak
	(
	Kode varchar(10) NOT NULL,
	Pelanggan varchar(10) NOT NULL,
	TglMulai datetime NULL,
	TglSelesai datetime NULL,
	StatusRute varchar(50) NULL,
	Rute varchar(10) NULL,
	AC bit NULL,
	Toilet bit NULL,
	AirSuspension bit NULL,
	KapasitasSeat int NULL,
	Harga money NULL,
	POEksternal varchar(200) NULL,
	Status varchar(50) NOT NULL,
	Keterangan varchar(MAX) NULL,
	IntervalPenagihan varchar(50) NULL,
	CreateDate datetime NULL,
	CreateBy varchar(10) NULL,
	Operator varchar(10) NULL,
	TglEntry datetime NULL,
	TglCetak datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Kontrak SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Kontrak)
	 EXEC('INSERT INTO dbo.Tmp_Kontrak (Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, AC, Toilet, AirSuspension, KapasitasSeat, Harga, Status, Keterangan, IntervalPenagihan, CreateDate, CreateBy, Operator, TglEntry, TglCetak)
		SELECT Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, AC, Toilet, AirSuspension, KapasitasSeat, Harga, Status, Keterangan, IntervalPenagihan, CreateDate, CreateBy, Operator, TglEntry, TglCetak FROM dbo.Kontrak WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.ArmadaKontrak
	DROP CONSTRAINT FK_ArmadaKontrak_Kontrak
GO
ALTER TABLE dbo.PenagihanAnjem
	DROP CONSTRAINT FK_PenagihanAnjem_Kontrak
GO
ALTER TABLE dbo.RealisasiTrayek
	DROP CONSTRAINT FK_RealisasiTrayek_Kontrak
GO
ALTER TABLE dbo.MasterSO
	DROP CONSTRAINT FK_MasterSO_Kontrak
GO
ALTER TABLE dbo.KasBonAnjem
	DROP CONSTRAINT FK_KasBonAnjem_Kontrak
GO
ALTER TABLE dbo.RealisasiAnjem
	DROP CONSTRAINT FK_RealisasiAnjem_Kontrak
GO
DROP TABLE dbo.Kontrak
GO
EXECUTE sp_rename N'dbo.Tmp_Kontrak', N'Kontrak', 'OBJECT' 
GO
ALTER TABLE dbo.Kontrak ADD CONSTRAINT
	PK_Kontrak PRIMARY KEY CLUSTERED 
	(
	Kode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Kontrak ADD CONSTRAINT
	FK_Kontrak_Rute FOREIGN KEY
	(
	Rute
	) REFERENCES dbo.Rute
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Kontrak ADD CONSTRAINT
	FK_Kontrak_User FOREIGN KEY
	(
	CreateBy
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Kontrak ADD CONSTRAINT
	FK_Kontrak_User1 FOREIGN KEY
	(
	Operator
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Kontrak ADD CONSTRAINT
	FK_Kontrak_Pelanggan FOREIGN KEY
	(
	Pelanggan
	) REFERENCES dbo.Pelanggan
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE TRIGGER [dbo].[KONTRAK_UPDATE] ON dbo.Kontrak
for UPDATE
AS
BEGIN
	update KONTRAK set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
CREATE TRIGGER [dbo].[KONTRAK_INSERT] ON dbo.Kontrak
for INSERT
AS
BEGIN
	update KONTRAK set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.RealisasiAnjem ADD CONSTRAINT
	FK_RealisasiAnjem_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.RealisasiAnjem SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.RealisasiAnjem', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.RealisasiAnjem', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.RealisasiAnjem', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.KasBonAnjem ADD CONSTRAINT
	FK_KasBonAnjem_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.KasBonAnjem SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.KasBonAnjem', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.KasBonAnjem', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.KasBonAnjem', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSO ADD CONSTRAINT
	FK_MasterSO_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.RealisasiTrayek ADD CONSTRAINT
	FK_RealisasiTrayek_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.RealisasiTrayek SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.RealisasiTrayek', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.RealisasiTrayek', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.RealisasiTrayek', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	FK_PenagihanAnjem_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PenagihanAnjem SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ArmadaKontrak ADD CONSTRAINT
	FK_ArmadaKontrak_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ArmadaKontrak SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ArmadaKontrak', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ArmadaKontrak', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ArmadaKontrak', 'Object', 'CONTROL') as Contr_Per 