unit KontrakDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TKontrakDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    KontrakQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQPOEksternal: TStringField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1TglMulai: TcxGridDBColumn;
    cxGrid1DBTableView1TglSelesai: TcxGridDBColumn;
    cxGrid1DBTableView1StatusRute: TcxGridDBColumn;
    cxGrid1DBTableView1AC: TcxGridDBColumn;
    cxGrid1DBTableView1Toilet: TcxGridDBColumn;
    cxGrid1DBTableView1AirSuspension: TcxGridDBColumn;
    cxGrid1DBTableView1KapasitasSeat: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1POEksternal: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView1IntervalPenagihan: TcxGridDBColumn;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    KontrakQDari: TStringField;
    KontrakQKe: TStringField;
    cxGrid1DBTableView1Dari: TcxGridDBColumn;
    cxGrid1DBTableView1Ke: TcxGridDBColumn;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    KontrakQNamaPelanggan: TStringField;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    KontrakQPlatNo: TStringField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    q:TSDQuery;
    a:string;
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
    constructor Create(aOwner: TComponent;Query1:string);overload;
  end;

var
  KontrakDropDownFm: TKontrakDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }
constructor TKontrakDropDownFm.Create(aOwner: TComponent; Query1: TSDQuery);
begin
  inherited Create(aOwner);
  q:=Query1;

end;

constructor TKontrakDropDownFm.Create(aOwner: TComponent; Query1: String);
begin
  inherited Create(aOwner);
  a:=Query1;

end;

procedure TKontrakDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TKontrakDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=KontrakQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TKontrakDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KontrakQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TKontrakDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=KontrakQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TKontrakDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KontrakQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TKontrakDropDownFm.FormCreate(Sender: TObject);
begin
PelangganQ.Open;
RuteQ.Open;
if a='' then
  DataSource1.DataSet:=q else
  begin
    KontrakQ.Close;
    KontrakQ.SQL.Text:=a;
    KontrakQ.Open;
  end;

end;

end.
