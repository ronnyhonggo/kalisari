object LPBFm: TLPBFm
  Left = 192
  Top = 44
  Width = 928
  Height = 676
  Caption = 'LPBFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 8
    Width = 912
    Height = 321
    Align = alCustom
    Caption = 'Panel1'
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object Panel2: TPanel
      Left = 8
      Top = 56
      Width = 313
      Height = 49
      Color = clBtnHighlight
      TabOrder = 1
      object RbtPO: TRadioButton
        Left = 16
        Top = 16
        Width = 49
        Height = 17
        Caption = 'PO'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        TabStop = True
        OnClick = RbtPOClick
      end
      object RbtCNC: TRadioButton
        Left = 88
        Top = 16
        Width = 129
        Height = 17
        Caption = 'Cash and Carry'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = RbtCNCClick
      end
      object RbtRetur: TRadioButton
        Left = 232
        Top = 16
        Width = 65
        Height = 17
        Caption = 'Retur'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = RbtReturClick
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 120
      Width = 320
      Height = 200
      Align = alCustom
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Pemeriksa: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Pemeriksa'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Penerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penerima'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1PO: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1POEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'PO'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1Retur: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1ReturEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Retur'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1CashNCarry: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'CashNCarry'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1TglTerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglTerima'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Keterangan'
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1Supplier: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SupplierEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Supplier'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 384
      Top = 1
      Width = 527
      Height = 319
      Align = alRight
      TabOrder = 3
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource3
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        object cxGrid1DBTableView1Barang: TcxGridDBColumn
          DataBinding.FieldName = 'Barang'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1BarangPropertiesButtonClick
          Width = 78
        end
        object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
          DataBinding.FieldName = 'Jumlah'
        end
        object cxGrid1DBTableView1JenisSesuai: TcxGridDBColumn
          DataBinding.FieldName = 'JenisSesuai'
          Width = 63
        end
        object cxGrid1DBTableView1KondisiBaik: TcxGridDBColumn
          DataBinding.FieldName = 'KondisiBaik'
          Width = 67
        end
        object cxGrid1DBTableView1JumlahSesuai: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahSesuai'
          Width = 69
        end
        object cxGrid1DBTableView1TepatWaktu: TcxGridDBColumn
          DataBinding.FieldName = 'TepatWaktu'
          Width = 64
        end
        object cxGrid1DBTableView1Diterima: TcxGridDBColumn
          DataBinding.FieldName = 'Diterima'
          Width = 49
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel3: TPanel
    Left = 8
    Top = 328
    Width = 913
    Height = 273
    Caption = 'Panel3'
    TabOrder = 1
    object cxGrid2: TcxGrid
      Left = -7
      Top = -7
      Width = 911
      Height = 224
      Align = alCustom
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        object cxGrid2DBTableView1Supplier: TcxGridDBColumn
          DataBinding.FieldName = 'Supplier'
        end
        object cxGrid2DBTableView1Pemeriksa: TcxGridDBColumn
          DataBinding.FieldName = 'Pemeriksa'
        end
        object cxGrid2DBTableView1Penerima: TcxGridDBColumn
          DataBinding.FieldName = 'Penerima'
        end
        object cxGrid2DBTableView1PO: TcxGridDBColumn
          DataBinding.FieldName = 'PO'
        end
        object cxGrid2DBTableView1CashNCarry: TcxGridDBColumn
          DataBinding.FieldName = 'CashNCarry'
        end
        object cxGrid2DBTableView1Retur: TcxGridDBColumn
          DataBinding.FieldName = 'Retur'
        end
        object cxGrid2DBTableView1TglTerima: TcxGridDBColumn
          DataBinding.FieldName = 'TglTerima'
        end
        object cxGrid2DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object BtnSave: TButton
      Left = 27
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BtnSaveClick
    end
    object BtnDelete: TButton
      Left = 136
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Delete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtnDeleteClick
    end
    object Button1: TButton
      Left = 248
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object ExitBtn: TButton
      Left = 359
      Top = 231
      Width = 97
      Height = 33
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = ExitBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 602
    Width = 921
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from LPB')
    UpdateObject = MasterUpdate
    Left = 40
    Top = 40
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object MasterQPemeriksa: TStringField
      FieldName = 'Pemeriksa'
      Required = True
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object MasterQPO: TStringField
      FieldName = 'PO'
      Size = 10
    end
    object MasterQRetur: TStringField
      FieldName = 'Retur'
      Size = 10
    end
    object MasterQCashNCarry: TStringField
      FieldName = 'CashNCarry'
      Size = 10
    end
    object MasterQTglTerima: TDateTimeField
      FieldName = 'TglTerima'
      Required = True
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Supplier, Pemeriksa, Penerima, PO, Retur, CashNCarr' +
        'y, TglTerima, Keterangan, CreateDate, CreateBy, Operator, TglEnt' +
        'ry, TglCetak, Status'
      'from LPB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update LPB'
      'set'
      '  Kode = :Kode,'
      '  Supplier = :Supplier,'
      '  Pemeriksa = :Pemeriksa,'
      '  Penerima = :Penerima,'
      '  PO = :PO,'
      '  Retur = :Retur,'
      '  CashNCarry = :CashNCarry,'
      '  TglTerima = :TglTerima,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into LPB'
      
        '  (Kode, Supplier, Pemeriksa, Penerima, PO, Retur, CashNCarry, T' +
        'glTerima, Keterangan, CreateDate, CreateBy, Operator, TglEntry, ' +
        'TglCetak, Status)'
      'values'
      
        '  (:Kode, :Supplier, :Pemeriksa, :Penerima, :PO, :Retur, :CashNC' +
        'arry, :TglTerima, :Keterangan, :CreateDate, :CreateBy, :Operator' +
        ', :TglEntry, :TglCetak, :Status)')
    DeleteSQL.Strings = (
      'delete from LPB'
      'where'
      '  Kode = :OLD_Kode')
    Left = 120
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 80
    Top = 40
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from lpb order by kode desc')
    Left = 160
    Top = 40
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ViewLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from LPB')
    Left = 480
    Top = 512
    object ViewLPBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewLPBQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object ViewLPBQPemeriksa: TStringField
      FieldName = 'Pemeriksa'
      Required = True
      Size = 10
    end
    object ViewLPBQPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object ViewLPBQPO: TStringField
      FieldName = 'PO'
      Size = 10
    end
    object ViewLPBQRetur: TStringField
      FieldName = 'Retur'
      Size = 10
    end
    object ViewLPBQCashNCarry: TStringField
      FieldName = 'CashNCarry'
      Size = 10
    end
    object ViewLPBQTglTerima: TDateTimeField
      FieldName = 'TglTerima'
      Required = True
    end
    object ViewLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewLPBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewLPBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewLPBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewLPBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewLPBQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewLPBQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = ViewLPBQ
    Left = 520
    Top = 520
  end
  object DetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from DetailLPB where kodelpb=:text')
    UpdateObject = UpdateDetailLPB
    Left = 488
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailLPBQKodeLPB: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object DetailLPBQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailLPBQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailLPBQJenisSesuai: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object DetailLPBQKondisiBaik: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object DetailLPBQJumlahSesuai: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object DetailLPBQTepatWaktu: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object DetailLPBQDiterima: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object DetailLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object UpdateDetailLPB: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, Jumlah' +
        'Sesuai, TepatWaktu, Diterima, Keterangan'
      'from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update DetailLPB'
      'set'
      '  KodeLPB = :KodeLPB,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  JenisSesuai = :JenisSesuai,'
      '  KondisiBaik = :KondisiBaik,'
      '  JumlahSesuai = :JumlahSesuai,'
      '  TepatWaktu = :TepatWaktu,'
      '  Diterima = :Diterima,'
      '  Keterangan = :Keterangan'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into DetailLPB'
      
        '  (KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, JumlahSesu' +
        'ai, TepatWaktu, Diterima, Keterangan)'
      'values'
      
        '  (:KodeLPB, :Barang, :Jumlah, :JenisSesuai, :KondisiBaik, :Juml' +
        'ahSesuai, :TepatWaktu, :Diterima, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    Left = 560
    Top = 280
  end
  object DataSource3: TDataSource
    DataSet = DetailLPBQ
    Left = 528
    Top = 280
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 344
    Top = 240
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object POQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from PO')
    Left = 336
    Top = 200
    object POQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object POQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object POQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object POQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object POQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object POQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object POQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object POQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object POQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object POQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object POQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object POQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object POQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object POQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object POQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
  object ReturQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Retur')
    Left = 344
    Top = 280
    object ReturQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ReturQTglRetur: TDateTimeField
      FieldName = 'TglRetur'
      Required = True
    end
    object ReturQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Required = True
      Size = 10
    end
    object ReturQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ReturQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ReturQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ReturQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ReturQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ReturQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ReturQKodePO: TStringField
      FieldName = 'KodePO'
      Size = 10
    end
    object ReturQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object ReturQRetur: TBooleanField
      FieldName = 'Retur'
    end
    object ReturQComplaint: TBooleanField
      FieldName = 'Complaint'
    end
  end
  object CashNCarryQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from CashNCarry')
    Left = 336
    Top = 160
    object CashNCarryQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CashNCarryQKasBon: TCurrencyField
      FieldName = 'KasBon'
      Required = True
    end
    object CashNCarryQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object CashNCarryQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CashNCarryQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CashNCarryQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CashNCarryQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CashNCarryQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object CashNCarryQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Supplier')
    Left = 336
    Top = 128
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Barang')
    Left = 336
    Top = 88
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangPOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select db.Barang from DaftarBeli db left join DetailPO dp on dp.' +
        'KodeDaftarBeli=db.Kode where dp.KodePO=:text')
    Left = 344
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object BarangPOQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
  end
  object BarangCNCQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select db.Barang from DaftarBeli db left join DetailCashNCarry d' +
        'p on dp.KodeDaftarBeli=db.Kode where dp.KodeCNC=:text')
    Left = 392
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object BarangCNCQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
  end
  object KodeDetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kodeLPB from Detaillpb order by kodeLPB desc')
    Left = 592
    Top = 288
    object KodeDetailLPBQkodeLPB: TStringField
      FieldName = 'kodeLPB'
      Required = True
      Size = 10
    end
  end
  object DeleteDetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailLPB where KodeLPB=:text')
    Left = 648
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailLPBQKodeLPB: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object DeleteDetailLPBQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DeleteDetailLPBQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DeleteDetailLPBQJenisSesuai: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object DeleteDetailLPBQKondisiBaik: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object DeleteDetailLPBQJumlahSesuai: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object DeleteDetailLPBQTepatWaktu: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object DeleteDetailLPBQDiterima: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object DeleteDetailLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object UpdateDeleteLPB: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, Jumlah' +
        'Sesuai, TepatWaktu, Diterima, Keterangan'
      'from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update DetailLPB'
      'set'
      '  KodeLPB = :KodeLPB,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  JenisSesuai = :JenisSesuai,'
      '  KondisiBaik = :KondisiBaik,'
      '  JumlahSesuai = :JumlahSesuai,'
      '  TepatWaktu = :TepatWaktu,'
      '  Diterima = :Diterima,'
      '  Keterangan = :Keterangan'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into DetailLPB'
      
        '  (KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, JumlahSesu' +
        'ai, TepatWaktu, Diterima, Keterangan)'
      'values'
      
        '  (:KodeLPB, :Barang, :Jumlah, :JenisSesuai, :KondisiBaik, :Juml' +
        'ahSesuai, :TepatWaktu, :Diterima, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    Left = 712
    Top = 344
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailLPB where KodeLPB=:text')
    UpdateObject = UpdateDeleteLPB
    Left = 672
    Top = 352
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StringField1: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BooleanField1: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object BooleanField2: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object BooleanField3: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object BooleanField4: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object BooleanField5: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
