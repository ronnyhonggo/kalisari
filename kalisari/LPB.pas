unit LPB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, StdCtrls, ComCtrls, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxInplaceContainer, cxVGrid, cxDBVGrid,
  cxTextEdit, cxMaskEdit, cxButtonEdit, ExtCtrls, SDEngine;

type
  TLPBFm = class(TForm)
    Panel1: TPanel;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    Panel2: TPanel;
    RbtPO: TRadioButton;
    RbtCNC: TRadioButton;
    RbtRetur: TRadioButton;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel3: TPanel;
    StatusBar: TStatusBar;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    BtnSave: TButton;
    BtnDelete: TButton;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQSupplier: TStringField;
    MasterQPemeriksa: TStringField;
    MasterQPenerima: TStringField;
    MasterQPO: TStringField;
    MasterQRetur: TStringField;
    MasterQCashNCarry: TStringField;
    MasterQTglTerima: TDateTimeField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatus: TStringField;
    MasterUpdate: TSDUpdateSQL;
    DataSource1: TDataSource;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ViewLPBQ: TSDQuery;
    DataSource2: TDataSource;
    ExitBtn: TButton;
    cxDBVerticalGrid1Pemeriksa: TcxDBEditorRow;
    cxDBVerticalGrid1Penerima: TcxDBEditorRow;
    cxDBVerticalGrid1PO: TcxDBEditorRow;
    cxDBVerticalGrid1Retur: TcxDBEditorRow;
    cxDBVerticalGrid1CashNCarry: TcxDBEditorRow;
    cxDBVerticalGrid1TglTerima: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxDBVerticalGrid1Supplier: TcxDBEditorRow;
    DetailLPBQ: TSDQuery;
    UpdateDetailLPB: TSDUpdateSQL;
    DataSource3: TDataSource;
    DetailLPBQKodeLPB: TStringField;
    DetailLPBQBarang: TStringField;
    DetailLPBQJumlah: TIntegerField;
    DetailLPBQJenisSesuai: TBooleanField;
    DetailLPBQKondisiBaik: TBooleanField;
    DetailLPBQJumlahSesuai: TBooleanField;
    DetailLPBQTepatWaktu: TBooleanField;
    DetailLPBQDiterima: TBooleanField;
    DetailLPBQKeterangan: TMemoField;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1JenisSesuai: TcxGridDBColumn;
    cxGrid1DBTableView1KondisiBaik: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahSesuai: TcxGridDBColumn;
    cxGrid1DBTableView1TepatWaktu: TcxGridDBColumn;
    cxGrid1DBTableView1Diterima: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    POQ: TSDQuery;
    ReturQ: TSDQuery;
    POQKode: TStringField;
    POQSupplier: TStringField;
    POQTermin: TStringField;
    POQTglKirim: TDateTimeField;
    POQGrandTotal: TCurrencyField;
    POQStatus: TStringField;
    POQCreateDate: TDateTimeField;
    POQCreateBy: TStringField;
    POQOperator: TStringField;
    POQTglEntry: TDateTimeField;
    POQTglCetak: TDateTimeField;
    POQStatusKirim: TStringField;
    POQDiskon: TCurrencyField;
    POQKirim: TBooleanField;
    POQAmbil: TBooleanField;
    ReturQKode: TStringField;
    ReturQTglRetur: TDateTimeField;
    ReturQPelaksana: TStringField;
    ReturQKeterangan: TMemoField;
    ReturQCreateDate: TDateTimeField;
    ReturQCreateBy: TStringField;
    ReturQOperator: TStringField;
    ReturQTglEntry: TDateTimeField;
    ReturQTglCetak: TDateTimeField;
    ReturQKodePO: TStringField;
    ReturQStatus: TStringField;
    ReturQRetur: TBooleanField;
    ReturQComplaint: TBooleanField;
    CashNCarryQ: TSDQuery;
    CashNCarryQKode: TStringField;
    CashNCarryQKasBon: TCurrencyField;
    CashNCarryQPenyetuju: TStringField;
    CashNCarryQCreateDate: TDateTimeField;
    CashNCarryQCreateBy: TStringField;
    CashNCarryQOperator: TStringField;
    CashNCarryQTglEntry: TDateTimeField;
    CashNCarryQTglCetak: TDateTimeField;
    CashNCarryQStatus: TStringField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    ViewLPBQKode: TStringField;
    ViewLPBQSupplier: TStringField;
    ViewLPBQPemeriksa: TStringField;
    ViewLPBQPenerima: TStringField;
    ViewLPBQPO: TStringField;
    ViewLPBQRetur: TStringField;
    ViewLPBQCashNCarry: TStringField;
    ViewLPBQTglTerima: TDateTimeField;
    ViewLPBQKeterangan: TMemoField;
    ViewLPBQCreateDate: TDateTimeField;
    ViewLPBQCreateBy: TStringField;
    ViewLPBQOperator: TStringField;
    ViewLPBQTglEntry: TDateTimeField;
    ViewLPBQTglCetak: TDateTimeField;
    ViewLPBQStatus: TStringField;
    cxGrid2DBTableView1Supplier: TcxGridDBColumn;
    cxGrid2DBTableView1Pemeriksa: TcxGridDBColumn;
    cxGrid2DBTableView1Penerima: TcxGridDBColumn;
    cxGrid2DBTableView1PO: TcxGridDBColumn;
    cxGrid2DBTableView1Retur: TcxGridDBColumn;
    cxGrid2DBTableView1CashNCarry: TcxGridDBColumn;
    cxGrid2DBTableView1TglTerima: TcxGridDBColumn;
    cxGrid2DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TIntegerField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangPOQ: TSDQuery;
    BarangPOQBarang: TStringField;
    BarangCNCQ: TSDQuery;
    BarangCNCQBarang: TStringField;
    KodeDetailLPBQ: TSDQuery;
    KodeDetailLPBQkodeLPB: TStringField;
    DeleteDetailLPBQ: TSDQuery;
    UpdateDeleteLPB: TSDUpdateSQL;
    DeleteDetailLPBQKodeLPB: TStringField;
    DeleteDetailLPBQBarang: TStringField;
    DeleteDetailLPBQJumlah: TIntegerField;
    DeleteDetailLPBQJenisSesuai: TBooleanField;
    DeleteDetailLPBQKondisiBaik: TBooleanField;
    DeleteDetailLPBQJumlahSesuai: TBooleanField;
    DeleteDetailLPBQTepatWaktu: TBooleanField;
    DeleteDetailLPBQDiterima: TBooleanField;
    DeleteDetailLPBQKeterangan: TMemoField;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    BooleanField5: TBooleanField;
    MemoField1: TMemoField;
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExitBtnClick(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure RbtPOClick(Sender: TObject);
    procedure RbtCNCClick(Sender: TObject);
    procedure RbtReturClick(Sender: TObject);
    procedure cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1POEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1ReturEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid1DBTableView1BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure BtnDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LPBFm: TLPBFm;
    MasterOriSQL: string;
  paramkode :string;
  DetailLPBQOriSQL: string;
  jenis : string;

implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

procedure TLPBFm.KodeEditEnter(Sender: TObject);
begin
KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  BtnSave.Enabled:=false;
 // BtnKepalaTeknik.Enabled:=false;
  //BtnSetuju.Enabled:=false;
 // BtnDelete.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TLPBFm.KodeEditExit(Sender: TObject);
begin
if KodeEdit.Text<>'' then
  begin
   MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //DMFm.GetDateQ.Open;
      //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;

      //DMFm.GetDateQ.Close;
      btnsave.Enabled:=menuutamafm.UserQInsertLPB.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    BtnSave.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
  end;
end;

procedure TLPBFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
release;
end;

procedure TLPBFm.FormCreate(Sender: TObject);
begin
MasterOriSQL:=MasterQ.SQL.Text;
DetailLPBQOriSQL:=DetailLPBQ.SQL.Text;
  //DetailBonBarangOriSQL:=DetailBonBarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TLPBFm.FormShow(Sender: TObject);
begin
KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  ViewLPBQ.Active:=TRUE;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertLPB.AsBoolean;
  BtnSave.Enabled:=menuutamafm.UserQUpdateLPB.AsBoolean;
  BtnDelete.Enabled:=menuutamafm.UserQDeleteLPB.AsBoolean;

     cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
   cxDBVerticalGrid1Penerima.Visible:=FALSE;
   cxDBVerticalGrid1PO.Visible:=FALSE;
   cxDBVerticalGrid1Retur.Visible:=FALSE;
   cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
   cxDBVerticalGrid1TglTerima.Visible:=FALSE;
   cxDBVerticalGrid1Keterangan.Visible:=FALSE;
   cxDBVerticalGrid1Supplier.Visible:=FALSE;

   cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
   cxDBVerticalGrid1Penerima.Visible:=TRUE;
   cxDBVerticalGrid1PO.Visible:=TRUE;
      cxDBVerticalGrid1TglTerima.Visible:=TRUE;
   cxDBVerticalGrid1Keterangan.Visible:=TRUE;
   cxDBVerticalGrid1Supplier.Visible:=TRUE;
  //RbtSPK.Checked:=TRUE;
end;

procedure TLPBFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TLPBFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TLPBFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  //cxDBVerticalGrid1.SetFocus;
end;

procedure TLPBFm.RbtPOClick(Sender: TObject);
begin
   cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
   cxDBVerticalGrid1Penerima.Visible:=FALSE;
   cxDBVerticalGrid1PO.Visible:=FALSE;
   cxDBVerticalGrid1Retur.Visible:=FALSE;
   cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
   cxDBVerticalGrid1TglTerima.Visible:=FALSE;
   cxDBVerticalGrid1Keterangan.Visible:=FALSE;
   cxDBVerticalGrid1Supplier.Visible:=FALSE;

   cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
   cxDBVerticalGrid1Penerima.Visible:=TRUE;
   cxDBVerticalGrid1PO.Visible:=TRUE;
      cxDBVerticalGrid1TglTerima.Visible:=TRUE;
   cxDBVerticalGrid1Keterangan.Visible:=TRUE;
   cxDBVerticalGrid1Supplier.Visible:=TRUE;
end;

procedure TLPBFm.RbtCNCClick(Sender: TObject);
begin
   cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
   cxDBVerticalGrid1Penerima.Visible:=FALSE;
   cxDBVerticalGrid1PO.Visible:=FALSE;
   cxDBVerticalGrid1Retur.Visible:=FALSE;
   cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
   cxDBVerticalGrid1TglTerima.Visible:=FALSE;
   cxDBVerticalGrid1Keterangan.Visible:=FALSE;
   cxDBVerticalGrid1Supplier.Visible:=FALSE;

      cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
   cxDBVerticalGrid1Penerima.Visible:=TRUE;
    cxDBVerticalGrid1CashNCarry.Visible:=TRUE;
       cxDBVerticalGrid1TglTerima.Visible:=TRUE;
   cxDBVerticalGrid1Keterangan.Visible:=TRUE;
   cxDBVerticalGrid1Supplier.Visible:=TRUE;
end;

procedure TLPBFm.RbtReturClick(Sender: TObject);
begin
   cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
   cxDBVerticalGrid1Penerima.Visible:=FALSE;
   cxDBVerticalGrid1PO.Visible:=FALSE;
   cxDBVerticalGrid1Retur.Visible:=FALSE;
   cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
   cxDBVerticalGrid1TglTerima.Visible:=FALSE;
   cxDBVerticalGrid1Keterangan.Visible:=FALSE;
   cxDBVerticalGrid1Supplier.Visible:=FALSE;

      cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
   cxDBVerticalGrid1Penerima.Visible:=TRUE;
   cxDBVerticalGrid1Retur.Visible:=TRUE;
    cxDBVerticalGrid1PO.Visible:=TRUE;
       cxDBVerticalGrid1TglTerima.Visible:=TRUE;
   cxDBVerticalGrid1Keterangan.Visible:=TRUE;
   cxDBVerticalGrid1Supplier.Visible:=TRUE;
end;

procedure TLPBFm.cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPemeriksa.AsString:=PegawaiQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.cxDBVerticalGrid1POEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
POQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  //POQ.ExecSQL;
  POQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,POQ);
    if DropDownFm.ShowModal=MrOK then
    begin
     MasterQ.Open;
     MasterQ.Edit;
      MasterQPO.AsString:=POQKode.AsString;
      MasterQSupplier.AsString:=POQSupplier.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.cxDBVerticalGrid1ReturEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
ReturQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
 ReturQ.ExecSQL;
 ReturQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,ReturQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRetur.AsString:=ReturQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  CashNCarryQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
 CashNCarryQ.ExecSQL;
 CashNCarryQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,CashNCarryQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQCashNCarry.AsString:=CashNCarryQKode.AsString;
      
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
 SupplierQ.ExecSQL;
 SupplierQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,SupplierQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSupplier.AsString:=SupplierQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLPBFm.BtnSaveClick(Sender: TObject);
begin
 if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
   // showmessage ('Tes3');
    DetailLPBQ.Open;
    DetailLPBQ.Edit;
    DetailLPBQ.First;
   // showmessage ('Tes4');
    while DetailLPBQ.Eof=false do
    begin
      DetailLPBQ.Edit;
      DetailLPBQKodeLPB.AsString:=KodeEdit.Text;
        detailLPBQ.Post;
      DetailLPBQ.Next;
    //  showmessage ('Tes2');
    end;

   // showmessage ('Tes');
    DetailLPBQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailLPBQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    //cxButtonEdit1PropertiesButtonClick(sender,0);

 except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

    //MenuUtamaFm.Database1.Rollback;
   // MasterQ.RollbackUpdates;
    //ShowMessage('Penyimpanan Gagal');


  end;
  ViewLPBQ.Close;
  ViewLPBQ.ExecSQL;
  ViewLPBQ.Open;
end;

procedure TLPBFm.cxGrid1DBTableView1BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL:string;
begin
BarangQ.Close;
tempSQL:=BarangQ.SQL.Text;
BarangQ.SQL.Clear;
if rbtPO.Checked=TRUE then
begin
   barangQ.SQL.Text:='select b.* from barang b, PO p, DetailPO dp, DaftarBeli db where dp.KodePO=p.kode and dp.KodeDaftarBeli=db.kode and b.kode=db.barang and p.status<>"FINISHED"';
end
else if rbtCNC.Checked=TRUE then
begin
  barangQ.SQL.Text:='select b.* from barang b, CashNCarry cnc, DetailCashNCarry dc, DaftarBeli db where dc.KodeCNC=cnc.kode and dc.KodeDaftarBeli=db.kode and b.kode=db.barang and cnc.status<>"FINISHED"';
end
else if RbtRetur.Checked=TRUE then
begin
  barangQ.SQL.Text:='select b.* from barang b, Retur r, DetailRetur dr where dr.KodeRetur=r.kode and b.kode=dr.barang and r.status<>"FINISHED"';
end;
{if jenis='Retur' then
begin
//BarangQ.SQL.Add('select distinct Barang.* from Barang,DetailRetur where barang.Kode=DetailRetur.Barang and DetailRetur.KodeRetur=MasterQRetur');
end

else if jenis='CNC' then
begin
 //BarangQ.SQL.Add('select distinct Barang.* from Barang,DetailCashNCarry where barang.Kode=DetailCashNCarry.Barang and DetailRetur.KodeRetur=MasterQRetur');
       BarangCNCQ.Close;
      BarangCNCQ.ParamByName('text').AsString := MasterQCashNCarry.AsString;
      BarangCNCQ.Open;
 end

else
begin
 //BarangQ.SQL.Add('select db.Barang from DaftarBeli db left join DetailPO dp on dp.KodeDaftarBeli=db.Kode where dp.KodePO='+QuotedStr(MasterQPO.AsString));
      BarangPOQ.Close;
      BarangPOQ.ParamByName('text').AsString := MasterQPO.AsString;
      BarangPOQ.Open;
 end;

//BarangQ.SQL.Text:='select distinct Barang.* from Barang,DetailBonBarang where barang.Kode=DetailBonBarang.KodeBarang and DetailBonBarang.KodeBonBarang=';
//BarangQ.Open;
if jenis='PO' then
begin
DropDownFm:=TDropdownfm.Create(Self,BarangPOQ);
end

else if jenis='CNC' then
begin
DropDownFm:=TDropdownfm.Create(Self,BarangCNCQ);
end;  }
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
    //DetailLPBQ.Close;
    //DetailLPBQ.Open;
    DetailLPBQ.Insert;
    DetailLPBQBarang.AsString:=BarangQKode.AsString;
     //CobaQ.Close;
      //CobaQ.ParamByName('text').AsString := BarangQKode.AsString;
      //CobaQ.ParamByName('text').AsString := DetailBonBarangQKodeBarang.AsString;
      //ShowMessage(CobaQ.ParamByName('text').AsString );
      //CobaQ.ParamByName('text2').AsString:=DetailBonBarangQKodeBonBarang.AsString;
     //CobaQ.Open;
    {if jenis='PO' then
    begin
    DetailLPBQBarang.AsString:=BarangPOQBarang.AsString;
    end

    else if jenis='CNC' then
    begin
     DetailLPBQBarang.AsString:=BarangCNCQBarang.AsString;
    end; }
   // DetailBonKeluarBarangQKodeBKB.AsString:=MasterQBonBarang.AsString;
    //DetailBonKeluarBarangQDiminta.AsInteger:= CobaQDiminta.AsInteger;

    end;
        DropDownFm.Release;
    BarangQ.SQL.Text:=tempSQL;
end;

procedure TLPBFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewLPBQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;

    end;


    BtnSave.Enabled:=True;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    cxDBVerticalGrid1.Enabled:=True;
    DetailLPBQ.Close;
    //DetailBonKeluarBarangQ.ParamByName('text').AsString := MasterQKode.AsString;
    //DetailLPBQ.Open;
        //DetailLPBQ.Close;
    DetailLPBQ.ParamByName('text').AsString := MasterQKode.AsString;
    DetailLPBQ.Open;
    KodeEdit.Text:=MasterQKode.AsString;

    if MasterQRetur.AsString <> '' then
begin
jenis:='Retur';
end

else if MasterQCashNCarry.AsString <> '' then
begin
jenis:='CNC';
 end

else
begin
jenis:='PO';
 end;

If jenis='Retur' then
begin
 RbtPO.Checked:=FALSE;
 RBTCNC.Checked:=FALSE;
 RBTRetur.Checked:=TRUE;
end

else If jenis='CNC' then
begin
 RbtPO.Checked:=FALSE;
 RBTCNC.Checked:=TRUE;
 RBTRetur.Checked:=FALSE;
end

else
begin
 RbtPO.Checked:=TRUE;
 RBTCNC.Checked:=FALSE;
 RBTRetur.Checked:=FALSE;
end;

end;

procedure TLPBFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
  DetailLPBQ.Edit;

  //KodeDetailLPBQ.Close;
  //KodeDetailLPBQ.Open;

  {if KodeDetailLPBQkodeLPB.AsString='' then
  begin
      KodeDetailLPBQKodeLPB.AsString:='0000000000';
  end;}

  //DetailLPBQKodeLPB.AsString:= DMFm.Fill(InttoStr(StrtoInt(KodeDetailLPBQkodeLPB.AsString)+1),10,'0',True);;


 // ShowMessage(DetailLPBQKodeLPB.AsString);
    DetailLPBQKodeLPB.AsString:='0';
    DetailLPBQ.Post;

   { MenuUtamaFm.Database1.StartTransaction;
    try
      DetailLPBQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailLPBQ.CommitUpdates;
      except
      MenuUtamaFm.Database1.Rollback;
      DetailLPBQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    DetailLPBQ.Close;
    DetailLPBQ.SQL.Text:=DetailLPBQOriSQL;
   //DetailLPBQ.ExecSQL;
    DetailLPBQ.Open;
    end;  }

    cxGrid1DBTableView1.Focused:=false;
  viewLPBQ.Refresh;
    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TLPBFm.BtnDeleteClick(Sender: TObject);
begin
if MessageDlg('Hapus LPB '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteDetailLPBQ.Close;
      DeleteDetailLPBQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DeleteDetailLPBQ.Open;
      DeleteDetailLPBQ.First;
       while DeleteDetailLPBQ.Eof=FALSE
       do
       begin
        SDQuery1.SQL.Text:=('delete from DetailLPB where KodeLPB='+QuotedStr(KodeEdit.Text)+' and Barang='+QuotedStr(DeleteDetailLPBQBarang.AsString));
        SDQuery1.ExecSQL;
        DeleteDetailLPBQ.Next;
       end;
       {DeleteDetailLPBQ.Open;
       DeleteDetailLPBQ.First;
       while DeleteDetailLPBQ.Eof=FALSE
       do
       begin
       DeleteDetailLPBQ.Delete;
       DeleteDetailLPBQ.Next;
       end;
       showMessage('A');
       DeleteDetailLPBQ.ApplyUpdates;}
       //showMessage('B');
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('LPB telah dihapus.',mtInformation,[mbOK],0);
       DeleteDetailLPBQ.Close;

     except
       on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

      { MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('LPB pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
       }
     end;
     KodeEdit.SetFocus;
     ViewLPBQ.Close;
  ViewLPBQ.ExecSQL;
  ViewLPBQ.Open;
  end;
end;

end.
