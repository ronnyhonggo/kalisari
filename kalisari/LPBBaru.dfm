object LPBBaruFm: TLPBBaruFm
  Left = 163
  Top = 37
  Width = 1198
  Height = 676
  Caption = 'LPBBaruFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1182
    Height = 345
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 336
      Height = 343
      Align = alLeft
      Caption = 'Panel4'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 29
        Width = 24
        Height = 13
        Caption = 'Kode'
      end
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 1
        Top = 120
        Width = 334
        Height = 222
        Align = alBottom
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsView.RowHeaderWidth = 183
        OptionsBehavior.GoToNextCellOnTab = True
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = DataSource1
        Version = 1
        object cxDBVerticalGrid1Pemeriksa: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Pemeriksa'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1NamaPemeriksa: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'NamaPemeriksa'
          Properties.Options.Editing = False
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1JabatanPemeriksa: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'JabatanPemeriksa'
          Properties.Options.Editing = False
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1Penerima: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Penerima'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 3
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1NamaPenerima: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'NamaPenerima'
          Properties.Options.Editing = False
          ID = 4
          ParentID = 3
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1JabatanPenerima: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'JabatanPenerima'
          Properties.Options.Editing = False
          ID = 5
          ParentID = 3
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1PO: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1POEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'PO'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 6
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid1Retur: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1ReturEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Retur'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 7
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid1CashNCarry: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'CashNCarry'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 8
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1Verpal: TcxDBEditorRow
          Properties.Caption = 'Storing'
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1VerpalEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Verpal'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 9
          ParentID = -1
          Index = 5
          Version = 1
        end
        object cxDBVerticalGrid1DikerjakanLuar: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
          Properties.EditProperties.ImmediatePost = True
          Properties.EditProperties.OnChange = cxDBVerticalGrid1DikerjakanLuarEditPropertiesChange
          Properties.DataBinding.FieldName = 'DikerjakanLuar'
          ID = 10
          ParentID = -1
          Index = 6
          Version = 1
        end
        object cxDBVerticalGrid1SupplierBarangRusak: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SupplierBarangRusakEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'SupplierBarangRusak'
          Styles.Content = MenuUtamaFm.cxStyle5
          Visible = False
          ID = 11
          ParentID = -1
          Index = 7
          Version = 1
        end
        object cxDBVerticalGrid1NamaSupplierBrgBekas: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'NamaSupplierBrgBekas'
          ID = 12
          ParentID = 11
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1BiayaPekerjaanLuar: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'BiayaPekerjaanLuar'
          Styles.Content = MenuUtamaFm.cxStyle5
          Visible = False
          ID = 13
          ParentID = -1
          Index = 8
          Version = 1
        end
        object cxDBVerticalGrid1KeteranganPekerjaanLuar: TcxDBEditorRow
          Height = 40
          Properties.DataBinding.FieldName = 'KeteranganPekerjaanLuar'
          Styles.Content = MenuUtamaFm.cxStyle5
          Visible = False
          ID = 14
          ParentID = -1
          Index = 9
          Version = 1
        end
        object cxDBVerticalGrid1TglTerima: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'TglTerima'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 15
          ParentID = -1
          Index = 10
          Version = 1
        end
        object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
          Height = 36
          Properties.DataBinding.FieldName = 'Keterangan'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 16
          ParentID = -1
          Index = 11
          Version = 1
        end
        object cxDBVerticalGrid1Supplier: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SupplierEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Supplier'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 17
          ParentID = -1
          Index = 12
          Version = 1
        end
        object cxDBVerticalGrid1NamaSupplier: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'NamaSupplier'
          Properties.Options.Editing = False
          ID = 18
          ParentID = 17
          Index = 0
          Version = 1
        end
      end
      object Panel2: TPanel
        Left = 1
        Top = 56
        Width = 334
        Height = 64
        Align = alBottom
        Color = clBtnHighlight
        TabOrder = 1
        object RbtPO: TRadioButton
          Left = 16
          Top = 16
          Width = 49
          Height = 17
          Caption = 'PO'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          TabStop = True
          OnClick = RbtPOClick
        end
        object RbtCNC: TRadioButton
          Left = 88
          Top = 16
          Width = 129
          Height = 17
          Caption = 'Cash and Carry'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = RbtCNCClick
        end
        object RbtRetur: TRadioButton
          Left = 232
          Top = 16
          Width = 65
          Height = 17
          Caption = 'Retur'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = RbtReturClick
        end
        object RbtStoring: TRadioButton
          Left = 16
          Top = 40
          Width = 73
          Height = 17
          Caption = 'Verpal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = RbtStoringClick
        end
        object rbtnBarangRusak: TRadioButton
          Left = 88
          Top = 40
          Width = 129
          Height = 17
          Caption = 'Barang Rusak'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = rbtnBarangRusakClick
        end
      end
      object KodeEdit: TcxButtonEdit
        Left = 48
        Top = 26
        Properties.Buttons = <
          item
            Caption = '+'
            Default = True
            Kind = bkText
          end>
        Properties.OnButtonClick = KodeEditPropertiesButtonClick
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebs3D
        Style.HotTrack = False
        Style.ButtonStyle = bts3D
        TabOrder = 2
        OnEnter = KodeEditEnter
        OnExit = KodeEditExit
        OnKeyDown = KodeEditKeyDown
        Width = 121
      end
    end
    object Panel5: TPanel
      Left = 337
      Top = 1
      Width = 844
      Height = 343
      Align = alClient
      Caption = 'Panel5'
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 842
        Height = 341
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = False
          Navigator.Buttons.Next.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Visible = False
          Navigator.Buttons.Post.Visible = True
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.Visible = True
          DataController.DataSource = DataSource3
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NewItemRow.Visible = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1Barang: TcxGridDBColumn
            DataBinding.FieldName = 'Barang'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxGrid1DBTableView1BarangPropertiesButtonClick
            Width = 78
          end
          object cxGrid1DBTableView1Nama: TcxGridDBColumn
            DataBinding.FieldName = 'Nama'
            Width = 120
          end
          object cxGrid1DBTableView1TotalDipesan: TcxGridDBColumn
            DataBinding.FieldName = 'TotalDipesan'
            Width = 70
          end
          object cxGrid1DBTableView1SisaDipesan: TcxGridDBColumn
            DataBinding.FieldName = 'SisaDipesan'
          end
          object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
            DataBinding.FieldName = 'Jumlah'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.OnValidate = cxGrid1DBTableView1JumlahPropertiesValidate
          end
          object cxGrid1DBTableView1JenisSesuai: TcxGridDBColumn
            DataBinding.FieldName = 'JenisSesuai'
            Width = 63
          end
          object cxGrid1DBTableView1KondisiBaik: TcxGridDBColumn
            DataBinding.FieldName = 'KondisiBaik'
            Width = 67
          end
          object cxGrid1DBTableView1JumlahSesuai: TcxGridDBColumn
            DataBinding.FieldName = 'JumlahSesuai'
            Width = 69
          end
          object cxGrid1DBTableView1TepatWaktu: TcxGridDBColumn
            DataBinding.FieldName = 'TepatWaktu'
            Width = 64
          end
          object cxGrid1DBTableView1Diterima: TcxGridDBColumn
            DataBinding.FieldName = 'Diterima'
            Width = 49
          end
          object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
            DataBinding.FieldName = 'Keterangan'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 345
    Width = 1182
    Height = 274
    Align = alClient
    Caption = 'Panel3'
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 29
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1180
      Height = 224
      Align = alTop
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Supplier: TcxGridDBColumn
          DataBinding.FieldName = 'Supplier'
        end
        object cxGrid2DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPemeriksa'
          Width = 144
        end
        object cxGrid2DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenerima'
          Width = 184
        end
        object cxGrid2DBTableView1PO: TcxGridDBColumn
          DataBinding.FieldName = 'PO'
        end
        object cxGrid2DBTableView1CashNCarry: TcxGridDBColumn
          DataBinding.FieldName = 'CashNCarry'
        end
        object cxGrid2DBTableView1Retur: TcxGridDBColumn
          DataBinding.FieldName = 'Retur'
        end
        object cxGrid2DBTableView1Verpal: TcxGridDBColumn
          DataBinding.FieldName = 'Verpal'
        end
        object cxGrid2DBTableView1DikerjakanLuar: TcxGridDBColumn
          DataBinding.FieldName = 'DikerjakanLuar'
          Width = 105
        end
        object cxGrid2DBTableView1SupplierBarangRusak: TcxGridDBColumn
          DataBinding.FieldName = 'SupplierBarangRusak'
          Width = 94
        end
        object cxGrid2DBTableView1TglTerima: TcxGridDBColumn
          DataBinding.FieldName = 'TglTerima'
        end
        object cxGrid2DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 78
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 137
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object BtnSave: TButton
      Left = 27
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BtnSaveClick
    end
    object BtnDelete: TButton
      Left = 136
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Delete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtnDeleteClick
    end
    object Button1: TButton
      Left = 248
      Top = 232
      Width = 97
      Height = 33
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = Button1Click
    end
    object ExitBtn: TButton
      Left = 359
      Top = 231
      Width = 97
      Height = 33
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = ExitBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 884
      Top = 225
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 48
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 619
    Width = 1182
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from LPB')
    UpdateObject = MasterUpdate
    Left = 264
    Top = 16
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      LookupCache = True
      Size = 10
    end
    object MasterQPemeriksa: TStringField
      FieldName = 'Pemeriksa'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQPO: TStringField
      FieldName = 'PO'
      Size = 10
    end
    object MasterQRetur: TStringField
      FieldName = 'Retur'
      Size = 10
    end
    object MasterQCashNCarry: TStringField
      FieldName = 'CashNCarry'
      Size = 10
    end
    object MasterQTglTerima: TDateTimeField
      FieldName = 'TglTerima'
      Required = True
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQNamaPemeriksa: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPemeriksa'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pemeriksa'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPemeriksa: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPemeriksa'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Pemeriksa'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 50
      Lookup = True
    end
    object MasterQVerpal: TStringField
      FieldName = 'Verpal'
      Size = 10
    end
    object MasterQDikerjakanLuar: TBooleanField
      FieldName = 'DikerjakanLuar'
    end
    object MasterQSupplierBarangRusak: TStringField
      FieldName = 'SupplierBarangRusak'
      Size = 10
    end
    object MasterQNamaSupplierBrgBekas: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplierBrgBekas'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'SupplierBarangRusak'
      Size = 50
      Lookup = True
    end
    object MasterQBiayaPekerjaanLuar: TCurrencyField
      FieldName = 'BiayaPekerjaanLuar'
    end
    object MasterQKeteranganPekerjaanLuar: TMemoField
      FieldName = 'KeteranganPekerjaanLuar'
      BlobType = ftMemo
    end
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Supplier, Pemeriksa, Penerima, PO, Retur, CashNCarr' +
        'y, TglTerima, Keterangan, CreateDate, CreateBy, Operator, TglEnt' +
        'ry, TglCetak, Status, Verpal, DikerjakanLuar, SupplierBarangRusa' +
        'k, BiayaPekerjaanLuar, KeteranganPekerjaanLuar'
      'from LPB'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update LPB'
      'set'
      '  Kode = :Kode,'
      '  Supplier = :Supplier,'
      '  Pemeriksa = :Pemeriksa,'
      '  Penerima = :Penerima,'
      '  PO = :PO,'
      '  Retur = :Retur,'
      '  CashNCarry = :CashNCarry,'
      '  TglTerima = :TglTerima,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  Status = :Status,'
      '  Verpal = :Verpal,'
      '  DikerjakanLuar = :DikerjakanLuar,'
      '  SupplierBarangRusak = :SupplierBarangRusak,'
      '  BiayaPekerjaanLuar = :BiayaPekerjaanLuar,'
      '  KeteranganPekerjaanLuar = :KeteranganPekerjaanLuar'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into LPB'
      
        '  (Kode, Supplier, Pemeriksa, Penerima, PO, Retur, CashNCarry, T' +
        'glTerima, Keterangan, CreateDate, CreateBy, Operator, TglEntry, ' +
        'TglCetak, Status, Verpal, DikerjakanLuar, SupplierBarangRusak, B' +
        'iayaPekerjaanLuar, KeteranganPekerjaanLuar)'
      'values'
      
        '  (:Kode, :Supplier, :Pemeriksa, :Penerima, :PO, :Retur, :CashNC' +
        'arry, :TglTerima, :Keterangan, :CreateDate, :CreateBy, :Operator' +
        ', :TglEntry, :TglCetak, :Status, :Verpal, :DikerjakanLuar, :Supp' +
        'lierBarangRusak, :BiayaPekerjaanLuar, :KeteranganPekerjaanLuar)')
    DeleteSQL.Strings = (
      'delete from LPB'
      'where'
      '  Kode = :OLD_Kode')
    Left = 288
    Top = 80
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 272
    Top = 48
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from lpb order by kode desc')
    Left = 216
    Top = 32
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ViewLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select cast(TglTerima as date) as Tanggal,* '
      'from LPB'
      
        'where (TglTerima>=:text1 and TglTerima<=:text2) or TglTerima is ' +
        'null'
      'order by tglentry desc')
    Left = 480
    Top = 512
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewLPBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewLPBQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object ViewLPBQPemeriksa: TStringField
      FieldName = 'Pemeriksa'
      Required = True
      Size = 10
    end
    object ViewLPBQNamaPemeriksa: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPemeriksa'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pemeriksa'
      Size = 50
      Lookup = True
    end
    object ViewLPBQPenerima: TStringField
      FieldName = 'Penerima'
      Required = True
      Size = 10
    end
    object ViewLPBQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object ViewLPBQPO: TStringField
      FieldName = 'PO'
      Size = 10
    end
    object ViewLPBQRetur: TStringField
      FieldName = 'Retur'
      Size = 10
    end
    object ViewLPBQCashNCarry: TStringField
      FieldName = 'CashNCarry'
      Size = 10
    end
    object ViewLPBQTglTerima: TDateTimeField
      FieldName = 'TglTerima'
      Required = True
    end
    object ViewLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewLPBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewLPBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewLPBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewLPBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewLPBQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewLPBQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object ViewLPBQTanggal: TStringField
      FieldName = 'Tanggal'
    end
    object ViewLPBQVerpal: TStringField
      FieldName = 'Verpal'
      Size = 10
    end
    object ViewLPBQDikerjakanLuar: TBooleanField
      FieldName = 'DikerjakanLuar'
    end
    object ViewLPBQSupplierBarangRusak: TStringField
      FieldName = 'SupplierBarangRusak'
      Size = 10
    end
  end
  object DataSource2: TDataSource
    DataSet = ViewLPBQ
    Left = 520
    Top = 520
  end
  object DetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *, cast("" as varchar(50)) as NamaBarang, cast('#39#39' as inte' +
        'ger) as TotalDipesan, cast('#39#39' as integer) as SisaDipesan   from ' +
        'DetailLPB where kodelpb=:text')
    UpdateObject = UpdateDetailLPB
    Left = 488
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailLPBQKodeLPB: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object DetailLPBQBarang: TStringField
      FieldName = 'Barang'
      LookupCache = True
      Required = True
      Size = 10
    end
    object DetailLPBQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailLPBQJenisSesuai: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object DetailLPBQKondisiBaik: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object DetailLPBQJumlahSesuai: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object DetailLPBQTepatWaktu: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object DetailLPBQDiterima: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object DetailLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailLPBQTotalDipesan: TIntegerField
      FieldName = 'TotalDipesan'
    end
    object DetailLPBQSisaDipesan: TIntegerField
      FieldName = 'SisaDipesan'
    end
    object DetailLPBQNama: TStringField
      FieldKind = fkLookup
      FieldName = 'Nama'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object DataSource3: TDataSource
    DataSet = DetailLPBQ
    Left = 536
    Top = 280
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 344
    Top = 240
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object POQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from PO')
    Left = 336
    Top = 200
    object POQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object POQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Visible = False
      Size = 10
    end
    object POQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 50
      Lookup = True
    end
    object POQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object POQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object POQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object POQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object POQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object POQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object POQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object POQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object POQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object POQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object POQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object POQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object POQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
  object ReturQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Retur')
    Left = 344
    Top = 280
    object ReturQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ReturQTglRetur: TDateTimeField
      FieldName = 'TglRetur'
      Required = True
    end
    object ReturQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Required = True
      Visible = False
      Size = 10
    end
    object ReturQNamaPelaksana: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelaksana'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelaksana'
      Size = 50
      Lookup = True
    end
    object ReturQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ReturQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ReturQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ReturQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ReturQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ReturQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object ReturQKodePO: TStringField
      FieldName = 'KodePO'
      Size = 10
    end
    object ReturQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object ReturQRetur: TBooleanField
      FieldName = 'Retur'
    end
    object ReturQComplaint: TBooleanField
      FieldName = 'Complaint'
    end
  end
  object CashNCarryQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from CashNCarry')
    Left = 336
    Top = 160
    object CashNCarryQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CashNCarryQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object CashNCarryQKasBon: TCurrencyField
      FieldName = 'KasBon'
      Required = True
    end
    object CashNCarryQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Visible = False
      Size = 10
    end
    object CashNCarryQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object CashNCarryQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object CashNCarryQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object CashNCarryQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object CashNCarryQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object CashNCarryQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Supplier')
    Left = 336
    Top = 128
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Barang')
    Left = 336
    Top = 88
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object BarangPOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select db.Barang from DaftarBeli db left join DetailPO dp on dp.' +
        'KodeDaftarBeli=db.Kode where dp.KodePO=:text')
    Left = 344
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object BarangPOQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
  end
  object BarangCNCQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select db.Barang from DaftarBeli db left join DetailCashNCarry d' +
        'p on dp.KodeDaftarBeli=db.Kode where dp.KodeCNC=:text')
    Left = 392
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object BarangCNCQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
  end
  object KodeDetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kodeLPB from Detaillpb order by kodeLPB desc')
    Left = 592
    Top = 288
    object KodeDetailLPBQkodeLPB: TStringField
      FieldName = 'kodeLPB'
      Required = True
      Size = 10
    end
  end
  object DeleteDetailLPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailLPB where KodeLPB=:text')
    Left = 648
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailLPBQKodeLPB: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object DeleteDetailLPBQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DeleteDetailLPBQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object DeleteDetailLPBQJenisSesuai: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object DeleteDetailLPBQKondisiBaik: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object DeleteDetailLPBQJumlahSesuai: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object DeleteDetailLPBQTepatWaktu: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object DeleteDetailLPBQDiterima: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object DeleteDetailLPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object UpdateDeleteLPB: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, Jumlah' +
        'Sesuai, TepatWaktu, Diterima, Keterangan'
      'from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update DetailLPB'
      'set'
      '  KodeLPB = :KodeLPB,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  JenisSesuai = :JenisSesuai,'
      '  KondisiBaik = :KondisiBaik,'
      '  JumlahSesuai = :JumlahSesuai,'
      '  TepatWaktu = :TepatWaktu,'
      '  Diterima = :Diterima,'
      '  Keterangan = :Keterangan'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into DetailLPB'
      
        '  (KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, JumlahSesu' +
        'ai, TepatWaktu, Diterima, Keterangan)'
      'values'
      
        '  (:KodeLPB, :Barang, :Jumlah, :JenisSesuai, :KondisiBaik, :Juml' +
        'ahSesuai, :TepatWaktu, :Diterima, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    Left = 712
    Top = 392
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailLPB where KodeLPB=:text')
    Left = 656
    Top = 416
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StringField1: TStringField
      FieldName = 'KodeLPB'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object BooleanField1: TBooleanField
      FieldName = 'JenisSesuai'
      Required = True
    end
    object BooleanField2: TBooleanField
      FieldName = 'KondisiBaik'
      Required = True
    end
    object BooleanField3: TBooleanField
      FieldName = 'JumlahSesuai'
      Required = True
    end
    object BooleanField4: TBooleanField
      FieldName = 'TepatWaktu'
      Required = True
    end
    object BooleanField5: TBooleanField
      FieldName = 'Diterima'
      Required = True
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object HitungQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @TotalDipesan int;'
      'declare @TotalTerima int;'
      'declare @SisaDipesan int;'
      
        'set @TotalDipesan=(select sum(jumlahbeli) from DaftarBeli db, De' +
        'tailPO dp where db.Kode=dp.KodeDaftarBeli'
      'and KodePO=:text and db.Barang=:text2);'
      
        'set @TotalTerima=(select SUM(dlpb.Jumlah) from DetailLPB dlpb, L' +
        'PB '
      
        'where dlpb.KodeLPB=LPB.Kode and  LPB.PO=:text and Barang=:text2)' +
        ';'
      'if @TotalTerima is NULL'
      'begin'
      'set @TotalTerima=0;'
      'end;'
      'set @SisaDipesan=@TotalDipesan-@TotalTerima;'
      'select TotalDipesan= @TotalDipesan, '
      'SisaDiminta=@SisaDipesan')
    Left = 320
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object HitungQTotalDipesan: TIntegerField
      FieldName = 'TotalDipesan'
    end
    object HitungQSisaDiminta: TIntegerField
      FieldName = 'SisaDiminta'
    end
  end
  object UpdateDetailLPB: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, Jumlah' +
        'Sesuai, TepatWaktu, Diterima, Keterangan'
      'from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update DetailLPB'
      'set'
      '  KodeLPB = :KodeLPB,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  JenisSesuai = :JenisSesuai,'
      '  KondisiBaik = :KondisiBaik,'
      '  JumlahSesuai = :JumlahSesuai,'
      '  TepatWaktu = :TepatWaktu,'
      '  Diterima = :Diterima,'
      '  Keterangan = :Keterangan'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into DetailLPB'
      
        '  (KodeLPB, Barang, Jumlah, JenisSesuai, KondisiBaik, JumlahSesu' +
        'ai, TepatWaktu, Diterima, Keterangan)'
      'values'
      
        '  (:KodeLPB, :Barang, :Jumlah, :JenisSesuai, :KondisiBaik, :Juml' +
        'ahSesuai, :TepatWaktu, :Diterima, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailLPB'
      'where'
      '  KodeLPB = :OLD_KodeLPB and'
      '  Barang = :OLD_Barang')
    Left = 560
    Top = 280
  end
  object HitungCNCQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @TotalDipesan int;'
      'declare @TotalTerima int;'
      'declare @SisaDipesan int;'
      
        'set @TotalDipesan=(select jumlahbeli from DaftarBeli db, DetailC' +
        'ashNCarry dc where db.Kode=dc.KodeDaftarBeli'
      'and dc.KodeCNC=:text and db.Barang=:text2);'
      
        'set @TotalTerima=(select SUM(dlpb.Jumlah) from DetailLPB dlpb, L' +
        'PB '
      
        'where dlpb.KodeLPB=LPB.Kode and  LPB.CashNCarry=:text and Barang' +
        '=:text2);'
      'if @TotalTerima is NULL'
      'begin'
      'set @TotalTerima=0;'
      'end;'
      'set @SisaDipesan=@TotalDipesan-@TotalTerima;'
      'select TotalDipesan= @TotalDipesan, SisaDipesan=@SisaDipesan')
    Left = 320
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object HitungCNCQTotalDipesan: TIntegerField
      FieldName = 'TotalDipesan'
    end
    object HitungCNCQSisaDipesan: TIntegerField
      FieldName = 'SisaDipesan'
    end
  end
  object HitungReturQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'declare @TotalDipesan int;'
      'declare @TotalTerima int;'
      'declare @SisaDipesan int;'
      
        'set @TotalDipesan=(select jumlah from DetailRetur dr where KodeR' +
        'etur=:text and Barang=:text2);'
      
        'set @TotalTerima=(select SUM(dlpb.Jumlah) from DetailLPB dlpb, L' +
        'PB '
      
        'where dlpb.KodeLPB=LPB.Kode and  LPB.Retur=:text and Barang=:tex' +
        't2);'
      'if @TotalTerima is NULL'
      'begin'
      'set @TotalTerima=0;'
      'end;'
      'set @SisaDipesan=@TotalDipesan-@TotalTerima;'
      'select TotalDipesan= @TotalDipesan, SisaDipesan=@SisaDipesan')
    Left = 176
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object HitungReturQTotalDipesan: TIntegerField
      FieldName = 'TotalDipesan'
    end
    object HitungReturQSisaDipesan: TIntegerField
      FieldName = 'SisaDipesan'
    end
  end
  object SuppQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from PO where kode=:text')
    Left = 192
    Top = 248
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SuppQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SuppQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object SuppQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object SuppQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object SuppQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object SuppQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SuppQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SuppQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SuppQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SuppQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SuppQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SuppQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
    object SuppQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object SuppQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object SuppQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
  end
  object cariRealisasiBeli: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select RealisasiJumlah '
      'from DetailCashNCarry dcnc,Barang b,DaftarBeli db'
      'where dcnc.KodeCNC=:text and dcnc.KodeDaftarBeli=db.Kode'
      'and b.kode=db.Barang and db.Barang=:text2')
    Left = 432
    Top = 56
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object cariRealisasiBelirealisasijumlah: TFloatField
      FieldName = 'realisasijumlah'
    end
  end
  object hitungLPBCNC: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select sum(jumlah) as total from detaillpb dlpb,lpb l'
      'where dlpb.kodelpb=l.kode '
      'and l.cashncarry=:text and dlpb.barang=:text2'
      '')
    Left = 464
    Top = 56
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object hitungLPBCNCtotal: TIntegerField
      FieldName = 'total'
    end
  end
  object getDetailBonBarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select sum(jumlahdiminta) as total from detailbonbarang dbb, bon' +
        'barang bb'
      'where bb.kode=dbb.kodebonbarang and bb.storing=:text'
      'and dbb.kodebarang=:text2')
    Left = 224
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object getDetailBonBarangQtotal: TFloatField
      FieldName = 'total'
    end
  end
  object hitungLPBVerpalQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select sum(jumlah) as total from detaillpb dlpb,lpb l'
      'where dlpb.kodelpb=l.kode '
      'and l.verpal=:text and dlpb.barang=:text2'
      '')
    Left = 256
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object hitungLPBVerpalQtotal: TIntegerField
      FieldName = 'total'
    end
  end
end
