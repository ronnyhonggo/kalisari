unit LPBBaru;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, StdCtrls, ComCtrls, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxInplaceContainer, cxVGrid, cxDBVGrid,
  cxTextEdit, cxMaskEdit, cxButtonEdit, ExtCtrls, SDEngine, cxLabel,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxCheckBox, cxCheckGroup;

type
  TLPBBaruFm = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    StatusBar: TStatusBar;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    BtnSave: TButton;
    BtnDelete: TButton;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQSupplier: TStringField;
    MasterQPemeriksa: TStringField;
    MasterQPenerima: TStringField;
    MasterQPO: TStringField;
    MasterQRetur: TStringField;
    MasterQCashNCarry: TStringField;
    MasterQTglTerima: TDateTimeField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatus: TStringField;
    MasterUpdate: TSDUpdateSQL;
    DataSource1: TDataSource;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ViewLPBQ: TSDQuery;
    DataSource2: TDataSource;
    ExitBtn: TButton;
    DetailLPBQ: TSDQuery;
    DataSource3: TDataSource;
    DetailLPBQKodeLPB: TStringField;
    DetailLPBQBarang: TStringField;
    DetailLPBQJumlah: TIntegerField;
    DetailLPBQJenisSesuai: TBooleanField;
    DetailLPBQKondisiBaik: TBooleanField;
    DetailLPBQJumlahSesuai: TBooleanField;
    DetailLPBQTepatWaktu: TBooleanField;
    DetailLPBQDiterima: TBooleanField;
    DetailLPBQKeterangan: TMemoField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    POQ: TSDQuery;
    ReturQ: TSDQuery;
    POQKode: TStringField;
    POQSupplier: TStringField;
    POQTermin: TStringField;
    POQTglKirim: TDateTimeField;
    POQGrandTotal: TCurrencyField;
    POQStatus: TStringField;
    POQCreateDate: TDateTimeField;
    POQCreateBy: TStringField;
    POQOperator: TStringField;
    POQTglEntry: TDateTimeField;
    POQTglCetak: TDateTimeField;
    POQStatusKirim: TStringField;
    POQDiskon: TCurrencyField;
    POQKirim: TBooleanField;
    POQAmbil: TBooleanField;
    ReturQKode: TStringField;
    ReturQTglRetur: TDateTimeField;
    ReturQPelaksana: TStringField;
    ReturQKeterangan: TMemoField;
    ReturQCreateDate: TDateTimeField;
    ReturQCreateBy: TStringField;
    ReturQOperator: TStringField;
    ReturQTglEntry: TDateTimeField;
    ReturQTglCetak: TDateTimeField;
    ReturQKodePO: TStringField;
    ReturQStatus: TStringField;
    ReturQRetur: TBooleanField;
    ReturQComplaint: TBooleanField;
    CashNCarryQ: TSDQuery;
    CashNCarryQKode: TStringField;
    CashNCarryQKasBon: TCurrencyField;
    CashNCarryQPenyetuju: TStringField;
    CashNCarryQCreateDate: TDateTimeField;
    CashNCarryQCreateBy: TStringField;
    CashNCarryQOperator: TStringField;
    CashNCarryQTglEntry: TDateTimeField;
    CashNCarryQTglCetak: TDateTimeField;
    CashNCarryQStatus: TStringField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    ViewLPBQKode: TStringField;
    ViewLPBQSupplier: TStringField;
    ViewLPBQPemeriksa: TStringField;
    ViewLPBQPenerima: TStringField;
    ViewLPBQPO: TStringField;
    ViewLPBQRetur: TStringField;
    ViewLPBQCashNCarry: TStringField;
    ViewLPBQTglTerima: TDateTimeField;
    ViewLPBQKeterangan: TMemoField;
    ViewLPBQCreateDate: TDateTimeField;
    ViewLPBQCreateBy: TStringField;
    ViewLPBQOperator: TStringField;
    ViewLPBQTglEntry: TDateTimeField;
    ViewLPBQTglCetak: TDateTimeField;
    ViewLPBQStatus: TStringField;
    cxGrid2DBTableView1Supplier: TcxGridDBColumn;
    cxGrid2DBTableView1PO: TcxGridDBColumn;
    cxGrid2DBTableView1Retur: TcxGridDBColumn;
    cxGrid2DBTableView1CashNCarry: TcxGridDBColumn;
    cxGrid2DBTableView1TglTerima: TcxGridDBColumn;
    cxGrid2DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangPOQ: TSDQuery;
    BarangPOQBarang: TStringField;
    BarangCNCQ: TSDQuery;
    BarangCNCQBarang: TStringField;
    KodeDetailLPBQ: TSDQuery;
    KodeDetailLPBQkodeLPB: TStringField;
    DeleteDetailLPBQ: TSDQuery;
    UpdateDeleteLPB: TSDUpdateSQL;
    DeleteDetailLPBQKodeLPB: TStringField;
    DeleteDetailLPBQBarang: TStringField;
    DeleteDetailLPBQJumlah: TIntegerField;
    DeleteDetailLPBQJenisSesuai: TBooleanField;
    DeleteDetailLPBQKondisiBaik: TBooleanField;
    DeleteDetailLPBQJumlahSesuai: TBooleanField;
    DeleteDetailLPBQTepatWaktu: TBooleanField;
    DeleteDetailLPBQDiterima: TBooleanField;
    DeleteDetailLPBQKeterangan: TMemoField;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    BooleanField5: TBooleanField;
    MemoField1: TMemoField;
    HitungQ: TSDQuery;
    HitungQTotalDipesan: TIntegerField;
    HitungQSisaDiminta: TIntegerField;
    DetailLPBQTotalDipesan: TIntegerField;
    DetailLPBQSisaDipesan: TIntegerField;
    UpdateDetailLPB: TSDUpdateSQL;
    HitungCNCQ: TSDQuery;
    HitungCNCQTotalDipesan: TIntegerField;
    MasterQNamaPemeriksa: TStringField;
    MasterQJabatanPemeriksa: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    MasterQNamaSupplier: TStringField;
    POQNamaSupplier: TStringField;
    ReturQNamaPelaksana: TStringField;
    CashNCarryQNamaPenyetuju: TStringField;
    ViewLPBQNamaPemeriksa: TStringField;
    ViewLPBQNamaPenerima: TStringField;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    HitungCNCQSisaDipesan: TIntegerField;
    HitungReturQ: TSDQuery;
    HitungReturQTotalDipesan: TIntegerField;
    HitungReturQSisaDipesan: TIntegerField;
    lbl1: TLabel;
    Panel4: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1Pemeriksa: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPemeriksa: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPemeriksa: TcxDBEditorRow;
    cxDBVerticalGrid1Penerima: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPenerima: TcxDBEditorRow;
    cxDBVerticalGrid1JabatanPenerima: TcxDBEditorRow;
    cxDBVerticalGrid1PO: TcxDBEditorRow;
    cxDBVerticalGrid1Retur: TcxDBEditorRow;
    cxDBVerticalGrid1CashNCarry: TcxDBEditorRow;
    cxDBVerticalGrid1TglTerima: TcxDBEditorRow;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxDBVerticalGrid1Supplier: TcxDBEditorRow;
    cxDBVerticalGrid1NamaSupplier: TcxDBEditorRow;
    Panel2: TPanel;
    RbtPO: TRadioButton;
    RbtCNC: TRadioButton;
    RbtRetur: TRadioButton;
    KodeEdit: TcxButtonEdit;
    Label1: TLabel;
    Panel5: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1TotalDipesan: TcxGridDBColumn;
    cxGrid1DBTableView1SisaDipesan: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1JenisSesuai: TcxGridDBColumn;
    cxGrid1DBTableView1KondisiBaik: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahSesuai: TcxGridDBColumn;
    cxGrid1DBTableView1TepatWaktu: TcxGridDBColumn;
    cxGrid1DBTableView1Diterima: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    SuppQ: TSDQuery;
    SuppQKode: TStringField;
    SuppQSupplier: TStringField;
    SuppQTermin: TStringField;
    SuppQTglKirim: TDateTimeField;
    SuppQGrandTotal: TCurrencyField;
    SuppQStatus: TStringField;
    SuppQCreateDate: TDateTimeField;
    SuppQCreateBy: TStringField;
    SuppQOperator: TStringField;
    SuppQTglEntry: TDateTimeField;
    SuppQTglCetak: TDateTimeField;
    SuppQStatusKirim: TStringField;
    SuppQDiskon: TCurrencyField;
    SuppQKirim: TBooleanField;
    SuppQAmbil: TBooleanField;
    DetailLPBQNama: TStringField;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cariRealisasiBeli: TSDQuery;
    cariRealisasiBelirealisasijumlah: TFloatField;
    hitungLPBCNC: TSDQuery;
    hitungLPBCNCtotal: TIntegerField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    MasterQVerpal: TStringField;
    cxDBVerticalGrid1Verpal: TcxDBEditorRow;
    RbtStoring: TRadioButton;
    getDetailBonBarangQ: TSDQuery;
    hitungLPBVerpalQ: TSDQuery;
    hitungLPBVerpalQtotal: TIntegerField;
    getDetailBonBarangQtotal: TFloatField;
    rbtnBarangRusak: TRadioButton;
    MasterQDikerjakanLuar: TBooleanField;
    MasterQSupplierBarangRusak: TStringField;
    cxDBVerticalGrid1DikerjakanLuar: TcxDBEditorRow;
    cxDBVerticalGrid1SupplierBarangRusak: TcxDBEditorRow;
    MasterQNamaSupplierBrgBekas: TStringField;
    cxDBVerticalGrid1NamaSupplierBrgBekas: TcxDBEditorRow;
    ViewLPBQTanggal: TStringField;
    ViewLPBQVerpal: TStringField;
    ViewLPBQDikerjakanLuar: TBooleanField;
    ViewLPBQSupplierBarangRusak: TStringField;
    cxGrid2DBTableView1Verpal: TcxGridDBColumn;
    cxGrid2DBTableView1DikerjakanLuar: TcxGridDBColumn;
    cxGrid2DBTableView1SupplierBarangRusak: TcxGridDBColumn;
    MasterQBiayaPekerjaanLuar: TCurrencyField;
    MasterQKeteranganPekerjaanLuar: TMemoField;
    cxDBVerticalGrid1BiayaPekerjaanLuar: TcxDBEditorRow;
    cxDBVerticalGrid1KeteranganPekerjaanLuar: TcxDBEditorRow;
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExitBtnClick(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure RbtPOClick(Sender: TObject);
    procedure RbtCNCClick(Sender: TObject);
    procedure RbtReturClick(Sender: TObject);
    procedure cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1POEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1ReturEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid1DBTableView1BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure BtnDeleteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxGrid1DBTableView1JumlahPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure RbtStoringClick(Sender: TObject);
    procedure cxDBVerticalGrid1VerpalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure rbtnBarangRusakClick(Sender: TObject);
    procedure cxDBVerticalGrid1SupplierBarangRusakEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1DikerjakanLuarEditPropertiesChange(
      Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LPBBaruFm: TLPBBaruFm;
    MasterOriSQL: string;
  paramkode :string;
  DetailLPBQOriSQL: string;
  jenis : string;

implementation

uses MenuUtama, DropDown, DM, BarangLPBDropDown, PegawaiDropDown, PODropDown, ReturDropDown, CashNCarryDropDown, SupplierDropDown,
  StoringDropDown;

{$R *.dfm}

procedure TLPBBaruFm.KodeEditEnter(Sender: TObject);
begin
KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  cxGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  BtnSave.Enabled:=false;
 // BtnKepalaTeknik.Enabled:=false;
  //BtnSetuju.Enabled:=false;
  BtnDelete.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TLPBBaruFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      BtnDelete.Enabled:=menuutamafm.UserQDeleteLPB.AsBoolean;
    end;
    BtnSave.Enabled:=menuutamafm.UserQUpdateLPB.AsBoolean;
    cxDBVerticalGrid1.Enabled:=true;
  end;
  DetailLPBQ.Close;
  DetailLPBQ.SQL.Text:=DetailLPBQOriSQL;
  DetailLPBQ.ParamByName('text').AsString:='';
  DetailLPBQ.Open;
  DetailLPBQ.Close;
end;

procedure TLPBBaruFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
release;
end;

procedure TLPBBaruFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailLPBQOriSQL:=DetailLPBQ.SQL.Text;
  //DetailBonBarangOriSQL:=DetailBonBarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TLPBBaruFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  SupplierQ.Open;
  PegawaiQ.Open;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewLPBQ.Close;
  ViewLPBQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewLPBQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewLPBQ.Open;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  KodeEditPropertiesButtonClick(sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertLPB.AsBoolean;
  BtnSave.Enabled:=menuutamafm.UserQUpdateLPB.AsBoolean;
  BtnDelete.Enabled:=menuutamafm.UserQDeleteLPB.AsBoolean;

  cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
  cxDBVerticalGrid1Penerima.Visible:=FALSE;
  cxDBVerticalGrid1PO.Visible:=FALSE;
  cxDBVerticalGrid1Retur.Visible:=FALSE;
  cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
  cxDBVerticalGrid1Verpal.Visible:=FALSE;
  cxDBVerticalGrid1TglTerima.Visible:=FALSE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;

  cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
  cxDBVerticalGrid1Penerima.Visible:=TRUE;
  cxDBVerticalGrid1PO.Visible:=TRUE;
  cxDBVerticalGrid1TglTerima.Visible:=TRUE;
  cxDBVerticalGrid1Keterangan.Visible:=TRUE;
  cxDBVerticalGrid1Supplier.Visible:=TRUE;
  RbtPO.Checked:=TRUE;
end;

procedure TLPBBaruFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TLPBBaruFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TLPBBaruFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  cxGrid1.Enabled:=true;
end;

procedure TLPBBaruFm.RbtPOClick(Sender: TObject);
begin
  cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
  cxDBVerticalGrid1Penerima.Visible:=FALSE;
  cxDBVerticalGrid1PO.Visible:=FALSE;
  cxDBVerticalGrid1Retur.Visible:=FALSE;
  cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
  cxDBVerticalGrid1Verpal.Visible:=FALSE;
  cxDBVerticalGrid1TglTerima.Visible:=FALSE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;

  cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
  cxDBVerticalGrid1Penerima.Visible:=TRUE;
  cxDBVerticalGrid1PO.Visible:=TRUE;
  cxDBVerticalGrid1TglTerima.Visible:=TRUE;
  cxDBVerticalGrid1Keterangan.Visible:=TRUE;
  cxDBVerticalGrid1Supplier.Visible:=TRUE;
  cxDBVerticalGrid1DikerjakanLuar.Visible:=FALSE;
  cxDBVerticalGrid1SupplierBarangRusak.Visible:=FALSE;
  MasterQDikerjakanLuar.AsVariant:=null;
  MasterQSupplierBarangRusak.AsVariant:=null;

  MasterQCashNCarry.AsVariant:=null;
  MasterQRetur.AsVariant:=null;
  MasterQVerpal.AsVariant:=null;
  DetailLPBQ.close;
  DetailLPBQ.open;
end;

procedure TLPBBaruFm.RbtCNCClick(Sender: TObject);
begin
   cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
   cxDBVerticalGrid1Penerima.Visible:=FALSE;
   cxDBVerticalGrid1PO.Visible:=FALSE;
   cxDBVerticalGrid1Retur.Visible:=FALSE;
   cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
   cxDBVerticalGrid1Verpal.Visible:=FALSE;
   cxDBVerticalGrid1TglTerima.Visible:=FALSE;
   cxDBVerticalGrid1Keterangan.Visible:=FALSE;
   cxDBVerticalGrid1Supplier.Visible:=FALSE;

   cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
   cxDBVerticalGrid1Penerima.Visible:=TRUE;
   cxDBVerticalGrid1CashNCarry.Visible:=TRUE;
   cxDBVerticalGrid1TglTerima.Visible:=TRUE;
   cxDBVerticalGrid1Keterangan.Visible:=TRUE;
   cxDBVerticalGrid1Supplier.Visible:=TRUE;
   cxDBVerticalGrid1DikerjakanLuar.Visible:=FALSE;
   cxDBVerticalGrid1SupplierBarangRusak.Visible:=FALSE;
   MasterQDikerjakanLuar.AsVariant:=null;
   MasterQSupplierBarangRusak.AsVariant:=null;

   MasterQPO.AsVariant:=null;
   MasterQRetur.AsVariant:=null;
   MasterQVerpal.AsVariant:=null;
   DetailLPBQ.close;
   DetailLPBQ.open;
end;

procedure TLPBBaruFm.RbtReturClick(Sender: TObject);
begin
  cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
  cxDBVerticalGrid1Penerima.Visible:=FALSE;
  cxDBVerticalGrid1PO.Visible:=FALSE;
  cxDBVerticalGrid1Retur.Visible:=FALSE;
  cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
  cxDBVerticalGrid1Verpal.Visible:=FALSE;
  cxDBVerticalGrid1TglTerima.Visible:=FALSE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;

  cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
  cxDBVerticalGrid1Penerima.Visible:=TRUE;
  cxDBVerticalGrid1Retur.Visible:=TRUE;
  cxDBVerticalGrid1PO.Visible:=TRUE;
  cxDBVerticalGrid1TglTerima.Visible:=TRUE;
  cxDBVerticalGrid1Keterangan.Visible:=TRUE;
  cxDBVerticalGrid1Supplier.Visible:=TRUE;
  cxDBVerticalGrid1DikerjakanLuar.Visible:=FALSE;
  cxDBVerticalGrid1SupplierBarangRusak.Visible:=FALSE;
  MasterQDikerjakanLuar.AsVariant:=null;
  MasterQSupplierBarangRusak.AsVariant:=null;

  MasterQCashNCarry.AsVariant:=null;
  MasterQPO.AsVariant:=null;
  MasterQVerpal.AsVariant:=null;
  DetailLPBQ.close;
  DetailLPBQ.open;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1PemeriksaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;}
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPemeriksa.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1PenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;}
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1POEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PODropDownFm:=TPODropdownfm.Create(Self);
  if PODropDownFm.ShowModal=MrOK then
  begin
   MasterQ.Open;
   MasterQ.Edit;
    MasterQPO.AsString:=PODropDownFm.kode;
    SuppQ.Close;
    SuppQ.ParamByName('text').AsString:=MasterQPO.AsString;
    SuppQ.Open;
    MasterQSupplier.AsString:=SuppQSupplier.AsString;
    DetailLPBQ.Close;
    DetailLPBQ.Open;
  end;
  PODropDownFm.Release;
  cxGrid1.Enabled:=true;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1ReturEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin

  ReturDropDownFm:=TReturDropdownfm.Create(Self);
  if ReturDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQRetur.AsString:=ReturDropDownFm.kode;
    DetailLPBQ.Close;
    DetailLPBQ.Open;
  end;
  ReturDropDownFm.Release;
  cxGrid1.Enabled:=true;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1CashNCarryEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  CashNCarryDropDownFm:=TCashNCarryDropdownfm.Create(Self);
  if CashNCarryDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQCashNCarry.AsString:=CashNCarryDropDownFm.kode;
    DetailLPBQ.Close;
    DetailLPBQ.Open;
  end;
  CashNCarryDropDownFm.Release;
  cxGrid1.Enabled:=true;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{  SupplierQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
 SupplierQ.ExecSQL;
 SupplierQ.Open;}
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSupplier.AsString:=SupplierDropDownFm.kode;
    //MasterVGrid.SetFocus;
  end;
  SupplierDropDownFm.Release;
end;

procedure TLPBBaruFm.BtnSaveClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end; //untuk Mode:Entry
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailLPBQ.Open;
    DetailLPBQ.Edit;
    DetailLPBQ.First;
    while DetailLPBQ.Eof=false do
    begin
      DetailLPBQ.Edit;
      DetailLPBQKodeLPB.AsString:=KodeEdit.Text;
      detailLPBQ.Post;
      DetailLPBQ.Next;
    end;
    DetailLPBQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailLPBQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
 except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailLPBQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  KodeEdit.SetFocus;
  ViewLPBQ.Close;
  ViewLPBQ.Open;
  DetailLPBQ.Close;
  DetailLPBQ.SQL.Text:=DetailLPBQOriSQL;
  DetailLPBQ.ParamByName('text').AsString:='';
  DetailLPBQ.Open;
  DetailLPBQ.Close;
  KodeEditPropertiesButtonClick(self,0);
end;

procedure TLPBBaruFm.cxGrid1DBTableView1BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL,nama,q: string;
begin
  if rbtPO.Checked=TRUE then
  begin
    q:='select b.* from barang b, DetailPO dp, DaftarBeli db where dp.KodeDaftarBeli=db.kode and b.kode=db.barang and dp.KodePO=' +QuotedStr(MasterQPO.AsString) +'and dp.StatusPO<>"FINISHED"';
  end
  else if rbtCNC.Checked=TRUE then
  begin
    q:='select b.* from barang b, DetailCashNCarry dc, DaftarBeli db where dc.KodeDaftarBeli=db.kode and b.kode=db.barang and dc.KodeCNC=' +QuotedStr(MasterQCashNCarry.AsString)+ 'and dc.StatusCNC<>"FINISHED"';
  end
  else if RbtRetur.Checked=TRUE then
  begin
    q:='select b.* from barang b, Retur r, DetailRetur dr where dr.KodeRetur=r.kode and b.kode=dr.barang and r.status<>"FINISHED"';
  end
  else if RbtStoring.Checked=TRUE then
  begin
    q:='select distinct b.Kode,b.Nama,b.Jumlah,b.Satuan,b.MinimumStok,b.MaximumStok,b.StandardUmur,b.Lokasi,b.ClaimNWarranty,b.DurasiClaimNWarranty,b.Kategori,b.SingleSupplier,b.Harga,b.Keterangan,b.Createdate,b.createby,b.operator,b.tglentry';
    q:= q + ' from barang b, BonBarang bb, DetailBonBarang dbb where dbb.KodeBonBarang=bb.kode and b.kode=dbb.KodeBarang and bb.Storing=' + QuotedStr(MasterQVerpal.AsString);
  end
  else begin
    q:='select * from barang';
  end;

  BarangLPBDropDownFm:=TBarangLPBDropdownfm.Create(Self,q);
  if BarangLPBDropDownFm.ShowModal=MrOK then
  begin
    //DetailLPBQ.Close;
    DetailLPBQ.Open;
    DetailLPBQ.Insert;
    DetailLPBQ.Edit;
    DetailLPBQBarang.AsString:=BarangLPBDropDownFm.kode;

    if (MasterQRetur.AsString<>'') then
    begin
      HitungReturQ.Close;
      HitungReturQ.ParamByName('text').AsString:=MasterQRetur.AsString;
      HitungReturQ.ParamByName('text2').AsString:=BarangLPBDropDownFm.kode;
      HitungReturQ.Open;
      DetailLPBQ.Edit;
      DetailLPBQTotalDipesan.AsInteger:=HitungReturQTotalDipesan.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=HitungReturQSisaDipesan.AsInteger;
    end
    else if (MasterQPO.AsString<>'') then
    begin
      HitungQ.Close;
      HitungQ.ParamByName('text').AsString:=MasterQPO.AsString;
      HitungQ.ParamByName('text2').AsString:=BarangLPBDropDownFm.kode;
      HitungQ.Open;
      DetailLPBQTotalDipesan.AsInteger:=HitungQTotalDipesan.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=HitungQSisaDiminta.AsInteger;
    end
    else if (MasterQCashNCarry.AsString<>'') then
    begin
      cariRealisasiBeli.Close();
      cariRealisasiBeli.ParamByName('text').AsString:=MasterQCashNCarry.AsString;
      cariRealisasiBeli.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      cariRealisasiBeli.Open();

      hitungLPBCNC.close();
      hitungLPBCNC.ParamByName('text').AsString:=MasterQCashNCarry.AsString;
      hitungLPBCNC.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      hitungLPBCNC.open();

      DetailLPBQTotalDipesan.AsInteger:=cariRealisasiBelirealisasijumlah.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=cariRealisasiBelirealisasijumlah.AsInteger-hitungLPBCNCtotal.AsInteger;
    end
    else if (MasterQVerpal.AsString<>'') then
    begin
      getDetailBonBarangQ.Close;
      getDetailBonBarangQ.ParamByName('text').AsString:=MasterQVerpal.AsString;
      getDetailBonBarangQ.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      getDetailBonBarangQ.Open;

      DetailLPBQTotalDipesan.AsInteger:=getDetailBonBarangQTotal.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=getDetailBonBarangQtotal.AsInteger-hitungLPBVerpalQtotal.AsInteger;
    end
    else begin
      DetailLPBQTotalDipesan.AsInteger:=9999;
      DetailLPBQSisaDipesan.AsInteger:=9999;
    end;
  end;
  BarangLPBDropDownFm.Release;
  DetailLPBQJenisSesuai.AsBoolean:=TRUE;
  DetailLPBQKondisiBaik.AsBoolean:=TRUE;
  DetailLPBQTepatWaktu.AsBoolean:=TRUE;
  DetailLPBQJumlahSesuai.AsBoolean:=TRUE;
  DetailLPBQDiterima.AsBoolean:=TRUE;
end;

procedure TLPBBaruFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  cxGrid1.Enabled:=true;
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewLPBQKode.AsString+'%'));
  MasterQ.Open;
  if MasterQ.IsEmpty then
  begin
    StatusBar.Panels[0].Text:= 'Mode : Entry';
    MasterQ.Append;
    MasterQ.Edit;
  end
  else
  begin
    StatusBar.Panels[0].Text:= 'Mode : Edit';
    MasterQ.Edit;
    BtnDelete.Enabled:=menuutamafm.UserQDeleteLPB.AsBoolean;
  end;

  BtnSave.Enabled:=menuutamafm.UserQUpdateLPB.AsBoolean;
  cxDBVerticalGrid1.Enabled:=True;
  BarangQ.Open;
  SupplierQ.Open;
  PegawaiQ.Open;
  CashNCarryQ.Open;
  POQ.Open;
  ReturQ.Open;
  BarangPOQ.Open;
  BarangCNCQ.Open;
  KodeDetailLPBQ.Close;
  HitungCNCQ.Close;
  DeleteDetailLPBQ.Close;
  SDQuery1.Close;
  DetailLPBQ.Close;
  HitungQ.Close;

  if MasterQRetur.AsString <> '' then
  begin
    jenis:='Retur';
  end
  else if MasterQCashNCarry.AsString <> '' then
  begin
    jenis:='CNC';
  end
  else if MasterQVerpal.AsString <> '' then
  begin
    jenis:='Verpal';
  end
  else if MasterQPO.AsString<>'' then
  begin
    jenis:='PO';
  end
  else begin
    jenis:='Lain'
  end;

  If jenis='Retur' then
  begin
    RbtPO.Checked:=FALSE;
    RBTCNC.Checked:=FALSE;
    RBTRetur.Checked:=TRUE;
    RbtStoring.Checked:=False;
    rbtnBarangRusak.Checked:=False;
  end
  else If jenis='CNC' then
  begin
    RbtPO.Checked:=FALSE;
    RBTCNC.Checked:=TRUE;
    RBTRetur.Checked:=FALSE;
    RbtStoring.Checked:=False;
    rbtnBarangRusak.Checked:=False;
  end
  else If jenis='Verpal' then
  begin
    RbtPO.Checked:=FALSE;
    RBTCNC.Checked:=False;
    RBTRetur.Checked:=FALSE;
    RbtStoring.Checked:=True;
    rbtnBarangRusak.Checked:=False;
  end
  else If jenis='PO' then
  begin
    RbtPO.Checked:=TRUE;
    RBTCNC.Checked:=FALSE;
    RBTRetur.Checked:=FALSE;
    RbtStoring.Checked:=False;
    rbtnBarangRusak.Checked:=False;
  end
  else begin
    RbtPO.Checked:=False;
    RBTCNC.Checked:=FALSE;
    RBTRetur.Checked:=FALSE;
    RbtStoring.Checked:=False;
    rbtnBarangRusak.Checked:=True;
  end;
  DetailLPBQ.Close;
  DetailLPBQ.ParamByName('text').AsString := MasterQKode.AsString;
  DetailLPBQ.Open;
  DetailLPBQ.Edit;
  DetailLPBQ.First;
  while DetailLPBQ.Eof=false do
  begin
    DetailLPBQ.Edit;
    if (MasterQRetur.AsString<>'') then
    begin
      HitungReturQ.Close;
      HitungReturQ.ParamByName('text').AsString:=MasterQRetur.AsString;
      HitungReturQ.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      HitungReturQ.Open;
      DetailLPBQ.Edit;
      DetailLPBQTotalDipesan.AsInteger:=HitungReturQTotalDipesan.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=HitungReturQSisaDipesan.AsInteger;
    end
    else if (MasterQPO.AsString<>'') then
    begin
      HitungQ.Close;
      HitungQ.ParamByName('text').AsString:=MasterQPO.AsString;
      HitungQ.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      HitungQ.Open;
      DetailLPBQ.Edit;
      DetailLPBQTotalDipesan.AsInteger:=HitungQTotalDipesan.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=HitungQSisaDiminta.AsInteger;
    end
    else if (MasterQCashNCarry.AsString<>'') then
    begin
      cariRealisasiBeli.Close();
      cariRealisasiBeli.ParamByName('text').AsString:=MasterQCashNCarry.AsString;
      cariRealisasiBeli.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      cariRealisasiBeli.Open();

      hitungLPBCNC.close();
      hitungLPBCNC.ParamByName('text').AsString:=MasterQCashNCarry.AsString;
      hitungLPBCNC.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      hitungLPBCNC.open();

      DetailLPBQTotalDipesan.AsInteger:=cariRealisasiBelirealisasijumlah.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=cariRealisasiBelirealisasijumlah.AsInteger-hitungLPBCNCtotal.AsInteger;
    end
    else if (MasterQVerpal.AsString<>'') then
    begin
      getDetailBonBarangQ.Close;
      getDetailBonBarangQ.ParamByName('text').AsString:=MasterQVerpal.AsString;
      getDetailBonBarangQ.ParamByName('text2').AsString:=DetailLPBQBarang.AsString;
      getDetailBonBarangQ.Open;

      DetailLPBQTotalDipesan.AsInteger:=getDetailBonBarangQtotal.AsInteger;
      DetailLPBQSisaDipesan.AsInteger:=getDetailBonBarangQtotal.AsInteger-hitungLPBVerpalQtotal.AsInteger;
    end
    else begin
      DetailLPBQTotalDipesan.AsInteger:=9999;
      DetailLPBQSisaDipesan.AsInteger:=9999;
    end;
    DetailLPBQ.Next;
  end;
  KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TLPBBaruFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
  DetailLPBQ.Edit;

  //KodeDetailLPBQ.Close;
  //KodeDetailLPBQ.Open;

  {if KodeDetailLPBQkodeLPB.AsString='' then
  begin
      KodeDetailLPBQKodeLPB.AsString:='0000000000';
  end;}

  //DetailLPBQKodeLPB.AsString:= DMFm.Fill(InttoStr(StrtoInt(KodeDetailLPBQkodeLPB.AsString)+1),10,'0',True);;


 // ShowMessage(DetailLPBQKodeLPB.AsString);
    DetailLPBQKodeLPB.AsString:='0';
    DetailLPBQ.Post;

   { MenuUtamaFm.Database1.StartTransaction;
    try
      DetailLPBQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailLPBQ.CommitUpdates;
      except
      MenuUtamaFm.Database1.Rollback;
      DetailLPBQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    DetailLPBQ.Close;
    DetailLPBQ.SQL.Text:=DetailLPBQOriSQL;
   //DetailLPBQ.ExecSQL;
    DetailLPBQ.Open;
    end;  }

    cxGrid1DBTableView1.Focused:=false;
  viewLPBQ.Refresh;
    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';
  end;
end;

procedure TLPBBaruFm.BtnDeleteClick(Sender: TObject);
begin
  if MessageDlg('Hapus LPB '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteDetailLPBQ.Close;
      DeleteDetailLPBQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DeleteDetailLPBQ.Open;
      DeleteDetailLPBQ.First;
       while DeleteDetailLPBQ.Eof=FALSE
       do
       begin
        SDQuery1.SQL.Text:=('delete from DetailLPB where KodeLPB='+QuotedStr(KodeEdit.Text)+' and Barang='+QuotedStr(DeleteDetailLPBQBarang.AsString));
        SDQuery1.ExecSQL;
        DeleteDetailLPBQ.Next;
       end;

       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('LPB telah dihapus.',mtInformation,[mbOK],0);
       DeleteDetailLPBQ.Close;
       DetailLPBQ.Close;
       DetailLPBQ.Open;

     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('LPB pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     ViewLPBQ.Close;
     ViewLPBQ.ExecSQL;
     ViewLPBQ.Open;
     KodeEditPropertiesButtonClick(sender,0);
  end;
end;

procedure TLPBBaruFm.Button1Click(Sender: TObject);
begin
ViewLPBQ.Close;
ViewLPBQ.Open;
end;

procedure TLPBBaruFm.cxGrid1DBTableView1JumlahPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailLPBQJumlah.AsInteger:=DisplayValue;
  if DetailLPBQJumlah.AsInteger>DetailLPBQSisaDipesan.AsInteger then
  begin
    DetailLPBQJumlah.AsInteger:=DetailLPBQSisaDipesan.AsInteger;
    ShowMessage('Jumlah tidak boleh lebih besar dari sisa dipesan.');
  end;
end;

procedure TLPBBaruFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewLPBQ.Close;
  ViewLPBQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewLPBQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewLPBQ.Open;
end;

procedure TLPBBaruFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewLPBQ.Close;
  ViewLPBQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewLPBQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewLPBQ.Open;
end;

procedure TLPBBaruFm.RbtStoringClick(Sender: TObject);
begin
  cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
  cxDBVerticalGrid1Penerima.Visible:=FALSE;
  cxDBVerticalGrid1PO.Visible:=FALSE;
  cxDBVerticalGrid1Retur.Visible:=FALSE;
  cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
  cxDBVerticalGrid1Verpal.Visible:=FALSE;
  cxDBVerticalGrid1TglTerima.Visible:=FALSE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;

  cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
  cxDBVerticalGrid1Penerima.Visible:=TRUE;
  cxDBVerticalGrid1Verpal.Visible:=TRUE;
  cxDBVerticalGrid1TglTerima.Visible:=TRUE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;
  MasterQDikerjakanLuar.AsVariant:=null;
  MasterQSupplierBarangRusak.AsVariant:=null;

  MasterQCashNCarry.AsVariant:=null;
  MasterQRetur.AsVariant:=null;
  MasterQPO.AsVariant:=null;
  DetailLPBQ.close;
  DetailLPBQ.open;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1VerpalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  StoringDropDownFm:=TStoringDropdownfm.Create(Self);
  if StoringDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQVerpal.AsString:=StoringDropDownFm.kode;
    DetailLPBQ.Close;
    DetailLPBQ.Open;
  end;
  StoringDropDownFm.Release;
  cxGrid1.Enabled:=true;
end;

procedure TLPBBaruFm.rbtnBarangRusakClick(Sender: TObject);
begin
  cxDBVerticalGrid1Pemeriksa.Visible:=FALSE;
  cxDBVerticalGrid1Penerima.Visible:=FALSE;
  cxDBVerticalGrid1PO.Visible:=FALSE;
  cxDBVerticalGrid1Retur.Visible:=FALSE;
  cxDBVerticalGrid1CashNCarry.Visible:=FALSE;
  cxDBVerticalGrid1Verpal.Visible:=FALSE;
  cxDBVerticalGrid1TglTerima.Visible:=FALSE;
  cxDBVerticalGrid1Keterangan.Visible:=FALSE;
  cxDBVerticalGrid1Supplier.Visible:=FALSE;

  cxDBVerticalGrid1Pemeriksa.Visible:=TRUE;
  cxDBVerticalGrid1Penerima.Visible:=TRUE;
  cxDBVerticalGrid1TglTerima.Visible:=TRUE;
  cxDBVerticalGrid1Keterangan.Visible:=TRUE;
  cxDBVerticalGrid1Supplier.Visible:=TRUE;
  cxDBVerticalGrid1DikerjakanLuar.Visible:=TRUE;
  cxDBVerticalGrid1SupplierBarangRusak.Visible:=False;

  MasterQPO.AsVariant:=null;
  MasterQCashNCarry.AsVariant:=null;
  MasterQRetur.AsVariant:=null;
  MasterQVerpal.AsVariant:=null;
  cxGrid1.Enabled:=true;
  DetailLPBQ.close;
  DetailLPBQ.open;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1SupplierBarangRusakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSupplierBarangRusak.AsString:=SupplierDropDownFm.kode;
    //MasterVGrid.SetFocus;
  end;
  SupplierDropDownFm.Release;
end;

procedure TLPBBaruFm.cxDBVerticalGrid1DikerjakanLuarEditPropertiesChange(
  Sender: TObject);
begin
  if MasterQDikerjakanLuar.AsBoolean=true then begin
    cxDBVerticalGrid1SupplierBarangRusak.Visible:=TRUE;
    cxDBVerticalGrid1BiayaPekerjaanLuar.Visible:=TRUE;
    cxDBVerticalGrid1KeteranganPekerjaanLuar.Visible:=TRUE;
  end
  else begin
    cxDBVerticalGrid1SupplierBarangRusak.Visible:=FALSE;
    cxDBVerticalGrid1BiayaPekerjaanLuar.Visible:=FALSE;
    cxDBVerticalGrid1KeteranganPekerjaanLuar.Visible:=FALSE;
    MasterQSupplierBarangRusak.AsVariant:=null;
  end;
end;

end.
