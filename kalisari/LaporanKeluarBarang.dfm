inherited LaporanKeluarBarangFm: TLaporanKeluarBarangFm
  Left = 157
  Top = 164
  Caption = 'Laporan Keluar Barang'
  ClientWidth = 588
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescrip: TLabel
    Width = 588
    Caption = 
      'This example demonstates the ExpressQuantumGrid printing capabil' +
      'ities.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Width = 588
  end
  inherited ToolBar1: TToolBar
    Width = 588
    object tbtnFullCollapse: TToolButton
      Left = 123
      Top = 0
      Action = actFullCollapse
      ParentShowHint = False
      ShowHint = True
    end
    object tbtnFullExpand: TToolButton
      Left = 146
      Top = 0
      Action = actFullExpand
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 41
    Width = 588
    Height = 32
    Align = alTop
    TabOrder = 2
    object DateTimePicker1: TDateTimePicker
      Left = 24
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587301469910000000
      Time = 41416.587301469910000000
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 144
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587307858800000000
      Time = 41416.587307858800000000
      TabOrder = 1
    end
    object Button1: TButton
      Left = 263
      Top = 6
      Width = 81
      Height = 21
      Caption = 'Show'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 359
      Top = 6
      Width = 81
      Height = 21
      Caption = 'Export XLS'
      TabOrder = 3
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 73
    Width = 588
    Height = 396
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object cxGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 586
      Height = 394
      Align = alClient
      TabOrder = 0
      object tvPlanets: TcxGridTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.HeaderAutoHeight = True
        Styles.StyleSheet = tvssDevExpress
        object tvPlanetsNAME: TcxGridColumn
          Caption = 'Name'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvPlanetsNO: TcxGridColumn
          Caption = '#'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvPlanetsORBITS: TcxGridColumn
          Caption = 'Orbits'
          RepositoryItem = edrepCenterText
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
        end
        object tvPlanetsDISTANCE: TcxGridColumn
          Caption = 'Distance (000km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object tvPlanetsPERIOD: TcxGridColumn
          Caption = 'Period (days)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          Width = 80
        end
        object tvPlanetsDISCOVERER: TcxGridColumn
          Caption = 'Discoverer'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsDATE: TcxGridColumn
          Caption = 'Date'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsRADIUS: TcxGridColumn
          Caption = 'Radius (km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridDBBandedTableView1: TcxGridDBBandedTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Bands = <
          item
          end>
      end
      object cxGridDBBandedTableView2: TcxGridDBBandedTableView
        DataController.DataSource = MasterDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            FieldName = 'Harga'
            DisplayText = 'Total Harga'
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        Bands = <
          item
          end>
        object cxGridDBBandedTableView2PlatNo: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PlatNo'
          Visible = False
          GroupIndex = 0
          Width = 102
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2NoBody: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NoBody'
          Width = 106
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2Nama: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Nama'
          Width = 242
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2JumlahKeluar: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JumlahKeluar'
          Width = 94
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2TglKeluar: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TglKeluar'
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBBandedTableView2
      end
    end
  end
  inherited mmMain: TMainMenu
    Top = 171
    inherited miOptions: TMenuItem
      object miFullCollapsing: TMenuItem [0]
        Action = actFullCollapse
      end
      object miFullExpand: TMenuItem [1]
        Action = actFullExpand
      end
      object N3: TMenuItem [2]
        Caption = '-'
      end
    end
    inherited miHelp: TMenuItem
      Caption = ''
      Enabled = False
      Visible = False
    end
  end
  inherited sty: TActionList
    Top = 163
    object actFullExpand: TAction
      Category = 'Options'
      Caption = 'Full &Expand'
      Hint = 'Full expand'
      ImageIndex = 8
      OnExecute = actFullExpandExecute
    end
    object actFullCollapse: TAction
      Category = 'Options'
      Caption = 'Full &Collapse'
      Hint = 'Full collapse'
      ImageIndex = 7
      OnExecute = actFullCollapseExecute
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Left = 656
    Top = 152
    object dxComponentPrinterLink1: TdxGridReportLink
      Component = cxGrid
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 200
      PrinterPage.GrayShading = True
      PrinterPage.Header = 100
      PrinterPage.Margins.Bottom = 500
      PrinterPage.Margins.Left = 500
      PrinterPage.Margins.Right = 500
      PrinterPage.Margins.Top = 500
      PrinterPage.PageSize.X = 8500
      PrinterPage.PageSize.Y = 11000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 1
      BuiltInReportLink = True
    end
  end
  inherited dxPSEngineController1: TdxPSEngineController
    Active = True
    Left = 688
    Top = 152
  end
  inherited ilMain: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010128
  end
  inherited XPManifest1: TXPManifest
    Left = 728
    Top = 152
  end
  object edrepMain: TcxEditRepository
    Left = 160
    Top = 179
    object edrepCenterText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taCenter
    end
    object edrepRightText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taRightJustify
    end
  end
  object StyleRepository: TcxStyleRepository
    Left = 120
    Top = 179
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16777088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object tvssDevExpress: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.Inactive = cxStyle10
      Styles.IncSearch = cxStyle11
      Styles.Selection = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      BuiltIn = True
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select a.PlatNo, a.NoBody, b.Nama, dbkb.JumlahKeluar, bkb.TglKel' +
        'uar'
      'from Armada a, Barang b,'
      #9'PermintaanPerbaikan pp, LaporanPerbaikan lp, '
      #9'SuratPerintahKerja spk, BonBarang bb, '
      #9'BonKeluarBarang bkb, DetailBKB dbkb'
      'where pp.Armada=a.Kode and pp.Kode=lp.PP and'
      #9'lp.Kode=spk.LaporanPerbaikan and spk.Kode=bb.SPK and'
      #9'bb.Kode=bkb.BonBarang and bkb.Kode=dbkb.KodeBKB and'
      #9'dbkb.Barang=b.Kode'
      
        '                and bkb.TglKeluar>=:text and bkb.TglKeluar<=:tex' +
        't2'
      'union all'
      
        'select a.PlatNo, a.NoBody, b.Nama, dbkb.JumlahKeluar, bkb.TglKel' +
        'uar'
      'from Armada a, Barang b,'
      #9'LaporanPerawatan lp, '
      #9'SuratPerintahKerja spk, BonBarang bb, '
      #9'BonKeluarBarang bkb, DetailBKB dbkb'
      'where lp.Armada=a.Kode and lp.Kode=spk.LaporanPerawatan and '
      #9'spk.Kode=bb.SPK and bb.Kode=bkb.BonBarang and '
      #9'bkb.Kode=dbkb.KodeBKB and dbkb.Barang=b.Kode'
      #9'and bkb.TglKeluar>=:text and bkb.TglKeluar<=:text2'
      'union all'
      
        'select a.PlatNo, a.NoBody, b.Nama, dbkb.JumlahKeluar, bkb.TglKel' +
        'uar'
      'from Armada a, Barang b,'
      #9'Rebuild rb, '
      #9'SuratPerintahKerja spk, BonBarang bb, '
      #9'BonKeluarBarang bkb, DetailBKB dbkb'
      'where rb.DariArmada=a.Kode and rb.Kode=spk.Rebuild and '
      #9'spk.Kode=bb.SPK and bb.Kode=bkb.BonBarang and '
      #9'bkb.Kode=dbkb.KodeBKB and dbkb.Barang=b.Kode'
      #9'and bkb.TglKeluar>=:text and bkb.TglKeluar<=:text2'
      'order by a.PlatNo')
    Left = 240
    Top = 136
    ParamData = <
      item
        DataType = ftTime
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftTime
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftTime
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object MasterQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MasterQJumlahKeluar: TFloatField
      FieldName = 'JumlahKeluar'
      Required = True
    end
    object MasterQTglKeluar: TDateTimeField
      FieldName = 'TglKeluar'
      Required = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 272
    Top = 136
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Excel File|.xls'
    FilterIndex = 0
    Left = 56
    Top = 304
  end
end
