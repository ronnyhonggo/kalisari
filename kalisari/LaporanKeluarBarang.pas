unit LaporanKeluarBarang;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DemoBasicMain, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, ImgList, Menus, ComCtrls,
  ToolWin, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxGrid, cxClasses,
  cxEditRepositoryItems, dxPScxCommon, dxPScxGridLnk, XPMan, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, DB,
  cxDBData, cxGridBandedTableView, cxGridDBBandedTableView, SDEngine,
  ExtCtrls, cxRadioGroup, cxContainer, cxCheckBox;

type
  TLaporanKeluarBarangFm = class(TDemoBasicMainForm)
    edrepMain: TcxEditRepository;
    edrepCenterText: TcxEditRepositoryTextItem;
    edrepRightText: TcxEditRepositoryTextItem;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    tvssDevExpress: TcxGridTableViewStyleSheet;
    dxComponentPrinterLink1: TdxGridReportLink;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid: TcxGrid;
    tvPlanets: TcxGridTableView;
    tvPlanetsNAME: TcxGridColumn;
    tvPlanetsNO: TcxGridColumn;
    tvPlanetsORBITS: TcxGridColumn;
    tvPlanetsDISTANCE: TcxGridColumn;
    tvPlanetsPERIOD: TcxGridColumn;
    tvPlanetsDISCOVERER: TcxGridColumn;
    tvPlanetsDATE: TcxGridColumn;
    tvPlanetsRADIUS: TcxGridColumn;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedTableView2: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    MasterQPlatNo: TStringField;
    MasterQNoBody: TStringField;
    MasterQNama: TStringField;
    MasterQJumlahKeluar: TFloatField;
    cxGridDBBandedTableView2PlatNo: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2NoBody: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2Nama: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2JumlahKeluar: TcxGridDBBandedColumn;
    MasterQTglKeluar: TDateTimeField;
    cxGridDBBandedTableView2TglKeluar: TcxGridDBBandedColumn;
    procedure actFullExpandExecute(Sender: TObject);
    procedure actFullCollapseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure CustomizeColumns;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMSize(var Msg: TMessage); message WM_SIZE;
  end;

var
  LaporanKeluarBarangFm: TLaporanKeluarBarangFm;
  MasterQORISQL:string;

implementation

{$R *.dfm}
uses DateUtils, cxGridExportLink;

procedure TLaporanKeluarBarangFm.actFullExpandExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView2.DataController.Groups.FullExpand;
  //tvPlanets.DataController.Groups.FullExpand;
end;

procedure TLaporanKeluarBarangFm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if (FormStyle = fsStayOnTop) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end;

procedure TLaporanKeluarBarangFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(LaporanKeluarBarangFm.Handle, SW_RESTORE) ;
end;

procedure TLaporanKeluarBarangFm.actFullCollapseExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView2.DataController.Groups.FullCollapse;
  //tvPlanets.DataController.Groups.FullCollapse;
end;

procedure TLaporanKeluarBarangFm.CustomizeColumns;
const
  cDistance = 3;
  cPeriod = 4;
  cRadius = 7;
var
  I: Integer;
begin
  DecimalSeparator := '.';
  with tvPlanets do
  for I := 0 to ColumnCount - 1 do
    if I in [cDistance, cRadius] then
      Columns[I].DataBinding.ValueTypeClass := TcxIntegerValueType
    else
      if I in [cPeriod] then
      Columns[I].DataBinding.ValueTypeClass := TcxFloatValueType
      else
       Columns[I].DataBinding.ValueTypeClass := TcxStringValueType;
end;



procedure TLaporanKeluarBarangFm.FormCreate(Sender: TObject);
begin
  inherited;
  CustomizeColumns;
  MasterQORISQL:=MasterQ.SQL.Text;
end;

procedure TLaporanKeluarBarangFm.FormShow(Sender: TObject);
begin
  tvPlanets.DataController.Groups.FullExpand;
  DateTimePicker1.DateTime:=Today;
  DateTimePicker2.DateTime:=Today+1;
end;

procedure TLaporanKeluarBarangFm.Button1Click(Sender: TObject);
var total:currency;
begin
  inherited;
  MasterQ.Close;
  MasterQ.ParamByName('text').AsDate:= DateTimePicker1.Date;
  MasterQ.ParamByName('text2').AsDate:=DateTimePicker2.Date;
  MasterQ.Open;
  cxGridDBBandedTableView2.DataController.Groups.FullExpand;
end;

procedure TLaporanKeluarBarangFm.Button2Click(Sender: TObject);
begin
  inherited;
  SaveDialog1.Execute;
  if SaveDialog1.FileName<>'' then begin
    ExportGridToExcel(SaveDialog1.FileName, cxGrid);
    ShowMessage('Export Berhasil');
  end
  else
    ShowMessage('Export Dibatalkan');
end;

procedure TLaporanKeluarBarangFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
FreeAndNil(LaporanKeluarBarangFm);
end;

end.
