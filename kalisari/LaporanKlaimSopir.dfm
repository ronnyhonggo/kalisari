inherited LaporanKlaimSopirFm: TLaporanKlaimSopirFm
  Left = 196
  Top = 104
  Caption = 'Laporan Klaim Sopir'
  ClientWidth = 940
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescrip: TLabel
    Width = 940
    Caption = 
      'This example demonstates the ExpressQuantumGrid printing capabil' +
      'ities.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Width = 940
  end
  inherited ToolBar1: TToolBar
    Width = 940
    object tbtnFullCollapse: TToolButton
      Left = 123
      Top = 0
      Action = actFullCollapse
      ParentShowHint = False
      ShowHint = True
    end
    object tbtnFullExpand: TToolButton
      Left = 146
      Top = 0
      Action = actFullExpand
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 41
    Width = 940
    Height = 32
    Align = alTop
    TabOrder = 2
    object DateTimePicker1: TDateTimePicker
      Left = 24
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587301469910000000
      Time = 41416.587301469910000000
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 144
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587307858800000000
      Time = 41416.587307858800000000
      TabOrder = 1
    end
    object Button1: TButton
      Left = 263
      Top = 6
      Width = 81
      Height = 21
      Caption = 'Show'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 348
      Top = 6
      Width = 81
      Height = 21
      Caption = 'Export XLS'
      TabOrder = 3
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 73
    Width = 940
    Height = 396
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object cxGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 938
      Height = 394
      Align = alClient
      TabOrder = 0
      object tvPlanets: TcxGridTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.HeaderAutoHeight = True
        Styles.StyleSheet = tvssDevExpress
        object tvPlanetsNAME: TcxGridColumn
          Caption = 'Name'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvPlanetsNO: TcxGridColumn
          Caption = '#'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvPlanetsORBITS: TcxGridColumn
          Caption = 'Orbits'
          RepositoryItem = edrepCenterText
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
        end
        object tvPlanetsDISTANCE: TcxGridColumn
          Caption = 'Distance (000km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object tvPlanetsPERIOD: TcxGridColumn
          Caption = 'Period (days)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          Width = 80
        end
        object tvPlanetsDISCOVERER: TcxGridColumn
          Caption = 'Discoverer'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsDATE: TcxGridColumn
          Caption = 'Date'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsRADIUS: TcxGridColumn
          Caption = 'Radius (km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridDBBandedTableView1: TcxGridDBBandedTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Bands = <
          item
          end>
      end
      object cxGridDBBandedTableView2: TcxGridDBBandedTableView
        DataController.DataSource = MasterDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            FieldName = 'Nominal'
            DisplayText = 'Total KasBon'
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        Bands = <
          item
            Width = 556
          end>
        object cxGridDBBandedTableView2Tanggal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Tanggal'
          Width = 130
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2NamaPengemudi: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPengemudi'
          Width = 259
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView2ClaimSopir: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ClaimSopir'
          Width = 167
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBBandedTableView2
      end
    end
  end
  inherited mmMain: TMainMenu
    Top = 171
    inherited miOptions: TMenuItem
      object miFullCollapsing: TMenuItem [0]
        Action = actFullCollapse
      end
      object miFullExpand: TMenuItem [1]
        Action = actFullExpand
      end
      object N3: TMenuItem [2]
        Caption = '-'
      end
    end
    inherited miHelp: TMenuItem
      Caption = ''
      Enabled = False
      Visible = False
    end
  end
  inherited sty: TActionList
    Top = 163
    object actFullExpand: TAction
      Category = 'Options'
      Caption = 'Full &Expand'
      Hint = 'Full expand'
      ImageIndex = 8
      OnExecute = actFullExpandExecute
    end
    object actFullCollapse: TAction
      Category = 'Options'
      Caption = 'Full &Collapse'
      Hint = 'Full collapse'
      ImageIndex = 7
      OnExecute = actFullCollapseExecute
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Left = 656
    Top = 152
    object dxComponentPrinterLink1: TdxGridReportLink
      Component = cxGrid
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 200
      PrinterPage.GrayShading = True
      PrinterPage.Header = 100
      PrinterPage.Margins.Bottom = 500
      PrinterPage.Margins.Left = 500
      PrinterPage.Margins.Right = 500
      PrinterPage.Margins.Top = 500
      PrinterPage.PageSize.X = 8500
      PrinterPage.PageSize.Y = 11000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 1
      BuiltInReportLink = True
    end
  end
  inherited dxPSEngineController1: TdxPSEngineController
    Active = True
    Left = 688
    Top = 152
  end
  inherited ilMain: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010128
  end
  inherited XPManifest1: TXPManifest
    Left = 728
    Top = 152
  end
  object edrepMain: TcxEditRepository
    Left = 160
    Top = 179
    object edrepCenterText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taCenter
    end
    object edrepRightText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taRightJustify
    end
  end
  object StyleRepository: TcxStyleRepository
    Left = 120
    Top = 179
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16777088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object tvssDevExpress: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.Inactive = cxStyle10
      Styles.IncSearch = cxStyle11
      Styles.Selection = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      BuiltIn = True
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from masterso')
    Left = 56
    Top = 272
    object SDQuery1Kodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SDQuery1Tgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SDQuery1Pelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SDQuery1Berangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SDQuery1Tiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SDQuery1Harga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SDQuery1PPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SDQuery1PembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SDQuery1TglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SDQuery1CaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SDQuery1KeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SDQuery1NoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SDQuery1NominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SDQuery1PenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SDQuery1Pelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SDQuery1TglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SDQuery1CaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SDQuery1KetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SDQuery1NoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SDQuery1NominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SDQuery1PenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SDQuery1Extend: TBooleanField
      FieldName = 'Extend'
    end
    object SDQuery1TglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SDQuery1BiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SDQuery1PPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SDQuery1KapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SDQuery1AC: TBooleanField
      FieldName = 'AC'
    end
    object SDQuery1Toilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SDQuery1AirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SDQuery1Rute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SDQuery1TglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SDQuery1Armada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SDQuery1Kontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SDQuery1PICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SDQuery1JamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SDQuery1NoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SDQuery1AlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SDQuery1Status: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SDQuery1StatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SDQuery1ReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SDQuery1PenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SDQuery1Keterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SDQuery1CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery1CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery1Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery1TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery1TglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 88
    Top = 272
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select lp.Tanggal,p.Nama as NamaPengemudi,lp.ClaimSopir'
      'from LaporanPerbaikan lp'
      'left join Pegawai p on p.Kode=lp.SopirClaim'
      'where lp.Tanggal>=:text and lp.Tanggal<=:text2'
      'order by lp.Tanggal')
    UpdateObject = MasterUs
    Left = 8
    Top = 56
    ParamData = <
      item
        DataType = ftDate
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQNamaPengemudi: TStringField
      FieldName = 'NamaPengemudi'
      Size = 50
    end
    object MasterQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 40
    Top = 56
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, SuratJalan, Nominal, CreateDate, CreateBy, Operator' +
        ', TglEntry, TglCetak'#13#10'from bonsopir'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    ModifySQL.Strings = (
      'update bonsopir'
      'set'
      '  Kode = :Kode,'
      '  SuratJalan = :SuratJalan,'
      '  Nominal = :Nominal,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    InsertSQL.Strings = (
      'insert into bonsopir'
      
        '  (Kode, SuratJalan, Nominal, CreateDate, CreateBy, Operator, Tg' +
        'lEntry, TglCetak)'
      'values'
      
        '  (:Kode, :SuratJalan, :Nominal, :CreateDate, :CreateBy, :Operat' +
        'or, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from bonsopir'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    Left = 64
    Top = 56
  end
  object RealisasiAnjemQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select TanggalSelesai,SPBUAYaniLiter,Armada,cast('#39#39' as money) as' +
        ' Rupiah from RealisasiAnjem ra where SPBUAYaniLiter is not NULL ' +
        'and SPBUAYaniLiter>0'
      'and TanggalSelesai>=:text1 and TanggalSelesai<=:text2 union all'
      
        'select Tanggal,SPBUAYaniLiter,Armada,cast('#39#39' as money) as Rupiah' +
        ' from RealisasiTrayek where SPBUAYaniLiter is not NULL and SPBUA' +
        'YaniLiter>0'
      'and Tanggal>=:text1 and Tanggal<=:text2'
      'union all'
      
        'select TglKembali,SPBUAYaniLiter, so.Armada,cast('#39#39' as money) as' +
        ' Rupiah from MasterSJ sj left join'
      
        'MasterSO so on sj.NoSO=so.Kodenota where SPBUAYaniLiter is not N' +
        'ULL and SPBUAYaniLiter>0 and TglKembali>=:text1 and TglKembali<=' +
        ':text2')
    UpdateObject = RealisasiAnjemUs
    Left = 56
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object RealisasiAnjemQTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object RealisasiAnjemQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object RealisasiAnjemQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object RealisasiAnjemQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object RealisasiAnjemQJenisBBM: TStringField
      FieldKind = fkLookup
      FieldName = 'JenisBBM'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JenisBBM'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object RealisasiAnjemQRupiah: TCurrencyField
      FieldName = 'Rupiah'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 72
    Top = 120
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object RealisasiAnjemUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Peng' +
        'emudi, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, S' +
        'isaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULua' +
        'rLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiS' +
        'opir, PremiKernet, TabunganSopir, Tol, BiayaLainLain, Keterangan' +
        'BiayaLainLain, CreateBy, Operator, CreateDate, TglEntry'
      'from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  TanggalMulai = :TanggalMulai,'
      '  TanggalSelesai = :TanggalSelesai,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiAnjem'
      
        '  (Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Pengemud' +
        'i, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, SisaD' +
        'isetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULuarLit' +
        'er, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiSopir' +
        ', PremiKernet, TabunganSopir, Tol, BiayaLainLain, KeteranganBiay' +
        'aLainLain, CreateBy, Operator, CreateDate, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :TanggalMulai, :TanggalSelesai, :Armada, :Pe' +
        'ngemudi, :KilometerAwal, :KilometerAkhir, :Pendapatan, :Pengelua' +
        'ran, :SisaDisetor, :SPBUAYaniLiter, :SPBUAYaniUang, :SPBUAYaniJa' +
        'm, :SPBULuarLiter, :SPBULuarUang, :SPBULuarUangDiberi, :SPBULuar' +
        'Detail, :PremiSopir, :PremiKernet, :TabunganSopir, :Tol, :BiayaL' +
        'ainLain, :KeteranganBiayaLainLain, :CreateBy, :Operator, :Create' +
        'Date, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    Left = 88
    Top = 240
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai')
    Left = 8
    Top = 120
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from rute')
    Left = 40
    Top = 120
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 250
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 250
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 104
    Top = 120
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 100
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Excel File|.xls'
    FilterIndex = 0
    Left = 56
    Top = 304
  end
end
