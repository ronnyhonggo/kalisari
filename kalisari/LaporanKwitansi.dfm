inherited LaporanKwitansiFm: TLaporanKwitansiFm
  Left = 197
  Top = 101
  Caption = 'Laporan Kwitansi SO'
  ClientWidth = 1233
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescrip: TLabel
    Width = 1233
    Caption = 
      'This example demonstates the ExpressQuantumGrid printing capabil' +
      'ities.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Width = 1233
  end
  inherited ToolBar1: TToolBar
    Width = 1233
    object tbtnFullCollapse: TToolButton
      Left = 123
      Top = 0
      Action = actFullCollapse
      ParentShowHint = False
      ShowHint = True
    end
    object tbtnFullExpand: TToolButton
      Left = 146
      Top = 0
      Action = actFullExpand
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 41
    Width = 1233
    Height = 40
    Align = alTop
    TabOrder = 2
    object DateTimePicker1: TDateTimePicker
      Left = 24
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587301469910000000
      Time = 41416.587301469910000000
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 144
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587307858800000000
      Time = 41416.587307858800000000
      TabOrder = 1
    end
    object Button1: TButton
      Left = 264
      Top = 8
      Width = 81
      Height = 22
      Caption = 'REFRESH'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 351
      Top = 9
      Width = 81
      Height = 21
      Caption = 'Export XLS'
      TabOrder = 3
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 81
    Width = 1233
    Height = 388
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object cxGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 1231
      Height = 386
      Align = alClient
      TabOrder = 0
      object tvPlanets: TcxGridTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.HeaderAutoHeight = True
        Styles.StyleSheet = tvssDevExpress
        object tvPlanetsNAME: TcxGridColumn
          Caption = 'Name'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvPlanetsNO: TcxGridColumn
          Caption = '#'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvPlanetsORBITS: TcxGridColumn
          Caption = 'Orbits'
          RepositoryItem = edrepCenterText
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
        end
        object tvPlanetsDISTANCE: TcxGridColumn
          Caption = 'Distance (000km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object tvPlanetsPERIOD: TcxGridColumn
          Caption = 'Period (days)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          Width = 80
        end
        object tvPlanetsDISCOVERER: TcxGridColumn
          Caption = 'Discoverer'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsDATE: TcxGridColumn
          Caption = 'Date'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsRADIUS: TcxGridColumn
          Caption = 'Radius (km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridDBBandedTableView1: TcxGridDBBandedTableView
        DataController.DataSource = MasterDs
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skSum
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            DisplayText = 'Total'
            Sorted = True
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ExpandButtonsForEmptyDetails = False
        OptionsView.Footer = True
        OptionsView.GroupByHeaderLayout = ghlHorizontal
        OptionsView.GroupFooterMultiSummaries = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
        OptionsView.BandCaptionsInColumnAlternateCaption = True
        OptionsView.BandHeaderEndEllipsis = True
        Bands = <
          item
            Width = 1491
          end>
        object cxGridDBBandedTableView1Kode: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Kode'
          Width = 59
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Nominal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Nominal'
          Width = 119
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1PPN: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TotalPPN'
          Width = 111
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1CaraPembayaran: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CaraPembayaran'
          Width = 104
          Position.BandIndex = 0
          Position.ColIndex = 10
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1StatusKwitansi: TcxGridDBBandedColumn
          DataBinding.FieldName = 'StatusKwitansi'
          Width = 79
          Position.BandIndex = 0
          Position.ColIndex = 12
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1TanggalPembayaran: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TanggalPembayaran'
          Width = 132
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1PPh: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PPh'
          Width = 77
          Position.BandIndex = 0
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1NamaPelanggan: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPelanggan'
          Width = 136
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1NamaPenerima: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPenerima'
          Width = 89
          Position.BandIndex = 0
          Position.ColIndex = 11
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1JumlahDiterima: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JumlahDiterima'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Width = 117
          Position.BandIndex = 0
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1MarkUpNominal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MarkUpNominal'
          Width = 118
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Diskon: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Diskon'
          Width = 97
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Untuk: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Untuk'
          Width = 105
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1KeteranganPembayaran: TcxGridDBBandedColumn
          DataBinding.FieldName = 'KeteranganPembayaran'
          Width = 131
          Position.BandIndex = 0
          Position.ColIndex = 13
          Position.RowIndex = 0
        end
      end
      object cxGridDBTableView1: TcxGridDBTableView
        DataController.DataSource = DataSource1
        DataController.DetailKeyFieldNames = 'KodeKwitansi'
        DataController.MasterKeyFieldNames = 'Kode'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object cxGridDBTableView1KodeSO: TcxGridDBColumn
          DataBinding.FieldName = 'KodeSO'
          Width = 70
        end
        object cxGridDBTableView1Nominal: TcxGridDBColumn
          DataBinding.FieldName = 'Nominal'
          Width = 70
        end
        object cxGridDBTableView1Asal: TcxGridDBColumn
          DataBinding.FieldName = 'Asal'
          Width = 200
        end
        object cxGridDBTableView1Tujuan: TcxGridDBColumn
          DataBinding.FieldName = 'Tujuan'
          Width = 200
        end
        object cxGridDBTableView1berangkat: TcxGridDBColumn
          DataBinding.FieldName = 'berangkat'
          Width = 200
        end
        object cxGridDBTableView1tiba: TcxGridDBColumn
          DataBinding.FieldName = 'tiba'
          Width = 200
        end
        object cxGridDBTableView1harga: TcxGridDBColumn
          DataBinding.FieldName = 'harga'
          Width = 200
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBBandedTableView1
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited mmMain: TMainMenu
    Left = 32
    Top = 187
    inherited miOptions: TMenuItem
      object miFullCollapsing: TMenuItem [0]
        Action = actFullCollapse
      end
      object miFullExpand: TMenuItem [1]
        Action = actFullExpand
      end
      object N3: TMenuItem [2]
        Caption = '-'
      end
    end
    inherited miHelp: TMenuItem
      Visible = False
    end
  end
  inherited sty: TActionList
    Left = 72
    Top = 179
    object actFullExpand: TAction
      Category = 'Options'
      Caption = 'Full &Expand'
      Hint = 'Full expand'
      ImageIndex = 8
      OnExecute = actFullExpandExecute
    end
    object actFullCollapse: TAction
      Category = 'Options'
      Caption = 'Full &Collapse'
      Hint = 'Full collapse'
      ImageIndex = 7
      OnExecute = actFullCollapseExecute
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Left = 656
    Top = 152
    object dxComponentPrinterLink1: TdxGridReportLink
      Active = True
      Component = cxGrid
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 200
      PrinterPage.GrayShading = True
      PrinterPage.Header = 100
      PrinterPage.Margins.Bottom = 500
      PrinterPage.Margins.Left = 500
      PrinterPage.Margins.Right = 500
      PrinterPage.Margins.Top = 500
      PrinterPage.PageSize.X = 8500
      PrinterPage.PageSize.Y = 11000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 1
      ReportDocument.CreationDate = 41672.679896562500000000
      BuiltInReportLink = True
    end
  end
  inherited dxPSEngineController1: TdxPSEngineController
    Active = True
    Left = 688
    Top = 152
  end
  inherited ilMain: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010160
  end
  inherited XPManifest1: TXPManifest
    Left = 728
    Top = 152
  end
  object edrepMain: TcxEditRepository
    Left = 184
    Top = 179
    object edrepCenterText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taCenter
    end
    object edrepRightText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taRightJustify
    end
  end
  object StyleRepository: TcxStyleRepository
    Left = 144
    Top = 179
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16777088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object tvssDevExpress: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.Inactive = cxStyle10
      Styles.IncSearch = cxStyle11
      Styles.Selection = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      BuiltIn = True
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select dk.*, so.kodenota, so.berangkat, so.tiba, so.harga, r.mua' +
        't as "Asal", r.bongkar as "Tujuan"'
      'from detailkwitansiso dk, masterso so, rute r'
      'where so.kodenota=dk.kodeso and r.kode=so.rute')
    Left = 400
    Top = 224
    object DetailQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object DetailQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
    object DetailQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object DetailQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
      Size = 10
    end
    object DetailQberangkat: TDateTimeField
      FieldName = 'berangkat'
    end
    object DetailQtiba: TDateTimeField
      FieldName = 'tiba'
    end
    object DetailQharga: TCurrencyField
      FieldName = 'harga'
      Required = True
    end
    object DetailQAsal: TStringField
      FieldName = 'Asal'
      Required = True
      Size = 250
    end
    object DetailQTujuan: TStringField
      FieldName = 'Tujuan'
      Required = True
      Size = 250
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 480
    Top = 232
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select distinct k.*, p.namapt as "NamaPelanggan", pg.nama as "Na' +
        'maPenerima", '
      
        '(select sum(so.ppn) from detailkwitansiso dks, masterso so where' +
        ' so.kodenota=dks.kodeso and dks.kodekwitansi=k.kode) as TotalPPN' +
        ','
      
        'case when ((pph=1) and (untuk='#39'pelunasan'#39')) then (k.nominal-(sel' +
        'ect sum(so.ppn) from detailkwitansiso dks, masterso so where so.' +
        'kodenota=dks.kodeso and dks.kodekwitansi=k.kode))*0.98 else k.no' +
        'minal end as "JumlahDiterima"'
      'from kwitansi k, detailkwitansiso dk, pelanggan p, pegawai pg'
      
        'where pg.kode=k.penerimapembayaran and dk.kodekwitansi=k.kode an' +
        'd p.kode=k.pelanggan and '
      'tanggalpembayaran >= :text1 and tanggalpembayaran <=:text2')
    UpdateObject = MasterUs
    Left = 272
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object MasterQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object MasterQMarkUp: TCurrencyField
      FieldName = 'MarkUp'
    end
    object MasterQUangMuka: TCurrencyField
      FieldName = 'UangMuka'
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object MasterQPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object MasterQCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
    end
    object MasterQKeteranganPembayaran: TMemoField
      FieldName = 'KeteranganPembayaran'
      BlobType = ftMemo
    end
    object MasterQKeperluan: TStringField
      FieldName = 'Keperluan'
      Size = 10
    end
    object MasterQStatusKwitansi: TStringField
      FieldName = 'StatusKwitansi'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQUntuk: TStringField
      FieldName = 'Untuk'
      Size = 10
    end
    object MasterQMarkUpNominal: TCurrencyField
      FieldName = 'MarkUpNominal'
    end
    object MasterQTitipKuitansi: TBooleanField
      FieldName = 'TitipKuitansi'
    end
    object MasterQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object MasterQPeriode: TStringField
      FieldName = 'Periode'
      Size = 200
    end
    object MasterQCekPrint: TBooleanField
      FieldName = 'CekPrint'
    end
    object MasterQTanggalPembayaran: TDateTimeField
      FieldName = 'TanggalPembayaran'
    end
    object MasterQPPh: TBooleanField
      FieldName = 'PPh'
    end
    object MasterQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Required = True
      Size = 100
    end
    object MasterQNamaPenerima: TStringField
      FieldName = 'NamaPenerima'
      Required = True
      Size = 50
    end
    object MasterQJumlahDiterima: TFloatField
      FieldName = 'JumlahDiterima'
    end
    object MasterQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object MasterQTotalPPN: TCurrencyField
      FieldName = 'TotalPPN'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 312
    Top = 176
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other'
      'from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSJ'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSJ'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other)')
    DeleteSQL.Strings = (
      'delete from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 344
    Top = 168
  end
  object RealisasiAnjemQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select TanggalSelesai,SPBUAYaniLiter,Armada,cast('#39#39' as money) as' +
        ' Rupiah from RealisasiAnjem ra where SPBUAYaniLiter is not NULL ' +
        'and SPBUAYaniLiter>0'
      'and TanggalSelesai>=:text1 and TanggalSelesai<=:text2 union all'
      
        'select Tanggal,SPBUAYaniLiter,Armada,cast('#39#39' as money) as Rupiah' +
        ' from RealisasiTrayek where SPBUAYaniLiter is not NULL and SPBUA' +
        'YaniLiter>0'
      'and Tanggal>=:text1 and Tanggal<=:text2'
      'union all'
      
        'select TglKembali,SPBUAYaniLiter, so.Armada,cast('#39#39' as money) as' +
        ' Rupiah from MasterSJ sj left join'
      
        'MasterSO so on sj.NoSO=so.Kodenota where SPBUAYaniLiter is not N' +
        'ULL and SPBUAYaniLiter>0 and TglKembali>=:text1 and TglKembali<=' +
        ':text2')
    UpdateObject = RealisasiAnjemUs
    Left = 56
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object RealisasiAnjemQTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object RealisasiAnjemQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object RealisasiAnjemQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object RealisasiAnjemQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object RealisasiAnjemQJenisBBM: TStringField
      FieldKind = fkLookup
      FieldName = 'JenisBBM'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JenisBBM'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object RealisasiAnjemQRupiah: TCurrencyField
      FieldName = 'Rupiah'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 144
    Top = 232
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object RealisasiAnjemUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Peng' +
        'emudi, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, S' +
        'isaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULua' +
        'rLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiS' +
        'opir, PremiKernet, TabunganSopir, Tol, BiayaLainLain, Keterangan' +
        'BiayaLainLain, CreateBy, Operator, CreateDate, TglEntry'
      'from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  TanggalMulai = :TanggalMulai,'
      '  TanggalSelesai = :TanggalSelesai,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiAnjem'
      
        '  (Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Pengemud' +
        'i, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, SisaD' +
        'isetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULuarLit' +
        'er, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiSopir' +
        ', PremiKernet, TabunganSopir, Tol, BiayaLainLain, KeteranganBiay' +
        'aLainLain, CreateBy, Operator, CreateDate, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :TanggalMulai, :TanggalSelesai, :Armada, :Pe' +
        'ngemudi, :KilometerAwal, :KilometerAkhir, :Pendapatan, :Pengelua' +
        'ran, :SisaDisetor, :SPBUAYaniLiter, :SPBUAYaniUang, :SPBUAYaniJa' +
        'm, :SPBULuarLiter, :SPBULuarUang, :SPBULuarUangDiberi, :SPBULuar' +
        'Detail, :PremiSopir, :PremiKernet, :TabunganSopir, :Tol, :BiayaL' +
        'ainLain, :KeteranganBiayaLainLain, :CreateBy, :Operator, :Create' +
        'Date, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    Left = 88
    Top = 232
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Excel File|.xls'
    FilterIndex = 0
    Left = 56
    Top = 304
  end
end
