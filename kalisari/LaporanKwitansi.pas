unit LaporanKwitansi;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DemoBasicMain, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, ImgList, Menus, ComCtrls,
  ToolWin, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxGrid, cxClasses,
  cxEditRepositoryItems, dxPScxCommon, dxPScxGridLnk, XPMan, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, DB,
  cxDBData, cxGridBandedTableView, cxGridDBBandedTableView, SDEngine,
  ExtCtrls, cxCalendar, cxCurrencyEdit, cxGridDBTableView;

type
  TLaporanKwitansiFm = class(TDemoBasicMainForm)
    edrepMain: TcxEditRepository;
    edrepCenterText: TcxEditRepositoryTextItem;
    edrepRightText: TcxEditRepositoryTextItem;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    tvssDevExpress: TcxGridTableViewStyleSheet;
    dxComponentPrinterLink1: TdxGridReportLink;
    DetailQ: TSDQuery;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid: TcxGrid;
    tvPlanets: TcxGridTableView;
    tvPlanetsNAME: TcxGridColumn;
    tvPlanetsNO: TcxGridColumn;
    tvPlanetsORBITS: TcxGridColumn;
    tvPlanetsDISTANCE: TcxGridColumn;
    tvPlanetsPERIOD: TcxGridColumn;
    tvPlanetsDISCOVERER: TcxGridColumn;
    tvPlanetsDATE: TcxGridColumn;
    tvPlanetsRADIUS: TcxGridColumn;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUs: TSDUpdateSQL;
    RealisasiAnjemQ: TSDQuery;
    ArmadaQ: TSDQuery;
    RealisasiAnjemUs: TSDUpdateSQL;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    RealisasiAnjemQPlatNo: TStringField;
    RealisasiAnjemQJenisBBM: TStringField;
    RealisasiAnjemQTanggalSelesai: TDateTimeField;
    RealisasiAnjemQSPBUAYaniLiter: TFloatField;
    RealisasiAnjemQArmada: TStringField;
    RealisasiAnjemQRupiah: TCurrencyField;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQNominal: TCurrencyField;
    MasterQPPN: TCurrencyField;
    MasterQMarkUp: TCurrencyField;
    MasterQUangMuka: TCurrencyField;
    MasterQPelanggan: TStringField;
    MasterQPenerimaPembayaran: TStringField;
    MasterQCaraPembayaran: TStringField;
    MasterQKeteranganPembayaran: TMemoField;
    MasterQKeperluan: TStringField;
    MasterQStatusKwitansi: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQUntuk: TStringField;
    MasterQMarkUpNominal: TCurrencyField;
    MasterQTitipKuitansi: TBooleanField;
    MasterQTitipTagihan: TBooleanField;
    MasterQPeriode: TStringField;
    MasterQCekPrint: TBooleanField;
    MasterQTanggalPembayaran: TDateTimeField;
    MasterQPPh: TBooleanField;
    cxGridDBBandedTableView1Kode: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Nominal: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1PPN: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1CaraPembayaran: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1StatusKwitansi: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1TanggalPembayaran: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1PPh: TcxGridDBBandedColumn;
    MasterQNamaPelanggan: TStringField;
    cxGridDBBandedTableView1NamaPelanggan: TcxGridDBBandedColumn;
    MasterQNamaPenerima: TStringField;
    cxGridDBBandedTableView1NamaPenerima: TcxGridDBBandedColumn;
    MasterQJumlahDiterima: TFloatField;
    cxGridDBBandedTableView1JumlahDiterima: TcxGridDBBandedColumn;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView1: TcxGridDBTableView;
    DetailQKodeKwitansi: TStringField;
    DetailQKodeSO: TStringField;
    DetailQNominal: TCurrencyField;
    DetailQkodenota: TStringField;
    DetailQberangkat: TDateTimeField;
    DetailQtiba: TDateTimeField;
    DetailQharga: TCurrencyField;
    DetailQAsal: TStringField;
    DetailQTujuan: TStringField;
    cxGridDBTableView1KodeSO: TcxGridDBColumn;
    cxGridDBTableView1Nominal: TcxGridDBColumn;
    cxGridDBTableView1berangkat: TcxGridDBColumn;
    cxGridDBTableView1tiba: TcxGridDBColumn;
    cxGridDBTableView1harga: TcxGridDBColumn;
    cxGridDBTableView1Asal: TcxGridDBColumn;
    cxGridDBTableView1Tujuan: TcxGridDBColumn;
    cxGridDBBandedTableView1MarkUpNominal: TcxGridDBBandedColumn;
    MasterQDiskon: TCurrencyField;
    cxGridDBBandedTableView1Diskon: TcxGridDBBandedColumn;
    MasterQTotalPPN: TCurrencyField;
    cxGridDBBandedTableView1Untuk: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1KeteranganPembayaran: TcxGridDBBandedColumn;
    procedure actFullExpandExecute(Sender: TObject);
    procedure actFullCollapseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure CustomizeColumns;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMSize(var Msg: TMessage); message WM_SIZE;
  end;

var
  LaporanKwitansiFm: TLaporanKwitansiFm;

implementation

{$R *.dfm}
uses DateUtils, cxGridExportLink;

procedure TLaporanKwitansiFm.actFullExpandExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullExpand;
  //tvPlanets.DataController.Groups.FullExpand;
end;

procedure TLaporanKwitansiFm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if (FormStyle = fsStayOnTop) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end;

procedure TLaporanKwitansiFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(LaporanKwitansiFm.Handle, SW_RESTORE) ;
end;

procedure TLaporanKwitansiFm.actFullCollapseExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullCollapse;
  //tvPlanets.DataController.Groups.FullCollapse;
end;

procedure TLaporanKwitansiFm.CustomizeColumns;
const
  cDistance = 3;
  cPeriod = 4;
  cRadius = 7;
var
  I: Integer;
begin
  DecimalSeparator := '.';
  with tvPlanets do
  for I := 0 to ColumnCount - 1 do
    if I in [cDistance, cRadius] then
      Columns[I].DataBinding.ValueTypeClass := TcxIntegerValueType
    else
      if I in [cPeriod] then
      Columns[I].DataBinding.ValueTypeClass := TcxFloatValueType
      else
       Columns[I].DataBinding.ValueTypeClass := TcxStringValueType;
end;

procedure TLaporanKwitansiFm.FormCreate(Sender: TObject);
begin
  inherited;
  CustomizeColumns;
end;

procedure TLaporanKwitansiFm.FormShow(Sender: TObject);
begin
  tvPlanets.DataController.Groups.FullExpand;
  DateTimePicker1.DateTime:=Today;
DateTimePicker2.DateTime:=Today;
end;

procedure TLaporanKwitansiFm.Button1Click(Sender: TObject);
var total:currency;
begin
  inherited;
  MasterQ.Close;
  MasterQ.ParamByName('text1').AsDate:= DateTimePicker1.Date;
  MasterQ.ParamByName('text2').AsDate:=DateTimePicker2.Date;
  MasterQ.Open;
  DetailQ.Open;
end;

procedure TLaporanKwitansiFm.Button2Click(Sender: TObject);
begin
  inherited;
  SaveDialog1.Execute;
  if SaveDialog1.FileName<>'' then begin
    ExportGridToExcel(SaveDialog1.FileName, cxGrid);
    ShowMessage('Export Berhasil');
  end
  else
    ShowMessage('Export Dibatalkan');
end;

procedure TLaporanKwitansiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
FreeAndNil(LaporanKwitansiFm);
end;

end.
