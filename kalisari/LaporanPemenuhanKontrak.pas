unit LaporanPemenuhanKontrak;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DemoBasicMain, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, ImgList, Menus, ComCtrls,
  ToolWin, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxGrid, cxClasses,
  cxEditRepositoryItems, dxPScxCommon, dxPScxGridLnk, XPMan, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, DB,
  cxDBData, cxGridBandedTableView, cxGridDBBandedTableView, SDEngine,
  ExtCtrls, cxRadioGroup, cxContainer, cxCheckBox;

type
  TLaporanPemenuhanKontrakFm = class(TDemoBasicMainForm)
    edrepMain: TcxEditRepository;
    edrepCenterText: TcxEditRepositoryTextItem;
    edrepRightText: TcxEditRepositoryTextItem;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    tvssDevExpress: TcxGridTableViewStyleSheet;
    dxComponentPrinterLink1: TdxGridReportLink;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    SDQuery1Kodenota: TStringField;
    SDQuery1Tgl: TDateTimeField;
    SDQuery1Pelanggan: TStringField;
    SDQuery1Berangkat: TDateTimeField;
    SDQuery1Tiba: TDateTimeField;
    SDQuery1Harga: TCurrencyField;
    SDQuery1PPN: TCurrencyField;
    SDQuery1PembayaranAwal: TCurrencyField;
    SDQuery1TglPembayaranAwal: TDateTimeField;
    SDQuery1CaraPembayaranAwal: TStringField;
    SDQuery1KeteranganCaraPembayaranAwal: TStringField;
    SDQuery1NoKwitansiPembayaranAwal: TStringField;
    SDQuery1NominalKwitansiPembayaranAwal: TCurrencyField;
    SDQuery1PenerimaPembayaranAwal: TStringField;
    SDQuery1Pelunasan: TCurrencyField;
    SDQuery1TglPelunasan: TDateTimeField;
    SDQuery1CaraPembayaranPelunasan: TStringField;
    SDQuery1KetCaraPembayaranPelunasan: TStringField;
    SDQuery1NoKwitansiPelunasan: TStringField;
    SDQuery1NominalKwitansiPelunasan: TCurrencyField;
    SDQuery1PenerimaPelunasan: TStringField;
    SDQuery1Extend: TBooleanField;
    SDQuery1TglKembaliExtend: TDateTimeField;
    SDQuery1BiayaExtend: TCurrencyField;
    SDQuery1PPNExtend: TCurrencyField;
    SDQuery1KapasitasSeat: TIntegerField;
    SDQuery1AC: TBooleanField;
    SDQuery1Toilet: TBooleanField;
    SDQuery1AirSuspension: TBooleanField;
    SDQuery1Rute: TStringField;
    SDQuery1TglFollowUp: TDateTimeField;
    SDQuery1Armada: TStringField;
    SDQuery1Kontrak: TStringField;
    SDQuery1PICJemput: TMemoField;
    SDQuery1JamJemput: TDateTimeField;
    SDQuery1NoTelpPICJemput: TStringField;
    SDQuery1AlamatJemput: TMemoField;
    SDQuery1Status: TStringField;
    SDQuery1StatusPembayaran: TStringField;
    SDQuery1ReminderPending: TDateTimeField;
    SDQuery1PenerimaPending: TStringField;
    SDQuery1Keterangan: TMemoField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1TglCetak: TDateTimeField;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid: TcxGrid;
    tvPlanets: TcxGridTableView;
    tvPlanetsNAME: TcxGridColumn;
    tvPlanetsNO: TcxGridColumn;
    tvPlanetsORBITS: TcxGridColumn;
    tvPlanetsDISTANCE: TcxGridColumn;
    tvPlanetsPERIOD: TcxGridColumn;
    tvPlanetsDISCOVERER: TcxGridColumn;
    tvPlanetsDATE: TcxGridColumn;
    tvPlanetsRADIUS: TcxGridColumn;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedTableView2: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUs: TSDUpdateSQL;
    RealisasiAnjemQ: TSDQuery;
    ArmadaQ: TSDQuery;
    RealisasiAnjemUs: TSDUpdateSQL;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    RealisasiAnjemQPlatNo: TStringField;
    RealisasiAnjemQJenisBBM: TStringField;
    RealisasiAnjemQTanggalSelesai: TDateTimeField;
    RealisasiAnjemQSPBUAYaniLiter: TFloatField;
    RealisasiAnjemQArmada: TStringField;
    RealisasiAnjemQRupiah: TCurrencyField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    SaveDialog1: TSaveDialog;
    Button2: TButton;
    MasterQNoKontrak: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQKotaAsal: TStringField;
    MasterQKotaTujuan: TStringField;
    MasterQTanggalMulai: TDateTimeField;
    MasterQTanggalSelesai: TDateTimeField;
    MasterQPlatNo: TStringField;
    MasterQNamaPengemudi: TStringField;
    cxGridDBBandedTableView2NoKontrak: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2NamaPelanggan: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2KotaAsal: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2KotaTujuan: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2TanggalMulai: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2TanggalSelesai: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2PlatNo: TcxGridDBBandedColumn;
    cxGridDBBandedTableView2NamaPengemudi: TcxGridDBBandedColumn;
    procedure actFullExpandExecute(Sender: TObject);
    procedure actFullCollapseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure CustomizeColumns;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMSize(var Msg: TMessage); message WM_SIZE;
  end;

var
  LaporanPemenuhanKontrakFm: TLaporanPemenuhanKontrakFm;

implementation

{$R *.dfm}
uses DateUtils, cxGridExportLink;

procedure TLaporanPemenuhanKontrakFm.actFullExpandExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView2.DataController.Groups.FullExpand;
  //tvPlanets.DataController.Groups.FullExpand;
end;

procedure TLaporanPemenuhanKontrakFm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if (FormStyle = fsStayOnTop) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end;

procedure TLaporanPemenuhanKontrakFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(LaporanPemenuhanKontrakFm.Handle, SW_RESTORE) ;
end;

procedure TLaporanPemenuhanKontrakFm.actFullCollapseExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView2.DataController.Groups.FullCollapse;
  //tvPlanets.DataController.Groups.FullCollapse;
end;

procedure TLaporanPemenuhanKontrakFm.CustomizeColumns;
const
  cDistance = 3;
  cPeriod = 4;
  cRadius = 7;
var
  I: Integer;
begin
  DecimalSeparator := '.';
  with tvPlanets do
  for I := 0 to ColumnCount - 1 do
    if I in [cDistance, cRadius] then
      Columns[I].DataBinding.ValueTypeClass := TcxIntegerValueType
    else
      if I in [cPeriod] then
      Columns[I].DataBinding.ValueTypeClass := TcxFloatValueType
      else
       Columns[I].DataBinding.ValueTypeClass := TcxStringValueType;
end;



procedure TLaporanPemenuhanKontrakFm.FormCreate(Sender: TObject);
begin
  inherited;
  CustomizeColumns;
  
end;

procedure TLaporanPemenuhanKontrakFm.FormShow(Sender: TObject);
begin
  tvPlanets.DataController.Groups.FullExpand;
  DateTimePicker1.DateTime:=Today;
DateTimePicker2.DateTime:=Today;
end;

procedure TLaporanPemenuhanKontrakFm.Button1Click(Sender: TObject);
var total:currency;
begin
  inherited;
  MasterQ.Close;
  MasterQ.ParamByName('text').AsDate:= DateTimePicker1.Date;
  MasterQ.ParamByName('text2').AsDate:=DateTimePicker2.Date;
  MasterQ.Open;
end;

procedure TLaporanPemenuhanKontrakFm.Button2Click(Sender: TObject);
begin
  inherited;
  SaveDialog1.Execute;
  if SaveDialog1.FileName<>'' then begin
    ExportGridToExcel(SaveDialog1.FileName, cxGrid);
    ShowMessage('Export Berhasil');
  end
  else
    ShowMessage('Export Dibatalkan');
end;

procedure TLaporanPemenuhanKontrakFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
FreeAndNil(LaporanPemenuhanKontrakFm);
end;

end.
