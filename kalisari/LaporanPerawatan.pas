unit LaporanPerawatan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox;

type
  TLaporanPerawatanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PegawaiQ: TSDQuery;
    PPQ: TSDQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewPPQ: TSDQuery;
    SJDS: TDataSource;
    ArmadaQ: TSDQuery;
    Crpe1: TCrpe;
    ekorQ: TSDQuery;
    ekorQkode: TStringField;
    ekorQpanjang: TStringField;
    ekorQberat: TStringField;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    jarakQJarak: TIntegerField;
    KodeQkode: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    ViewDs: TDataSource;
    cxButton1: TcxButton;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQArmada: TStringField;
    PPQPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    ViewQ: TSDQuery;
    VSPKQ: TSDQuery;
    SPKDs: TDataSource;
    VSPKQKode: TStringField;
    VSPKQTanggal: TDateTimeField;
    VSPKQLaporanPerbaikan: TStringField;
    VSPKQLaporanPerawatan: TStringField;
    VSPKQRebuild: TStringField;
    VSPKQMekanik: TStringField;
    VSPKQDetailTindakan: TMemoField;
    VSPKQWaktuMulai: TDateTimeField;
    VSPKQWaktuSelesai: TDateTimeField;
    VSPKQStatus: TStringField;
    VSPKQKeterangan: TStringField;
    VSPKQCreateDate: TDateTimeField;
    VSPKQCreateBy: TStringField;
    VSPKQOperator: TStringField;
    VSPKQTglEntry: TDateTimeField;
    VSPKQNamaMekanik: TStringField;
    Panel1: TPanel;
    VLPQ: TSDQuery;
    LPDs: TDataSource;
    VKnbalQ: TSDQuery;
    KnbalDs: TDataSource;
    Panel4: TPanel;
    Label2: TLabel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1Column4: TcxGridDBColumn;
    cxGrid3DBTableView1Column1: TcxGridDBColumn;
    cxGrid3DBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBTableView1Column3: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    VTKQ: TSDQuery;
    TKDs: TDataSource;
    VKnbalQKode: TStringField;
    VKnbalQNama: TStringField;
    VKnbalQbarang: TStringField;
    VKnbalQKeBarang: TStringField;
    VTKQKode: TStringField;
    VTKQTglTukar: TDateTimeField;
    VTKQDariArmada2: TStringField;
    VTKQKeArmada2: TStringField;
    VTKQBarangDari2: TStringField;
    VTKQBarangKe2: TStringField;
    VTKQDariArmada: TStringField;
    VTKQKeArmada: TStringField;
    VTKQBarangDari: TStringField;
    VTKQBarangKe: TStringField;
    VTKQPeminta: TStringField;
    VTKQLaporanPerbaikan: TStringField;
    VTKQCreateDate: TDateTimeField;
    VTKQCreateBy: TStringField;
    VTKQOperator: TStringField;
    VTKQTglEntry: TDateTimeField;
    VTKQTglCetak: TDateTimeField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQWaktuMulai: TDateTimeField;
    MasterQWaktuSelesai: TDateTimeField;
    MasterQVerifikator: TStringField;
    MasterQButuhPerbaikan: TBooleanField;
    MasterQKmArmadaSekarang: TIntegerField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQArmada: TStringField;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridWaktuMulai: TcxDBEditorRow;
    MasterVGridWaktuSelesai: TcxDBEditorRow;
    MasterVGridVerifikator: TcxDBEditorRow;
    MasterVGridButuhPerbaikan: TcxDBEditorRow;
    MasterVGridKmArmadaSekarang: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterQNamaVerifikator: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterQNoBody: TStringField;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterQNamaOperator: TStringField;
    ViewQKode: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQWaktuMulai: TDateTimeField;
    ViewQWaktuSelesai: TDateTimeField;
    ViewQVerifikator: TStringField;
    ViewQButuhPerbaikan: TBooleanField;
    ViewQKmArmadaSekarang: TIntegerField;
    ViewQStatus: TStringField;
    ViewQKeterangan: TMemoField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQTglCetak: TDateTimeField;
    ViewQArmada: TStringField;
    ViewQNamaVerifikator: TStringField;
    ViewQNamaOperator: TStringField;
    ViewQNobody: TStringField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQPlatNo: TStringField;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    Panel2: TPanel;
    Panel5: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Column5: TcxGridDBColumn;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    Label3: TLabel;
    cxGrid4: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    JenisPerawatanUS: TSDUpdateSQL;
    JenisPerawatanQ: TSDQuery;
    JenisPerawatanDs: TDataSource;
    JenisPerawatanQKodePerawatan: TStringField;
    JenisPerawatanQJenisPerawatan: TStringField;
    SPQ: TSDQuery;
    SPQKode: TStringField;
    SPQJenisKendaraan: TStringField;
    SPQTipePerawatan: TStringField;
    SPQPeriodeWaktu: TIntegerField;
    SPQPeriodeKm: TIntegerField;
    SPQStandardWaktu: TIntegerField;
    MasterQJenisKendaraan: TStringField;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    JenisPerawatanQDetailPerawatan: TStringField;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    DeleteJenisPerawatanQ: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    UpdateDeleteJenisPerawatan: TSDUpdateSQL;
    ExecuteDeleteJenisPerawatanQ: TSDQuery;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    VLPQBonBarang: TStringField;
    VLPQNamaBarang: TStringField;
    ViewQPlatNo: TStringField;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQRekanan: TBooleanField;
    ArmadaQKodeRekanan: TStringField;
    VLPQJumlahDiminta: TFloatField;
    VLPQJumlahKeluar: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton1Click(Sender: TObject);
    procedure MasterVGridPPEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridVerifikatorEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridOperatorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGridDBTableView1Column2PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  LaporanPerawatanFm: TLaporanPerawatanFm;
  MasterOriSQL: string;
  paramkode:string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, ArmadaDropDown,
  PegawaiDropDown, StandarPerawatanDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TLaporanPerawatanFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TLaporanPerawatanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TLaporanPerawatanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TLaporanPerawatanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  ArmadaQ.Open;
  PegawaiQ.Open;
  JenisPerawatanQ.Open;
  SPQ.Open;
end;

procedure TLaporanPerawatanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
  VSPKQ.Close;
  VLPQ.Close;
  VKnbalQ.Close;
  VTKQ.Close;
  SPQ.Close;
  JenisPerawatanQ.Close;
  MasterQTanggal.Value:=Trunc(Now());
end;

procedure TLaporanPerawatanFm.KodeEditEnter(Sender: TObject);
begin
  //buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  cxButton1.Enabled:=false;
  MasterQ.Close;
end;

procedure TLaporanPerawatanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPerawatan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePerawatan.AsBoolean;
  cxButton1.Enabled:=menuutamafm.UserQDeletePerawatan.AsBoolean;
end;

procedure TLaporanPerawatanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:= 'ON PROCESS';
      MasterQButuhPerbaikan.AsBoolean:=False;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxButton1.Enabled:=menuutamafm.UserQDeletePerawatan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerawatan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TLaporanPerawatanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TLaporanPerawatanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;

    JenisPerawatanQ.Open;
    JenisPerawatanQ.Edit;
    JenisPerawatanQ.First;
    while JenisPerawatanQ.Eof=false do
    begin
      JenisPerawatanQ.Edit;
      JenisPerawatanQKodePerawatan.AsString:=KodeEdit.Text;
      JenisPerawatanQ.Post;
      JenisPerawatanQ.Next;
    end;
    JenisPerawatanQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    JenisPerawatanQ.CommitUpdates;
    ShowMessage('Laporan Perawatan dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      JenisPerawatanQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  viewq.Refresh;
  JenisPerawatanQ.Open;
  JenisPerawatanQ.Refresh;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TLaporanPerawatanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TLaporanPerawatanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TLaporanPerawatanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Laporan Perawatan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Laporan Perawatan telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Laporan Perawatan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TLaporanPerawatanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLaporanPerawatanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    //buttonCetak.Enabled:=true;
    kodeedit.Text:=ViewQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerawatan.AsBoolean;
    MasterVGrid.Enabled:=True;
    BarangQ.Open;
    DeleteJenisPerawatanQ.Open;
    ExecuteDeleteJenisPerawatanQ.Open;
    //cxDBVerticalGrid1.Enabled:=True;
    VSPKQ.Close;
    VSPKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VSPKQ.Open;
    VLPQ.Close;
    VLPQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VLPQ.Open;
    VKnbalQ.Close;
    VKnbalQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VKnbalQ.Open;
    VTKQ.Close;
    VTKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VTKQ.Open;
    SPQ.Close;
    SPQ.SQL.Text:='select * from standarperawatan';
    SPQ.Open;
    JenisPerawatanQ.Close;
    JenisPerawatanQ.ParamByName('text').AsString:= ViewQKode.AsString;
    JenisPerawatanQ.Open;
    cxGrid4.Enabled:=True;
    //JenisPerawatanQ.Refresh;
    //ShowMessage(ViewQKode.AsString);
end;

procedure TLaporanPerawatanFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;
end;

procedure TLaporanPerawatanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ekorQ.Close;
    ekorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ekorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQEkor.AsString:= ekorQkode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TLaporanPerawatanFm.KodeEditKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #13 Then Begin
    //buttonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select pp.*,lp.Kode as kode_lp, a.PlatNo,a.Kode as kode_armada, s.Nama from permintaanperbaikan pp left outer join laporanperbaikan lp on pp.kode=lp.PP, armada a, sopir s where pp.armada=a.kode and a.sopir=s.kode and pp.kode = '+ QuotedStr(KodeEdit.Text));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerawatan.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
  end;
end;

procedure TLaporanPerawatanFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus PermintaanPerbaikan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteJenisPerawatanQ.Close;
       DeleteJenisPerawatanQ.ParamByName('text').AsString:=MasterQKode.AsString;
       DeleteJenisPerawatanQ.Open;
       DeleteJenisPerawatanQ.First;
       while DeleteJenisPerawatanQ.Eof=FALSE
       do
       begin
        ExecuteDeleteJenisPerawatanQ.SQL.Text:=('delete from DetailPerawatanJenis where KodePerawatan='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteJenisPerawatanQ.ExecSQL;
        DeleteJenisPerawatanQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('PermintaanPerbaikan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       JenisPerawatanQ.RollbackUpdates;
       MessageDlg('PermintaanPerbaikan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewPPQ.Refresh;
  end;
  ViewQ.Refresh;
  JenisPerawatanQ.Refresh;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TLaporanPerawatanFm.MasterVGridPPEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var tempsql : string;
begin
    PPQ.Close;
    tempsql:=PPQ.SQL.Text;
    PPQ.SQL.Text:='select pp.* from permintaanperbaikan pp left outer join laporanperbaikan lp on pp.kode=lp.pp where lp.kode is NULL';
    PPQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PPQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQVerifikator.AsString:= PPQKode.AsString;
    end;
    DropDownFm.Release;
    PPQ.Close;
    PPQ.SQL.Text:=tempsql;
    PPQ.Open;
end;

procedure TLaporanPerawatanFm.MasterVGridVerifikatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQVerifikator.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TLaporanPerawatanFm.MasterVGridOperatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiQ.Close;
  PegawaiQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
  if DropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQOperator.AsString:=PegawaiQKode.AsString;
    MasterQNamaOperator.AsString:=PegawaiQNama.AsString;
  end;
  DropDownFm.Release;
end;

procedure TLaporanPerawatanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:= ArmadaDropDownFm.kode;
    SPQ.Close;
    SPQ.SQL.Text:='select * from standarperawatan where jeniskendaraan=:text';
    SPQ.ParamByName('text').AsString:= MasterQJenisKendaraan.AsString;
    SPQ.Open;
    cxGrid4.Enabled:=True;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TLaporanPerawatanFm.cxGridDBTableView1Column2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  StandarPerawatanDropDownFm:=TStandarPerawatanDropDownFm.Create(self,MasterQJenisKendaraan.AsString);
  if StandarPerawatanDropDownFm.ShowModal=MrOK then
  begin
    JenisPerawatanQ.Open;
    JenisPerawatanQ.Edit;
    JenisPerawatanQKodePerawatan.AsString:='0';
    JenisPerawatanQJenisPerawatan.AsString:=StandarPerawatanDropDownFm.kode;
  end;
  StandarPerawatanDropDownFm.Release;
end;

procedure TLaporanPerawatanFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
    JenisPerawatanQ.Edit;
    JenisPerawatanQKodePerawatan.AsString:='0';
    JenisPerawatanQ.Post;
  end;
end;

procedure TLaporanPerawatanFm.ExitBtnClick(Sender: TObject);
begin
Release;
end;

procedure TLaporanPerawatanFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewQ.Open;
end;

procedure TLaporanPerawatanFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewQ.Open;
end;

end.
