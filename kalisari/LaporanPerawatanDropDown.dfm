object LaporanPerawatanDropDownFm: TLaporanPerawatanDropDownFm
  Left = 353
  Top = 222
  Width = 956
  Height = 497
  Caption = 'LaporanPerawatanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 940
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 94
      end
      object cxGrid1DBTableView1ArmadaPlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'ArmadaPlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 109
      end
      object cxGrid1DBTableView1ArmadaNoBody: TcxGridDBColumn
        DataBinding.FieldName = 'ArmadaNoBody'
        Options.SortByDisplayText = isbtOn
        Width = 118
      end
      object cxGrid1DBTableView1NamaVerifikator: TcxGridDBColumn
        DataBinding.FieldName = 'NamaVerifikator'
        Width = 175
      end
      object cxGrid1DBTableView1ButuhPerbaikan: TcxGridDBColumn
        DataBinding.FieldName = 'ButuhPerbaikan'
        Width = 120
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 111
      end
      object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
        DataBinding.FieldName = 'Keterangan'
        Width = 314
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object LPRQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from laporanperawatan where status<>"FINISHED"'
      'order by tglentry desc')
    Left = 216
    Top = 368
    object LPRQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LPRQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object LPRQArmadaPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'ArmadaPlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object LPRQArmadaNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'ArmadaNoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object LPRQNamaVerifikator: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaVerifikator'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Verifikator'
      Size = 50
      Lookup = True
    end
    object LPRQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
      Visible = False
    end
    object LPRQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
      Visible = False
    end
    object LPRQVerifikator: TStringField
      FieldName = 'Verifikator'
      Visible = False
      Size = 10
    end
    object LPRQButuhPerbaikan: TBooleanField
      FieldName = 'ButuhPerbaikan'
    end
    object LPRQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
      Required = True
      Visible = False
    end
    object LPRQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object LPRQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object LPRQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object LPRQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object LPRQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object LPRQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object LPRQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object LPRQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
  end
  object LPBDs: TDataSource
    DataSet = LPRQ
    Left = 280
    Top = 368
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 248
    Top = 368
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 312
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
end
