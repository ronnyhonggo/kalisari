object LaporanPerbaikanDropDownFm: TLaporanPerbaikanDropDownFm
  Left = 200
  Top = 221
  Width = 1193
  Height = 498
  Caption = 'LaporanPerbaikanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1177
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 90
      end
      object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 155
      end
      object cxGrid1DBTableView1NoBody: TcxGridDBColumn
        DataBinding.FieldName = 'NoBody'
        Options.SortByDisplayText = isbtOn
        Width = 79
      end
      object cxGrid1DBTableView1AnalisaMasalah: TcxGridDBColumn
        DataBinding.FieldName = 'AnalisaMasalah'
        Width = 628
      end
      object cxGrid1DBTableView1TindakanPerbaikan: TcxGridDBColumn
        DataBinding.FieldName = 'TindakanPerbaikan'
        Width = 637
      end
      object cxGrid1DBTableView1KmArmadaSekarang: TcxGridDBColumn
        DataBinding.FieldName = 'KmArmadaSekarang'
        Width = 84
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 111
      end
      object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
        DataBinding.FieldName = 'Keterangan'
        Width = 405
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object LPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select lp.*, a.platno as '#39'PlatNo'#39', a.nobody as '#39'NoBody'#39
      
        'from laporanperbaikan lp, permintaanperbaikan pp left join armad' +
        'a a on pp.armada=a.kode'
      'where lp.pp=pp.kode AND lp.status<>"FINISHED"'
      'order by tglentry desc')
    Left = 216
    Top = 368
    object LPBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LPBQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object LPBQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object LPBQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object LPBQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      Required = True
      BlobType = ftMemo
    end
    object LPBQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object LPBQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object LPBQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object LPBQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
    end
    object LPBQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Size = 10
    end
    object LPBQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
    end
    object LPBQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object LPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object LPBQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object LPBQSopirClaim: TStringField
      FieldName = 'SopirClaim'
      Size = 10
    end
    object LPBQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object LPBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object LPBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object LPBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object LPBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object LPBQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object LPBQArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'Armada'
      LookupDataSet = PPQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Armada'
      KeyFields = 'PP'
      Size = 10
      Lookup = True
    end
    object LPBQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object LPBQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
  end
  object LPBDs: TDataSource
    DataSet = LPBQ
    Left = 280
    Top = 368
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 248
    Top = 368
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 304
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object PPQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from permintaanperbaikan')
    Left = 344
    Top = 368
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
end
