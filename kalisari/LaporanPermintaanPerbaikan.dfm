object LaporanPermintaanPerbaikanFm: TLaporanPermintaanPerbaikanFm
  Left = 121
  Top = 63
  BorderStyle = bsDialog
  Caption = 'Laporan Pekerjaan Teknik'
  ClientHeight = 612
  ClientWidth = 952
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 952
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 536
    Width = 952
    Height = 50
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 179
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxButton1: TcxButton
      Left = 92
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = cxButton1Click
    end
    object buttonCetak: TcxButton
      Left = 563
      Top = 9
      Width = 75
      Height = 25
      Caption = 'CETAK'
      Enabled = False
      TabOrder = 3
      OnClick = buttonCetakClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 654
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 48
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 425
    Height = 305
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 169
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridDBEditorRow7: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tanggal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPP: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPPEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PP'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPeminta'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 1
      Index = 1
      Version = 1
    end
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Height = 52
      Properties.EditPropertiesClassName = 'TcxMemoProperties'
      Properties.EditProperties.MaxLength = 1000
      Properties.DataBinding.FieldName = 'Keluhan'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 1
      Index = 2
      Version = 1
    end
    object MasterVGridClaimSopir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ClaimSopir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridSopirClaim: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridSopirClaimEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'SopirClaim'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridDBEditorRow4: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPengemudi'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object MasterVGridKeteranganClaimSopir: TcxDBEditorRow
      Height = 52
      Properties.DataBinding.FieldName = 'KeteranganClaimSopir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridAnalisaMasalah: TcxDBEditorRow
      Height = 50
      Properties.DataBinding.FieldName = 'AnalisaMasalah'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridWaktuMulai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'WaktuMulai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridWaktuSelesai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'WaktuSelesai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridVerifikator: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridVerifikatorEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Verifikator'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridDBEditorRow6: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaVerifikator'
      Properties.Options.Editing = False
      ID = 13
      ParentID = 12
      Index = 0
      Version = 1
    end
    object MasterVGridTglSerahTerima: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglSerahTerima'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridPICSerahTerima: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPICSerahTerimaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PICSerahTerima'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridDBEditorRow5: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPIC'
      Properties.Options.Editing = False
      ID = 16
      ParentID = 15
      Index = 0
      Version = 1
    end
    object MasterVGridKmArmadaSekarang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KmArmadaSekarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Items = <
        item
          Caption = 'ON PROCESS'
          Value = 'ON PROCESS'
        end
        item
          Caption = 'PENDING'
          Value = 'PENDING'
        end
        item
          Caption = 'FINISHED'
          Value = 'FINISHED'
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 18
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 51
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 19
      ParentID = -1
      Index = 13
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 586
    Width = 952
    Height = 26
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 353
    Width = 952
    Height = 183
    Align = alBottom
    BevelOuter = bvRaised
    BevelWidth = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = ViewDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 79
      end
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 83
      end
      object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Width = 86
      end
      object cxGrid1DBTableView1NoBody: TcxGridDBColumn
        DataBinding.FieldName = 'NoBody'
        Width = 56
      end
      object cxGrid1DBTableView1AnalisaMasalah: TcxGridDBColumn
        DataBinding.FieldName = 'AnalisaMasalah'
        Width = 232
      end
      object cxGrid1DBTableView1TindakanPerbaikan: TcxGridDBColumn
        DataBinding.FieldName = 'TindakanPerbaikan'
        Width = 268
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 95
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel2: TPanel
    Left = 689
    Top = 48
    Width = 263
    Height = 305
    Align = alCustom
    TabOrder = 6
    object Panel5: TPanel
      Left = 1
      Top = 134
      Width = 261
      Height = 170
      Align = alBottom
      TabOrder = 0
      object Label4: TLabel
        Left = 1
        Top = 1
        Width = 259
        Height = 24
        Align = alTop
        Caption = 'Tukar Komponen : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid5: TcxGrid
        Left = 1
        Top = 25
        Width = 259
        Height = 144
        Align = alClient
        TabOrder = 0
        object cxGrid5DBTableView1: TcxGridDBTableView
          DataController.DataSource = TKDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid5DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
            Width = 33
          end
          object cxGrid5DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'TglTukar'
            Width = 92
          end
          object cxGrid5DBTableView1Column6: TcxGridDBColumn
            DataBinding.FieldName = 'Barang Ke'
            Width = 130
          end
        end
        object cxGrid5Level1: TcxGridLevel
          GridView = cxGrid5DBTableView1
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 261
      Height = 133
      Align = alClient
      Caption = 'Panel6'
      TabOrder = 1
      object Label3: TLabel
        Left = 1
        Top = 1
        Width = 259
        Height = 24
        Align = alTop
        Caption = 'Kanibal : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid4: TcxGrid
        Left = 1
        Top = 25
        Width = 259
        Height = 107
        Align = alClient
        TabOrder = 0
        object cxGrid4DBTableView1: TcxGridDBTableView
          DataController.DataSource = KnbalDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid4DBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
            Width = 59
          end
          object cxGrid4DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'nama'
            Width = 75
          end
          object cxGrid4DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'KeBarang'
          end
        end
        object cxGrid4Level1: TcxGridLevel
          GridView = cxGrid4DBTableView1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 425
    Top = 48
    Width = 527
    Height = 305
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 5
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 525
      Height = 133
      Align = alClient
      Caption = 'Panel3'
      TabOrder = 0
      object Label1: TLabel
        Left = 1
        Top = 1
        Width = 523
        Height = 24
        Align = alTop
        Caption = 'SPK : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid2: TcxGrid
        Left = 1
        Top = 25
        Width = 523
        Height = 107
        Align = alClient
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          DataController.DataSource = SPKDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView1Kode: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
            Width = 69
          end
          object cxGrid2DBTableView1Tanggal: TcxGridDBColumn
            DataBinding.FieldName = 'Tanggal'
            Width = 78
          end
          object cxGrid2DBTableView1NamaMekanik: TcxGridDBColumn
            DataBinding.FieldName = 'NamaMekanik'
            Width = 135
          end
          object cxGrid2DBTableView1DetailTindakan: TcxGridDBColumn
            DataBinding.FieldName = 'DetailTindakan'
            Width = 165
          end
          object cxGrid2DBTableView1Status: TcxGridDBColumn
            DataBinding.FieldName = 'Status'
            Width = 103
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 134
      Width = 525
      Height = 170
      Align = alBottom
      Caption = 'Panel4'
      TabOrder = 1
      object Label2: TLabel
        Left = 1
        Top = 1
        Width = 523
        Height = 24
        Align = alTop
        Caption = 'Bon Barang : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid3: TcxGrid
        Left = 1
        Top = 25
        Width = 523
        Height = 144
        Align = alClient
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          DataController.DataSource = LPDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1Column4: TcxGridDBColumn
            DataBinding.FieldName = 'BonBarang'
          end
          object cxGrid3DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'NamaBarang'
            Width = 194
          end
          object cxGrid3DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'JumlahDiminta'
            Width = 139
          end
          object cxGrid3DBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'JumlahKeluar'
            Width = 164
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
  end
  object MasterQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select *, '#39#39' as NoBody, '#39#39' as PlatNo, '#39#39' as NamaPeminta, '#39#39' as K' +
        'eluhan, '#39#39' as NamaPengemudi, '#39#39' as NamaVerifikator, '#39#39' as NamaPI' +
        'C '
      'from laporanperbaikan')
    UpdateObject = MasterUS
    Left = 249
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object MasterQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object MasterQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object MasterQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object MasterQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object MasterQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
    end
    object MasterQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Size = 10
    end
    object MasterQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object MasterQSopirClaim: TStringField
      FieldName = 'SopirClaim'
      Size = 10
    end
    object MasterQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNoBody: TStringField
      FieldName = 'NoBody'
      LookupCache = True
    end
    object MasterQKeluhan: TStringField
      FieldName = 'Keluhan'
      LookupCache = True
      Size = 250
    end
    object MasterQNamaPeminta: TStringField
      FieldName = 'NamaPeminta'
      LookupCache = True
      Size = 100
    end
    object MasterQNamaPengemudi: TStringField
      FieldName = 'NamaPengemudi'
      Size = 100
    end
    object MasterQNamaVerifikator: TStringField
      FieldName = 'NamaVerifikator'
      LookupCache = True
      Size = 100
    end
    object MasterQNamaPIC: TStringField
      FieldName = 'NamaPIC'
      LookupCache = True
      Size = 100
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      LookupCache = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 284
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PP, Tanggal, AnalisaMasalah, TindakanPerbaikan, Wak' +
        'tuMulai, WaktuSelesai, Verifikator, TglSerahTerima, PICSerahTeri' +
        'ma, KmArmadaSekarang, Status, Keterangan, ClaimSopir, SopirClaim' +
        ', KeteranganClaimSopir, CreateDate, CreateBy, Operator, TglEntry' +
        ', TglCetak'#13#10'from laporanperbaikan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update laporanperbaikan'
      'set'
      '  Kode = :Kode,'
      '  PP = :PP,'
      '  Tanggal = :Tanggal,'
      '  AnalisaMasalah = :AnalisaMasalah,'
      '  TindakanPerbaikan = :TindakanPerbaikan,'
      '  WaktuMulai = :WaktuMulai,'
      '  WaktuSelesai = :WaktuSelesai,'
      '  Verifikator = :Verifikator,'
      '  TglSerahTerima = :TglSerahTerima,'
      '  PICSerahTerima = :PICSerahTerima,'
      '  KmArmadaSekarang = :KmArmadaSekarang,'
      '  Status = :Status,'
      '  Keterangan = :Keterangan,'
      '  ClaimSopir = :ClaimSopir,'
      '  SopirClaim = :SopirClaim,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into laporanperbaikan'
      
        '  (Kode, PP, Tanggal, AnalisaMasalah, TindakanPerbaikan, WaktuMu' +
        'lai, WaktuSelesai, Verifikator, TglSerahTerima, PICSerahTerima, ' +
        'KmArmadaSekarang, Status, Keterangan, ClaimSopir, SopirClaim, Ke' +
        'teranganClaimSopir, CreateDate, CreateBy, Operator, TglEntry, Tg' +
        'lCetak)'
      'values'
      
        '  (:Kode, :PP, :Tanggal, :AnalisaMasalah, :TindakanPerbaikan, :W' +
        'aktuMulai, :WaktuSelesai, :Verifikator, :TglSerahTerima, :PICSer' +
        'ahTerima, :KmArmadaSekarang, :Status, :Keterangan, :ClaimSopir, ' +
        ':SopirClaim, :KeteranganClaimSopir, :CreateDate, :CreateBy, :Ope' +
        'rator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from laporanperbaikan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 316
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from laporanperbaikan order by kode desc')
    Left = 201
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'Select * from pegawai'
      'where kode=:text')
    Left = 417
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object PPQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select pp.* '
      
        'from permintaanperbaikan pp left outer join laporanperbaikan lp ' +
        'on pp.kode=lp.pp'
      'where pp.kode=:text')
    Left = 385
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PPQRekanan: TStringField
      FieldName = 'Rekanan'
      Size = 10
    end
    object PPQCaraMinta: TStringField
      FieldName = 'CaraMinta'
      Size = 50
    end
    object PPQNoMinta: TStringField
      FieldName = 'NoMinta'
      Size = 50
    end
    object PPQJamMinta: TStringField
      FieldName = 'JamMinta'
      Size = 50
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Size = 10
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object viewPPQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select pp.* from permintaanperbaikan pp left outer join laporanp' +
        'erbaikan lp on pp.kode=lp.pp where lp.kode is NULL')
    Left = 481
    Top = 9
    object viewPPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewPPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object viewPPQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewPPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object viewPPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object viewPPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewPPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewPPQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewPPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewPPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object SJDS: TDataSource
    DataSet = viewPPQ
    Enabled = False
    Left = 582
    Top = 13
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada'
      'where kode=:text')
    Left = 449
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object ArmadaQRekanan: TBooleanField
      FieldName = 'Rekanan'
    end
    object ArmadaQKodeRekanan: TStringField
      FieldName = 'KodeRekanan'
      Size = 10
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 168
    Top = 8
  end
  object ekorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select kode,panjang,berat from ekor')
    Left = 615
    Top = 15
    object ekorQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object ekorQpanjang: TStringField
      FieldName = 'panjang'
      Size = 50
    end
    object ekorQberat: TStringField
      FieldName = 'berat'
      Size = 50
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewQ
    Left = 545
    Top = 12
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(lp.tanggal as date) as Tgl,lp.*, a.PlatNo, a.NoBody'
      
        'from LaporanPerbaikan lp left join PermintaanPerbaikan pp on lp.' +
        'PP=pp.Kode'
      'left join Armada a on pp.Armada=a.Kode'
      
        'where (lp.Tanggal>=:text1 and lp.Tanggal<=:text2) or lp.Tanggal ' +
        'is null'
      'order by tglentry desc')
    Left = 512
    Top = 12
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewQTgl: TStringField
      FieldName = 'Tgl'
    end
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object ViewQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object ViewQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object ViewQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object ViewQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object ViewQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object ViewQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object ViewQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
    end
    object ViewQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Size = 10
    end
    object ViewQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object ViewQSopirClaim: TStringField
      FieldName = 'SopirClaim'
      Size = 10
    end
    object ViewQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
    end
    object ViewQNoBody: TStringField
      FieldName = 'NoBody'
      Required = True
    end
  end
  object VSPKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select spk.*,p.nama as NamaMekanik from suratperintahkerja spk l' +
        'eft join pegawai p on spk.mekanik=p.kode'
      'where laporanperbaikan = :text')
    Left = 676
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VSPKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VSPKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object VSPKQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object VSPKQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object VSPKQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object VSPKQLainLain: TBooleanField
      FieldName = 'LainLain'
    end
    object VSPKQPlatNo: TStringField
      FieldName = 'PlatNo'
    end
    object VSPKQNoBody: TStringField
      FieldName = 'NoBody'
    end
    object VSPKQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object VSPKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object VSPKQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object VSPKQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object VSPKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object VSPKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object VSPKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object VSPKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object VSPKQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object VSPKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object VSPKQHasilKerja: TMemoField
      FieldName = 'HasilKerja'
      BlobType = ftMemo
    end
    object VSPKQKategoriPerbaikan: TStringField
      FieldName = 'KategoriPerbaikan'
      Size = 50
    end
    object VSPKQNamaMekanik: TStringField
      FieldName = 'NamaMekanik'
      Size = 50
    end
  end
  object SPKDs: TDataSource
    DataSet = VSPKQ
    Left = 708
    Top = 15
  end
  object VLPQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select bb.kode as BonBarang, b.nama as NamaBarang, dbb.JumlahDim' +
        'inta, (select sum(dbkb.jumlahkeluar) from bonkeluarbarang bkb, d' +
        'etailbkb dbkb where bkb.kode=dbkb.kodebkb AND bkb.bonbarang = bb' +
        '.kode AND dbb.KodeBonBarang=bb.Kode AND dbkb.Barang=dbb.KodeBara' +
        'ng) as JumlahKeluar'
      'from bonbarang bb, barang b, detailbonbarang dbb'
      
        'where bb.laporanperbaikan = :text AND dbb.kodebonbarang = bb.kod' +
        'e AND b.kode = dbb.kodebarang')
    Left = 742
    Top = 12
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VLPQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object VLPQNamaBarang: TStringField
      FieldName = 'NamaBarang'
      Required = True
      Size = 50
    end
    object VLPQJumlahDiminta: TFloatField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object VLPQJumlahKeluar: TFloatField
      FieldName = 'JumlahKeluar'
    end
  end
  object LPDs: TDataSource
    DataSet = VLPQ
    Left = 775
    Top = 13
  end
  object VKnbalQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select abk.Kode, b.Nama, abk.barang'
      'from ambilbarangkanibal abk, barangkanibal bk, barang b'
      
        'where abk.laporanperbaikan = :text and bk.kode=abk.daribarang an' +
        'd b.kode = bk.barang')
    Left = 805
    Top = 13
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VKnbalQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VKnbalQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object VKnbalQbarang: TStringField
      FieldName = 'barang'
      Size = 10
    end
    object VKnbalQKeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KeBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'barang'
      Size = 50
      Lookup = True
    end
  end
  object KnbalDs: TDataSource
    DataSet = VKnbalQ
    Left = 836
    Top = 14
  end
  object VTKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *'
      'from tukarkomponen tk, suratperintahkerja spk'
      'where spk.laporanperbaikan = :text and tk.spk = spk.kode')
    Left = 865
    Top = 12
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ' '
      end>
    object VTKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VTKQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
      Required = True
    end
    object VTKQDariArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object VTKQKeArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
    object VTKQBarangDari2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Dari'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangDari'
      Size = 50
      Lookup = True
    end
    object VTKQBarangKe2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Ke'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangKe'
      Size = 50
      Lookup = True
    end
    object VTKQDariArmada: TStringField
      FieldName = 'DariArmada'
      Required = True
      Size = 10
    end
    object VTKQKeArmada: TStringField
      FieldName = 'KeArmada'
      Required = True
      Size = 10
    end
    object VTKQBarangDari: TStringField
      FieldName = 'BarangDari'
      Required = True
      Size = 10
    end
    object VTKQBarangKe: TStringField
      FieldName = 'BarangKe'
      Required = True
      Size = 10
    end
    object VTKQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object VTKQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object VTKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object VTKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object VTKQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object VTKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object VTKQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object TKDs: TDataSource
    DataSet = VTKQ
    Left = 897
    Top = 9
  end
  object BarangQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *'
      'from barang')
    Left = 353
    Top = 7
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQFoto: TBlobField
      FieldName = 'Foto'
    end
    object BarangQNoPabrikan: TStringField
      FieldName = 'NoPabrikan'
      Size = 50
    end
  end
  object updateQ: TSDQuery
    Options = []
    Left = 648
    Top = 16
  end
end
