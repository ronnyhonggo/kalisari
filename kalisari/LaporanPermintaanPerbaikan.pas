unit LaporanPermintaanPerbaikan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox;

type
  TLaporanPermintaanPerbaikanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PegawaiQ: TSDQuery;
    PPQ: TSDQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewPPQ: TSDQuery;
    SJDS: TDataSource;
    ArmadaQ: TSDQuery;
    Crpe1: TCrpe;
    ekorQ: TSDQuery;
    ekorQkode: TStringField;
    ekorQpanjang: TStringField;
    ekorQberat: TStringField;
    KodeQkode: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    ViewDs: TDataSource;
    cxButton1: TcxButton;
    MasterVGridPP: TcxDBEditorRow;
    MasterVGridAnalisaMasalah: TcxDBEditorRow;
    MasterVGridWaktuMulai: TcxDBEditorRow;
    MasterVGridWaktuSelesai: TcxDBEditorRow;
    MasterVGridVerifikator: TcxDBEditorRow;
    MasterVGridTglSerahTerima: TcxDBEditorRow;
    MasterVGridPICSerahTerima: TcxDBEditorRow;
    MasterVGridKmArmadaSekarang: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridClaimSopir: TcxDBEditorRow;
    MasterVGridSopirClaim: TcxDBEditorRow;
    MasterVGridKeteranganClaimSopir: TcxDBEditorRow;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    MasterVGridDBEditorRow5: TcxDBEditorRow;
    MasterVGridDBEditorRow6: TcxDBEditorRow;
    MasterVGridDBEditorRow7: TcxDBEditorRow;
    ViewQ: TSDQuery;
    VSPKQ: TSDQuery;
    SPKDs: TDataSource;
    Panel1: TPanel;
    VLPQ: TSDQuery;
    LPDs: TDataSource;
    Panel2: TPanel;
    VKnbalQ: TSDQuery;
    KnbalDs: TDataSource;
    Panel3: TPanel;
    Label1: TLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    Panel4: TPanel;
    Label2: TLabel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1Column4: TcxGridDBColumn;
    cxGrid3DBTableView1Column1: TcxGridDBColumn;
    cxGrid3DBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBTableView1Column3: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    Panel5: TPanel;
    Panel6: TPanel;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4DBTableView1Column1: TcxGridDBColumn;
    cxGrid4DBTableView1Column2: TcxGridDBColumn;
    cxGrid4Level1: TcxGridLevel;
    Label3: TLabel;
    Label4: TLabel;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5: TcxGrid;
    VTKQ: TSDQuery;
    TKDs: TDataSource;
    VKnbalQKode: TStringField;
    VKnbalQNama: TStringField;
    VKnbalQbarang: TStringField;
    VKnbalQKeBarang: TStringField;
    cxGrid4DBTableView1Column3: TcxGridDBColumn;
    VTKQKode: TStringField;
    VTKQTglTukar: TDateTimeField;
    VTKQDariArmada2: TStringField;
    VTKQKeArmada2: TStringField;
    VTKQBarangDari2: TStringField;
    VTKQBarangKe2: TStringField;
    cxGrid5DBTableView1Column1: TcxGridDBColumn;
    cxGrid5DBTableView1Column2: TcxGridDBColumn;
    cxGrid5DBTableView1Column6: TcxGridDBColumn;
    VTKQDariArmada: TStringField;
    VTKQKeArmada: TStringField;
    VTKQBarangDari: TStringField;
    VTKQBarangKe: TStringField;
    VTKQPeminta: TStringField;
    VTKQLaporanPerbaikan: TStringField;
    VTKQCreateDate: TDateTimeField;
    VTKQCreateBy: TStringField;
    VTKQOperator: TStringField;
    VTKQTglEntry: TDateTimeField;
    VTKQTglCetak: TDateTimeField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    buttonCetak: TcxButton;
    viewPPQKode: TStringField;
    viewPPQTanggal: TDateTimeField;
    viewPPQArmada: TStringField;
    viewPPQPeminta: TStringField;
    viewPPQKeluhan: TMemoField;
    viewPPQCreateDate: TDateTimeField;
    viewPPQCreateBy: TStringField;
    viewPPQOperator: TStringField;
    viewPPQTglEntry: TDateTimeField;
    viewPPQTglCetak: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    VLPQBonBarang: TStringField;
    VLPQNamaBarang: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQRekanan: TBooleanField;
    ArmadaQKodeRekanan: TStringField;
    VLPQJumlahDiminta: TFloatField;
    VLPQJumlahKeluar: TFloatField;
    MasterQKode: TStringField;
    MasterQPP: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQAnalisaMasalah: TMemoField;
    MasterQTindakanPerbaikan: TMemoField;
    MasterQWaktuMulai: TDateTimeField;
    MasterQWaktuSelesai: TDateTimeField;
    MasterQVerifikator: TStringField;
    MasterQTglSerahTerima: TDateTimeField;
    MasterQPICSerahTerima: TStringField;
    MasterQKmArmadaSekarang: TIntegerField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQClaimSopir: TCurrencyField;
    MasterQSopirClaim: TStringField;
    MasterQKeteranganClaimSopir: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQNoBody: TStringField;
    MasterQNamaPeminta: TStringField;
    MasterQKeluhan: TStringField;
    MasterQNamaPengemudi: TStringField;
    MasterQNamaVerifikator: TStringField;
    MasterQNamaPIC: TStringField;
    MasterQPlatNo: TStringField;
    VSPKQKode: TStringField;
    VSPKQTanggal: TDateTimeField;
    VSPKQLaporanPerbaikan: TStringField;
    VSPKQLaporanPerawatan: TStringField;
    VSPKQRebuild: TStringField;
    VSPKQLainLain: TBooleanField;
    VSPKQPlatNo: TStringField;
    VSPKQNoBody: TStringField;
    VSPKQMekanik: TStringField;
    VSPKQDetailTindakan: TMemoField;
    VSPKQWaktuMulai: TDateTimeField;
    VSPKQWaktuSelesai: TDateTimeField;
    VSPKQStatus: TStringField;
    VSPKQKeterangan: TStringField;
    VSPKQCreateDate: TDateTimeField;
    VSPKQCreateBy: TStringField;
    VSPKQOperator: TStringField;
    VSPKQTglEntry: TDateTimeField;
    VSPKQHasilKerja: TMemoField;
    VSPKQKategoriPerbaikan: TStringField;
    VSPKQNamaMekanik: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid2DBTableView1DetailTindakan: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    cxGrid2DBTableView1NamaMekanik: TcxGridDBColumn;
    updateQ: TSDQuery;
    ViewQTgl: TStringField;
    ViewQKode: TStringField;
    ViewQPP: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQAnalisaMasalah: TMemoField;
    ViewQTindakanPerbaikan: TMemoField;
    ViewQWaktuMulai: TDateTimeField;
    ViewQWaktuSelesai: TDateTimeField;
    ViewQVerifikator: TStringField;
    ViewQTglSerahTerima: TDateTimeField;
    ViewQPICSerahTerima: TStringField;
    ViewQKmArmadaSekarang: TIntegerField;
    ViewQStatus: TStringField;
    ViewQKeterangan: TMemoField;
    ViewQClaimSopir: TCurrencyField;
    ViewQSopirClaim: TStringField;
    ViewQKeteranganClaimSopir: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQTglCetak: TDateTimeField;
    ViewQPlatNo: TStringField;
    ViewQNoBody: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1AnalisaMasalah: TcxGridDBColumn;
    cxGrid1DBTableView1TindakanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQArmada: TStringField;
    PPQRekanan: TStringField;
    PPQCaraMinta: TStringField;
    PPQNoMinta: TStringField;
    PPQJamMinta: TStringField;
    PPQPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton1Click(Sender: TObject);
    procedure MasterVGridPPEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridVerifikatorEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPICSerahTerimaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure MasterVGridSopirClaimEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  LaporanPermintaanPerbaikanFm: TLaporanPermintaanPerbaikanFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, PermintaanPerbaikanDropDown,
  PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TLaporanPermintaanPerbaikanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TLaporanPermintaanPerbaikanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TLaporanPermintaanPerbaikanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TLaporanPermintaanPerbaikanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  PPQ.Close;
  PPQ.ParamByName('text').AsString:='';
  PPQ.Open;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:='';
  ArmadaQ.Open;
  PegawaiQ.Close;
  PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.Open;
{  ArmadaQ.Open;
  PegawaiQ.Open;
  PPQ.Open; }
end;

procedure TLaporanPermintaanPerbaikanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
//  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
  VSPKQ.Close;
  VLPQ.Close;
  VKnbalQ.Close;
  VTKQ.Close;
  MasterQTanggal.Value:=Trunc(Now());
end;

procedure TLaporanPermintaanPerbaikanFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  cxButton1.Enabled:=false;
  MasterQ.Close;
end;

procedure TLaporanPermintaanPerbaikanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
//  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewQ.Open;

  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPerbaikan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikan.AsBoolean;
  cxButton1.Enabled:=menuutamafm.UserQDeletePerbaikan.AsBoolean;
end;

procedure TLaporanPermintaanPerbaikanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:='ON PROCESS';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxButton1.Enabled:=menuutamafm.UserQDeletePerbaikan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TLaporanPermintaanPerbaikanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TLaporanPermintaanPerbaikanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Laporan Pekerjaan Teknik dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  viewq.Refresh;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TLaporanPermintaanPerbaikanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TLaporanPermintaanPerbaikanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TLaporanPermintaanPerbaikanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Laporan Pekerjaan Teknik '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Laporan Pekerjaan Teknik telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Laporan Pekerjaan Teknik pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TLaporanPermintaanPerbaikanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TLaporanPermintaanPerbaikanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    buttonCetak.Enabled:=true;
    kodeedit.Text:=ViewQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikan.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
//    BarangQ.Open;
    MasterQ.Edit;
    
    PPQ.Close;
    PPQ.ParamByName('text').AsString:=MasterQPP.AsString;
    PPQ.Open;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=PPQArmada.AsString;
    ArmadaQ.Open;
    MasterQPlatNo.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBody.AsString:=ArmadaQNoBody.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=PPQPeminta.AsString;
    PegawaiQ.Open;
    MasterQNamaPeminta.AsString:=PegawaiQNama.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopirClaim.AsString;
    PegawaiQ.Open;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQVerifikator.AsString;
    PegawaiQ.Open;
    MasterQNamaVerifikator.AsString:=PegawaiQNama.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQPICSerahTerima.AsString;
    PegawaiQ.Open;
    MasterQNamaPIC.AsString:=PegawaiQNama.AsString;

    MasterQKeluhan.AsString:=PPQKeluhan.AsString;
    VSPKQ.Close;
    VSPKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VSPKQ.Open;
    VLPQ.Close;
    VLPQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VLPQ.Open;
    
{    VKnbalQ.Close;
    VKnbalQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VKnbalQ.Open;
    VTKQ.Close;
    VTKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VTKQ.Open; }
    //ShowMessage(ViewQKode.AsString);
end;

procedure TLaporanPermintaanPerbaikanFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   { jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;     }
end;

procedure TLaporanPermintaanPerbaikanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {   ekorQ.Close;
    ekorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ekorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQEkor.AsString:= ekorQkode.AsString;
    end;
    DropDownFm.Release;    }
end;

procedure TLaporanPermintaanPerbaikanFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus PermintaanPerbaikan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('PermintaanPerbaikan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('PermintaanPerbaikan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewQ.Refresh;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TLaporanPermintaanPerbaikanFm.MasterVGridPPEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var tempsql : string;
begin
  PermintaanPerbaikanDropDownFm:=TPermintaanPerbaikanDropDownFm.Create(self);
  if PermintaanPerbaikanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPP.AsString:=PermintaanPerbaikanDropDownFm.kode;
    PPQ.Close;
    PPQ.ParamByName('text').AsString:=PermintaanPerbaikanDropDownFm.kode;
    PPQ.Open;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=PPQArmada.AsString;
    ArmadaQ.Open;
    MasterQPlatNo.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBody.AsString:=ArmadaQNoBody.AsString;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=PPQPeminta.AsString;
    PegawaiQ.Open;
    MasterQNamaPeminta.AsString:=PegawaiQNama.AsString;
    MasterQKeluhan.AsString:=PPQKeluhan.AsString;
  end;
  PermintaanPerbaikanDropDownFm.Release;
end;

procedure TLaporanPermintaanPerbaikanFm.MasterVGridVerifikatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQVerifikator.AsString:= PegawaiDropDownFm.kode;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
    PegawaiQ.Open;
    MasterQNamaVerifikator.AsString:=PegawaiQNama.AsString;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TLaporanPermintaanPerbaikanFm.MasterVGridPICSerahTerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPICSerahTerima.AsString:= PegawaiDropDownFm.kode;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
    PegawaiQ.Open;
    MasterQNamaPIC.AsString:=PegawaiQNama.AsString;    
  end;
  PegawaiDropDownFm.Release;
end;

procedure TLaporanPermintaanPerbaikanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TLaporanPermintaanPerbaikanFm.MasterVGridSopirClaimEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSopirClaim.AsString:= PegawaiDropDownFm.kode;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=PegawaiDropDownFm.kode;
    PegawaiQ.Open;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TLaporanPermintaanPerbaikanFm.buttonCetakClick(Sender: TObject);
begin
  if MessageDlg('Cetak Laporan Pekerjaan Teknik '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'laporanperbaikan.rpt';
    Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    Crpe1.Print;
    Crpe1.CloseWindow;
    updateQ.Close;
    updateQ.SQL.Clear;
    updateQ.SQL.Add('update laporanperbaikan set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
    updateQ.ExecSQL;
    updateQ.Close;
    //viewSJQ.Close;
    //viewSJQ.Open;
  end;
end;

procedure TLaporanPermintaanPerbaikanFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewQ.Open;
end;

procedure TLaporanPermintaanPerbaikanFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  viewQ.Close;
  viewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewQ.Open;
end;

end.
