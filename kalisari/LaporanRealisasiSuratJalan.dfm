inherited LaporanRealisasiSuratJalanFm: TLaporanRealisasiSuratJalanFm
  Left = 84
  Top = 116
  Caption = 'Laporan Realisasi Surat Jalan'
  ClientWidth = 1102
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescrip: TLabel
    Width = 1102
    Caption = 
      'This example demonstates the ExpressQuantumGrid printing capabil' +
      'ities.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Width = 1102
  end
  inherited ToolBar1: TToolBar
    Width = 1102
    object tbtnFullCollapse: TToolButton
      Left = 123
      Top = 0
      Action = actFullCollapse
      ParentShowHint = False
      ShowHint = True
    end
    object tbtnFullExpand: TToolButton
      Left = 146
      Top = 0
      Action = actFullExpand
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 41
    Width = 1102
    Height = 40
    Align = alTop
    TabOrder = 2
    object DateTimePicker1: TDateTimePicker
      Left = 24
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587301469910000000
      Time = 41416.587301469910000000
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 144
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587307858800000000
      Time = 41416.587307858800000000
      TabOrder = 1
    end
    object Button1: TButton
      Left = 264
      Top = 8
      Width = 81
      Height = 22
      Caption = 'REFRESH'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 351
      Top = 9
      Width = 81
      Height = 21
      Caption = 'Export XLS'
      TabOrder = 3
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 81
    Width = 1102
    Height = 388
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object cxGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 1100
      Height = 386
      Align = alClient
      TabOrder = 0
      object tvPlanets: TcxGridTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.HeaderAutoHeight = True
        Styles.StyleSheet = tvssDevExpress
        object tvPlanetsNAME: TcxGridColumn
          Caption = 'Name'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvPlanetsNO: TcxGridColumn
          Caption = '#'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvPlanetsORBITS: TcxGridColumn
          Caption = 'Orbits'
          RepositoryItem = edrepCenterText
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
        end
        object tvPlanetsDISTANCE: TcxGridColumn
          Caption = 'Distance (000km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object tvPlanetsPERIOD: TcxGridColumn
          Caption = 'Period (days)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          Width = 80
        end
        object tvPlanetsDISCOVERER: TcxGridColumn
          Caption = 'Discoverer'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsDATE: TcxGridColumn
          Caption = 'Date'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsRADIUS: TcxGridColumn
          Caption = 'Radius (km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridDBBandedTableView1: TcxGridDBBandedTableView
        DataController.DataSource = MasterDs
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skSum
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            Column = cxGridDBBandedTableView1SisaDisetor
            DisplayText = 'Total'
            Sorted = True
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ExpandButtonsForEmptyDetails = False
        OptionsView.Footer = True
        OptionsView.GroupByHeaderLayout = ghlHorizontal
        OptionsView.GroupFooterMultiSummaries = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
        OptionsView.BandCaptionsInColumnAlternateCaption = True
        OptionsView.BandHeaderEndEllipsis = True
        Bands = <
          item
            Width = 908
          end>
        object cxGridDBBandedTableView1PlatNo: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PlatNo'
          Width = 71
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1TanggalMulai: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TanggalMulai'
          Width = 124
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1TanggalSelesai: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TanggalSelesai'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ShowTime = False
          Width = 144
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1SisaDisetor: TcxGridDBBandedColumn
          DataBinding.FieldName = 'SisaDisetor'
          Width = 146
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1LastEditor: TcxGridDBBandedColumn
          DataBinding.FieldName = 'LastEditor'
          Width = 135
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1TglEntry: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TglEntry'
          Width = 135
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Realisasi: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Realisasi'
          Width = 153
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBBandedTableView1
      end
    end
  end
  inherited mmMain: TMainMenu
    Left = 32
    Top = 187
    inherited miOptions: TMenuItem
      object miFullCollapsing: TMenuItem [0]
        Action = actFullCollapse
      end
      object miFullExpand: TMenuItem [1]
        Action = actFullExpand
      end
      object N3: TMenuItem [2]
        Caption = '-'
      end
    end
    inherited miHelp: TMenuItem
      Visible = False
    end
  end
  inherited sty: TActionList
    Left = 72
    Top = 179
    object actFullExpand: TAction
      Category = 'Options'
      Caption = 'Full &Expand'
      Hint = 'Full expand'
      ImageIndex = 8
      OnExecute = actFullExpandExecute
    end
    object actFullCollapse: TAction
      Category = 'Options'
      Caption = 'Full &Collapse'
      Hint = 'Full collapse'
      ImageIndex = 7
      OnExecute = actFullCollapseExecute
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Left = 656
    Top = 152
    object dxComponentPrinterLink1: TdxGridReportLink
      Active = True
      Component = cxGrid
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 200
      PrinterPage.GrayShading = True
      PrinterPage.Header = 100
      PrinterPage.Margins.Bottom = 500
      PrinterPage.Margins.Left = 500
      PrinterPage.Margins.Right = 500
      PrinterPage.Margins.Top = 500
      PrinterPage.PageSize.X = 8500
      PrinterPage.PageSize.Y = 11000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 1
      ReportDocument.CreationDate = 41672.673528240740000000
      BuiltInReportLink = True
    end
  end
  inherited dxPSEngineController1: TdxPSEngineController
    Active = True
    Left = 688
    Top = 152
  end
  inherited ilMain: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010160
  end
  inherited XPManifest1: TXPManifest
    Left = 728
    Top = 152
  end
  object edrepMain: TcxEditRepository
    Left = 184
    Top = 179
    object edrepCenterText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taCenter
    end
    object edrepRightText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taRightJustify
    end
  end
  object StyleRepository: TcxStyleRepository
    Left = 144
    Top = 179
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16777088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object tvssDevExpress: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.Inactive = cxStyle10
      Styles.IncSearch = cxStyle11
      Styles.Selection = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      BuiltIn = True
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from masterso')
    Left = 400
    Top = 224
    object SDQuery1Kodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SDQuery1Tgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SDQuery1Pelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SDQuery1Berangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SDQuery1Tiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SDQuery1Harga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SDQuery1PPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SDQuery1PembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SDQuery1TglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SDQuery1CaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SDQuery1KeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SDQuery1NoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SDQuery1NominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SDQuery1PenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SDQuery1Pelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SDQuery1TglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SDQuery1CaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SDQuery1KetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SDQuery1NoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SDQuery1NominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SDQuery1PenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SDQuery1Extend: TBooleanField
      FieldName = 'Extend'
    end
    object SDQuery1TglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SDQuery1BiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SDQuery1PPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SDQuery1KapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SDQuery1AC: TBooleanField
      FieldName = 'AC'
    end
    object SDQuery1Toilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SDQuery1AirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SDQuery1Rute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SDQuery1TglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SDQuery1Armada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SDQuery1Kontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SDQuery1PICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SDQuery1JamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SDQuery1NoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SDQuery1AlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SDQuery1Status: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SDQuery1StatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SDQuery1ReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SDQuery1PenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SDQuery1Keterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SDQuery1CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery1CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery1Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery1TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery1TglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 480
    Top = 232
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select a.PlatNo, ra.TanggalMulai, ra.TanggalSelesai, ra.SisaDise' +
        'tor, p.nama as "LastEditor", ra.TglEntry, "Karyawan" as Realisas' +
        'i'
      'from RealisasiAnjem ra , armada a, pegawai p, [user] u'
      'where'
      'ra.armada=a.kode and u.kode=ra.operator and p.kode=u.kodepegawai'
      'and ra.tanggalmulai>=:text1 and ra.tanggalmulai-1<=:text2'
      ''
      'union all'
      ''
      
        'select a.PlatNo, rt.tanggal as "TanggalMulai", rt.tanggal as "Ta' +
        'nggalSelesai", rt.SisaDisetor, p.nama as "LastEditor", rt.TglEnt' +
        'ry, "Trayek" as Realisasi'
      'from RealisasiTrayek rt, armada a, pegawai p, [user] u'
      'where '
      'rt.armada=a.kode and u.kode=rt.operator and p.kode=u.kodepegawai'
      'and rt.tanggal>=:text1 and rt.tanggal<=:text2 '
      ''
      'union all'
      ''
      
        'select a.PlatNo, so.berangkat as "TanggalMulai", so.tiba as "Tan' +
        'ggalSelesai", sj.SisaDisetor, p.nama as "LastEditor", sj.TglEntr' +
        'y, "Rombongan" as Realisasi'
      'from armada a, [user] u, pegawai p, MasterSJ sj left join'
      'MasterSO so on sj.NoSO=so.Kodenota '
      
        'where a.kode=so.armada and u.kode=sj.operator and u.kodepegawai=' +
        'p.kode and'
      
        'sj.tglentry>=:text1 and sj.tglentry-1<=:text2 and not (sj.SisaDi' +
        'setor is NULL)'
      ''
      'order by TglEntry')
    UpdateObject = MasterUs
    Left = 8
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object MasterQTanggalMulai: TDateTimeField
      FieldName = 'TanggalMulai'
    end
    object MasterQTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object MasterQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object MasterQLastEditor: TStringField
      FieldName = 'LastEditor'
      Required = True
      Size = 50
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQRealisasi: TStringField
      FieldName = 'Realisasi'
      Required = True
      Size = 9
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 40
    Top = 56
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other'
      'from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSJ'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSJ'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other)')
    DeleteSQL.Strings = (
      'delete from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 64
    Top = 56
  end
  object RealisasiAnjemQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select TanggalSelesai,SPBUAYaniLiter,Armada,cast('#39#39' as money) as' +
        ' Rupiah from RealisasiAnjem ra where SPBUAYaniLiter is not NULL ' +
        'and SPBUAYaniLiter>0'
      'and TanggalSelesai>=:text1 and TanggalSelesai<=:text2 union all'
      
        'select Tanggal,SPBUAYaniLiter,Armada,cast('#39#39' as money) as Rupiah' +
        ' from RealisasiTrayek where SPBUAYaniLiter is not NULL and SPBUA' +
        'YaniLiter>0'
      'and Tanggal>=:text1 and Tanggal<=:text2'
      'union all'
      
        'select TglKembali,SPBUAYaniLiter, so.Armada,cast('#39#39' as money) as' +
        ' Rupiah from MasterSJ sj left join'
      
        'MasterSO so on sj.NoSO=so.Kodenota where SPBUAYaniLiter is not N' +
        'ULL and SPBUAYaniLiter>0 and TglKembali>=:text1 and TglKembali<=' +
        ':text2')
    UpdateObject = RealisasiAnjemUs
    Left = 56
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object RealisasiAnjemQTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object RealisasiAnjemQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object RealisasiAnjemQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object RealisasiAnjemQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object RealisasiAnjemQJenisBBM: TStringField
      FieldKind = fkLookup
      FieldName = 'JenisBBM'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JenisBBM'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object RealisasiAnjemQRupiah: TCurrencyField
      FieldName = 'Rupiah'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 144
    Top = 232
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object RealisasiAnjemUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Peng' +
        'emudi, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, S' +
        'isaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULua' +
        'rLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiS' +
        'opir, PremiKernet, TabunganSopir, Tol, BiayaLainLain, Keterangan' +
        'BiayaLainLain, CreateBy, Operator, CreateDate, TglEntry'
      'from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  TanggalMulai = :TanggalMulai,'
      '  TanggalSelesai = :TanggalSelesai,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiAnjem'
      
        '  (Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Pengemud' +
        'i, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, SisaD' +
        'isetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULuarLit' +
        'er, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiSopir' +
        ', PremiKernet, TabunganSopir, Tol, BiayaLainLain, KeteranganBiay' +
        'aLainLain, CreateBy, Operator, CreateDate, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :TanggalMulai, :TanggalSelesai, :Armada, :Pe' +
        'ngemudi, :KilometerAwal, :KilometerAkhir, :Pendapatan, :Pengelua' +
        'ran, :SisaDisetor, :SPBUAYaniLiter, :SPBUAYaniUang, :SPBUAYaniJa' +
        'm, :SPBULuarLiter, :SPBULuarUang, :SPBULuarUangDiberi, :SPBULuar' +
        'Detail, :PremiSopir, :PremiKernet, :TabunganSopir, :Tol, :BiayaL' +
        'ainLain, :KeteranganBiayaLainLain, :CreateBy, :Operator, :Create' +
        'Date, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    Left = 88
    Top = 232
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Excel File|.xls'
    FilterIndex = 0
    Left = 56
    Top = 304
  end
end
