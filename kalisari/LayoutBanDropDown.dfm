object LayoutBanDropDownFm: TLayoutBanDropDownFm
  Left = 189
  Top = 190
  Width = 570
  Height = 347
  Caption = 'LayoutBanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 554
    Height = 308
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LayoutStyle = lsMultiRecordView
    OptionsView.CellAutoHeight = True
    OptionsBehavior.IncSearch = True
    OptionsData.Editing = False
    ParentFont = False
    TabOrder = 0
    OnDblClick = cxDBVerticalGrid1DblClick
    DataController.DataSource = LPBDs
    Version = 1
    object cxDBVerticalGrid1Nama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Nama'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1JumlahBan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JumlahBan'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Gambar: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Gambar'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object LayoutBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from masterlayoutban')
    Left = 24
    Top = 48
    object LayoutBanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LayoutBanQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object LayoutBanQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object LayoutBanQGambar: TBlobField
      FieldName = 'Gambar'
    end
  end
  object LPBDs: TDataSource
    DataSet = LayoutBanQ
    Left = 112
    Top = 56
  end
end
