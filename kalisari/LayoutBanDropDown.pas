unit LayoutBanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, cxImage, cxVGrid,
  cxDBVGrid, cxInplaceContainer;

type
  TLayoutBanDropDownFm = class(TForm)
    LayoutBanQ: TSDQuery;
    LPBDs: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    LayoutBanQNama: TStringField;
    LayoutBanQGambar: TBlobField;
    cxDBVerticalGrid1Nama: TcxDBEditorRow;
    cxDBVerticalGrid1Gambar: TcxDBEditorRow;
    LayoutBanQKode: TStringField;
    LayoutBanQJumlahBan: TIntegerField;
    cxDBVerticalGrid1JumlahBan: TcxDBEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode,jumlahban:string;
  end;

var
  LayoutBanDropDownFm: TLayoutBanDropDownFm;

implementation

{$R *.dfm}

procedure TLayoutBanDropDownFm.FormCreate(Sender: TObject);
begin
  LayoutBanQ.Open;
end;

procedure TLayoutBanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
{  kode:=LayoutBanQKode.AsString;
  ModalResult:=mrOK;   }
end;

procedure TLayoutBanDropDownFm.cxDBVerticalGrid1DblClick(Sender: TObject);
begin
  kode:=LayoutBanQKode.AsString;
  jumlahban:=LayoutBanQJumlahBan.AsString;
  ModalResult:=mrOK;
end;

end.
