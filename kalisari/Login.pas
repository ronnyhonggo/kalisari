unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxControls, cxContainer,
  cxEdit, cxTextEdit, cxGraphics, cxLookAndFeels, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue;

type
  TLoginFm = class(TForm)
    IdEdit: TcxTextEdit;
    CancelButton: TcxButton;
    LoginButton: TcxButton;
    lbl1: TLabel;
    lbl2: TLabel;
    PasswordEdit: TEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LoginButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    UserName,password:string;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  LoginFm: TLoginFm;

implementation

uses MenuUtama;

{$R *.dfm}

{ TForm1 }

constructor TLoginFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TLoginFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TLoginFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Release;
//  MenuUtamaFm.Close;
end;

procedure TLoginFm.LoginButtonClick(Sender: TObject);
begin
  username:=IdEdit.Text;
  password:=PasswordEdit.Text;
end;

end.
