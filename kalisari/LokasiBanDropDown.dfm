object LokasiBanDropDownFm: TLokasiBanDropDownFm
  Left = 331
  Top = 150
  Width = 530
  Height = 370
  Caption = 'LokasiBanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 514
    Height = 331
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LayoutStyle = lsMultiRecordView
    OptionsView.CellAutoHeight = True
    OptionsView.RowHeaderWidth = 112
    OptionsView.ValueWidth = 188
    OptionsBehavior.IncSearch = True
    OptionsData.Editing = False
    ParentFont = False
    TabOrder = 0
    OnDblClick = cxDBVerticalGrid1DblClick
    DataController.DataSource = LPBDs
    Version = 1
    object cxDBVerticalGrid1NamaLayout: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaLayout'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Nama: TcxDBEditorRow
      Properties.Caption = 'NamaLokasi'
      Properties.DataBinding.FieldName = 'Nama'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Gambar: TcxDBEditorRow
      Height = 170
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Gambar'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object LokasiAvaiBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select mlkb.* from masterlokasiban mlkb, MasterLayoutBan mlyb, A' +
        'rmada a where mlkb.KodeLayout=mlyb.Kode and a.LayoutBan=mlyb.Kod' +
        'e and a.Kode=:text'
      'and mlkb.Kode not in('
      
        'select kodelokasiban from DetailArmadaBan where KodeArmada=:text' +
        ')')
    Left = 8
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object LokasiAvaiBanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LokasiAvaiBanQKodeLayout: TStringField
      FieldName = 'KodeLayout'
      Required = True
      Size = 10
    end
    object LokasiAvaiBanQNamaLayout: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaLayout'
      LookupDataSet = LayoutBanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeLayout'
      Size = 50
      Lookup = True
    end
    object LokasiAvaiBanQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object LokasiAvaiBanQGambar: TBlobField
      FieldName = 'Gambar'
      Required = True
    end
  end
  object LPBDs: TDataSource
    DataSet = LokasiAvaiBanQ
    Left = 72
    Top = 96
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 40
    Top = 96
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object LayoutBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from masterlayoutban')
    Left = 104
    Top = 96
    object LayoutBanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LayoutBanQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object LayoutBanQGambar: TBlobField
      FieldName = 'Gambar'
    end
  end
  object LokasiBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Left = 144
    Top = 96
  end
end
