unit LokasiBanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, cxImage, cxVGrid,
  cxDBVGrid, cxInplaceContainer;

type
  TLokasiBanDropDownFm = class(TForm)
    LokasiAvaiBanQ: TSDQuery;
    LPBDs: TDataSource;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    LokasiAvaiBanQKode: TStringField;
    LokasiAvaiBanQKodeLayout: TStringField;
    LokasiAvaiBanQNama: TStringField;
    LokasiAvaiBanQGambar: TBlobField;
    cxDBVerticalGrid1Nama: TcxDBEditorRow;
    cxDBVerticalGrid1Gambar: TcxDBEditorRow;
    LayoutBanQ: TSDQuery;
    LayoutBanQKode: TStringField;
    LayoutBanQNama: TStringField;
    LayoutBanQGambar: TBlobField;
    LokasiAvaiBanQNamaLayout: TStringField;
    cxDBVerticalGrid1NamaLayout: TcxDBEditorRow;
    LokasiBanQ: TSDQuery;
    procedure cxDBVerticalGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  LokasiBanDropDownFm: TLokasiBanDropDownFm;
  armada,LokSQLOri:string;

implementation

{$R *.dfm}

constructor TLokasiBanDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  armada:=kd;
end;

constructor TLokasiBanDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TLokasiBanDropDownFm.cxDBVerticalGrid1DblClick(Sender: TObject);
begin
  kode:=LokasiAvaiBanQKode.AsString;
  LokasiAvaiBanQ.SQL.Text:=LokSQLOri;
  ModalResult:=mrOK;
end;

procedure TLokasiBanDropDownFm.FormShow(Sender: TObject);
begin
  LayoutBanQ.Open;
  ArmadaQ.Open;
  LokSQLOri:=LokasiAvaiBanQ.SQL.Text;
  if armada='' then
    begin
      LokasiAvaiBanQ.Close;
      LokasiAvaiBanQ.SQL.Text:='select * from masterlokasiban';
      LokasiAvaiBanQ.Open;
    end
  else
    begin
      LokasiAvaiBanQ.Close;
      LokasiAvaiBanQ.ParamByName('text').AsString:= armada;
      LokasiAvaiBanQ.Open;
      if LokasiAvaiBanQ.RecordCount<1 then
        begin
          LokasiAvaiBanQ.Close;
          LokasiAvaiBanQ.SQL.Text:='select mlokb.* from MasterLokasiBan mlokb, Armada ar where ar.Kode='+QuotedStr(armada)+' AND ar.LayoutBan=mlokb.KodeLayout';
          LokasiAvaiBanQ.Open;
        end
    end;
end;

end.
