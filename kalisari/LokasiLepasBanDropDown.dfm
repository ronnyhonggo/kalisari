object LokasiLepasBanDropDownFm: TLokasiLepasBanDropDownFm
  Left = 303
  Top = 155
  Width = 499
  Height = 289
  Caption = 'LokasiLepasBanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 483
    Height = 250
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LayoutStyle = lsMultiRecordView
    OptionsView.CellAutoHeight = True
    OptionsView.RowHeaderWidth = 107
    OptionsView.ValueWidth = 171
    OptionsBehavior.IncSearch = True
    OptionsData.Editing = False
    ParentFont = False
    TabOrder = 0
    OnDblClick = cxDBVerticalGrid1DblClick
    DataController.DataSource = LPBDs
    Version = 1
    object cxDBVerticalGrid1Nama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Nama'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Gambar: TcxDBEditorRow
      Height = 170
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Gambar'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1kodeidban: TcxDBEditorRow
      Properties.Caption = 'KodeIDBan'
      Properties.DataBinding.FieldName = 'kodeidban'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object LokasiAvaiBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select mlokb.*, dab.kodeban, b.kodeidban'
      
        'from masterlayoutban mlayb, armada ar, ban b, masterlokasiban ml' +
        'okb'
      
        'left outer join detailarmadaban dab on dab.kodelokasiban=mlokb.k' +
        'ode'
      
        'where mlokb.kodelayout=mlayb.kode AND dab.kodearmada=:text AND a' +
        'r.kode=dab.kodearmada AND ar.layoutban=mlayb.kode AND dab.kodelo' +
        'kasiban is not NULL AND dab.kodeban=b.kode')
    Left = 8
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object LokasiAvaiBanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LokasiAvaiBanQKodeLayout: TStringField
      FieldName = 'KodeLayout'
      Required = True
      Size = 10
    end
    object LokasiAvaiBanQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object LokasiAvaiBanQGambar: TBlobField
      FieldName = 'Gambar'
      Required = True
    end
    object LokasiAvaiBanQkodeidban: TStringField
      FieldName = 'kodeidban'
      Size = 50
    end
    object LokasiAvaiBanQkodeban: TStringField
      FieldName = 'kodeban'
      Size = 10
    end
  end
  object LPBDs: TDataSource
    DataSet = LokasiAvaiBanQ
    Left = 72
    Top = 96
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 40
    Top = 96
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object DetailArmadaBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailarmadaban where kodelokasiban=:text')
    Left = 112
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailArmadaBanQKodeBan: TStringField
      FieldName = 'KodeBan'
      Required = True
      Size = 10
    end
    object DetailArmadaBanQKodeArmada: TStringField
      FieldName = 'KodeArmada'
      Required = True
      Size = 10
    end
    object DetailArmadaBanQKodeLokasiBan: TStringField
      FieldName = 'KodeLokasiBan'
      Required = True
      Size = 10
    end
  end
end
