object LookupRekananFm: TLookupRekananFm
  Left = 256
  Top = 98
  BorderStyle = bsDialog
  Caption = 'Lookup Rekanan'
  ClientHeight = 392
  ClientWidth = 922
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 360
    Top = 16
    Width = 77
    Height = 16
    Caption = 's/d tgl hari ini'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 680
    Top = 16
    Width = 72
    Height = 16
    Caption = 's/d Bulan ini'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 922
    Height = 392
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DSMaster
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
      end
      object cxGrid1DBTableView1Nama: TcxGridDBColumn
        DataBinding.FieldName = 'Nama'
        Width = 178
      end
      object cxGrid1DBTableView1Alamat: TcxGridDBColumn
        DataBinding.FieldName = 'Alamat'
        Width = 205
      end
      object cxGrid1DBTableView1NoTelp: TcxGridDBColumn
        DataBinding.FieldName = 'NoTelp'
      end
      object cxGrid1DBTableView1NamaPIC: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPIC'
        Width = 168
      end
      object cxGrid1DBTableView1NoTelpPIC: TcxGridDBColumn
        DataBinding.FieldName = 'NoTelpPIC'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DSMaster: TDataSource
    DataSet = MasterQ
    Left = 48
    Top = 296
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from masterrekanan')
    Left = 16
    Top = 296
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object MasterQNoTelp: TStringField
      FieldName = 'NoTelp'
    end
    object MasterQNamaPIC: TStringField
      FieldName = 'NamaPIC'
      Size = 50
    end
    object MasterQNoTelpPIC: TStringField
      FieldName = 'NoTelpPIC'
    end
  end
end
