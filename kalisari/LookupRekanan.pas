unit LookupRekanan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxHyperLinkEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  SDEngine, cxButtonEdit, UCrpeClasses, UCrpe32, StdCtrls, ComCtrls,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  ExtCtrls, cxTextEdit, cxDropDownEdit;

type
  TLookupRekananFm = class(TForm)
    DSMaster: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    MasterQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQAlamat: TStringField;
    MasterQNoTelp: TStringField;
    MasterQNamaPIC: TStringField;
    MasterQNoTelpPIC: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Alamat: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelp: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPIC: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelpPIC: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:String;
  end;

var
  LookupRekananFm: TLookupRekananFm;

implementation
uses DateUtils, menuutama, StrUtils;

{$R *.dfm}

procedure TLookupRekananFm.FormShow(Sender: TObject);
begin
  MasterQ.Active:=true;
end;

procedure TLookupRekananFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=MasterQKode.AsString;
  ModalResult:=mrOK;
end;

end.
