object MasterArmadaFm: TMasterArmadaFm
  Left = 415
  Top = 0
  Width = 537
  Height = 788
  BorderIcons = [biSystemMenu]
  Caption = 'Master Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 521
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 680
    Width = 521
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 416
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 521
    Height = 632
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 183
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridPlatNo: TcxDBEditorRow
      Properties.Caption = 'PlatNo *'
      Properties.DataBinding.FieldName = 'PlatNo'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNoBody: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridJenisKendaraan: TcxDBEditorRow
      Properties.Caption = 'JenisKendaraan *'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridJenisKendaraanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'JenisKendaraan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridDetailJenis: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailJenis'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object MasterVGridDetailTipe: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailTipe'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object MasterVGridJumlahSeat: TcxDBEditorRow
      Properties.Caption = 'JumlahSeat *'
      Properties.DataBinding.FieldName = 'JumlahSeat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridAC: TcxDBEditorRow
      Properties.Caption = 'AC *'
      Properties.DataBinding.FieldName = 'AC'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridJenisAC: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridDBJenisACEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'JenisAC'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridMerkAC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'MerkAC'
      Properties.Options.Editing = False
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object MasterVGridTipeAC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TipeAC'
      Properties.Options.Editing = False
      ID = 9
      ParentID = 7
      Index = 1
      Version = 1
    end
    object MasterVGridToilet: TcxDBEditorRow
      Properties.Caption = 'Toilet *'
      Properties.DataBinding.FieldName = 'Toilet'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridAirSuspension: TcxDBEditorRow
      Properties.Caption = 'Air Suspension *'
      Properties.DataBinding.FieldName = 'AirSuspension'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridTahunPembuatan: TcxDBEditorRow
      Properties.Caption = 'TahunPembuatan *'
      Properties.DataBinding.FieldName = 'TahunPembuatan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridJenisBBM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JenisBBM'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridKapasitasTangkiBBM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KapasitasTangkiBBM'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridLevelArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LevelArmada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridLayoutBan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridLayoutBanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'LayoutBan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridNamaLayout: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaLayout'
      Properties.Options.Editing = False
      ID = 17
      ParentID = 16
      Index = 0
      Version = 1
    end
    object MasterVGridJumlahBan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JumlahBan'
      Properties.Options.Editing = False
      ID = 18
      ParentID = 16
      Index = 1
      Version = 1
    end
    object MasterVGridLokasi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Items = <
        item
          Caption = 'Surabaya'
          Value = 'Surabaya'
        end
        item
          Caption = 'Tuban'
          Value = 'Tuban'
        end
        item
          Caption = 'Kaltim'
          Value = 'Kaltim'
        end
        item
          Caption = 'Payton'
          Value = 'Payton'
        end
        item
          Caption = 'Gresik'
          Value = 'Gresik'
        end
        item
          Caption = 'Beji/Pasuruan'
          Value = 'Beji/Pasuruan'
        end
        item
          Caption = 'Lainnya'
          Value = 'Lainnya'
        end>
      Properties.DataBinding.FieldName = 'Lokasi'
      ID = 19
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridAktif: TcxDBEditorRow
      Properties.Caption = 'Aktif *'
      Properties.DataBinding.FieldName = 'Aktif'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 20
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridSopir: TcxDBEditorRow
      Properties.Caption = 'Pengemudi'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridSopirEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Sopir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 21
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridDetailPengemudi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailPengemudi'
      Properties.Options.Editing = False
      ID = 22
      ParentID = 21
      Index = 0
      Version = 1
    end
    object MasterVGridSTNKPajakExpired: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'STNKPajakExpired'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 23
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridKirSelesai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirSelesai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 24
      ParentID = -1
      Index = 17
      Version = 1
    end
    object MasterVGridKmSekarang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KmSekarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 25
      ParentID = -1
      Index = 18
      Version = 1
    end
    object MasterVGridNoRangka: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoRangka'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 26
      ParentID = -1
      Index = 19
      Version = 1
    end
    object MasterVGridNoMesin: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoMesin'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 27
      ParentID = -1
      Index = 20
      Version = 1
    end
    object MasterVGridRekanan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnChange = MasterVGridRekananEditPropertiesChange
      Properties.DataBinding.FieldName = 'Rekanan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 28
      ParentID = -1
      Index = 21
      Version = 1
    end
    object MasterVGridKodeRekanan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKodeRekananEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'KodeRekanan'
      ID = 29
      ParentID = 28
      Index = 0
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 33
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 30
      ParentID = -1
      Index = 22
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 731
    Width = 521
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from armada')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object MasterQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object MasterQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object MasterQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object MasterQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object MasterQMerkAC: TStringField
      FieldKind = fkLookup
      FieldName = 'MerkAC'
      LookupDataSet = ACQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Merk'
      KeyFields = 'JenisAC'
      Lookup = True
    end
    object MasterQTipeAC: TStringField
      FieldKind = fkLookup
      FieldName = 'TipeAC'
      LookupDataSet = ACQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tipe'
      KeyFields = 'JenisAC'
      Size = 10
      Lookup = True
    end
    object MasterQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object MasterQAirSuspension: TBooleanField
      DisplayLabel = 'ASus'
      FieldName = 'AirSuspension'
      Required = True
    end
    object MasterQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object MasterQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object MasterQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object MasterQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object MasterQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object MasterQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object MasterQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object MasterQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object MasterQDetailJenis: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailJenis'
      LookupDataSet = JenisKendaraanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaJenis'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQDetailPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPengemudi'
      LookupDataSet = SopirQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Lookup = True
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object MasterQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object MasterQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object MasterQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object MasterQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object MasterQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object MasterQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object MasterQNamaLayout: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaLayout'
      LookupDataSet = LayoutBanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'LayoutBan'
      Size = 50
      Lookup = True
    end
    object MasterQDetailTipe: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailTipe'
      LookupDataSet = JenisKendaraanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tipe'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
    object MasterQRekanan: TBooleanField
      FieldName = 'Rekanan'
    end
    object MasterQKodeRekanan: TStringField
      FieldName = 'KodeRekanan'
      Size = 10
    end
    object MasterQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, ' +
        'JumlahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspen' +
        'sion, Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, L' +
        'evelArmada, LayoutBan, JumlahBan, Keterangan, Aktif, AC, KmSekar' +
        'ang, CreateDate, CreateBy, Operator, TglEntry, STNKPerpanjangExp' +
        'ired, KirMulai, Rekanan, KodeRekanan, Lokasi'
      'from armada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update armada'
      'set'
      '  Kode = :Kode,'
      '  PlatNo = :PlatNo,'
      '  NoBody = :NoBody,'
      '  NoRangka = :NoRangka,'
      '  NoMesin = :NoMesin,'
      '  JenisKendaraan = :JenisKendaraan,'
      '  JumlahSeat = :JumlahSeat,'
      '  JenisBBM = :JenisBBM,'
      '  TahunPembuatan = :TahunPembuatan,'
      '  JenisAC = :JenisAC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Sopir = :Sopir,'
      '  KapasitasTangkiBBM = :KapasitasTangkiBBM,'
      '  STNKPajakExpired = :STNKPajakExpired,'
      '  KirSelesai = :KirSelesai,'
      '  LevelArmada = :LevelArmada,'
      '  LayoutBan = :LayoutBan,'
      '  JumlahBan = :JumlahBan,'
      '  Keterangan = :Keterangan,'
      '  Aktif = :Aktif,'
      '  AC = :AC,'
      '  KmSekarang = :KmSekarang,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  STNKPerpanjangExpired = :STNKPerpanjangExpired,'
      '  KirMulai = :KirMulai,'
      '  Rekanan = :Rekanan,'
      '  KodeRekanan = :KodeRekanan,'
      '  Lokasi = :Lokasi'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into armada'
      
        '  (Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, Juml' +
        'ahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspension' +
        ', Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, Level' +
        'Armada, LayoutBan, JumlahBan, Keterangan, Aktif, AC, KmSekarang,' +
        ' CreateDate, CreateBy, Operator, TglEntry, STNKPerpanjangExpired' +
        ', KirMulai, Rekanan, KodeRekanan, Lokasi)'
      'values'
      
        '  (:Kode, :PlatNo, :NoBody, :NoRangka, :NoMesin, :JenisKendaraan' +
        ', :JumlahSeat, :JenisBBM, :TahunPembuatan, :JenisAC, :Toilet, :A' +
        'irSuspension, :Sopir, :KapasitasTangkiBBM, :STNKPajakExpired, :K' +
        'irSelesai, :LevelArmada, :LayoutBan, :JumlahBan, :Keterangan, :A' +
        'ktif, :AC, :KmSekarang, :CreateDate, :CreateBy, :Operator, :TglE' +
        'ntry, :STNKPerpanjangExpired, :KirMulai, :Rekanan, :KodeRekanan,' +
        ' :Lokasi)')
    DeleteSQL.Strings = (
      'delete from armada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from armada order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai where UPPER (jabatan) = '#39'PENGEMUDI'#39)
    Left = 409
    Top = 199
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object SopirQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
  end
  object ACQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterac'
      '')
    Left = 449
    Top = 199
    object ACQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ACQMerk: TStringField
      FieldName = 'Merk'
      Size = 50
    end
    object ACQTipe: TStringField
      FieldName = 'Tipe'
      Size = 50
    end
  end
  object JenisKendaraanQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 368
    Top = 200
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object LayoutBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from masterlayoutban')
    Left = 360
    Top = 240
    object LayoutBanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object LayoutBanQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object LayoutBanQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object LayoutBanQGambar: TBlobField
      FieldName = 'Gambar'
    end
  end
  object UbahQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 256
    Top = 352
  end
end
