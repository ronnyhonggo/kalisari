unit MasterArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxClasses, cxMRUEdit, cxLabel,
  cxCheckBox, cxImage, cxMemo;

type
  TMasterArmadaFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    SopirQ: TSDQuery;
    ACQ: TSDQuery;
    JenisKendaraanQ: TSDQuery;
    cxLabel1: TcxLabel;
    MasterQKode: TStringField;
    MasterQPlatNo: TStringField;
    MasterQJumlahSeat: TIntegerField;
    MasterQTahunPembuatan: TStringField;
    MasterQNoBody: TStringField;
    MasterQJenisAC: TStringField;
    MasterQJenisBBM: TStringField;
    MasterQKapasitasTangkiBBM: TIntegerField;
    MasterQLevelArmada: TStringField;
    MasterQJumlahBan: TIntegerField;
    MasterQAktif: TBooleanField;
    MasterQAC: TBooleanField;
    MasterQToilet: TBooleanField;
    MasterQAirSuspension: TBooleanField;
    MasterQKmSekarang: TIntegerField;
    MasterQKeterangan: TStringField;
    MasterQSopir: TStringField;
    MasterQJenisKendaraan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQSTNKPajakExpired: TDateTimeField;
    MasterQSTNKPerpanjangExpired: TDateTimeField;
    MasterQKirMulai: TDateTimeField;
    MasterQKirSelesai: TDateTimeField;
    MasterQNoRangka: TStringField;
    MasterQNoMesin: TStringField;
    SopirQKode: TStringField;
    SopirQNama: TStringField;
    SopirQJabatan: TStringField;
    MasterQDetailPengemudi: TStringField;
    JenisKendaraanQKode: TStringField;
    JenisKendaraanQNamaJenis: TStringField;
    JenisKendaraanQTipe: TMemoField;
    JenisKendaraanQKategori: TStringField;
    JenisKendaraanQKeterangan: TMemoField;
    ACQKode: TStringField;
    ACQMerk: TStringField;
    ACQTipe: TStringField;
    MasterQMerkAC: TStringField;
    MasterQTipeAC: TStringField;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridNoBody: TcxDBEditorRow;
    MasterVGridJumlahSeat: TcxDBEditorRow;
    MasterVGridAC: TcxDBEditorRow;
    MasterVGridJenisAC: TcxDBEditorRow;
    MasterVGridMerkAC: TcxDBEditorRow;
    MasterVGridTipeAC: TcxDBEditorRow;
    MasterVGridToilet: TcxDBEditorRow;
    MasterVGridAirSuspension: TcxDBEditorRow;
    MasterVGridTahunPembuatan: TcxDBEditorRow;
    MasterVGridJenisBBM: TcxDBEditorRow;
    MasterVGridKapasitasTangkiBBM: TcxDBEditorRow;
    MasterVGridLevelArmada: TcxDBEditorRow;
    MasterVGridJumlahBan: TcxDBEditorRow;
    MasterVGridAktif: TcxDBEditorRow;
    MasterVGridJenisKendaraan: TcxDBEditorRow;
    MasterVGridDetailJenis: TcxDBEditorRow;
    MasterVGridSopir: TcxDBEditorRow;
    MasterVGridDetailPengemudi: TcxDBEditorRow;
    MasterVGridSTNKPajakExpired: TcxDBEditorRow;
    MasterVGridKirSelesai: TcxDBEditorRow;
    MasterVGridKmSekarang: TcxDBEditorRow;
    MasterVGridNoRangka: TcxDBEditorRow;
    MasterVGridNoMesin: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterQDetailJenis: TStringField;
    MasterQLayoutBan: TStringField;
    MasterVGridLayoutBan: TcxDBEditorRow;
    LayoutBanQ: TSDQuery;
    LayoutBanQKode: TStringField;
    LayoutBanQNama: TStringField;
    LayoutBanQJumlahBan: TIntegerField;
    LayoutBanQGambar: TBlobField;
    MasterQNamaLayout: TStringField;
    MasterVGridNamaLayout: TcxDBEditorRow;
    MasterQDetailTipe: TStringField;
    MasterVGridDetailTipe: TcxDBEditorRow;
    MasterQRekanan: TBooleanField;
    MasterVGridRekanan: TcxDBEditorRow;
    MasterQKodeRekanan: TStringField;
    MasterVGridKodeRekanan: TcxDBEditorRow;
    UbahQ: TSDQuery;
    MasterQLokasi: TStringField;
    MasterVGridLokasi: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridSopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSopirEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridJenisKendaraanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDBJenisACEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridLayoutBanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKodeRekananEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridRekananEditPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  MasterArmadaFm: TMasterArmadaFm;
  paramkode:string;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, ArmadaDropDown, LayoutBanDropDown,
  PegawaiDropDown, ACDropDown, LookupRekanan;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterArmadaFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TMasterArmadaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterArmadaFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterArmadaFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
    ACQ.Open;
  LayoutBanQ.Open;
  JenisKendaraanQ.Open;
  SopirQ.Open;
end;

procedure TMasterArmadaFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterArmadaFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterArmadaFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterArmada.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterArmada.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterArmada.AsBoolean;

end;

procedure TMasterArmadaFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQAktif.AsBoolean :=false;
      MasterQAC.AsBoolean:=false;
      MasterQToilet.AsBoolean:=false;
      MasterQAirSuspension.AsBoolean:=false;
      savebtn.Enabled:=menuutamafm.UserQInsertMasterArmada.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TMasterArmadaFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterArmadaFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Armada dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        showmessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterArmadaFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;}
end;

procedure TMasterArmadaFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterArmadaFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Armada '+KodeEdit.Text+' - '+MasterQPlatNo.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       UbahQ.Close;
       UbahQ.SQL.Text:='update AmbilBarangKanibal set KeArmada="0000000000" where KeArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update DetailArmadaBan set KodeArmada="0000000000" where KodeArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update KeluhanPelanggan set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update KlaimPengemudi set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update LaporanPerawatan set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update LepasBan set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update MasterSO set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update PasangBan set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update PermintaanPerbaikan set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update PORebuild set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update RealisasiAnjem set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update RealisasiTrayek set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update Rebuild set DariArmada="0000000000" where DariArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update Rebuild set KeArmada="0000000000" where KeArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update Storing set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update TukarKomponen set DariArmada="0000000000" where DariArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update TukarKomponen set KeArmada="0000000000" where KeArmada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       UbahQ.Close;
       UbahQ.SQL.Text:='update BBMNonSO set Armada="0000000000" where Armada='+QuotedStr(MasterQKode.AsString);
       UbahQ.ExecSQL;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Armada telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        MessageDlg('Armada pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
       end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterArmadaFm.SearchBtnClick(Sender: TObject);
begin
    //MasterQ.SQL.Text:=MasterOriSQL;
    ArmadaDropDownFm:=TArmadaDropdownfm.Create(Self);
    if ArmadaDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=ArmadaDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    ArmadaDropDownFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridSopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSopir.AsString:=PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    {SopirQ.Close;
    //SopirQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SopirQ.ExecSQL;
    SopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSopir.AsString:=SopirQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;}
end;

procedure TMasterArmadaFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TMasterArmadaFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridPlatNo);
end;

procedure TMasterArmadaFm.MasterVGridJenisKendaraanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    JenisKendaraanQ.Close;
    //JenisKendaraanQ.ParamByName('text').AsString:='';
    JenisKendaraanQ.ExecSQL;
    JenisKendaraanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,JenisKendaraanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQJenisKendaraan.AsString:=JenisKendaraanQKode.AsString;
      MasterQDetailJenis.AsString:=JenisKendaraanQNamaJenis.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridDBJenisACEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ACDropDownFm:=TACDropdownfm.Create(Self);
  if ACDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQJenisAC.AsString:=ACDropDownFm.kode;
  end;
  ACDropDownFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridLayoutBanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LayoutBanDropDownFm:=TLayoutBanDropDownFm.Create(self);
  if LayoutBanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQLayoutBan.AsString:= LayoutBanDropDownFm.kode;
    MasterQJumlahBan.AsString:= LayoutBanDropDownFm.jumlahban;
  end;
  LayoutBanDropDownFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridKodeRekananEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LookupRekananFm:=TLookupRekananFm.Create(self);
  if LookupRekananFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKodeRekanan.AsString:= LookupRekananFm.kode;
  end;
  LookupRekananFm.Release;
end;

procedure TMasterArmadaFm.MasterVGridRekananEditPropertiesChange(
  Sender: TObject);
begin
  if MasterQRekanan.AsBoolean=True then
    MasterVGridKodeRekanan.Visible:=True
  else
    MasterVGridKodeRekanan.Visible:=False;
end;

end.
