object MasterAssemblyFm: TMasterAssemblyFm
  Left = 419
  Top = 170
  Width = 670
  Height = 497
  BorderIcons = [biSystemMenu]
  Caption = 'Assembly'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 654
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 391
    Width = 654
    Height = 49
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 252
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 171
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 356
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 3
      Height = 47
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
    object ApprBtn: TcxButton
      Left = 90
      Top = 10
      Width = 75
      Height = 25
      Caption = 'APPROVE'
      TabOrder = 4
      OnClick = ApprBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 440
    Width = 654
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 654
    Height = 153
    Align = alTop
    TabOrder = 3
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 312
      Height = 151
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 133
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      OnExit = MasterVGridExit
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridTanggal: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tanggal'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridPeminta: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridPemintaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Peminta'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridNamaPeminta: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPeminta'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 1
        Index = 0
        Version = 1
      end
      object MasterVGridPenyetuju: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridPenyetujuEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Penyetuju'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridNamaPenyetuju: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenyetuju'
        Properties.Options.Editing = False
        ID = 4
        ParentID = 3
        Index = 0
        Version = 1
      end
      object MasterVGridBarangJadi: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridBarangJadiEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'BarangJadi'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 5
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridNamaBarang: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaBarang'
        Properties.Options.Editing = False
        ID = 6
        ParentID = 5
        Index = 0
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 313
      Top = 1
      Width = 340
      Height = 151
      Align = alClient
      TabOrder = 1
      OnExit = cxGrid1Exit
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsData.Appending = True
        OptionsData.DeletingConfirmation = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1KodeBarangBahan: TcxGridDBColumn
          DataBinding.FieldName = 'KodeBarangBahan'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KodeBarangBahanPropertiesButtonClick
          Width = 96
        end
        object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Styles.Content = MenuUtamaFm.cxStyle5
          Width = 156
        end
        object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
          DataBinding.FieldName = 'Jumlah'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
        Options.DetailTabsPosition = dtpLeft
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 201
    Width = 654
    Height = 190
    Align = alClient
    TabOrder = 4
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 652
      Height = 20
      Align = alTop
      Caption = 'History Assembly :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 21
      Width = 652
      Height = 168
      Align = alClient
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Tanggal: TcxGridDBColumn
          DataBinding.FieldName = 'Tanggal'
          Width = 93
        end
        object cxGrid2DBTableView1NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Width = 166
        end
        object cxGrid2DBTableView1NamaPeminta: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPeminta'
          Width = 130
        end
        object cxGrid2DBTableView1NamaPenyetuju: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPenyetuju'
          Width = 136
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 100
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterassembly')
    UpdateObject = MasterUS
    Left = 361
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
      Required = True
    end
    object MasterQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object MasterQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object MasterQBarangJadi: TStringField
      FieldName = 'BarangJadi'
      Required = True
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object MasterQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangJadi'
      Size = 50
      Lookup = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, Peminta, Penyetuju, BarangJadi, CreateDate' +
        ', CreateBy, Status'
      'from masterassembly'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update masterassembly'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  Peminta = :Peminta,'
      '  Penyetuju = :Penyetuju,'
      '  BarangJadi = :BarangJadi,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into masterassembly'
      
        '  (Kode, Tanggal, Peminta, Penyetuju, BarangJadi, CreateDate, Cr' +
        'eateBy, Status)'
      'values'
      
        '  (:Kode, :Tanggal, :Peminta, :Penyetuju, :BarangJadi, :CreateDa' +
        'te, :CreateBy, :Status)')
    DeleteSQL.Strings = (
      'delete from masterassembly'
      'where'
      '  Kode = :OLD_Kode')
    Left = 444
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from masterassembly order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailAssemblyQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from detailassembly where kodeass=:text')
    UpdateObject = SDUpdateSQL1
    Left = 601
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailAssemblyQKodeAss: TStringField
      FieldName = 'KodeAss'
      Required = True
      Size = 10
    end
    object DetailAssemblyQKodeBarangBahan: TStringField
      FieldName = 'KodeBarangBahan'
      Required = True
      Size = 10
    end
    object DetailAssemblyQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object DetailAssemblyQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarangBahan'
      Size = 50
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailAssemblyQ
    Left = 484
    Top = 6
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 561
    Top = 9
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from detailstandard order by kode desc')
    Left = 321
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeAss, KodeBarangBahan, Jumlah'
      'from detailassembly'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    ModifySQL.Strings = (
      'update detailassembly'
      'set'
      '  KodeAss = :KodeAss,'
      '  KodeBarangBahan = :KodeBarangBahan,'
      '  Jumlah = :Jumlah'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    InsertSQL.Strings = (
      'insert into detailassembly'
      '  (KodeAss, KodeBarangBahan, Jumlah)'
      'values'
      '  (:KodeAss, :KodeBarangBahan, :Jumlah)')
    DeleteSQL.Strings = (
      'delete from detailassembly'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    Left = 524
    Top = 10
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 257
    Top = 9
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object DeleteDetailAssemblyQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailassembly where kodeass=:text')
    Left = 528
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailAssemblyQKodeAss: TStringField
      FieldName = 'KodeAss'
      Required = True
      Size = 10
    end
    object DeleteDetailAssemblyQKodeBarangBahan: TStringField
      FieldName = 'KodeBarangBahan'
      Required = True
      Size = 10
    end
    object DeleteDetailAssemblyQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
  object ExecuteDeleteDetailAssemblyQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailassembly where kodeass=:text')
    UpdateObject = UpdateDeleteDetailStandardUS
    Left = 592
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object UpdateDeleteDetailStandardUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeAss, KodeBarangBahan, Jumlah'
      'from detailassembly'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    ModifySQL.Strings = (
      'update detailassembly'
      'set'
      '  KodeAss = :KodeAss,'
      '  KodeBarangBahan = :KodeBarangBahan,'
      '  Jumlah = :Jumlah'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    InsertSQL.Strings = (
      'insert into detailassembly'
      '  (KodeAss, KodeBarangBahan, Jumlah)'
      'values'
      '  (:KodeAss, :KodeBarangBahan, :Jumlah)')
    DeleteSQL.Strings = (
      'delete from detailassembly'
      'where'
      '  KodeAss = :OLD_KodeAss and'
      '  KodeBarangBahan = :OLD_KodeBarangBahan and'
      '  Jumlah = :OLD_Jumlah')
    Left = 560
    Top = 184
  end
  object ViewAssemblyQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterassembly ma'
      
        'where (ma.Tanggal>=:text1 and ma.Tanggal<=:text2) or ma.tanggal ' +
        'is null'
      'order by tanggal desc')
    Left = 328
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewAssemblyQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewAssemblyQTanggal: TDateTimeField
      FieldName = 'Tanggal'
      Required = True
    end
    object ViewAssemblyQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object ViewAssemblyQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object ViewAssemblyQBarangJadi: TStringField
      FieldName = 'BarangJadi'
      Required = True
      Size = 10
    end
    object ViewAssemblyQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangJadi'
      Size = 50
      Lookup = True
    end
    object ViewAssemblyQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object ViewAssemblyQNamaPenyetuju: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenyetuju'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penyetuju'
      Size = 50
      Lookup = True
    end
    object ViewAssemblyQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewAssemblyQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewAssemblyQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewAssemblyQ
    Left = 360
    Top = 432
  end
end
