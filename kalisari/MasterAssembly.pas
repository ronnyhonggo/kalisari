unit MasterAssembly;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel, cxCheckBox, cxDropDownEdit,
  cxGroupBox;

type                                                
  TMasterAssemblyFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    DetailAssemblyQ: TSDQuery;
    DataSource1: TDataSource;
    BarangQ: TSDQuery;
    Kode2Q: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    Kode2Qkode: TStringField;
    PegawaiQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    DeleteDetailAssemblyQ: TSDQuery;
    ExecuteDeleteDetailAssemblyQ: TSDQuery;
    UpdateDeleteDetailStandardUS: TSDUpdateSQL;
    BarangQJumlah: TFloatField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQPeminta: TStringField;
    MasterQPenyetuju: TStringField;
    MasterQBarangJadi: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPeminta: TStringField;
    MasterQNamaPenyetuju: TStringField;
    MasterQNamaBarang: TStringField;
    DetailAssemblyQKodeAss: TStringField;
    DetailAssemblyQKodeBarangBahan: TStringField;
    DetailAssemblyQJumlah: TFloatField;
    DetailAssemblyQNamaBarang: TStringField;
    Panel1: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridPeminta: TcxDBEditorRow;
    MasterVGridNamaPeminta: TcxDBEditorRow;
    MasterVGridPenyetuju: TcxDBEditorRow;
    MasterVGridNamaPenyetuju: TcxDBEditorRow;
    MasterVGridBarangJadi: TcxDBEditorRow;
    MasterVGridNamaBarang: TcxDBEditorRow;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1KodeBarangBahan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel2: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    Label1: TLabel;
    ViewAssemblyQ: TSDQuery;
    ViewDs: TDataSource;
    ViewAssemblyQKode: TStringField;
    ViewAssemblyQTanggal: TDateTimeField;
    ViewAssemblyQPeminta: TStringField;
    ViewAssemblyQPenyetuju: TStringField;
    ViewAssemblyQBarangJadi: TStringField;
    ViewAssemblyQNamaBarang: TStringField;
    ViewAssemblyQNamaPeminta: TStringField;
    ViewAssemblyQNamaPenyetuju: TStringField;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid2DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid2DBTableView1NamaPeminta: TcxGridDBColumn;
    cxGrid2DBTableView1NamaPenyetuju: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    DeleteDetailAssemblyQKodeAss: TStringField;
    DeleteDetailAssemblyQKodeBarangBahan: TStringField;
    DeleteDetailAssemblyQJumlah: TFloatField;
    ApprBtn: TcxButton;
    MasterQStatus: TStringField;
    ViewAssemblyQCreateDate: TDateTimeField;
    ViewAssemblyQCreateBy: TStringField;
    ViewAssemblyQStatus: TStringField;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1Exit(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1KodeBarangBahanPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPemintaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPenyetujuEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridBarangJadiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ApprBtnClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterAssemblyFm: TMasterAssemblyFm;

  MasterOriSQL, DetailOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, StandarPerawatanDropDown, JenisKendaraanDropDown,
  BarangDropDown, PegawaiDropDown, BarangBekasDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterAssemblyFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterAssemblyFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterAssemblyFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterAssemblyFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailAssemblyQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  BarangQ.Open;
  PegawaiQ.Open;
  ViewAssemblyQ.Open; 
end;

procedure TMasterAssemblyFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterAssemblyFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterAssemblyFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-3;
  ViewAssemblyQ.Close;
  ViewAssemblyQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewAssemblyQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewAssemblyQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertAssemb.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateAssemb.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteAssemb.AsBoolean;
end;

procedure TMasterAssemblyFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:='ON PROCESS';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteAssemb.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateAssemb.AsBoolean;
    MasterVGrid.Enabled:=True;
{    DetailQ.Close;
    DetailQ.ParamByName('text').AsString:='';
    detailq.Open;   }
  end;

end;

procedure TMasterAssemblyFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterAssemblyFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailAssemblyQ.Open;
    DetailAssemblyQ.Edit;
    DetailAssemblyQ.First;
    while DetailAssemblyQ.Eof=FALSE do
    begin
        DetailAssemblyQ.Edit;
        DetailAssemblyQKodeAss.AsString:=KodeEdit.Text;
        DetailAssemblyQ.Post;
        DetailAssemblyQ.Next;
    end;
    //DetailQ.ApplyUpdates;
    DetailAssemblyQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
   // DetailQ.CommitUpdates;
    ShowMessage('Assembly dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    //KodeEdit.SetFocus;
   FormShow(self);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailAssemblyQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  DetailAssemblyQ.Close;
  DetailAssemblyQ.ParamByName('text').AsString:='';
  DetailAssemblyQ.Open;
  DetailAssemblyQ.Close;
  ViewAssemblyQ.Close;
  ViewAssemblyQ.Open;
end;

procedure TMasterAssemblyFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Standard '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteDetailAssemblyQ.Close;
       DeleteDetailAssemblyQ.ParamByName('text').AsString:=KodeEdit.Text;
       DeleteDetailAssemblyQ.Open;
       DeleteDetailAssemblyQ.First;
       while DeleteDetailAssemblyQ.Eof=FALSE
       do
       begin
        ExecuteDeleteDetailAssemblyQ.SQL.Text:=('delete from DetailAssembly where KodeAss='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteDetailAssemblyQ.ExecSQL;
        DeleteDetailAssemblyQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Assembly telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DeleteDetailAssemblyQ.RollbackUpdates;
      MessageDlg('Assembly pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
  DetailAssemblyQ.Close;
  DetailAssemblyQ.ParamByName('text').AsString:='';
  DetailAssemblyQ.Open;
  ViewAssemblyQ.Close;
  ViewAssemblyQ.Open;
end;

procedure TMasterAssemblyFm.SearchBtnClick(Sender: TObject);
begin
{    //MasterQ.SQL.Text:=MasterOriSQL;
    StandarPerawatanDropDownFm:=TStandarPerawatanDropdownfm.Create(Self,'all');
    if StandarPerawatanDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=StandarPerawatanDropDownFm.kode;
      KodeEditExit(Sender);
      detailq.Close;
      detailq.ParamByName('text').AsString := MasterQKode.AsString;
      detailq.ExecSQL;
      detailq.Open;
      MasterVGrid.SetFocus;
    end;
    StandarPerawatanDropDownFm.Release;     }
end;

procedure TMasterAssemblyFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TMasterAssemblyFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  //ShowMessage(inttostr(abuttonindex));
  if abuttonindex=10 then
  begin
  DetailAssemblyQ.Edit;
  DetailAssemblyQ.Post;
  end;
end;

procedure TMasterAssemblyFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.Navigator.Buttons.Post.Click;
end;

procedure TMasterAssemblyFm.ExitBtnClick(Sender: TObject);
begin
  release;
end;

procedure TMasterAssemblyFm.cxGrid1DBTableView1KodeBarangBahanPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  DetailAssemblyQ.Open;
  DetailAssemblyQ.Insert;
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    DetailAssemblyQKodeBarangBahan.AsString:= BarangDropDownFm.kode;
  end;
  DetailAssemblyQKodeAss.AsString:='0';
  BarangDropDownFm.Release;
end;

procedure TMasterAssemblyFm.MasterVGridPemintaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPeminta.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TMasterAssemblyFm.MasterVGridPenyetujuEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPenyetuju.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TMasterAssemblyFm.MasterVGridBarangJadiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarangJadi.AsString:= BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

procedure TMasterAssemblyFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kodeedit.Text:=ViewAssemblyQKode.AsString;
  KodeEditExit(self);
  DetailAssemblyQ.Close;
  DetailAssemblyQ.ParamByName('text').AsString:=MasterQKode.AsString;
  DetailAssemblyQ.Open;
end;

procedure TMasterAssemblyFm.ApprBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MasterQ.Edit;
  MasterQStatus.AsString:='APPROVED';
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailAssemblyQ.Open;
    DetailAssemblyQ.Edit;
    DetailAssemblyQ.First;
    while DetailAssemblyQ.Eof=FALSE do
    begin
        DetailAssemblyQ.Edit;
        DetailAssemblyQKodeAss.AsString:=KodeEdit.Text;
        DetailAssemblyQ.Post;
        DetailAssemblyQ.Next;
    end;
    DetailAssemblyQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Assembly dengan Kode ' + KodeEdit.Text + ' telah disimpan');
   FormShow(self);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailAssemblyQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  DetailAssemblyQ.Close;
  DetailAssemblyQ.ParamByName('text').AsString:='';
  DetailAssemblyQ.Open;
  DetailAssemblyQ.Close;
  ViewAssemblyQ.Close;
  ViewAssemblyQ.Open;
end;

procedure TMasterAssemblyFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewAssemblyQ.Close;
  ViewAssemblyQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewAssemblyQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewAssemblyQ.Open;
end;

procedure TMasterAssemblyFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewAssemblyQ.Close;
  ViewAssemblyQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewAssemblyQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewAssemblyQ.Open;
end;

end.
