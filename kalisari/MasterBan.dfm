object MasterBanFm: TMasterBanFm
  Left = 447
  Top = 142
  Width = 504
  Height = 398
  BorderIcons = [biSystemMenu]
  Caption = 'Master Ban'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 488
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 307
    Width = 488
    Height = 34
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 4
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 4
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 412
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 488
    Height = 259
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 131
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridKodeBarang: TcxDBEditorRow
      Properties.Caption = 'KodeBarang *'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKodeBarangEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'KodeBarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailBarang'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridKodeIDBan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KodeIDBan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridMerk: TcxDBEditorRow
      Properties.Caption = 'Merk *'
      Properties.DataBinding.FieldName = 'Merk'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridJenis: TcxDBEditorRow
      Properties.Caption = 'Jenis *'
      Properties.DataBinding.FieldName = 'Jenis'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridUkuranBan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UkuranBan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridStandardUmur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandardUmur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridStandardKm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandardKm'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridVulkanisir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Vulkanisir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridTanggalVulkanisir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalVulkanisir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KedalamanAlur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.Caption = 'Status *'
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 4
      Properties.EditProperties.Items = <
        item
          Caption = 'Baru'
          Value = 'BARU'
        end
        item
          Caption = 'Aktif'
          Value = 'AKTIF'
        end
        item
          Caption = 'Non-Aktif'
          Value = 'NON-AKTIF'
        end
        item
          Caption = 'Mati'
          Value = 'MATI'
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 10
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 341
    Width = 488
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from ban')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object MasterQDetailBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 50
      Lookup = True
    end
    object MasterQKodeIDBan: TStringField
      FieldName = 'KodeIDBan'
      Size = 50
    end
    object MasterQMerk: TStringField
      FieldName = 'Merk'
      Required = True
      Size = 50
    end
    object MasterQJenis: TStringField
      FieldName = 'Jenis'
      Required = True
      Size = 50
    end
    object MasterQUkuranBan: TStringField
      FieldName = 'UkuranBan'
      Size = 50
    end
    object MasterQRing: TStringField
      FieldName = 'Ring'
      Size = 50
    end
    object MasterQVelg: TStringField
      FieldName = 'Velg'
      Size = 50
    end
    object MasterQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object MasterQStandardKm: TIntegerField
      FieldName = 'StandardKm'
    end
    object MasterQVulkanisir: TBooleanField
      FieldName = 'Vulkanisir'
    end
    object MasterQTanggalVulkanisir: TDateTimeField
      FieldName = 'TanggalVulkanisir'
    end
    object MasterQKedalamanAlur: TIntegerField
      FieldName = 'KedalamanAlur'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, KodeBarang, KodeIDBan, Merk, Jenis, UkuranBan, Ring' +
        ', Velg, StandardUmur, StandardKm, Vulkanisir, TanggalVulkanisir,' +
        ' KedalamanAlur, Status, CreateDate, CreateBy, Operator, TglEntry' +
        #13#10'from ban'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update ban'
      'set'
      '  Kode = :Kode,'
      '  KodeBarang = :KodeBarang,'
      '  KodeIDBan = :KodeIDBan,'
      '  Merk = :Merk,'
      '  Jenis = :Jenis,'
      '  UkuranBan = :UkuranBan,'
      '  Ring = :Ring,'
      '  Velg = :Velg,'
      '  StandardUmur = :StandardUmur,'
      '  StandardKm = :StandardKm,'
      '  Vulkanisir = :Vulkanisir,'
      '  TanggalVulkanisir = :TanggalVulkanisir,'
      '  KedalamanAlur = :KedalamanAlur,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into ban'
      
        '  (Kode, KodeBarang, KodeIDBan, Merk, Jenis, UkuranBan, Ring, Ve' +
        'lg, StandardUmur, StandardKm, Vulkanisir, TanggalVulkanisir, Ked' +
        'alamanAlur, Status, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :KodeBarang, :KodeIDBan, :Merk, :Jenis, :UkuranBan, :R' +
        'ing, :Velg, :StandardUmur, :StandardKm, :Vulkanisir, :TanggalVul' +
        'kanisir, :KedalamanAlur, :Status, :CreateDate, :CreateBy, :Opera' +
        'tor, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from ban'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from ban order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from barang')
    Left = 249
    Top = 9
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
end
