unit MasterBan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, cxLabel, cxCheckBox;

type
  TMasterBanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    BarangQ: TSDQuery;
    cxLabel1: TcxLabel;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQSingleSupplier: TBooleanField;
    MasterVGridKodeBarang: TcxDBEditorRow;
    MasterVGridKodeIDBan: TcxDBEditorRow;
    MasterVGridMerk: TcxDBEditorRow;
    MasterVGridJenis: TcxDBEditorRow;
    MasterVGridUkuranBan: TcxDBEditorRow;
    MasterVGridStandardUmur: TcxDBEditorRow;
    MasterVGridStandardKm: TcxDBEditorRow;
    MasterVGridVulkanisir: TcxDBEditorRow;
    MasterVGridTanggalVulkanisir: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterQDetailBarang: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterQKode: TStringField;
    MasterQKodeBarang: TStringField;
    MasterQKodeIDBan: TStringField;
    MasterQMerk: TStringField;
    MasterQJenis: TStringField;
    MasterQUkuranBan: TStringField;
    MasterQRing: TStringField;
    MasterQVelg: TStringField;
    MasterQStandardUmur: TIntegerField;
    MasterQStandardKm: TIntegerField;
    MasterQVulkanisir: TBooleanField;
    MasterQTanggalVulkanisir: TDateTimeField;
    MasterQKedalamanAlur: TIntegerField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    BarangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridKodeBarangEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterBanFm: TMasterBanFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, BanDropDown, BarangDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterBanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterBanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterBanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterBanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterBanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterBanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
//  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  MasterQVulkanisir.AsBoolean:=False;
  MasterQStatus.Value:='BARU';
end;

procedure TMasterBanFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterBanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterBan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBan.AsBoolean;
end;

procedure TMasterBanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TMasterBanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterBanFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Ban dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      showMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterBanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  MasterQCreateBy.AsString:=User;
end;

procedure TMasterBanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  MasterQOperator.AsString:=User;
end;

procedure TMasterBanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Ban '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Ban telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        MessageDlg('Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
       end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterBanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    BanDropDownFm:=TBanDropdownfm.Create(Self);
    if BanDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=BanDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    BanDropDownFm.Release;
end;

procedure TMasterBanFm.MasterVGridExit(Sender: TObject);
begin
  SaveBtn.SetFocus;
end;

procedure TMasterBanFm.MasterVGridEnter(Sender: TObject);
begin
 // mastervgrid.FocusRow(MasterVGridKodeBarang);
end;

procedure TMasterBanFm.MasterVGridKodeBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropdownfm.Create(Self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKodeBarang.AsString:=BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

end.
