object MasterBarangFm: TMasterBarangFm
  Left = 436
  Top = 134
  Width = 893
  Height = 526
  BorderIcons = [biSystemMenu]
  Caption = 'Master Barang'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 877
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 433
    Width = 877
    Height = 35
    Align = alBottom
    AutoSize = True
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 9
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 9
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 9
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 300
      Top = 1
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 377
    Height = 385
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 150
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.Caption = 'Nama *'
      Properties.DataBinding.FieldName = 'Nama'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridSatuan: TcxDBEditorRow
      Properties.Caption = 'Satuan *'
      Properties.DataBinding.FieldName = 'Satuan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridJumlah: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Jumlah'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridMinimumStok: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'MinimumStok'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridMaximumStok: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'MaximumStok'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridStandardUmur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandardUmur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridLokasi: TcxDBEditorRow
      Properties.Caption = 'Lokasi *'
      Properties.DataBinding.FieldName = 'Lokasi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridNoPabrikan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoPabrikan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridClaimNWarranty: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ClaimNWarranty'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridDurasiClaimNWarranty: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DurasiClaimNWarranty'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridKategori: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKategoriEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kategori'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridDetailKategori: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailKategori'
      Properties.Options.Editing = False
      ID = 11
      ParentID = 10
      Index = 0
      Version = 1
    end
    object MasterVGridSingleSupplier: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.DataBinding.FieldName = 'SingleSupplier'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridHarga: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Harga'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 35
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridFoto: TcxDBEditorRow
      Height = 47
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Foto'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 14
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 468
    Width = 877
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 377
    Top = 48
    Width = 250
    Height = 385
    Align = alRight
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'namajenis'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1Column2PropertiesButtonClick
        Width = 138
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'tipe'
        Width = 108
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 627
    Top = 48
    Width = 250
    Height = 385
    Align = alRight
    TabOrder = 5
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGridDBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DataSource2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'KodeSupplier'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGridDBTableView1Column1PropertiesButtonClick
        Width = 93
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'NamaSupplier'
        Styles.Content = MenuUtamaFm.cxStyle5
        Width = 152
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from barang')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MasterQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object MasterQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object MasterQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object MasterQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object MasterQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object MasterQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object MasterQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object MasterQDetailKategori: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailKategori'
      LookupDataSet = KategoriQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'Kategori'
      Lookup = True
    end
    object MasterQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object MasterQFoto: TBlobField
      FieldName = 'Foto'
    end
    object MasterQNoPabrikan: TStringField
      FieldName = 'NoPabrikan'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Sta' +
        'ndardUmur, Lokasi, ClaimNWarranty, DurasiClaimNWarranty, Kategor' +
        'i, SingleSupplier, Harga, Keterangan, CreateDate, CreateBy, Oper' +
        'ator, TglEntry, Foto, NoPabrikan'#13#10'from barang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  MinimumStok = :MinimumStok,'
      '  MaximumStok = :MaximumStok,'
      '  StandardUmur = :StandardUmur,'
      '  Lokasi = :Lokasi,'
      '  ClaimNWarranty = :ClaimNWarranty,'
      '  DurasiClaimNWarranty = :DurasiClaimNWarranty,'
      '  Kategori = :Kategori,'
      '  SingleSupplier = :SingleSupplier,'
      '  Harga = :Harga,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Foto = :Foto,'
      '  NoPabrikan = :NoPabrikan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      
        '  (Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Standar' +
        'dUmur, Lokasi, ClaimNWarranty, DurasiClaimNWarranty, Kategori, S' +
        'ingleSupplier, Harga, Keterangan, CreateDate, CreateBy, Operator' +
        ', TglEntry, Foto, NoPabrikan)'
      'values'
      
        '  (:Kode, :Nama, :Jumlah, :Satuan, :MinimumStok, :MaximumStok, :' +
        'StandardUmur, :Lokasi, :ClaimNWarranty, :DurasiClaimNWarranty, :' +
        'Kategori, :SingleSupplier, :Harga, :Keterangan, :CreateDate, :Cr' +
        'eateBy, :Operator, :TglEntry, :Foto, :NoPabrikan)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from barang order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      
        'select d.*, j.namajenis, j.tipe from barang b, detailbarang d, j' +
        'eniskendaraan j where d.kodejeniskendaraan=j.kode and b.kode=d.k' +
        'odebarang and b.kode = :text')
    UpdateObject = SDUpdateSQL1
    Left = 249
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
        Value = ''
      end>
    object DetailQnamajenis: TStringField
      FieldName = 'namajenis'
      Required = True
      Size = 50
    end
    object DetailQtipe: TMemoField
      FieldName = 'tipe'
      Required = True
      BlobType = ftMemo
    end
    object DetailQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 492
    Top = 14
  end
  object JenisQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 569
    Top = 9
    object JenisQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DetailBarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    AfterDelete = DetailBarangQAfterDelete
    SQL.Strings = (
      'select * from detailbarang')
    Left = 601
    Top = 17
    object DetailBarangQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailBarangQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from detailbarang order by kode desc')
    Left = 377
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeJenisKendaraan'
      'from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    ModifySQL.Strings = (
      'update detailbarang'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeJenisKendaraan = :KodeJenisKendaraan'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    InsertSQL.Strings = (
      'insert into detailbarang'
      '  (KodeBarang, KodeJenisKendaraan)'
      'values'
      '  (:KodeBarang, :KodeJenisKendaraan)')
    DeleteSQL.Strings = (
      'delete from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    Left = 524
    Top = 2
  end
  object ListSupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = ListSupplierQAfterDelete
    SQL.Strings = (
      
        'select *, '#39#39' as NamaSupplier from listsupplier where kodebarang=' +
        ' :text')
    UpdateObject = SDUpdateSQL2
    Left = 689
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ListSupplierQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object ListSupplierQKodeSupplier: TStringField
      FieldName = 'KodeSupplier'
      Required = True
      Size = 10
    end
    object ListSupplierQNamaSupplier: TStringField
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'KodeSupplier'
      Required = True
      Size = 50
      Lookup = True
    end
  end
  object SDUpdateSQL2: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeSupplier'#13#10'from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    ModifySQL.Strings = (
      'update listsupplier'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeSupplier = :KodeSupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    InsertSQL.Strings = (
      'insert into listsupplier'
      '  (KodeBarang, KodeSupplier)'
      'values'
      '  (:KodeBarang, :KodeSupplier)')
    DeleteSQL.Strings = (
      'delete from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    Left = 732
    Top = 10
  end
  object DataSource2: TDataSource
    DataSet = ListSupplierQ
    Left = 652
    Top = 6
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from supplier')
    Left = 801
    Top = 7
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object KategoriQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from KategoriBarang')
    Left = 768
    Top = 8
    object KategoriQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriQKategori: TStringField
      FieldName = 'Kategori'
      Required = True
      Size = 50
    end
    object KategoriQMinStok: TIntegerField
      FieldName = 'MinStok'
    end
    object KategoriQMaxStok: TIntegerField
      FieldName = 'MaxStok'
    end
  end
  object DeleteJenisKendaraanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailbarang'
      'where kodebarang=:text')
    Left = 424
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteJenisKendaraanQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DeleteJenisKendaraanQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object ExecuteDeleteJenisKendaraanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailbarang'
      'where kodebarang=:text')
    UpdateObject = UpdateDeleteJenisKendaraanUS
    Left = 488
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ExecuteDeleteJenisKendaraanQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object ExecuteDeleteJenisKendaraanQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object UpdateDeleteJenisKendaraanUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeJenisKendaraan'
      'from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    ModifySQL.Strings = (
      'update detailbarang'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeJenisKendaraan = :KodeJenisKendaraan'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    InsertSQL.Strings = (
      'insert into detailbarang'
      '  (KodeBarang, KodeJenisKendaraan)'
      'values'
      '  (:KodeBarang, :KodeJenisKendaraan)')
    DeleteSQL.Strings = (
      'delete from detailbarang'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeJenisKendaraan = :OLD_KodeJenisKendaraan')
    Left = 456
    Top = 400
  end
  object DeleteListSupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from listsupplier'
      'where kodebarang=:text')
    Left = 712
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteListSupplierQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DeleteListSupplierQKodeSupplier: TStringField
      FieldName = 'KodeSupplier'
      Required = True
      Size = 10
    end
  end
  object ExecuteDeleteListSupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from listsupplier'
      'where kodebarang=:text')
    UpdateObject = UpdateDeleteListSupplierUS
    Left = 776
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ExecuteDeleteListSupplierQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object ExecuteDeleteListSupplierQKodeSupplier: TStringField
      FieldName = 'KodeSupplier'
      Required = True
      Size = 10
    end
  end
  object UpdateDeleteListSupplierUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBarang, KodeSupplier'#13#10'from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    ModifySQL.Strings = (
      'update listsupplier'
      'set'
      '  KodeBarang = :KodeBarang,'
      '  KodeSupplier = :KodeSupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    InsertSQL.Strings = (
      'insert into listsupplier'
      '  (KodeBarang, KodeSupplier)'
      'values'
      '  (:KodeBarang, :KodeSupplier)')
    DeleteSQL.Strings = (
      'delete from listsupplier'
      'where'
      '  KodeBarang = :OLD_KodeBarang and'
      '  KodeSupplier = :OLD_KodeSupplier')
    Left = 744
    Top = 400
  end
end
