unit MasterBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel, cxCheckBox, cxImage;

type
  TMasterBarangFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    DetailQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    DataSource1: TDataSource;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    DetailQnamajenis: TStringField;
    DetailQtipe: TMemoField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    JenisQ: TSDQuery;
    JenisQKode: TStringField;
    JenisQNamaJenis: TStringField;
    JenisQTipe: TMemoField;
    JenisQKeterangan: TMemoField;
    DetailBarangQ: TSDQuery;
    Kode2Q: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    DetailQKodeBarang: TStringField;
    DetailQKodeJenisKendaraan: TStringField;
    Kode2Qkode: TStringField;
    cxLabel1: TcxLabel;
    ListSupplierQ: TSDQuery;
    SDUpdateSQL2: TSDUpdateSQL;
    DataSource2: TDataSource;
    ListSupplierQKodeBarang: TStringField;
    ListSupplierQKodeSupplier: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQKategori: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    ListSupplierQNamaSupplier: TStringField;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    DetailBarangQKodeBarang: TStringField;
    DetailBarangQKodeJenisKendaraan: TStringField;
    KategoriQ: TSDQuery;
    KategoriQKode: TStringField;
    KategoriQKategori: TStringField;
    KategoriQMinStok: TIntegerField;
    KategoriQMaxStok: TIntegerField;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQSatuan: TStringField;
    MasterQMinimumStok: TIntegerField;
    MasterQMaximumStok: TIntegerField;
    MasterQStandardUmur: TIntegerField;
    MasterQLokasi: TStringField;
    MasterQClaimNWarranty: TBooleanField;
    MasterQDurasiClaimNWarranty: TIntegerField;
    MasterQKategori: TStringField;
    MasterQSingleSupplier: TBooleanField;
    MasterQHarga: TCurrencyField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQDetailKategori: TStringField;
    MasterVGridNama: TcxDBEditorRow;
    MasterVGridSatuan: TcxDBEditorRow;
    MasterVGridMinimumStok: TcxDBEditorRow;
    MasterVGridMaximumStok: TcxDBEditorRow;
    MasterVGridStandardUmur: TcxDBEditorRow;
    MasterVGridLokasi: TcxDBEditorRow;
    MasterVGridClaimNWarranty: TcxDBEditorRow;
    MasterVGridDurasiClaimNWarranty: TcxDBEditorRow;
    MasterVGridKategori: TcxDBEditorRow;
    MasterVGridSingleSupplier: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridDetailKategori: TcxDBEditorRow;
    DeleteJenisKendaraanQ: TSDQuery;
    ExecuteDeleteJenisKendaraanQ: TSDQuery;
    UpdateDeleteJenisKendaraanUS: TSDUpdateSQL;
    DeleteJenisKendaraanQKodeBarang: TStringField;
    DeleteJenisKendaraanQKodeJenisKendaraan: TStringField;
    ExecuteDeleteJenisKendaraanQKodeBarang: TStringField;
    ExecuteDeleteJenisKendaraanQKodeJenisKendaraan: TStringField;
    DeleteListSupplierQ: TSDQuery;
    ExecuteDeleteListSupplierQ: TSDQuery;
    UpdateDeleteListSupplierUS: TSDUpdateSQL;
    DeleteListSupplierQKodeBarang: TStringField;
    DeleteListSupplierQKodeSupplier: TStringField;
    ExecuteDeleteListSupplierQKodeBarang: TStringField;
    ExecuteDeleteListSupplierQKodeSupplier: TStringField;
    MasterVGridJumlah: TcxDBEditorRow;
    MasterQJumlah: TFloatField;
    MasterQFoto: TBlobField;
    MasterQNoPabrikan: TStringField;
    MasterVGridFoto: TcxDBEditorRow;
    MasterVGridNoPabrikan: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure DetailBarangQAfterDelete(DataSet: TDataSet);
    procedure ExitBtnClick(Sender: TObject);
    procedure DetailQAfterDelete(DataSet: TDataSet);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure ListSupplierQAfterDelete(DataSet: TDataSet);
    procedure MasterVGridKategoriEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1Column2PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterBarangFm: TMasterBarangFm;

  MasterOriSQL, DetailOriSQL, ListSupplierOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, BarangDropDown, JenisKendaraanDropDown, SupplierDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterBarangFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterBarangFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterBarangFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterBarangFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailQ.SQL.Text;
  ListSupplierOriSQL:=ListSupplierQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  JenisQ.Open;
  DetailBarangQ.Open;
  KategoriQ.Open;
  SupplierQ.Open;
  DetailQ.Open;
  ListSupplierQ.Open;
end;

procedure TMasterBarangFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  Detailq.Close;
  ListSupplierQ.Close;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterBarangFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterBarangFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterBarang.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarang.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBarang.AsBoolean;
end;

procedure TMasterBarangFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBarang.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarang.AsBoolean;
    MasterVGrid.Enabled:=True;
    detailq.SQL.Text:=DetailOriSQL;
    detailq.Close;
    detailq.ParamByName('text').AsString:='';
    detailq.Open;

    ListSupplierQ.Close;
    ListSupplierQ.ParamByName('text').AsString:='';
    ListSupplierQ.Open;
  end;
end;

procedure TMasterBarangFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterBarangFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailQ.Open;
    DetailQ.Edit;
    DetailQ.First;
    while DetailQ.Eof=FALSE do
    begin
        DetailQ.Edit;
        DetailQKodeBarang.AsString:=KodeEdit.Text;
        DetailQ.Post;
        DetailQ.Next;
    end;

    ListSupplierQ.Open;
    ListSupplierQ.Edit;
    ListSupplierQ.First;
    while ListSupplierQ.Eof=FALSE do
    begin
        ListSupplierQ.Edit;
        ListSupplierQKodeBarang.AsString:=KodeEdit.Text;
        ListSupplierQ.Post;
        ListSupplierQ.Next;
    end;
    DetailQ.ApplyUpdates;
    ListSupplierQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailQ.CommitUpdates;
    ListSupplierQ.CommitUpdates;
    ShowMessage('Barang dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
    FormShow(self);
  except
    on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        showMessage('Penyimpanan Gagal');
    end;
  end;
  DetailQ.Close;
  detailq.SQL.Text:=DetailOriSQL;
  detailq.ParamByName('text').AsString:='';
  detailq.Open;
  detailq.Close;
  ListSupplierQ.Close;
  ListSupplierQ.SQL.Text:=ListSupplierOriSQL;
  ListSupplierQ.ParamByName('text').AsString:='';
  ListSupplierQ.Open;
  ListSupplierQ.Close;
  KodeEdit.SetFocus;
end;

procedure TMasterBarangFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterBarangFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterBarangFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Barang '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteJenisKendaraanQ.Close;
       DeleteJenisKendaraanQ.ParamByName('text').AsString:=KodeEdit.Text;
       DeleteJenisKendaraanQ.Open;
       DeleteJenisKendaraanQ.First;
       while DeleteJenisKendaraanQ.Eof=FALSE
       do
       begin
        ExecuteDeleteJenisKendaraanQ.SQL.Text:=('delete from DetailBarang where KodeBarang='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteJenisKendaraanQ.ExecSQL;
        DeleteJenisKendaraanQ.Next;
       end;
       DeleteListSupplierQ.Close;
       DeleteListSupplierQ.ParamByName('text').AsString:=KodeEdit.Text;
       DeleteListSupplierQ.Open;
       DeleteListSupplierQ.First;
       while DeleteListSupplierQ.Eof=FALSE
       do
       begin
        ExecuteDeleteListSupplierQ.SQL.Text:=('delete from ListSupplier where KodeBarang='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteListSupplierQ.ExecSQL;
        DeleteListSupplierQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Barang telah dihapus.',mtInformation,[mbOK],0);
     except
      on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        ListSupplierQ.RollbackUpdates;
        DetailQ.RollbackUpdates;
        MessageDlg('Barang pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
       end;
     end;
     KodeEdit.SetFocus;
  end;
  ListSupplierQ.Close;
  ListSupplierQ.ParamByName('text').AsString:='';
  ListSupplierQ.Open;
  DetailQ.Close;
  DetailQ.ParamByName('text').AsString:='';
  DetailQ.Open;
end;

procedure TMasterBarangFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    BarangDropDownFm:=TBarangDropdownfm.Create(Self);
    if BarangDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=BarangDropDownFm.kode;
      KodeEditExit(Sender);
      DeleteJenisKendaraanQ.Open;
      ExecuteDeleteJenisKendaraanQ.Open;
      DeleteListSupplierQ.Open;
      ExecuteDeleteListSupplierQ.Open;
      detailq.Close;
      detailq.ParamByName('text').AsString := MasterQKode.AsString;
      detailq.Open;
      ListSupplierQ.Close;
      ListSupplierQ.ParamByName('text').AsString := MasterQKode.asstring;
      listsupplierq.Open;
      MasterVGrid.SetFocus;
    end;
    BarangDropDownFm.Release;
end;

procedure TMasterBarangFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridNama);
end;

procedure TMasterBarangFm.cxButton1Click(Sender: TObject);
var i : integer;
begin
  for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
  begin
    showmessage(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[1])
  end;

end;

procedure TMasterBarangFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  //ShowMessage(inttostr(abuttonindex));

  if abuttonindex=10 then
  begin
  DetailQ.Edit;

  DetailQKodeBarang.AsString:=MasterQKode.AsString;
  DetailQ.Post;
   { DropDownFm:=TDropdownfm.Create(Self,JenisQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      detailq.Open;
      detailq.Insert;
      DetailQKodeBarang.AsString:=masterqkode.AsString;
      detailqkodejeniskendaraan.AsString:=jenisqkode.AsString;
      detailqnamajenis.AsString:=jenisqnamajenis.asstring;
      detailqtipe.asstring:=jenisqtipe.asstring;
      detailq.Post;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;

    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg'; }
  end;
end;

procedure TMasterBarangFm.DetailBarangQAfterDelete(DataSet: TDataSet);
begin
    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;
end;

procedure TMasterBarangFm.ExitBtnClick(Sender: TObject);
begin
  release;
end;

procedure TMasterBarangFm.DetailQAfterDelete(DataSet: TDataSet);
begin
MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
end;

procedure TMasterBarangFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
  ListSupplierQ.Edit;
  ListSupplierQKodeBarang.AsString:=MasterQKode.AsString;
  ListSupplierQ.Post;
   { DropDownFm:=TDropdownfm.Create(Self,SupplierQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      ListSupplierQ.Open;
      listsupplierq.Insert;
      ListSupplierQKodeBarang.AsString:=masterqkode.AsString;
      ListSupplierQKodeSupplier.AsString:=supplierqkode.AsString;
      listsupplierq.Post;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
      listsupplierq.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      listsupplierq.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      listsupplierq.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      listsupplierq.Close;
      listsupplierq.SQL.Text:=ListSupplierOriSQL;
      listsupplierq.ExecSQL;
      listsupplierq.Open;
    end;
    cxGrid1DBTableView1.Focused:=false;

    {cxGrid1DBTableView1.DataController.RecordCount:=cxGrid1DBTableView1.DataController.RecordCount+1;
    cxGrid1DBTableView1.DataController.Values[0,0]:='aaaa';
    cxGrid1DBTableView1.DataController.Values[0,1]:='bbbb';}
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[0].EditValue:='asdasd';
    //cxGrid1DBTableView1.NewItemRow.GridView.Columns[1].EditValue:='gfsdgsdg';  }
  end;
end;

procedure TMasterBarangFm.ListSupplierQAfterDelete(DataSet: TDataSet);
begin
    MenuUtamaFm.Database1.StartTransaction;
    try
      ListSupplierQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      ListSupplierQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      ListSupplierQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      ListSupplierQ.Close;
      ListSupplierQ.SQL.Text:=ListSupplierOriSQL;
      ListSupplierQ.ExecSQL;
      ListSupplierQ.Open;
    end;
end;




procedure TMasterBarangFm.MasterVGridKategoriEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    KategoriQ.Close;
    //KategoriQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    KategoriQ.ExecSQL;
    KategoriQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,KategoriQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKategori.AsString:=KategoriQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterBarangFm.cxGrid1DBTableView1Column2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
     JenisKendaraanDropDownFm:=TJenisKendaraanDropdownfm.Create(Self);
    if JenisKendaraanDropDownFm.ShowModal=MrOK then
    begin
      DetailQ.Open;
      DetailQ.Insert;

      //ArmadaKontrakQPlatNo.AsString:= ArmadaQPlatNo.AsString;
      //ArmadaKontrakQKontrak.AsString:=ArmadaDropDownFm.kode;
      //ArmadaKontrakQPlatNo.AsString:=ArmadaDropDownFm.plat;
      DetailQKodeJenisKendaraan.AsString:=JenisKendaraanDropDownFm.kode;
      DetailQnamajenis.AsString:=JenisKendaraanDropDownFm.nama;
      DetailQtipe.AsString:=JenisKendaraanDropDownFm.tipe;
      //ArmadaKontrakQ.Post;
    end;
    JenisKendaraanDropDownFm.Release;
end;

procedure TMasterBarangFm.cxGridDBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
     SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
    if SupplierDropDownFm.ShowModal=MrOK then
    begin
      ListSupplierQ.Open;
      ListSupplierQ.Insert;
      ListSupplierQKodeSupplier.AsString:=SupplierDropDownFm.kode;
      ListSupplierQNamaSupplier.AsString:=SupplierDropDownFm.nama;
    end;
    SupplierDropDownFm.Release;
end;

end.
