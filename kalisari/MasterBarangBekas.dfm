object MasterBarangBekasFm: TMasterBarangBekasFm
  Left = 471
  Top = 188
  Width = 587
  Height = 298
  Caption = 'Master Barang Bekas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 240
    Width = 571
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 189
    Width = 571
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 416
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 571
    Height = 141
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 3
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 569
      Height = 139
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 135
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = MAsterDs
      Version = 1
      object MasterVGridNama: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Nama'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridJumlah: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Jumlah'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridStandardUmur: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'StandardUmur'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridLokasi: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Lokasi'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridHarga: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Harga'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Keterangan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BarangBekas')
    UpdateObject = MAsterUs
    Left = 304
    Top = 16
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object MasterQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object MasterQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object MasterQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object MAsterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Jumlah, StandardUmur, Lokasi, Harga, Keterang' +
        'an, CreateDate, CreateBy, Operator, TglEntry'
      'from BarangBekas'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update BarangBekas'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Jumlah = :Jumlah,'
      '  StandardUmur = :StandardUmur,'
      '  Lokasi = :Lokasi,'
      '  Harga = :Harga,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into BarangBekas'
      
        '  (Kode, Nama, Jumlah, StandardUmur, Lokasi, Harga, Keterangan, ' +
        'CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :Nama, :Jumlah, :StandardUmur, :Lokasi, :Harga, :Keter' +
        'angan, :CreateDate, :CreateBy, :Operator, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from BarangBekas'
      'where'
      '  Kode = :OLD_Kode')
    Left = 368
    Top = 16
  end
  object MAsterDs: TDataSource
    DataSet = MasterQ
    Left = 336
    Top = 16
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from BarangBekas order by kode desc')
    Left = 257
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
