unit MasterBarangBekas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, Menus, cxStyles, cxVGrid, cxDBVGrid, DB, SDEngine,
  cxInplaceContainer, cxLabel, StdCtrls, cxButtons, cxTextEdit, cxMaskEdit,
  cxButtonEdit, ExtCtrls, ComCtrls;

type
  TMasterBarangBekasFm = class(TForm)
    StatusBar: TStatusBar;
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    SearchBtn: TcxButton;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    cxLabel1: TcxLabel;
    Panel1: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    MasterQ: TSDQuery;
    MAsterUs: TSDUpdateSQL;
    MAsterDs: TDataSource;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQJumlah: TIntegerField;
    MasterQStandardUmur: TIntegerField;
    MasterQLokasi: TStringField;
    MasterQHarga: TCurrencyField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterVGridNama: TcxDBEditorRow;
    MasterVGridJumlah: TcxDBEditorRow;
    MasterVGridStandardUmur: TcxDBEditorRow;
    MasterVGridLokasi: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MasterBarangBekasFm: TMasterBarangBekasFm;
  MasterOriSql:string;

implementation

uses DM, MenuUtama, DropDown, BarangBekasDropDown;

{$R *.dfm}

procedure TMasterBarangBekasFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TMasterBarangBekasFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterBarangBekasFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterBarangBekas.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarangBekas.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBarangBekas.AsBoolean;
end;

procedure TMasterBarangBekasFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterBarangBekasFm.KodeEditExit(Sender: TObject);
begin
 if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterBarangBekas.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarangBekas.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TMasterBarangBekasFm.KodeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterBarangBekasFm.SaveBtnClick(Sender: TObject);
begin
 if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Barang Bekas dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterBarangBekasFm.DeleteBtnClick(Sender: TObject);
begin
 if MessageDlg('Hapus BarangBekas '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Barang Bekas telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Barang Bekas pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterBarangBekasFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TMasterBarangBekasFm.SearchBtnClick(Sender: TObject);
begin
  //MasterQ.SQL.Text:=MasterOriSQL;
    BarangBekasDropDownFm:=TBarangBekasDropdownfm.Create(Self);
    if BarangBekasDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=BarangBekasDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    BarangBekasDropDownFm.Release;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterBarangBekas.AsBoolean;
end;

procedure TMasterBarangBekasFm.KodeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

end.
