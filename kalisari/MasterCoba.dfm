object MasterCobaFm: TMasterCobaFm
  Left = 426
  Top = 219
  Width = 607
  Height = 469
  Caption = 'Master Coba'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 361
    Width = 591
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 412
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 56
    Width = 577
    Height = 153
    OptionsView.RowHeaderWidth = 91
    TabOrder = 3
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBVerticalGrid1Kode: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kode'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1PlatNo: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Panjang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Panjang'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1Kapasitas: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kapasitas'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1Aktif: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Aktif'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1Sopir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Sopir'
      ID = 10
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1Ekor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Ekor'
      ID = 11
      ParentID = -1
      Index = 7
      Version = 1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from armada')
    UpdateObject = MasterUS
    Left = 265
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object MasterQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object MasterQKapasitas: TFloatField
      FieldName = 'Kapasitas'
      Required = True
    end
    object MasterQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object MasterQEkor: TStringField
      FieldName = 'Ekor'
      Required = True
      Size = 10
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PlatNo, Panjang, Kapasitas, Aktif, Keterangan, Crea' +
        'teDate, CreateBy, Operator, TglEntry, Sopir, Ekor'#13#10'from armada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update armada'
      'set'
      '  Kode = :Kode,'
      '  PlatNo = :PlatNo,'
      '  Panjang = :Panjang,'
      '  Kapasitas = :Kapasitas,'
      '  Aktif = :Aktif,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Sopir = :Sopir,'
      '  Ekor = :Ekor'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into armada'
      
        '  (Kode, PlatNo, Panjang, Kapasitas, Aktif, Keterangan, CreateDa' +
        'te, CreateBy, Operator, TglEntry, Sopir, Ekor)'
      'values'
      
        '  (:Kode, :PlatNo, :Panjang, :Kapasitas, :Aktif, :Keterangan, :C' +
        'reateDate, :CreateBy, :Operator, :TglEntry, :Sopir, :Ekor)')
    DeleteSQL.Strings = (
      'delete from armada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from sopir order by kode desc')
    Left = 321
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
