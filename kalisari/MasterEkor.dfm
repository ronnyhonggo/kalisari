object MasterEkorFm: TMasterEkorFm
  Left = 471
  Top = 321
  Width = 506
  Height = 298
  AutoSize = True
  Caption = 'Master Ekor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 490
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 190
    Width = 490
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 490
    Height = 142
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 115
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridPanjang: TcxDBEditorRow
      Properties.Caption = 'Panjang (mtr)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'Panjang'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridBerat: TcxDBEditorRow
      Properties.Caption = 'Berat (ton)'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'Berat'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'JumlahBan'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridKirMulai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirMulai'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridKirSelesai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirSelesai'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 241
    Width = 490
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from ekor')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPanjang: TStringField
      FieldName = 'Panjang'
      Size = 50
    end
    object MasterQBerat: TStringField
      FieldName = 'Berat'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object MasterQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object MasterQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Panjang, Berat, CreateDate, CreateBy, TglEntry, Ope' +
        'rator, JumlahBan, KirMulai, KirSelesai'#13#10'from ekor'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update ekor'
      'set'
      '  Kode = :Kode,'
      '  Panjang = :Panjang,'
      '  Berat = :Berat,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  JumlahBan = :JumlahBan,'
      '  KirMulai = :KirMulai,'
      '  KirSelesai = :KirSelesai'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into ekor'
      
        '  (Kode, Panjang, Berat, CreateDate, CreateBy, TglEntry, Operato' +
        'r, JumlahBan, KirMulai, KirSelesai)'
      'values'
      
        '  (:Kode, :Panjang, :Berat, :CreateDate, :CreateBy, :TglEntry, :' +
        'Operator, :JumlahBan, :KirMulai, :KirSelesai)')
    DeleteSQL.Strings = (
      'delete from ekor'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from ekor order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
