object MasterHargaBBMFm: TMasterHargaBBMFm
  Left = 447
  Top = 270
  Width = 512
  Height = 306
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Harga BBM'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 198
    Width = 496
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 92
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 259
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      Visible = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 420
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 496
    Height = 150
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 114
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridHargaSolar: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = MasterVGridHargaSolarEditPropertiesValidate
      Properties.DataBinding.FieldName = 'HargaSolar'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridHargaBensin: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = MasterVGridHargaBensinEditPropertiesValidate
      Properties.DataBinding.FieldName = 'HargaBensin'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 249
    Width = 496
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from HargaBBM')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQHargaSolar: TCurrencyField
      FieldName = 'HargaSolar'
      Required = True
    end
    object MasterQHargaBensin: TCurrencyField
      FieldName = 'HargaBensin'
      Required = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, HargaSolar, HargaBensin'
      'from HargaBBM'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update HargaBBM'
      'set'
      '  Kode = :Kode,'
      '  HargaSolar = :HargaSolar,'
      '  HargaBensin = :HargaBensin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into HargaBBM'
      '  (Kode, HargaSolar, HargaBensin)'
      'values'
      '  (:Kode, :HargaSolar, :HargaBensin)')
    DeleteSQL.Strings = (
      'delete from HargaBBM'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from HargaBBM order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from sopir where kode like '#39'%'#39' + :text + '#39'%'#39' or nama li' +
        'ke '#39'%'#39' + :text + '#39'%'#39
      '')
    Left = 409
    Top = 199
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object SopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object SopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object SopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SopirQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object EkorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from ekor where kode like '#39'%'#39' + :text + '#39'%'#39' '
      '')
    Left = 449
    Top = 199
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object EkorQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object EkorQPanjang: TStringField
      FieldName = 'Panjang'
      Size = 50
    end
    object EkorQBerat: TStringField
      FieldName = 'Berat'
      Size = 50
    end
    object EkorQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object EkorQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object EkorQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object EkorQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
  end
  object JenisKendaraanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 368
    Top = 200
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
