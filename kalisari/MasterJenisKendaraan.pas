unit MasterJenisKendaraan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, cxCheckBox, cxMemo;

type
  TMasterJenisKendaraanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    SopirQ: TSDQuery;
    EkorQ: TSDQuery;
    SopirQKode: TStringField;
    SopirQNama: TStringField;
    SopirQAlamat: TStringField;
    SopirQNotelp: TStringField;
    SopirQCreateDate: TDateTimeField;
    SopirQCreateBy: TStringField;
    SopirQOperator: TStringField;
    SopirQTglEntry: TDateTimeField;
    EkorQKode: TStringField;
    EkorQPanjang: TStringField;
    EkorQBerat: TStringField;
    EkorQCreateDate: TDateTimeField;
    EkorQCreateBy: TStringField;
    EkorQTglEntry: TDateTimeField;
    EkorQOperator: TStringField;
    JenisKendaraanQ: TSDQuery;
    JenisKendaraanQKode: TStringField;
    JenisKendaraanQNamaJenis: TStringField;
    JenisKendaraanQTipe: TMemoField;
    JenisKendaraanQKeterangan: TMemoField;
    KodeQkode: TStringField;
    cxLabel1: TcxLabel;
    MasterQKode: TStringField;
    MasterQNamaJenis: TStringField;
    MasterQTipe: TMemoField;
    MasterQKategori: TStringField;
    MasterQKeterangan: TMemoField;
    MasterVGridNamaJenis: TcxDBEditorRow;
    MasterVGridTipe: TcxDBEditorRow;
    MasterVGridKategori: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridSopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSopirEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridEkorEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridJenisKendaraanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterJenisKendaraanFm: TMasterJenisKendaraanFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, JenisKendaraanDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterJenisKendaraanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterJenisKendaraanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterJenisKendaraanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterJenisKendaraanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterJenisKendaraanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterJenisKendaraanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterJenisKendaraanFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterJenisKendaraanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertJenisKendaraan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateJenisKendaraan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteJenisKendaraan.AsBoolean;
end;

procedure TMasterJenisKendaraanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteJenisKendaraan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateJenisKendaraan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TMasterJenisKendaraanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterJenisKendaraanFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Jenis Kendaraan dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterJenisKendaraanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterJenisKendaraanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterJenisKendaraanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Jenis Kendaraan '+KodeEdit.Text+' - '+MasterQNamaJenis.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Jenis Kendaraan telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Jenis Kendaraan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterJenisKendaraanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    JenisKendaraanDropDownFm:=TJenisKendaraanDropdownfm.Create(Self);
    if JenisKendaraanDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=JenisKendaraanDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    JenisKendaraanDropDownFm.Release;
end;

procedure TMasterJenisKendaraanFm.MasterVGridSopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SopirQ.Close;
    SopirQ.ParamByName('text').AsString:='';
    SopirQ.ExecSQL;
    SopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString:=SopirQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterJenisKendaraanFm.MasterVGridSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SopirQ.Close;
    SopirQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SopirQ.ExecSQL;
    SopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString:=SopirQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterJenisKendaraanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    EkorQ.Close;
    EkorQ.ParamByName('text').AsString:='';
    EkorQ.ExecSQL;
    EkorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,EkorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQEkor.AsString:=EkorQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterJenisKendaraanFm.MasterVGridEkorEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    EkorQ.Close;
    EkorQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    EkorQ.ExecSQL;
    EkorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,EkorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQEkor.AsString:=EkorQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterJenisKendaraanFm.MasterVGridExit(Sender: TObject);
begin
  SaveBtn.SetFocus;
end;

procedure TMasterJenisKendaraanFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridNamaJenis);
end;

procedure TMasterJenisKendaraanFm.MasterVGridJenisKendaraanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    JenisKendaraanQ.Close;
    //JenisKendaraanQ.ParamByName('text').AsString:='';
    JenisKendaraanQ.ExecSQL;
    JenisKendaraanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,JenisKendaraanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQJenisKendaraan.AsString:=JenisKendaraanQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

end.
