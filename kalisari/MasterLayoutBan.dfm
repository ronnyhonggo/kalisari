object MasterLayoutBanFm: TMasterLayoutBanFm
  Left = 486
  Top = 179
  Width = 425
  Height = 442
  BorderIcons = [biSystemMenu]
  Caption = 'Master Layout Ban'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 409
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 333
    Width = 409
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 324
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 409
    Height = 285
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 117
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.Caption = 'Nama *'
      Properties.DataBinding.FieldName = 'Nama'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridJumlahBan: TcxDBEditorRow
      Properties.Caption = 'JumlahBan *'
      Properties.DataBinding.FieldName = 'JumlahBan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridGambar: TcxDBEditorRow
      Height = 241
      Properties.Caption = 'Gambar *'
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Gambar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 384
    Width = 409
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterlayoutban')
    UpdateObject = MasterUS
    Left = 297
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object MasterQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object MasterQGambar: TBlobField
      FieldName = 'Gambar'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 332
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, Nama, JumlahBan, Gambar'#13#10'from masterlayoutban'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update masterlayoutban'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  JumlahBan = :JumlahBan,'
      '  Gambar = :Gambar'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into masterlayoutban'
      '  (Kode, Nama, JumlahBan, Gambar)'
      'values'
      '  (:Kode, :Nama, :JumlahBan, :Gambar)')
    DeleteSQL.Strings = (
      'delete from masterlayoutban'
      'where'
      '  Kode = :OLD_Kode')
    Left = 364
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from masterlayoutban order by kode desc')
    Left = 265
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
