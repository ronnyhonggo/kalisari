unit MasterLokasiBan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, cxLabel, cxImage,
  cxCheckBox;

type
  TMasterLokasiBanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    cxLabel1: TcxLabel;
    LayoutQ: TSDQuery;
    LayoutQKode: TStringField;
    LayoutQNama: TStringField;
    LayoutQGambar: TBlobField;
    MasterVGridKodeLayout: TcxDBEditorRow;
    MasterVGridNama: TcxDBEditorRow;
    MasterVGridGambar: TcxDBEditorRow;
    MasterVGridNamaLayout: TcxDBEditorRow;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQKodeLayout: TStringField;
    MasterQNama: TStringField;
    MasterQGambar: TBlobField;
    MasterQNamaLayout: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridKodeLayoutEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterLokasiBanFm: TMasterLokasiBanFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, LayoutBanDropDown, LokasiBanDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterLokasiBanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterLokasiBanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterLokasiBanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterLokasiBanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterLokasiBanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterLokasiBanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  MasterVGrid.Enabled:=true;
  KodeEditExit(sender);

  MasterVGrid.SetFocus;
end;

procedure TMasterLokasiBanFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterLokasiBanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterLokasiBan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterLokasiBan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterLokasiBan.AsBoolean;
end;

procedure TMasterLokasiBanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterLokasiBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterLokasiBan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TMasterLokasiBanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterLokasiBanFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Ban dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
     on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        showMessage('Penyimpanan Gagal');
       end;
  end;
end;

procedure TMasterLokasiBanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
//  DMFm.GetDateQ.Open;
//  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
//  MasterQCreateBy.AsString:=User;
//  DMFm.GetDateQ.Close;
end;

procedure TMasterLokasiBanFm.MasterQBeforePost(DataSet: TDataSet);
begin
//  DMFm.GetDateQ.Open;
//  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
//  MasterQOperator.AsString:=User;
//  DMFm.GetDateQ.Close;
end;

procedure TMasterLokasiBanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Ban '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Ban telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        MessageDlg('Lokasi Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
       end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterLokasiBanFm.SearchBtnClick(Sender: TObject);
begin
    LokasiBanDropDownFm:=TLokasiBanDropDownFm.Create(self);
    if LokasiBanDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=LokasiBanDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    LokasiBanDropDownFm.Release;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterLokasiBan.AsBoolean;
end;

procedure TMasterLokasiBanFm.MasterVGridExit(Sender: TObject);
begin
  //SaveBtn.SetFocus;
end;

procedure TMasterLokasiBanFm.MasterVGridKodeLayoutEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LayoutBanDropDownFm:=TLayoutBanDropDownFm.Create(self);
  if LayoutBanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKodeLayout.AsString:= LayoutBanDropDownFm.kode;
  end;
  LayoutBanDropDownFm.Release;
end;

end.
