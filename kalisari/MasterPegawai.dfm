object MasterPegawaiFm: TMasterPegawaiFm
  Left = 478
  Top = 131
  Width = 500
  Height = 519
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Pegawai'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 484
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 410
    Width = 484
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 408
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 484
    Height = 362
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 120
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.Caption = 'Nama *'
      Properties.DataBinding.FieldName = 'Nama'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNoKtp: TcxDBEditorRow
      Properties.Caption = 'NoKTP'
      Properties.DataBinding.FieldName = 'NoKtp'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridAktif: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Aktif'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridKota: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kota'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridNoTelp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoTelp'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridNoHP: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoHP'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridTglLahir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglLahir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridGaji: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Gaji'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridJabatan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Jabatan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridMulaiBekerja: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'MulaiBekerja'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridNomorSIM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NomorSIM'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridExpiredSIM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ExpiredSIM'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 55
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 461
    Width = 484
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object MasterQNoKtp: TStringField
      FieldName = 'NoKtp'
      Size = 50
    end
    object MasterQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object MasterQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object MasterQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object MasterQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object MasterQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object MasterQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object MasterQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object MasterQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object MasterQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object MasterQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Alamat, Kota, NoTelp, NoHP, TglLahir, Gaji, J' +
        'abatan, MulaiBekerja, NomorSIM, ExpiredSIM, Aktif, Keterangan, N' +
        'oKTP'#13#10'from pegawai'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update pegawai'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Alamat = :Alamat,'
      '  Kota = :Kota,'
      '  NoTelp = :NoTelp,'
      '  NoHP = :NoHP,'
      '  TglLahir = :TglLahir,'
      '  Gaji = :Gaji,'
      '  Jabatan = :Jabatan,'
      '  MulaiBekerja = :MulaiBekerja,'
      '  NomorSIM = :NomorSIM,'
      '  ExpiredSIM = :ExpiredSIM,'
      '  Aktif = :Aktif,'
      '  Keterangan = :Keterangan,'
      '  NoKTP = :NoKTP'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into pegawai'
      
        '  (Kode, Nama, Alamat, Kota, NoTelp, NoHP, TglLahir, Gaji, Jabat' +
        'an, MulaiBekerja, NomorSIM, ExpiredSIM, Aktif, Keterangan, NoKTP' +
        ')'
      'values'
      
        '  (:Kode, :Nama, :Alamat, :Kota, :NoTelp, :NoHP, :TglLahir, :Gaj' +
        'i, :Jabatan, :MulaiBekerja, :NomorSIM, :ExpiredSIM, :Aktif, :Ket' +
        'erangan, :NoKTP)')
    DeleteSQL.Strings = (
      'delete from pegawai'
      'where'
      '  Kode = :OLD_Kode')
    Left = 428
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pegawai order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
