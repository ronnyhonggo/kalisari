object MasterPersenPremiFm: TMasterPersenPremiFm
  Left = 360
  Top = 93
  Width = 662
  Height = 548
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Persen Premi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 440
    Width = 646
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 92
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 259
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      Visible = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 420
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 646
    Height = 392
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 217
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridCategoryRow1: TcxCategoryRow
      Properties.Caption = 'Pengemudi'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPatas1: TcxDBEditorRow
      Properties.Caption = 'Patas1 (975.000 - 1.200.000) '
      Properties.DataBinding.FieldName = 'Patas1'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridPatas2: TcxDBEditorRow
      Properties.Caption = 'Patas2 (1.200.000 - 1.700.000)'
      Properties.DataBinding.FieldName = 'Patas2'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridPatas3: TcxDBEditorRow
      Properties.Caption = 'Patas3 (>1.700.000)'
      Properties.DataBinding.FieldName = 'Patas3'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridBoomel1: TcxDBEditorRow
      Properties.Caption = 'Boomel1 (1.000.000 - 1.200.000)'
      Properties.DataBinding.FieldName = 'Boomel1'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridBoomel2: TcxDBEditorRow
      Properties.Caption = 'Boomel2 (1.200.000 - 1.450.000)'
      Properties.DataBinding.FieldName = 'Boomel2'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridBoomel3: TcxDBEditorRow
      Properties.Caption = 'Boomel3 (>1.450.000)'
      Properties.DataBinding.FieldName = 'Boomel3'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridCategoryRow2: TcxCategoryRow
      Properties.Caption = 'Kernet'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridPatasKernet1: TcxDBEditorRow
      Properties.Caption = 'Patas1 (975.000 - 1.200.000) '
      Properties.DataBinding.FieldName = 'PatasKernet1'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridPatasKernet2: TcxDBEditorRow
      Properties.Caption = 'Patas2 (1.200.000 - 1.700.000)'
      Properties.DataBinding.FieldName = 'PatasKernet2'
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridPatasKernet3: TcxDBEditorRow
      Properties.Caption = 'Patas3 (>1.700.000)'
      Properties.DataBinding.FieldName = 'PatasKernet3'
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridBoomelKernet1: TcxDBEditorRow
      Properties.Caption = 'Boomel1 (1.000.000 - 1.200.000)'
      Properties.DataBinding.FieldName = 'BoomelKernet1'
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridBoomelKernet2: TcxDBEditorRow
      Properties.Caption = 'Boomel2 (1.200.000 - 1.450.000)'
      Properties.DataBinding.FieldName = 'BoomelKernet2'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridBoomeKernet3: TcxDBEditorRow
      Properties.Caption = 'Boomel3 (>1.450.000)'
      Properties.DataBinding.FieldName = 'BoomeKernet3'
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridCategoryRow3: TcxCategoryRow
      Properties.Caption = 'Kondektur'
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridBoomelKondektur1: TcxDBEditorRow
      Properties.Caption = 'Boomel1 (1.000.000 - 1.200.000)'
      Properties.DataBinding.FieldName = 'BoomelKondektur1'
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridBoomelKondektur2: TcxDBEditorRow
      Properties.Caption = 'Boomel2 (1.200.000 - 1.450.000)'
      Properties.DataBinding.FieldName = 'BoomelKondektur2'
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridBoomelKondektur3: TcxDBEditorRow
      Properties.Caption = 'Boomel3 (>1.450.000)'
      Properties.DataBinding.FieldName = 'BoomelKondektur3'
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 491
    Width = 646
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from MasterPersenPremi')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPatas1: TFloatField
      FieldName = 'Patas1'
    end
    object MasterQPatas2: TFloatField
      FieldName = 'Patas2'
    end
    object MasterQPatas3: TFloatField
      FieldName = 'Patas3'
    end
    object MasterQBoomel1: TFloatField
      FieldName = 'Boomel1'
    end
    object MasterQBoomel2: TFloatField
      FieldName = 'Boomel2'
    end
    object MasterQBoomel3: TFloatField
      FieldName = 'Boomel3'
    end
    object MasterQPatasKernet1: TFloatField
      FieldName = 'PatasKernet1'
    end
    object MasterQPatasKernet2: TFloatField
      FieldName = 'PatasKernet2'
    end
    object MasterQPatasKernet3: TFloatField
      FieldName = 'PatasKernet3'
    end
    object MasterQBoomelKernet1: TFloatField
      FieldName = 'BoomelKernet1'
    end
    object MasterQBoomelKernet2: TFloatField
      FieldName = 'BoomelKernet2'
    end
    object MasterQBoomeKernet3: TFloatField
      FieldName = 'BoomeKernet3'
    end
    object MasterQBoomelKondektur1: TFloatField
      FieldName = 'BoomelKondektur1'
    end
    object MasterQBoomelKondektur2: TFloatField
      FieldName = 'BoomelKondektur2'
    end
    object MasterQBoomelKondektur3: TFloatField
      FieldName = 'BoomelKondektur3'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Patas1, Patas2, Patas3, Boomel1, Boomel2, Boomel3, ' +
        'PatasKernet1, PatasKernet2, PatasKernet3, BoomelKernet1, BoomelK' +
        'ernet2, BoomeKernet3, BoomelKondektur1, BoomelKondektur2, Boomel' +
        'Kondektur3'
      'from MasterPersenPremi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update MasterPersenPremi'
      'set'
      '  Kode = :Kode,'
      '  Patas1 = :Patas1,'
      '  Patas2 = :Patas2,'
      '  Patas3 = :Patas3,'
      '  Boomel1 = :Boomel1,'
      '  Boomel2 = :Boomel2,'
      '  Boomel3 = :Boomel3,'
      '  PatasKernet1 = :PatasKernet1,'
      '  PatasKernet2 = :PatasKernet2,'
      '  PatasKernet3 = :PatasKernet3,'
      '  BoomelKernet1 = :BoomelKernet1,'
      '  BoomelKernet2 = :BoomelKernet2,'
      '  BoomeKernet3 = :BoomeKernet3,'
      '  BoomelKondektur1 = :BoomelKondektur1,'
      '  BoomelKondektur2 = :BoomelKondektur2,'
      '  BoomelKondektur3 = :BoomelKondektur3'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into MasterPersenPremi'
      
        '  (Kode, Patas1, Patas2, Patas3, Boomel1, Boomel2, Boomel3, Pata' +
        'sKernet1, PatasKernet2, PatasKernet3, BoomelKernet1, BoomelKerne' +
        't2, BoomeKernet3, BoomelKondektur1, BoomelKondektur2, BoomelKond' +
        'ektur3)'
      'values'
      
        '  (:Kode, :Patas1, :Patas2, :Patas3, :Boomel1, :Boomel2, :Boomel' +
        '3, :PatasKernet1, :PatasKernet2, :PatasKernet3, :BoomelKernet1, ' +
        ':BoomelKernet2, :BoomeKernet3, :BoomelKondektur1, :BoomelKondekt' +
        'ur2, :BoomelKondektur3)')
    DeleteSQL.Strings = (
      'delete from MasterPersenPremi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from MasterPersenPremi order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from sopir where kode like '#39'%'#39' + :text + '#39'%'#39' or nama li' +
        'ke '#39'%'#39' + :text + '#39'%'#39
      '')
    Left = 409
    Top = 199
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object SopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object SopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object SopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SopirQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object EkorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from ekor where kode like '#39'%'#39' + :text + '#39'%'#39' '
      '')
    Left = 449
    Top = 199
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object EkorQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object EkorQPanjang: TStringField
      FieldName = 'Panjang'
      Size = 50
    end
    object EkorQBerat: TStringField
      FieldName = 'Berat'
      Size = 50
    end
    object EkorQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object EkorQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object EkorQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object EkorQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
  end
  object JenisKendaraanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 368
    Top = 200
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
