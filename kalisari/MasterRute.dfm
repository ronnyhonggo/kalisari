object MasterRuteFm: TMasterRuteFm
  Left = 516
  Top = 73
  Width = 758
  Height = 594
  BorderIcons = [biSystemMenu]
  Caption = 'Master Rute'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 742
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      TabStop = False
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 485
    Width = 742
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 376
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 451
    Height = 437
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 156
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridMuat: TcxDBEditorRow
      Properties.Caption = 'Asal *'
      Properties.DataBinding.FieldName = 'Muat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridBongkar: TcxDBEditorRow
      Properties.Caption = 'Tujuan *'
      Properties.DataBinding.FieldName = 'Bongkar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridJarak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Jarak'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridKategori: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kategori'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridLevelRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LevelRute'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridPoin: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Poin'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridPremiPengemudi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PremiPengemudi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridPremiKernet: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PremiKernet'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridPremiKondektur: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PremiKondektur'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridMel: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Mel'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridTol: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tol'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridUangJalanBesar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UangJalanBesar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridUangJalanKecil: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UangJalanKecil'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridUangBBM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UangBBM'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridUangMakan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UangMakan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridWaktu: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Waktu'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridStandarHargaMax: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandarHargaMax'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridStandarHarga: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandarHarga'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 536
    Width = 742
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 451
    Top = 48
    Width = 291
    Height = 437
    Align = alRight
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataSource = HargaDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Nama: TcxGridDBColumn
        DataBinding.FieldName = 'Nama'
        Width = 220
      end
      object cxGrid1DBTableView1Harga: TcxGridDBColumn
        DataBinding.FieldName = 'Harga'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from rute')
    UpdateObject = MasterUS
    Left = 289
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object MasterQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object MasterQPoin: TFloatField
      FieldName = 'Poin'
    end
    object MasterQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object MasterQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object MasterQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object MasterQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object MasterQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object MasterQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object MasterQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object MasterQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object MasterQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object MasterQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object MasterQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object MasterQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 250
    end
    object MasterQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 250
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 324
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Muat, Bongkar, Jarak, Kategori, LevelRute, Poin, Pr' +
        'emiPengemudi, PremiKernet, PremiKondektur, Mel, Tol, UangJalanBe' +
        'sar, UangJalanKecil, UangBBM, UangMakan, Waktu, StandarHargaMax,' +
        ' StandarHarga, CreateDate, CreateBy, Operator, TglEntry'
      'from rute'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update rute'
      'set'
      '  Kode = :Kode,'
      '  Muat = :Muat,'
      '  Bongkar = :Bongkar,'
      '  Jarak = :Jarak,'
      '  Kategori = :Kategori,'
      '  LevelRute = :LevelRute,'
      '  Poin = :Poin,'
      '  PremiPengemudi = :PremiPengemudi,'
      '  PremiKernet = :PremiKernet,'
      '  PremiKondektur = :PremiKondektur,'
      '  Mel = :Mel,'
      '  Tol = :Tol,'
      '  UangJalanBesar = :UangJalanBesar,'
      '  UangJalanKecil = :UangJalanKecil,'
      '  UangBBM = :UangBBM,'
      '  UangMakan = :UangMakan,'
      '  Waktu = :Waktu,'
      '  StandarHargaMax = :StandarHargaMax,'
      '  StandarHarga = :StandarHarga,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into rute'
      
        '  (Kode, Muat, Bongkar, Jarak, Kategori, LevelRute, Poin, PremiP' +
        'engemudi, PremiKernet, PremiKondektur, Mel, Tol, UangJalanBesar,' +
        ' UangJalanKecil, UangBBM, UangMakan, Waktu, StandarHargaMax, Sta' +
        'ndarHarga, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :Muat, :Bongkar, :Jarak, :Kategori, :LevelRute, :Poin,' +
        ' :PremiPengemudi, :PremiKernet, :PremiKondektur, :Mel, :Tol, :Ua' +
        'ngJalanBesar, :UangJalanKecil, :UangBBM, :UangMakan, :Waktu, :St' +
        'andarHargaMax, :StandarHarga, :CreateDate, :CreateBy, :Operator,' +
        ' :TglEntry)')
    DeleteSQL.Strings = (
      'delete from rute'
      'where'
      '  Kode = :OLD_Kode')
    Left = 356
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from rute order by kode desc')
    Left = 249
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai where UPPER (jabatan) = '#39'PENGEMUDI'#39)
    Left = 601
    Top = 7
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object SopirQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
  end
  object ACQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from masterac where kode like '#39'%'#39' + :text + '#39'%'#39' '
      '')
    Left = 641
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ACQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ACQMerk: TStringField
      FieldName = 'Merk'
      Size = 50
    end
    object ACQTipe: TStringField
      FieldName = 'Tipe'
      Size = 50
    end
  end
  object HargaRuteQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from hargarute where koderute = :text')
    UpdateObject = HargaRuteUS
    Left = 448
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HargaRuteQKodeRute: TStringField
      FieldName = 'KodeRute'
      Required = True
      Size = 10
    end
    object HargaRuteQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 200
    end
    object HargaRuteQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
  end
  object HargaRuteUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeRute, Nama, Harga'#13#10'from hargarute'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    ModifySQL.Strings = (
      'update hargarute'
      'set'
      '  KodeRute = :KodeRute,'
      '  Nama = :Nama,'
      '  Harga = :Harga'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    InsertSQL.Strings = (
      'insert into hargarute'
      '  (KodeRute, Nama, Harga)'
      'values'
      '  (:KodeRute, :Nama, :Harga)')
    DeleteSQL.Strings = (
      'delete from hargarute'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    Left = 520
    Top = 8
  end
  object HargaDs: TDataSource
    DataSet = HargaRuteQ
    Left = 484
    Top = 6
  end
  object DeleteHargaRuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from hargarute where koderute = :text')
    Left = 536
    Top = 496
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object ExecuteDeleteHargaRuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from hargarute where koderute = :text')
    UpdateObject = UpdateDeleteHargaRuteUS
    Left = 616
    Top = 496
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object UpdateDeleteHargaRuteUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeRute, Nama, Harga'#13#10'from hargarute'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    ModifySQL.Strings = (
      'update hargarute'
      'set'
      '  KodeRute = :KodeRute,'
      '  Nama = :Nama,'
      '  Harga = :Harga'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    InsertSQL.Strings = (
      'insert into hargarute'
      '  (KodeRute, Nama, Harga)'
      'values'
      '  (:KodeRute, :Nama, :Harga)')
    DeleteSQL.Strings = (
      'delete from hargarute'
      'where'
      '  KodeRute = :OLD_KodeRute and'
      '  Nama = :OLD_Nama and'
      '  Harga = :OLD_Harga')
    Left = 576
    Top = 496
  end
end
