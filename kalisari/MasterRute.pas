unit MasterRute;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxClasses, cxMRUEdit, cxLabel,
  cxCheckBox, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxGridCustomView, cxGrid, cxMemo;

type
  TMasterRuteFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    SopirQ: TSDQuery;
    ACQ: TSDQuery;
    HargaRuteQ: TSDQuery;
    cxLabel1: TcxLabel;
    SopirQKode: TStringField;
    SopirQNama: TStringField;
    SopirQJabatan: TStringField;
    ACQKode: TStringField;
    ACQMerk: TStringField;
    ACQTipe: TStringField;
    MasterQKode: TStringField;
    MasterQJarak: TIntegerField;
    MasterQKategori: TStringField;
    MasterQLevelRute: TStringField;
    MasterQPoin: TFloatField;
    MasterQPremiPengemudi: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQPremiKondektur: TCurrencyField;
    MasterQMel: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQUangJalanBesar: TCurrencyField;
    MasterQUangJalanKecil: TCurrencyField;
    MasterQUangBBM: TCurrencyField;
    MasterQUangMakan: TCurrencyField;
    MasterQWaktu: TIntegerField;
    MasterQStandarHargaMax: TCurrencyField;
    MasterQStandarHarga: TCurrencyField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterVGridMuat: TcxDBEditorRow;
    MasterVGridBongkar: TcxDBEditorRow;
    MasterVGridJarak: TcxDBEditorRow;
    MasterVGridKategori: TcxDBEditorRow;
    MasterVGridLevelRute: TcxDBEditorRow;
    MasterVGridPoin: TcxDBEditorRow;
    MasterVGridPremiPengemudi: TcxDBEditorRow;
    MasterVGridPremiKernet: TcxDBEditorRow;
    MasterVGridPremiKondektur: TcxDBEditorRow;
    MasterVGridMel: TcxDBEditorRow;
    MasterVGridTol: TcxDBEditorRow;
    MasterVGridUangJalanBesar: TcxDBEditorRow;
    MasterVGridUangJalanKecil: TcxDBEditorRow;
    MasterVGridUangBBM: TcxDBEditorRow;
    MasterVGridUangMakan: TcxDBEditorRow;
    MasterVGridWaktu: TcxDBEditorRow;
    MasterVGridStandarHargaMax: TcxDBEditorRow;
    MasterVGridStandarHarga: TcxDBEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    HargaRuteUS: TSDUpdateSQL;
    HargaRuteQKodeRute: TStringField;
    HargaRuteQNama: TStringField;
    HargaRuteQHarga: TCurrencyField;
    HargaDs: TDataSource;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    DeleteHargaRuteQ: TSDQuery;
    ExecuteDeleteHargaRuteQ: TSDQuery;
    UpdateDeleteHargaRuteUS: TSDUpdateSQL;
    MasterQMuat: TStringField;
    MasterQBongkar: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridSopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSopirEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridDBJenisACEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  MasterRuteFm: TMasterRuteFm;
  paramkode:string;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, ArmadaDropDown, RuteDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterRuteFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TMasterRuteFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterRuteFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterRuteFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterRuteFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterRuteFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  HargaRuteQ.Close;
  HargaRuteQ.ParamByName('text').AsString:=KodeEdit.Text;
  HargaRuteQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterRuteFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterRuteFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterRute.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterRute.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterRute.AsBoolean;
end;

procedure TMasterRuteFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterRute.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterRute.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TMasterRuteFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterRuteFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
      HargaRuteQ.Open;
      HargaRuteQ.First;
      While HargaRuteQ.Eof=false do
      begin
        HargaRuteQ.Edit;
        HargaRuteQKodeRute.AsString:=MasterQKode.AsString;
        HargaRuteQ.Post;
        HargaRuteQ.Next;
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    HargaRuteQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    HargaRuteQ.CommitUpdates;
    ShowMessage('Rute dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      HargaRuteQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterRuteFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;}
end;

procedure TMasterRuteFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterRuteFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Armada '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteHargaRuteQ.Close;
       DeleteHargaRuteQ.ParamByName('text').AsString:=KodeEdit.Text;
       DeleteHargaRuteQ.Open;
       DeleteHargaRuteQ.First;
       while DeleteHargaRuteQ.Eof=FALSE
       do
       begin
        ExecuteDeleteHargaRuteQ.SQL.Text:=('delete from HargaRute where KodeRute='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteHargaRuteQ.ExecSQL;
        DeleteHargaRuteQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Rute telah dihapus.',mtInformation,[mbOK],0);
     except
      on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DeleteHargaRuteQ.RollbackUpdates;
      MessageDlg('Barang pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
      end;
     end;
     KodeEdit.SetFocus;
  end;
  HargaRuteQ.Close;
  HargaRuteQ.ParamByName('text').AsString:='';
  HargaRuteQ.Open;
end;

procedure TMasterRuteFm.SearchBtnClick(Sender: TObject);
begin
    //MasterQ.SQL.Text:=MasterOriSQL;
    RuteDropDownFm:=TRuteDropdownfm.Create(Self);
    if RuteDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=RuteDropDownFm.kode;
      KodeEditExit(Sender);
      DeleteHargaRuteQ.Open;
      ExecuteDeleteHargaRuteQ.Open;
      MasterVGrid.SetFocus;
    end;
    RuteDropDownFm.Release;
    HargaRuteQ.Close;
    HargaRuteQ.ParamByName('text').AsString:=KodeEdit.Text;
    HargaRuteQ.Open;
end;

procedure TMasterRuteFm.MasterVGridSopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SopirQ.Close;
    //SopirQ.ParamByName('text').AsString:='';
    SopirQ.ExecSQL;
    SopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString:=SopirQKode.AsString;
      //MasterQDetailPengemudi.AsString:=SopirQNama.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterRuteFm.MasterVGridSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    {SopirQ.Close;
    //SopirQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SopirQ.ExecSQL;
    SopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSopir.AsString:=SopirQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;}
end;

procedure TMasterRuteFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TMasterRuteFm.MasterVGridEnter(Sender: TObject);
begin
// mastervgrid.FocusRow(MasterVGridPlatNo);
end;

procedure TMasterRuteFm.MasterVGridDBJenisACEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ACQ.Close;
    ACQ.ParamByName('text').AsString:='';
    ACQ.ExecSQL;
    ACQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ACQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
     { MasterQJenisAC.AsString:=ACQKode.AsString;
      MasterQMerkAC.AsString:=ACQMerk.AsString;
      MasterQTipeAC.AsString:=ACQTipe.AsString; }
     { if MasterQJenisAC.AsString <>'' then
      begin
        MasterQAC.AsBoolean:=true;
      end
      else
      begin
        MasterQAC.AsBoolean:=false;
      end;   }
      //MasterQEkor.AsString:=EkorQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TMasterRuteFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  If AbuttonIndex=10 then
  begin
    HargaRuteQKodeRute.AsString:=KodeEdit.Text;
    HargaRuteQ.Post;
  end;
end;

end.
