/*
   22 April 201317:10:24
   User: sa
   Server: TOSHIBA-PC\SQLEXPRESS
   Database: BungaDaru
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_Sopir
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_Sopir1
GO
ALTER TABLE dbo.Sopir SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Sopir', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Sopir', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Sopir', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_Jarak
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_Jarak1
GO
ALTER TABLE dbo.Jarak SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Jarak', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Jarak', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Jarak', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_Ekor
GO
ALTER TABLE dbo.Armada SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Armada', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Armada', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Armada', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_User1
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_User
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[User]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MasterSJ
	DROP CONSTRAINT FK_MasterSJ_MasterSO
GO
ALTER TABLE dbo.MasterSO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MasterSJ
	(
	Kodenota varchar(10) NOT NULL,
	Tgl datetime NOT NULL,
	NoSO varchar(10) NOT NULL,
	Sopir varchar(10) NULL,
	Sopir2 varchar(10) NULL,
	Crew varchar(50) NULL,
	Ekor varchar(10) NULL,
	Keterangan varchar(100) NULL,
	KirKepala bit NOT NULL,
	KirEkor bit NOT NULL,
	STNK bit NOT NULL,
	Pajak bit NOT NULL,
	xNama varchar(50) NULL,
	xTgl datetime NULL,
	xNoSJ varchar(20) NULL,
	TglKembali datetime NULL,
	Status varchar(50) NOT NULL,
	CreateDate datetime NULL,
	CreateBy varchar(10) NULL,
	Operator varchar(10) NULL,
	TglEntry datetime NULL,
	Laporan varchar(MAX) NULL,
	TglRealisasi datetime NULL,
	RealisasiTonaseMuat numeric(18, 4) NULL,
	RealisasiTonase numeric(18, 4) NULL,
	RealisasiTonaseBongkar numeric(18, 4) NULL,
	Awal varchar(10) NULL,
	Akhir varchar(10) NULL,
	TglCetak datetime NULL,
	ClaimSopir money NULL,
	KeteranganClaimSopir varchar(50) NULL,
	UangTambahanSopir money NULL,
	KeteranganUangTambahanSopir varchar(50) NULL,
	KomisiSopir money NULL,
	KomisiKernet money NULL,
	mel money NULL,
	tol money NULL,
	UangJalan money NULL,
	UangBongkarMuat money NULL,
	UangBBM money NULL,
	RealisasiBBM money NULL,
	UangMakan money NULL,
	UangTimbangan money NULL,
	UangOrganda money NULL,
	TabunganSopir money NULL,
	BiayaLainLain money NULL,
	KeteranganBiayaLainLain varchar(50) NULL,
	Other money NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_MasterSJ SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.MasterSJ)
	 EXEC('INSERT INTO dbo.Tmp_MasterSJ (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, Ekor, Keterangan, KirKepala, KirEkor, STNK, Pajak, xNama, xTgl, xNoSJ, TglKembali, Status, CreateDate, CreateBy, Operator, TglEntry, Laporan, TglRealisasi, RealisasiTonaseMuat, RealisasiTonase, Awal, Akhir, TglCetak, ClaimSopir, KeteranganClaimSopir, UangTambahanSopir, KeteranganUangTambahanSopir, KomisiSopir, KomisiKernet, mel, tol, UangJalan, UangBongkarMuat, UangBBM, RealisasiBBM, UangMakan, UangTimbangan, UangOrganda, TabunganSopir, BiayaLainLain, KeteranganBiayaLainLain, Other)
		SELECT Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, Ekor, Keterangan, KirKepala, KirEkor, STNK, Pajak, xNama, xTgl, xNoSJ, TglKembali, Status, CreateDate, CreateBy, Operator, TglEntry, Laporan, TglRealisasi, RealisasiTonaseMuat, RealisasiTonase, Awal, Akhir, TglCetak, ClaimSopir, KeteranganClaimSopir, UangTambahanSopir, KeteranganUangTambahanSopir, KomisiSopir, KomisiKernet, mel, tol, UangJalan, UangBongkarMuat, UangBBM, RealisasiBBM, UangMakan, UangTimbangan, UangOrganda, TabunganSopir, BiayaLainLain, KeteranganBiayaLainLain, Other FROM dbo.MasterSJ WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.BonBBM
	DROP CONSTRAINT FK_BonBBM_MasterSJ
GO
ALTER TABLE dbo.BonSopir
	DROP CONSTRAINT FK_BonSopir_MasterSJ
GO
ALTER TABLE dbo.KeluhanPelanggan
	DROP CONSTRAINT FK_KeluhanPelanggan_MasterSJ
GO
ALTER TABLE dbo.MemuatSJ
	DROP CONSTRAINT FK_MemuatSJ_MasterSJ
GO
ALTER TABLE dbo.Penagihan
	DROP CONSTRAINT FK_penagihan_MasterSJ
GO
ALTER TABLE dbo.Storing
	DROP CONSTRAINT FK_Storing_MasterSJ
GO
DROP TABLE dbo.MasterSJ
GO
EXECUTE sp_rename N'dbo.Tmp_MasterSJ', N'MasterSJ', 'OBJECT' 
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	PK_MasterSJ PRIMARY KEY CLUSTERED 
	(
	Kodenota
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_MasterSO FOREIGN KEY
	(
	NoSO
	) REFERENCES dbo.MasterSO
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_User1 FOREIGN KEY
	(
	Operator
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_Ekor FOREIGN KEY
	(
	Ekor
	) REFERENCES dbo.Armada
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_Jarak FOREIGN KEY
	(
	Awal
	) REFERENCES dbo.Jarak
	(
	kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_Jarak1 FOREIGN KEY
	(
	Akhir
	) REFERENCES dbo.Jarak
	(
	kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_Sopir FOREIGN KEY
	(
	Sopir
	) REFERENCES dbo.Sopir
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_Sopir1 FOREIGN KEY
	(
	Sopir2
	) REFERENCES dbo.Sopir
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MasterSJ ADD CONSTRAINT
	FK_MasterSJ_User FOREIGN KEY
	(
	CreateBy
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE trigger UbahKM
on dbo.MasterSJ
for UPDATE
as
declare @TglKembaliIns datetime;
declare @TglKembaliDel datetime;
declare @KodeSO varchar(10);
declare @KodeRute varchar(10);
declare @Jarak int;
declare @KmLama int;
declare @KmBaru int;
declare @KodeArmada varchar(10);
set @TglKembaliIns=(select i.TglKembali from inserted i);
set @TglKembaliDel=(select d.TglKembali from deleted d);
set @KodeSO=(select i.NoSO from inserted i);
if @TglKembaliIns is not NULL
begin
set @KodeRute=(select Rute from MasterSO where Kodenota=@KodeSO);
set @Jarak=(select Jarak from Rute where Kode=@KodeRute);
if @Jarak is NULL
begin
set @Jarak=0;
end
	if @TglKembaliDel is NULL
	begin
		set @KodeArmada=(select Armada from MasterSO where Kodenota=@KodeSO);
		set @KmLama=(select KmSekarang from Armada where Kode=@KodeArmada);
		if @KmLama is NULL 
		begin
			set @KmLama=0;
		end
		set @KmBaru=@KmLama+@Jarak;
		update Armada set KmSekarang=@KmBaru where Kode=@KodeArmada
	end
	
end
GO
CREATE TRIGGER [dbo].[MASTERSJ_UPDATE] ON dbo.MasterSJ
for UPDATE
AS
BEGIN
	update MASTERSJ set TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
CREATE TRIGGER [dbo].[MASTERSJ_INSERT] ON dbo.MasterSJ
for INSERT
AS
BEGIN
	update MASTERSJ set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MasterSJ', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MasterSJ', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MasterSJ', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Storing ADD CONSTRAINT
	FK_Storing_MasterSJ FOREIGN KEY
	(
	SuratJalan
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Storing SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Storing', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Storing', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Storing', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	FK_penagihan_MasterSJ FOREIGN KEY
	(
	SuratJalan
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Penagihan SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.MemuatSJ ADD CONSTRAINT
	FK_MemuatSJ_MasterSJ FOREIGN KEY
	(
	KodeSJ
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.MemuatSJ SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MemuatSJ', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MemuatSJ', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MemuatSJ', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.KeluhanPelanggan ADD CONSTRAINT
	FK_KeluhanPelanggan_MasterSJ FOREIGN KEY
	(
	SuratJalan
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.KeluhanPelanggan SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.KeluhanPelanggan', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.KeluhanPelanggan', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.KeluhanPelanggan', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.BonSopir ADD CONSTRAINT
	FK_BonSopir_MasterSJ FOREIGN KEY
	(
	SuratJalan
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.BonSopir SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.BonSopir', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.BonSopir', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.BonSopir', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.BonBBM ADD CONSTRAINT
	FK_BonBBM_MasterSJ FOREIGN KEY
	(
	SuratJalan
	) REFERENCES dbo.MasterSJ
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.BonBBM SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.BonBBM', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.BonBBM', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.BonBBM', 'Object', 'CONTROL') as Contr_Per 