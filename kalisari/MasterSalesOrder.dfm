object MasterSalesOrderFm: TMasterSalesOrderFm
  Left = 426
  Top = 212
  Width = 607
  Height = 406
  Caption = 'Master Sales Order'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 297
    Width = 591
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 591
    Height = 249
    Align = alClient
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTgl: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ClearKey = 46
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'Tgl'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPelangganEditPropertiesButtonClick
      Properties.EditProperties.OnValidate = MasterVGridPelangganEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Pelanggan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridTglOrder: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.DateOnError = deToday
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'TglOrder'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridJenisBarang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JenisBarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridPanjang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Panjang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridBerat: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Berat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridRuteEditPropertiesButtonClick
      Properties.EditProperties.OnValidate = MasterVGridRuteEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Rute'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridHarga: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.DisplayFormat = 'Rp,0.00;(Rp,0.00)'
      Properties.DataBinding.FieldName = 'Harga'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 3
      Properties.EditProperties.DefaultValue = 1
      Properties.EditProperties.Items = <
        item
          Caption = 'Deal'
          Value = 1
        end
        item
          Caption = 'Follow Up'
          Value = 2
        end
        item
          Caption = 'Batal'
          Value = 0
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridTglFollowUp: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ClearKey = 46
      Properties.EditProperties.DateButtons = []
      Properties.EditProperties.DateOnError = deNull
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'TglFollowUp'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridArmadaEditPropertiesButtonClick
      Properties.EditProperties.OnValidate = MasterVGridArmadaEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Armada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 348
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from masterso')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
    end
    object MasterQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Size = 100
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object MasterQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object MasterQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object MasterQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Armada, Berat, CreateBy, CreateDate, Harga, JenisBarang, ' +
        'Keterangan, Kodenota, Operator, Panjang, Pelanggan, Rute, Status' +
        ', Tgl, TglEntry, TglFollowUp, TglOrder'#13#10'from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update masterso'
      'set'
      '  Armada = :Armada,'
      '  Berat = :Berat,'
      '  CreateBy = :CreateBy,'
      '  CreateDate = :CreateDate,'
      '  Harga = :Harga,'
      '  JenisBarang = :JenisBarang,'
      '  Keterangan = :Keterangan,'
      '  Kodenota = :Kodenota,'
      '  Operator = :Operator,'
      '  Panjang = :Panjang,'
      '  Pelanggan = :Pelanggan,'
      '  Rute = :Rute,'
      '  Status = :Status,'
      '  Tgl = :Tgl,'
      '  TglEntry = :TglEntry,'
      '  TglFollowUp = :TglFollowUp,'
      '  TglOrder = :TglOrder'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into masterso'
      
        '  (Armada, Berat, CreateBy, CreateDate, Harga, JenisBarang, Kete' +
        'rangan, Kodenota, Operator, Panjang, Pelanggan, Rute, Status, Tg' +
        'l, TglEntry, TglFollowUp, TglOrder)'
      'values'
      
        '  (:Armada, :Berat, :CreateBy, :CreateDate, :Harga, :JenisBarang' +
        ', :Keterangan, :Kodenota, :Operator, :Panjang, :Pelanggan, :Rute' +
        ', :Status, :Tgl, :TglEntry, :TglFollowUp, :TglOrder)')
    DeleteSQL.Strings = (
      'delete from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 452
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from masterso order by kodenota desc')
    Left = 289
    Top = 7
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'a like '#39'%'#39' + :text + '#39'%'#39
      '')
    Left = 385
    Top = 151
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 12
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 481
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQBorongan: TCurrencyField
      FieldName = 'Borongan'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 505
    Top = 287
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object ArmadaQKapasitas: TFloatField
      FieldName = 'Kapasitas'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object ArmadaQEkor: TStringField
      FieldName = 'Ekor'
      Required = True
      Size = 10
    end
  end
end
