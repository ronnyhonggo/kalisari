object MasterSopirFm: TMasterSopirFm
  Left = 427
  Top = 256
  Width = 500
  Height = 318
  AutoSize = True
  Caption = 'Master Sopir'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 492
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 214
    Width = 492
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 400
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 492
    Height = 166
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.Caption = 'Nama*'
      Properties.DataBinding.FieldName = 'Nama'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridNotelp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Notelp'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridNoKTP: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoKTP'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridNoSIM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoSIM'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridExpiredSIM: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ExpiredSIM'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 265
    Width = 492
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from sopir')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object MasterQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Required = True
      Visible = False
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Required = True
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
    object MasterQNoSIM: TStringField
      FieldName = 'NoSIM'
      Size = 50
    end
    object MasterQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Alamat, Notelp, CreateDate, CreateBy, Operato' +
        'r, TglEntry, NoKTP, NoSIM, ExpiredSIM'
      'from sopir'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update sopir'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Alamat = :Alamat,'
      '  Notelp = :Notelp,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  NoKTP = :NoKTP,'
      '  NoSIM = :NoSIM,'
      '  ExpiredSIM = :ExpiredSIM'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into sopir'
      
        '  (Kode, Nama, Alamat, Notelp, CreateDate, CreateBy, Operator, T' +
        'glEntry, NoKTP, NoSIM, ExpiredSIM)'
      'values'
      
        '  (:Kode, :Nama, :Alamat, :Notelp, :CreateDate, :CreateBy, :Oper' +
        'ator, :TglEntry, :NoKTP, :NoSIM, :ExpiredSIM)')
    DeleteSQL.Strings = (
      'delete from sopir'
      'where'
      '  Kode = :OLD_Kode')
    Left = 436
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from sopir order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
