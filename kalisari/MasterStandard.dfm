object MasterStandardFm: TMasterStandardFm
  Left = 458
  Top = 203
  Width = 768
  Height = 286
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Standard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 752
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 177
    Width = 752
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 336
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 417
    Height = 129
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 146
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridJenisKendaraan: TcxDBEditorRow
      Properties.Caption = 'JenisKendaraan *'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridJenisKendaraanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'JenisKendaraan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridDetailKendaraan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailKendaraan'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridTipePerawatan: TcxDBEditorRow
      Properties.Caption = 'TipePerawatan *'
      Properties.DataBinding.FieldName = 'TipePerawatan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridPeriodeWaktu: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PeriodeWaktu'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridPeriodeKm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PeriodeKm'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridStandardWaktu: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandardWaktu'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 228
    Width = 752
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 418
    Top = 48
    Width = 334
    Height = 129
    Align = alRight
    TabOrder = 4
    OnExit = cxGrid1Exit
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Barang: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1BarangPropertiesButtonClick
      end
      object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Styles.Content = MenuUtamaFm.cxStyle5
        Width = 151
      end
      object cxGrid1DBTableView1JumlahDibutuhkan: TcxGridDBColumn
        DataBinding.FieldName = 'JumlahDibutuhkan'
        Width = 94
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
      Options.DetailTabsPosition = dtpLeft
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from standarperawatan')
    UpdateObject = MasterUS
    Left = 361
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object MasterQTipePerawatan: TStringField
      FieldName = 'TipePerawatan'
      Required = True
      Size = 50
    end
    object MasterQPeriodeWaktu: TIntegerField
      FieldName = 'PeriodeWaktu'
    end
    object MasterQPeriodeKm: TIntegerField
      FieldName = 'PeriodeKm'
    end
    object MasterQStandardWaktu: TIntegerField
      FieldName = 'StandardWaktu'
    end
    object MasterQDetailKendaraan: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailKendaraan'
      LookupDataSet = JenisQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaJenis'
      KeyFields = 'JenisKendaraan'
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, JenisKendaraan, DetailKendaraan, TipePerawatan, Per' +
        'iodeWaktu, PeriodeKm, StandardWaktu'
      'from standarperawatan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update standarperawatan'
      'set'
      '  Kode = :Kode,'
      '  JenisKendaraan = :JenisKendaraan,'
      '  TipePerawatan = :TipePerawatan,'
      '  PeriodeWaktu = :PeriodeWaktu,'
      '  PeriodeKm = :PeriodeKm,'
      '  StandardWaktu = :StandardWaktu'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into standarperawatan'
      
        '  (Kode, JenisKendaraan, TipePerawatan, PeriodeWaktu, PeriodeKm,' +
        ' StandardWaktu)'
      'values'
      
        '  (:Kode, :JenisKendaraan, :TipePerawatan, :PeriodeWaktu, :Perio' +
        'deKm, :StandardWaktu)')
    DeleteSQL.Strings = (
      'delete from standarperawatan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 444
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from standarperawatan order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      
        'select d.* from detailstandard d, standarperawatan s where s.kod' +
        'e=d.kodestandard and s.kode= :text')
    UpdateObject = SDUpdateSQL1
    Left = 265
    Top = 9
    ParamData = <
      item
        DataType = ftInteger
        Name = 'text'
        ParamType = ptInput
        Value = '0'
      end>
    object DetailQKodeStandard: TStringField
      FieldName = 'KodeStandard'
      Required = True
      Size = 10
    end
    object DetailQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object DetailQJumlahDibutuhkan: TFloatField
      FieldName = 'JumlahDibutuhkan'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 484
    Top = 6
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from barang')
    Left = 569
    Top = 9
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object DetailStandardQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from detailbarang')
    Left = 617
    Top = 9
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object MemoField1: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object MemoField2: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailStandardQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailStandardQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from detailstandard order by kode desc')
    Left = 321
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeStandard, Barang, JumlahDibutuhkan'#13#10'from detailstanda' +
        'rd'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update detailstandard'
      'set'
      '  KodeStandard = :KodeStandard,'
      '  Barang = :Barang,'
      '  JumlahDibutuhkan = :JumlahDibutuhkan'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into detailstandard'
      '  (KodeStandard, Barang, JumlahDibutuhkan)'
      'values'
      '  (:KodeStandard, :Barang, :JumlahDibutuhkan)')
    DeleteSQL.Strings = (
      'delete from detailstandard'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    Left = 524
    Top = 10
  end
  object JenisQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 233
    Top = 9
    object JenisQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailstandard where kodestandard=:text')
    Left = 528
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailStandardQKodeStandard: TStringField
      FieldName = 'KodeStandard'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQJumlahDibutuhkan: TFloatField
      FieldName = 'JumlahDibutuhkan'
      Required = True
    end
  end
  object ExecuteDeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailstandard where kodestandard=:text')
    UpdateObject = UpdateDeleteDetailStandardUS
    Left = 592
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object UpdateDeleteDetailStandardUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeStandard, Barang, JumlahDibutuhkan'#13#10'from detailstanda' +
        'rd'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update detailstandard'
      'set'
      '  KodeStandard = :KodeStandard,'
      '  Barang = :Barang,'
      '  JumlahDibutuhkan = :JumlahDibutuhkan'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into detailstandard'
      '  (KodeStandard, Barang, JumlahDibutuhkan)'
      'values'
      '  (:KodeStandard, :Barang, :JumlahDibutuhkan)')
    DeleteSQL.Strings = (
      'delete from detailstandard'
      'where'
      '  KodeStandard = :OLD_KodeStandard and'
      '  Barang = :OLD_Barang')
    Left = 560
    Top = 184
  end
end
