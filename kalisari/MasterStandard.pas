unit MasterStandard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel, cxCheckBox;

type
  TMasterStandardFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    DetailQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    DataSource1: TDataSource;
    BarangQ: TSDQuery;
    DetailStandardQ: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    MemoField1: TMemoField;
    MemoField2: TMemoField;
    Kode2Q: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    Kode2Qkode: TStringField;
    MasterQKode: TStringField;
    MasterQJenisKendaraan: TStringField;
    MasterQTipePerawatan: TStringField;
    MasterQPeriodeWaktu: TIntegerField;
    MasterQPeriodeKm: TIntegerField;
    MasterQStandardWaktu: TIntegerField;
    JenisQ: TSDQuery;
    JenisQKode: TStringField;
    JenisQNamaJenis: TStringField;
    JenisQTipe: TMemoField;
    JenisQKeterangan: TMemoField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    cxLabel1: TcxLabel;
    MasterVGridJenisKendaraan: TcxDBEditorRow;
    MasterVGridTipePerawatan: TcxDBEditorRow;
    MasterVGridPeriodeWaktu: TcxDBEditorRow;
    MasterVGridPeriodeKm: TcxDBEditorRow;
    MasterVGridStandardWaktu: TcxDBEditorRow;
    MasterVGridDetailKendaraan: TcxDBEditorRow;
    MasterQDetailKendaraan: TStringField;
    DetailStandardQKodeBarang: TStringField;
    DetailStandardQKodeJenisKendaraan: TStringField;
    DetailQKodeStandard: TStringField;
    DetailQBarang: TStringField;
    DetailQNamaBarang: TStringField;
    cxGrid1DBTableView1Barang: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    DeleteDetailStandardQ: TSDQuery;
    ExecuteDeleteDetailStandardQ: TSDQuery;
    UpdateDeleteDetailStandardUS: TSDUpdateSQL;
    DeleteDetailStandardQKodeStandard: TStringField;
    DeleteDetailStandardQBarang: TStringField;
    BarangQJumlah: TFloatField;
    DeleteDetailStandardQJumlahDibutuhkan: TFloatField;
    DetailQJumlahDibutuhkan: TFloatField;
    cxGrid1DBTableView1JumlahDibutuhkan: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridJenisKendaraanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure DetailQAfterDelete(DataSet: TDataSet);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterStandardFm: TMasterStandardFm;

  MasterOriSQL, DetailOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, StandarPerawatanDropDown, JenisKendaraanDropDown,
  BarangDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterStandardFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterStandardFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterStandardFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterStandardFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  JenisQ.Open;
//  DetailQ.Open;
  BarangQ.Open;  
end;

procedure TMasterStandardFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  detailq.Close;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterStandardFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterStandardFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterStandardPerawatan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterStandardPerawatan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterStandardPerawatan.AsBoolean;
end;

procedure TMasterStandardFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterStandardPerawatan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterStandardPerawatan.AsBoolean;
    MasterVGrid.Enabled:=True;
    DetailQ.Close;
    DetailQ.ParamByName('text').AsString:='';
    detailq.Open;
  end;

end;

procedure TMasterStandardFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterStandardFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    DetailQ.Open;
    DetailQ.Edit;
    DetailQ.First;
    while DetailQ.Eof=FALSE do
    begin
        DetailQ.Edit;
        DetailQKodeStandard.AsString:=KodeEdit.Text;
        detailq.Post;
        DetailQ.Next;
    end;
    //DetailQ.ApplyUpdates;
    DetailQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
   // DetailQ.CommitUpdates;
    ShowMessage('Standard dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    //KodeEdit.SetFocus;
   FormShow(self);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  DetailQ.Close;
  DetailQ.ParamByName('text').AsString:='';
  detailq.Open;
  DetailQ.Close;
end;

procedure TMasterStandardFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterStandardFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;

end;

procedure TMasterStandardFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Standard '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DeleteDetailStandardQ.Close;
       DeleteDetailStandardQ.ParamByName('text').AsString:=KodeEdit.Text;
       DeleteDetailStandardQ.Open;
       DeleteDetailStandardQ.First;
       while DeleteDetailStandardQ.Eof=FALSE
       do
       begin
        ExecuteDeleteDetailStandardQ.SQL.Text:=('delete from DetailStandard where KodeStandard='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteDetailStandardQ.ExecSQL;
        DeleteDetailStandardQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Standard telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DeleteDetailStandardQ.RollbackUpdates;
      MessageDlg('Standart pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
  DetailQ.Close;
  DetailQ.ParamByName('text').AsString:='';
  DetailQ.Open;
end;

procedure TMasterStandardFm.SearchBtnClick(Sender: TObject);
begin
    //MasterQ.SQL.Text:=MasterOriSQL;
    StandarPerawatanDropDownFm:=TStandarPerawatanDropdownfm.Create(Self,'all');
    if StandarPerawatanDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=StandarPerawatanDropDownFm.kode;
      KodeEditExit(Sender);
      detailq.Close;
      detailq.ParamByName('text').AsString := MasterQKode.AsString;
      detailq.Open;
      MasterVGrid.SetFocus;
    end;
    StandarPerawatanDropDownFm.Release;
end;

procedure TMasterStandardFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TMasterStandardFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridJenisKendaraan);
end;

procedure TMasterStandardFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  //ShowMessage(inttostr(abuttonindex));
  if abuttonindex=10 then
  begin
  DetailQ.Edit;
  DetailQ.Post;
  end;
   { MenuUtamaFm.Database1.StartTransaction;
    try
      //ShowMessage(DetailQKodeStandard.AsString+DetailQBarang.AsString+DetailQJumlahDibutuhkan.AsString);
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
          // on E : Exception do
            //  ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
  end
  else if AButtonIndex=8 then
  begin
    //detailq.Delete;
    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
  end;}
end;

procedure TMasterStandardFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.Navigator.Buttons.Post.Click;
end;

procedure TMasterStandardFm.cxGrid1DBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  detailq.ParamByName('text').AsString:=masterqkode.AsString;
  detailq.Open;
  detailq.Insert;

  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    DetailQBarang.AsString:= BarangDropDownFm.kode;
  end;
  DetailQKodeStandard.AsString:=masterqkode.AsString;
  BarangDropDownFm.Release;
{
  DropDownFm:=TDropdownfm.Create(Self,BarangQ);
  if DropDownFm.ShowModal=MrOK then
  begin
    cxGrid1DBTableView1.DataController.setvalue(cxGrid1DBTableView1.DataController.FocusedDataRowIndex,0,barangqkode.AsString);
    detailqbarang.AsString:=barangqkode.AsString;
    cxGrid1DBTableView1.DataController.setvalue(cxGrid1DBTableView1.DataController.FocusedDataRowIndex,1,barangqnama.AsString);
    detailqnama.AsString:=barangqnama.AsString;
  end;
  DetailQKodeStandard.AsString:=masterqkode.AsString;
  DropDownFm.Release;    }
end;

procedure TMasterStandardFm.MasterVGridJenisKendaraanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //DropDownFm:=TDropdownfm.Create(Self,JenisQ);
  //if DropDownFm.ShowModal=MrOK then
  //begin
    {JenisQ.Close;
    //KategoriQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    JenisQ.ExecSQL;
    JenisQ.Open;}
    JenisKendaraanDropDownFm:=TJenisKendaraanDropdownfm.Create(Self);
    if JenisKendaraanDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQJenisKendaraan.AsString:=JenisKendaraanDropDownFm.kode;
      //MasterQDetailKendaraan.AsString:=JenisQNamaJenis.AsString;
      //MasterVGrid.SetFocus;
      //end:
  end;
  JenisKendaraanDropDownFm.Release;
end;

procedure TMasterStandardFm.DetailQAfterDelete(DataSet: TDataSet);
begin
  MenuUtamaFm.Database1.StartTransaction;
    try
      DetailQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailQ.CommitUpdates;
    except
      MenuUtamaFm.Database1.Rollback;
      DetailQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
      DetailQ.Close;
      detailq.SQL.Text:=DetailOriSQL;
      detailq.ExecSQL;
      detailq.Open;
    end;
end;

procedure TMasterStandardFm.ExitBtnClick(Sender: TObject);
begin
  release;
end;

procedure TMasterStandardFm.cxGrid1DBTableView1BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  detailq.ParamByName('text').AsString:=masterqkode.AsString;
  detailq.Open;
  detailq.Insert;

  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    DetailQBarang.AsString:= BarangDropDownFm.kode;
  end;
  DetailQKodeStandard.AsString:=masterqkode.AsString;
  BarangDropDownFm.Release;
end;

end.
