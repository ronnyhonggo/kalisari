object MasterSupplierFm: TMasterSupplierFm
  Left = 506
  Top = 204
  Width = 517
  Height = 544
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Supplier'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 435
    Width = 501
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 424
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 501
    Height = 387
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 170
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNamaToko: TcxDBEditorRow
      Properties.Caption = 'NamaToko *'
      Properties.DataBinding.FieldName = 'NamaToko'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.Caption = 'Alamat *'
      Properties.DataBinding.FieldName = 'Alamat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridNoTelp: TcxDBEditorRow
      Properties.Caption = 'NoTelp *'
      Properties.DataBinding.FieldName = 'NoTelp'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridFax: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Fax'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridEmail: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Email'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridKategori: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kategori'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridStandarTermOfPayment: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'StandarTermOfPayment'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridCashNCarry: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'CashNCarry'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridNPWP: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NPWP'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridNamaPIC1: TcxDBEditorRow
      Properties.Caption = 'NamaPIC1 *'
      Properties.DataBinding.FieldName = 'NamaPIC1'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridTelpPIC1: TcxDBEditorRow
      Properties.Caption = 'TelpPIC1 *'
      Properties.DataBinding.FieldName = 'TelpPIC1'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridJabatanPIC1: TcxDBEditorRow
      Properties.Caption = 'JabatanPIC1 *'
      Properties.DataBinding.FieldName = 'JabatanPIC1'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridNamaPIC2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPIC2'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridTelpPIC2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpPIC2'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridJabatanPIC2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanPIC2'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridNamaPIC3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPIC3'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridTelpPIC3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpPIC3'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridJabatanPIC3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanPIC3'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 486
    Width = 501
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from supplier')
    UpdateObject = MasterUS
    Left = 273
    Top = 17
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object MasterQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object MasterQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object MasterQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object MasterQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object MasterQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object MasterQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object MasterQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object MasterQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object MasterQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object MasterQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object MasterQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object MasterQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object MasterQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object MasterQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object MasterQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, NamaToko, Alamat, NoTelp, Fax, Email, Kategori, Sta' +
        'ndarTermOfPayment, CashNCarry, NPWP, NamaPIC1, TelpPIC1, Jabatan' +
        'PIC1, NamaPIC2, TelpPIC2, JabatanPIC2, NamaPIC3, TelpPIC3, Jabat' +
        'anPIC3, CreateDate, CreateBy, Operator, TglEntry'
      'from supplier'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update supplier'
      'set'
      '  Kode = :Kode,'
      '  NamaToko = :NamaToko,'
      '  Alamat = :Alamat,'
      '  NoTelp = :NoTelp,'
      '  Fax = :Fax,'
      '  Email = :Email,'
      '  Kategori = :Kategori,'
      '  StandarTermOfPayment = :StandarTermOfPayment,'
      '  CashNCarry = :CashNCarry,'
      '  NPWP = :NPWP,'
      '  NamaPIC1 = :NamaPIC1,'
      '  TelpPIC1 = :TelpPIC1,'
      '  JabatanPIC1 = :JabatanPIC1,'
      '  NamaPIC2 = :NamaPIC2,'
      '  TelpPIC2 = :TelpPIC2,'
      '  JabatanPIC2 = :JabatanPIC2,'
      '  NamaPIC3 = :NamaPIC3,'
      '  TelpPIC3 = :TelpPIC3,'
      '  JabatanPIC3 = :JabatanPIC3,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into supplier'
      
        '  (Kode, NamaToko, Alamat, NoTelp, Fax, Email, Kategori, Standar' +
        'TermOfPayment, CashNCarry, NPWP, NamaPIC1, TelpPIC1, JabatanPIC1' +
        ', NamaPIC2, TelpPIC2, JabatanPIC2, NamaPIC3, TelpPIC3, JabatanPI' +
        'C3, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :NamaToko, :Alamat, :NoTelp, :Fax, :Email, :Kategori, ' +
        ':StandarTermOfPayment, :CashNCarry, :NPWP, :NamaPIC1, :TelpPIC1,' +
        ' :JabatanPIC1, :NamaPIC2, :TelpPIC2, :JabatanPIC2, :NamaPIC3, :T' +
        'elpPIC3, :JabatanPIC3, :CreateDate, :CreateBy, :Operator, :TglEn' +
        'try)')
    DeleteSQL.Strings = (
      'delete from supplier'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from supplier order by kode desc')
    Left = 337
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
