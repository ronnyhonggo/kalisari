unit MasterSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxLabel;

type
  TMasterSupplierFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    cxLabel1: TcxLabel;
    MasterQKode: TStringField;
    MasterQNamaToko: TStringField;
    MasterQAlamat: TStringField;
    MasterQNoTelp: TStringField;
    MasterQFax: TStringField;
    MasterQEmail: TStringField;
    MasterQKategori: TStringField;
    MasterQStandarTermOfPayment: TStringField;
    MasterQCashNCarry: TBooleanField;
    MasterQNPWP: TStringField;
    MasterQNamaPIC1: TStringField;
    MasterQTelpPIC1: TStringField;
    MasterQJabatanPIC1: TStringField;
    MasterQNamaPIC2: TStringField;
    MasterQTelpPIC2: TStringField;
    MasterQJabatanPIC2: TStringField;
    MasterQNamaPIC3: TStringField;
    MasterQTelpPIC3: TStringField;
    MasterQJabatanPIC3: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterVGridNamaToko: TcxDBEditorRow;
    MasterVGridAlamat: TcxDBEditorRow;
    MasterVGridNoTelp: TcxDBEditorRow;
    MasterVGridFax: TcxDBEditorRow;
    MasterVGridEmail: TcxDBEditorRow;
    MasterVGridKategori: TcxDBEditorRow;
    MasterVGridStandarTermOfPayment: TcxDBEditorRow;
    MasterVGridCashNCarry: TcxDBEditorRow;
    MasterVGridNPWP: TcxDBEditorRow;
    MasterVGridNamaPIC1: TcxDBEditorRow;
    MasterVGridTelpPIC1: TcxDBEditorRow;
    MasterVGridJabatanPIC1: TcxDBEditorRow;
    MasterVGridNamaPIC2: TcxDBEditorRow;
    MasterVGridTelpPIC2: TcxDBEditorRow;
    MasterVGridJabatanPIC2: TcxDBEditorRow;
    MasterVGridNamaPIC3: TcxDBEditorRow;
    MasterVGridTelpPIC3: TcxDBEditorRow;
    MasterVGridJabatanPIC3: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterSupplierFm: TMasterSupplierFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, SupplierDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterSupplierFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterSupplierFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterSupplierFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterSupplierFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterSupplierFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterSupplierFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterSupplierFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterSupplierFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterSupplier.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterSupplier.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterSupplier.AsBoolean;
end;

procedure TMasterSupplierFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQCashNCarry.AsBoolean :=false;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterSupplier.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterSupplier.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TMasterSupplierFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterSupplierFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Supplier dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterSupplierFm.MasterQAfterInsert(DataSet: TDataSet);
begin
//  DMFm.GetDateQ.Open;
//  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
//  DMFm.GetDateQ.Close;
end;

procedure TMasterSupplierFm.MasterQBeforePost(DataSet: TDataSet);
begin
//  DMFm.GetDateQ.Open;
//  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
//  DMFm.GetDateQ.Close;
end;

procedure TMasterSupplierFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Supplier '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Supplier telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Supplier pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterSupplierFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
    if SupplierDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=SupplierDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    SupplierDropDownFm.Release;
end;

procedure TMasterSupplierFm.MasterVGridEnter(Sender: TObject);
begin
  MasterVGrid.FocusRow(MasterVGridNamaToko);
end;

end.
