object MasterSuratJalanFm: TMasterSuratJalanFm
  Left = 659
  Top = 231
  Width = 580
  Height = 516
  Caption = 'Master Surat Jalan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 564
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 407
    Width = 564
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 564
    Height = 359
    Align = alClient
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTgl: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ClearKey = 46
      Properties.EditProperties.DateOnError = deToday
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'Tgl'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNoSO: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridNoSOEditPropertiesButtonClick
      Properties.EditProperties.OnValidate = MasterVGridNoSOEditPropertiesValidate
      Properties.DataBinding.FieldName = 'NoSO'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridCrew: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Crew'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridKirKepala: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirKepala'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridKirEkor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirEkor'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridSTNK: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'STNK'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridPajak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pajak'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridTali: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Tali'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridRantai: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Rantai'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridKayu: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'Kayu'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridxNama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'xNama'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridxTgl: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ClearKey = 46
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'xTgl'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridxNoSJ: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'xNoSJ'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridNamaMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaMuat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridTelpMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpMuat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridNamaBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaBongkar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridTelpBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpBongkar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
    object MasterVGridLaporan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Laporan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 18
      ParentID = -1
      Index = 18
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 458
    Width = 564
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from mastersj')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQKirKepala: TBooleanField
      FieldName = 'KirKepala'
      Required = True
    end
    object MasterQKirEkor: TBooleanField
      FieldName = 'KirEkor'
      Required = True
    end
    object MasterQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object MasterQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object MasterQTali: TIntegerField
      FieldName = 'Tali'
      Required = True
    end
    object MasterQRantai: TIntegerField
      FieldName = 'Rantai'
      Required = True
    end
    object MasterQKayu: TIntegerField
      FieldName = 'Kayu'
      Required = True
    end
    object MasterQxNama: TStringField
      FieldName = 'xNama'
      Size = 50
    end
    object MasterQxTgl: TDateTimeField
      FieldName = 'xTgl'
    end
    object MasterQxNoSJ: TStringField
      FieldName = 'xNoSJ'
    end
    object MasterQNamaMuat: TStringField
      FieldName = 'NamaMuat'
      Size = 50
    end
    object MasterQTelpMuat: TStringField
      FieldName = 'TelpMuat'
      Size = 12
    end
    object MasterQNamaBongkar: TStringField
      FieldName = 'NamaBongkar'
      Size = 50
    end
    object MasterQTelpBongkar: TStringField
      FieldName = 'TelpBongkar'
      Size = 12
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQLaporan: TStringField
      FieldName = 'Laporan'
      Size = 100
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 444
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select CreateBy, CreateDate, Crew, Kayu, Keterangan, KirEkor, Ki' +
        'rKepala, Kodenota, Laporan, NamaBongkar, NamaMuat, NoSO, Operato' +
        'r, Pajak, Rantai, STNK, Tali, TelpBongkar, TelpMuat, Tgl, TglEnt' +
        'ry, xNama, xNoSJ, xTgl'
      'from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update mastersj'
      'set'
      '  CreateBy = :CreateBy,'
      '  CreateDate = :CreateDate,'
      '  Crew = :Crew,'
      '  Kayu = :Kayu,'
      '  Keterangan = :Keterangan,'
      '  KirEkor = :KirEkor,'
      '  KirKepala = :KirKepala,'
      '  Kodenota = :Kodenota,'
      '  Laporan = :Laporan,'
      '  NamaBongkar = :NamaBongkar,'
      '  NamaMuat = :NamaMuat,'
      '  NoSO = :NoSO,'
      '  Operator = :Operator,'
      '  Pajak = :Pajak,'
      '  Rantai = :Rantai,'
      '  STNK = :STNK,'
      '  Tali = :Tali,'
      '  TelpBongkar = :TelpBongkar,'
      '  TelpMuat = :TelpMuat,'
      '  Tgl = :Tgl,'
      '  TglEntry = :TglEntry,'
      '  xNama = :xNama,'
      '  xNoSJ = :xNoSJ,'
      '  xTgl = :xTgl'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into mastersj'
      
        '  (CreateBy, CreateDate, Crew, Kayu, Keterangan, KirEkor, KirKep' +
        'ala, Kodenota, Laporan, NamaBongkar, NamaMuat, NoSO, Operator, P' +
        'ajak, Rantai, STNK, Tali, TelpBongkar, TelpMuat, Tgl, TglEntry, ' +
        'xNama, xNoSJ, xTgl)'
      'values'
      
        '  (:CreateBy, :CreateDate, :Crew, :Kayu, :Keterangan, :KirEkor, ' +
        ':KirKepala, :Kodenota, :Laporan, :NamaBongkar, :NamaMuat, :NoSO,' +
        ' :Operator, :Pajak, :Rantai, :STNK, :Tali, :TelpBongkar, :TelpMu' +
        'at, :Tgl, :TglEntry, :xNama, :xNoSJ, :xTgl)')
    DeleteSQL.Strings = (
      'delete from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 500
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from mastersj order by kodenota desc')
    Left = 393
    Top = 7
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from masterso where kodenota like '#39'%'#39' + :text + '#39'%'#39' or ' +
        'tgl like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 513
    Top = 415
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
    end
    object SOQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Size = 100
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object SOQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
end
