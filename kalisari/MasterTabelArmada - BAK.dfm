object MasterTabelArmadaFm: TMasterTabelArmadaFm
  Left = 247
  Top = 220
  Width = 1004
  Height = 498
  Caption = 'Master Tabel Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 225
    Height = 387
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 223
      Height = 385
      Align = alClient
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        OnCellDblClick = cxGridDBTableView1CellDblClick
        DataController.DataSource = DSArmada
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGridDBTableView1PlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'PlatNo'
          Options.Editing = False
        end
        object cxGridDBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Options.Editing = False
          Width = 73
        end
        object cxGridDBTableView1JumlahSeat: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahSeat'
          Options.Editing = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 441
    Width = 988
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel8: TPanel
    Left = 225
    Top = 0
    Width = 763
    Height = 387
    Align = alClient
    Caption = 'Panel8'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 761
      Height = 385
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
        DataController.DataSource = MasterDS
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Armada: TcxGridDBColumn
          DataBinding.FieldName = 'Armada'
          Options.Editing = False
        end
        object cxGrid1DBTableView1NoPlat: TcxGridDBColumn
          DataBinding.FieldName = 'NoPlat'
          Options.Editing = False
        end
        object cxGrid1DBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Options.Editing = False
        end
        object cxGrid1DBTableView1Trayek: TcxGridDBColumn
          DataBinding.FieldName = 'Trayek'
        end
        object cxGrid1DBTableView1JamTrayek: TcxGridDBColumn
          DataBinding.FieldName = 'JamTrayek'
        end
        object cxGrid1DBTableView1Kontrak: TcxGridDBColumn
          DataBinding.FieldName = 'Kontrak'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KontrakPropertiesButtonClick
        end
        object cxGrid1DBTableView1Sopir: TcxGridDBColumn
          DataBinding.FieldName = 'Sopir'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1SopirPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaSopir: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSopir'
          Options.Editing = False
          Width = 118
        end
        object cxGrid1DBTableView1Kondektur: TcxGridDBColumn
          DataBinding.FieldName = 'Kondektur'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KondekturPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaKondektur: TcxGridDBColumn
          DataBinding.FieldName = 'NamaKondektur'
          Options.Editing = False
          Width = 115
        end
        object cxGrid1DBTableView1cek: TcxGridDBColumn
          DataBinding.FieldName = 'cek'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 387
    Width = 988
    Height = 54
    Align = alBottom
    TabOrder = 3
    object BtnSave: TButton
      Left = 8
      Top = 8
      Width = 129
      Height = 45
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      WordWrap = True
      OnClick = BtnSaveClick
    end
    object Button3: TButton
      Left = 141
      Top = 8
      Width = 121
      Height = 45
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button3Click
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from MasterTabelArmada')
    UpdateObject = SDUMaster
    Left = 320
    Top = 48
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterQTrayek: TStringField
      FieldName = 'Trayek'
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQNamaKondektur: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKondektur'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kondektur'
      Size = 50
      Lookup = True
    end
    object MasterQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Size = 50
      Lookup = True
    end
    object MasterQNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ2
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ2
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQcek: TBooleanField
      FieldName = 'cek'
    end
    object MasterQJamTrayek: TMemoField
      FieldName = 'JamTrayek'
      BlobType = ftMemo
    end
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from MasterTabelArmada order by kode desc')
    Left = 320
    Top = 80
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 552
    Top = 48
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select a.* from Armada a'
      'where a.kode not in (select armada from masterTabelArmada)')
    UpdateObject = SDUArmada
    Left = 520
    Top = 48
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object MasterDS: TDataSource
    DataSet = MasterQ
    Left = 352
    Top = 48
  end
  object SDUMaster: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopi' +
        'r, cek'#13#10'from MasterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update MasterTabelArmada'
      'set'
      '  Kode = :Kode,'
      '  Armada = :Armada,'
      '  Trayek = :Trayek,'
      '  JamTrayek = :JamTrayek,'
      '  Kontrak = :Kontrak,'
      '  Kondektur = :Kondektur,'
      '  Sopir = :Sopir,'
      '  cek = :cek'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into MasterTabelArmada'
      
        '  (Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopir, c' +
        'ek)'
      'values'
      
        '  (:Kode, :Armada, :Trayek, :JamTrayek, :Kontrak, :Kondektur, :S' +
        'opir, :cek)')
    DeleteSQL.Strings = (
      'delete from MasterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 384
    Top = 48
  end
  object DSArmada: TDataSource
    DataSet = ArmadaQ
    Left = 520
    Top = 80
  end
  object SDUArmada: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, ' +
        'JumlahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspen' +
        'sion, Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, L' +
        'evelArmada, LayoutBan, JumlahBan, Keterangan, Aktif, AC, KmSekar' +
        'ang, CreateDate, CreateBy, Operator, TglEntry, STNKPerpanjangExp' +
        'ired, KirMulai'#13#10'from Armada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Armada'
      'set'
      '  Kode = :Kode,'
      '  PlatNo = :PlatNo,'
      '  NoBody = :NoBody,'
      '  NoRangka = :NoRangka,'
      '  NoMesin = :NoMesin,'
      '  JenisKendaraan = :JenisKendaraan,'
      '  JumlahSeat = :JumlahSeat,'
      '  JenisBBM = :JenisBBM,'
      '  TahunPembuatan = :TahunPembuatan,'
      '  JenisAC = :JenisAC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Sopir = :Sopir,'
      '  KapasitasTangkiBBM = :KapasitasTangkiBBM,'
      '  STNKPajakExpired = :STNKPajakExpired,'
      '  KirSelesai = :KirSelesai,'
      '  LevelArmada = :LevelArmada,'
      '  LayoutBan = :LayoutBan,'
      '  JumlahBan = :JumlahBan,'
      '  Keterangan = :Keterangan,'
      '  Aktif = :Aktif,'
      '  AC = :AC,'
      '  KmSekarang = :KmSekarang,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  STNKPerpanjangExpired = :STNKPerpanjangExpired,'
      '  KirMulai = :KirMulai'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Armada'
      
        '  (Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, Juml' +
        'ahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspension' +
        ', Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, Level' +
        'Armada, LayoutBan, JumlahBan, Keterangan, Aktif, AC, KmSekarang,' +
        ' CreateDate, CreateBy, Operator, TglEntry, STNKPerpanjangExpired' +
        ', KirMulai)'
      'values'
      
        '  (:Kode, :PlatNo, :NoBody, :NoRangka, :NoMesin, :JenisKendaraan' +
        ', :JumlahSeat, :JenisBBM, :TahunPembuatan, :JenisAC, :Toilet, :A' +
        'irSuspension, :Sopir, :KapasitasTangkiBBM, :STNKPajakExpired, :K' +
        'irSelesai, :LevelArmada, :LayoutBan, :JumlahBan, :Keterangan, :A' +
        'ktif, :AC, :KmSekarang, :CreateDate, :CreateBy, :Operator, :TglE' +
        'ntry, :STNKPerpanjangExpired, :KirMulai)')
    DeleteSQL.Strings = (
      'delete from Armada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 488
    Top = 48
  end
  object ArmadaQ2: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 552
    Top = 80
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object StringField6: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object StringField7: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object StringField8: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object StringField9: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object BooleanField1: TBooleanField
      FieldName = 'Toilet'
    end
    object BooleanField2: TBooleanField
      FieldName = 'AirSuspension'
    end
    object StringField10: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object IntegerField2: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object StringField11: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object IntegerField3: TIntegerField
      FieldName = 'JumlahBan'
    end
    object StringField12: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object BooleanField3: TBooleanField
      FieldName = 'Aktif'
    end
    object BooleanField4: TBooleanField
      FieldName = 'AC'
    end
    object IntegerField4: TIntegerField
      FieldName = 'KmSekarang'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object StringField13: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object StringField14: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
end
