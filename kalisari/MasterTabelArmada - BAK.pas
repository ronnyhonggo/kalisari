unit MasterTabelArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxTimeEdit, cxCheckBox, Buttons, cxDropDownEdit,
  cxGroupBox;

type
  TMasterTabelArmadaFm = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    MasterQ: TSDQuery;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    Panel8: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel2: TPanel;
    BtnSave: TButton;
    Button3: TButton;
    MasterDS: TDataSource;
    SDUMaster: TSDUpdateSQL;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    DSArmada: TDataSource;
    cxGridDBTableView1PlatNo: TcxGridDBColumn;
    cxGridDBTableView1NoBody: TcxGridDBColumn;
    cxGridDBTableView1JumlahSeat: TcxGridDBColumn;
    MasterQKode: TStringField;
    MasterQArmada: TStringField;
    MasterQTrayek: TStringField;
    MasterQKontrak: TStringField;
    MasterQKondektur: TStringField;
    MasterQSopir: TStringField;
    cxGrid1DBTableView1Armada: TcxGridDBColumn;
    cxGrid1DBTableView1Trayek: TcxGridDBColumn;
    cxGrid1DBTableView1Kontrak: TcxGridDBColumn;
    cxGrid1DBTableView1Kondektur: TcxGridDBColumn;
    cxGrid1DBTableView1Sopir: TcxGridDBColumn;
    MasterQNamaKondektur: TStringField;
    MasterQNamaSopir: TStringField;
    cxGrid1DBTableView1NamaKondektur: TcxGridDBColumn;
    cxGrid1DBTableView1NamaSopir: TcxGridDBColumn;
    SDUArmada: TSDUpdateSQL;
    MasterQNoPlat: TStringField;
    MasterQNoBody: TStringField;
    cxGrid1DBTableView1NoPlat: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    ArmadaQ2: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField1: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    StringField10: TStringField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    StringField11: TStringField;
    IntegerField3: TIntegerField;
    StringField12: TStringField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    IntegerField4: TIntegerField;
    DateTimeField3: TDateTimeField;
    StringField13: TStringField;
    StringField14: TStringField;
    DateTimeField4: TDateTimeField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    MasterQcek: TBooleanField;
    MasterQJamTrayek: TMemoField;
    cxGrid1DBTableView1JamTrayek: TcxGridDBColumn;
    cxGrid1DBTableView1cek: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1KontrakPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1SopirPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1KondekturPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MasterTabelArmadaFm: TMasterTabelArmadaFm;
   MasterOriSQL: string;
   DetailBonBarangOriSQL: string;
   paramkode,paramkode2 :string;
   baru:integer;

implementation

uses DM, MenuUtama, DropDown, BarangDropDown, SPKDropDown, StoringDropDown, PegawaiDropDown,
  ArmadaDropDown, KontrakDropDown;

{$R *.dfm}

procedure TMasterTabelArmadaFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
end;

procedure TMasterTabelArmadaFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterTabelArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterTabelArmadaFm.FormShow(Sender: TObject);
begin
  ArmadaQ.Open;
  PegawaiQ.Open;
  MasterQ.Open;
  BtnSave.Enabled:=true;//menuutamafm.UserQUpdateBonBarang.AsBoolean;
end;

procedure TMasterTabelArmadaFm.BtnSaveClick(Sender: TObject);
var kode:string;
    i:integer;
begin
  KodeQ.Open;
  if KodeQ.IsEmpty then begin
    //kode:=DMFm.Fill('1',10,'0',True);
    i:=1;
  end
  else begin
    //kode:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    i:=StrToInt(KodeQkode.AsString)+1;
  end;
  MasterQ.Edit;
  MasterQ.First;
  while MasterQ.Eof=false do begin
    MasterQ.Edit;
    if MasterQKode.AsString='new' then begin
      MasterQKode.AsString:=DMFm.Fill(IntToStr(i),10,'0',True);
      MasterQ.Post;
      i:=i+1;
    end;
    MasterQ.Next;
  end;
  KodeQ.Close;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  ArmadaQ.Refresh;
end;

procedure TMasterTabelArmadaFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
    MasterQ.Post;
  end;
end;

procedure TMasterTabelArmadaFm.Button3Click(Sender: TObject);
begin
  Release;
end;

procedure TMasterTabelArmadaFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  MasterQ.Insert;
  MasterQKode.AsString:='new';
  MasterQArmada.AsString:=ArmadaQKode.AsString;
  ArmadaQ.Delete;
end;

procedure TMasterTabelArmadaFm.cxGrid1DBTableView1KontrakPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KontrakDropDownFm:=TKontrakDropdownfm.Create(Self,'select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"');
  if KontrakDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakDropDownFm.kode;
  end;
  KontrakDropDownFm.Release;
end;

procedure TMasterTabelArmadaFm.cxGrid1DBTableView1SopirPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQSopir.AsString:=PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TMasterTabelArmadaFm.cxGrid1DBTableView1KondekturPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQKondektur.AsString:=PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TMasterTabelArmadaFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[10] = true) then
  begin
    ACanvas.Brush.Color := clRed;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

end.
