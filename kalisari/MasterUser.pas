unit MasterUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxPC,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxTabbedMDI,
  dxSkinsdxNavBarPainter, dxNavBar, cxCheckBox, cxDBEdit, cxClasses;

type
  TMasterUserFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    cxDBCheckBox1: TcxDBCheckBox;
    cxDBCheckBox2: TcxDBCheckBox;
    cxDBCheckBox3: TcxDBCheckBox;
    cxDBCheckBox4: TcxDBCheckBox;
    cxDBCheckBox5: TcxDBCheckBox;
    cxDBCheckBox6: TcxDBCheckBox;
    cxDBCheckBox7: TcxDBCheckBox;
    cxDBCheckBox8: TcxDBCheckBox;
    cxDBCheckBox9: TcxDBCheckBox;
    cxDBCheckBox10: TcxDBCheckBox;
    cxDBCheckBox11: TcxDBCheckBox;
    cxDBCheckBox12: TcxDBCheckBox;
    cxDBCheckBox13: TcxDBCheckBox;
    cxDBCheckBox14: TcxDBCheckBox;
    cxDBCheckBox15: TcxDBCheckBox;
    cxDBCheckBox16: TcxDBCheckBox;
    cxDBCheckBox17: TcxDBCheckBox;
    cxDBCheckBox18: TcxDBCheckBox;
    cxDBCheckBox19: TcxDBCheckBox;
    cxDBCheckBox20: TcxDBCheckBox;
    cxDBCheckBox21: TcxDBCheckBox;
    cxDBCheckBox22: TcxDBCheckBox;
    cxDBCheckBox23: TcxDBCheckBox;
    cxDBCheckBox24: TcxDBCheckBox;
    cxDBCheckBox29: TcxDBCheckBox;
    cxDBCheckBox30: TcxDBCheckBox;
    cxDBCheckBox31: TcxDBCheckBox;
    cxDBCheckBox32: TcxDBCheckBox;
    cxDBCheckBox33: TcxDBCheckBox;
    cxDBCheckBox34: TcxDBCheckBox;
    cxDBCheckBox35: TcxDBCheckBox;
    cxDBCheckBox36: TcxDBCheckBox;
    cxDBCheckBox37: TcxDBCheckBox;
    cxDBCheckBox38: TcxDBCheckBox;
    cxDBCheckBox39: TcxDBCheckBox;
    cxDBCheckBox40: TcxDBCheckBox;
    cxDBCheckBox41: TcxDBCheckBox;
    cxDBCheckBox42: TcxDBCheckBox;
    cxDBCheckBox43: TcxDBCheckBox;
    cxDBCheckBox44: TcxDBCheckBox;
    cxDBCheckBox45: TcxDBCheckBox;
    cxDBCheckBox46: TcxDBCheckBox;
    cxDBCheckBox47: TcxDBCheckBox;
    cxDBCheckBox48: TcxDBCheckBox;
    cxDBCheckBox53: TcxDBCheckBox;
    cxDBCheckBox54: TcxDBCheckBox;
    cxDBCheckBox55: TcxDBCheckBox;
    cxDBCheckBox56: TcxDBCheckBox;
    cxDBCheckBox57: TcxDBCheckBox;
    cxDBCheckBox58: TcxDBCheckBox;
    cxDBCheckBox59: TcxDBCheckBox;
    cxDBCheckBox60: TcxDBCheckBox;
    cxDBCheckBox61: TcxDBCheckBox;
    cxDBCheckBox62: TcxDBCheckBox;
    cxDBCheckBox63: TcxDBCheckBox;
    cxDBCheckBox64: TcxDBCheckBox;
    cxDBCheckBox65: TcxDBCheckBox;
    cxDBCheckBox66: TcxDBCheckBox;
    cxDBCheckBox67: TcxDBCheckBox;
    cxDBCheckBox68: TcxDBCheckBox;
    cxDBCheckBox69: TcxDBCheckBox;
    cxDBCheckBox70: TcxDBCheckBox;
    cxDBCheckBox71: TcxDBCheckBox;
    cxDBCheckBox72: TcxDBCheckBox;
    cxDBCheckBox73: TcxDBCheckBox;
    cxDBCheckBox76: TcxDBCheckBox;
    cxDBCheckBox75: TcxDBCheckBox;
    cxDBCheckBox74: TcxDBCheckBox;
    cxDBCheckBox77: TcxDBCheckBox;
    cxDBCheckBox78: TcxDBCheckBox;
    cxDBCheckBox79: TcxDBCheckBox;
    cxDBCheckBox80: TcxDBCheckBox;
    cxDBCheckBox81: TcxDBCheckBox;
    cxDBCheckBox82: TcxDBCheckBox;
    cxDBCheckBox83: TcxDBCheckBox;
    cxDBCheckBox84: TcxDBCheckBox;
    cxDBCheckBox85: TcxDBCheckBox;
    cxDBCheckBox86: TcxDBCheckBox;
    cxDBCheckBox87: TcxDBCheckBox;
    cxDBCheckBox88: TcxDBCheckBox;
    cxDBCheckBox89: TcxDBCheckBox;
    cxDBCheckBox90: TcxDBCheckBox;
    cxDBCheckBox91: TcxDBCheckBox;
    cxDBCheckBox92: TcxDBCheckBox;
    cxDBCheckBox93: TcxDBCheckBox;
    cxDBCheckBox94: TcxDBCheckBox;
    cxDBCheckBox95: TcxDBCheckBox;
    cxDBCheckBox96: TcxDBCheckBox;
    cxDBCheckBox97: TcxDBCheckBox;
    cxDBCheckBox98: TcxDBCheckBox;
    cxDBCheckBox99: TcxDBCheckBox;
    cxDBCheckBox100: TcxDBCheckBox;
    cxDBCheckBox101: TcxDBCheckBox;
    cxDBCheckBox102: TcxDBCheckBox;
    cxDBCheckBox103: TcxDBCheckBox;
    cxDBCheckBox104: TcxDBCheckBox;
    cxDBCheckBox105: TcxDBCheckBox;
    cxDBCheckBox106: TcxDBCheckBox;
    cxDBCheckBox107: TcxDBCheckBox;
    cxDBCheckBox108: TcxDBCheckBox;
    cxDBCheckBox109: TcxDBCheckBox;
    cxDBCheckBox110: TcxDBCheckBox;
    cxDBCheckBox111: TcxDBCheckBox;
    cxDBCheckBox112: TcxDBCheckBox;
    cxDBCheckBox113: TcxDBCheckBox;
    cxDBCheckBox114: TcxDBCheckBox;
    cxDBCheckBox115: TcxDBCheckBox;
    cxDBCheckBox116: TcxDBCheckBox;
    cxDBCheckBox117: TcxDBCheckBox;
    cxDBCheckBox118: TcxDBCheckBox;
    cxDBCheckBox119: TcxDBCheckBox;
    cxDBCheckBox120: TcxDBCheckBox;
    cxDBCheckBox121: TcxDBCheckBox;
    cxDBCheckBox122: TcxDBCheckBox;
    cxDBCheckBox123: TcxDBCheckBox;
    cxDBCheckBox124: TcxDBCheckBox;
    cxDBCheckBox125: TcxDBCheckBox;
    cxDBCheckBox126: TcxDBCheckBox;
    cxDBCheckBox127: TcxDBCheckBox;
    cxDBCheckBox128: TcxDBCheckBox;
    cxDBCheckBox129: TcxDBCheckBox;
    cxDBCheckBox130: TcxDBCheckBox;
    cxDBCheckBox131: TcxDBCheckBox;
    cxDBCheckBox132: TcxDBCheckBox;
    cxDBCheckBox133: TcxDBCheckBox;
    cxDBCheckBox134: TcxDBCheckBox;
    cxDBCheckBox135: TcxDBCheckBox;
    cxDBCheckBox136: TcxDBCheckBox;
    cxDBCheckBox137: TcxDBCheckBox;
    cxDBCheckBox138: TcxDBCheckBox;
    cxDBCheckBox139: TcxDBCheckBox;
    cxDBCheckBox140: TcxDBCheckBox;
    cxDBCheckBox141: TcxDBCheckBox;
    cxDBCheckBox142: TcxDBCheckBox;
    cxDBCheckBox143: TcxDBCheckBox;
    cxDBCheckBox144: TcxDBCheckBox;
    cxDBCheckBox145: TcxDBCheckBox;
    cxDBCheckBox146: TcxDBCheckBox;
    cxDBCheckBox147: TcxDBCheckBox;
    cxDBCheckBox148: TcxDBCheckBox;
    cxDBCheckBox149: TcxDBCheckBox;
    cxDBCheckBox150: TcxDBCheckBox;
    cxDBCheckBox151: TcxDBCheckBox;
    cxDBCheckBox152: TcxDBCheckBox;
    cxDBCheckBox153: TcxDBCheckBox;
    cxDBCheckBox154: TcxDBCheckBox;
    cxDBCheckBox155: TcxDBCheckBox;
    cxDBCheckBox156: TcxDBCheckBox;
    cxDBCheckBox157: TcxDBCheckBox;
    cxDBCheckBox158: TcxDBCheckBox;
    cxDBCheckBox159: TcxDBCheckBox;
    cxDBCheckBox160: TcxDBCheckBox;
    cxDBCheckBox161: TcxDBCheckBox;
    cxDBCheckBox162: TcxDBCheckBox;
    cxDBCheckBox163: TcxDBCheckBox;
    cxDBCheckBox164: TcxDBCheckBox;
    cxDBCheckBox168: TcxDBCheckBox;
    cxDBCheckBox170: TcxDBCheckBox;
    cxDBCheckBox184: TcxDBCheckBox;
    cxDBCheckBox185: TcxDBCheckBox;
    cxDBCheckBox186: TcxDBCheckBox;
    cxDBCheckBox187: TcxDBCheckBox;
    cxDBCheckBox188: TcxDBCheckBox;
    cxDBCheckBox189: TcxDBCheckBox;
    cxDBCheckBox190: TcxDBCheckBox;
    cxDBCheckBox191: TcxDBCheckBox;
    cxDBCheckBox192: TcxDBCheckBox;
    cxDBCheckBox193: TcxDBCheckBox;
    cxDBCheckBox194: TcxDBCheckBox;
    cxDBCheckBox195: TcxDBCheckBox;
    cxDBCheckBox196: TcxDBCheckBox;
    cxDBCheckBox197: TcxDBCheckBox;
    cxDBCheckBox198: TcxDBCheckBox;
    cxDBCheckBox199: TcxDBCheckBox;
    MasterVGridUsername: TcxDBEditorRow;
    MasterVGridPassword: TcxDBEditorRow;
    MasterVGridJenis: TcxDBEditorRow;
    MasterVGridKodePegawai: TcxDBEditorRow;
    cxDBCheckBox200: TcxDBCheckBox;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    MasterVGridNamaPegawai: TcxDBEditorRow;
    MasterVGridAlamatPegawai: TcxDBEditorRow;
    MasterVGridKotaPegawai: TcxDBEditorRow;
    MasterVGridTelpPegawai: TcxDBEditorRow;
    MasterVGridTglLahirPegawai: TcxDBEditorRow;
    MasterVGridGajiPegawai: TcxDBEditorRow;
    MasterVGridJabatanPegawai: TcxDBEditorRow;
    MasterVGridMulaiPegawai: TcxDBEditorRow;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBCheckBox201: TcxDBCheckBox;
    cxDBCheckBox202: TcxDBCheckBox;
    cxDBCheckBox203: TcxDBCheckBox;
    cxDBCheckBox204: TcxDBCheckBox;
    cxDBCheckBox205: TcxDBCheckBox;
    cxDBCheckBox206: TcxDBCheckBox;
    cxDBCheckBox207: TcxDBCheckBox;
    MasterQKode: TStringField;
    MasterQUsername: TStringField;
    MasterQPassword: TStringField;
    MasterQJenis: TStringField;
    MasterQKodePegawai: TStringField;
    MasterQMasterArmada: TBooleanField;
    MasterQInsertMasterArmada: TBooleanField;
    MasterQUpdateMasterArmada: TBooleanField;
    MasterQDeleteMasterArmada: TBooleanField;
    MasterQMasterJenisKendaraan: TBooleanField;
    MasterQInsertJenisKendaraan: TBooleanField;
    MasterQUpdateJenisKendaraan: TBooleanField;
    MasterQDeleteJenisKendaraan: TBooleanField;
    MasterQMasterEkor: TBooleanField;
    MasterQInsertMasterEkor: TBooleanField;
    MasterQUpdateMasterEkor: TBooleanField;
    MasterQDeleteMasterEkor: TBooleanField;
    MasterQMasterPelanggan: TBooleanField;
    MasterQInsertMasterPelanggan: TBooleanField;
    MasterQUpdateMasterPelanggan: TBooleanField;
    MasterQDeleteMasterPelanggan: TBooleanField;
    MasterQMasterRute: TBooleanField;
    MasterQInsertMasterRute: TBooleanField;
    MasterQUpdateMasterRute: TBooleanField;
    MasterQDeleteMasterRute: TBooleanField;
    MasterQMasterSopir: TBooleanField;
    MasterQInsertMasterSopir: TBooleanField;
    MasterQUpdateMasterSopir: TBooleanField;
    MasterQDeleteMasterSopir: TBooleanField;
    MasterQMasterJarak: TBooleanField;
    MasterQInsertMasterJarak: TBooleanField;
    MasterQUpdateMasterJarak: TBooleanField;
    MasterQDeleteMasterJarak: TBooleanField;
    MasterQMasterBan: TBooleanField;
    MasterQInsertMasterBan: TBooleanField;
    MasterQUpdateMasterBan: TBooleanField;
    MasterQDeleteMasterBan: TBooleanField;
    MasterQMasterStandardPerawatan: TBooleanField;
    MasterQInsertMasterStandardPerawatan: TBooleanField;
    MasterQUpdateMasterStandardPerawatan: TBooleanField;
    MasterQDeleteMasterStandardPerawatan: TBooleanField;
    MasterQMasterBarang: TBooleanField;
    MasterQInsertMasterBarang: TBooleanField;
    MasterQUpdateMasterBarang: TBooleanField;
    MasterQDeleteMasterBarang: TBooleanField;
    MasterQMasterJenisPerbaikan: TBooleanField;
    MasterQInsertMasterJenisPerbaikan: TBooleanField;
    MasterQUpdateMasterJenisPerbaikan: TBooleanField;
    MasterQDeleteMasterJenisPerbaikan: TBooleanField;
    MasterQMasterSupplier: TBooleanField;
    MasterQInsertMasterSupplier: TBooleanField;
    MasterQUpdateMasterSupplier: TBooleanField;
    MasterQDeleteMasterSupplier: TBooleanField;
    MasterQMasterUser: TBooleanField;
    MasterQInsertMasterUser: TBooleanField;
    MasterQUpdateMasterUser: TBooleanField;
    MasterQDeleteMasterUser: TBooleanField;
    MasterQMasterMekanik: TBooleanField;
    MasterQInsertMasterMekanik: TBooleanField;
    MasterQUpdateMasterMekanik: TBooleanField;
    MasterQDeleteMasterMekanik: TBooleanField;
    MasterQMasterPegawai: TBooleanField;
    MasterQInsertMasterPegawai: TBooleanField;
    MasterQUpdateMasterPegawai: TBooleanField;
    MasterQDeleteMasterPegawai: TBooleanField;
    MasterQKontrak: TBooleanField;
    MasterQInsertKontrak: TBooleanField;
    MasterQUpdateKontrak: TBooleanField;
    MasterQDeleteKontrak: TBooleanField;
    MasterQSO: TBooleanField;
    MasterQInsertSO: TBooleanField;
    MasterQUpdateSO: TBooleanField;
    MasterQDeleteSO: TBooleanField;
    MasterQSetHargaSO: TBooleanField;
    MasterQSetStatusSO: TBooleanField;
    MasterQViewHargaSO: TBooleanField;
    MasterQKeluhanPelanggan: TBooleanField;
    MasterQInsertKeluhanPelanggan: TBooleanField;
    MasterQUpdateKeluhanPelanggan: TBooleanField;
    MasterQDeleteKeluhanPelanggan: TBooleanField;
    MasterQDaftarPelanggan: TBooleanField;
    MasterQJadwalArmada: TBooleanField;
    MasterQPilihArmada: TBooleanField;
    MasterQUpdatePilihArmada: TBooleanField;
    MasterQSuratJalan: TBooleanField;
    MasterQInsertSuratJalan: TBooleanField;
    MasterQUpdateSuratJalan: TBooleanField;
    MasterQDeleteSuratJalan: TBooleanField;
    MasterQRealisasiSuratJalan: TBooleanField;
    MasterQInsertRealisasiSuratJalan: TBooleanField;
    MasterQUpdateRealisasiSuratJalan: TBooleanField;
    MasterQDeleteRealisasiSuratJalan: TBooleanField;
    MasterQStoring: TBooleanField;
    MasterQInsertStoring: TBooleanField;
    MasterQUpdateStoring: TBooleanField;
    MasterQDeleteStoring: TBooleanField;
    MasterQDaftarBeli: TBooleanField;
    MasterQUpdateDaftarBeli: TBooleanField;
    MasterQPilihSupplierDaftarBeli: TBooleanField;
    MasterQPO: TBooleanField;
    MasterQInsertPO: TBooleanField;
    MasterQUpdatePO: TBooleanField;
    MasterQDeletePO: TBooleanField;
    MasterQApprovalPO: TBooleanField;
    MasterQCashNCarry: TBooleanField;
    MasterQInsertCashNCarry: TBooleanField;
    MasterQUpdateCashNCarry: TBooleanField;
    MasterQDeleteCashNCarry: TBooleanField;
    MasterQKomplainNRetur: TBooleanField;
    MasterQInsertKomplainNRetur: TBooleanField;
    MasterQUpdateKomplainRetur: TBooleanField;
    MasterQDeleteKomplainRetur: TBooleanField;
    MasterQDaftarSupplier: TBooleanField;
    MasterQBonBarang: TBooleanField;
    MasterQInsertBonBarang: TBooleanField;
    MasterQUpdateBonBarang: TBooleanField;
    MasterQDeleteBonBarang: TBooleanField;
    MasterQSetBeliBonBarang: TBooleanField;
    MasterQSetMintaBonBarang: TBooleanField;
    MasterQTambahStokBonBarang: TBooleanField;
    MasterQPerbaikanBonBarang: TBooleanField;
    MasterQPerawatanBonBarang: TBooleanField;
    MasterQApprovalBonBarang: TBooleanField;
    MasterQProcessBonBarang: TBooleanField;
    MasterQApprovalMoneyBonBarang: TCurrencyField;
    MasterQSetUrgentBonBarang: TBooleanField;
    MasterQBonKeluarBarang: TBooleanField;
    MasterQInsertBonKeluarBarang: TBooleanField;
    MasterQUpdateBonKeluarBarang: TBooleanField;
    MasterQDeleteBonKeluarBarang: TBooleanField;
    MasterQLPB: TBooleanField;
    MasterQInsertLPB: TBooleanField;
    MasterQUpdateLPB: TBooleanField;
    MasterQDeleteLPB: TBooleanField;
    MasterQPemutihan: TBooleanField;
    MasterQInsertPemutihan: TBooleanField;
    MasterQUpdatePemutihan: TBooleanField;
    MasterQDeletePemutihan: TBooleanField;
    MasterQApprovalPemutihan: TBooleanField;
    MasterQDaftarMinStok: TBooleanField;
    MasterQDaftarStokBarang: TBooleanField;
    MasterQLaporanRiwayatStokBarang: TBooleanField;
    MasterQLaporanRiwayatHargaBarang: TBooleanField;
    MasterQNotifikasiPerawatan: TBooleanField;
    MasterQInsertNotifikasiPerawatan: TBooleanField;
    MasterQPermintaanPerbaikan: TBooleanField;
    MasterQInsertPermintaanPerbaikan: TBooleanField;
    MasterQUpdatePermintaanPerbaikan: TBooleanField;
    MasterQDeletePermintaanPerbaikan: TBooleanField;
    MasterQPerbaikan: TBooleanField;
    MasterQInsertPerbaikan: TBooleanField;
    MasterQUpdatePerbaikan: TBooleanField;
    MasterQDeletePerbaikan: TBooleanField;
    MasterQPerawatan: TBooleanField;
    MasterQInsertPerawatan: TBooleanField;
    MasterQUpdatePerawatan: TBooleanField;
    MasterQDeletePerawatan: TBooleanField;
    MasterQLepasPasangBan: TBooleanField;
    MasterQInsertLepasPasangBan: TBooleanField;
    MasterQUpdateLepasPasangBan: TBooleanField;
    MasterQDeleteLepasPasangBan: TBooleanField;
    MasterQPerbaikanBan: TBooleanField;
    MasterQInsertPerbaikanBan: TBooleanField;
    MasterQUpdatePerbaikanBan: TBooleanField;
    MasterQDeletePerbaikanBan: TBooleanField;
    MasterQTukarKomponen: TBooleanField;
    MasterQInsertTukarKomponen: TBooleanField;
    MasterQUpdateTukarKomponen: TBooleanField;
    MasterQDeleteTukarKomponen: TBooleanField;
    MasterQKasBon: TBooleanField;
    MasterQInsertKasBon: TBooleanField;
    MasterQUpdateKasBon: TBooleanField;
    MasterQDeleteKasBon: TBooleanField;
    MasterQPenagihan: TBooleanField;
    MasterQInsertPenagihan: TBooleanField;
    MasterQUpdatePenagihan: TBooleanField;
    MasterQDeletePenagihan: TBooleanField;
    MasterQBayarPO: TBooleanField;
    MasterQInsertBayarPO: TBooleanField;
    MasterQUpdateBayarPO: TBooleanField;
    MasterQDeleteBayarPO: TBooleanField;
    MasterQLaporanSalesOrder: TBooleanField;
    MasterQLaporanRealisasiOrder: TBooleanField;
    MasterQLaporanOrderTidakSesuai: TBooleanField;
    MasterQLaporanKeluhanPelanggan: TBooleanField;
    MasterQLaporanPendapatan: TBooleanField;
    MasterQLaporanPemenuhanKontrak: TBooleanField;
    MasterQLaporanEfisiensiMuatan: TBooleanField;
    MasterQLaporanPendapatanDanPoinSopir: TBooleanField;
    MasterQLaporanPenggunaanBan: TBooleanField;
    MasterQLaporanPenggunaanBarang: TBooleanField;
    MasterQLaporanKegiatanMekanik: TBooleanField;
    MasterQLaporanKegiatanTeknik: TBooleanField;
    MasterQLaporanRiwayatKendaraan: TBooleanField;
    MasterQLaporanPemenuhanBarang: TBooleanField;
    MasterQLaporanPenerimaanNPembelian: TBooleanField;
    MasterQLaporanPenambahanOli: TBooleanField;
    MasterQLaporanRetur: TBooleanField;
    MasterQLaporanPenjualanUser: TBooleanField;
    MasterQLaporanProfitPerSO: TBooleanField;
    MasterQSettingParameterNotifikasi: TBooleanField;
    MasterQSettingWaktuNotifikasi: TBooleanField;
    MasterQTampilkanNotifikasi: TBooleanField;
    MasterQSettingNotifikasi: TBooleanField;
    MasterQFollowUpCustomer: TBooleanField;
    MasterQInactiveCustomer: TBooleanField;
    MasterQPembuatanSuratJalan: TBooleanField;
    MasterQNewBonBarang: TBooleanField;
    MasterQNewDaftarBeli: TBooleanField;
    MasterQNewPermintaanPerbaikan: TBooleanField;
    MasterQNotifPilihArmada: TBooleanField;
    MasterQNewLPB: TBooleanField;
    MasterQMinimumStok: TBooleanField;
    MasterQNamaPegawai: TStringField;
    MasterQAlamatPegawai: TStringField;
    MasterQKotaPegawai: TStringField;
    MasterQTelpPegawai: TStringField;
    MasterQMulaiBekerja: TDateTimeField;
    MasterQJabatanPegawai: TStringField;
    MasterQGajiPegawai: TCurrencyField;
    MasterQTglLahirPegawai: TDateTimeField;
    cxDBCheckBox208: TcxDBCheckBox;
    cxDBCheckBox209: TcxDBCheckBox;
    cxDBCheckBox210: TcxDBCheckBox;
    MasterQSTNKExpired: TBooleanField;
    MasterQSTNKPajakExpired: TBooleanField;
    MasterQKirExpired: TBooleanField;
    cxDBCheckBox211: TcxDBCheckBox;
    MasterQSimExpired: TBooleanField;
    MasterQMasterBKanibal: TBooleanField;
    MasterQInsertMasterBKanibal: TBooleanField;
    MasterQUpdateMasterBKanibal: TBooleanField;
    MasterQDeleteMasterBKanibal: TBooleanField;
    MasterQMasterKBarang: TBooleanField;
    MasterQInsertMasterKBarang: TBooleanField;
    MasterQUpdateMasterKBarang: TBooleanField;
    MasterQDeleteMasterKBarang: TBooleanField;
    MasterQMasterAC: TBooleanField;
    MasterQInsertMasterAC: TBooleanField;
    MasterQUpdateMasterAC: TBooleanField;
    MasterQDeleteMasterAC: TBooleanField;
    cxDBCheckBox212: TcxDBCheckBox;
    cxDBCheckBox213: TcxDBCheckBox;
    cxDBCheckBox214: TcxDBCheckBox;
    cxDBCheckBox215: TcxDBCheckBox;
    MasterQInsertKasBonAnjem: TBooleanField;
    MasterQUpdateKasBonAnjem: TBooleanField;
    MasterQDeleteKasBonAnjem: TBooleanField;
    MasterQKasBonAnjem: TBooleanField;
    MasterQPembuatanKwitansi: TBooleanField;
    MasterQInsertPembuatanKwitansi: TBooleanField;
    MasterQUpdatePembuatanKwitansi: TBooleanField;
    MasterQDeletePembuatanKwitansi: TBooleanField;
    cxDBCheckBox216: TcxDBCheckBox;
    cxDBCheckBox217: TcxDBCheckBox;
    cxDBCheckBox218: TcxDBCheckBox;
    cxDBCheckBox219: TcxDBCheckBox;
    MasterQViewJadwal: TBooleanField;
    MasterQRealisasiAnjem: TBooleanField;
    MasterQInsertRealisasiAnjem: TBooleanField;
    MasterQUpdateRealisasiAnjem: TBooleanField;
    MasterQDeleteRealisasiAnjem: TBooleanField;
    MasterQRealisasiTrayek: TBooleanField;
    MasterQInsertRealisasiTrayek: TBooleanField;
    MasterQUpdateRealisasiTrayek: TBooleanField;
    MasterQDeleteRealisasiTrayek: TBooleanField;
    MasterQUpdateKm: TBooleanField;
    MasterQUpdateUpdateKm: TBooleanField;
    cxDBCheckBox220: TcxDBCheckBox;
    cxDBCheckBox221: TcxDBCheckBox;
    cxDBCheckBox222: TcxDBCheckBox;
    cxDBCheckBox223: TcxDBCheckBox;
    cxDBCheckBox224: TcxDBCheckBox;
    cxDBCheckBox225: TcxDBCheckBox;
    cxDBCheckBox226: TcxDBCheckBox;
    cxDBCheckBox227: TcxDBCheckBox;
    cxDBCheckBox228: TcxDBCheckBox;
    cxDBCheckBox229: TcxDBCheckBox;
    MasterQPOUmum: TBooleanField;
    MasterQInsertPOUmum: TBooleanField;
    MasterQUpdatePOUmum: TBooleanField;
    MasterQDeletePOUmum: TBooleanField;
    cxDBCheckBox230: TcxDBCheckBox;
    cxDBCheckBox231: TcxDBCheckBox;
    cxDBCheckBox232: TcxDBCheckBox;
    cxDBCheckBox233: TcxDBCheckBox;
    cxDBCheckBox234: TcxDBCheckBox;
    cxDBCheckBox235: TcxDBCheckBox;
    MasterQDaftarMinimumStokKategori: TBooleanField;
    MasterQKeluarMasukBarangBekas: TBooleanField;
    MasterQInsertKeluarMasukBarangBekas: TBooleanField;
    MasterQUpdateKeluarMasukBarangBekas: TBooleanField;
    MasterQDeleteKeluarMasukBarangBekas: TBooleanField;
    cxDBCheckBox236: TcxDBCheckBox;
    cxDBCheckBox237: TcxDBCheckBox;
    cxDBCheckBox238: TcxDBCheckBox;
    cxDBCheckBox239: TcxDBCheckBox;
    MasterQRebuild: TBooleanField;
    MasterQInsertRebuild: TBooleanField;
    MasterQUpdateRebuild: TBooleanField;
    MasterQDeleteRebuild: TBooleanField;
    MasterQSPK: TBooleanField;
    MasterQInsertSPK: TBooleanField;
    MasterQUpdateSPK: TBooleanField;
    MasterQDeleteSPK: TBooleanField;
    MasterQAmbilBarangKanibal: TBooleanField;
    MasterQInsertAmbilBarangKanibal: TBooleanField;
    MasterQUpdateAmbilBarangKanibal: TBooleanField;
    MasterQDeleteAmbilBarangKanibal: TBooleanField;
    cxDBCheckBox240: TcxDBCheckBox;
    cxDBCheckBox241: TcxDBCheckBox;
    cxDBCheckBox242: TcxDBCheckBox;
    cxDBCheckBox243: TcxDBCheckBox;
    cxDBCheckBox244: TcxDBCheckBox;
    cxDBCheckBox245: TcxDBCheckBox;
    cxDBCheckBox246: TcxDBCheckBox;
    cxDBCheckBox247: TcxDBCheckBox;
    cxDBCheckBox248: TcxDBCheckBox;
    cxDBCheckBox249: TcxDBCheckBox;
    cxDBCheckBox250: TcxDBCheckBox;
    cxDBCheckBox251: TcxDBCheckBox;
    cxDBCheckBox252: TcxDBCheckBox;
    cxDBCheckBox253: TcxDBCheckBox;
    cxDBCheckBox254: TcxDBCheckBox;
    cxDBCheckBox255: TcxDBCheckBox;
    cxDBCheckBox256: TcxDBCheckBox;
    cxDBCheckBox257: TcxDBCheckBox;
    cxDBCheckBox258: TcxDBCheckBox;
    cxDBCheckBox259: TcxDBCheckBox;
    MasterQMasterBarangBekas: TBooleanField;
    MasterQInsertMasterBarangBekas: TBooleanField;
    MasterQUpdateMasterBarangBekas: TBooleanField;
    MasterQDeleteMasterBarangBekas: TBooleanField;
    MasterQMasterLayoutBan: TBooleanField;
    MasterQInsertMasterLayoutBan: TBooleanField;
    MasterQUpdateMasterLayoutBan: TBooleanField;
    MasterQDeleteMasterLayoutBan: TBooleanField;
    MasterQMasterLokasiBan: TBooleanField;
    MasterQInsertMasterLokasiBan: TBooleanField;
    MasterQUpdateMasterLokasiBan: TBooleanField;
    MasterQDeleteMasterLokasiBan: TBooleanField;
    cxDBCheckBox260: TcxDBCheckBox;
    cxDBCheckBox261: TcxDBCheckBox;
    cxDBCheckBox262: TcxDBCheckBox;
    cxDBCheckBox263: TcxDBCheckBox;
    MasterQInsertMasterJenisKendaraan: TBooleanField;
    cxDBCheckBox264: TcxDBCheckBox;
    cxDBCheckBox265: TcxDBCheckBox;
    cxDBCheckBox266: TcxDBCheckBox;
    cxDBCheckBox267: TcxDBCheckBox;
    cxDBCheckBox25: TcxDBCheckBox;
    MasterQApprovalTukarKomponen: TBooleanField;
    cxDBCheckBox26: TcxDBCheckBox;
    cxDBCheckBox27: TcxDBCheckBox;
    MasterQRiwayatHargaBarang: TBooleanField;
    MasterQLaporanBBM: TBooleanField;
    MasterQLaporanSJ: TBooleanField;
    cxDBCheckBox28: TcxDBCheckBox;
    MasterQSkin: TStringField;
    MasterQCetakKwitansi: TBooleanField;
    cxDBCheckBox49: TcxDBCheckBox;
    cxDBCheckBox50: TcxDBCheckBox;
    MasterQPPRekanan: TBooleanField;
    MasterQBKBRekanan: TBooleanField;
    cxDBCheckBox51: TcxDBCheckBox;
    cxDBCheckBox52: TcxDBCheckBox;
    cxDBCheckBox165: TcxDBCheckBox;
    cxDBCheckBox166: TcxDBCheckBox;
    cxDBCheckBox167: TcxDBCheckBox;
    MasterQLaporanSO: TBooleanField;
    MasterQLaporanPerjalananArmada: TBooleanField;
    MasterQLaporanKegTeknik: TBooleanField;
    MasterQLaporanDaftarBarang: TBooleanField;
    MasterQInsertCNCRetur: TBooleanField;
    MasterQUpdateCNCRetur: TBooleanField;
    MasterQDeleteCNCRetur: TBooleanField;
    cxDBCheckBox169: TcxDBCheckBox;
    cxDBCheckBox171: TcxDBCheckBox;
    cxDBCheckBox172: TcxDBCheckBox;
    cxDBCheckBox173: TcxDBCheckBox;
    MasterQInsertMasterRekanan: TBooleanField;
    MasterQUpdateMasterRekanan: TBooleanField;
    MasterQDeleteMasterRekanan: TBooleanField;
    MasterQMasterRekanan: TBooleanField;
    cxDBCheckBox174: TcxDBCheckBox;
    cxDBCheckBox175: TcxDBCheckBox;
    cxDBCheckBox176: TcxDBCheckBox;
    cxDBCheckBox177: TcxDBCheckBox;
    MasterQCNCRetur: TBooleanField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxDBCheckBox178: TcxDBCheckBox;
    cxDBCheckBox182: TcxDBCheckBox;
    cxDBCheckBox183: TcxDBCheckBox;
    cxDBCheckBox268: TcxDBCheckBox;
    cxDBCheckBox179: TcxDBCheckBox;
    cxDBCheckBox180: TcxDBCheckBox;
    cxDBCheckBox181: TcxDBCheckBox;
    cxDBCheckBox269: TcxDBCheckBox;
    cxDBCheckBox270: TcxDBCheckBox;
    cxDBCheckBox271: TcxDBCheckBox;
    MasterQPenjualanSparepart: TBooleanField;
    MasterQInsertPenjualanSpr: TBooleanField;
    MasterQUpdatePenjualanSpr: TBooleanField;
    MasterQDeletePenjualanSpr: TBooleanField;
    MasterQAssemb: TBooleanField;
    MasterQInsertAssemb: TBooleanField;
    MasterQUpdateAssemb: TBooleanField;
    MasterQDeleteAssemb: TBooleanField;
    MasterQApproveAssemb: TBooleanField;
    MasterQLunasKwitansi: TBooleanField;
    MasterQApproveKwitansi: TBooleanField;
    cxDBCheckBox272: TcxDBCheckBox;
    cxDBCheckBox273: TcxDBCheckBox;
    cxDBCheckBox274: TcxDBCheckBox;
    MasterQVerifikasiRKaryawan: TBooleanField;
    MasterQVerifikasiRTrayek: TBooleanField;
    MasterQVerifikasiRSJ: TBooleanField;
    MasterQLaporanRealisasiSuratJalan: TBooleanField;
    MasterQLaporanKasBon: TBooleanField;
    MasterQLaporanKeluarBarang: TBooleanField;
    MasterQLaporanKwitansiSO: TBooleanField;
    MasterQLaporanKlaimSopir: TBooleanField;
    MasterQLaporanRekanan: TBooleanField;
    MasterQMasterPersenPremi: TBooleanField;
    MasterQInsertMasterPersenPremi: TBooleanField;
    MasterQUpdateMasterPersenPremi: TBooleanField;
    MasterQDeleteMasterPersenPremi: TBooleanField;
    cxDBCheckBox275: TcxDBCheckBox;
    cxDBCheckBox276: TcxDBCheckBox;
    cxDBCheckBox277: TcxDBCheckBox;
    cxDBCheckBox278: TcxDBCheckBox;
    cxDBCheckBox279: TcxDBCheckBox;
    cxDBCheckBox280: TcxDBCheckBox;
    cxDBCheckBox281: TcxDBCheckBox;
    cxDBCheckBox282: TcxDBCheckBox;
    cxDBCheckBox283: TcxDBCheckBox;
    cxDBCheckBox284: TcxDBCheckBox;
    cxDBCheckBox285: TcxDBCheckBox;
    MasterQMasterGrupPelanggan: TBooleanField;
    MasterQInsertMasterGrupPelanggan: TBooleanField;
    MasterQUpdateMasterGrupPelanggan: TBooleanField;
    MasterQDeleteMasterGrupPelanggan: TBooleanField;
    cxDBCheckBox286: TcxDBCheckBox;
    cxDBCheckBox287: TcxDBCheckBox;
    cxDBCheckBox288: TcxDBCheckBox;
    cxDBCheckBox289: TcxDBCheckBox;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridKodePegawaiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBCheckBox1Click(Sender: TObject);
    procedure cxDBCheckBox5Click(Sender: TObject);
    procedure cxDBCheckBox9Click(Sender: TObject);
    procedure cxDBCheckBox13Click(Sender: TObject);
    procedure cxDBCheckBox17Click(Sender: TObject);
    procedure cxDBCheckBox21Click(Sender: TObject);
    procedure cxDBCheckBox29Click(Sender: TObject);
    procedure cxDBCheckBox33Click(Sender: TObject);
    procedure cxDBCheckBox37Click(Sender: TObject);
    procedure cxDBCheckBox41Click(Sender: TObject);
    procedure cxDBCheckBox45Click(Sender: TObject);
    procedure cxDBCheckBox53Click(Sender: TObject);
    procedure cxDBCheckBox199Click(Sender: TObject);
    procedure cxDBCheckBox57Click(Sender: TObject);
    procedure cxDBCheckBox61Click(Sender: TObject);
    procedure cxDBCheckBox68Click(Sender: TObject);
    procedure cxDBCheckBox76Click(Sender: TObject);
    procedure cxDBCheckBox74Click(Sender: TObject);
    procedure cxDBCheckBox83Click(Sender: TObject);
    procedure cxDBCheckBox87Click(Sender: TObject);
    procedure cxDBCheckBox88Click(Sender: TObject);
    procedure cxDBCheckBox91Click(Sender: TObject);
    procedure cxDBCheckBox96Click(Sender: TObject);
    procedure cxDBCheckBox100Click(Sender: TObject);
    procedure cxDBCheckBox105Click(Sender: TObject);
    procedure cxDBCheckBox116Click(Sender: TObject);
    procedure cxDBCheckBox120Click(Sender: TObject);
    procedure cxDBCheckBox124Click(Sender: TObject);
    procedure cxDBCheckBox130Click(Sender: TObject);
    procedure cxDBCheckBox131Click(Sender: TObject);
    procedure cxDBCheckBox133Click(Sender: TObject);
    procedure cxDBCheckBox137Click(Sender: TObject);
    procedure cxDBCheckBox141Click(Sender: TObject);
    procedure cxDBCheckBox145Click(Sender: TObject);
    procedure cxDBCheckBox149Click(Sender: TObject);
    procedure cxDBCheckBox203Click(Sender: TObject);
    procedure cxDBCheckBox153Click(Sender: TObject);
    procedure cxDBCheckBox157Click(Sender: TObject);
    procedure cxDBCheckBox161Click(Sender: TObject);
    procedure cxDBCheckBox184Click(Sender: TObject);
    procedure cxDBCheckBox207Click(Sender: TObject);
    procedure cxDBCheckBox213Click(Sender: TObject);
    procedure cxDBCheckBox217Click(Sender: TObject);
    procedure cxDBCheckBox220Click(Sender: TObject);
    procedure cxDBCheckBox224Click(Sender: TObject);
    procedure cxDBCheckBox228Click(Sender: TObject);
    procedure cxDBCheckBox230Click(Sender: TObject);
    procedure cxDBCheckBox236Click(Sender: TObject);
    procedure cxDBCheckBox240Click(Sender: TObject);
    procedure cxDBCheckBox244Click(Sender: TObject);
    procedure cxDBCheckBox248Click(Sender: TObject);
    procedure cxDBCheckBox252Click(Sender: TObject);
    procedure cxDBCheckBox260Click(Sender: TObject);
    procedure cxDBCheckBox264Click(Sender: TObject);
    procedure cxDBCheckBox256Click(Sender: TObject);
    procedure cxDBCheckBox169Click(Sender: TObject);
    procedure cxDBCheckBox174Click(Sender: TObject);
    procedure cxDBCheckBox282Click(Sender: TObject);
    procedure cxDBCheckBox286Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterUserFm: TMasterUserFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, Math, UserDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterUserFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterUserFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterUserFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterUserFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterUserFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterUserFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterUserFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterUserFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterUser.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterUser.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterUser.AsBoolean;
  PegawaiQ.Open;
end;

procedure TMasterUserFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQMasterArmada.AsBoolean:=false;
      MasterQMasterJenisKendaraan.AsBoolean:=false;
      MasterQMasterEkor.AsBoolean:=false;
      MasterQUpdateMasterArmada.AsBoolean:=false;
      MasterQUpdateJenisKendaraan.AsBoolean:=false;
      MasterQUpdateMasterEkor.AsBoolean:=false;
      MasterQUpdateMasterPelanggan.AsBoolean:=false;
      MasterQUpdateMasterRute.AsBoolean:=false;
      MasterQUpdateMasterSopir.AsBoolean:=false;
      MasterQUpdateMasterJarak.AsBoolean:=false;
      MasterQUpdateMasterBan.AsBoolean:=false;
      MasterQUpdateMasterStandardPerawatan .AsBoolean:=false;
      MasterQUpdateMasterSupplier.AsBoolean:=false;
      MasterQUpdateMasterBarang.AsBoolean:=false;
      MasterQUpdateMasterJenisPerbaikan.AsBoolean:=false;
      MasterQUpdateMasterUser.AsBoolean:=false;
      MasterQUpdateMasterMekanik.AsBoolean:=false;
      MasterQUpdateMasterPegawai.AsBoolean:=false;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterUser.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterUser.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TMasterUserFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterUserFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('User dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TMasterUserFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterUserFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterUserFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus User '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('User telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('User pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterUserFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    UserDropDownFm:=TUserDropdownfm.Create(Self);
    if UserDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=UserDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    UserDropDownFm.Release;
end;

procedure TMasterUserFm.MasterVGridEnter(Sender: TObject);
begin
 // MasterVGrid.FocusRow(MasterVGridNama);
end;

procedure TMasterUserFm.MasterVGridKodePegawaiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      Masterq.Edit;
      MasterQKodePegawai.AsString:=PegawaiQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TMasterUserFm.cxDBCheckBox1Click(Sender: TObject);
begin
  if cxdbcheckbox1.Checked then
  begin
    MasterQInsertMasterArmada.AsBoolean:=true;
    MasterQUpdateMasterArmada.AsBoolean:=true;
    MasterQDeleteMasterArmada.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterArmada.AsBoolean:=false;
    MasterQUpdateMasterArmada.AsBoolean:=false;
    MasterQDeleteMasterArmada.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox5Click(Sender: TObject);
begin
  if cxdbcheckbox5.Checked then
  begin
    MasterQInsertMasterUser.AsBoolean:=true;
    MasterQUpdateMasterUser.AsBoolean:=true;
    MasterQDeleteMasterUser.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterUser.AsBoolean:=false;
    MasterQUpdateMasterUser.AsBoolean:=false;
    MasterQDeleteMasterUser.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox9Click(Sender: TObject);
begin
  if cxdbcheckbox9.Checked then
  begin
    MasterQInsertMasterBKanibal.AsBoolean:=true;
    MasterQUpdateMasterBKanibal.AsBoolean:=true;
    MasterQDeleteMasterBKanibal.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterBKanibal.AsBoolean:=false;
    MasterQUpdateMasterBKanibal.AsBoolean:=false;
    MasterQDeleteMasterBKanibal.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox13Click(Sender: TObject);
begin
  if cxdbcheckbox13.Checked then
  begin
    MasterQInsertMasterKBarang.AsBoolean:=true;
    MasterQUpdateMasterKBarang.AsBoolean:=true;
    MasterQDeleteMasterKBarang.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterKBarang.AsBoolean:=false;
    MasterQUpdateMasterKBarang.AsBoolean:=false;
    MasterQDeleteMasterKBarang.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox17Click(Sender: TObject);
begin
  if cxdbcheckbox17.Checked then
  begin
    MasterQInsertMasterRute.AsBoolean:=true;
    MasterQUpdateMasterRute.AsBoolean:=true;
    MasterQDeleteMasterRute.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterRute.AsBoolean:=false;
    MasterQUpdateMasterRute.AsBoolean:=false;
    MasterQDeleteMasterRute.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox21Click(Sender: TObject);
begin
  if cxdbcheckbox21.Checked then
  begin
    MasterQInsertMasterAC.AsBoolean:=true;
    MasterQUpdateMasterAC.AsBoolean:=true;
    MasterQDeleteMasterAC.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterAC.AsBoolean:=false;
    MasterQUpdateMasterAC.AsBoolean:=false;
    MasterQDeleteMasterAC.AsBoolean:=false;
  end;
end;



procedure TMasterUserFm.cxDBCheckBox29Click(Sender: TObject);
begin
  if cxdbcheckbox29.Checked then
  begin
    MasterQInsertMasterBan.AsBoolean:=true;
    MasterQUpdateMasterBan.AsBoolean:=true;
    MasterQDeleteMasterBan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterBan.AsBoolean:=false;
    MasterQUpdateMasterBan.AsBoolean:=false;
    MasterQDeleteMasterBan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox33Click(Sender: TObject);
begin
  if cxdbcheckbox33.Checked then
  begin
    MasterQInsertMasterBarang.AsBoolean:=true;
    MasterQUpdateMasterBarang.AsBoolean:=true;
    MasterQDeleteMasterBarang.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterBarang.AsBoolean:=false;
    MasterQUpdateMasterBarang.AsBoolean:=false;
    MasterQDeleteMasterBarang.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox37Click(Sender: TObject);
begin
  if cxdbcheckbox37.Checked then
  begin
    MasterQInsertMasterSupplier.AsBoolean:=true;
    MasterQUpdateMasterSupplier.AsBoolean:=true;
    MasterQDeleteMasterSupplier.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterSupplier.AsBoolean:=false;
    MasterQUpdateMasterSupplier.AsBoolean:=false;
    MasterQDeleteMasterSupplier.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox41Click(Sender: TObject);
begin
  if cxdbcheckbox41.Checked then
  begin
    MasterQInsertMasterPegawai.AsBoolean:=true;
    MasterQUpdateMasterPegawai.AsBoolean:=true;
    MasterQDeleteMasterPegawai.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterPegawai.AsBoolean:=false;
    MasterQUpdateMasterPegawai.AsBoolean:=false;
    MasterQDeleteMasterPegawai.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox45Click(Sender: TObject);
begin
  if cxdbcheckbox45.Checked then
  begin
    MasterQInsertMasterJenisPerbaikan.AsBoolean:=true;
    MasterQUpdateMasterJenisPerbaikan.AsBoolean:=true;
    MasterQDeleteMasterJenisPerbaikan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterJenisPerbaikan.AsBoolean:=false;
    MasterQUpdateMasterJenisPerbaikan.AsBoolean:=false;
    MasterQDeleteMasterJenisPerbaikan.AsBoolean:=false;
  end;
end;



procedure TMasterUserFm.cxDBCheckBox53Click(Sender: TObject);
begin
  if cxdbcheckbox53.Checked then
  begin
    MasterQInsertMasterPelanggan.AsBoolean:=true;
    MasterQUpdateMasterPelanggan.AsBoolean:=true;
    MasterQDeleteMasterPelanggan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterPelanggan.AsBoolean:=false;
    MasterQUpdateMasterPelanggan.AsBoolean:=false;
    MasterQDeleteMasterPelanggan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox199Click(Sender: TObject);
begin
  if cxdbcheckbox199.Checked then
  begin
    MasterQInsertMasterStandardPerawatan.AsBoolean:=true;
    MasterQUpdateMasterStandardPerawatan.AsBoolean:=true;
    MasterQDeleteMasterStandardPerawatan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterStandardPerawatan.AsBoolean:=false;
    MasterQUpdateMasterStandardPerawatan.AsBoolean:=false;
    MasterQDeleteMasterStandardPerawatan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox57Click(Sender: TObject);
begin
  if cxdbcheckbox57.Checked then
  begin
    MasterQInsertKontrak.AsBoolean:=true;
    MasterQUpdateKontrak.AsBoolean:=true;
    MasterQDeleteKontrak.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKontrak.AsBoolean:=false;
    MasterQUpdateKontrak.AsBoolean:=false;
    MasterQDeleteKontrak.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox61Click(Sender: TObject);
begin
  if cxdbcheckbox61.Checked then
  begin
    MasterQInsertSO.AsBoolean:=true;
    MasterQUpdateSO.AsBoolean:=true;
    MasterQDeleteSO.AsBoolean:=true;
    MasterQSetHargaSO.AsBoolean:=true;
    MasterQSetStatusSO.AsBoolean:=true;
    MasterQViewHargaSO.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertSO.AsBoolean:=false;
    MasterQUpdateSO.AsBoolean:=false;
    MasterQDeleteSO.AsBoolean:=false;
    MasterQSetHargaSO.AsBoolean:=false;
    MasterQSetStatusSO.AsBoolean:=false;
    MasterQViewHargaSO.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox68Click(Sender: TObject);
begin
  if cxdbcheckbox68.Checked then
  begin
    MasterQInsertKeluhanPelanggan.AsBoolean:=true;
    MasterQUpdateKeluhanPelanggan.AsBoolean:=true;
    MasterQDeleteKeluhanPelanggan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKeluhanPelanggan.AsBoolean:=false;
    MasterQUpdateKeluhanPelanggan.AsBoolean:=false;
    MasterQDeleteKeluhanPelanggan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox76Click(Sender: TObject);
begin
  if cxdbcheckbox76.Checked then
  begin
    MasterQUpdatePilihArmada.AsBoolean:=true;
  end
  else
  begin
    MasterQUpdatePilihArmada.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox74Click(Sender: TObject);
begin
  if cxdbcheckbox74.Checked then
  begin
    MasterQInsertSuratJalan.AsBoolean:=true;
    MasterQUpdateSuratJalan.AsBoolean:=true;
    MasterQDeleteSuratJalan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertSuratJalan.AsBoolean:=false;
    MasterQUpdateSuratJalan.AsBoolean:=false;
    MasterQDeleteSuratJalan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox83Click(Sender: TObject);
begin
  if cxdbcheckbox83.Checked then
  begin
    MasterQInsertRealisasiSuratJalan.AsBoolean:=true;
    MasterQUpdateRealisasiSuratJalan.AsBoolean:=true;
    MasterQDeleteRealisasiSuratJalan.AsBoolean:=true;
    MasterQVerifikasiRSJ.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertRealisasiSuratJalan.AsBoolean:=false;
    MasterQUpdateRealisasiSuratJalan.AsBoolean:=false;
    MasterQDeleteRealisasiSuratJalan.AsBoolean:=false;
    MasterQVerifikasiRSJ.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox87Click(Sender: TObject);
begin
  if cxdbcheckbox87.Checked then
  begin
    MasterQInsertStoring.AsBoolean:=true;
    MasterQUpdateStoring.AsBoolean:=true;
    MasterQDeleteStoring.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertStoring.AsBoolean:=false;
    MasterQUpdateStoring.AsBoolean:=false;
    MasterQDeleteStoring.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox88Click(Sender: TObject);
begin
  if cxdbcheckbox88.Checked then
  begin
    MasterQUpdateDaftarBeli.AsBoolean:=true;
    MasterQPilihSupplierDaftarBeli.AsBoolean:=true;
  end
  else
  begin
    MasterQUpdateDaftarBeli.AsBoolean:=false;
    MasterQPilihSupplierDaftarBeli.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox91Click(Sender: TObject);
begin
  if cxdbcheckbox91.Checked then
  begin
    MasterQInsertPO.AsBoolean:=true;
    MasterQUpdatePO.AsBoolean:=true;
    MasterQDeletePO.AsBoolean:=true;
    MasterQApprovalPO.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPO.AsBoolean:=false;
    MasterQUpdatePO.AsBoolean:=false;
    MasterQDeletePO.AsBoolean:=false;
    MasterQApprovalPO.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox96Click(Sender: TObject);
begin
  if cxdbcheckbox96.Checked then
  begin
    MasterQInsertCashNCarry.AsBoolean:=true;
    MasterQUpdateCashNCarry.AsBoolean:=true;
    MasterQDeleteCashNCarry.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertCashNCarry.AsBoolean:=false;
    MasterQUpdateCashNCarry.AsBoolean:=false;
    MasterQDeleteCashNCarry.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox100Click(Sender: TObject);
begin
  if cxdbcheckbox100.Checked then
  begin
    MasterQInsertKomplainNRetur.AsBoolean:=true;
    MasterQUpdateKomplainRetur.AsBoolean:=true;
    MasterQDeleteKomplainRetur.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKomplainNRetur.AsBoolean:=false;
    MasterQUpdateKomplainRetur.AsBoolean:=false;
    MasterQDeleteKomplainRetur.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox105Click(Sender: TObject);
begin
  if cxdbcheckbox105.Checked then
  begin
    MasterQInsertBonBarang.AsBoolean:=true;
    MasterQUpdateBonBarang.AsBoolean:=true;
    MasterQDeleteBonBarang.AsBoolean:=true;
    MasterQTambahStokBonBarang.AsBoolean:=true;
    MasterQPerbaikanBonBarang.AsBoolean:=true;
    MasterQPerawatanBonBarang.AsBoolean:=true;
    MasterQSetBeliBonBarang.AsBoolean:=true;
    MasterQSetMintaBonBarang.AsBoolean:=true;
    MasterQSetUrgentBonBarang.AsBoolean:=true;
    MasterQProcessBonBarang.AsBoolean:=true;
    MasterQApprovalBonBarang.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertBonBarang.AsBoolean:=false;
    MasterQUpdateBonBarang.AsBoolean:=false;
    MasterQDeleteBonBarang.AsBoolean:=false;
    MasterQTambahStokBonBarang.AsBoolean:=false;
    MasterQPerbaikanBonBarang.AsBoolean:=false;
    MasterQPerawatanBonBarang.AsBoolean:=false;
    MasterQSetBeliBonBarang.AsBoolean:=false;
    MasterQSetMintaBonBarang.AsBoolean:=false;
    MasterQSetUrgentBonBarang.AsBoolean:=false;
    MasterQProcessBonBarang.AsBoolean:=false;
    MasterQApprovalBonBarang.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox116Click(Sender: TObject);
begin
  if cxdbcheckbox116.Checked then
  begin
    MasterQInsertBonKeluarBarang.AsBoolean:=true;
    MasterQUpdateBonKeluarBarang.AsBoolean:=true;
    MasterQDeleteBonKeluarBarang.AsBoolean:=true;
    MasterQBKBRekanan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertBonKeluarBarang.AsBoolean:=false;
    MasterQUpdateBonKeluarBarang.AsBoolean:=false;
    MasterQDeleteBonKeluarBarang.AsBoolean:=false;
    MasterQBKBRekanan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox120Click(Sender: TObject);
begin
  if cxdbcheckbox120.Checked then
  begin
    MasterQInsertLPB.AsBoolean:=true;
    MasterQUpdateLPB.AsBoolean:=true;
    MasterQDeleteLPB.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertLPB.AsBoolean:=false;
    MasterQUpdateLPB.AsBoolean:=false;
    MasterQDeleteLPB.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox124Click(Sender: TObject);
begin
  if cxdbcheckbox124.Checked then
  begin
    MasterQInsertPemutihan.AsBoolean:=true;
    MasterQUpdatePemutihan.AsBoolean:=true;
    MasterQDeletePemutihan.AsBoolean:=true;
    MasterQApprovalPemutihan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPemutihan.AsBoolean:=false;
    MasterQUpdatePemutihan.AsBoolean:=false;
    MasterQDeletePemutihan.AsBoolean:=false;
    MasterQApprovalPemutihan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox130Click(Sender: TObject);
begin
  if cxdbcheckbox130.Checked then
  begin
    MasterQLaporanRiwayatStokBarang.AsBoolean:=true;
    MasterQLaporanRiwayatHargaBarang.AsBoolean:=true;
  end
  else
  begin
    MasterQLaporanRiwayatStokBarang.AsBoolean:=false;
    MasterQLaporanRiwayatHargaBarang.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox131Click(Sender: TObject);
begin
  if cxDBCheckBox131.Checked then
  begin
    MasterQInsertNotifikasiPerawatan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertNotifikasiPerawatan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox133Click(Sender: TObject);
begin
  if cxdbcheckbox133.Checked then
  begin
    MasterQInsertPermintaanPerbaikan.AsBoolean:=true;
    MasterQUpdatePermintaanPerbaikan.AsBoolean:=true;
    MasterQDeletePermintaanPerbaikan.AsBoolean:=true;
    MasterQPPRekanan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPermintaanPerbaikan.AsBoolean:=false;
    MasterQUpdatePermintaanPerbaikan.AsBoolean:=false;
    MasterQDeletePermintaanPerbaikan.AsBoolean:=false;
    MasterQPPRekanan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox137Click(Sender: TObject);
begin
  if cxdbcheckbox137.Checked then
  begin
    MasterQInsertPerbaikan.AsBoolean:=true;
    MasterQUpdatePerbaikan.AsBoolean:=true;
    MasterQDeletePerbaikan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPerbaikan.AsBoolean:=false;
    MasterQUpdatePerbaikan.AsBoolean:=false;
    MasterQDeletePerbaikan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox141Click(Sender: TObject);
begin
  if cxdbcheckbox141.Checked then
  begin
    MasterQInsertPerawatan.AsBoolean:=true;
    MasterQUpdatePerawatan.AsBoolean:=true;
    MasterQDeletePerawatan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPerawatan.AsBoolean:=false;
    MasterQUpdatePerawatan.AsBoolean:=false;
    MasterQDeletePerawatan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox145Click(Sender: TObject);
begin
  if cxdbcheckbox145.Checked then
  begin
    MasterQInsertLepasPasangBan.AsBoolean:=true;
    MasterQUpdateLepasPasangBan.AsBoolean:=true;
    MasterQDeleteLepasPasangBan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertLepasPasangBan.AsBoolean:=false;
    MasterQUpdateLepasPasangBan.AsBoolean:=false;
    MasterQDeleteLepasPasangBan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox149Click(Sender: TObject);
begin
  if cxdbcheckbox149.Checked then
  begin
    MasterQInsertPerbaikanBan.AsBoolean:=true;
    MasterQUpdatePerbaikanBan.AsBoolean:=true;
    MasterQDeletePerbaikanBan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPerbaikanBan.AsBoolean:=false;
    MasterQUpdatePerbaikanBan.AsBoolean:=false;
    MasterQDeletePerbaikanBan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox203Click(Sender: TObject);
begin
  if cxdbcheckbox203.Checked then
  begin
    MasterQInsertTukarKomponen.AsBoolean:=true;
    MasterQUpdateTukarKomponen.AsBoolean:=true;
    MasterQDeleteTukarKomponen.AsBoolean:=true;
    MasterQApprovalTukarKomponen.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertTukarKomponen.AsBoolean:=false;
    MasterQUpdateTukarKomponen.AsBoolean:=false;
    MasterQDeleteTukarKomponen.AsBoolean:=false;
    MasterQApprovalTukarKomponen.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox153Click(Sender: TObject);
begin
  If cxDBCheckBox153.Checked then
  begin
    MasterQInsertKasBon.AsBoolean:=true;
    MasterQUpdateKasBon.AsBoolean:=true;
    MasterQDeleteKasBon.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKasBon.AsBoolean:=false;
    MasterQUpdateKasBon.AsBoolean:=false;
    MasterQDeleteKasBon.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox157Click(Sender: TObject);
begin
  If cxDBCheckBox157.Checked then
  begin
    MasterQInsertPenagihan.AsBoolean:=true;
    MasterQUpdatePenagihan.AsBoolean:=true;
    MasterQDeletePenagihan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPenagihan.AsBoolean:=false;
    MasterQUpdatePenagihan.AsBoolean:=false;
    MasterQDeletePenagihan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox161Click(Sender: TObject);
begin
  If cxDBCheckBox161.Checked then
  begin
    MasterQInsertBayarPO.AsBoolean:=true;
    MasterQUpdateBayarPO.AsBoolean:=true;
    MasterQDeleteBayarPO.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertBayarPO.AsBoolean:=false;
    MasterQUpdateBayarPO.AsBoolean:=false;
    MasterQDeleteBayarPO.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox184Click(Sender: TObject);
begin
  if cxDBCheckBox184.Checked then
  begin
    MasterQFollowUpCustomer.AsBoolean:=true;
    MasterQInactiveCustomer.AsBoolean:=true;
    MasterQPembuatanSuratJalan.AsBoolean:=true;
    MasterQPembuatanSuratJalan.AsBoolean:=true;
    MasterQNewBonBarang.AsBoolean:=true;
    MasterQNewDaftarBeli.AsBoolean:=true;
    MasterQNewPermintaanPerbaikan.AsBoolean:=true;
    MasterQNotifPilihArmada.AsBoolean:=true;
    MasterQNewLPB.AsBoolean:=true;
    MasterQMinimumStok.AsBoolean:=true;
    MasterQSTNKExpired.AsBoolean:=true;
    MasterQSTNKPajakExpired.AsBoolean:=true;
    MasterQKirExpired.AsBoolean:=true;
    MasterQSimExpired.AsBoolean:=true;
  end
  else
  begin
    MasterQFollowUpCustomer.AsBoolean:=false;
    MasterQInactiveCustomer.AsBoolean:=false;
    MasterQPembuatanSuratJalan.AsBoolean:=false;
    MasterQPembuatanSuratJalan.AsBoolean:=false;
    MasterQNewBonBarang.AsBoolean:=false;
    MasterQNewDaftarBeli.AsBoolean:=false;
    MasterQNewPermintaanPerbaikan.AsBoolean:=false;
    MasterQNotifPilihArmada.AsBoolean:=false;
    MasterQNewLPB.AsBoolean:=false;
    MasterQMinimumStok.AsBoolean:=false;
    MasterQSTNKExpired.AsBoolean:=false;
    MasterQSTNKPajakExpired.AsBoolean:=false;
    MasterQKirExpired.AsBoolean:=false;
    MasterQSimExpired.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox207Click(Sender: TObject);
begin
  If cxDBCheckBox207.Checked then
  begin
    MasterQSettingParameterNotifikasi.AsBoolean:=true;
    MasterQSettingWaktuNotifikasi.AsBoolean:=true;
  end
  else
  begin
    MasterQSettingParameterNotifikasi.AsBoolean:=false;
    MasterQSettingWaktuNotifikasi.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox213Click(Sender: TObject);
begin
  If cxDBCheckBox213.Checked then
  begin
    MasterQInsertKasBonAnjem.AsBoolean:=true;
    MasterQUpdateKasBonAnjem.AsBoolean:=true;
    MasterQDeleteKasBonAnjem.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKasBonAnjem.AsBoolean:=false;
    MasterQUpdateKasBonAnjem.AsBoolean:=false;
    MasterQDeleteKasBonAnjem.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox217Click(Sender: TObject);
begin
  If cxDBCheckBox217.Checked then
  begin
    MasterQInsertPembuatanKwitansi.AsBoolean:=true;
    MasterQUpdatePembuatanKwitansi.AsBoolean:=true;
    MasterQDeletePembuatanKwitansi.AsBoolean:=true;
    MasterQCetakKwitansi.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPembuatanKwitansi.AsBoolean:=false;
    MasterQUpdatePembuatanKwitansi.AsBoolean:=false;
    MasterQDeletePembuatanKwitansi.AsBoolean:=false;
    MasterQCetakKwitansi.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox220Click(Sender: TObject);
begin
  if cxdbcheckbox220.Checked then
  begin
    MasterQInsertRealisasiAnjem.AsBoolean:=true;
    MasterQUpdateRealisasiAnjem.AsBoolean:=true;
    MasterQDeleteRealisasiAnjem.AsBoolean:=true;
    MasterQVerifikasiRKaryawan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertRealisasiAnjem.AsBoolean:=false;
    MasterQUpdateRealisasiAnjem.AsBoolean:=false;
    MasterQDeleteRealisasiAnjem.AsBoolean:=false;
    MasterQVerifikasiRKaryawan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox224Click(Sender: TObject);
begin
  if cxdbcheckbox224.Checked then
  begin
    MasterQInsertRealisasiTrayek.AsBoolean:=true;
    MasterQUpdateRealisasiTrayek.AsBoolean:=true;
    MasterQDeleteRealisasiTrayek.AsBoolean:=true;
    MasterQVerifikasiRTrayek.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertRealisasiTrayek.AsBoolean:=false;
    MasterQUpdateRealisasiTrayek.AsBoolean:=false;
    MasterQDeleteRealisasiTrayek.AsBoolean:=false;
    MasterQVerifikasiRTrayek.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox228Click(Sender: TObject);
begin
  if cxdbcheckbox228.Checked then
  begin
    MasterQUpdateUpdateKm.AsBoolean:=true;
  end
  else
  begin
MasterQUpdateUpdateKm.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox230Click(Sender: TObject);
begin
  if cxdbcheckbox230.Checked then
  begin
    MasterQInsertPOUmum.AsBoolean:=true;
    MasterQUpdatePOUmum.AsBoolean:=true;
    MasterQDeletePOUmum.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertPOUmum.AsBoolean:=false;
    MasterQUpdatePOUmum.AsBoolean:=false;
    MasterQDeletePOUmum.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox236Click(Sender: TObject);
begin
  if cxdbcheckbox236.Checked then
  begin
    MasterQInsertKeluarMasukBarangBekas.AsBoolean:=true;
    MasterQUpdateKeluarMasukBarangBekas.AsBoolean:=true;
    MasterQDeleteKeluarMasukBarangBekas.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertKeluarMasukBarangBekas.AsBoolean:=false;
    MasterQUpdateKeluarMasukBarangBekas.AsBoolean:=false;
    MasterQDeleteKeluarMasukBarangBekas.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox240Click(Sender: TObject);
begin
  if cxdbcheckbox240.Checked then
  begin
    MasterQInsertRebuild.AsBoolean:=true;
    MasterQUpdateRebuild.AsBoolean:=true;
    MasterQDeleteRebuild.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertRebuild.AsBoolean:=false;
    MasterQUpdateRebuild.AsBoolean:=false;
    MasterQDeleteRebuild.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox244Click(Sender: TObject);
begin
  if cxdbcheckbox244.Checked then
  begin
    MasterQInsertSPK.AsBoolean:=true;
    MasterQUpdateSPK.AsBoolean:=true;
    MasterQDeleteSPK.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertSPK.AsBoolean:=false;
    MasterQUpdateSPK.AsBoolean:=false;
    MasterQDeleteSPK.AsBoolean:=false;
  end;

end;

procedure TMasterUserFm.cxDBCheckBox248Click(Sender: TObject);
begin
  if cxdbcheckbox248.Checked then
  begin
    MasterQInsertAmbilBarangKanibal.AsBoolean:=true;
    MasterQUpdateAmbilBarangKanibal.AsBoolean:=true;
    MasterQDeleteAmbilBarangKanibal.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertAmbilBarangKanibal.AsBoolean:=false;
    MasterQUpdateAmbilBarangKanibal.AsBoolean:=false;
    MasterQDeleteAmbilBarangKanibal.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox252Click(Sender: TObject);
begin
  if cxdbcheckbox252.Checked then
  begin
    MasterQInsertMasterBarangBekas.AsBoolean:=true;
    MasterQUpdateMasterBarangBekas.AsBoolean:=true;
    MasterQDeleteMasterBarangBekas.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterBarangBekas.AsBoolean:=false;
    MasterQUpdateMasterBarangBekas.AsBoolean:=false;
    MasterQDeleteMasterBarangBekas.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox260Click(Sender: TObject);
begin
  if cxdbcheckbox260.Checked then
  begin
    MasterQInsertMasterLayoutBan.AsBoolean:=true;
    MasterQUpdateMasterLayoutBan.AsBoolean:=true;
    MasterQDeleteMasterLayoutBan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterLayoutBan.AsBoolean:=false;
    MasterQUpdateMasterLayoutBan.AsBoolean:=false;
    MasterQDeleteMasterLayoutBan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox264Click(Sender: TObject);
begin
  if cxdbcheckbox264.Checked then
  begin
    MasterQInsertMasterLokasiBan.AsBoolean:=true;
    MasterQUpdateMasterLokasiBan.AsBoolean:=true;
    MasterQDeleteMasterLokasiBan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterLokasiBan.AsBoolean:=false;
    MasterQUpdateMasterLokasiBan.AsBoolean:=false;
    MasterQDeleteMasterLokasiBan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox256Click(Sender: TObject);
begin
  if cxdbcheckbox256.Checked then
  begin
    MasterQInsertJenisKendaraan.AsBoolean:=true;
    MasterQUpdateJenisKendaraan.AsBoolean:=true;
    MasterQDeleteJenisKendaraan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertJenisKendaraan.AsBoolean:=false;
    MasterQUpdateJenisKendaraan.AsBoolean:=false;
    MasterQDeleteJenisKendaraan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox169Click(Sender: TObject);
begin
  if cxdbcheckbox169.Checked then
  begin
    MasterQInsertMasterRekanan.AsBoolean:=true;
    MasterQUpdateMasterRekanan.AsBoolean:=true;
    MasterQDeleteMasterRekanan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterRekanan.AsBoolean:=false;
    MasterQUpdateMasterRekanan.AsBoolean:=false;
    MasterQDeleteMasterRekanan.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox174Click(Sender: TObject);
begin
  if cxdbcheckbox174.Checked then
  begin
    MasterQInsertCNCRetur.AsBoolean:=true;
    MasterQUpdateCNCRetur.AsBoolean:=true;
    MasterQDeleteCNCRetur.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertCNCRetur.AsBoolean:=false;
    MasterQUpdateCNCRetur.AsBoolean:=false;
    MasterQDeleteCNCRetur.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox282Click(Sender: TObject);
begin
  if cxDBCheckBox282.Checked then
  begin
    MasterQInsertMasterPersenPremi.AsBoolean:=true;
    MasterQUpdateMasterPersenPremi.AsBoolean:=true;
    MasterQDeleteMasterPersenPremi.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterPersenPremi.AsBoolean:=false;
    MasterQUpdateMasterPersenPremi.AsBoolean:=false;
    MasterQDeleteMasterPersenPremi.AsBoolean:=false;
  end;
end;

procedure TMasterUserFm.cxDBCheckBox286Click(Sender: TObject);
begin
  if cxDBCheckBox286.Checked then
  begin
    MasterQInsertMasterGrupPelanggan.AsBoolean:=true;
    MasterQUpdateMasterGrupPelanggan.AsBoolean:=true;
    MasterQDeleteMasterGrupPelanggan.AsBoolean:=true;
  end
  else
  begin
    MasterQInsertMasterGrupPelanggan.AsBoolean:=false;
    MasterQUpdateMasterGrupPelanggan.AsBoolean:=false;
    MasterQDeleteMasterGrupPelanggan.AsBoolean:=false;
  end;
end;

end.
