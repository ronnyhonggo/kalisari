unit MenuUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, DB, SDEngine, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxListBox, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxGDIPlusClasses, cxImage, jpeg, ExtCtrls, gifimage,
  cxLabel, dxSkinsdxBarPainter, dxBar, cxClasses, cxCheckBox, cxRadioGroup,
  cxBarEditItem, dxSkinsForm, cxGridCardView, cxStyles, cxGridTableView;

type
  TMenuUtamaFm = class(TForm)
    MainMenu1: TMainMenu;
    Master1: TMenuItem;
    ransaksi1: TMenuItem;
    Armada1: TMenuItem;
    DataBase1: TSDDatabase;
    UserQ: TSDQuery;
    SO1: TMenuItem;
    SuratJalan1: TMenuItem;
    Fitur1: TMenuItem;
    JadwalArmada1: TMenuItem;
    Pelanggan1: TMenuItem;
    Rute1: TMenuItem;
    User1: TMenuItem;
    PilihOrder1: TMenuItem;
    JenisKendaraan1: TMenuItem;
    Sopir2: TMenuItem;
    Perbaikan1: TMenuItem;
    Barang1: TMenuItem;
    Perawatan1: TMenuItem;
    Ban2: TMenuItem;
    Kontrak1: TMenuItem;
    Perjalanan1: TMenuItem;
    Storing2: TMenuItem;
    KasBon3: TMenuItem;
    KeluhanPelanggan2: TMenuItem;
    DaftarPelanggan1: TMenuItem;
    RealisasiSuratJalan1: TMenuItem;
    Laporan1: TMenuItem;
    Image1: TImage;
    cxImage1: TcxImage;
    Laporan2: TMenuItem;
    LaporanOrderTidakSesuai1: TMenuItem;
    LaporanKeluhanPelanggan1: TMenuItem;
    LaporanPendapatan1: TMenuItem;
    LaporanPemenuhanKontrak1: TMenuItem;
    LaporanEfisiensiMuatan1: TMenuItem;
    LaporanPendapatandanPoinSopir1: TMenuItem;
    LaporanPenggunaanBan1: TMenuItem;
    Notifikasi1: TMenuItem;
    ampilkan1: TMenuItem;
    Setting1: TMenuItem;
    Timer1: TTimer;
    cxLabel1: TcxLabel;
    Penagihan1: TMenuItem;
    LaporanSalesOrder1: TMenuItem;
    Pembelian1: TMenuItem;
    Gudang1: TMenuItem;
    eknik1: TMenuItem;
    NotifikasiPerawatan1: TMenuItem;
    DaftarBeli1: TMenuItem;
    PO1: TMenuItem;
    CashAndCarry1: TMenuItem;
    KomplaindanRetur1: TMenuItem;
    BonBarang1: TMenuItem;
    DaftarBeli2: TMenuItem;
    LaporanPenerimaanBarang1: TMenuItem;
    Pemutihan1: TMenuItem;
    PermintaanPerbaikan1: TMenuItem;
    LaporanPerbaikan1: TMenuItem;
    LaporanPerawatan1: TMenuItem;
    LepasPasangBan1: TMenuItem;
    PerbaikanBan1: TMenuItem;
    DaftarSupplier1: TMenuItem;
    DaftarMinimumStok1: TMenuItem;
    DaftarStokBarang1: TMenuItem;
    BayarPO1: TMenuItem;
    LaporanPenggunaanBarang1: TMenuItem;
    LaporanKegiatanMekanik1: TMenuItem;
    LaporanKegiatanTeknik1: TMenuItem;
    LaporanRiwayatKendaraan1: TMenuItem;
    LaporanPemenuhanBarang1: TMenuItem;
    LaporanPenerimaandanPembelianBarang1: TMenuItem;
    LaporanPenambahanOli1: TMenuItem;
    LaporanRetur1: TMenuItem;
    LaporanPenjualanPerUser1: TMenuItem;
    LaporanProfitPerSO1: TMenuItem;
    NotifQ: TSDQuery;
    Pegawai1: TMenuItem;
    ukarKomponen1: TMenuItem;
    RealisasiAnJem1: TMenuItem;
    RealisasiTrayek1: TMenuItem;
    AC1: TMenuItem;
    SuratPerintahKerja1: TMenuItem;
    Rebuild1: TMenuItem;
    BarangKanibal1: TMenuItem;
    KategoriBarang1: TMenuItem;
    AmbilBarangKanibal1: TMenuItem;
    DaftarMinimumStokKategori1: TMenuItem;
    KasBonAnjem1: TMenuItem;
    PenagihanAnjem1: TMenuItem;
    ViewJadwal1: TMenuItem;
    BarangBekas1: TMenuItem;
    KeluarMasukBarangBekas1: TMenuItem;
    PembuatanKwitansi1: TMenuItem;
    UpdateKm1: TMenuItem;
    LayoutBan: TMenuItem;
    LokasiBan: TMenuItem;
    POUmum1: TMenuItem;
    LaporanPemakaianBBM1: TMenuItem;
    LaporanSuratJalan1: TMenuItem;
    RiwayatHargaBarang1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    ACMenu: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    ArmadaMenu: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    Master: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton18: TdxBarButton;
    dxBarButton19: TdxBarButton;
    dxBarButton20: TdxBarButton;
    dxBarButton21: TdxBarButton;
    dxBarButton22: TdxBarButton;
    dxBarButton23: TdxBarButton;
    dxBarButton24: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarButton26: TdxBarButton;
    dxBarButton27: TdxBarButton;
    dxBarButton28: TdxBarButton;
    dxBarButton29: TdxBarButton;
    dxBarButton30: TdxBarButton;
    dxBarButton31: TdxBarButton;
    dxBarButton32: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton33: TdxBarButton;
    dxBarButton34: TdxBarButton;
    dxBarButton35: TdxBarButton;
    dxBarButton36: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton37: TdxBarButton;
    dxBarButton38: TdxBarButton;
    dxBarButton39: TdxBarButton;
    dxBarButton40: TdxBarButton;
    dxBarButton41: TdxBarButton;
    dxBarButton42: TdxBarButton;
    dxBarButton43: TdxBarButton;
    dxBarButton44: TdxBarButton;
    dxBarButton45: TdxBarButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton46: TdxBarButton;
    dxBarButton47: TdxBarButton;
    dxBarButton48: TdxBarButton;
    dxBarButton49: TdxBarButton;
    dxBarButton50: TdxBarButton;
    dxBarButton51: TdxBarButton;
    dxBarButton52: TdxBarButton;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton53: TdxBarButton;
    dxBarButton54: TdxBarButton;
    dxBarButton55: TdxBarButton;
    dxBarButton56: TdxBarButton;
    dxBarButton57: TdxBarButton;
    dxBarButton58: TdxBarButton;
    dxBarButton59: TdxBarButton;
    dxBarButton60: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarButton61: TdxBarButton;
    dxBarButton62: TdxBarButton;
    dxBarButton63: TdxBarButton;
    dxBarButton64: TdxBarButton;
    dxBarButton65: TdxBarButton;
    dxBarButton66: TdxBarButton;
    dxBarButton67: TdxBarButton;
    dxBarButton68: TdxBarButton;
    dxBarButton69: TdxBarButton;
    dxBarButton70: TdxBarButton;
    dxBarSubItem8: TdxBarSubItem;
    dxBarSubItem9: TdxBarSubItem;
    dxBarButton71: TdxBarButton;
    dxBarButton72: TdxBarButton;
    dxBarButton73: TdxBarButton;
    dxBarButton74: TdxBarButton;
    dxBarSubItem10: TdxBarSubItem;
    dxBarButton75: TdxBarButton;
    dxBarButton76: TdxBarButton;
    dxBarButton77: TdxBarButton;
    dxBarButton78: TdxBarButton;
    dxBarSubItem11: TdxBarSubItem;
    dxBarButton79: TdxBarButton;
    dxBarButton80: TdxBarButton;
    dxBarSubItem12: TdxBarSubItem;
    dxBarButton81: TdxBarButton;
    cxBarEditItem1: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    dxBarButton82: TdxBarButton;
    dxSkinController1: TdxSkinController;
    dxBarButton83: TdxBarButton;
    dxBarButton84: TdxBarButton;
    dxBarButton85: TdxBarButton;
    dxBarButton86: TdxBarButton;
    dxBarSubItem13: TdxBarSubItem;
    dxBarButton87: TdxBarButton;
    dxBarButton88: TdxBarButton;
    dxBarButton89: TdxBarButton;
    dxBarButton90: TdxBarButton;
    dxBarButton91: TdxBarButton;
    dxBarButton92: TdxBarButton;
    dxBarButton93: TdxBarButton;
    dxBarButton94: TdxBarButton;
    dxBarButton95: TdxBarButton;
    dxBarButton96: TdxBarButton;
    dxBarButton97: TdxBarButton;
    dxBarButton98: TdxBarButton;
    dxBarButton99: TdxBarButton;
    dxBarButton100: TdxBarButton;
    dxBarButton101: TdxBarButton;
    dxBarButton102: TdxBarButton;
    dxBarButton103: TdxBarButton;
    dxBarButton104: TdxBarButton;
    dxBarButton105: TdxBarButton;
    dxBarButton106: TdxBarButton;
    dxBarButton107: TdxBarButton;
    dxBarButton108: TdxBarButton;
    dxBarButton109: TdxBarButton;
    dxBarButton110: TdxBarButton;
    dxBarButton111: TdxBarButton;
    dxBarButton112: TdxBarButton;
    dxBarButton113: TdxBarButton;
    dxBarButton114: TdxBarButton;
    dxBarButton115: TdxBarButton;
    dxBarButton116: TdxBarButton;
    dxBarButton117: TdxBarButton;
    dxBarButton118: TdxBarButton;
    dxBarButton119: TdxBarButton;
    dxBarButton120: TdxBarButton;
    dxBarButton121: TdxBarButton;
    dxBarButton122: TdxBarButton;
    dxBarButton123: TdxBarButton;
    dxBarSubItem14: TdxBarSubItem;
    dxBarButton124: TdxBarButton;
    dxBarButton125: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarButton126: TdxBarButton;
    dxBarButton127: TdxBarButton;
    dxBarButton128: TdxBarButton;
    dxBarButton129: TdxBarButton;
    dxBarButton130: TdxBarButton;
    dxBarButton131: TdxBarButton;
    dxBarButton132: TdxBarButton;
    dxBarButton133: TdxBarButton;
    dxBarButton134: TdxBarButton;
    dxBarButton135: TdxBarButton;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    GridTableViewStyleSheetDevExpress: TcxGridTableViewStyleSheet;
    GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet;
    UserQKode: TStringField;
    UserQUsername: TStringField;
    UserQPassword: TStringField;
    UserQJenis: TStringField;
    UserQKodePegawai: TStringField;
    UserQMasterBKanibal: TBooleanField;
    UserQInsertMasterBKanibal: TBooleanField;
    UserQUpdateMasterBKanibal: TBooleanField;
    UserQDeleteMasterBKanibal: TBooleanField;
    UserQMasterKBarang: TBooleanField;
    UserQInsertMasterKBarang: TBooleanField;
    UserQUpdateMasterKBarang: TBooleanField;
    UserQDeleteMasterKBarang: TBooleanField;
    UserQMasterAC: TBooleanField;
    UserQInsertMasterAC: TBooleanField;
    UserQUpdateMasterAC: TBooleanField;
    UserQDeleteMasterAC: TBooleanField;
    UserQMasterArmada: TBooleanField;
    UserQInsertMasterArmada: TBooleanField;
    UserQUpdateMasterArmada: TBooleanField;
    UserQDeleteMasterArmada: TBooleanField;
    UserQMasterJenisKendaraan: TBooleanField;
    UserQInsertJenisKendaraan: TBooleanField;
    UserQUpdateJenisKendaraan: TBooleanField;
    UserQDeleteJenisKendaraan: TBooleanField;
    UserQMasterEkor: TBooleanField;
    UserQInsertMasterEkor: TBooleanField;
    UserQUpdateMasterEkor: TBooleanField;
    UserQDeleteMasterEkor: TBooleanField;
    UserQMasterPelanggan: TBooleanField;
    UserQInsertMasterPelanggan: TBooleanField;
    UserQUpdateMasterPelanggan: TBooleanField;
    UserQDeleteMasterPelanggan: TBooleanField;
    UserQMasterRute: TBooleanField;
    UserQInsertMasterRute: TBooleanField;
    UserQUpdateMasterRute: TBooleanField;
    UserQDeleteMasterRute: TBooleanField;
    UserQMasterSopir: TBooleanField;
    UserQInsertMasterSopir: TBooleanField;
    UserQUpdateMasterSopir: TBooleanField;
    UserQDeleteMasterSopir: TBooleanField;
    UserQMasterJarak: TBooleanField;
    UserQInsertMasterJarak: TBooleanField;
    UserQUpdateMasterJarak: TBooleanField;
    UserQDeleteMasterJarak: TBooleanField;
    UserQMasterBan: TBooleanField;
    UserQInsertMasterBan: TBooleanField;
    UserQUpdateMasterBan: TBooleanField;
    UserQDeleteMasterBan: TBooleanField;
    UserQMasterStandardPerawatan: TBooleanField;
    UserQInsertMasterStandardPerawatan: TBooleanField;
    UserQUpdateMasterStandardPerawatan: TBooleanField;
    UserQDeleteMasterStandardPerawatan: TBooleanField;
    UserQMasterBarang: TBooleanField;
    UserQInsertMasterBarang: TBooleanField;
    UserQUpdateMasterBarang: TBooleanField;
    UserQDeleteMasterBarang: TBooleanField;
    UserQMasterJenisPerbaikan: TBooleanField;
    UserQInsertMasterJenisPerbaikan: TBooleanField;
    UserQUpdateMasterJenisPerbaikan: TBooleanField;
    UserQDeleteMasterJenisPerbaikan: TBooleanField;
    UserQMasterSupplier: TBooleanField;
    UserQInsertMasterSupplier: TBooleanField;
    UserQUpdateMasterSupplier: TBooleanField;
    UserQDeleteMasterSupplier: TBooleanField;
    UserQMasterUser: TBooleanField;
    UserQInsertMasterUser: TBooleanField;
    UserQUpdateMasterUser: TBooleanField;
    UserQDeleteMasterUser: TBooleanField;
    UserQMasterMekanik: TBooleanField;
    UserQInsertMasterMekanik: TBooleanField;
    UserQUpdateMasterMekanik: TBooleanField;
    UserQDeleteMasterMekanik: TBooleanField;
    UserQMasterPegawai: TBooleanField;
    UserQInsertMasterPegawai: TBooleanField;
    UserQUpdateMasterPegawai: TBooleanField;
    UserQDeleteMasterPegawai: TBooleanField;
    UserQKontrak: TBooleanField;
    UserQInsertKontrak: TBooleanField;
    UserQUpdateKontrak: TBooleanField;
    UserQDeleteKontrak: TBooleanField;
    UserQSO: TBooleanField;
    UserQInsertSO: TBooleanField;
    UserQUpdateSO: TBooleanField;
    UserQDeleteSO: TBooleanField;
    UserQSetHargaSO: TBooleanField;
    UserQSetStatusSO: TBooleanField;
    UserQViewHargaSO: TBooleanField;
    UserQKeluhanPelanggan: TBooleanField;
    UserQInsertKeluhanPelanggan: TBooleanField;
    UserQUpdateKeluhanPelanggan: TBooleanField;
    UserQDeleteKeluhanPelanggan: TBooleanField;
    UserQDaftarPelanggan: TBooleanField;
    UserQJadwalArmada: TBooleanField;
    UserQPilihArmada: TBooleanField;
    UserQUpdatePilihArmada: TBooleanField;
    UserQSuratJalan: TBooleanField;
    UserQInsertSuratJalan: TBooleanField;
    UserQUpdateSuratJalan: TBooleanField;
    UserQDeleteSuratJalan: TBooleanField;
    UserQRealisasiSuratJalan: TBooleanField;
    UserQInsertRealisasiSuratJalan: TBooleanField;
    UserQUpdateRealisasiSuratJalan: TBooleanField;
    UserQDeleteRealisasiSuratJalan: TBooleanField;
    UserQStoring: TBooleanField;
    UserQInsertStoring: TBooleanField;
    UserQUpdateStoring: TBooleanField;
    UserQDeleteStoring: TBooleanField;
    UserQDaftarBeli: TBooleanField;
    UserQUpdateDaftarBeli: TBooleanField;
    UserQPilihSupplierDaftarBeli: TBooleanField;
    UserQPO: TBooleanField;
    UserQInsertPO: TBooleanField;
    UserQUpdatePO: TBooleanField;
    UserQDeletePO: TBooleanField;
    UserQApprovalPO: TBooleanField;
    UserQCashNCarry: TBooleanField;
    UserQInsertCashNCarry: TBooleanField;
    UserQUpdateCashNCarry: TBooleanField;
    UserQDeleteCashNCarry: TBooleanField;
    UserQKomplainNRetur: TBooleanField;
    UserQInsertKomplainNRetur: TBooleanField;
    UserQUpdateKomplainRetur: TBooleanField;
    UserQDeleteKomplainRetur: TBooleanField;
    UserQDaftarSupplier: TBooleanField;
    UserQBonBarang: TBooleanField;
    UserQInsertBonBarang: TBooleanField;
    UserQUpdateBonBarang: TBooleanField;
    UserQDeleteBonBarang: TBooleanField;
    UserQSetBeliBonBarang: TBooleanField;
    UserQSetMintaBonBarang: TBooleanField;
    UserQTambahStokBonBarang: TBooleanField;
    UserQPerbaikanBonBarang: TBooleanField;
    UserQPerawatanBonBarang: TBooleanField;
    UserQApprovalBonBarang: TBooleanField;
    UserQProcessBonBarang: TBooleanField;
    UserQApprovalMoneyBonBarang: TCurrencyField;
    UserQSetUrgentBonBarang: TBooleanField;
    UserQBonKeluarBarang: TBooleanField;
    UserQInsertBonKeluarBarang: TBooleanField;
    UserQUpdateBonKeluarBarang: TBooleanField;
    UserQDeleteBonKeluarBarang: TBooleanField;
    UserQLPB: TBooleanField;
    UserQInsertLPB: TBooleanField;
    UserQUpdateLPB: TBooleanField;
    UserQDeleteLPB: TBooleanField;
    UserQPemutihan: TBooleanField;
    UserQInsertPemutihan: TBooleanField;
    UserQUpdatePemutihan: TBooleanField;
    UserQDeletePemutihan: TBooleanField;
    UserQApprovalPemutihan: TBooleanField;
    UserQDaftarMinStok: TBooleanField;
    UserQDaftarStokBarang: TBooleanField;
    UserQLaporanRiwayatStokBarang: TBooleanField;
    UserQLaporanRiwayatHargaBarang: TBooleanField;
    UserQNotifikasiPerawatan: TBooleanField;
    UserQInsertNotifikasiPerawatan: TBooleanField;
    UserQPermintaanPerbaikan: TBooleanField;
    UserQInsertPermintaanPerbaikan: TBooleanField;
    UserQUpdatePermintaanPerbaikan: TBooleanField;
    UserQDeletePermintaanPerbaikan: TBooleanField;
    UserQPerbaikan: TBooleanField;
    UserQInsertPerbaikan: TBooleanField;
    UserQUpdatePerbaikan: TBooleanField;
    UserQDeletePerbaikan: TBooleanField;
    UserQPerawatan: TBooleanField;
    UserQInsertPerawatan: TBooleanField;
    UserQUpdatePerawatan: TBooleanField;
    UserQDeletePerawatan: TBooleanField;
    UserQLepasPasangBan: TBooleanField;
    UserQInsertLepasPasangBan: TBooleanField;
    UserQUpdateLepasPasangBan: TBooleanField;
    UserQDeleteLepasPasangBan: TBooleanField;
    UserQPerbaikanBan: TBooleanField;
    UserQInsertPerbaikanBan: TBooleanField;
    UserQUpdatePerbaikanBan: TBooleanField;
    UserQDeletePerbaikanBan: TBooleanField;
    UserQTukarKomponen: TBooleanField;
    UserQInsertTukarKomponen: TBooleanField;
    UserQUpdateTukarKomponen: TBooleanField;
    UserQDeleteTukarKomponen: TBooleanField;
    UserQKasBon: TBooleanField;
    UserQInsertKasBon: TBooleanField;
    UserQUpdateKasBon: TBooleanField;
    UserQDeleteKasBon: TBooleanField;
    UserQPenagihan: TBooleanField;
    UserQInsertPenagihan: TBooleanField;
    UserQUpdatePenagihan: TBooleanField;
    UserQDeletePenagihan: TBooleanField;
    UserQBayarPO: TBooleanField;
    UserQInsertBayarPO: TBooleanField;
    UserQUpdateBayarPO: TBooleanField;
    UserQDeleteBayarPO: TBooleanField;
    UserQLaporanSalesOrder: TBooleanField;
    UserQLaporanRealisasiOrder: TBooleanField;
    UserQLaporanOrderTidakSesuai: TBooleanField;
    UserQLaporanKeluhanPelanggan: TBooleanField;
    UserQLaporanPendapatan: TBooleanField;
    UserQLaporanPemenuhanKontrak: TBooleanField;
    UserQLaporanEfisiensiMuatan: TBooleanField;
    UserQLaporanPendapatanDanPoinSopir: TBooleanField;
    UserQLaporanPenggunaanBan: TBooleanField;
    UserQLaporanPenggunaanBarang: TBooleanField;
    UserQLaporanKegiatanMekanik: TBooleanField;
    UserQLaporanKegiatanTeknik: TBooleanField;
    UserQLaporanRiwayatKendaraan: TBooleanField;
    UserQLaporanPemenuhanBarang: TBooleanField;
    UserQLaporanPenerimaanNPembelian: TBooleanField;
    UserQLaporanPenambahanOli: TBooleanField;
    UserQLaporanRetur: TBooleanField;
    UserQLaporanPenjualanUser: TBooleanField;
    UserQLaporanProfitPerSO: TBooleanField;
    UserQSettingParameterNotifikasi: TBooleanField;
    UserQSettingWaktuNotifikasi: TBooleanField;
    UserQTampilkanNotifikasi: TBooleanField;
    UserQSettingNotifikasi: TBooleanField;
    UserQFollowUpCustomer: TBooleanField;
    UserQInactiveCustomer: TBooleanField;
    UserQPembuatanSuratJalan: TBooleanField;
    UserQNewBonBarang: TBooleanField;
    UserQNewDaftarBeli: TBooleanField;
    UserQNewPermintaanPerbaikan: TBooleanField;
    UserQNotifPilihArmada: TBooleanField;
    UserQNewLPB: TBooleanField;
    UserQMinimumStok: TBooleanField;
    UserQSTNKExpired: TBooleanField;
    UserQSTNKPajakExpired: TBooleanField;
    UserQKirExpired: TBooleanField;
    UserQSimExpired: TBooleanField;
    UserQKasBonAnjem: TBooleanField;
    UserQInsertKasBonAnjem: TBooleanField;
    UserQUpdateKasBonAnjem: TBooleanField;
    UserQDeleteKasBonAnjem: TBooleanField;
    UserQPembuatanKwitansi: TBooleanField;
    UserQInsertPembuatanKwitansi: TBooleanField;
    UserQUpdatePembuatanKwitansi: TBooleanField;
    UserQDeletePembuatanKwitansi: TBooleanField;
    UserQViewJadwal: TBooleanField;
    UserQRealisasiAnjem: TBooleanField;
    UserQInsertRealisasiAnjem: TBooleanField;
    UserQUpdateRealisasiAnjem: TBooleanField;
    UserQDeleteRealisasiAnjem: TBooleanField;
    UserQRealisasiTrayek: TBooleanField;
    UserQInsertRealisasiTrayek: TBooleanField;
    UserQUpdateRealisasiTrayek: TBooleanField;
    UserQDeleteRealisasiTrayek: TBooleanField;
    UserQUpdateKm: TBooleanField;
    UserQUpdateUpdateKm: TBooleanField;
    UserQPOUmum: TBooleanField;
    UserQInsertPOUmum: TBooleanField;
    UserQUpdatePOUmum: TBooleanField;
    UserQDeletePOUmum: TBooleanField;
    UserQDaftarMinimumStokKategori: TBooleanField;
    UserQKeluarMasukBarangBekas: TBooleanField;
    UserQInsertKeluarMasukBarangBekas: TBooleanField;
    UserQUpdateKeluarMasukBarangBekas: TBooleanField;
    UserQDeleteKeluarMasukBarangBekas: TBooleanField;
    UserQRebuild: TBooleanField;
    UserQInsertRebuild: TBooleanField;
    UserQUpdateRebuild: TBooleanField;
    UserQDeleteRebuild: TBooleanField;
    UserQSPK: TBooleanField;
    UserQInsertSPK: TBooleanField;
    UserQUpdateSPK: TBooleanField;
    UserQDeleteSPK: TBooleanField;
    UserQAmbilBarangKanibal: TBooleanField;
    UserQInsertAmbilBarangKanibal: TBooleanField;
    UserQUpdateAmbilBarangKanibal: TBooleanField;
    UserQDeleteAmbilBarangKanibal: TBooleanField;
    UserQMasterBarangBekas: TBooleanField;
    UserQInsertMasterBarangBekas: TBooleanField;
    UserQUpdateMasterBarangBekas: TBooleanField;
    UserQDeleteMasterBarangBekas: TBooleanField;
    UserQMasterLayoutBan: TBooleanField;
    UserQInsertMasterLayoutBan: TBooleanField;
    UserQUpdateMasterLayoutBan: TBooleanField;
    UserQDeleteMasterLayoutBan: TBooleanField;
    UserQMasterLokasiBan: TBooleanField;
    UserQInsertMasterLokasiBan: TBooleanField;
    UserQUpdateMasterLokasiBan: TBooleanField;
    UserQDeleteMasterLokasiBan: TBooleanField;
    UserQInsertMasterJenisKendaraan: TBooleanField;
    UserQApprovalTukarKomponen: TBooleanField;
    UserQRiwayatHargaBarang: TBooleanField;
    UserQLaporanBBM: TBooleanField;
    UserQLaporanSJ: TBooleanField;
    UserQSkin: TStringField;
    UpdateUserSkinQ: TSDQuery;
    NotifQKode: TStringField;
    NotifQWaktuPengecekan: TIntegerField;
    NotifQInactiveCustomer: TIntegerField;
    NotifQPilihKendaraan: TIntegerField;
    NotifQSuratJalan: TIntegerField;
    NotifQKontrak: TIntegerField;
    NotifQSTNKExpired: TIntegerField;
    NotifQSTNKPExpired: TIntegerField;
    NotifQKIRExpired: TIntegerField;
    UserQCetakKwitansi: TBooleanField;
    CashAndCarryRetur1: TMenuItem;
    dxBarButton136: TdxBarButton;
    UserQPPRekanan: TBooleanField;
    UserQBKBRekanan: TBooleanField;
    dxBarButton137: TdxBarButton;
    dxBarButton138: TdxBarButton;
    UserQLaporanSO: TBooleanField;
    UserQLaporanPerjalananArmada: TBooleanField;
    UserQLaporanKegTeknik: TBooleanField;
    UserQLaporanDaftarBarang: TBooleanField;
    UserQInsertCNCRetur: TBooleanField;
    UserQUpdateCNCRetur: TBooleanField;
    UserQDeleteCNCRetur: TBooleanField;
    UserQInsertMasterRekanan: TBooleanField;
    UserQUpdateMasterRekanan: TBooleanField;
    UserQDeleteMasterRekanan: TBooleanField;
    UserQMasterRekanan: TBooleanField;
    UserQCNCRetur: TBooleanField;
    dxBarButton139: TdxBarButton;
    dxBarButton140: TdxBarButton;
    dxBarButton141: TdxBarButton;
    SettingQ: TSDQuery;
    SettingQAutoLogOut: TIntegerField;
    dxBarButton142: TdxBarButton;
    Timer2: TTimer;
    Label1: TLabel;
    dxBarButton143: TdxBarButton;
    dxBarButton144: TdxBarButton;
    dxBarButton145: TdxBarButton;
    dxBarButton146: TdxBarButton;
    dxBarButton147: TdxBarButton;
    dxBarButton148: TdxBarButton;
    dxBarButton149: TdxBarButton;
    dxBarButton150: TdxBarButton;
    dxBarButton151: TdxBarButton;
    dxBarButton152: TdxBarButton;
    dxBarButton153: TdxBarButton;
    UserQPenjualanSparepart: TBooleanField;
    UserQInsertPenjualanSpr: TBooleanField;
    UserQUpdatePenjualanSpr: TBooleanField;
    UserQDeletePenjualanSpr: TBooleanField;
    UserQAssemb: TBooleanField;
    UserQInsertAssemb: TBooleanField;
    UserQUpdateAssemb: TBooleanField;
    UserQDeleteAssemb: TBooleanField;
    UserQApproveAssemb: TBooleanField;
    UserQLunasKwitansi: TBooleanField;
    UserQApproveKwitansi: TBooleanField;
    dxBarButton154: TdxBarButton;
    dxBarButton155: TdxBarButton;
    dxBarButton156: TdxBarButton;
    UserQVerifikasiRSJ: TBooleanField;
    UserQVerifikasiRTrayek: TBooleanField;
    UserQVerifikasiRKaryawan: TBooleanField;
    dxBarButton157: TdxBarButton;
    dxBarButton158: TdxBarButton;
    dxBarButton159: TdxBarButton;
    dxBarButton160: TdxBarButton;
    dxBarButton161: TdxBarButton;
    dxBarButton162: TdxBarButton;
    dxBarButton163: TdxBarButton;
    dxBarButton164: TdxBarButton;
    UserQLaporanRealisasiSuratJalan: TBooleanField;
    UserQLaporanKasBon: TBooleanField;
    UserQLaporanKeluarBarang: TBooleanField;
    UserQLaporanKwitansiSO: TBooleanField;
    UserQLaporanKlaimSopir: TBooleanField;
    UserQLaporanRekanan: TBooleanField;
    UserQMasterPersenPremi: TBooleanField;
    UserQInsertMasterPersenPremi: TBooleanField;
    UserQUpdateMasterPersenPremi: TBooleanField;
    UserQDeleteMasterPersenPremi: TBooleanField;
    UserQMasterGrupPelanggan: TBooleanField;
    UserQInsertMasterGrupPelanggan: TBooleanField;
    UserQUpdateMasterGrupPelanggan: TBooleanField;
    UserQDeleteMasterGrupPelanggan: TBooleanField;
    dxBarButton165: TdxBarButton;
    tesQ: TSDQuery;
    tesQkode: TStringField;
    Timer3: TTimer;
    procedure Armada1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SO1Click(Sender: TObject);
    procedure SuratJalan1Click(Sender: TObject);
    procedure JadwalArmada1Click(Sender: TObject);
    procedure SuratJalan2Click(Sender: TObject);
    procedure SalesOrder1Click(Sender: TObject);
    procedure Pelanggan1Click(Sender: TObject);
    procedure Rute1Click(Sender: TObject);
    procedure Sopir1Click(Sender: TObject);
    procedure User1Click(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure PilihOrder1Click(Sender: TObject);
    procedure Kontrak1Click(Sender: TObject);
    procedure JenisKendaraan1Click(Sender: TObject);
    procedure Notifikasi1Click(Sender: TObject);
    procedure cxImage1Click(Sender: TObject);
    procedure KasBon3Click(Sender: TObject);
    procedure Storing2Click(Sender: TObject);
    procedure RealisasiSuratJalan1Click(Sender: TObject);
    procedure DaftarPelanggan1Click(Sender: TObject);
    procedure KeluhanPelanggan2Click(Sender: TObject);
    procedure Laporan2Click(Sender: TObject);
    procedure LaporanOrderTidakSesuai1Click(Sender: TObject);
    procedure LaporanPendapatandanPoinSopir1Click(Sender: TObject);
    procedure LaporanKeluhanPelanggan1Click(Sender: TObject);
    procedure LaporanPenggunaanBan1Click(Sender: TObject);
    procedure LaporanPendapatan1Click(Sender: TObject);
    procedure LaporanPemenuhanKontrak1Click(Sender: TObject);
    procedure LaporanEfisiensiMuatan1Click(Sender: TObject);
    procedure ampilkan1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Setting1Click(Sender: TObject);
    procedure Jarak1Click(Sender: TObject);
    procedure Ban2Click(Sender: TObject);
    procedure Barang1Click(Sender: TObject);
    procedure Perawatan1Click(Sender: TObject);
    procedure Mekanik1Click(Sender: TObject);
    procedure Sopir2Click(Sender: TObject);
    procedure Penagihan1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LaporanSalesOrder1Click(Sender: TObject);
    procedure NotifikasiPerawatan1Click(Sender: TObject);
    procedure Perbaikan1Click(Sender: TObject);
    procedure Pemutihan1Click(Sender: TObject);
    procedure DaftarBeli1Click(Sender: TObject);
    procedure PO1Click(Sender: TObject);
    procedure CashAndCarry1Click(Sender: TObject);
    procedure KomplaindanRetur1Click(Sender: TObject);
    procedure DaftarSupplier1Click(Sender: TObject);
    procedure BonBarang1Click(Sender: TObject);
    procedure DaftarBeli2Click(Sender: TObject);
    procedure LaporanPenerimaanBarang1Click(Sender: TObject);
    procedure PermintaanPerbaikan1Click(Sender: TObject);
    procedure LaporanPerbaikan1Click(Sender: TObject);
    procedure LaporanPerawatan1Click(Sender: TObject);
    procedure LepasPasangBan1Click(Sender: TObject);
    procedure PerbaikanBan1Click(Sender: TObject);
    procedure DaftarMinimumStok1Click(Sender: TObject);
    procedure DaftarStokBarang1Click(Sender: TObject);
    procedure BayarPO1Click(Sender: TObject);
    procedure LaporanPenggunaanBarang1Click(Sender: TObject);
    procedure LaporanKegiatanMekanik1Click(Sender: TObject);
    procedure LaporanKegiatanTeknik1Click(Sender: TObject);
    procedure LaporanRiwayatKendaraan1Click(Sender: TObject);
    procedure LaporanPemenuhanBarang1Click(Sender: TObject);
    procedure LaporanPenerimaandanPembelianBarang1Click(Sender: TObject);
    procedure LaporanPenambahanOli1Click(Sender: TObject);
    procedure LaporanRetur1Click(Sender: TObject);
    procedure LaporanPenjualanPerUser1Click(Sender: TObject);
    procedure LaporanProfitPerSO1Click(Sender: TObject);
    procedure Pegawai1Click(Sender: TObject);
    procedure ukarKomponen1Click(Sender: TObject);
    procedure LaporanRiwayatHargaBarang1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RealisasiAnJem1Click(Sender: TObject);
    procedure RealisasiTrayek1Click(Sender: TObject);
    procedure AC1Click(Sender: TObject);
    procedure Rebuild1Click(Sender: TObject);
    procedure BarangKanibal1Click(Sender: TObject);
    procedure KategoriBarang1Click(Sender: TObject);
    procedure AmbilBarangKanibal1Click(Sender: TObject);
    procedure DaftarMinimumStokKategori1Click(Sender: TObject);
    procedure SuratPerintahKerja1Click(Sender: TObject);
    procedure KasBonAnjem1Click(Sender: TObject);
    procedure PenagihanAnjem1Click(Sender: TObject);
    procedure ViewJadwal1Click(Sender: TObject);
    procedure BarangBekas1Click(Sender: TObject);
    procedure KeluarMasukBarangBekas1Click(Sender: TObject);
    procedure PembuatanKwitansi1Click(Sender: TObject);
    procedure UpdateKm1Click(Sender: TObject);
    procedure LayoutBanClick(Sender: TObject);
    procedure LokasiBanClick(Sender: TObject);
    procedure POUmum1Click(Sender: TObject);
    procedure LaporanPemakaianBBM1Click(Sender: TObject);
    procedure LaporanSuratJalan1Click(Sender: TObject);
    procedure RiwayatHargaBarang1Click(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton18Click(Sender: TObject);
    procedure dxBarButton19Click(Sender: TObject);
    procedure dxBarButton20Click(Sender: TObject);
    procedure dxBarButton21Click(Sender: TObject);
    procedure dxBarButton22Click(Sender: TObject);
    procedure dxBarButton23Click(Sender: TObject);
    procedure dxBarButton24Click(Sender: TObject);
    procedure dxBarButton25Click(Sender: TObject);
    procedure dxBarButton26Click(Sender: TObject);
    procedure dxBarButton27Click(Sender: TObject);
    procedure dxBarButton28Click(Sender: TObject);
    procedure dxBarButton29Click(Sender: TObject);
    procedure dxBarButton30Click(Sender: TObject);
    procedure dxBarButton31Click(Sender: TObject);
    procedure dxBarButton32Click(Sender: TObject);
    procedure dxBarButton33Click(Sender: TObject);
    procedure dxBarButton34Click(Sender: TObject);
    procedure dxBarButton35Click(Sender: TObject);
    procedure dxBarButton36Click(Sender: TObject);
    procedure dxBarButton37Click(Sender: TObject);
    procedure dxBarButton38Click(Sender: TObject);
    procedure dxBarButton39Click(Sender: TObject);
    procedure dxBarButton40Click(Sender: TObject);
    procedure dxBarButton41Click(Sender: TObject);
    procedure dxBarButton42Click(Sender: TObject);
    procedure dxBarButton43Click(Sender: TObject);
    procedure dxBarButton44Click(Sender: TObject);
    procedure dxBarButton45Click(Sender: TObject);
    procedure dxBarButton46Click(Sender: TObject);
    procedure dxBarButton47Click(Sender: TObject);
    procedure dxBarButton48Click(Sender: TObject);
    procedure dxBarButton49Click(Sender: TObject);
    procedure dxBarButton50Click(Sender: TObject);
    procedure dxBarButton51Click(Sender: TObject);
    procedure dxBarButton52Click(Sender: TObject);
    procedure dxBarButton53Click(Sender: TObject);
    procedure dxBarButton54Click(Sender: TObject);
    procedure dxBarButton55Click(Sender: TObject);
    procedure dxBarButton56Click(Sender: TObject);
    procedure dxBarButton57Click(Sender: TObject);
    procedure dxBarButton58Click(Sender: TObject);
    procedure dxBarButton59Click(Sender: TObject);
    procedure dxBarButton60Click(Sender: TObject);
    procedure dxBarButton61Click(Sender: TObject);
    procedure dxBarButton62Click(Sender: TObject);
    procedure dxBarButton63Click(Sender: TObject);
    procedure dxBarButton64Click(Sender: TObject);
    procedure dxBarButton65Click(Sender: TObject);
    procedure dxBarButton66Click(Sender: TObject);
    procedure dxBarButton67Click(Sender: TObject);
    procedure dxBarButton68Click(Sender: TObject);
    procedure dxBarButton69Click(Sender: TObject);
    procedure dxBarButton70Click(Sender: TObject);
    procedure dxBarButton71Click(Sender: TObject);
    procedure dxBarButton72Click(Sender: TObject);
    procedure dxBarButton73Click(Sender: TObject);
    procedure dxBarButton74Click(Sender: TObject);
    procedure dxBarButton75Click(Sender: TObject);
    procedure dxBarButton76Click(Sender: TObject);
    procedure dxBarButton77Click(Sender: TObject);
    procedure dxBarButton78Click(Sender: TObject);
    procedure dxBarButton79Click(Sender: TObject);
    procedure dxBarButton80Click(Sender: TObject);
    procedure dxBarButton82Click(Sender: TObject);
    procedure dxBarButton83Click(Sender: TObject);
    procedure dxBarButton84Click(Sender: TObject);
    procedure dxBarButton85Click(Sender: TObject);
    procedure dxBarButton86Click(Sender: TObject);
    procedure dxBarButton87Click(Sender: TObject);
    procedure dxBarButton96Click(Sender: TObject);
    procedure dxBarButton97Click(Sender: TObject);
    procedure dxBarButton124Click(Sender: TObject);
    procedure dxBarButton128Click(Sender: TObject);
    procedure dxBarButton103Click(Sender: TObject);
    procedure dxBarButton104Click(Sender: TObject);
    procedure dxBarButton105Click(Sender: TObject);
    procedure dxBarButton106Click(Sender: TObject);
    procedure dxBarButton107Click(Sender: TObject);
    procedure dxBarButton108Click(Sender: TObject);
    procedure dxBarButton109Click(Sender: TObject);
    procedure dxBarButton110Click(Sender: TObject);
    procedure dxBarButton111Click(Sender: TObject);
    procedure dxBarButton112Click(Sender: TObject);
    procedure dxBarButton113Click(Sender: TObject);
    procedure dxBarButton114Click(Sender: TObject);
    procedure dxBarButton115Click(Sender: TObject);
    procedure dxBarButton116Click(Sender: TObject);
    procedure dxBarButton117Click(Sender: TObject);
    procedure dxBarButton118Click(Sender: TObject);
    procedure dxBarButton119Click(Sender: TObject);
    procedure dxBarButton120Click(Sender: TObject);
    procedure dxBarButton121Click(Sender: TObject);
    procedure dxBarButton122Click(Sender: TObject);
    procedure dxBarButton123Click(Sender: TObject);
    procedure dxBarButton125Click(Sender: TObject);
    procedure dxBarButton126Click(Sender: TObject);
    procedure dxBarButton127Click(Sender: TObject);
    procedure dxBarButton129Click(Sender: TObject);
    procedure dxBarButton130Click(Sender: TObject);
    procedure dxBarButton131Click(Sender: TObject);
    procedure dxBarButton132Click(Sender: TObject);
    procedure dxBarButton133Click(Sender: TObject);
    procedure dxBarButton134Click(Sender: TObject);
    procedure dxBarButton135Click(Sender: TObject);
    procedure updateSkin;
    procedure dxBarButton136Click(Sender: TObject);
    procedure dxBarButton137Click(Sender: TObject);
    procedure dxBarButton138Click(Sender: TObject);
    procedure dxBarButton139Click(Sender: TObject);
    procedure dxBarButton140Click(Sender: TObject);
    procedure dxBarButton141Click(Sender: TObject);
    procedure dxBarButton142Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure dxBarButton143Click(Sender: TObject);
    procedure dxBarButton146Click(Sender: TObject);
    procedure dxBarButton147Click(Sender: TObject);
    procedure dxBarButton148Click(Sender: TObject);
    procedure dxBarButton149Click(Sender: TObject);
    procedure dxBarButton150Click(Sender: TObject);
    procedure dxBarButton152Click(Sender: TObject);
    procedure dxBarButton153Click(Sender: TObject);
    procedure dxBarButton154Click(Sender: TObject);
    procedure dxBarButton155Click(Sender: TObject);
    procedure dxBarButton156Click(Sender: TObject);
    procedure dxBarButton157Click(Sender: TObject);
    procedure dxBarButton158Click(Sender: TObject);
    procedure dxBarButton159Click(Sender: TObject);
    procedure dxBarButton160Click(Sender: TObject);
    procedure dxBarButton161Click(Sender: TObject);
    procedure dxBarButton162Click(Sender: TObject);
    procedure dxBarButton163Click(Sender: TObject);
    procedure dxBarButton164Click(Sender: TObject);
    procedure dxBarButton165Click(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     param_kodebon:String;
     param_kode:String;
     param_nama:String;
     param_sql_detail:String;
     param_sql:String;
     stok:integer;
     //User:string;
       datalama,databaru : array [1..500] of string;

  end;

var
  MenuUtamaFm: TMenuUtamaFm;
  User,username,password:string;
  ticker,tickerlimit,inactivelimit : integer;
  logouttime: integer;

implementation

uses
MasterArmada,
     Login, NotaSO, NotaSJ, JadwalArmada, MasterSuratJalan,
  MasterSalesOrder, MasterPelanggan, MasterRute, MasterSopir, MasterUser, MasterEkor,
  PickOrder, Kontrak, masterjeniskendaraan, Notifikasi, KasBon, Storing,RealisasiSuratJalan,DaftarPeserta,
  KeluhanPelanggan, FrmLaporanPendapatanSopir, fmpenggunaanban,
  FmPendapatan, FmLaporanEfisiensi, FrmLaporanRealisasi,
  frmlaporantidaksesuai, frmLaporanKeluhan, FmLaporanKontrak,
  SettingNotifikasi, MasterJarak, MasterBan, MasterBarang, MasterStandard, MasterSupplier,
  MasterMekanik, Penagihan, LaporanSO, NotifPerawatan,
  MasterJenisPerbaikan, Pemutihan, DaftarBeli, PO, CNC, KomplainDanRetur,
  DaftarSupliers, BonBarang, BonKeluarBarang, LPB, PermintaanPerbaikan,
  LaporanPerawatan, LaporanPermintaanPerbaikan, PasangBan, PerbaikanBan,
  DaftarSupplier, DaftarMinStok, riwayat, Bayar, PenggunaanBarang,
  KegiatanMekanik, KegiatanTeknik, Riwayatkendaraan, PemenuhanBarang,
  PenerimaanBarang, PenambahanOli, Retur, LaporanPenjualanUser,
  LaporanPendapatanSOBiaya, MasterPegawai, TukarKomponen, RiwayatBarang,
  RealisasiAnjem, RealisasiTrayek, MasterAC, Rebuild, MasterBarangKanibal,
  MasterKategoriBarang, TukarKomponenKanibal, DaftarMinStokKategori,
  SuratPerintahKerja, KasBonAnjem, PenagihanAnjem, BonBarangBaru,
  BonKeluarBarangBaru, LPBBaru, DaftarBeliBaru, ViewJadwal,
  KeluarMasukBarangBekas, MasterBarangBekas, PembuatanKwitansi,
  UpdateKilometer, MasterLayoutBan, MasterLokasiBan, PORebuild,
  PembayaranPO, LaporanBBMBaru, LaporanPO, ChartDataDrillingDemoMain,
  BaseForm, LaporanPerjalananArmada, LaporanKegiatanTeknik, CNCRetur,
  MasterRekanan, LaporanDaftarBarang, Audit, MasterTabelArmada,
  TabelArmada,Setting, ViewTabelArmada, LaporanPembayaranSO, LaporanKasBon,
  MasterAssembly, VTabelArmada, LaporanKeluarBarang, PenjualanSparePart,
  MasterHargaBBM, LaporanKwitansi, LaporanRealisasiSuratJalan,
  KlaimPengemudi, BBMNonSO, PrintSOHarian, KeluarBarangCabang,
  PeminjamanBarang, MasterGrupPelanggan, LaporanKlaimSopir,
  MasterPersenPremi, LaporanRekanan, LaporanPemenuhanKontrak;

{$R *.dfm}

function IdleTime: DWord;
var
  LastInput: TLastInputInfo;
begin
  LastInput.cbSize := SizeOf(TLastInputInfo);
  GetLastInputInfo(LastInput);
  Result := (GetTickCount - LastInput.dwTime) DIV 1000;
end;

procedure TMenuUtamaFm.updateSkin;
begin
  UpdateUserSkinQ.Close;
  UpdateUserSkinQ.ParamByName('text').AsString:=dxSkinController1.SkinName;
  UpdateUserSkinQ.ParamByName('text2').AsString:=UserQKode.AsString;
  UpdateUserSkinQ.ExecSQL;
end;

procedure TMenuUtamaFm.Armada1Click(Sender: TObject);
begin
  MasterArmadaFm:= TMasterArmadaFm.create(self,'');
end;

procedure TMenuUtamaFm.FormCreate(Sender: TObject);
var   F: Textfile;
  str: string;
  temp :integer;
  masuk,keluar :boolean;
begin
  DataBase1.Close;
  ticker:=0;
  AssignFile(f, 'dt.dat'); {Assigns the Filename}
  Reset(f); {Opens the file for reading}
  Readln(f, str);
  tickerlimit:=strtoint(str);
  Readln(f, str);
  inactivelimit:=strtoint(str);
  CloseFile(f);
  //timer1.Enabled:=true;
  AssignFile(f, 'database.txt'); {Assigns the Filename}
  Reset(f); {Opens the file for reading}
  Readln(f, str);
  DataBase1.RemoteDatabase:=str;
  Readln(f, str);
  Database1.Params.Values['USER NAME']:=str;
  Readln(f, str);
  Database1.Params.Values['PASSWORD']:=str;
  Closefile(f);
  keluar:=false;
  masuk:=false;
  //FmBaseForm:=TFmBaseForm.create(self);
  while masuk=false do
  begin
    LoginFm:=TLoginFm.Create(Self);
    temp := LoginFm.ShowModal;
    if temp=mrOk then
    begin
      DataBase1.Open;
      UserQ.ParamByName('username').AsString:=loginFm.UserName;
      UserQ.ParamByName('password').AsString:=loginFm.password;
      UserQ.Open;
      username:=loginFm.UserName;
      password:=loginFm.password;
      User:=UserQKode.AsString;
      UserQ.Close;
      if user='' then
      begin
        ShowMessage('login gagal');
        //Application.Terminate;
      end
      else begin
        masuk:=true;
        SettingQ.Open;
        logouttime:=0;
       // Timer2.Enabled:=true;
      end;
    end
    else if temp=mrCancel then
    begin
      //ShowMessage('cancel');
      masuk:=true;
      Application.Terminate;
    end;
  end;
  if keluar then application.Terminate;
  //cxImage1.Picture.LoadFromFile('att.gif');
  userq.Open;
  //NotifQ.Open;
end;

procedure TMenuUtamaFm.SO1Click(Sender: TObject);
begin
  NotaSOFm:= TNotaSOFm.create(self,'');
  NotaSOFm.ShowModal();
end;

procedure TMenuUtamaFm.SuratJalan1Click(Sender: TObject);
begin
  NotaSJFm:= TNotaSJFm.create(self);
  NotaSJFm.ShowModal();
end;

procedure TMenuUtamaFm.JadwalArmada1Click(Sender: TObject);
begin
  JadwalArmadaFm:= TJadwalArmadaFm.create(self);
end;

procedure TMenuUtamaFm.SuratJalan2Click(Sender: TObject);
begin
    MasterSuratJalanFm:= TMasterSuratJalanFm.create(self);
end;

procedure TMenuUtamaFm.SalesOrder1Click(Sender: TObject);
begin
    MasterSalesOrderFm:= TMasterSalesOrderFm.create(self);
end;

procedure TMenuUtamaFm.Pelanggan1Click(Sender: TObject);
begin
     MasterPelangganFm:= TMasterPelangganFm.create(self);
end;

procedure TMenuUtamaFm.Rute1Click(Sender: TObject);
begin
  MasterRuteFm:= TMasterRuteFm.create(self,'');
end;

procedure TMenuUtamaFm.Sopir1Click(Sender: TObject);
begin
  MasterSopirFm:= TMasterSopirFm.create(self);
end;

procedure TMenuUtamaFm.User1Click(Sender: TObject);
begin
  MasterUserFm:= TMasterUserFm.create(self);
end;

procedure TMenuUtamaFm.E1Click(Sender: TObject);
begin
  MasterEkorFm:= TMasterEkorFm.create(self);
end;

procedure TMenuUtamaFm.PilihOrder1Click(Sender: TObject);
begin
  PickOrderFm:= TPickOrderFm.create(self);
end;

procedure TMenuUtamaFm.Kontrak1Click(Sender: TObject);
begin
  KontrakFm:= TKontrakFm.create(self,'');
end;

procedure TMenuUtamaFm.JenisKendaraan1Click(Sender: TObject);
begin
  MasterJenisKendaraanFm := TMasterJenisKendaraanFm.create(self);
end;

procedure TMenuUtamaFm.Notifikasi1Click(Sender: TObject);
begin
 // NotifikasiFm := TNotifikasiFm.create(self);
end;

procedure TMenuUtamaFm.cxImage1Click(Sender: TObject);
begin
  //cxImage1.Visible:=false;
  //NotifikasiFm := TNotifikasiFm.create(self);
  NotifikasiFm := TNotifikasiFm.create(self);
  notifikasifm.ShowModal;
end;

procedure TMenuUtamaFm.KasBon3Click(Sender: TObject);
begin
  KasBonFm := TKasBonFm.create(self);
  kasbonfm.ShowModal;
end;

procedure TMenuUtamaFm.Storing2Click(Sender: TObject);
begin
  StoringFm := TStoringFm.create(self);
end;

procedure TMenuUtamaFm.RealisasiSuratJalan1Click(Sender: TObject);
begin
  RealisasiSuratJalanFm := TRealisasiSuratJalanFm.Create(self);
  RealisasiSuratJalanFm.ShowModal();
end;

procedure TMenuUtamaFm.DaftarPelanggan1Click(Sender: TObject);
begin
  DaftarPesertaFm := TDaftarPesertaFm.create(self,'');
end;

procedure TMenuUtamaFm.KeluhanPelanggan2Click(Sender: TObject);
begin
  KeluhanPelangganFM := TKeluhanPelangganFm.create(self);
end;

procedure TMenuUtamaFm.Laporan2Click(Sender: TObject);
begin
  frmrealisasi := tfrmrealisasi.create(self);
  FrmRealisasi.Showmodal();
end;

procedure TMenuUtamaFm.LaporanOrderTidakSesuai1Click(Sender: TObject);
begin
  fmlaporantidaksesuai := tfmlaporantidaksesuai.create(self);
  fmlaporantidaksesuai.Showmodal();
end;

procedure TMenuUtamaFm.LaporanPendapatandanPoinSopir1Click(
  Sender: TObject);
begin
  frmpendapatansopir := tfrmpendapatansopir.create(self);
  frmpendapatansopir.ShowModal();
end;

procedure TMenuUtamaFm.LaporanKeluhanPelanggan1Click(Sender: TObject);
begin
  frmkeluhan := tfrmkeluhan.create(self);
  frmkeluhan.ShowModal();
end;

procedure TMenuUtamaFm.LaporanPenggunaanBan1Click(Sender: TObject);
begin
  frmpenggunaanban := tfrmpenggunaanban.create(self);
  frmpenggunaanban.showmodal();
end;

procedure TMenuUtamaFm.LaporanPendapatan1Click(Sender: TObject);
begin
  frmpendapatan := tfrmpendapatan.create(self);
  frmpendapatan.ShowModal();
end;

procedure TMenuUtamaFm.LaporanPemenuhanKontrak1Click(Sender: TObject);
begin
  frmkontrak := tfrmkontrak.create(self);
  frmkontrak.ShowModal();
end;

procedure TMenuUtamaFm.LaporanEfisiensiMuatan1Click(Sender: TObject);
begin
  frmefisiensi := tfrmefisiensi.create(self);
  frmefisiensi.ShowModal();
end;

procedure TMenuUtamaFm.ampilkan1Click(Sender: TObject);
begin
  NotifikasiFm := TNotifikasiFm.create(self);
  notifikasifm.ShowModal;
end;

procedure TMenuUtamaFm.Timer1Timer(Sender: TObject);
begin
{  ticker:=ticker+1;
  if ticker>=NotifQWaktuPengecekan.AsInteger then
  begin
    notifikasiFm := TnotifikasiFM.Create(self);
    notifikasifm.Visible:=false;
    if notifikasi.jumnotif=0 then
    begin
      notifikasifm.Release;
      cximage1.Visible:=false;
      cxLabel1.Visible:=false;
    end
    else
    begin
      cximage1.Visible:=true;
      cxlabel1.Caption:=inttostr(notifikasi.jumnotif);
      cxLabel1.Visible:=true;
    end;
    ticker:=0;
  end;             }
end;

procedure TMenuUtamaFm.Setting1Click(Sender: TObject);
begin
  SettingNotifikasiFm :=TSettingNotifikasiFm.create(Self);
  Settingnotifikasifm.ShowModal;
end;

procedure TMenuUtamaFm.Jarak1Click(Sender: TObject);
begin
  MasterJarakFm := TMasterJarakFm.create(Self);
end;

procedure TMenuUtamaFm.Ban2Click(Sender: TObject);
begin
  MasterBanFm := TMasterBanFm.create(Self);
end;

procedure TMenuUtamaFm.Barang1Click(Sender: TObject);
begin
  MasterBarangFm := TMasterBarangFm.create(Self);
end;

procedure TMenuUtamaFm.Perawatan1Click(Sender: TObject);
begin
  MasterStandardFm := TMasterStandardFm.create(Self);
end;

procedure TMenuUtamaFm.Mekanik1Click(Sender: TObject);
begin
  MasterMekanikFm := TMasterMekanikFm.create(Self);
end;

procedure TMenuUtamaFm.Sopir2Click(Sender: TObject);
begin
  MasterSupplierFm := TMasterSupplierFm.create(Self);
end;

procedure TMenuUtamaFm.Penagihan1Click(Sender: TObject);
begin
  PenagihanFm := TPenagihanFm.create(self);
end;

procedure TMenuUtamaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TMenuUtamaFm.LaporanSalesOrder1Click(Sender: TObject);
begin
  LaporanSOFm := TLaporanSOFm.create(self);
  LaporanSOFm.showmodal();
end;

procedure TMenuUtamaFm.NotifikasiPerawatan1Click(Sender: TObject);
begin
  NotifPerawatanFm := TNotifPerawatanFm.create(self);
end;

procedure TMenuUtamaFm.Perbaikan1Click(Sender: TObject);
begin
  MasterJenisPerbaikanFm :=TMasterJenisPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.Pemutihan1Click(Sender: TObject);
begin
PemutihanFm:=TPemutihanFm.create(self);
end;

procedure TMenuUtamaFm.DaftarBeli1Click(Sender: TObject);
begin
  DaftarBeliBaruFm := TDaftarBeliBaruFm.create(self);
  DaftarBeliBaruFm.Show;
end;

procedure TMenuUtamaFm.PO1Click(Sender: TObject);
begin
  POFm := TPOFm.create(self);
  POFm.Show;
end;

procedure TMenuUtamaFm.CashAndCarry1Click(Sender: TObject);
begin
  CnCFm := TCnCFm.create(self);
  CNCFm.Show;
end;

procedure TMenuUtamaFm.KomplaindanRetur1Click(Sender: TObject);
begin
  KomplainDanReturFm := TKomplainDanReturFm.create(self);
  KomplainDanReturFm.Show;
end;

procedure TMenuUtamaFm.DaftarSupplier1Click(Sender: TObject);
begin
  DaftarSupplierFm := TDaftarSupplierFm.create(self);
  DaftarSupplierFm.show;
end;

procedure TMenuUtamaFm.BonBarang1Click(Sender: TObject);
begin
  BonBarangBaruFm := TBonBarangBaruFm.create(self);
  BonBarangBaruFm.Show;
end;

procedure TMenuUtamaFm.DaftarBeli2Click(Sender: TObject);
begin
  BonKeluarBarangBaruFm := TBonKeluarBarangBaruFm.create(self);
  BonKeluarBarangBaruFm.Show;
end;

procedure TMenuUtamaFm.LaporanPenerimaanBarang1Click(Sender: TObject);
begin
  Lpbbarufm := Tlpbbarufm.create(self);
  Lpbbarufm.Show;
end;

procedure TMenuUtamaFm.PermintaanPerbaikan1Click(Sender: TObject);
begin
  PermintaanPerbaikanFm := TPermintaanPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPerbaikan1Click(Sender: TObject);
begin
  LaporanPermintaanPerbaikanFm := TLaporanPermintaanPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPerawatan1Click(Sender: TObject);
begin
  LaporanPerawatanFm := TLaporanPerawatanFm.create(self,'');
end;

procedure TMenuUtamaFm.LepasPasangBan1Click(Sender: TObject);
begin
  pasangbanfm := tpasangbanfm.create(self);
end;

procedure TMenuUtamaFm.PerbaikanBan1Click(Sender: TObject);
begin
  perbaikanbanfm := tperbaikanbanfm.create(self);
end;

procedure TMenuUtamaFm.DaftarMinimumStok1Click(Sender: TObject);
begin
  daftarminstokfm := tdaftarminstokfm.create(self);
end;

procedure TMenuUtamaFm.DaftarStokBarang1Click(Sender: TObject);
begin
  riwayatfm := triwayatfm.create(self);
  riwayatfm.show;
end;

procedure TMenuUtamaFm.BayarPO1Click(Sender: TObject);
begin
  PembayaranPOFm:=TPembayaranPOFm.create(self);
   PembayaranPOFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanPenggunaanBarang1Click(Sender: TObject);
begin
  penggunaanbarangFm := tpenggunaanbarangFm.create(self);
  penggunaanbarangFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanKegiatanMekanik1Click(Sender: TObject);
begin
kegiatanMekanikFm := tkegiatanMekanikFm.create(self);
  kegiatanMekanikFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanKegiatanTeknik1Click(Sender: TObject);
begin
kegiatanTeknikFm := tkegiatanTeknikFm.create(self);
  kegiatanTeknikFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanRiwayatKendaraan1Click(Sender: TObject);
begin
 RiwayatKendaraanFm := tRiwayatKendaraanFm.create(self);
  RiwayatKendaraanFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanPemenuhanBarang1Click(Sender: TObject);
begin
 pemenuhanBarangFm := tpemenuhanBarangFm.create(self);
  pemenuhanBarangFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanPenerimaandanPembelianBarang1Click(
  Sender: TObject);
begin
 PenerimaanBarangFm := tPenerimaanBarangFm.create(self);
  PenerimaanBarangFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanPenambahanOli1Click(Sender: TObject);
begin
 PenambahanOliFm := tPenambahanOliFm.create(self);
  PenambahanOliFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanRetur1Click(Sender: TObject);
begin
    ReturFm := tReturFm.create(self);
  ReturFm.Showmodal;
end;

procedure TMenuUtamaFm.LaporanPenjualanPerUser1Click(Sender: TObject);
begin
  LaporanPenjualanUserFm := TLaporanPenjualanUserFm.create(self);
  laporanpenjualanuserfm.ShowModal;
end;

procedure TMenuUtamaFm.LaporanProfitPerSO1Click(Sender: TObject);
begin
  LaporanPendapatanSOBiayaFm := TLaporanPendapatanSOBiayaFm.create(self);
  laporanpendapatansobiayafm.ShowModal;
end;

procedure TMenuUtamaFm.Pegawai1Click(Sender: TObject);
begin
  MasterPegawaiFm :=TMasterPegawaiFm.create(self,'');
end;

procedure TMenuUtamaFm.ukarKomponen1Click(Sender: TObject);
begin
  TukarKomponenFm := TTukarKomponenFm.create(self);
end;

procedure TMenuUtamaFm.LaporanRiwayatHargaBarang1Click(Sender: TObject);
begin
//  riwayatbarangFm:= Triwayatbarangfm.create(self);
end;

procedure TMenuUtamaFm.FormShow(Sender: TObject);
begin
  Timer3.Enabled:=true;
  UserQ.Open;
  dxBarButton1.Enabled:=UserQMasterAC.AsBoolean;
  dxBarButton2.Enabled:=UserQMasterArmada.AsBoolean;
  dxBarButton18.Enabled:=UserQMasterBan.AsBoolean;
  dxBarButton19.Enabled:=UserQMasterLayoutBan.AsBoolean;
  dxBarButton20.Enabled:=UserQMasterLokasiBan.AsBoolean;
  dxBarButton21.Enabled:=UserQMasterBarang.AsBoolean;
  dxBarButton22.Enabled:=UserQMasterBarangBekas.AsBoolean;
  dxBarButton23.Enabled:=UserQMasterBKanibal.AsBoolean;
  dxBarButton24.Enabled:=UserQMasterJenisKendaraan.AsBoolean;
  dxBarButton25.Enabled:=UserQMasterJenisPerbaikan.AsBoolean;
  dxBarButton26.Enabled:=UserQMasterKBarang.AsBoolean;
  dxBarButton27.Enabled:=UserQMasterPegawai.AsBoolean;
  dxBarButton28.Enabled:=UserQMasterPelanggan.AsBoolean;
  dxBarButton137.Enabled:=UserQMasterRekanan.AsBoolean;
  dxBarButton29.Enabled:=UserQMasterRute.AsBoolean;
  dxBarButton30.Enabled:=UserQMasterStandardPerawatan.AsBoolean;
  dxBarButton31.Enabled:=UserQMasterSupplier.AsBoolean;
  dxBarButton32.Enabled:=UserQMasterUser.AsBoolean;
  dxBarButton33.Enabled:=UserQKontrak.AsBoolean;
  dxBarButton34.Enabled:=UserQSO.AsBoolean;
  dxBarButton35.Enabled:=UserQKeluhanPelanggan.AsBoolean;
  dxBarButton36.Enabled:=UserQDaftarPelanggan.AsBoolean;
  dxBarButton37.Enabled:=UserQJadwalArmada.AsBoolean;
  dxBarButton38.Enabled:=UserQViewJadwal.AsBoolean;
  dxBarButton39.Enabled:=UserQPilihArmada.AsBoolean;
  dxBarButton40.Enabled:=UserQSuratJalan.AsBoolean;
  dxBarButton41.Enabled:=UserQRealisasiSuratJalan.AsBoolean;
  dxBarButton42.Enabled:=UserQRealisasiAnjem.AsBoolean;
  dxBarButton43.Enabled:=UserQRealisasiTrayek.AsBoolean;
  dxBarButton44.Enabled:=UserQStoring.AsBoolean;
  dxBarButton45.Enabled:=UserQUpdateKm.AsBoolean;
  dxBarButton46.Enabled:=UserQDaftarBeli.AsBoolean;
  dxBarButton47.Enabled:=UserQPO.AsBoolean;
  dxBarButton48.Enabled:=UserQCashNCarry.AsBoolean;
  dxBarButton136.Enabled:=UserQCNCRetur.AsBoolean;
  dxBarButton49.Enabled:=UserQKomplainNRetur.AsBoolean;
  dxBarButton50.Enabled:=UserQDaftarSupplier.AsBoolean;
  dxBarButton51.Enabled:=UserQPOUmum.AsBoolean;
  dxBarButton52.Enabled:=UserQRiwayatHargaBarang.AsBoolean;
  dxBarButton53.Enabled:=UserQBonBarang.AsBoolean;
  dxBarButton54.Enabled:=UserQBonKeluarBarang.AsBoolean;
  dxBarButton55.Enabled:=UserQLPB.AsBoolean;
  dxBarButton56.Enabled:=UserQPemutihan.AsBoolean;
  dxBarButton57.Enabled:=UserQDaftarMinStok.AsBoolean;
  dxBarButton58.Enabled:=UserQDaftarMinimumStokKategori.AsBoolean;
  dxBarButton59.Enabled:=UserQDaftarStokBarang.AsBoolean;
  dxBarButton60.Enabled:=UserQKeluarMasukBarangBekas.AsBoolean;
  dxBarButton61.Enabled:=UserQNotifikasiPerawatan.AsBoolean;
  dxBarButton62.Enabled:=UserQPermintaanPerbaikan.AsBoolean;
  dxBarButton63.Enabled:=UserQPerbaikan.AsBoolean;
  dxBarButton64.Enabled:=UserQPerawatan.AsBoolean;
  dxBarButton65.Enabled:=UserQRebuild.AsBoolean;
  dxBarButton66.Enabled:=UserQSPK.AsBoolean;
  dxBarButton67.Enabled:=UserQTukarKomponen.AsBoolean;
  dxBarButton68.Enabled:=UserQAmbilBarangKanibal.AsBoolean;
  dxBarButton69.Enabled:=UserQLepasPasangBan.AsBoolean;
  dxBarButton70.Enabled:=UserQPerbaikanBan.AsBoolean;
  dxBarButton71.Enabled:=UserQKasBon.AsBoolean;
  dxBarButton72.Enabled:=UserQKasBonAnjem.AsBoolean;
  dxBarButton73.Enabled:=UserQBayarPO.AsBoolean;
  dxBarButton74.Enabled:=UserQPembuatanKwitansi.AsBoolean;
  dxBarButton75.Enabled:=UserQLaporanKeluhanPelanggan.AsBoolean;
  dxBarButton76.Enabled:=UserQLaporanPemenuhanKontrak.AsBoolean;
  dxBarButton77.Enabled:=UserQLaporanBBM.AsBoolean;
  dxBarButton78.Enabled:=UserQLaporanSJ.AsBoolean;
  dxBarButton133.Enabled:=UserQLaporanSO.AsBoolean;
  dxBarButton134.Enabled:=UserQLaporanPerjalananArmada.AsBoolean;
  dxBarButton135.Enabled:=UserQLaporanKegTeknik.AsBoolean;
  dxBarButton138.Enabled:=UserQLaporanDaftarBarang.AsBoolean;
  dxBarButton79.Enabled:=UserQTampilkanNotifikasi.AsBoolean;
  dxBarButton80.Enabled:=UserQSettingNotifikasi.AsBoolean;
  dxBarButton150.Enabled:=UserQPenjualanSparepart.AsBoolean;
  dxBarButton148.Enabled:=UserQAssemb.AsBoolean;
  dxBarButton154.Enabled:=UserQLaporanRealisasiSuratJalan.AsBoolean;
  dxBarButton147.Enabled:=UserQLaporanKasBon.AsBoolean;
  dxBarButton149.Enabled:=UserQLaporanKeluarBarang.AsBoolean;
  dxBarButton153.Enabled:=UserQLaporanKwitansiSO.AsBoolean;
  dxBarButton161.Enabled:=UserQLaporanKlaimSopir.AsBoolean;
  dxBarButton163.Enabled:=UserQLaporanRekanan.AsBoolean;
  dxBarButton164.Enabled:=UserQLaporanPemenuhanKontrak.AsBoolean;
  dxBarButton162.Enabled:=UserQMasterPersenPremi.AsBoolean;
  dxBarButton160.Enabled:=UserQMasterGrupPelanggan.AsBoolean;
  {AC1.Enabled:=UserQMasterAC.AsBoolean;
  BarangKanibal1.Enabled:=UserQMasterBKanibal.AsBoolean;
  KategoriBarang1.Enabled:=UserQMasterKBarang.AsBoolean;
  armada1.Enabled:=UserQMasterArmada.AsBoolean;
  JenisKendaraan1.Enabled:=UserQMasterJenisKendaraan.AsBoolean;
  //e1.Enabled:=UserQMasterEkor.AsBoolean;
  pelanggan1.Enabled:=UserQMasterPelanggan.AsBoolean;
  //jarak1.Enabled:=UserQMasterJarak.AsBoolean;
  Rute1.enabled:=userqmasterrute.AsBoolean;
  Barang1.Enabled:=UserQMasterBarang.AsBoolean;
  ban2.Enabled:=userqmasterban.AsBoolean;
  perawatan1.Enabled:=UserQMasterStandardPerawatan.AsBoolean;
  perbaikan1.Enabled:=UserQMasterJenisPerbaikan.AsBoolean;
  sopir2.Enabled:=UserQMasterSupplier.AsBoolean;
  Pegawai1.Enabled:=UserQMasterPegawai.AsBoolean;
  user1.Enabled:=UserQMasterUser.AsBoolean;
  kontrak1.Enabled:=UserQKontrak.AsBoolean;
  so1.Enabled:=UserQSO.AsBoolean;
  KeluhanPelanggan2.Enabled:=UserQKeluhanPelanggan.AsBoolean;
  daftarpelanggan1.Enabled:=UserQDaftarPelanggan.AsBoolean;
  jadwalarmada1.Enabled:=UserQJadwalArmada.AsBoolean;
  PilihOrder1.Enabled:=UserQPilihArmada.AsBoolean;
  suratjalan1.Enabled:=UserQSuratJalan.AsBoolean;
  RealisasiSuratJalan1.Enabled:=UserQRealisasiSuratJalan.AsBoolean;
  Storing2.Enabled:=userqstoring.AsBoolean;
  daftarbeli1.Enabled:=UserQDaftarBeli.AsBoolean;
  PO1.Enabled:=UserQPO.AsBoolean;
  CashAndCarry1.Enabled:=UserQCashNCarry.AsBoolean;
  KomplaindanRetur1.Enabled:=UserQKomplainNRetur.AsBoolean;
  DaftarSupplier1.Enabled:=UserQDaftarSupplier.AsBoolean;
  bonbarang1.Enabled:=UserQBonBarang.AsBoolean;
  DaftarBeli2.Enabled:=UserQBonKeluarBarang.AsBoolean;
  LaporanPenerimaanBarang1.Enabled:=UserQLPB.AsBoolean;
  pemutihan1.Enabled:=UserQPemutihan.AsBoolean;
  DaftarMinimumStok1.Enabled:=UserQDaftarMinStok.AsBoolean;
  DaftarStokBarang1.Enabled:=UserQDaftarStokBarang.AsBoolean;
  NotifikasiPerawatan1.Enabled:=UserQNotifikasiPerawatan.AsBoolean;
  LaporanPerbaikan1.Enabled:=UserQPerbaikan.AsBoolean;
  LaporanPerawatan1.Enabled:=UserQPerawatan.AsBoolean;
  PermintaanPerbaikan1.Enabled:=UserQPermintaanPerbaikan.AsBoolean;
  LepasPasangBan1.Enabled:=UserQLepasPasangBan.AsBoolean;
  PerbaikanBan1.Enabled:=UserQPerbaikanBan.AsBoolean;
  ukarKomponen1.Enabled:=UserQTukarKomponen.AsBoolean;
  KasBon3.Enabled:=UserQKasBon.AsBoolean;
  Penagihan1.Enabled:=UserQPenagihan.AsBoolean;
  BayarPO1.Enabled:=UserQBayarPO.AsBoolean;
  ampilkan1.Enabled:=UserQTampilkanNotifikasi.AsBoolean;
  setting1.Enabled:=UserQSettingNotifikasi.AsBoolean;
  LaporanSalesOrder1.Enabled:=UserQLaporanSalesOrder.AsBoolean;
  laporan2.Enabled:=UserQLaporanRealisasiOrder.AsBoolean;
  LaporanOrderTidakSesuai1.Enabled:=UserQLaporanOrderTidakSesuai.AsBoolean;
  LaporanKeluhanPelanggan1.Enabled:=UserQLaporanKeluhanPelanggan.AsBoolean;
  LaporanPendapatan1.Enabled:=UserQLaporanPendapatan.AsBoolean;
  LaporanPemenuhanKontrak1.Enabled:=UserQLaporanPemenuhanKontrak.AsBoolean;
  LaporanEfisiensiMuatan1.Enabled:=UserQLaporanEfisiensiMuatan.AsBoolean;
  LaporanPendapatandanPoinSopir1.Enabled:=UserQLaporanPendapatanDanPoinSopir.AsBoolean;
  LaporanPenggunaanBan1.Enabled:=UserQLaporanPenggunaanBan.AsBoolean;
  LaporanPenggunaanBarang1.Enabled:=UserQLaporanPenggunaanBarang.AsBoolean;
  LaporanKegiatanMekanik1.Enabled:=UserQLaporanKegiatanMekanik.AsBoolean;
  LaporanKegiatanTeknik1.Enabled:=UserQLaporanKegiatanTeknik.AsBoolean;
  LaporanRiwayatKendaraan1.Enabled:=UserQLaporanRiwayatKendaraan.AsBoolean;
  laporanpemenuhanbarang1.Enabled:=UserQLaporanPemenuhanBarang.AsBoolean;
  LaporanPenerimaandanPembelianBarang1.Enabled:=UserQLaporanPenerimaanNPembelian.AsBoolean;
  LaporanPenambahanOli1.Enabled:=UserQLaporanPenambahanOli.AsBoolean;
  LaporanProfitPerSO1.Enabled:=UserQLaporanProfitPerSO.AsBoolean;
  laporanretur1.Enabled:=UserQLaporanRetur.AsBoolean;
  LaporanPenjualanPerUser1.Enabled:=UserQLaporanPenjualanUser.AsBoolean;
  PembuatanKwitansi1.Enabled:=UserQPembuatanKwitansi.AsBoolean;
  KasBonAnjem1.Enabled:=UserQKasBonAnjem.AsBoolean;
  ViewJadwal1.Enabled:=UserQViewJadwal.AsBoolean;
  RealisasiAnJem1.Enabled:=UserQRealisasiAnjem.AsBoolean;
  RealisasiTrayek1.Enabled:=UserQRealisasiTrayek.AsBoolean;
  UpdateKm1.Enabled:=UserQUpdateKm.AsBoolean;
  POUmum1.Enabled:=UserQPOUmum.AsBoolean;
  DaftarMinimumStokKategori1.Enabled:=UserQDaftarMinimumStokKategori.AsBoolean;
  KeluarMasukBarangBekas1.Enabled:=UserQKeluarMasukBarangBekas.AsBoolean;
  Rebuild1.Enabled:=UserQRebuild.AsBoolean;
  SuratPerintahKerja1.Enabled:=UserQSPK.AsBoolean;
  AmbilBarangKanibal1.Enabled:=UserQAmbilBarangKanibal.AsBoolean;
  BarangBekas1.Enabled:=UserQMasterBarangBekas.AsBoolean;
  LayoutBan.Enabled:=UserQMasterLayoutBan.AsBoolean;
  LokasiBan.Enabled:=UserQMasterLokasiBan.AsBoolean;
  RiwayatHargaBarang1.Enabled:=UserQRiwayatHargaBarang.AsBoolean;
  LaporanPemakaianBBM1.Enabled:=UserQLaporanBBM.AsBoolean;
  LaporanSuratJalan1.Enabled:=UserQLaporanSJ.AsBoolean;}
  dxSkinController1.SkinName:=UserQSkin.AsString;
  //NotifQ.Open;
  SettingQ.Open;
end;

procedure TMenuUtamaFm.RealisasiAnJem1Click(Sender: TObject);
begin
  RealisasiAnjemFm :=TRealisasiAnjemFm.create(self);
  RealisasiAnjemFm.ShowModal();
end;

procedure TMenuUtamaFm.RealisasiTrayek1Click(Sender: TObject);
begin
    RealisasiTrayekFm :=TRealisasiTrayekFm.create(self,'');
    RealisasiTrayekFm.Show;
end;

procedure TMenuUtamaFm.AC1Click(Sender: TObject);
begin
  MasterACFm:= TMasterACFm.create(self);
end;

procedure TMenuUtamaFm.Rebuild1Click(Sender: TObject);
begin
  RebuildFm := TRebuildFm.create(self);
end;

procedure TMenuUtamaFm.BarangKanibal1Click(Sender: TObject);
begin
  MasterBarangKanibalFm:=TMasterBarangKanibalFm.create(self);
end;

procedure TMenuUtamaFm.KategoriBarang1Click(Sender: TObject);
begin
  MasterKategoriBarangFm:=TMasterKategoriBarangFm.create(self);
end;

procedure TMenuUtamaFm.AmbilBarangKanibal1Click(Sender: TObject);
begin
  TukarKomponenKanibalFm:=TTukarKomponenKanibalFm.create(self);
end;

procedure TMenuUtamaFm.DaftarMinimumStokKategori1Click(Sender: TObject);
begin
  DaftarMinStokKategoriFm:=TDaftarMinStokKategoriFm.create(self);
end;

procedure TMenuUtamaFm.SuratPerintahKerja1Click(Sender: TObject);
begin
  SuratPerintahKerjaFm:=TSuratPerintahKerjaFm.create(self);
  SuratPerintahKerjaFm.ShowModal();
end;

procedure TMenuUtamaFm.KasBonAnjem1Click(Sender: TObject);
begin
  KasBonAnjemFm:=TKasBonAnjemFm.create(self);
end;

procedure TMenuUtamaFm.PenagihanAnjem1Click(Sender: TObject);
begin
  PenagihanAnjemFm:=TPenagihanAnjemFm.create(self);
end;

procedure TMenuUtamaFm.ViewJadwal1Click(Sender: TObject);
begin
  ViewJadwalFm:=TViewJadwalFm.create(self);
end;

procedure TMenuUtamaFm.BarangBekas1Click(Sender: TObject);
begin
  MasterBarangBekasFm:=TMasterBarangBekasFm.create(self);
end;

procedure TMenuUtamaFm.KeluarMasukBarangBekas1Click(Sender: TObject);
begin
  KeluarMasukBarangBekasFm := TKeluarMasukBarangBekasFm.create(self);
end;

procedure TMenuUtamaFm.PembuatanKwitansi1Click(Sender: TObject);
begin
  PembuatanKwitansiFm:= TPembuatanKwitansiFm.create(self);
  PembuatanKwitansiFm.ShowModal();
end;

procedure TMenuUtamaFm.UpdateKm1Click(Sender: TObject);
begin
  UpdateKilometerFm:= TUpdateKilometerFm.create(self);
end;

procedure TMenuUtamaFm.LayoutBanClick(Sender: TObject);
begin
  MasterLayoutBanFm:= TMasterLayoutBanFm.create(self);
end;

procedure TMenuUtamaFm.LokasiBanClick(Sender: TObject);
begin
  MasterLokasiBanFm:= TMasterLokasiBanFm.create(self);
end;

procedure TMenuUtamaFm.POUmum1Click(Sender: TObject);
begin
  PORebuildFm :=TPORebuildFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPemakaianBBM1Click(Sender: TObject);
begin
  LaporanBBMBaruFm := TLaporanBBMBaruFm.create(self);
end;

procedure TMenuUtamaFm.LaporanSuratJalan1Click(Sender: TObject);
begin
  LaporanPOFm := TLaporanPOFm.create(self);
end;

procedure TMenuUtamaFm.RiwayatHargaBarang1Click(Sender: TObject);
begin
  //FRMMain := TFRMMain.create(self);
  FRMMain.Show;
end;

procedure TMenuUtamaFm.dxBarButton1Click(Sender: TObject);
begin
  MasterACFm:= TMasterACFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton2Click(Sender: TObject);
begin
  MasterArmadaFm:= TMasterArmadaFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton18Click(Sender: TObject);
begin
  MasterBanFm := TMasterBanFm.create(Self);
end;

procedure TMenuUtamaFm.dxBarButton19Click(Sender: TObject);
begin
  MasterLayoutBanFm:= TMasterLayoutBanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton20Click(Sender: TObject);
begin
  MasterLokasiBanFm:= TMasterLokasiBanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton21Click(Sender: TObject);
begin
  MasterBarangFm := TMasterBarangFm.create(Self);
end;

procedure TMenuUtamaFm.dxBarButton22Click(Sender: TObject);
begin
  MasterBarangBekasFm:=TMasterBarangBekasFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton23Click(Sender: TObject);
begin
  MasterBarangKanibalFm:=TMasterBarangKanibalFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton24Click(Sender: TObject);
begin
  MasterJenisKendaraanFm := TMasterJenisKendaraanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton25Click(Sender: TObject);
begin
  MasterJenisPerbaikanFm :=TMasterJenisPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton26Click(Sender: TObject);
begin
  MasterKategoriBarangFm:=TMasterKategoriBarangFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton27Click(Sender: TObject);
begin
  MasterPegawaiFm :=TMasterPegawaiFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton28Click(Sender: TObject);
begin
     MasterPelangganFm:= TMasterPelangganFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton29Click(Sender: TObject);
begin
  MasterRuteFm:= TMasterRuteFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton30Click(Sender: TObject);
begin
MasterStandardFm := TMasterStandardFm.create(Self);
end;

procedure TMenuUtamaFm.dxBarButton31Click(Sender: TObject);
begin
MasterSupplierFm := TMasterSupplierFm.create(Self);
end;

procedure TMenuUtamaFm.dxBarButton32Click(Sender: TObject);
begin
MasterUserFm:= TMasterUserFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton33Click(Sender: TObject);
begin
KontrakFm:= TKontrakFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton34Click(Sender: TObject);
begin
  NotaSOFm:= TNotaSOFm.create(self,'');
  NotaSOFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton35Click(Sender: TObject);
begin
KeluhanPelangganFM := TKeluhanPelangganFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton36Click(Sender: TObject);
begin
DaftarPesertaFm := TDaftarPesertaFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton37Click(Sender: TObject);
begin
JadwalArmadaFm:= TJadwalArmadaFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton38Click(Sender: TObject);
begin
ViewJadwalFm:=TViewJadwalFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton39Click(Sender: TObject);
begin
PickOrderFm:= TPickOrderFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton40Click(Sender: TObject);
begin
  NotaSJFm:= TNotaSJFm.create(self);
  NotaSJFm.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton41Click(Sender: TObject);
begin
  RealisasiSuratJalanFm := TRealisasiSuratJalanFm.Create(self);
//  RealisasiSuratJalanFm.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton42Click(Sender: TObject);
begin
{  RealisasiAnjemFm :=TRealisasiAnjemFm.create(self);
  RealisasiAnjemFm.ShowModal();  }
    RealisasiAnjemFm := TRealisasiAnjemFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton43Click(Sender: TObject);
begin
    RealisasiTrayekFm :=TRealisasiTrayekFm.create(self,'');
    RealisasiTrayekFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton44Click(Sender: TObject);
begin
  StoringFm := TStoringFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton45Click(Sender: TObject);
begin
  UpdateKilometerFm:= TUpdateKilometerFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton46Click(Sender: TObject);
begin
  DaftarBeliBaruFm := TDaftarBeliBaruFm.create(self);
  DaftarBeliBaruFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton47Click(Sender: TObject);
begin
  POFm := TPOFm.create(self);
  POFm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton48Click(Sender: TObject);
begin
  CnCFm := TCnCFm.create(self);
  CNCFm.Showmodal;
end;

procedure TMenuUtamaFm.dxBarButton49Click(Sender: TObject);
begin
  KomplainDanReturFm := TKomplainDanReturFm.create(self);
  KomplainDanReturFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton50Click(Sender: TObject);
begin
  DaftarSupplierFm := TDaftarSupplierFm.create(self);
  DaftarSupplierFm.show;
end;

procedure TMenuUtamaFm.dxBarButton51Click(Sender: TObject);
begin
  PORebuildFm :=TPORebuildFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton52Click(Sender: TObject);
begin
  Frmmain:=tFrmMain.Create(self);
  FRMMain.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton53Click(Sender: TObject);
begin
  BonBarangBaruFm := TBonBarangBaruFm.create(self);
  BonBarangBaruFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton54Click(Sender: TObject);
begin
  BonKeluarBarangBaruFm := TBonKeluarBarangBaruFm.create(self);
  BonKeluarBarangBaruFm.Show;
end;

procedure TMenuUtamaFm.dxBarButton55Click(Sender: TObject);
begin
  Lpbbarufm := Tlpbbarufm.create(self);
  Lpbbarufm.Show;
end;

procedure TMenuUtamaFm.dxBarButton56Click(Sender: TObject);
begin
PemutihanFm:=TPemutihanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton57Click(Sender: TObject);
begin
daftarminstokfm := tdaftarminstokfm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton58Click(Sender: TObject);
begin
  DaftarMinStokKategoriFm:=TDaftarMinStokKategoriFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton59Click(Sender: TObject);
begin
  riwayatfm := triwayatfm.create(self);
  riwayatfm.show;
end;

procedure TMenuUtamaFm.dxBarButton60Click(Sender: TObject);
begin
KeluarMasukBarangBekasFm := TKeluarMasukBarangBekasFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton61Click(Sender: TObject);
begin
NotifPerawatanFm := TNotifPerawatanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton62Click(Sender: TObject);
begin
PermintaanPerbaikanFm := TPermintaanPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton63Click(Sender: TObject);
begin
LaporanPermintaanPerbaikanFm := TLaporanPermintaanPerbaikanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton64Click(Sender: TObject);
begin
  LaporanPerawatanFm := TLaporanPerawatanFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton65Click(Sender: TObject);
begin
RebuildFm := TRebuildFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton66Click(Sender: TObject);
begin
  SuratPerintahKerjaFm:=TSuratPerintahKerjaFm.create(self);
  SuratPerintahKerjaFm.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton67Click(Sender: TObject);
begin
TukarKomponenFm := TTukarKomponenFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton68Click(Sender: TObject);
begin
 TukarKomponenKanibalFm:=TTukarKomponenKanibalFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton69Click(Sender: TObject);
begin
pasangbanfm := tpasangbanfm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton70Click(Sender: TObject);
begin
perbaikanbanfm := tperbaikanbanfm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton71Click(Sender: TObject);
begin
  KasBonFm := TKasBonFm.create(self);
  kasbonfm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton72Click(Sender: TObject);
begin
  KasBonAnjemFm:=TKasBonAnjemFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton73Click(Sender: TObject);
begin
  PembayaranPOFm:=TPembayaranPOFm.create(self);
   PembayaranPOFm.Showmodal;
end;

procedure TMenuUtamaFm.dxBarButton74Click(Sender: TObject);
begin
  PembuatanKwitansiFm:= TPembuatanKwitansiFm.create(self);
 // PembuatanKwitansiFm.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton75Click(Sender: TObject);
begin
  frmkeluhan := tfrmkeluhan.create(self);
  frmkeluhan.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton76Click(Sender: TObject);
begin
  frmkontrak := tfrmkontrak.create(self);
  frmkontrak.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton77Click(Sender: TObject);
begin
  LaporanBBMBaruFm := TLaporanBBMBaruFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton78Click(Sender: TObject);
begin
  LaporanPOFm := TLaporanPOFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton79Click(Sender: TObject);
begin
  NotifikasiFm := TNotifikasiFm.create(self);
  notifikasifm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton80Click(Sender: TObject);
begin
  SettingNotifikasiFm :=TSettingNotifikasiFm.create(Self);
  Settingnotifikasifm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton82Click(Sender: TObject);
begin
dxSkinController1.NativeStyle:=true;
end;

procedure TMenuUtamaFm.dxBarButton83Click(Sender: TObject);
begin
dxSkinController1.NativeStyle:=false;
dxSkinController1.Kind:=lfFlat;
end;

procedure TMenuUtamaFm.dxBarButton84Click(Sender: TObject);
begin
dxSkinController1.NativeStyle:=false;
dxSkinController1.Kind:=lfOffice11;
end;

procedure TMenuUtamaFm.dxBarButton85Click(Sender: TObject);
begin
dxSkinController1.NativeStyle:=false;
dxSkinController1.Kind:=lfStandard;
end;

procedure TMenuUtamaFm.dxBarButton86Click(Sender: TObject);
begin
dxSkinController1.NativeStyle:=false;
dxSkinController1.Kind:=lfUltraFlat;
end;

procedure TMenuUtamaFm.dxBarButton87Click(Sender: TObject);
begin
dxSkinController1.SkinName:='Black';
end;

procedure TMenuUtamaFm.dxBarButton96Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Black';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton97Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Blue';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton124Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Seven';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton128Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Stardust';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton103Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Caramel';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton104Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Coffee';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton105Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='DarkRoom';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton106Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='DarkSide';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton107Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Foggy';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton108Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='GlassOceans';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton109Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='iMaginary';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton110Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Lilian';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton111Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='LiquidSky';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton112Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='LondonLiquidSky';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton113Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='McSkin';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton114Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='MoneyTwins';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton115Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2007Black';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton116Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2007Blue';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton117Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2007Green';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton118Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2007Pink';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton119Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2007Silver';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton120Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2010Black';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton121Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2010Blue';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton122Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Office2010Silver';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton123Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Pumpkin';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton125Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Sharp';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton126Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Silver';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton127Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Springtime';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton129Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Summer2008';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton130Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='UserSkin';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton131Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Valentine';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton132Click(Sender: TObject);
begin
  dxSkinController1.SkinName:='Xmas2008Blue';
  updateSkin;
end;

procedure TMenuUtamaFm.dxBarButton133Click(Sender: TObject);
begin
  LaporanSoFm:=TLaporanSoFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton134Click(Sender: TObject);
begin
  LaporanPerjalananArmadaFm:=TLaporanPerjalananArmadaFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton135Click(Sender: TObject);
begin
  LaporanKegiatanTeknikFm:=TlaporanKegiatanTeknikFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton136Click(Sender: TObject);
begin
  CNCReturFm:=TCNCReturFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton137Click(Sender: TObject);
begin
  MasterRekananFm:=TMasterRekananFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton138Click(Sender: TObject);
begin
  LaporanDaftarBarangFm:=TLaporanDaftarBarangFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton139Click(Sender: TObject);
begin
  AuditFm:=TAuditFm.create(self);
  AuditFm.ShowModal();
end;

procedure TMenuUtamaFm.dxBarButton140Click(Sender: TObject);
begin
mastertabelarmadafm:=tmastertabelarmadafm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton141Click(Sender: TObject);
begin
  Tabelarmadafm:=ttabelarmadafm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton142Click(Sender: TObject);
begin
  SettingFm:=TSettingfm.create(self);
end;

procedure TMenuUtamaFm.Timer2Timer(Sender: TObject);
begin
 { Label1.Caption := Format('System idle time: %d s', [IdleTime]);
  //logouttime:=logouttime+1;
  //label1.Caption:=inttostr(logouttime);
  if IdleTime/60>=SettingQAutoLogOut.AsInteger then begin
    timer2.Enabled:=false;
    ShowMessage('Silahkan Login kembali');
    Application.Terminate;
  end; }
end;

procedure TMenuUtamaFm.Image1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  logouttime:=0;
end;

procedure TMenuUtamaFm.dxBarButton143Click(Sender: TObject);
begin
  VTabelarmadafm:=TVTabelArmadafm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton146Click(Sender: TObject);
begin
  LaporanPembayaranSOFm:=TLaporanPembayaranSOFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton147Click(Sender: TObject);
begin
  LaporanKasBonFm:=TLaporanKasBonFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton148Click(Sender: TObject);
begin
  MasterAssemblyFm:=TMasterAssemblyFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton149Click(Sender: TObject);
begin
  LaporanKeluarBarangFm:=TLaporanKeluarBarangFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton150Click(Sender: TObject);
begin
  PenjualanSparepartFm:=TPenjualanSparepartFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton152Click(Sender: TObject);
begin
  MasterHargaBBMFm := TMasterHargaBBMFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton153Click(Sender: TObject);
begin
  LaporanKwitansiFm:=TLaporanKwitansiFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton154Click(Sender: TObject);
begin
  LaporanRealisasiSuratJalanFm:=TLaporanRealisasiSuratJalanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton155Click(Sender: TObject);
begin
  KlaimPengemudiFm:=TKlaimPengemudiFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton156Click(Sender: TObject);
begin
  BBMNonSOFm:=TBBMNonSOFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton157Click(Sender: TObject);
begin
  PrintSOHarianFm:=TPrintSOHarianFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton158Click(Sender: TObject);
begin
  KeluarBarangCabangFm:=TKeluarBarangCabangFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton159Click(Sender: TObject);
begin
  PeminjamanBarangFm:=TPeminjamanBarangFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton160Click(Sender: TObject);
begin
MasterGrupPelangganFm:=TMasterGrupPelangganFm.create(self,'master');
end;

procedure TMenuUtamaFm.dxBarButton161Click(Sender: TObject);
begin
  LaporanKlaimSopirFm:=TLaporanKlaimSopirFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton162Click(Sender: TObject);
begin
  MasterPersenPremiFm := TMasterPersenPremiFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton163Click(Sender: TObject);
begin
  LaporanRekananFm:=TLaporanRekananFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton164Click(Sender: TObject);
begin
  LaporanPemenuhanKontrakFm:=TLaporanPemenuhanKontrakFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton165Click(Sender: TObject);
begin
try

      DataBase1.Connected:=false;
      DataBase1.Connected:=true;
      userQ.Close;
      UserQ.ParamByName('username').AsString:=username;
      UserQ.ParamByName('password').AsString:=password;
      UserQ.Open;

except
on E : Exception do begin
      showmessage('Koneksi gagal.');
end;
end;
end;

procedure TMenuUtamaFm.Timer3Timer(Sender: TObject);
begin
try
tesq.Close;
tesq.Open;
except
on E : Exception do begin
        DataBase1.Connected:=false;
        DataBase1.Connected:=true;
        userQ.Close;
        UserQ.ParamByName('username').AsString:=username;
        UserQ.ParamByName('password').AsString:=password;
        UserQ.Open;
        showmessage('Koneksi ulang.');
end;
end;
end;

end.
