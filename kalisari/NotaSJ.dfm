object NotaSJFm: TNotaSJFm
  Left = 275
  Top = 28
  Width = 856
  Height = 704
  Caption = 'Surat Jalan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 840
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 597
    Width = 840
    Height = 50
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 173
      Top = 11
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object buttonCetak: TcxButton
      Left = 255
      Top = 11
      Width = 75
      Height = 25
      Caption = 'CETAK'
      TabOrder = 2
      OnClick = buttonCetakClick
    end
    object deleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 3
      OnClick = deleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 542
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 48
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 361
    Height = 373
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 158
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterSOQ
    Version = 1
    object MasterVGridKodenota: TcxDBEditorRow
      Properties.Caption = 'NomorSO'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKodenotaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kodenota'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPelanggan'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridBerangkat: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.DataBinding.FieldName = 'Berangkat'
      Properties.Options.Editing = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridTiba: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.DataBinding.FieldName = 'Tiba'
      Properties.Options.Editing = False
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridKapasitasSeat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KapasitasSeat'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridAC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AC'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridToilet: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Toilet'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridAirSuspension: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AirSuspension'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Rute'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridDari: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Dari'
      ID = 9
      ParentID = 8
      Index = 0
      Version = 1
    end
    object MasterVGridKe: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Ke'
      ID = 10
      ParentID = 8
      Index = 1
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Armada'
      ID = 11
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridPlatNo: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo'
      Properties.Options.Editing = False
      ID = 12
      ParentID = 11
      Index = 0
      Version = 1
    end
    object MasterVGridNoBody: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody'
      Properties.Options.Editing = False
      ID = 13
      ParentID = 11
      Index = 1
      Version = 1
    end
    object MasterVGridPICJemput: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PICJemput'
      ID = 14
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridJamJemput: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JamJemput'
      ID = 15
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridNoTelpPICJemput: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoTelpPICJemput'
      ID = 16
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridAlamatJemput: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AlamatJemput'
      ID = 17
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Height = 47
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 18
      ParentID = -1
      Index = 14
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 647
    Width = 840
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxLabel1: TcxLabel
    Left = 360
    Top = 64
  end
  object cxLabel2: TcxLabel
    Left = 360
    Top = 88
  end
  object cxLabel3: TcxLabel
    Left = 360
    Top = 112
  end
  object cxLabel4: TcxLabel
    Left = 360
    Top = 136
  end
  object cxLabel5: TcxLabel
    Left = 360
    Top = 160
  end
  object cxLabel6: TcxLabel
    Left = 360
    Top = 184
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 361
    Top = 48
    Width = 479
    Height = 373
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 159
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 10
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.Caption = 'TglJalan'
      Properties.DataBinding.FieldName = 'Tgl'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Sopir: TcxDBEditorRow
      Properties.Caption = 'Pengemudi'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SopirEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Sopir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1NamaSopir1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPengemudi'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Sopir2: TcxDBEditorRow
      Properties.Caption = 'Pengemudi2'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1Sopir2EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Sopir2'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1NamaPengemudi2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPengemudi2'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 3
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1Crew: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Crew'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1Kir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1STNK: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'STNK'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1Pajak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pajak'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1Status: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      Properties.Options.Editing = False
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1TitipKwitansi: TcxDBEditorRow
      Properties.Caption = 'TitipKuitansi'
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.DataBinding.FieldName = 'TitipKwitansi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1TitipTagihan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TitipTagihanEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TitipTagihan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1DBNoKwitansi: TcxDBEditorRow
      Properties.Caption = 'NoKuitansi'
      Properties.DataBinding.FieldName = 'NoKwitansi'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1NominalKwitansi: TcxDBEditorRow
      Properties.Caption = 'NominalKuitansi'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DataBinding.FieldName = 'NominalKwitansi'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
      Height = 84
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckGroupProperties'
      Properties.EditProperties.EditValueFormat = cvfStatesString
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.Items = <
        item
          Caption = 'Tol'
        end
        item
          Caption = 'Parkir'
        end
        item
          Caption = 'Polisi'
        end
        item
          Caption = 'Retribusi'
        end
        item
          Caption = 'Penyebrangan Penumpang / Kapal'
        end>
      Properties.EditProperties.OnChange = cxDBVerticalGrid1DBEditorRow2EditPropertiesChange
      Properties.DataBinding.FieldName = 'Tambahan'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 15
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1TambahanTol: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TambahanTolEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TambahanTol'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid1TambahanParkir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TambahanParkirEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TambahanParkir'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1TambahanPolisi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TambahanPolisiEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TambahanPolisi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 18
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid1TambahanRetribusi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TambahanRetribusiEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TambahanRetribusi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 19
      ParentID = -1
      Index = 17
      Version = 1
    end
    object cxDBVerticalGrid1TambahanKapal: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid1TambahanKapalEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'TambahanKapal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 20
      ParentID = -1
      Index = 18
      Version = 1
    end
    object cxDBVerticalGrid1JumlahUangsaku: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JumlahUangsaku'
      ID = 21
      ParentID = -1
      Index = 19
      Version = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 421
    Width = 840
    Height = 176
    Align = alBottom
    BevelOuter = bvRaised
    BevelWidth = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      DataController.DataSource = SJDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'NoSO'
        Width = 120
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Width = 111
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        Caption = 'JumlahSeat'
        DataBinding.FieldName = 'KapasitasSeat'
        Width = 76
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPT'
        Options.SortByDisplayText = isbtOn
        Width = 259
      end
      object cxGrid1DBTableView1RuteBerangkat: TcxGridDBColumn
        DataBinding.FieldName = 'RuteBerangkat'
        Width = 133
      end
      object cxGrid1DBTableView1RuteTiba: TcxGridDBColumn
        DataBinding.FieldName = 'RuteTiba'
        Width = 136
      end
      object cxGrid1DBTableView1SudahPrint: TcxGridDBColumn
        DataBinding.FieldName = 'SudahPrint'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 560
    Top = 16
    TabOrder = 12
    Width = 121
  end
  object KasBonBtn: TButton
    Left = 688
    Top = 12
    Width = 75
    Height = 25
    Caption = 'Buat Kas Bon'
    TabOrder = 13
    OnClick = KasBonBtnClick
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    AfterPost = MasterQAfterPost
    SQL.Strings = (
      
        'select sj.*, '#39#39' as NamaPengemudi, '#39#39' as NamaPengemudi2, '#39#39' as Ta' +
        'mbahan,cast('#39#39' as bit) as TambahanTol,'
      
        'cast(-1 as bit) as TambahanParkir, cast(-1 as bit) as TambahanPo' +
        'lisi,'
      
        'cast(-1 as bit) as TambahanRetribusi, cast(-1 as bit) as Tambaha' +
        'nKapal,'
      
        '(select sum(ISNULL(Nominal,0)) from BonSopir bs where bs.SuratJa' +
        'lan=sj.KodeNota) as JumlahUangsaku'
      'from mastersj sj')
    UpdateObject = MasterUS
    Left = 265
    Top = 9
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object MasterQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQSopir2: TStringField
      FieldName = 'Sopir2'
      Size = 10
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object MasterQTitipKwitansi: TBooleanField
      FieldName = 'TitipKwitansi'
    end
    object MasterQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQKir: TBooleanField
      FieldName = 'Kir'
      Required = True
    end
    object MasterQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object MasterQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object MasterQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object MasterQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object MasterQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object MasterQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object MasterQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object MasterQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object MasterQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQLaporan: TMemoField
      FieldName = 'Laporan'
      BlobType = ftMemo
    end
    object MasterQTglRealisasi: TDateTimeField
      FieldName = 'TglRealisasi'
    end
    object MasterQAwal: TStringField
      FieldName = 'Awal'
      Size = 10
    end
    object MasterQAkhir: TStringField
      FieldName = 'Akhir'
      Size = 10
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object MasterQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object MasterQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object MasterQPremiSopir2: TCurrencyField
      FieldName = 'PremiSopir2'
    end
    object MasterQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object MasterQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object MasterQTabunganSopir2: TCurrencyField
      FieldName = 'TabunganSopir2'
    end
    object MasterQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object MasterQUangJalan: TCurrencyField
      FieldName = 'UangJalan'
    end
    object MasterQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object MasterQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object MasterQOther: TCurrencyField
      FieldName = 'Other'
    end
    object MasterQNominalKwitansi: TCurrencyField
      FieldName = 'NominalKwitansi'
    end
    object MasterQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object MasterQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object MasterQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object MasterQKeteranganBiayaLainLain: TStringField
      FieldName = 'KeteranganBiayaLainLain'
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQTambahan: TStringField
      FieldName = 'Tambahan'
      Size = 50
    end
    object MasterQUangInap: TCurrencyField
      FieldName = 'UangInap'
    end
    object MasterQUangParkir: TCurrencyField
      FieldName = 'UangParkir'
    end
    object MasterQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object MasterQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object MasterQTambahanTol: TBooleanField
      FieldName = 'TambahanTol'
    end
    object MasterQTambahanParkir: TBooleanField
      FieldName = 'TambahanParkir'
    end
    object MasterQTambahanPolisi: TBooleanField
      FieldName = 'TambahanPolisi'
    end
    object MasterQTambahanRetribusi: TBooleanField
      FieldName = 'TambahanRetribusi'
    end
    object MasterQTambahanKapal: TBooleanField
      FieldName = 'TambahanKapal'
    end
    object MasterQSudahPrint: TBooleanField
      FieldName = 'SudahPrint'
    end
    object MasterQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object MasterQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object MasterQNamaPengemudi: TStringField
      FieldName = 'NamaPengemudi'
      Size = 100
    end
    object MasterQNamaPengemudi2: TStringField
      FieldName = 'NamaPengemudi2'
      Size = 100
    end
    object MasterQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object MasterQDanaKebersihan: TCurrencyField
      FieldName = 'DanaKebersihan'
    end
    object MasterQVerifikasiStatus: TStringField
      FieldName = 'VerifikasiStatus'
      Size = 50
    end
    object MasterQKmSekarang: TFloatField
      FieldName = 'KmSekarang'
    end
    object MasterQSopirLuar: TStringField
      FieldName = 'SopirLuar'
      Size = 50
    end
    object MasterQJumlahUangsaku: TCurrencyField
      FieldName = 'JumlahUangsaku'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 340
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other' +
        ', SPBUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, Keteran' +
        'ganVerifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang, Sop' +
        'irLuar'
      'from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update mastersj'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other,'
      '  SPBUAYaniDetail = :SPBUAYaniDetail,'
      '  TitipTagihan = :TitipTagihan,'
      '  SudahPrint = :SudahPrint,'
      '  Verifikasi = :Verifikasi,'
      '  KeteranganVerifikasi = :KeteranganVerifikasi,'
      '  DanaKebersihan = :DanaKebersihan,'
      '  VerifikasiStatus = :VerifikasiStatus,'
      '  KmSekarang = :KmSekarang,'
      '  SopirLuar = :SopirLuar'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into mastersj'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other, SP' +
        'BUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, KeteranganV' +
        'erifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang, SopirLu' +
        'ar)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other, :SPBUAYaniDetail,' +
        ' :TitipTagihan, :SudahPrint, :Verifikasi, :KeteranganVerifikasi,' +
        ' :DanaKebersihan, :VerifikasiStatus, :KmSekarang, :SopirLuar)')
    DeleteSQL.Strings = (
      'delete from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 380
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from mastersj order by kodenota desc')
    Left = 297
    Top = 7
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AutoCalcFields = False
    CachedUpdates = False
    SQL.Strings = (
      
        'select so.*, p.NamaPT as NamaPelanggan, r.Muat as Dari, r.Bongka' +
        'r as Ke,'
      'a.PlatNo, a.NoBody'
      'from MasterSO so left join Pelanggan p on so.Pelanggan=p.Kode'
      'left join Rute r on so.Rute=r.Kode'
      'left join Armada a on so.Armada=a.Kode'
      'where so.Armada is not null AND so.Kodenota=:text')
    Left = 233
    Top = 247
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQTitlePICJemput: TStringField
      FieldName = 'TitlePICJemput'
      Size = 50
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object SOQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object SOQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
    object SOQKeteranganInternal: TMemoField
      FieldName = 'KeteranganInternal'
      BlobType = ftMemo
    end
    object SOQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Size = 100
    end
    object SOQDari: TStringField
      FieldName = 'Dari'
      Size = 250
    end
    object SOQKe: TStringField
      FieldName = 'Ke'
      Size = 250
    end
    object SOQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object SOQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 465
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 513
    Top = 7
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object viewSJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select cast(sj.tgl as date) as Tanggal,sj.*, a.PlatNo, so.Kapasi' +
        'tasSeat,'
      'p.NamaPT, r.Muat as RuteBerangkat, r.Bongkar as RuteTiba'
      'from mastersj sj left join masterso so on sj.NoSO=so.kodenota'
      'left join pelanggan p on so.pelanggan=p.kode '
      'left join Armada a on so.Armada=a.Kode'
      'left join Rute r on so.Rute=r.Kode'
      'where (sj.tgl >=:text1 and sj.tgl <=:text2) or sj.tgl is null'
      'order by sj.tgl desc')
    Left = 497
    Top = 385
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewSJQTanggal: TStringField
      FieldName = 'Tanggal'
    end
    object viewSJQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object viewSJQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object viewSJQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 10
    end
    object viewSJQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object viewSJQSopir2: TStringField
      FieldName = 'Sopir2'
      Size = 10
    end
    object viewSJQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object viewSJQTitipKwitansi: TBooleanField
      FieldName = 'TitipKwitansi'
    end
    object viewSJQNominalKwitansi: TCurrencyField
      FieldName = 'NominalKwitansi'
    end
    object viewSJQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object viewSJQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object viewSJQKir: TBooleanField
      FieldName = 'Kir'
      Required = True
    end
    object viewSJQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object viewSJQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object viewSJQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object viewSJQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object viewSJQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object viewSJQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object viewSJQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object viewSJQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object viewSJQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object viewSJQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object viewSJQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object viewSJQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object viewSJQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object viewSJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewSJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewSJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewSJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewSJQLaporan: TMemoField
      FieldName = 'Laporan'
      BlobType = ftMemo
    end
    object viewSJQTglRealisasi: TDateTimeField
      FieldName = 'TglRealisasi'
    end
    object viewSJQAwal: TStringField
      FieldName = 'Awal'
      Size = 10
    end
    object viewSJQAkhir: TStringField
      FieldName = 'Akhir'
      Size = 10
    end
    object viewSJQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewSJQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object viewSJQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 200
    end
    object viewSJQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object viewSJQPremiSopir2: TCurrencyField
      FieldName = 'PremiSopir2'
    end
    object viewSJQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object viewSJQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object viewSJQTabunganSopir2: TCurrencyField
      FieldName = 'TabunganSopir2'
    end
    object viewSJQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object viewSJQUangJalan: TCurrencyField
      FieldName = 'UangJalan'
    end
    object viewSJQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object viewSJQKeteranganBiayaLainLain: TStringField
      FieldName = 'KeteranganBiayaLainLain'
      Size = 200
    end
    object viewSJQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object viewSJQUangInap: TCurrencyField
      FieldName = 'UangInap'
    end
    object viewSJQUangParkir: TCurrencyField
      FieldName = 'UangParkir'
    end
    object viewSJQOther: TCurrencyField
      FieldName = 'Other'
    end
    object viewSJQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object viewSJQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object viewSJQSudahPrint: TBooleanField
      FieldName = 'SudahPrint'
    end
    object viewSJQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object viewSJQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object viewSJQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object viewSJQNamaPT: TStringField
      FieldName = 'NamaPT'
      Size = 100
    end
    object viewSJQRuteBerangkat: TStringField
      FieldName = 'RuteBerangkat'
      Size = 250
    end
    object viewSJQRuteTiba: TStringField
      FieldName = 'RuteTiba'
      Size = 250
    end
    object viewSJQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object viewSJQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
  end
  object SJDS: TDataSource
    DataSet = viewSJQ
    Left = 444
    Top = 390
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from rute')
    Left = 633
    Top = 55
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object jarakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 721
    Top = 63
    object jarakQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jarakQDari: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object jarakQKe: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object jarakQJumlahKm: TIntegerField
      FieldName = 'JumlahKm'
      Required = True
    end
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 185
    Top = 7
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 216
    Top = 8
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai where kode=:text'
      '')
    Left = 680
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object MasterSOQ: TDataSource
    DataSet = SOQ
    Left = 192
    Top = 240
  end
  object SOQView: TSDQuery
    DatabaseName = 'data'
    Options = []
    AutoCalcFields = False
    CachedUpdates = False
    SQL.Strings = (
      'select * from MasterSO')
    Left = 680
    Top = 112
    object SOQViewKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQViewTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQViewPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQViewBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQViewTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQViewHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQViewPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQViewTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQViewCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQViewNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQViewPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQViewPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQViewTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQViewCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQViewNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQViewPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQViewExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQViewTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQViewBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQViewKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQViewAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQViewToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQViewAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQViewRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQViewTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQViewArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQViewKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQViewPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQViewJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQViewNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQViewAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQViewStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQViewStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQViewReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQViewKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQViewCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQViewCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQViewOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQViewTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQViewTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object CekTitipanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select k.kode,(select case when k.TitipKuitansi IS NULL then 0 e' +
        'lse k.TitipKuitansi end) as TitipKuitansi,'
      
        '(select case when k.TitipTagihan IS NULL then 0 else k.TitipTagi' +
        'han end) as TitipTagihan  '
      'from DetailKwitansiSO dkso '
      'left join Kwitansi k on dkso.KodeKwitansi=k.Kode'
      'where dkso.KodeSO=:text')
    Left = 416
    Top = 464
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekTitipanQkode: TStringField
      FieldName = 'kode'
      Size = 10
    end
    object CekTitipanQTitipKuitansi: TIntegerField
      FieldName = 'TitipKuitansi'
    end
    object CekTitipanQTitipTagihan: TIntegerField
      FieldName = 'TitipTagihan'
    end
  end
  object DetailKwitansiSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiSO where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL1
    Left = 480
    Top = 464
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailKwitansiSOQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object DetailKwitansiSOQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
    object DetailKwitansiSOQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO, Nominal'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeSO, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 520
    Top = 464
  end
  object CekTKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select count(*) as hitung'
      'from MasterSJ where NoSO=:text and TitipKwitansi=1')
    Left = 568
    Top = 456
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekTKQhitung: TIntegerField
      FieldName = 'hitung'
    end
  end
  object CekTTQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select count(*) as hitung'
      'from MasterSJ where NoSO=:text and TitipTagihan=1')
    Left = 608
    Top = 464
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekTTQhitung: TIntegerField
      FieldName = 'hitung'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 416
    Top = 336
  end
  object KasBonQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from BonSopir')
    UpdateObject = KasBonUs
    Left = 544
    Top = 152
    object KasBonQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KasBonQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Required = True
      Size = 10
    end
    object KasBonQNominal: TCurrencyField
      FieldName = 'Nominal'
      Required = True
    end
    object KasBonQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KasBonQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KasBonQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KasBonQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KasBonQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object KodeKasBonQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from bonsopir order by kode desc')
    Left = 552
    Top = 184
    object KodeKasBonQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object KasBonUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, SuratJalan, Nominal, CreateDate, CreateBy, Operator' +
        ', TglEntry, TglCetak'
      'from BonSopir'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    ModifySQL.Strings = (
      'update BonSopir'
      'set'
      '  Kode = :Kode,'
      '  SuratJalan = :SuratJalan,'
      '  Nominal = :Nominal,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    InsertSQL.Strings = (
      'insert into BonSopir'
      
        '  (Kode, SuratJalan, Nominal, CreateDate, CreateBy, Operator, Tg' +
        'lEntry, TglCetak)'
      'values'
      
        '  (:Kode, :SuratJalan, :Nominal, :CreateDate, :CreateBy, :Operat' +
        'or, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from BonSopir'
      'where'
      '  Kode = :OLD_Kode and'
      '  SuratJalan = :OLD_SuratJalan')
    Left = 584
    Top = 152
  end
end
