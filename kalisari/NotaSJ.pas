unit NotaSJ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxCurrencyEdit,
  cxDropDownEdit, cxGroupBox, cxCheckGroup;

type
  TNotaSJFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    KodeQkodenota: TStringField;
    SOQ: TSDQuery;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    RuteQ: TSDQuery;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    updateQ: TSDQuery;
    buttonCetak: TcxButton;
    Crpe1: TCrpe;
    jarakQJumlahKm: TIntegerField;
    deleteBtn: TcxButton;
    cxDBVerticalGrid1Kir: TcxDBEditorRow;
    cxDBVerticalGrid1Pajak: TcxDBEditorRow;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    cxDBVerticalGrid1Sopir: TcxDBEditorRow;
    cxDBVerticalGrid1NamaSopir1: TcxDBEditorRow;
    cxDBVerticalGrid1Sopir2: TcxDBEditorRow;
    cxDBVerticalGrid1Crew: TcxDBEditorRow;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQNoSO: TStringField;
    MasterQSopir: TStringField;
    MasterQSopir2: TStringField;
    MasterQCrew: TStringField;
    MasterQTitipKwitansi: TBooleanField;
    MasterQNoKwitansi: TStringField;
    MasterQKir: TBooleanField;
    MasterQSTNK: TBooleanField;
    MasterQPajak: TBooleanField;
    MasterQTglKembali: TDateTimeField;
    MasterQSPBUAYaniLiter: TFloatField;
    MasterQSPBUAYaniUang: TCurrencyField;
    MasterQSPBULuarLiter: TFloatField;
    MasterQSPBULuarUang: TCurrencyField;
    MasterQSPBULuarUangDiberi: TCurrencyField;
    MasterQSPBULuarDetail: TMemoField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQLaporan: TMemoField;
    MasterQTglRealisasi: TDateTimeField;
    MasterQAwal: TStringField;
    MasterQAkhir: TStringField;
    MasterQTglCetak: TDateTimeField;
    MasterQClaimSopir: TCurrencyField;
    MasterQKeteranganClaimSopir: TStringField;
    MasterQPremiSopir: TCurrencyField;
    MasterQPremiSopir2: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQTabunganSopir: TCurrencyField;
    MasterQTabunganSopir2: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQUangJalan: TCurrencyField;
    MasterQBiayaLainLain: TCurrencyField;
    MasterQUangMakan: TCurrencyField;
    MasterQOther: TCurrencyField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    cxDBVerticalGrid1TitipKwitansi: TcxDBEditorRow;
    cxDBVerticalGrid1DBNoKwitansi: TcxDBEditorRow;
    cxDBVerticalGrid1NominalKwitansi: TcxDBEditorRow;
    MasterQNominalKwitansi: TCurrencyField;
    MasterQPendapatan: TCurrencyField;
    MasterQPengeluaran: TCurrencyField;
    MasterQSisaDisetor: TCurrencyField;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    MasterSOQ: TDataSource;
    MasterVGridKodenota: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridBerangkat: TcxDBEditorRow;
    MasterVGridTiba: TcxDBEditorRow;
    MasterVGridKapasitasSeat: TcxDBEditorRow;
    MasterVGridAC: TcxDBEditorRow;
    MasterVGridToilet: TcxDBEditorRow;
    MasterVGridAirSuspension: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridPICJemput: TcxDBEditorRow;
    MasterVGridJamJemput: TcxDBEditorRow;
    MasterVGridNoTelpPICJemput: TcxDBEditorRow;
    MasterVGridAlamatJemput: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPengemudi2: TcxDBEditorRow;
    cxDBVerticalGrid1STNK: TcxDBEditorRow;
    MasterVGridDari: TcxDBEditorRow;
    MasterVGridKe: TcxDBEditorRow;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridNoBody: TcxDBEditorRow;
    SOQView: TSDQuery;
    SOQViewKodenota: TStringField;
    SOQViewTgl: TDateTimeField;
    SOQViewPelanggan: TStringField;
    SOQViewBerangkat: TDateTimeField;
    SOQViewTiba: TDateTimeField;
    SOQViewHarga: TCurrencyField;
    SOQViewPembayaranAwal: TCurrencyField;
    SOQViewTglPembayaranAwal: TDateTimeField;
    SOQViewCaraPembayaranAwal: TStringField;
    SOQViewNoKwitansiPembayaranAwal: TStringField;
    SOQViewPenerimaPembayaranAwal: TStringField;
    SOQViewPelunasan: TCurrencyField;
    SOQViewTglPelunasan: TDateTimeField;
    SOQViewCaraPembayaranPelunasan: TStringField;
    SOQViewNoKwitansiPelunasan: TStringField;
    SOQViewPenerimaPelunasan: TStringField;
    SOQViewExtend: TBooleanField;
    SOQViewTglKembaliExtend: TDateTimeField;
    SOQViewBiayaExtend: TCurrencyField;
    SOQViewKapasitasSeat: TIntegerField;
    SOQViewAC: TBooleanField;
    SOQViewToilet: TBooleanField;
    SOQViewAirSuspension: TBooleanField;
    SOQViewRute: TStringField;
    SOQViewTglFollowUp: TDateTimeField;
    SOQViewArmada: TStringField;
    SOQViewKontrak: TStringField;
    SOQViewPICJemput: TMemoField;
    SOQViewJamJemput: TDateTimeField;
    SOQViewNoTelpPICJemput: TStringField;
    SOQViewAlamatJemput: TMemoField;
    SOQViewStatus: TStringField;
    SOQViewStatusPembayaran: TStringField;
    SOQViewReminderPending: TDateTimeField;
    SOQViewKeterangan: TMemoField;
    SOQViewCreateDate: TDateTimeField;
    SOQViewCreateBy: TStringField;
    SOQViewOperator: TStringField;
    SOQViewTglEntry: TDateTimeField;
    SOQViewTglCetak: TDateTimeField;
    MasterQKeteranganBiayaLainLain: TStringField;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    MasterQKeterangan: TMemoField;
    cxGrid1DBTableView1RuteBerangkat: TcxGridDBColumn;
    cxGrid1DBTableView1RuteTiba: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    MasterQTambahan: TStringField;
    MasterQUangInap: TCurrencyField;
    MasterQUangParkir: TCurrencyField;
    MasterQSPBUAYaniDetail: TStringField;
    MasterQTitipTagihan: TBooleanField;
    cxDBVerticalGrid1TitipTagihan: TcxDBEditorRow;
    CekTitipanQ: TSDQuery;
    CekTitipanQkode: TStringField;
    CekTitipanQTitipKuitansi: TIntegerField;
    CekTitipanQTitipTagihan: TIntegerField;
    DetailKwitansiSOQ: TSDQuery;
    SDUpdateSQL1: TSDUpdateSQL;
    DetailKwitansiSOQKodeKwitansi: TStringField;
    DetailKwitansiSOQKodeSO: TStringField;
    DetailKwitansiSOQNominal: TCurrencyField;
    CekTKQ: TSDQuery;
    CekTKQhitung: TIntegerField;
    CekTTQ: TSDQuery;
    CekTTQhitung: TIntegerField;
    MasterQTambahanTol: TBooleanField;
    cxDBVerticalGrid1TambahanTol: TcxDBEditorRow;
    MasterQTambahanParkir: TBooleanField;
    MasterQTambahanPolisi: TBooleanField;
    MasterQTambahanRetribusi: TBooleanField;
    MasterQTambahanKapal: TBooleanField;
    cxDBVerticalGrid1TambahanParkir: TcxDBEditorRow;
    cxDBVerticalGrid1TambahanPolisi: TcxDBEditorRow;
    cxDBVerticalGrid1TambahanRetribusi: TcxDBEditorRow;
    cxDBVerticalGrid1TambahanKapal: TcxDBEditorRow;
    SDQuery1: TSDQuery;
    cxGrid1DBTableView1SudahPrint: TcxGridDBColumn;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQTitlePICJemput: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    SOQKomisiPelanggan: TCurrencyField;
    SOQKeteranganRute: TMemoField;
    SOQKeteranganHarga: TMemoField;
    SOQKeteranganInternal: TMemoField;
    SOQNamaPelanggan: TStringField;
    SOQDari: TStringField;
    SOQKe: TStringField;
    SOQPlatNo: TStringField;
    SOQNoBody: TStringField;
    viewSJQTanggal: TStringField;
    viewSJQKodenota: TStringField;
    viewSJQTgl: TDateTimeField;
    viewSJQNoSO: TStringField;
    viewSJQSopir: TStringField;
    viewSJQSopir2: TStringField;
    viewSJQCrew: TStringField;
    viewSJQTitipKwitansi: TBooleanField;
    viewSJQNominalKwitansi: TCurrencyField;
    viewSJQNoKwitansi: TStringField;
    viewSJQKeterangan: TMemoField;
    viewSJQKir: TBooleanField;
    viewSJQSTNK: TBooleanField;
    viewSJQPajak: TBooleanField;
    viewSJQTglKembali: TDateTimeField;
    viewSJQPendapatan: TCurrencyField;
    viewSJQPengeluaran: TCurrencyField;
    viewSJQSisaDisetor: TCurrencyField;
    viewSJQSPBUAYaniLiter: TFloatField;
    viewSJQSPBUAYaniUang: TCurrencyField;
    viewSJQSPBULuarLiter: TFloatField;
    viewSJQSPBULuarUang: TCurrencyField;
    viewSJQSPBULuarUangDiberi: TCurrencyField;
    viewSJQSPBULuarDetail: TMemoField;
    viewSJQStatus: TStringField;
    viewSJQCreateDate: TDateTimeField;
    viewSJQCreateBy: TStringField;
    viewSJQOperator: TStringField;
    viewSJQTglEntry: TDateTimeField;
    viewSJQLaporan: TMemoField;
    viewSJQTglRealisasi: TDateTimeField;
    viewSJQAwal: TStringField;
    viewSJQAkhir: TStringField;
    viewSJQTglCetak: TDateTimeField;
    viewSJQClaimSopir: TCurrencyField;
    viewSJQKeteranganClaimSopir: TStringField;
    viewSJQPremiSopir: TCurrencyField;
    viewSJQPremiSopir2: TCurrencyField;
    viewSJQPremiKernet: TCurrencyField;
    viewSJQTabunganSopir: TCurrencyField;
    viewSJQTabunganSopir2: TCurrencyField;
    viewSJQTol: TCurrencyField;
    viewSJQUangJalan: TCurrencyField;
    viewSJQBiayaLainLain: TCurrencyField;
    viewSJQKeteranganBiayaLainLain: TStringField;
    viewSJQUangMakan: TCurrencyField;
    viewSJQUangInap: TCurrencyField;
    viewSJQUangParkir: TCurrencyField;
    viewSJQOther: TCurrencyField;
    viewSJQSPBUAYaniDetail: TStringField;
    viewSJQTitipTagihan: TBooleanField;
    viewSJQSudahPrint: TBooleanField;
    viewSJQVerifikasi: TCurrencyField;
    viewSJQKeteranganVerifikasi: TMemoField;
    viewSJQPlatNo: TStringField;
    viewSJQNamaPT: TStringField;
    viewSJQRuteBerangkat: TStringField;
    viewSJQRuteTiba: TStringField;
    viewSJQKapasitasSeat: TIntegerField;
    MasterQSudahPrint: TBooleanField;
    MasterQVerifikasi: TCurrencyField;
    MasterQKeteranganVerifikasi: TMemoField;
    MasterQNamaPengemudi: TStringField;
    MasterQNamaPengemudi2: TStringField;
    MasterQSPBUAYaniJam: TDateTimeField;
    viewSJQSPBUAYaniJam: TDateTimeField;
    cxTextEdit1: TcxTextEdit;
    KasBonBtn: TButton;
    KasBonQ: TSDQuery;
    KasBonQKode: TStringField;
    KasBonQSuratJalan: TStringField;
    KasBonQNominal: TCurrencyField;
    KasBonQCreateDate: TDateTimeField;
    KasBonQCreateBy: TStringField;
    KasBonQOperator: TStringField;
    KasBonQTglEntry: TDateTimeField;
    KasBonQTglCetak: TDateTimeField;
    KodeKasBonQ: TSDQuery;
    KodeKasBonQkode: TStringField;
    KasBonUs: TSDUpdateSQL;
    MasterQDanaKebersihan: TCurrencyField;
    MasterQVerifikasiStatus: TStringField;
    MasterQKmSekarang: TFloatField;
    MasterQSopirLuar: TStringField;
    MasterQJumlahUangsaku: TCurrencyField;
    cxDBVerticalGrid1JumlahUangsaku: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure buttonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1SopirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterQAfterPost(DataSet: TDataSet);
    procedure MasterVGridKodenotaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxDBVerticalGrid1DBEditorRow2EditPropertiesChange(
      Sender: TObject);
    procedure cxDBVerticalGrid1TitipTagihanEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1TambahanTolEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1TambahanParkirEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1TambahanPolisiEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1TambahanRetribusiEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1TambahanKapalEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure KasBonBtnClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  NotaSJFm: TNotaSJFm;
  MasterOriSQL, SOOriSQL,MasterKeterangan: string;
  kodeprint :String;
  IsiBaru : integer;
implementation

uses MenuUtama, DropDown, DM, StrUtils, SOSJDropDown, PengemudiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotaSJFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TNotaSJFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotaSJFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotaSJFm.ExitBtnClick(Sender: TObject);
begin
  Close;
  Release;
end;

procedure TNotaSJFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  SOOriSQL:=SOQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKodenota.Size;   
end;

procedure TNotaSJFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 try

  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  //MasterQ.Open;
  //SOQ.Open;
  MasterQ.Open;
  MasterVGridKodenota.Properties.EditProperties.Buttons[0].Enabled:=True;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  //DMFm.GetDateQ.Open;
  //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
 // MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  MasterQTitipKwitansi.AsBoolean:=false;
  MasterQTitipTagihan.AsBoolean:=false;
  //MasterQ.Close;
  //MasterQ.Open;
    SOQ.Close;
    SOQ.SQL.Text:=SOOriSQL;
    SOQ.ParamByName('text').AsString:='';
    SOQ.Open;
    SOQ.Close;

  //SOQ.Open;
  //SOQ.Insert;
  except
  end;
end;

procedure TNotaSJFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TNotaSJFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  PelangganQ.Open;
  PegawaiQ.Open;
  RuteQ.Open;
  ArmadaQ.Open;
  SOQView.Open;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-3;
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewSJQ.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertSuratJalan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSuratJalan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteSuratJalan.AsBoolean;
  ButtonCetak.Enabled:= TRUE;
  KasBonBtn.Enabled:=false;

end;

procedure TNotaSJFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
   { MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;  }
  masterq.Close;
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
  MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQKir.AsBoolean :=false;
      MasterQSTNK.AsBoolean :=false;
      MasterQPajak.AsBoolean :=false;
      MasterQTitipKwitansi.AsBoolean:=false;
      MasterQStatus.AsString :='IN PROGRESS';
      MasterQKeterangan.AsString:='Tol, Parkir, Polisi, Retribusi, Penyebrangan Penumpang / Kapal,  ditanggung oleh penyewa.';
      MasterQTambahanTol.AsBoolean:=true;
      MasterQTambahanParkir.AsBoolean:=true;
      MasterQTambahanPolisi.AsBoolean:=true;
      MasterQTambahanRetribusi.AsBoolean:=true;
      MasterQTambahanKapal.AsBoolean:=true;
      KasBonBtn.Enabled:=false;
      IsiBaru:=1;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSuratJalan.AsBoolean;
      KasBonBtn.Enabled:=true;
      IsiBaru:=0;
  SOQ.Close;
  SOQ.ParamByName('text').AsString:=MasterQNoSO.AsString;
  SOQ.Open;
  if MasterQTitipKwitansi.AsBoolean = TRUE then
    begin
      cxDBVerticalGrid1DBNoKwitansi.Visible:=true;
      cxDBVerticalGrid1NominalKwitansi.Visible:=true;
      MasterQ.Edit;
      MasterQNominalKwitansi.AsCurrency:=SOQHarga.AsCurrency+SOQBiayaExtend.AsCurrency+SOQPPN.AsCurrency+SOQPPNExtend.AsCurrency-SOQPembayaranAwal.AsCurrency-SOQPelunasan.AsCurrency;
    end
  else
    begin
      cxDBVerticalGrid1DBNoKwitansi.Visible:=false;
      cxDBVerticalGrid1NominalKwitansi.Visible:=false;
      MasterQ.Edit;
      MasterQNominalKwitansi.AsCurrency:=0;
    end;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateSuratJalan.AsBoolean;
    MasterVGrid.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
  if MasterQSopir.AsString<>'' then
  begin
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopir.AsString;
    PegawaiQ.Open;
    MasterQ.Edit;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  end;
  if MasterQSopir2.AsString<>'' then
  begin
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopir2.AsString;
    PegawaiQ.Open;
    MasterQ.Edit;
    MasterQNamaPengemudi2.AsString:=PegawaiQNama.AsString;
  end;
  end;
cxTextEdit1.Text:='';
end;

procedure TNotaSJFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;

  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TNotaSJFm.SaveBtnClick(Sender: TObject);
var sKode,nama1,nama2 :String;
begin
 // kodeprint :=  MasterQKodenota.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKodenota.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkodenota.AsString)+1),10,'0',True);
    end;
    //MasterQStatus.AsString :='IN PROGRESS';
    kodeprint :=  MasterQKodenota.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKodenota.AsString;
  end;
  //ShowMessage(MasterQKodeNota.AsString);
  MenuUtamaFm.Database1.StartTransaction;
  try
   if IsiBaru=0 then
  begin
    MasterQ.Edit;
    //MasterQOperator.AsString:=User;
    //MasterQTglEntry.AsDateTime:=now;
  end;
    MasterQ.ApplyUpdates;
    sKode := MasterQKodenota.AsString;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    //ShowMessage(sKode);
    ShowMessage('Surat Jalan dengan Kode ' + KodeEdit.Text + ' telah disimpan');

  except
  on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
      nama1:=MasterQNamaPengemudi.AsString;
    nama2:=MasterQNamaPengemudi2.AsString;
  SOQ.Close;
  viewSJQ.Close;
  viewSJQ.Open;
  KodeEdit.SetFocus;
  //cetak SJ
  if MessageDlg('Cetak Surat Jalan '+kodeprint+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    SDQuery1.SQL.Text:='update MasterSJ set SudahPrint=1 where Kodenota='+QuotedStr(sKode);
    SDQuery1.ExecSQL;
    viewSJQ.Close;
    viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
    viewSJQ.Open;
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
    Crpe1.ParamFields[0].CurrentValue:=sKode;
    Crpe1.ParamFields[1].CurrentValue:=nama1;
    Crpe1.ParamFields[2].CurrentValue:=nama2;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(kodeprint));
      updateQ.ExecSQL;
      updateQ.Close;
  end;
end;

procedure TNotaSJFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
 // SOQ.Close;
  //SOQ.Open
  //DMFm.GetDateQ.Close;
end;

procedure TNotaSJFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  if IsiBaru=0 then
  begin
    MasterQOperator.AsString:=User;
  end;
  //DMFm.GetDateQ.Close;   }
  MasterQStatus.AsString:='ON GOING';
end;

procedure TNotaSJFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKodeNota.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Surat Jalan telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Surat Jalan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     SOQ.Close;
     viewSJQ.Close;
     viewSJQ.Open;
    { SOQ.ParamByName('text').AsString:='';
     SOQ.Open;
     showmessage('a');
     SOQ.Close;     }
     KodeEdit.SetFocus;

  end;
end;

procedure TNotaSJFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSJFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQNoSO.AsString:=SOQkodenota.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSJFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL : string;
  begin
    SOQ.Close;
    tempSQL:=SOQ.SQL.Text;
    SOQ.SQL.Text:='select so.*  from MasterSO so left outer join MasterSJ sj on sj.NoSO=so.Kodenota where sj.Kodenota is NULL and so.Armada<>NULL';
    //SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
     // MasterQ.Open;
    //  MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQkodenota.AsString;

    end;
    DropDownFm.Release;
    SOQ.SQL.Text:=tempSQL;
end;

procedure TNotaSJFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  buttonCetak.Enabled:=true;
  KasBonBtn.Enabled:=true;
  cxTextEdit1.Text:='';
{  ArmadaQ.Open;
  PelangganQ.Open;
  PegawaiQ.Open;
  jarakQ.Open;
  RuteQ.Open;
  SOQ.Open;      }
  KodeEdit.SetFocus;
  KodeEdit.text:=viewSJQKodenota.AsString;
  //KodeEditExit(self);
  masterq.Close;
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
  MasterQ.Open;
  if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterVGridKodenota.Properties.EditProperties.Buttons[0].Enabled:=True;
    end
  else
    begin
      MasterQ.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterVGridKodenota.Properties.EditProperties.Buttons[0].Enabled:=False;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSuratJalan.AsBoolean;
    end;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSuratJalan.AsBoolean;
  MasterVGrid.Enabled:=True;
  buttonCetak.Enabled:=True;
  cxDBVerticalGrid1.Enabled:=true;
  if MasterQSopir.AsString<>'' then
  begin
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopir.AsString;
    PegawaiQ.Open;
    MasterQ.Edit;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  end;
  if MasterQSopir2.AsString<>'' then
  begin
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQSopir2.AsString;
    PegawaiQ.Open;
    MasterQ.Edit;
    MasterQNamaPengemudi2.AsString:=PegawaiQNama.AsString;
  end;
  SOQ.Close;
  SOQ.ParamByName('text').AsString:=MasterQNoSO.AsString;
  SOQ.Open;
  if MasterQTitipKwitansi.AsBoolean = TRUE then
    begin
      cxDBVerticalGrid1DBNoKwitansi.Visible:=true;
      cxDBVerticalGrid1NominalKwitansi.Visible:=true;
      MasterQ.Edit;
      MasterQNominalKwitansi.AsCurrency:=SOQHarga.AsCurrency+SOQBiayaExtend.AsCurrency+SOQPPN.AsCurrency+SOQPPNExtend.AsCurrency-SOQPembayaranAwal.AsCurrency-SOQPelunasan.AsCurrency;
    end
  else
    begin
      cxDBVerticalGrid1DBNoKwitansi.Visible:=false;
      cxDBVerticalGrid1NominalKwitansi.Visible:=false;
      MasterQ.Edit;
      MasterQNominalKwitansi.AsCurrency:=0;
    end;
  MasterQ.Edit;
end;

procedure TNotaSJFm.buttonCetakClick(Sender: TObject);
begin
  //cetak SJ
  if MessageDlg('Cetak Surat Jalan '+MasterQKodenota.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    SDQuery1.SQL.Text:='update MasterSJ set SudahPrint=1 where Kodenota='+QuotedStr(MasterQKodenota.AsString);
    SDQuery1.ExecSQL;
    viewSJQ.Close;
    viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
    viewSJQ.Open;
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
    Crpe1.ParamFields[0].CurrentValue:=MasterQKodenota.AsString;
    Crpe1.ParamFields[1].CurrentValue:=MasterQNamaPengemudi.AsString;
    Crpe1.ParamFields[2].CurrentValue:=MasterQNamaPengemudi2.AsString;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

    Crpe1.Execute;
  //  Crpe1.Print;
   // Crpe1.CloseWindow;
    updateQ.Close;
    updateQ.SQL.Clear;
    updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(MasterQKodenota.AsString));
    updateQ.ExecSQL;
    updateQ.Close;
    viewSJQ.Close;
    viewSJQ.Open;
  end;
  //end Cetak SJ
end;

procedure TNotaSJFm.cxDBVerticalGrid1SopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL:string;
  begin
   PengemudiDropDownFm:=TPengemudiDropdownfm.Create(Self);
    if PengemudiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      MasterQSopir.AsString:=PengemudiDropDownFm.kode;
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:=MasterQSopir.AsString;
      PegawaiQ.Open;
      MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
      //MasterQNoSO.AsString:=MasterQNoSO.AsString;
    end;
    PengemudiDropDownFm.Release;
    //PegawaiQ.SQL.Text:=tempSQL;
end;

procedure TNotaSJFm.cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
    var tempSQL:string;
  begin
   PengemudiDropDownFm:=TPengemudiDropdownfm.Create(Self);
    if PengemudiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      MasterQSopir2.AsString:=PengemudiDropDownFm.kode;
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:=MasterQSopir2.AsString;
      PegawaiQ.Open;
      MasterQNamaPengemudi2.AsString:=PegawaiQNama.AsString;
    end;
    PengemudiDropDownFm.Release;
    //PegawaiQ.SQL.Text:=tempSQL;
end;

procedure TNotaSJFm.MasterQAfterPost(DataSet: TDataSet);
begin
//MasterQ.Refresh;
end;

procedure TNotaSJFm.MasterVGridKodenotaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL,tempnokwitansi: string;
  var TitipKuitansi,TitipTagihan,ada:integer;
  begin
     ada:=0;
    TitipKuitansi:=0;
    TitipTagihan:=0;
    tempnokwitansi:='';
    SOSJDropDownFm:=TSOSJDropdownfm.Create(Self);
    if SOSJDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQNoSO.AsString:=SOSJDropDownFm.kode;
      SOQ.close;
      SOQ.ParamByName('text').AsString:=SOSJDropDownFm.kode;
      SOQ.Open;
      MasterQ.Edit;
      MasterQTgl.AsDateTime:=SOQBerangkat.AsDateTime;
    end;
    SOSJDropDownFm.Release;
    CekTitipanQ.Close;
    CekTitipanQ.ParamByName('text').AsString:=MasterQNoSO.AsString;
    CekTitipanQ.Open;
    if CekTitipanQTitipKuitansi.AsInteger=0 then
    begin
       MasterQ.Edit;
       MasterQTitipKwitansi.AsBoolean:=false;
    end
    else
    begin
        tempnokwitansi:=cektitipanqkode.asstring;
        DetailKwitansiSOQ.Close;
        DetailKwitansiSOQ.ParamByName('text').AsString:=CekTitipanQkode.AsString;
        DetailKwitansiSOQ.Open;
        DetailKwitansiSOQ.Edit;
        DetailKwitansiSOQ.First;
        while DetailKwitansiSOQ.Eof=false do
        begin
             CekTKQ.Close;
             CekTKQ.ParamByName('text').AsString:=DetailKwitansiSOQKodeSO.AsString;
             CekTKQ.Open;
             if CekTKQhitung.AsInteger=1 then
             begin
                 TitipKuitansi:=1;
             end;
             DetailKwitansiSOQ.Next;
        end;
        if TitipKuitansi=1 then
        begin
            MasterQ.Edit;
            MasterQTitipKwitansi.AsBoolean:=false;
        end
        else
        begin
            MasterQ.Edit;
            MasterQTitipKwitansi.AsBoolean:=true;
            ada:=1;
        end;
    end;

    if CekTitipanQTitipTagihan.AsInteger=0 then
    begin
       MasterQ.Edit;
       MasterQTitipTagihan.AsBoolean:=false;
    end
    else
    begin
      tempnokwitansi:=cektitipanqkode.AsString;
        DetailKwitansiSOQ.Close;
        DetailKwitansiSOQ.ParamByName('text').AsString:=CekTitipanQkode.AsString;
        DetailKwitansiSOQ.Open;
        DetailKwitansiSOQ.Edit;
        DetailKwitansiSOQ.First;
        while DetailKwitansiSOQ.Eof=false do
        begin
             CekTTQ.Close;
             CekTTQ.ParamByName('text').AsString:=DetailKwitansiSOQKodeSO.AsString;
             CekTTQ.Open;
             if CekTKQhitung.AsInteger=1 then
             begin
                 TitipTagihan:=1;
             end;
             DetailKwitansiSOQ.Next;
        end;
        if TitipTagihan=1 then
        begin
            MasterQ.Edit;
            MasterQTitipTagihan.AsBoolean:=false;
        end
        else
        begin
            MasterQ.Edit;
            MasterQTitipTagihan.AsBoolean:=true;
            ada:=1;
            cxDBVerticalGrid1DBNoKwitansi.Visible:=true;
            cxDBVerticalGrid1NominalKwitansi.Visible:=true;
            MasterQ.Edit;
            MasterQNominalKwitansi.AsCurrency:=SOQHarga.AsCurrency+SOQBiayaExtend.AsCurrency+SOQPPNExtend.AsCurrency-SOQPembayaranAwal.AsCurrency-SOQPelunasan.AsCurrency;
        end;
    end;
    if ada=1 then
    begin
        MasterKeterangan:='No. Kuitansi: '+tempnokwitansi+'.'+Chr(13)+Chr(10);
        MasterQ.Edit;
        MasterQKeterangan.AsString:=MasterKeterangan;
    end
    else
    begin
        MasterKeterangan:='';
        MasterQ.Edit;
        MasterQKeterangan.AsString:=MasterKeterangan;
    end;
    MasterQ.Edit;
    MasterQTambahanTol.AsBoolean:=true;
    MasterQTambahanParkir.AsBoolean:=true;
    MasterQTambahanPolisi.AsBoolean:=true;
    MasterQTambahanRetribusi.AsBoolean:=true;
    MasterQTambahanKapal.AsBoolean:=true;
      MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
end;

procedure TNotaSJFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSJQ.Open;
end;

procedure TNotaSJFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSJQ.Open;
end;

procedure TNotaSJFm.cxDBVerticalGrid1DBEditorRow2EditPropertiesChange(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahan.AsString[1]='1' then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahan.AsString[2]='1' then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahan.AsString[3]='1' then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahan.AsString[4]='1' then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahan.AsString[5]='1' then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
end;

procedure TNotaSJFm.cxDBVerticalGrid1TitipTagihanEditPropertiesEditValueChanged(
  Sender: TObject);
begin
 If MasterQTitipTagihan.AsBoolean=true then
  begin
    cxDBVerticalGrid1DBNoKwitansi.Visible:=true;
    cxDBVerticalGrid1NominalKwitansi.Visible:=true;
   MasterQNominalKwitansi.AsCurrency:=SOQHarga.AsCurrency+SOQBiayaExtend.AsCurrency+SOQPPNExtend.AsCurrency-SOQPembayaranAwal.AsCurrency-SOQPelunasan.AsCurrency;

  end
  else
  begin
     cxDBVerticalGrid1DBNoKwitansi.Visible:=false;
    cxDBVerticalGrid1NominalKwitansi.Visible:=false;
    MasterQNominalKwitansi.AsCurrency:=0;
  end;
end;

procedure TNotaSJFm.cxDBVerticalGrid1TambahanTolEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
  end;

procedure TNotaSJFm.cxDBVerticalGrid1TambahanParkirEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
  end;

procedure TNotaSJFm.cxDBVerticalGrid1TambahanPolisiEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
  end;

procedure TNotaSJFm.cxDBVerticalGrid1TambahanRetribusiEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
  end;

procedure TNotaSJFm.cxDBVerticalGrid1TambahanKapalEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  MasterQKeterangan.AsString:=MasterKeterangan;
  if MasterQTambahanTol.AsBoolean=True then MasterQKeterangan.AsString:=MasterQKeterangan.AsString+'Tol, ';
  if MasterQTambahanParkir.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Parkir, ';
  if MasterQTambahanPolisi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Polisi, ';
  if MasterQTambahanRetribusi.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Retribusi, ';
  if MasterQTambahanKapal.AsBoolean=True then MasterQKeterangan.AsString:=Masterqketerangan.asstring+'Penyebrangan Penumpang / Kapal, ';
  if MasterQKeterangan.AsString<>'' then
  begin
    if MasterQKeterangan.AsString<>MasterKeterangan then
    begin
      MasterQKeterangan.AsString:=MasterQKeterangan.AsString+' ditanggung oleh penyewa.';
    end;
  end;
end;

procedure TNotaSJFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[6] = true) then
  begin
    ACanvas.Brush.Color := 16706935;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

procedure TNotaSJFm.KasBonBtnClick(Sender: TObject);
begin
KodeKasBonQ.Close;
KodeKasBonQ.Open;
KasBonQ.Close;
KasBonQ.Open;
KasBonQ.Append;
KasBonQ.Edit;
if KodeKasBonQ.IsEmpty then
  begin
     KasBonQKode.AsString:=DMFm.Fill('1',10,'0',True);
  end
else
  begin
      KasBonQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeKasBonQkode.AsString)+1),10,'0',True);
  end;
KasBonQSuratJalan.AsString:=MasterQKodenota.AsString;
KasBonQNominal.AsCurrency:=strtocurr(cxTextEdit1.Text);
MenuUtamaFm.Database1.StartTransaction;
KasBonQ.ApplyUpdates;
MenuUtamaFm.Database1.Commit;
KasBonQ.CommitUpdates;
ShowMessage('Pembuatan Kas Bon Berhasil');
end;

end.
