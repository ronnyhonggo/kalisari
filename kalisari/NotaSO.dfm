object NotaSOFm: TNotaSOFm
  Left = 350
  Top = 0
  Width = 828
  Height = 728
  Align = alCustom
  Caption = 'Penawaran / Order (SO)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object Label1: TLabel
      Left = 440
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Copy'
    end
    object lblAudit: TLabel
      Left = 40
      Top = 32
      Width = 3
      Height = 13
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxRadioButton1: TcxRadioButton
      Left = 176
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Non Kontrak'
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = cxRadioButton1Click
      Transparent = True
    end
    object cxRadioButton2: TcxRadioButton
      Left = 276
      Top = 13
      Width = 113
      Height = 17
      Caption = 'Kontrak'
      TabOrder = 2
      OnClick = cxRadioButton2Click
      Transparent = True
    end
    object ButtonCopy: TButton
      Left = 600
      Top = 7
      Width = 75
      Height = 25
      Caption = 'Copy'
      TabOrder = 3
      OnClick = ButtonCopyClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 443
    Width = 812
    Height = 228
    Align = alBottom
    TabOrder = 2
    object Label2: TLabel
      Left = 256
      Top = 190
      Width = 86
      Height = 16
      Caption = 'Status Kuitansi'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 256
      Top = 206
      Width = 3
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 194
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object deleteBtn: TcxButton
      Left = 92
      Top = 194
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = deleteBtnClick
    end
    object ExitBtn: TcxButton
      Left = 180
      Top = 194
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object ButtonCetak: TcxButton
      Left = 664
      Top = 202
      Width = 75
      Height = 25
      Caption = 'CETAK'
      TabOrder = 3
      Visible = False
      OnClick = ButtonCetakClick
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 810
      Height = 176
      Align = alTop
      TabOrder = 4
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 808
        Height = 174
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = False
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = ViewDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              FieldName = 'KapasitasSeat'
              Column = cxGrid1DBTableView1Column8
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsData.Editing = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1Kodenota: TcxGridDBColumn
            DataBinding.FieldName = 'Kodenota'
            Width = 88
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Berangkat'
            PropertiesClassName = 'TcxDateEditProperties'
            Width = 120
          end
          object cxGrid1DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'Tiba'
            Width = 163
          end
          object cxGrid1DBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'NamaPelanggan'
            Options.SortByDisplayText = isbtOn
            Width = 184
          end
          object cxGrid1DBTableView1Column4: TcxGridDBColumn
            DataBinding.FieldName = 'Dari'
            Width = 112
          end
          object cxGrid1DBTableView1Column5: TcxGridDBColumn
            DataBinding.FieldName = 'Ke'
            Width = 122
          end
          object cxGrid1DBTableView1Column8: TcxGridDBColumn
            DataBinding.FieldName = 'KapasitasSeat'
            Width = 103
          end
          object cxGrid1DBTableView1Column6: TcxGridDBColumn
            DataBinding.FieldName = 'Status'
            Width = 141
          end
          object cxGrid1DBTableView1Column7: TcxGridDBColumn
            DataBinding.FieldName = 'StatusPembayaran'
            Width = 130
          end
          object cxGrid1DBTableView1AC: TcxGridDBColumn
            DataBinding.FieldName = 'AC'
          end
          object cxGrid1DBTableView1Toilet: TcxGridDBColumn
            DataBinding.FieldName = 'Toilet'
            Width = 47
          end
          object cxGrid1DBTableView1AirSuspension: TcxGridDBColumn
            DataBinding.FieldName = 'AirSuspension'
            Width = 45
          end
          object cxGrid1DBTableView1Column9: TcxGridDBColumn
            DataBinding.FieldName = 'PembuatSO'
            Width = 109
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxGroupBox1: TcxGroupBox
      Left = 520
      Top = 177
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 50
      Width = 291
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel1: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 369
    Height = 395
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 169
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTgl: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.DataBinding.FieldName = 'Tgl'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridKontrak: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKontrakEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kontrak'
      Visible = False
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPelangganEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pelanggan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridNamaPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPelanggan'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object MasterVGridAlamatPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'AlamatPelanggan'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object MasterVGridTelpPelanggan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'TelpPelanggan'
      Properties.Options.Editing = False
      ID = 5
      ParentID = 2
      Index = 2
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
          Caption = 'Lihat Harga'
        end>
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.OnButtonClick = MasterVGridRuteEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Rute'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridDari: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Dari'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object MasterVGridKe: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Ke'
      Properties.Options.Editing = False
      ID = 8
      ParentID = 6
      Index = 1
      Version = 1
    end
    object MasterVGridKategoriRute: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'KategoriRute'
      Properties.Options.Editing = False
      ID = 9
      ParentID = 6
      Index = 2
      Version = 1
    end
    object MasterVGridBerangkat: TcxDBEditorRow
      Properties.Caption = 'TglMulai'
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.Kind = ckDateTime
      Properties.DataBinding.FieldName = 'Berangkat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridTiba: TcxDBEditorRow
      Properties.Caption = 'TglSelesai'
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.Kind = ckDateTime
      Properties.DataBinding.FieldName = 'Tiba'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 11
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.DataBinding.FieldName = 'Harga'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 12
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridKeteranganHarga: TcxDBEditorRow
      Height = 34
      Properties.DataBinding.FieldName = 'KeteranganHarga'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 13
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridDBEditorRow6: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = MasterVGridDBEditorRow6EditPropertiesValidate
      Properties.DataBinding.FieldName = 'PPN'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 14
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KapasitasSeat'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 15
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridDBEditorRow3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AC'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 16
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridDBEditorRow4: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Toilet'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 17
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridDBEditorRow5: TcxDBEditorRow
      Height = 17
      Properties.DataBinding.FieldName = 'AirSuspension'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 18
      ParentID = -1
      Index = 12
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 671
    Width = 812
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxDBVerticalGrid2: TcxDBVerticalGrid
    Left = 369
    Top = 48
    Width = 443
    Height = 395
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 197
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 4
    OnEnter = cxDBVerticalGrid2Enter
    OnExit = cxDBVerticalGrid2Exit
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBVerticalGrid2TitlePICJemput: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Items = <
        item
          Caption = 'Bapak'
          Value = 'Bapak'
        end
        item
          Caption = 'Ibu'
          Value = 'Ibu'
        end
        item
          Caption = 'None'
          Value = ' '
        end>
      Properties.DataBinding.FieldName = 'TitlePICJemput'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2DBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PICJemput'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid2DBEditorRow4: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AlamatJemput'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid2DBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoTelpPICJemput'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid2DBEditorRow3: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.Kind = ckDateTime
      Properties.DataBinding.FieldName = 'JamJemput'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid2Status: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 2
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.Items = <
        item
          Caption = 'DEAL'
          Value = 'DEAL'
        end
        item
          Caption = 'PENDING'
          Value = 'PENDING'
        end
        item
          Caption = 'CANCEL'
          Value = 'CANCEL'
        end
        item
          Caption = 'NO DEAL'
          Value = 'NO DEAL'
        end
        item
          Caption = 'VERPAL'
          Value = 'VERPAL'
        end
        item
          Caption = 'GANTI'
          Value = 'GANTI'
        end>
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid2StatusEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid2ReminderPending: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.Kind = ckDateTime
      Properties.DataBinding.FieldName = 'ReminderPending'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid2PenerimaPending: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2PenerimaPendingEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PenerimaPending'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid2NamaPenerima: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPenerima'
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2JabatanPenerima: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanPenerima'
      ID = 9
      ParentID = 7
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid2Keterangan: TcxDBEditorRow
      Height = 47
      Properties.DataBinding.FieldName = 'Keterangan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid2Pelunasan: TcxDBEditorRow
      Properties.Caption = 'Pembayaran II'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid2PelunasanEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Pelunasan'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid2NoKwitansiPelunasan: TcxDBEditorRow
      Properties.Caption = 'NoKwitansiPembayaranII'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2NoKwitansiPelunasanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'NoKwitansiPelunasan'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid2NominalKwitansiPelunasan: TcxDBEditorRow
      Properties.Caption = 'NominalKwitansiPembayaranII'
      Properties.DataBinding.FieldName = 'NominalKwitansiPelunasan'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid2TglPelunasan: TcxDBEditorRow
      Properties.Caption = 'TglPembayaranII'
      Properties.DataBinding.FieldName = 'TglPelunasan'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid2CaraPembayaranPelunasan: TcxDBEditorRow
      Properties.Caption = 'CaraPembayaranII'
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Items = <
        item
          Caption = 'CASH'
          Value = 'CASH'
        end
        item
          Caption = 'EDC'
          Value = 'EDC'
        end
        item
          Caption = 'TRANSFER'
          Value = 'TRANSFER'
        end>
      Properties.DataBinding.FieldName = 'CaraPembayaranPelunasan'
      Visible = False
      ID = 15
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid2KetCaraPembayaranPelunasan: TcxDBEditorRow
      Height = 63
      Properties.Caption = 'Ket.CaraPembayaran'
      Properties.DataBinding.FieldName = 'KetCaraPembayaranPelunasan'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid2PenerimaPelunasan: TcxDBEditorRow
      Properties.Caption = 'PenerimaPembayaranII'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2PenerimaPelunasanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PenerimaPelunasan'
      Visible = False
      ID = 17
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid2NamaPegawaiPelunasan: TcxDBEditorRow
      Properties.Caption = 'NamaPenerima'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPegawaiPelunasan'
      Properties.Options.Editing = False
      Visible = False
      ID = 18
      ParentID = 17
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid2JabatanPenerimaPelunasan: TcxDBEditorRow
      Properties.Caption = 'JabatanPenerima'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'JabatanPenerimaPelunasan'
      Properties.Options.Editing = False
      Visible = False
      ID = 19
      ParentID = 17
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid2Extend: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.OnEditValueChanged = cxDBVerticalGrid2ExtendEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'Extend'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 20
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid2BiayaExtend: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid2BiayaExtendEditPropertiesValidate
      Properties.DataBinding.FieldName = 'BiayaExtend'
      Visible = False
      ID = 21
      ParentID = -1
      Index = 17
      Version = 1
    end
    object cxDBVerticalGrid2PPNExtend: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid2PPNExtendEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PPNExtend'
      Visible = False
      ID = 22
      ParentID = -1
      Index = 18
      Version = 1
    end
    object cxDBVerticalGrid2TglKembaliExtend: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglKembaliExtend'
      Visible = False
      ID = 23
      ParentID = -1
      Index = 19
      Version = 1
    end
    object cxDBVerticalGrid2StatusPembayaran: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'StatusPembayaran'
      Properties.Options.Editing = False
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 24
      ParentID = -1
      Index = 20
      Version = 1
    end
    object cxDBVerticalGrid2SisaPembayaran: TcxDBEditorRow
      Properties.Caption = 'SisaPembayaranSebelumSJ'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid2SisaPembayaranEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SisaPembayaran'
      Properties.Options.Editing = False
      Properties.Options.Filtering = False
      Properties.Options.IncSearch = False
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 25
      ParentID = -1
      Index = 21
      Version = 1
    end
    object cxDBVerticalGrid2KeteranganRute: TcxDBEditorRow
      Height = 59
      Properties.EditPropertiesClassName = 'TcxMemoProperties'
      Properties.DataBinding.FieldName = 'KeteranganRute'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 26
      ParentID = -1
      Index = 22
      Version = 1
    end
    object cxDBVerticalGrid2KeteranganInternal: TcxDBEditorRow
      Height = 57
      Properties.DataBinding.FieldName = 'KeteranganInternal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 27
      ParentID = -1
      Index = 23
      Version = 1
    end
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 472
    Top = 8
    Properties.OnChange = cxSpinEdit1PropertiesChange
    TabOrder = 5
    Width = 121
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *,cast('#39#39' as money) as SisaPembayaran from masterso')
    UpdateObject = MasterUS
    Left = 569
    Top = 57
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQTelpPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'TelpPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQKe: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQKategoriRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KategoriRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object MasterQStandardHargaRute: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'StandardHargaRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'StandarHarga'
      KeyFields = 'Rute'
      Lookup = True
    end
    object MasterQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object MasterQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object MasterQAC: TBooleanField
      FieldName = 'AC'
    end
    object MasterQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object MasterQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object MasterQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object MasterQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object MasterQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object MasterQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object MasterQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object MasterQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 50
    end
    object MasterQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object MasterQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object MasterQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object MasterQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 50
    end
    object MasterQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object MasterQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object MasterQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object MasterQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object MasterQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object MasterQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object MasterQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object MasterQNamaPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object MasterQStandarHargaMax: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'StandarHargaMax'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'StandarHargaMax'
      KeyFields = 'Rute'
      Lookup = True
    end
    object MasterQNamaPegawaiPelunasan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawaiPelunasan'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPelunasan'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerimaPelunasan: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerimaPelunasan'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPelunasan'
      Size = 50
      Lookup = True
    end
    object MasterQSisaPembayaran: TCurrencyField
      FieldName = 'SisaPembayaran'
      LookupCache = True
    end
    object MasterQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object MasterQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object MasterQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object MasterQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object MasterQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object MasterQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPending'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPending'
      Size = 50
      Lookup = True
    end
    object MasterQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object MasterQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object MasterQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object MasterQKeteranganRute: TMemoField
      DisplayWidth = 1000
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object MasterQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
    object MasterQTitlePICJemput: TStringField
      FieldName = 'TitlePICJemput'
      Size = 50
    end
    object MasterQKeteranganInternal: TMemoField
      FieldName = 'KeteranganInternal'
      BlobType = ftMemo
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 604
    Top = 54
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganC' +
        'araPembayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPemb' +
        'ayaranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, Car' +
        'aPembayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelu' +
        'nasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglK' +
        'embaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet,' +
        ' AirSuspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemp' +
        'ut, PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status,' +
        ' StatusPembayaran, ReminderPending, PenerimaPending, Keterangan,' +
        ' CreateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelan' +
        'ggan, KeteranganRute, KeteranganHarga, KeteranganInternal'#13#10'from ' +
        'masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update masterso'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  KeteranganCaraPembayaranAwal = :KeteranganCaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  KetCaraPembayaranPelunasan = :KetCaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  TitlePICJemput = :TitlePICJemput,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KomisiPelanggan = :KomisiPelanggan,'
      '  KeteranganRute = :KeteranganRute,'
      '  KeteranganHarga = :KeteranganHarga,'
      '  KeteranganInternal = :KeteranganInternal'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into masterso'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganCaraP' +
        'embayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPembayar' +
        'anAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPem' +
        'bayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelunasa' +
        'n, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglKemba' +
        'liExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet, Air' +
        'Suspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemput, ' +
        'PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status, Sta' +
        'tusPembayaran, ReminderPending, PenerimaPending, Keterangan, Cre' +
        'ateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelanggan' +
        ', KeteranganRute, KeteranganHarga, KeteranganInternal)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :Kete' +
        'ranganCaraPembayaranAwal, :NoKwitansiPembayaranAwal, :NominalKwi' +
        'tansiPembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPe' +
        'lunasan, :CaraPembayaranPelunasan, :KetCaraPembayaranPelunasan, ' +
        ':NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPeluna' +
        'san, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :Kapa' +
        'sitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :A' +
        'rmada, :Kontrak, :TitlePICJemput, :PICJemput, :JamJemput, :NoTel' +
        'pPICJemput, :AlamatJemput, :Status, :StatusPembayaran, :Reminder' +
        'Pending, :PenerimaPending, :Keterangan, :CreateDate, :CreateBy, ' +
        ':Operator, :TglEntry, :TglCetak, :KomisiPelanggan, :KeteranganRu' +
        'te, :KeteranganHarga, :KeteranganInternal)')
    DeleteSQL.Strings = (
      'delete from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 636
    Top = 50
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from masterso order by kodenota desc')
    Left = 697
    Top = 47
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39)
    Left = 777
    Top = 71
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      ''
      '')
    Left = 609
    Top = 87
    ParamData = <
      item
        DataType = ftInteger
        Name = 'text'
        ParamType = ptInput
        Value = ' '
      end
      item
        DataType = ftInteger
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
  end
  object ArmadaQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada'
      ''
      '')
    Left = 657
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select m.* from masterso m where'
      '(cast(m.berangkat as date) <= :text2 or Berangkat is null) and'
      '(cast(m.tiba as date) >= :text1 or Tiba is null)'
      'order by tglentry desc')
    Left = 313
    Top = 497
    ParamData = <
      item
        DataType = ftDate
        Name = 'text2'
        ParamType = ptInput
        Value = 41673d
      end
      item
        DataType = ftDate
        Name = 'text1'
        ParamType = ptInput
        Value = 3d
      end>
    object ViewSOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object ViewSOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object ViewSOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object ViewSOQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object ViewSOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object ViewSOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object ViewSOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object ViewSOQAC: TBooleanField
      FieldName = 'AC'
    end
    object ViewSOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ViewSOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ViewSOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object ViewSOQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object ViewSOQKe: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object ViewSOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object ViewSOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object ViewSOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object ViewSOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object ViewSOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object ViewSOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewSOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewSOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewSOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewSOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewSOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewSOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewSOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object ViewSOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object ViewSOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object ViewSOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object ViewSOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object ViewSOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object ViewSOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object ViewSOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object ViewSOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object ViewSOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object ViewSOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object ViewSOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object ViewSOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object ViewSOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object ViewSOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object ViewSOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object ViewSOQNamaPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object ViewSOQJabatanPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object ViewSOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object ViewSOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object ViewSOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object ViewSOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object ViewSOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object ViewSOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object ViewSOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object ViewSOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object ViewSOQKodePegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePegawai'
      LookupDataSet = UserQ
      LookupKeyFields = 'kode'
      LookupResultField = 'kodepegawai'
      KeyFields = 'CreateBy'
      Size = 10
      Lookup = True
    end
    object ViewSOQPembuatSO: TStringField
      FieldKind = fkLookup
      FieldName = 'PembuatSO'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodePegawai'
      Size = 50
      Lookup = True
    end
    object ViewSOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewSOQ
    Left = 428
    Top = 494
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from kontrak'
      ''
      '')
    Left = 697
    Top = 7
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object KontrakQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object KontrakQTelpPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'TelpPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
    object KontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 200
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 729
    Top = 16
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Visible = False
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Visible = False
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
      Visible = False
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
      Visible = False
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
      Visible = False
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Visible = False
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
      Visible = False
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
      Visible = False
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
  end
  object HargaRuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from HargaRute where KodeRute=:text')
    Left = 584
    Top = 344
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HargaRuteQKodeRute: TStringField
      FieldName = 'KodeRute'
      Required = True
      Size = 10
    end
    object HargaRuteQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 200
    end
    object HargaRuteQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\Jaya\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 496
    Top = 144
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 585
    Top = 215
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object updateQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object updateQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object updateQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object updateQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object updateQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object updateQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object updateQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object updateQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object updateQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object updateQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object updateQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object updateQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object updateQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object updateQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object updateQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object updateQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object updateQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object updateQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object updateQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object updateQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object updateQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object updateQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object updateQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object updateQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object updateQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object updateQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object updateQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object updateQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object CopyQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *,cast('#39#39' as money) as SisaPembayaran from masterso'
      'where kodenota=:text')
    UpdateObject = CopyUS
    Left = 705
    Top = 105
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CopyQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object CopyQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object CopyQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object CopyQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object CopyQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object CopyQTelpPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'TelpPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'Pelanggan'
      Size = 55
      Lookup = True
    end
    object CopyQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object CopyQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object CopyQKe: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object CopyQKategoriRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KategoriRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'Rute'
      Size = 55
      Lookup = True
    end
    object CopyQStandarHargaRute: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'StandardHargaRute'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'StandarHarga'
      KeyFields = 'Rute'
      Lookup = True
    end
    object CopyQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object CopyQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object CopyQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object CopyQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object CopyQAC: TBooleanField
      FieldName = 'AC'
    end
    object CopyQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object CopyQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object CopyQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object CopyQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object CopyQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object CopyQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object CopyQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object CopyQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CopyQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CopyQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CopyQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CopyQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CopyQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object CopyQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object CopyQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object CopyQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object CopyQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object CopyQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object CopyQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 50
    end
    object CopyQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object CopyQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object CopyQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object CopyQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 50
    end
    object CopyQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object CopyQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object CopyQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object CopyQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object CopyQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object CopyQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object CopyQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object CopyQNamaPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object CopyQJabatanPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPembayaranAwal'
      Size = 50
      Lookup = True
    end
    object CopyQStandarHargaMax: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'StandarHargaMax'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'StandarHargaMax'
      KeyFields = 'Rute'
      Lookup = True
    end
    object CopyQNamaPegawaiPelunasan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawaiPelunasan'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPelunasan'
      Size = 50
      Lookup = True
    end
    object CopyQJabatanPenerimaPelunasan: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerimaPelunasan'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPelunasan'
      Size = 50
      Lookup = True
    end
    object CopyQSisaPembayaran: TCurrencyField
      FieldName = 'SisaPembayaran'
      LookupCache = True
    end
    object CopyQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object CopyQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object CopyQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object CopyQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object CopyQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object CopyQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object CopyQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPending'
      Size = 50
      Lookup = True
    end
    object CopyQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPending'
      Size = 50
      Lookup = True
    end
    object CopyQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object CopyQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object CopyQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object CopyQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object CopyQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
    object CopyQTitlePICJemput: TStringField
      FieldName = 'TitlePICJemput'
      Size = 50
    end
  end
  object CopyDs: TDataSource
    DataSet = CopyQ
    Left = 732
    Top = 118
  end
  object CopyUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganC' +
        'araPembayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPemb' +
        'ayaranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, Car' +
        'aPembayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelu' +
        'nasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglK' +
        'embaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet,' +
        ' AirSuspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemp' +
        'ut, PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status,' +
        ' StatusPembayaran, ReminderPending, PenerimaPending, Keterangan,' +
        ' CreateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelan' +
        'ggan, KeteranganRute, KeteranganHarga'#13#10'from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update masterso'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  KeteranganCaraPembayaranAwal = :KeteranganCaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  KetCaraPembayaranPelunasan = :KetCaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  TitlePICJemput = :TitlePICJemput,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KomisiPelanggan = :KomisiPelanggan,'
      '  KeteranganRute = :KeteranganRute,'
      '  KeteranganHarga = :KeteranganHarga'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into masterso'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganCaraP' +
        'embayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPembayar' +
        'anAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPem' +
        'bayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelunasa' +
        'n, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglKemba' +
        'liExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet, Air' +
        'Suspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemput, ' +
        'PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status, Sta' +
        'tusPembayaran, ReminderPending, PenerimaPending, Keterangan, Cre' +
        'ateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelanggan' +
        ', KeteranganRute, KeteranganHarga)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :Kete' +
        'ranganCaraPembayaranAwal, :NoKwitansiPembayaranAwal, :NominalKwi' +
        'tansiPembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPe' +
        'lunasan, :CaraPembayaranPelunasan, :KetCaraPembayaranPelunasan, ' +
        ':NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPeluna' +
        'san, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :Kapa' +
        'sitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :A' +
        'rmada, :Kontrak, :TitlePICJemput, :PICJemput, :JamJemput, :NoTel' +
        'pPICJemput, :AlamatJemput, :Status, :StatusPembayaran, :Reminder' +
        'Pending, :PenerimaPending, :Keterangan, :CreateDate, :CreateBy, ' +
        ':Operator, :TglEntry, :TglCetak, :KomisiPelanggan, :KeteranganRu' +
        'te, :KeteranganHarga)')
    DeleteSQL.Strings = (
      'delete from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 764
    Top = 114
  end
  object InsQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Left = 640
    Top = 408
  end
  object deleteSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'delete masterso where kodenota=:text')
    Left = 344
    Top = 624
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object CekSOdiSJ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select noSO from masterSJ where noSO=:text')
    Left = 376
    Top = 624
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
  end
  object UserQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select kode, kodepegawai from [user]')
    Left = 408
    Top = 352
  end
  object UsersQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from [User]')
    Left = 488
    Top = 344
    object UsersQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object UsersQUsername: TStringField
      FieldName = 'Username'
      Required = True
    end
    object UsersQPassword: TStringField
      FieldName = 'Password'
      Required = True
    end
    object UsersQJenis: TStringField
      FieldName = 'Jenis'
      Size = 50
    end
    object UsersQKodePegawai: TStringField
      FieldName = 'KodePegawai'
      Size = 10
    end
    object UsersQMasterBKanibal: TBooleanField
      FieldName = 'MasterBKanibal'
    end
    object UsersQInsertMasterBKanibal: TBooleanField
      FieldName = 'InsertMasterBKanibal'
    end
    object UsersQUpdateMasterBKanibal: TBooleanField
      FieldName = 'UpdateMasterBKanibal'
    end
    object UsersQDeleteMasterBKanibal: TBooleanField
      FieldName = 'DeleteMasterBKanibal'
    end
    object UsersQMasterKBarang: TBooleanField
      FieldName = 'MasterKBarang'
    end
    object UsersQInsertMasterKBarang: TBooleanField
      FieldName = 'InsertMasterKBarang'
    end
    object UsersQUpdateMasterKBarang: TBooleanField
      FieldName = 'UpdateMasterKBarang'
    end
    object UsersQDeleteMasterKBarang: TBooleanField
      FieldName = 'DeleteMasterKBarang'
    end
    object UsersQMasterAC: TBooleanField
      FieldName = 'MasterAC'
    end
    object UsersQInsertMasterAC: TBooleanField
      FieldName = 'InsertMasterAC'
    end
    object UsersQUpdateMasterAC: TBooleanField
      FieldName = 'UpdateMasterAC'
    end
    object UsersQDeleteMasterAC: TBooleanField
      FieldName = 'DeleteMasterAC'
    end
    object UsersQMasterArmada: TBooleanField
      FieldName = 'MasterArmada'
    end
    object UsersQInsertMasterArmada: TBooleanField
      FieldName = 'InsertMasterArmada'
    end
    object UsersQUpdateMasterArmada: TBooleanField
      FieldName = 'UpdateMasterArmada'
    end
    object UsersQDeleteMasterArmada: TBooleanField
      FieldName = 'DeleteMasterArmada'
    end
    object UsersQMasterJenisKendaraan: TBooleanField
      FieldName = 'MasterJenisKendaraan'
    end
    object UsersQInsertJenisKendaraan: TBooleanField
      FieldName = 'InsertJenisKendaraan'
    end
    object UsersQUpdateJenisKendaraan: TBooleanField
      FieldName = 'UpdateJenisKendaraan'
    end
    object UsersQDeleteJenisKendaraan: TBooleanField
      FieldName = 'DeleteJenisKendaraan'
    end
    object UsersQMasterEkor: TBooleanField
      FieldName = 'MasterEkor'
    end
    object UsersQInsertMasterEkor: TBooleanField
      FieldName = 'InsertMasterEkor'
    end
    object UsersQUpdateMasterEkor: TBooleanField
      FieldName = 'UpdateMasterEkor'
    end
    object UsersQDeleteMasterEkor: TBooleanField
      FieldName = 'DeleteMasterEkor'
    end
    object UsersQMasterPelanggan: TBooleanField
      FieldName = 'MasterPelanggan'
    end
    object UsersQInsertMasterPelanggan: TBooleanField
      FieldName = 'InsertMasterPelanggan'
    end
    object UsersQUpdateMasterPelanggan: TBooleanField
      FieldName = 'UpdateMasterPelanggan'
    end
    object UsersQDeleteMasterPelanggan: TBooleanField
      FieldName = 'DeleteMasterPelanggan'
    end
    object UsersQMasterRute: TBooleanField
      FieldName = 'MasterRute'
    end
    object UsersQInsertMasterRute: TBooleanField
      FieldName = 'InsertMasterRute'
    end
    object UsersQUpdateMasterRute: TBooleanField
      FieldName = 'UpdateMasterRute'
    end
    object UsersQDeleteMasterRute: TBooleanField
      FieldName = 'DeleteMasterRute'
    end
    object UsersQMasterSopir: TBooleanField
      FieldName = 'MasterSopir'
    end
    object UsersQInsertMasterSopir: TBooleanField
      FieldName = 'InsertMasterSopir'
    end
    object UsersQUpdateMasterSopir: TBooleanField
      FieldName = 'UpdateMasterSopir'
    end
    object UsersQDeleteMasterSopir: TBooleanField
      FieldName = 'DeleteMasterSopir'
    end
    object UsersQMasterJarak: TBooleanField
      FieldName = 'MasterJarak'
    end
    object UsersQInsertMasterJarak: TBooleanField
      FieldName = 'InsertMasterJarak'
    end
    object UsersQUpdateMasterJarak: TBooleanField
      FieldName = 'UpdateMasterJarak'
    end
    object UsersQDeleteMasterJarak: TBooleanField
      FieldName = 'DeleteMasterJarak'
    end
    object UsersQMasterBan: TBooleanField
      FieldName = 'MasterBan'
    end
    object UsersQInsertMasterBan: TBooleanField
      FieldName = 'InsertMasterBan'
    end
    object UsersQUpdateMasterBan: TBooleanField
      FieldName = 'UpdateMasterBan'
    end
    object UsersQDeleteMasterBan: TBooleanField
      FieldName = 'DeleteMasterBan'
    end
    object UsersQMasterStandardPerawatan: TBooleanField
      FieldName = 'MasterStandardPerawatan'
    end
    object UsersQInsertMasterStandardPerawatan: TBooleanField
      FieldName = 'InsertMasterStandardPerawatan'
    end
    object UsersQUpdateMasterStandardPerawatan: TBooleanField
      FieldName = 'UpdateMasterStandardPerawatan'
    end
    object UsersQDeleteMasterStandardPerawatan: TBooleanField
      FieldName = 'DeleteMasterStandardPerawatan'
    end
    object UsersQMasterBarang: TBooleanField
      FieldName = 'MasterBarang'
    end
    object UsersQInsertMasterBarang: TBooleanField
      FieldName = 'InsertMasterBarang'
    end
    object UsersQUpdateMasterBarang: TBooleanField
      FieldName = 'UpdateMasterBarang'
    end
    object UsersQDeleteMasterBarang: TBooleanField
      FieldName = 'DeleteMasterBarang'
    end
    object UsersQMasterJenisPerbaikan: TBooleanField
      FieldName = 'MasterJenisPerbaikan'
    end
    object UsersQInsertMasterJenisPerbaikan: TBooleanField
      FieldName = 'InsertMasterJenisPerbaikan'
    end
    object UsersQUpdateMasterJenisPerbaikan: TBooleanField
      FieldName = 'UpdateMasterJenisPerbaikan'
    end
    object UsersQDeleteMasterJenisPerbaikan: TBooleanField
      FieldName = 'DeleteMasterJenisPerbaikan'
    end
    object UsersQMasterSupplier: TBooleanField
      FieldName = 'MasterSupplier'
    end
    object UsersQInsertMasterSupplier: TBooleanField
      FieldName = 'InsertMasterSupplier'
    end
    object UsersQUpdateMasterSupplier: TBooleanField
      FieldName = 'UpdateMasterSupplier'
    end
    object UsersQDeleteMasterSupplier: TBooleanField
      FieldName = 'DeleteMasterSupplier'
    end
    object UsersQMasterUser: TBooleanField
      FieldName = 'MasterUser'
    end
    object UsersQInsertMasterUser: TBooleanField
      FieldName = 'InsertMasterUser'
    end
    object UsersQUpdateMasterUser: TBooleanField
      FieldName = 'UpdateMasterUser'
    end
    object UsersQDeleteMasterUser: TBooleanField
      FieldName = 'DeleteMasterUser'
    end
    object UsersQMasterMekanik: TBooleanField
      FieldName = 'MasterMekanik'
    end
    object UsersQInsertMasterMekanik: TBooleanField
      FieldName = 'InsertMasterMekanik'
    end
    object UsersQUpdateMasterMekanik: TBooleanField
      FieldName = 'UpdateMasterMekanik'
    end
    object UsersQDeleteMasterMekanik: TBooleanField
      FieldName = 'DeleteMasterMekanik'
    end
    object UsersQMasterPegawai: TBooleanField
      FieldName = 'MasterPegawai'
    end
    object UsersQInsertMasterPegawai: TBooleanField
      FieldName = 'InsertMasterPegawai'
    end
    object UsersQUpdateMasterPegawai: TBooleanField
      FieldName = 'UpdateMasterPegawai'
    end
    object UsersQDeleteMasterPegawai: TBooleanField
      FieldName = 'DeleteMasterPegawai'
    end
    object UsersQKontrak: TBooleanField
      FieldName = 'Kontrak'
    end
    object UsersQInsertKontrak: TBooleanField
      FieldName = 'InsertKontrak'
    end
    object UsersQUpdateKontrak: TBooleanField
      FieldName = 'UpdateKontrak'
    end
    object UsersQDeleteKontrak: TBooleanField
      FieldName = 'DeleteKontrak'
    end
    object UsersQSO: TBooleanField
      FieldName = 'SO'
    end
    object UsersQInsertSO: TBooleanField
      FieldName = 'InsertSO'
    end
    object UsersQUpdateSO: TBooleanField
      FieldName = 'UpdateSO'
    end
    object UsersQDeleteSO: TBooleanField
      FieldName = 'DeleteSO'
    end
    object UsersQSetHargaSO: TBooleanField
      FieldName = 'SetHargaSO'
    end
    object UsersQSetStatusSO: TBooleanField
      FieldName = 'SetStatusSO'
    end
    object UsersQViewHargaSO: TBooleanField
      FieldName = 'ViewHargaSO'
    end
    object UsersQKeluhanPelanggan: TBooleanField
      FieldName = 'KeluhanPelanggan'
    end
    object UsersQInsertKeluhanPelanggan: TBooleanField
      FieldName = 'InsertKeluhanPelanggan'
    end
    object UsersQUpdateKeluhanPelanggan: TBooleanField
      FieldName = 'UpdateKeluhanPelanggan'
    end
    object UsersQDeleteKeluhanPelanggan: TBooleanField
      FieldName = 'DeleteKeluhanPelanggan'
    end
    object UsersQDaftarPelanggan: TBooleanField
      FieldName = 'DaftarPelanggan'
    end
    object UsersQJadwalArmada: TBooleanField
      FieldName = 'JadwalArmada'
    end
    object UsersQPilihArmada: TBooleanField
      FieldName = 'PilihArmada'
    end
    object UsersQUpdatePilihArmada: TBooleanField
      FieldName = 'UpdatePilihArmada'
    end
    object UsersQSuratJalan: TBooleanField
      FieldName = 'SuratJalan'
    end
    object UsersQInsertSuratJalan: TBooleanField
      FieldName = 'InsertSuratJalan'
    end
    object UsersQUpdateSuratJalan: TBooleanField
      FieldName = 'UpdateSuratJalan'
    end
    object UsersQDeleteSuratJalan: TBooleanField
      FieldName = 'DeleteSuratJalan'
    end
    object UsersQRealisasiSuratJalan: TBooleanField
      FieldName = 'RealisasiSuratJalan'
    end
    object UsersQInsertRealisasiSuratJalan: TBooleanField
      FieldName = 'InsertRealisasiSuratJalan'
    end
    object UsersQUpdateRealisasiSuratJalan: TBooleanField
      FieldName = 'UpdateRealisasiSuratJalan'
    end
    object UsersQDeleteRealisasiSuratJalan: TBooleanField
      FieldName = 'DeleteRealisasiSuratJalan'
    end
    object UsersQStoring: TBooleanField
      FieldName = 'Storing'
    end
    object UsersQInsertStoring: TBooleanField
      FieldName = 'InsertStoring'
    end
    object UsersQUpdateStoring: TBooleanField
      FieldName = 'UpdateStoring'
    end
    object UsersQDeleteStoring: TBooleanField
      FieldName = 'DeleteStoring'
    end
    object UsersQDaftarBeli: TBooleanField
      FieldName = 'DaftarBeli'
    end
    object UsersQUpdateDaftarBeli: TBooleanField
      FieldName = 'UpdateDaftarBeli'
    end
    object UsersQPilihSupplierDaftarBeli: TBooleanField
      FieldName = 'PilihSupplierDaftarBeli'
    end
    object UsersQPO: TBooleanField
      FieldName = 'PO'
    end
    object UsersQInsertPO: TBooleanField
      FieldName = 'InsertPO'
    end
    object UsersQUpdatePO: TBooleanField
      FieldName = 'UpdatePO'
    end
    object UsersQDeletePO: TBooleanField
      FieldName = 'DeletePO'
    end
    object UsersQApprovalPO: TBooleanField
      FieldName = 'ApprovalPO'
    end
    object UsersQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object UsersQInsertCashNCarry: TBooleanField
      FieldName = 'InsertCashNCarry'
    end
    object UsersQUpdateCashNCarry: TBooleanField
      FieldName = 'UpdateCashNCarry'
    end
    object UsersQDeleteCashNCarry: TBooleanField
      FieldName = 'DeleteCashNCarry'
    end
    object UsersQKomplainNRetur: TBooleanField
      FieldName = 'KomplainNRetur'
    end
    object UsersQInsertKomplainNRetur: TBooleanField
      FieldName = 'InsertKomplainNRetur'
    end
    object UsersQUpdateKomplainRetur: TBooleanField
      FieldName = 'UpdateKomplainRetur'
    end
    object UsersQDeleteKomplainRetur: TBooleanField
      FieldName = 'DeleteKomplainRetur'
    end
    object UsersQDaftarSupplier: TBooleanField
      FieldName = 'DaftarSupplier'
    end
    object UsersQBonBarang: TBooleanField
      FieldName = 'BonBarang'
    end
    object UsersQInsertBonBarang: TBooleanField
      FieldName = 'InsertBonBarang'
    end
    object UsersQUpdateBonBarang: TBooleanField
      FieldName = 'UpdateBonBarang'
    end
    object UsersQDeleteBonBarang: TBooleanField
      FieldName = 'DeleteBonBarang'
    end
    object UsersQSetBeliBonBarang: TBooleanField
      FieldName = 'SetBeliBonBarang'
    end
    object UsersQSetMintaBonBarang: TBooleanField
      FieldName = 'SetMintaBonBarang'
    end
    object UsersQTambahStokBonBarang: TBooleanField
      FieldName = 'TambahStokBonBarang'
    end
    object UsersQPerbaikanBonBarang: TBooleanField
      FieldName = 'PerbaikanBonBarang'
    end
    object UsersQPerawatanBonBarang: TBooleanField
      FieldName = 'PerawatanBonBarang'
    end
    object UsersQApprovalBonBarang: TBooleanField
      FieldName = 'ApprovalBonBarang'
    end
    object UsersQProcessBonBarang: TBooleanField
      FieldName = 'ProcessBonBarang'
    end
    object UsersQApprovalMoneyBonBarang: TCurrencyField
      FieldName = 'ApprovalMoneyBonBarang'
    end
    object UsersQSetUrgentBonBarang: TBooleanField
      FieldName = 'SetUrgentBonBarang'
    end
    object UsersQBonKeluarBarang: TBooleanField
      FieldName = 'BonKeluarBarang'
    end
    object UsersQInsertBonKeluarBarang: TBooleanField
      FieldName = 'InsertBonKeluarBarang'
    end
    object UsersQUpdateBonKeluarBarang: TBooleanField
      FieldName = 'UpdateBonKeluarBarang'
    end
    object UsersQDeleteBonKeluarBarang: TBooleanField
      FieldName = 'DeleteBonKeluarBarang'
    end
    object UsersQLPB: TBooleanField
      FieldName = 'LPB'
    end
    object UsersQInsertLPB: TBooleanField
      FieldName = 'InsertLPB'
    end
    object UsersQUpdateLPB: TBooleanField
      FieldName = 'UpdateLPB'
    end
    object UsersQDeleteLPB: TBooleanField
      FieldName = 'DeleteLPB'
    end
    object UsersQPemutihan: TBooleanField
      FieldName = 'Pemutihan'
    end
    object UsersQInsertPemutihan: TBooleanField
      FieldName = 'InsertPemutihan'
    end
    object UsersQUpdatePemutihan: TBooleanField
      FieldName = 'UpdatePemutihan'
    end
    object UsersQDeletePemutihan: TBooleanField
      FieldName = 'DeletePemutihan'
    end
    object UsersQApprovalPemutihan: TBooleanField
      FieldName = 'ApprovalPemutihan'
    end
    object UsersQDaftarMinStok: TBooleanField
      FieldName = 'DaftarMinStok'
    end
    object UsersQDaftarStokBarang: TBooleanField
      FieldName = 'DaftarStokBarang'
    end
    object UsersQLaporanRiwayatStokBarang: TBooleanField
      FieldName = 'LaporanRiwayatStokBarang'
    end
    object UsersQLaporanRiwayatHargaBarang: TBooleanField
      FieldName = 'LaporanRiwayatHargaBarang'
    end
    object UsersQNotifikasiPerawatan: TBooleanField
      FieldName = 'NotifikasiPerawatan'
    end
    object UsersQInsertNotifikasiPerawatan: TBooleanField
      FieldName = 'InsertNotifikasiPerawatan'
    end
    object UsersQPermintaanPerbaikan: TBooleanField
      FieldName = 'PermintaanPerbaikan'
    end
    object UsersQInsertPermintaanPerbaikan: TBooleanField
      FieldName = 'InsertPermintaanPerbaikan'
    end
    object UsersQUpdatePermintaanPerbaikan: TBooleanField
      FieldName = 'UpdatePermintaanPerbaikan'
    end
    object UsersQDeletePermintaanPerbaikan: TBooleanField
      FieldName = 'DeletePermintaanPerbaikan'
    end
    object UsersQPerbaikan: TBooleanField
      FieldName = 'Perbaikan'
    end
    object UsersQInsertPerbaikan: TBooleanField
      FieldName = 'InsertPerbaikan'
    end
    object UsersQUpdatePerbaikan: TBooleanField
      FieldName = 'UpdatePerbaikan'
    end
    object UsersQDeletePerbaikan: TBooleanField
      FieldName = 'DeletePerbaikan'
    end
    object UsersQPerawatan: TBooleanField
      FieldName = 'Perawatan'
    end
    object UsersQInsertPerawatan: TBooleanField
      FieldName = 'InsertPerawatan'
    end
    object UsersQUpdatePerawatan: TBooleanField
      FieldName = 'UpdatePerawatan'
    end
    object UsersQDeletePerawatan: TBooleanField
      FieldName = 'DeletePerawatan'
    end
    object UsersQLepasPasangBan: TBooleanField
      FieldName = 'LepasPasangBan'
    end
    object UsersQInsertLepasPasangBan: TBooleanField
      FieldName = 'InsertLepasPasangBan'
    end
    object UsersQUpdateLepasPasangBan: TBooleanField
      FieldName = 'UpdateLepasPasangBan'
    end
    object UsersQDeleteLepasPasangBan: TBooleanField
      FieldName = 'DeleteLepasPasangBan'
    end
    object UsersQPerbaikanBan: TBooleanField
      FieldName = 'PerbaikanBan'
    end
    object UsersQInsertPerbaikanBan: TBooleanField
      FieldName = 'InsertPerbaikanBan'
    end
    object UsersQUpdatePerbaikanBan: TBooleanField
      FieldName = 'UpdatePerbaikanBan'
    end
    object UsersQDeletePerbaikanBan: TBooleanField
      FieldName = 'DeletePerbaikanBan'
    end
    object UsersQTukarKomponen: TBooleanField
      FieldName = 'TukarKomponen'
    end
    object UsersQInsertTukarKomponen: TBooleanField
      FieldName = 'InsertTukarKomponen'
    end
    object UsersQUpdateTukarKomponen: TBooleanField
      FieldName = 'UpdateTukarKomponen'
    end
    object UsersQDeleteTukarKomponen: TBooleanField
      FieldName = 'DeleteTukarKomponen'
    end
    object UsersQKasBon: TBooleanField
      FieldName = 'KasBon'
    end
    object UsersQInsertKasBon: TBooleanField
      FieldName = 'InsertKasBon'
    end
    object UsersQUpdateKasBon: TBooleanField
      FieldName = 'UpdateKasBon'
    end
    object UsersQDeleteKasBon: TBooleanField
      FieldName = 'DeleteKasBon'
    end
    object UsersQPenagihan: TBooleanField
      FieldName = 'Penagihan'
    end
    object UsersQInsertPenagihan: TBooleanField
      FieldName = 'InsertPenagihan'
    end
    object UsersQUpdatePenagihan: TBooleanField
      FieldName = 'UpdatePenagihan'
    end
    object UsersQDeletePenagihan: TBooleanField
      FieldName = 'DeletePenagihan'
    end
    object UsersQBayarPO: TBooleanField
      FieldName = 'BayarPO'
    end
    object UsersQInsertBayarPO: TBooleanField
      FieldName = 'InsertBayarPO'
    end
    object UsersQUpdateBayarPO: TBooleanField
      FieldName = 'UpdateBayarPO'
    end
    object UsersQDeleteBayarPO: TBooleanField
      FieldName = 'DeleteBayarPO'
    end
    object UsersQLaporanSalesOrder: TBooleanField
      FieldName = 'LaporanSalesOrder'
    end
    object UsersQLaporanRealisasiOrder: TBooleanField
      FieldName = 'LaporanRealisasiOrder'
    end
    object UsersQLaporanOrderTidakSesuai: TBooleanField
      FieldName = 'LaporanOrderTidakSesuai'
    end
    object UsersQLaporanKeluhanPelanggan: TBooleanField
      FieldName = 'LaporanKeluhanPelanggan'
    end
    object UsersQLaporanPendapatan: TBooleanField
      FieldName = 'LaporanPendapatan'
    end
    object UsersQLaporanPemenuhanKontrak: TBooleanField
      FieldName = 'LaporanPemenuhanKontrak'
    end
    object UsersQLaporanEfisiensiMuatan: TBooleanField
      FieldName = 'LaporanEfisiensiMuatan'
    end
    object UsersQLaporanPendapatanDanPoinSopir: TBooleanField
      FieldName = 'LaporanPendapatanDanPoinSopir'
    end
    object UsersQLaporanPenggunaanBan: TBooleanField
      FieldName = 'LaporanPenggunaanBan'
    end
    object UsersQLaporanPenggunaanBarang: TBooleanField
      FieldName = 'LaporanPenggunaanBarang'
    end
    object UsersQLaporanKegiatanMekanik: TBooleanField
      FieldName = 'LaporanKegiatanMekanik'
    end
    object UsersQLaporanKegiatanTeknik: TBooleanField
      FieldName = 'LaporanKegiatanTeknik'
    end
    object UsersQLaporanRiwayatKendaraan: TBooleanField
      FieldName = 'LaporanRiwayatKendaraan'
    end
    object UsersQLaporanPemenuhanBarang: TBooleanField
      FieldName = 'LaporanPemenuhanBarang'
    end
    object UsersQLaporanPenerimaanNPembelian: TBooleanField
      FieldName = 'LaporanPenerimaanNPembelian'
    end
    object UsersQLaporanPenambahanOli: TBooleanField
      FieldName = 'LaporanPenambahanOli'
    end
    object UsersQLaporanRetur: TBooleanField
      FieldName = 'LaporanRetur'
    end
    object UsersQLaporanPenjualanUser: TBooleanField
      FieldName = 'LaporanPenjualanUser'
    end
    object UsersQLaporanProfitPerSO: TBooleanField
      FieldName = 'LaporanProfitPerSO'
    end
    object UsersQSettingParameterNotifikasi: TBooleanField
      FieldName = 'SettingParameterNotifikasi'
    end
    object UsersQSettingWaktuNotifikasi: TBooleanField
      FieldName = 'SettingWaktuNotifikasi'
    end
    object UsersQTampilkanNotifikasi: TBooleanField
      FieldName = 'TampilkanNotifikasi'
    end
    object UsersQSettingNotifikasi: TBooleanField
      FieldName = 'SettingNotifikasi'
    end
    object UsersQFollowUpCustomer: TBooleanField
      FieldName = 'FollowUpCustomer'
    end
    object UsersQInactiveCustomer: TBooleanField
      FieldName = 'InactiveCustomer'
    end
    object UsersQPembuatanSuratJalan: TBooleanField
      FieldName = 'PembuatanSuratJalan'
    end
    object UsersQNewBonBarang: TBooleanField
      FieldName = 'NewBonBarang'
    end
    object UsersQNewDaftarBeli: TBooleanField
      FieldName = 'NewDaftarBeli'
    end
    object UsersQNewPermintaanPerbaikan: TBooleanField
      FieldName = 'NewPermintaanPerbaikan'
    end
    object UsersQNotifPilihArmada: TBooleanField
      FieldName = 'NotifPilihArmada'
    end
    object UsersQNewLPB: TBooleanField
      FieldName = 'NewLPB'
    end
    object UsersQMinimumStok: TBooleanField
      FieldName = 'MinimumStok'
    end
    object UsersQSTNKExpired: TBooleanField
      FieldName = 'STNKExpired'
    end
    object UsersQSTNKPajakExpired: TBooleanField
      FieldName = 'STNKPajakExpired'
    end
    object UsersQKirExpired: TBooleanField
      FieldName = 'KirExpired'
    end
    object UsersQSimExpired: TBooleanField
      FieldName = 'SimExpired'
    end
    object UsersQKasBonAnjem: TBooleanField
      FieldName = 'KasBonAnjem'
    end
    object UsersQInsertKasBonAnjem: TBooleanField
      FieldName = 'InsertKasBonAnjem'
    end
    object UsersQUpdateKasBonAnjem: TBooleanField
      FieldName = 'UpdateKasBonAnjem'
    end
    object UsersQDeleteKasBonAnjem: TBooleanField
      FieldName = 'DeleteKasBonAnjem'
    end
    object UsersQPembuatanKwitansi: TBooleanField
      FieldName = 'PembuatanKwitansi'
    end
    object UsersQInsertPembuatanKwitansi: TBooleanField
      FieldName = 'InsertPembuatanKwitansi'
    end
    object UsersQUpdatePembuatanKwitansi: TBooleanField
      FieldName = 'UpdatePembuatanKwitansi'
    end
    object UsersQDeletePembuatanKwitansi: TBooleanField
      FieldName = 'DeletePembuatanKwitansi'
    end
    object UsersQViewJadwal: TBooleanField
      FieldName = 'ViewJadwal'
    end
    object UsersQRealisasiAnjem: TBooleanField
      FieldName = 'RealisasiAnjem'
    end
    object UsersQInsertRealisasiAnjem: TBooleanField
      FieldName = 'InsertRealisasiAnjem'
    end
    object UsersQUpdateRealisasiAnjem: TBooleanField
      FieldName = 'UpdateRealisasiAnjem'
    end
    object UsersQDeleteRealisasiAnjem: TBooleanField
      FieldName = 'DeleteRealisasiAnjem'
    end
    object UsersQRealisasiTrayek: TBooleanField
      FieldName = 'RealisasiTrayek'
    end
    object UsersQInsertRealisasiTrayek: TBooleanField
      FieldName = 'InsertRealisasiTrayek'
    end
    object UsersQUpdateRealisasiTrayek: TBooleanField
      FieldName = 'UpdateRealisasiTrayek'
    end
    object UsersQDeleteRealisasiTrayek: TBooleanField
      FieldName = 'DeleteRealisasiTrayek'
    end
    object UsersQUpdateKm: TBooleanField
      FieldName = 'UpdateKm'
    end
    object UsersQUpdateUpdateKm: TBooleanField
      FieldName = 'UpdateUpdateKm'
    end
    object UsersQPOUmum: TBooleanField
      FieldName = 'POUmum'
    end
    object UsersQInsertPOUmum: TBooleanField
      FieldName = 'InsertPOUmum'
    end
    object UsersQUpdatePOUmum: TBooleanField
      FieldName = 'UpdatePOUmum'
    end
    object UsersQDeletePOUmum: TBooleanField
      FieldName = 'DeletePOUmum'
    end
    object UsersQDaftarMinimumStokKategori: TBooleanField
      FieldName = 'DaftarMinimumStokKategori'
    end
    object UsersQKeluarMasukBarangBekas: TBooleanField
      FieldName = 'KeluarMasukBarangBekas'
    end
    object UsersQInsertKeluarMasukBarangBekas: TBooleanField
      FieldName = 'InsertKeluarMasukBarangBekas'
    end
    object UsersQUpdateKeluarMasukBarangBekas: TBooleanField
      FieldName = 'UpdateKeluarMasukBarangBekas'
    end
    object UsersQDeleteKeluarMasukBarangBekas: TBooleanField
      FieldName = 'DeleteKeluarMasukBarangBekas'
    end
    object UsersQRebuild: TBooleanField
      FieldName = 'Rebuild'
    end
    object UsersQInsertRebuild: TBooleanField
      FieldName = 'InsertRebuild'
    end
    object UsersQUpdateRebuild: TBooleanField
      FieldName = 'UpdateRebuild'
    end
    object UsersQDeleteRebuild: TBooleanField
      FieldName = 'DeleteRebuild'
    end
    object UsersQSPK: TBooleanField
      FieldName = 'SPK'
    end
    object UsersQInsertSPK: TBooleanField
      FieldName = 'InsertSPK'
    end
    object UsersQUpdateSPK: TBooleanField
      FieldName = 'UpdateSPK'
    end
    object UsersQDeleteSPK: TBooleanField
      FieldName = 'DeleteSPK'
    end
    object UsersQAmbilBarangKanibal: TBooleanField
      FieldName = 'AmbilBarangKanibal'
    end
    object UsersQInsertAmbilBarangKanibal: TBooleanField
      FieldName = 'InsertAmbilBarangKanibal'
    end
    object UsersQUpdateAmbilBarangKanibal: TBooleanField
      FieldName = 'UpdateAmbilBarangKanibal'
    end
    object UsersQDeleteAmbilBarangKanibal: TBooleanField
      FieldName = 'DeleteAmbilBarangKanibal'
    end
    object UsersQMasterBarangBekas: TBooleanField
      FieldName = 'MasterBarangBekas'
    end
    object UsersQInsertMasterBarangBekas: TBooleanField
      FieldName = 'InsertMasterBarangBekas'
    end
    object UsersQUpdateMasterBarangBekas: TBooleanField
      FieldName = 'UpdateMasterBarangBekas'
    end
    object UsersQDeleteMasterBarangBekas: TBooleanField
      FieldName = 'DeleteMasterBarangBekas'
    end
    object UsersQMasterLayoutBan: TBooleanField
      FieldName = 'MasterLayoutBan'
    end
    object UsersQInsertMasterLayoutBan: TBooleanField
      FieldName = 'InsertMasterLayoutBan'
    end
    object UsersQUpdateMasterLayoutBan: TBooleanField
      FieldName = 'UpdateMasterLayoutBan'
    end
    object UsersQDeleteMasterLayoutBan: TBooleanField
      FieldName = 'DeleteMasterLayoutBan'
    end
    object UsersQMasterLokasiBan: TBooleanField
      FieldName = 'MasterLokasiBan'
    end
    object UsersQInsertMasterLokasiBan: TBooleanField
      FieldName = 'InsertMasterLokasiBan'
    end
    object UsersQUpdateMasterLokasiBan: TBooleanField
      FieldName = 'UpdateMasterLokasiBan'
    end
    object UsersQDeleteMasterLokasiBan: TBooleanField
      FieldName = 'DeleteMasterLokasiBan'
    end
    object UsersQInsertMasterJenisKendaraan: TBooleanField
      FieldName = 'InsertMasterJenisKendaraan'
    end
    object UsersQApprovalTukarKomponen: TBooleanField
      FieldName = 'ApprovalTukarKomponen'
    end
    object UsersQRiwayatHargaBarang: TBooleanField
      FieldName = 'RiwayatHargaBarang'
    end
    object UsersQLaporanBBM: TBooleanField
      FieldName = 'LaporanBBM'
    end
    object UsersQLaporanSJ: TBooleanField
      FieldName = 'LaporanSJ'
    end
    object UsersQSkin: TStringField
      FieldName = 'Skin'
    end
    object UsersQCetakKwitansi: TBooleanField
      FieldName = 'CetakKwitansi'
    end
  end
  object AuditQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select *'
      'from Audit'
      'where NamaTabel= :nt AND KodeTabel= :kt'
      'order by createdate desc')
    Left = 440
    Top = 624
    ParamData = <
      item
        DataType = ftString
        Name = 'nt'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'kt'
        ParamType = ptInput
      end>
    object AuditQKode: TAutoIncField
      FieldName = 'Kode'
    end
    object AuditQNamaTabel: TStringField
      FieldName = 'NamaTabel'
      Size = 50
    end
    object AuditQKodeTabel: TStringField
      FieldName = 'KodeTabel'
      Size = 10
    end
    object AuditQDataLama: TMemoField
      FieldName = 'DataLama'
      BlobType = ftMemo
    end
    object AuditQDataBaru: TMemoField
      FieldName = 'DataBaru'
      BlobType = ftMemo
    end
    object AuditQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object AuditQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
  end
  object CariPelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Pelanggan where kode=:text')
    Left = 304
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CariPelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CariPelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 100
    end
    object CariPelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object CariPelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object CariPelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object CariPelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object CariPelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object CariPelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object CariPelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object CariPelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object CariPelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object CariPelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object CariPelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object CariPelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object CariPelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object CariPelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object CariPelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CariPelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CariPelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CariPelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CariPelangganQTitle: TStringField
      FieldName = 'Title'
      Size = 50
    end
  end
  object CekKuitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select count(*) as JumKuitansi from DetailKwitansiSO'
      'where KodeSO=:text')
    Left = 104
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekKuitansiQJumKuitansi: TIntegerField
      FieldName = 'JumKuitansi'
    end
  end
  object CekJumKuitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select sum(Nominal) as JumNominal from DetailKwitansiSO'
      'where KodeSO=:text')
    Left = 144
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekJumKuitansiQJumNominal: TCurrencyField
      FieldName = 'JumNominal'
    end
  end
  object CekLunasKuitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select COUNT(k.Kode) as JumBelumLunas'
      'from Kwitansi k'
      'left join DetailKwitansiSO dkso on dkso.KodeKwitansi=k.Kode'
      'where dkso.KodeSO=:text and k.StatusKwitansi='#39'BELUM LUNAS'#39)
    Left = 184
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekLunasKuitansiQJumBelumLunas: TIntegerField
      FieldName = 'JumBelumLunas'
    end
  end
end
