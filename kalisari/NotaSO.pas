unit NotaSO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxTimeEdit, cxCheckBox, UCrpeClasses, UCrpe32,
  cxSpinEdit, cxDBEdit, cxDropDownEdit, cxGroupBox, cxMemo;

type
  TNotaSOFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    KodeQkodenota: TStringField;
    ArmadaQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    ViewSOQ: TSDQuery;
    ViewDs: TDataSource;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    KontrakQ: TSDQuery;
    PelangganQKota: TStringField;
    deleteBtn: TcxButton;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    KontrakQNamaPelanggan: TStringField;
    KontrakQAlamatPelanggan: TStringField;
    KontrakQTelpPelanggan: TStringField;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQBerangkat: TDateTimeField;
    MasterQTiba: TDateTimeField;
    MasterQHarga: TCurrencyField;
    MasterQKapasitasSeat: TIntegerField;
    MasterQAC: TBooleanField;
    MasterQToilet: TBooleanField;
    MasterQRute: TStringField;
    MasterQTglFollowUp: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQKontrak: TStringField;
    MasterQPICJemput: TMemoField;
    MasterQAlamatJemput: TMemoField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQTelpPelanggan: TStringField;
    MasterQDari: TStringField;
    MasterQKe: TStringField;
    MasterQKategoriRute: TStringField;
    MasterQStandardHargaRute: TCurrencyField;
    MasterVGridTgl: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridAlamatPelanggan: TcxDBEditorRow;
    MasterVGridTelpPelanggan: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridDari: TcxDBEditorRow;
    MasterVGridKe: TcxDBEditorRow;
    MasterVGridKategoriRute: TcxDBEditorRow;
    MasterVGridBerangkat: TcxDBEditorRow;
    MasterVGridTiba: TcxDBEditorRow;
    MasterVGridKontrak: TcxDBEditorRow;
    MasterQAirSuspension: TBooleanField;
    ViewSOQKodenota: TStringField;
    ViewSOQTgl: TDateTimeField;
    ViewSOQPelanggan: TStringField;
    ViewSOQTiba: TDateTimeField;
    ViewSOQHarga: TCurrencyField;
    ViewSOQKapasitasSeat: TIntegerField;
    ViewSOQAC: TBooleanField;
    ViewSOQToilet: TBooleanField;
    ViewSOQAirSuspension: TBooleanField;
    ViewSOQRute: TStringField;
    ViewSOQTglFollowUp: TDateTimeField;
    ViewSOQArmada: TStringField;
    ViewSOQKontrak: TStringField;
    ViewSOQPICJemput: TMemoField;
    ViewSOQAlamatJemput: TMemoField;
    ViewSOQStatus: TStringField;
    ViewSOQKeterangan: TMemoField;
    ViewSOQCreateDate: TDateTimeField;
    ViewSOQCreateBy: TStringField;
    ViewSOQOperator: TStringField;
    ViewSOQTglEntry: TDateTimeField;
    ViewSOQTglCetak: TDateTimeField;
    ViewSOQNamaPelanggan: TStringField;
    ViewSOQDari: TStringField;
    ViewSOQKe: TStringField;
    MasterQPembayaranAwal: TCurrencyField;
    MasterQTglPembayaranAwal: TDateTimeField;
    MasterQCaraPembayaranAwal: TStringField;
    MasterQNoKwitansiPembayaranAwal: TStringField;
    MasterQPenerimaPembayaranAwal: TStringField;
    MasterQPelunasan: TCurrencyField;
    MasterQCaraPembayaranPelunasan: TStringField;
    MasterQNoKwitansiPelunasan: TStringField;
    MasterQPenerimaPelunasan: TStringField;
    MasterQExtend: TBooleanField;
    MasterQTglKembaliExtend: TDateTimeField;
    MasterQBiayaExtend: TCurrencyField;
    MasterQJamJemput: TDateTimeField;
    MasterQNoTelpPICJemput: TStringField;
    MasterQStatusPembayaran: TStringField;
    MasterQReminderPending: TDateTimeField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    KontrakQIntervalPenagihan: TStringField;
    ViewSOQPembayaranAwal: TCurrencyField;
    ViewSOQTglPembayaranAwal: TDateTimeField;
    ViewSOQCaraPembayaranAwal: TStringField;
    ViewSOQNoKwitansiPembayaranAwal: TStringField;
    ViewSOQPenerimaPembayaranAwal: TStringField;
    ViewSOQPelunasan: TCurrencyField;
    ViewSOQCaraPembayaranPelunasan: TStringField;
    ViewSOQNoKwitansiPelunasan: TStringField;
    ViewSOQPenerimaPelunasan: TStringField;
    ViewSOQExtend: TBooleanField;
    ViewSOQTglKembaliExtend: TDateTimeField;
    ViewSOQBiayaExtend: TCurrencyField;
    ViewSOQJamJemput: TDateTimeField;
    ViewSOQNoTelpPICJemput: TStringField;
    ViewSOQStatusPembayaran: TStringField;
    ViewSOQReminderPending: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2Pelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2TglPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2CaraPembayaranPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2NoKwitansiPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2PenerimaPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2StatusPembayaran: TcxDBEditorRow;
    cxDBVerticalGrid2Status: TcxDBEditorRow;
    ViewSOQNamaPegawai: TStringField;
    ViewSOQJabatanPegawai: TStringField;
    MasterQNamaPegawai: TStringField;
    MasterQJabatanPegawai: TStringField;
    MasterQStandarHargaMax: TCurrencyField;
    MasterQNamaPegawaiPelunasan: TStringField;
    MasterQJabatanPenerimaPelunasan: TStringField;
    cxDBVerticalGrid2NamaPegawaiPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2JabatanPenerimaPelunasan: TcxDBEditorRow;
    MasterQSisaPembayaran: TCurrencyField;
    cxDBVerticalGrid2SisaPembayaran: TcxDBEditorRow;
    cxDBVerticalGrid2Extend: TcxDBEditorRow;
    cxDBVerticalGrid2TglKembaliExtend: TcxDBEditorRow;
    cxDBVerticalGrid2BiayaExtend: TcxDBEditorRow;
    MasterQTglPelunasan: TDateTimeField;
    ViewSOQTglPelunasan: TDateTimeField;
    cxDBVerticalGrid2ReminderPending: TcxDBEditorRow;
    MasterQPPN: TCurrencyField;
    MasterQNominalKwitansiPembayaranAwal: TCurrencyField;
    MasterQNominalKwitansiPelunasan: TCurrencyField;
    MasterQPPNExtend: TCurrencyField;
    cxDBVerticalGrid2NominalKwitansiPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid2PPNExtend: TcxDBEditorRow;
    ViewSOQPPN: TCurrencyField;
    ViewSOQNominalKwitansiPembayaranAwal: TCurrencyField;
    ViewSOQNominalKwitansiPelunasan: TCurrencyField;
    ViewSOQPPNExtend: TCurrencyField;
    cxDBVerticalGrid2Keterangan: TcxDBEditorRow;
    HargaRuteQ: TSDQuery;
    HargaRuteQKodeRute: TStringField;
    HargaRuteQNama: TStringField;
    HargaRuteQHarga: TCurrencyField;
    MasterQPenerimaPending: TStringField;
    cxDBVerticalGrid2PenerimaPending: TcxDBEditorRow;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    cxDBVerticalGrid2NamaPenerima: TcxDBEditorRow;
    cxDBVerticalGrid2JabatanPenerima: TcxDBEditorRow;
    ExitBtn: TcxButton;
    Crpe1: TCrpe;
    updateQ: TSDQuery;
    updateQKode: TStringField;
    updateQPlatNo: TStringField;
    updateQJumlahSeat: TIntegerField;
    updateQTahunPembuatan: TStringField;
    updateQNoBody: TStringField;
    updateQJenisAC: TStringField;
    updateQJenisBBM: TStringField;
    updateQKapasitasTangkiBBM: TIntegerField;
    updateQLevelArmada: TStringField;
    updateQJumlahBan: TIntegerField;
    updateQAktif: TBooleanField;
    updateQAC: TBooleanField;
    updateQToilet: TBooleanField;
    updateQAirSuspension: TBooleanField;
    updateQKmSekarang: TIntegerField;
    updateQKeterangan: TStringField;
    updateQSopir: TStringField;
    updateQJenisKendaraan: TStringField;
    updateQCreateDate: TDateTimeField;
    updateQCreateBy: TStringField;
    updateQOperator: TStringField;
    updateQTglEntry: TDateTimeField;
    updateQSTNKPajakExpired: TDateTimeField;
    updateQSTNKPerpanjangExpired: TDateTimeField;
    updateQKirMulai: TDateTimeField;
    updateQKirSelesai: TDateTimeField;
    updateQNoRangka: TStringField;
    updateQNoMesin: TStringField;
    ButtonCetak: TcxButton;
    Label1: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    ButtonCopy: TButton;
    CopyQ: TSDQuery;
    CopyQKodenota: TStringField;
    CopyQTgl: TDateTimeField;
    CopyQPelanggan: TStringField;
    CopyQNamaPelanggan: TStringField;
    CopyQAlamatPelanggan: TStringField;
    CopyQTelpPelanggan: TStringField;
    CopyQRute: TStringField;
    CopyQDari: TStringField;
    CopyQKe: TStringField;
    CopyQKategoriRute: TStringField;
    CopyQStandarHargaRute: TCurrencyField;
    CopyQBerangkat: TDateTimeField;
    CopyQTiba: TDateTimeField;
    CopyQHarga: TCurrencyField;
    CopyQKapasitasSeat: TIntegerField;
    CopyQAC: TBooleanField;
    CopyQToilet: TBooleanField;
    CopyQTglFollowUp: TDateTimeField;
    CopyQArmada: TStringField;
    CopyQKontrak: TStringField;
    CopyQPICJemput: TMemoField;
    CopyQAlamatJemput: TMemoField;
    CopyQStatus: TStringField;
    CopyQKeterangan: TMemoField;
    CopyQCreateDate: TDateTimeField;
    CopyQCreateBy: TStringField;
    CopyQOperator: TStringField;
    CopyQTglEntry: TDateTimeField;
    CopyQTglCetak: TDateTimeField;
    CopyQAirSuspension: TBooleanField;
    CopyQPembayaranAwal: TCurrencyField;
    CopyQTglPembayaranAwal: TDateTimeField;
    CopyQCaraPembayaranAwal: TStringField;
    CopyQNoKwitansiPembayaranAwal: TStringField;
    CopyQPenerimaPembayaranAwal: TStringField;
    CopyQPelunasan: TCurrencyField;
    CopyQCaraPembayaranPelunasan: TStringField;
    CopyQNoKwitansiPelunasan: TStringField;
    CopyQPenerimaPelunasan: TStringField;
    CopyQExtend: TBooleanField;
    CopyQTglKembaliExtend: TDateTimeField;
    CopyQBiayaExtend: TCurrencyField;
    CopyQJamJemput: TDateTimeField;
    CopyQNoTelpPICJemput: TStringField;
    CopyQStatusPembayaran: TStringField;
    CopyQReminderPending: TDateTimeField;
    CopyQNamaPegawai: TStringField;
    CopyQJabatanPegawai: TStringField;
    CopyQStandarHargaMax: TCurrencyField;
    CopyQNamaPegawaiPelunasan: TStringField;
    CopyQJabatanPenerimaPelunasan: TStringField;
    CopyQSisaPembayaran: TCurrencyField;
    CopyQTglPelunasan: TDateTimeField;
    CopyQPPN: TCurrencyField;
    CopyQNominalKwitansiPembayaranAwal: TCurrencyField;
    CopyQNominalKwitansiPelunasan: TCurrencyField;
    CopyQPPNExtend: TCurrencyField;
    CopyQPenerimaPending: TStringField;
    CopyQNamaPenerima: TStringField;
    CopyQJabatanPenerima: TStringField;
    CopyDs: TDataSource;
    CopyUS: TSDUpdateSQL;
    InsQ: TSDQuery;
    MasterQKeteranganCaraPembayaranAwal: TStringField;
    ViewSOQKeteranganCaraPembayaranAwal: TStringField;
    ViewSOQPenerimaPending: TStringField;
    MasterQKetCaraPembayaranPelunasan: TStringField;
    cxDBVerticalGrid2KetCaraPembayaranPelunasan: TcxDBEditorRow;
    ViewSOQKetCaraPembayaranPelunasan: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    MasterVGridDBEditorRow5: TcxDBEditorRow;
    cxDBVerticalGrid2DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid2DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid2DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid2DBEditorRow4: TcxDBEditorRow;
    Panel1: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1Column9: TcxGridDBColumn;
    deleteSOQ: TSDQuery;
    CekSOdiSJ: TSDQuery;
    KontrakQPOEksternal: TStringField;
    KontrakQPlatNo: TStringField;
    UserQ: TSDQuery;
    ViewSOQKodePegawai: TStringField;
    MasterVGridDBEditorRow6: TcxDBEditorRow;
    UsersQ: TSDQuery;
    UsersQKode: TStringField;
    UsersQUsername: TStringField;
    UsersQPassword: TStringField;
    UsersQJenis: TStringField;
    UsersQKodePegawai: TStringField;
    UsersQMasterBKanibal: TBooleanField;
    UsersQInsertMasterBKanibal: TBooleanField;
    UsersQUpdateMasterBKanibal: TBooleanField;
    UsersQDeleteMasterBKanibal: TBooleanField;
    UsersQMasterKBarang: TBooleanField;
    UsersQInsertMasterKBarang: TBooleanField;
    UsersQUpdateMasterKBarang: TBooleanField;
    UsersQDeleteMasterKBarang: TBooleanField;
    UsersQMasterAC: TBooleanField;
    UsersQInsertMasterAC: TBooleanField;
    UsersQUpdateMasterAC: TBooleanField;
    UsersQDeleteMasterAC: TBooleanField;
    UsersQMasterArmada: TBooleanField;
    UsersQInsertMasterArmada: TBooleanField;
    UsersQUpdateMasterArmada: TBooleanField;
    UsersQDeleteMasterArmada: TBooleanField;
    UsersQMasterJenisKendaraan: TBooleanField;
    UsersQInsertJenisKendaraan: TBooleanField;
    UsersQUpdateJenisKendaraan: TBooleanField;
    UsersQDeleteJenisKendaraan: TBooleanField;
    UsersQMasterEkor: TBooleanField;
    UsersQInsertMasterEkor: TBooleanField;
    UsersQUpdateMasterEkor: TBooleanField;
    UsersQDeleteMasterEkor: TBooleanField;
    UsersQMasterPelanggan: TBooleanField;
    UsersQInsertMasterPelanggan: TBooleanField;
    UsersQUpdateMasterPelanggan: TBooleanField;
    UsersQDeleteMasterPelanggan: TBooleanField;
    UsersQMasterRute: TBooleanField;
    UsersQInsertMasterRute: TBooleanField;
    UsersQUpdateMasterRute: TBooleanField;
    UsersQDeleteMasterRute: TBooleanField;
    UsersQMasterSopir: TBooleanField;
    UsersQInsertMasterSopir: TBooleanField;
    UsersQUpdateMasterSopir: TBooleanField;
    UsersQDeleteMasterSopir: TBooleanField;
    UsersQMasterJarak: TBooleanField;
    UsersQInsertMasterJarak: TBooleanField;
    UsersQUpdateMasterJarak: TBooleanField;
    UsersQDeleteMasterJarak: TBooleanField;
    UsersQMasterBan: TBooleanField;
    UsersQInsertMasterBan: TBooleanField;
    UsersQUpdateMasterBan: TBooleanField;
    UsersQDeleteMasterBan: TBooleanField;
    UsersQMasterStandardPerawatan: TBooleanField;
    UsersQInsertMasterStandardPerawatan: TBooleanField;
    UsersQUpdateMasterStandardPerawatan: TBooleanField;
    UsersQDeleteMasterStandardPerawatan: TBooleanField;
    UsersQMasterBarang: TBooleanField;
    UsersQInsertMasterBarang: TBooleanField;
    UsersQUpdateMasterBarang: TBooleanField;
    UsersQDeleteMasterBarang: TBooleanField;
    UsersQMasterJenisPerbaikan: TBooleanField;
    UsersQInsertMasterJenisPerbaikan: TBooleanField;
    UsersQUpdateMasterJenisPerbaikan: TBooleanField;
    UsersQDeleteMasterJenisPerbaikan: TBooleanField;
    UsersQMasterSupplier: TBooleanField;
    UsersQInsertMasterSupplier: TBooleanField;
    UsersQUpdateMasterSupplier: TBooleanField;
    UsersQDeleteMasterSupplier: TBooleanField;
    UsersQMasterUser: TBooleanField;
    UsersQInsertMasterUser: TBooleanField;
    UsersQUpdateMasterUser: TBooleanField;
    UsersQDeleteMasterUser: TBooleanField;
    UsersQMasterMekanik: TBooleanField;
    UsersQInsertMasterMekanik: TBooleanField;
    UsersQUpdateMasterMekanik: TBooleanField;
    UsersQDeleteMasterMekanik: TBooleanField;
    UsersQMasterPegawai: TBooleanField;
    UsersQInsertMasterPegawai: TBooleanField;
    UsersQUpdateMasterPegawai: TBooleanField;
    UsersQDeleteMasterPegawai: TBooleanField;
    UsersQKontrak: TBooleanField;
    UsersQInsertKontrak: TBooleanField;
    UsersQUpdateKontrak: TBooleanField;
    UsersQDeleteKontrak: TBooleanField;
    UsersQSO: TBooleanField;
    UsersQInsertSO: TBooleanField;
    UsersQUpdateSO: TBooleanField;
    UsersQDeleteSO: TBooleanField;
    UsersQSetHargaSO: TBooleanField;
    UsersQSetStatusSO: TBooleanField;
    UsersQViewHargaSO: TBooleanField;
    UsersQKeluhanPelanggan: TBooleanField;
    UsersQInsertKeluhanPelanggan: TBooleanField;
    UsersQUpdateKeluhanPelanggan: TBooleanField;
    UsersQDeleteKeluhanPelanggan: TBooleanField;
    UsersQDaftarPelanggan: TBooleanField;
    UsersQJadwalArmada: TBooleanField;
    UsersQPilihArmada: TBooleanField;
    UsersQUpdatePilihArmada: TBooleanField;
    UsersQSuratJalan: TBooleanField;
    UsersQInsertSuratJalan: TBooleanField;
    UsersQUpdateSuratJalan: TBooleanField;
    UsersQDeleteSuratJalan: TBooleanField;
    UsersQRealisasiSuratJalan: TBooleanField;
    UsersQInsertRealisasiSuratJalan: TBooleanField;
    UsersQUpdateRealisasiSuratJalan: TBooleanField;
    UsersQDeleteRealisasiSuratJalan: TBooleanField;
    UsersQStoring: TBooleanField;
    UsersQInsertStoring: TBooleanField;
    UsersQUpdateStoring: TBooleanField;
    UsersQDeleteStoring: TBooleanField;
    UsersQDaftarBeli: TBooleanField;
    UsersQUpdateDaftarBeli: TBooleanField;
    UsersQPilihSupplierDaftarBeli: TBooleanField;
    UsersQPO: TBooleanField;
    UsersQInsertPO: TBooleanField;
    UsersQUpdatePO: TBooleanField;
    UsersQDeletePO: TBooleanField;
    UsersQApprovalPO: TBooleanField;
    UsersQCashNCarry: TBooleanField;
    UsersQInsertCashNCarry: TBooleanField;
    UsersQUpdateCashNCarry: TBooleanField;
    UsersQDeleteCashNCarry: TBooleanField;
    UsersQKomplainNRetur: TBooleanField;
    UsersQInsertKomplainNRetur: TBooleanField;
    UsersQUpdateKomplainRetur: TBooleanField;
    UsersQDeleteKomplainRetur: TBooleanField;
    UsersQDaftarSupplier: TBooleanField;
    UsersQBonBarang: TBooleanField;
    UsersQInsertBonBarang: TBooleanField;
    UsersQUpdateBonBarang: TBooleanField;
    UsersQDeleteBonBarang: TBooleanField;
    UsersQSetBeliBonBarang: TBooleanField;
    UsersQSetMintaBonBarang: TBooleanField;
    UsersQTambahStokBonBarang: TBooleanField;
    UsersQPerbaikanBonBarang: TBooleanField;
    UsersQPerawatanBonBarang: TBooleanField;
    UsersQApprovalBonBarang: TBooleanField;
    UsersQProcessBonBarang: TBooleanField;
    UsersQApprovalMoneyBonBarang: TCurrencyField;
    UsersQSetUrgentBonBarang: TBooleanField;
    UsersQBonKeluarBarang: TBooleanField;
    UsersQInsertBonKeluarBarang: TBooleanField;
    UsersQUpdateBonKeluarBarang: TBooleanField;
    UsersQDeleteBonKeluarBarang: TBooleanField;
    UsersQLPB: TBooleanField;
    UsersQInsertLPB: TBooleanField;
    UsersQUpdateLPB: TBooleanField;
    UsersQDeleteLPB: TBooleanField;
    UsersQPemutihan: TBooleanField;
    UsersQInsertPemutihan: TBooleanField;
    UsersQUpdatePemutihan: TBooleanField;
    UsersQDeletePemutihan: TBooleanField;
    UsersQApprovalPemutihan: TBooleanField;
    UsersQDaftarMinStok: TBooleanField;
    UsersQDaftarStokBarang: TBooleanField;
    UsersQLaporanRiwayatStokBarang: TBooleanField;
    UsersQLaporanRiwayatHargaBarang: TBooleanField;
    UsersQNotifikasiPerawatan: TBooleanField;
    UsersQInsertNotifikasiPerawatan: TBooleanField;
    UsersQPermintaanPerbaikan: TBooleanField;
    UsersQInsertPermintaanPerbaikan: TBooleanField;
    UsersQUpdatePermintaanPerbaikan: TBooleanField;
    UsersQDeletePermintaanPerbaikan: TBooleanField;
    UsersQPerbaikan: TBooleanField;
    UsersQInsertPerbaikan: TBooleanField;
    UsersQUpdatePerbaikan: TBooleanField;
    UsersQDeletePerbaikan: TBooleanField;
    UsersQPerawatan: TBooleanField;
    UsersQInsertPerawatan: TBooleanField;
    UsersQUpdatePerawatan: TBooleanField;
    UsersQDeletePerawatan: TBooleanField;
    UsersQLepasPasangBan: TBooleanField;
    UsersQInsertLepasPasangBan: TBooleanField;
    UsersQUpdateLepasPasangBan: TBooleanField;
    UsersQDeleteLepasPasangBan: TBooleanField;
    UsersQPerbaikanBan: TBooleanField;
    UsersQInsertPerbaikanBan: TBooleanField;
    UsersQUpdatePerbaikanBan: TBooleanField;
    UsersQDeletePerbaikanBan: TBooleanField;
    UsersQTukarKomponen: TBooleanField;
    UsersQInsertTukarKomponen: TBooleanField;
    UsersQUpdateTukarKomponen: TBooleanField;
    UsersQDeleteTukarKomponen: TBooleanField;
    UsersQKasBon: TBooleanField;
    UsersQInsertKasBon: TBooleanField;
    UsersQUpdateKasBon: TBooleanField;
    UsersQDeleteKasBon: TBooleanField;
    UsersQPenagihan: TBooleanField;
    UsersQInsertPenagihan: TBooleanField;
    UsersQUpdatePenagihan: TBooleanField;
    UsersQDeletePenagihan: TBooleanField;
    UsersQBayarPO: TBooleanField;
    UsersQInsertBayarPO: TBooleanField;
    UsersQUpdateBayarPO: TBooleanField;
    UsersQDeleteBayarPO: TBooleanField;
    UsersQLaporanSalesOrder: TBooleanField;
    UsersQLaporanRealisasiOrder: TBooleanField;
    UsersQLaporanOrderTidakSesuai: TBooleanField;
    UsersQLaporanKeluhanPelanggan: TBooleanField;
    UsersQLaporanPendapatan: TBooleanField;
    UsersQLaporanPemenuhanKontrak: TBooleanField;
    UsersQLaporanEfisiensiMuatan: TBooleanField;
    UsersQLaporanPendapatanDanPoinSopir: TBooleanField;
    UsersQLaporanPenggunaanBan: TBooleanField;
    UsersQLaporanPenggunaanBarang: TBooleanField;
    UsersQLaporanKegiatanMekanik: TBooleanField;
    UsersQLaporanKegiatanTeknik: TBooleanField;
    UsersQLaporanRiwayatKendaraan: TBooleanField;
    UsersQLaporanPemenuhanBarang: TBooleanField;
    UsersQLaporanPenerimaanNPembelian: TBooleanField;
    UsersQLaporanPenambahanOli: TBooleanField;
    UsersQLaporanRetur: TBooleanField;
    UsersQLaporanPenjualanUser: TBooleanField;
    UsersQLaporanProfitPerSO: TBooleanField;
    UsersQSettingParameterNotifikasi: TBooleanField;
    UsersQSettingWaktuNotifikasi: TBooleanField;
    UsersQTampilkanNotifikasi: TBooleanField;
    UsersQSettingNotifikasi: TBooleanField;
    UsersQFollowUpCustomer: TBooleanField;
    UsersQInactiveCustomer: TBooleanField;
    UsersQPembuatanSuratJalan: TBooleanField;
    UsersQNewBonBarang: TBooleanField;
    UsersQNewDaftarBeli: TBooleanField;
    UsersQNewPermintaanPerbaikan: TBooleanField;
    UsersQNotifPilihArmada: TBooleanField;
    UsersQNewLPB: TBooleanField;
    UsersQMinimumStok: TBooleanField;
    UsersQSTNKExpired: TBooleanField;
    UsersQSTNKPajakExpired: TBooleanField;
    UsersQKirExpired: TBooleanField;
    UsersQSimExpired: TBooleanField;
    UsersQKasBonAnjem: TBooleanField;
    UsersQInsertKasBonAnjem: TBooleanField;
    UsersQUpdateKasBonAnjem: TBooleanField;
    UsersQDeleteKasBonAnjem: TBooleanField;
    UsersQPembuatanKwitansi: TBooleanField;
    UsersQInsertPembuatanKwitansi: TBooleanField;
    UsersQUpdatePembuatanKwitansi: TBooleanField;
    UsersQDeletePembuatanKwitansi: TBooleanField;
    UsersQViewJadwal: TBooleanField;
    UsersQRealisasiAnjem: TBooleanField;
    UsersQInsertRealisasiAnjem: TBooleanField;
    UsersQUpdateRealisasiAnjem: TBooleanField;
    UsersQDeleteRealisasiAnjem: TBooleanField;
    UsersQRealisasiTrayek: TBooleanField;
    UsersQInsertRealisasiTrayek: TBooleanField;
    UsersQUpdateRealisasiTrayek: TBooleanField;
    UsersQDeleteRealisasiTrayek: TBooleanField;
    UsersQUpdateKm: TBooleanField;
    UsersQUpdateUpdateKm: TBooleanField;
    UsersQPOUmum: TBooleanField;
    UsersQInsertPOUmum: TBooleanField;
    UsersQUpdatePOUmum: TBooleanField;
    UsersQDeletePOUmum: TBooleanField;
    UsersQDaftarMinimumStokKategori: TBooleanField;
    UsersQKeluarMasukBarangBekas: TBooleanField;
    UsersQInsertKeluarMasukBarangBekas: TBooleanField;
    UsersQUpdateKeluarMasukBarangBekas: TBooleanField;
    UsersQDeleteKeluarMasukBarangBekas: TBooleanField;
    UsersQRebuild: TBooleanField;
    UsersQInsertRebuild: TBooleanField;
    UsersQUpdateRebuild: TBooleanField;
    UsersQDeleteRebuild: TBooleanField;
    UsersQSPK: TBooleanField;
    UsersQInsertSPK: TBooleanField;
    UsersQUpdateSPK: TBooleanField;
    UsersQDeleteSPK: TBooleanField;
    UsersQAmbilBarangKanibal: TBooleanField;
    UsersQInsertAmbilBarangKanibal: TBooleanField;
    UsersQUpdateAmbilBarangKanibal: TBooleanField;
    UsersQDeleteAmbilBarangKanibal: TBooleanField;
    UsersQMasterBarangBekas: TBooleanField;
    UsersQInsertMasterBarangBekas: TBooleanField;
    UsersQUpdateMasterBarangBekas: TBooleanField;
    UsersQDeleteMasterBarangBekas: TBooleanField;
    UsersQMasterLayoutBan: TBooleanField;
    UsersQInsertMasterLayoutBan: TBooleanField;
    UsersQUpdateMasterLayoutBan: TBooleanField;
    UsersQDeleteMasterLayoutBan: TBooleanField;
    UsersQMasterLokasiBan: TBooleanField;
    UsersQInsertMasterLokasiBan: TBooleanField;
    UsersQUpdateMasterLokasiBan: TBooleanField;
    UsersQDeleteMasterLokasiBan: TBooleanField;
    UsersQInsertMasterJenisKendaraan: TBooleanField;
    UsersQApprovalTukarKomponen: TBooleanField;
    UsersQRiwayatHargaBarang: TBooleanField;
    UsersQLaporanBBM: TBooleanField;
    UsersQLaporanSJ: TBooleanField;
    UsersQSkin: TStringField;
    UsersQCetakKwitansi: TBooleanField;
    ViewSOQPembuatSO: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel1: TcxLabel;
    MasterQKomisiPelanggan: TCurrencyField;
    MasterQKeteranganRute: TMemoField;
    cxDBVerticalGrid2KeteranganRute: TcxDBEditorRow;
    lblAudit: TLabel;
    AuditQ: TSDQuery;
    AuditQKode: TAutoIncField;
    AuditQNamaTabel: TStringField;
    AuditQKodeTabel: TStringField;
    AuditQDataLama: TMemoField;
    AuditQDataBaru: TMemoField;
    AuditQCreateBy: TStringField;
    AuditQCreateDate: TDateTimeField;
    MasterQKeteranganHarga: TMemoField;
    MasterVGridKeteranganHarga: TcxDBEditorRow;
    cxGrid1DBTableView1Kodenota: TcxGridDBColumn;
    CopyQKeteranganCaraPembayaranAwal: TStringField;
    CopyQKetCaraPembayaranPelunasan: TStringField;
    CopyQKomisiPelanggan: TCurrencyField;
    CopyQKeteranganRute: TMemoField;
    CopyQKeteranganHarga: TMemoField;
    MasterQTitlePICJemput: TStringField;
    cxDBVerticalGrid2TitlePICJemput: TcxDBEditorRow;
    CariPelangganQ: TSDQuery;
    CariPelangganQKode: TStringField;
    CariPelangganQNamaPT: TStringField;
    CariPelangganQAlamat: TStringField;
    CariPelangganQKota: TStringField;
    CariPelangganQNoTelp: TStringField;
    CariPelangganQEmail: TStringField;
    CariPelangganQNoFax: TStringField;
    CariPelangganQNamaPIC1: TStringField;
    CariPelangganQTelpPIC1: TStringField;
    CariPelangganQJabatanPIC1: TStringField;
    CariPelangganQNamaPIC2: TStringField;
    CariPelangganQTelpPIC2: TStringField;
    CariPelangganQJabatanPIC2: TStringField;
    CariPelangganQNamaPIC3: TStringField;
    CariPelangganQTelpPIC3: TStringField;
    CariPelangganQJabatanPIC3: TStringField;
    CariPelangganQCreateDate: TDateTimeField;
    CariPelangganQCreateBy: TStringField;
    CariPelangganQOperator: TStringField;
    CariPelangganQTglEntry: TDateTimeField;
    CariPelangganQTitle: TStringField;
    CopyQTitlePICJemput: TStringField;
    MasterQKeteranganInternal: TMemoField;
    cxDBVerticalGrid2KeteranganInternal: TcxDBEditorRow;
    Label2: TLabel;
    CekKuitansiQ: TSDQuery;
    CekKuitansiQJumKuitansi: TIntegerField;
    CekJumKuitansiQ: TSDQuery;
    CekJumKuitansiQJumNominal: TCurrencyField;
    CekLunasKuitansiQ: TSDQuery;
    CekLunasKuitansiQJumBelumLunas: TIntegerField;
    Label3: TLabel;
    cxGrid1DBTableView1AC: TcxGridDBColumn;
    cxGrid1DBTableView1Toilet: TcxGridDBColumn;
    cxGrid1DBTableView1AirSuspension: TcxGridDBColumn;
    ViewSOQBerangkat: TDateTimeField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridStatusEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid1Enter(Sender: TObject);
    procedure cxDBVerticalGrid1Exit(Sender: TObject);
    procedure cxRadioButton2Click(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure MasterVGridKontrakEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaPembayaranAwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2PenerimaPelunasanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1HargaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PembayaranAwalEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2PelunasanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2StatusEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid2ExtendEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBVerticalGrid2BiayaExtendEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PPNEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2PPNExtendEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SisaPembayaranEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterQAfterEdit(DataSet: TDataSet);
    procedure MasterQAfterPost(DataSet: TDataSet);
    procedure cxDBVerticalGrid2Enter(Sender: TObject);
    procedure cxDBVerticalGrid2Exit(Sender: TObject);
    procedure cxDBVerticalGrid2PenerimaPendingEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ButtonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1NoKwitansiPembayaranAwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2NoKwitansiPelunasanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ButtonCopyClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure MasterVGridDBEditorRow6EditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent; kd:string);
  end;

var
  NotaSOFm: TNotaSOFm;
  paramkode :string;
  MasterOriSQL: string;
  InsertBaru:integer;
  AuditOriSQL:string;
  
implementation

uses MenuUtama, DropDown, DM, Math, MasterPelanggan, StrUtils,
  KontrakDropDown, RuteDropDown, PelangganDropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotaSOFm.Create(aOwner: TComponent; kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TNotaSOFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotaSOFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotaSOFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;

end;

procedure TNotaSOFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKodenota.Size;
  userq.open;
  viewsoq.Close;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=cxDateEdit2.Date-1;
  viewsoq.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSOQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewSOQ.Open;
end;

procedure TNotaSOFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  //cxDBVerticalGrid1.SetFocus;
  cxDBVerticalGrid2.Enabled:=true;
  cxDBVerticalGrid2.SetFocus;
  InsertBaru:=1;
  //Audit
  MenuUtamaFm.datalama[1]:='';  
end;

procedure TNotaSOFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  //cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TNotaSOFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertSO.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSO.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteSO.AsBoolean;
  cxSpinEdit1.Enabled:=menuutamafm.UserQUpdateSO.AsBoolean;
  cxDBVerticalGrid2ReminderPending.Visible:=true;
  cxDBVerticalGrid2PenerimaPending.Visible:=true;
  cxDBVerticalGrid2NamaPenerima.Visible:=true;
  cxDBVerticalGrid2JabatanPenerima.Visible:=true;
  cxSpinEdit1.Value:=1;
  insertbaru:=-1;
  cxButtonEdit1PropertiesButtonClick(sender,0);
  //Audit
  AuditOriSQL:=AuditQ.SQL.Text;
end;

procedure TNotaSOFm.KodeEditExit(Sender: TObject);
var i:integer;
var extend,ppnextend : currency;
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:='PENDING';
      DMFm.GetDateQ.Open;
      MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
      MasterQAC.AsBoolean:=FALSE;
      MasterQToilet.AsBoolean:=FALSE;
      MasterQAirSuspension.AsBoolean:=FALSE;
      MasterQExtend.AsBoolean:=FALSE;
      MasterQStatusPembayaran.AsString:='BELUM LUNAS';
      DMFm.GetDateQ.Close;
      //Audit
      lblAudit.Caption:='';
      Label2.Caption:='';
      Label3.Caption:='';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSO.AsBoolean;
      //Audit
      for i:=1 to MasterQ.FieldCount-1 do
      begin
        MenuUtamaFm.datalama[i]:=MasterQ.Fields[i].AsString;
      end;
      AuditQ.Close;
      AuditQ.ParamByName('nt').AsString:='SO';
      AuditQ.ParamByName('kt').AsString:=Kodeedit.Text;
      AuditQ.Open;
      AuditQ.First;
      lblAudit.Caption:=IntToStr(AuditQ.RecordCount)+' times edited. Last edited on '+AuditQCreateDate.AsString;

      //label status kuitansi
      CekKuitansiQ.Close;
      CekKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
      CekKuitansiQ.Open;
      if CekKuitansiQJumKuitansi.AsInteger=0 then
      begin
         Label2.Caption:='SO belum memiliki kuitansi.';
         Label3.Caption:='';
      end

      else  //kalau ada kuitansi
      begin
          if MasterQBiayaExtend.AsCurrency = NULL then
          begin
             extend:=0;
          end
          else
          begin
              extend:=MasterQBiayaExtend.AsCurrency;
          end;

          if MasterQPPNExtend.AsCurrency = NULL then
          begin
             ppnextend:=0;
          end
          else
          begin
              ppnextend:=MasterQPPNExtend.AsCurrency;
          end;
          CekJumKuitansiQ.Close;
          CekJumKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
          CekJumKuitansiQ.Open;
          if CekJumKuitansiQJumNominal.AsCurrency<(MasterQHarga.AsCurrency+extend+ppnextend) then
          begin
              Label2.Caption:='Sudah ada kuitansi,';
              Label3.Caption:='tapi belum mencapai harga total';
          end

          else
          begin
              CekLunasKuitansiQ.Close;
              CekLunasKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
              CekLunasKuitansiQ.Open;
              if CekLunasKuitansiQJumBelumLunas.AsInteger>0 then
              begin
                 Label2.Caption:='Seluruh kuitansi telah dibuat,';
                 Label3.Caption:='tapi belum lunas';
              end
              else
              begin
                 Label2.Caption:='Seluruh kuitansi telah dibuat';
                 Label3.Caption:='dan sudah lunas';
              end;
          end;
      end;

      //akhir label status kuitansi      
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateSO.AsBoolean;
    MasterVGrid.Enabled:=True;
    MasterQ.Edit;

  end;

end;

procedure TNotaSOFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;


function PartialSpell( strInput : String) : String;
var strSebutan, strAngka, strHasil : String;
    sTAngka : char;
    i,sAngka : Byte;
begin
  For i := length(strInput) DownTo 1 do
   begin
    //--ambil satu digit
    sTAngka := strInput[i];
    if not (sTAngka  in ['0'..'9']) then continue;
    sAngka := strToInt(copy(strInput, i, 1));
    //--ubah angka menjadi huruf yang sesuai, kecuali angka nol
    Case sAngka of
       1: strAngka := 'satu ';
       2: strAngka := 'dua ';
       3: strAngka := 'tiga ';
       4: strAngka := 'empat ';
       5: strAngka := 'lima ';
       6: strAngka := 'enam ';
       7: strAngka := 'tujuh ';
       8: strAngka := 'delapan ';
       9: strAngka := 'sembilan ';
       0: begin
             strAngka := '';
             strSebutan := '';
          end;
    End;
    strSebutan := '';
    //--cek kondisi khusus untuk angka 1 yang bisa berubah jad 'se'
    If (sAngka = 1) And (i <> length(strInput)) Then strAngka := 'se';
    //--tambahkan satuan yang sesuai yaitu puluh, belas, ratus
    If ((length(strInput) - i) = 1) And (sAngka <> 0) Then
     begin
      strSebutan := 'puluh ';
      If (sAngka = 1) And (strHasil <> '') Then
       begin
        strSebutan := 'belas ';
        If strHasil = 'satu ' Then strAngka := 'se'
          Else strAngka := strHasil;
        strHasil := '';
       end
     End
     else if ((length(strInput) - i) = 2) And (sAngka <> 0) Then
         strSebutan := 'ratus ';
    strHasil := strAngka + strSebutan + strHasil
  end;
  PartialSpell := strHasil
end;

function Spell(strInput : String) : String ;
var i : integer;
    j : Byte;
  strPars, strOlah, strSebutan, strHasil, strSpell : String;
begin
  if length(strInput)>15 then raise Exception.Create('Nilai maksimal 999.999.999.999.999');
  strInput:=Copy(strInput,1,15);
  strPars := strInput;
  i := length(strInput);
  repeat
    //--mengambil angka 3 digit dimulai dari belakang
    j := length(strPars) Mod 3;
    If j <> 0 Then
    begin
      strOlah := copy(strPars, 1, j);
      strPars := copy(strPars, j + 1, length(strPars) - j)
    end
    Else
    begin
      strOlah := copy(strPars, 1, 3);
      strPars := copy(strPars,4,length(strPars)-3);
    End;
    //--membilang 3 digit angka yang ada
    strSpell := PartialSpell(strOlah);
    //--menambahkan satuan yang sesuai, misalnya : ribu, juta, milyar, trilyun
    If strSpell <> '' Then
      If i > 12 Then
        strSebutan := 'trilyun '
       else if i > 9 Then
         strSebutan := 'milyar '
        else if i > 6 Then
          strSebutan := 'juta '
         else if i > 3 Then
          begin
           strSebutan := 'ribu ';
           If strSpell = 'satu ' Then strSpell := 'se'
          end;
    strHasil := strHasil + strSpell + strSebutan;
    strSpell := '';
    strSebutan := '';
    dec(i,3);
  until i<1;
  if Length(StrHasil)>0 then StrHasil[1]:=UpCase(StrHasil[1]);
  Spell := strHasil;
end;


procedure TNotaSOFm.SaveBtnClick(Sender: TObject);
var counter:integer;
    i:integer;
    tlama,tbaru:string;
begin
if MasterQBerangkat.AsDateTime>MasterQTiba.AsDateTime then
begin
   showmessage('Tanggal selesai harus lebih besar dari tanggal mulai.');
end
else
begin
  counter:=cxSpinEdit1.Value;
  i:=1;
  MasterQ.Edit;
  MasterQJamJemput.AsDateTime:=MasterQBerangkat.AsDateTime;

  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
   
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKodeNota.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkodenota.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKodenota.AsString:=KodeEdit.Text;
    end;

    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKodenota.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    //Audit
    if MenuUtamaFm.datalama[1]<>'' then
    begin
      for i:=1 to MasterQ.FieldCount-1 do
      begin
        if MasterQ.Fields[i].AsString<>MenuUtamaFm.datalama[i] then
          begin
            MenuUtamaFm.databaru[i]:=MasterQ.Fields[i].AsString;
            tbaru:=tbaru+MasterQ.Fields[i].FieldName+' : '+MenuUtamaFm.databaru[i]+#13;
            tlama:=tlama+MasterQ.Fields[i].FieldName+' : '+MenuUtamaFm.datalama[i]+#13;
          end;
      end;
      if (tbaru<>'') then
      begin
        tbaru:=tbaru+'UserID : '+MenuUtamaFm.UserQUsername.AsString+#13;
        tbaru:=tbaru+'Time : '+MasterQTglEntry.AsString+#13;
        AuditQ.SQL.Text:='insert into Audit Values ('+QuotedStr('SO')+','+QuotedStr(MasterQKodenota.AsString)+','+QuotedStr(tlama)+','+QuotedStr(tbaru)+','+QuotedStr(MenuUtamaFm.UserQKode.AsString)+','+QuotedStr(DateToStr(Date))+')';
        AuditQ.ExecSQL;
        AuditQ.SQL.Text:=AuditOriSQL;
      end;
      lblAudit.Caption:='';
    end;
    ShowMessage('Simpan Berhasil');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
     MasterQ.Close;
      KodeEdit.Text:='';
      MasterVGrid.Enabled:=false;
      MasterVGrid.Enabled:=false;
      //cxDBVerticalGrid1.Enabled:=false;
      cxDBVerticalGrid2.Enabled:=false;
  ViewSOQ.Close;
  //ViewSOQ.ExecSQL;
  ViewSOQ.Open;
end;
end;

procedure TNotaSOFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TNotaSOFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TNotaSOFm.DeleteBtnClick(Sender: TObject);
var i:integer;
    tlama,tbaru,tkode:string;
begin
  if MessageDlg('Hapus SO '+KodeEdit.Text+' - '+MasterQKodeNota.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       //Audit
       tkode:=KodeEdit.Text;
       for i:=1 to MasterQ.FieldCount-1 do
       begin
         MenuUtamaFm.databaru[i]:='Deleted';
         tbaru:=tbaru+MasterQ.Fields[i].FieldName+' : '+MenuUtamaFm.databaru[i]+#13;
         tlama:=tlama+MasterQ.Fields[i].FieldName+' : '+MenuUtamaFm.datalama[i]+#13;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       //Audit
       AuditQ.SQL.Text:='insert into Audit Values ('+QuotedStr('SO')+','+QuotedStr(tkode)+','+QuotedStr(tlama)+','+QuotedStr(tbaru)+','+QuotedStr(MenuUtamaFm.UserQKode.AsString)+','+QuotedStr(DateToStr(Date))+')';
       AuditQ.ExecSQL;
       AuditQ.SQL.Text:=AuditOriSQL;
       lblAudit.Caption:='';
      MessageDlg('SO telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('SO pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     ViewSOQ.Close;
     ViewSOQ.ExecSQL;
     ViewSOQ.Open;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TNotaSOFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if AButtonIndex=0 then
  begin
    PelangganDropDownFm:=TPelangganDropdownfm.Create(Self);
    if PelangganDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      PelangganQ.Close;
      PelangganQ.open;
      MasterQPelanggan.AsString:=PelangganDropDownFm.kode;
      CariPelangganQ.Close;
      CariPelangganQ.ParamByName('text').AsString:=MasterQPelanggan.AsString;
      CariPelangganQ.Open;
      if CariPelangganQNamaPIC2.AsString<>'' then
      begin
        MasterQPICJemput.AsString:=CariPelangganQNamaPIC1.AsString+' / '+CariPelangganQNamaPIC2.AsString;
        MasterQNoTelpPICJemput.AsString:=CariPelangganQTelpPIC1.AsString+' / '+CariPelangganQTelpPIC2.AsString;
      end
      else
      begin
        MasterQPICJemput.AsString:=CariPelangganQNamaPIC1.AsString;
        MasterQNoTelpPICJemput.AsString:=CariPelangganQTelpPIC1.AsString;
      end;
      MasterQAlamatJemput.AsString:=CariPelangganQAlamat.AsString+', '+CariPelangganQKota.AsString;
    end;
    PelangganDropDownFm.Release;
  end
  else if Abuttonindex=1 then
  begin
    MasterPelangganFm := TMasterPelangganFm.create(self);
  end;
end;

procedure TNotaSOFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=PelangganQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
if AButtonIndex=0 then
begin
    {RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='';
    RuteQ.ExecSQL;
    RuteQ.Open;  }
    RuteDropDownFm:=TRuteDropdownfm.Create(Self);
    if RuteDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteDropDownFm.kode;
      //MasterQRute.AsString:=RuteQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    RuteDropDownFm.Release;
    MasterQ.Edit;
    MasterQKeteranganRute.AsString:=MasterQDari.AsString+' - '+MasterQKe.AsString;
end

else
begin
HargaRuteQ.Close;
HargaRuteQ.ParamByName('text').AsString:=MasterQRute.AsString;
HargaRuteQ.Open;
     DropDownFm:=TDropdownfm.Create(Self,HargaRuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DropDownFm.Release;
    end;
end;

end;

procedure TNotaSOFm.MasterVGridRuteEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridArmadaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridPelanggan);
  MasterQ.Edit;
  MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.MasterVGridStatusEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  //ShowMessage(MasterVGridStatus);
end;

procedure TNotaSOFm.cxDBVerticalGrid1Enter(Sender: TObject);
begin
  MasterQ.Edit;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

//    mastervgrid.FocusRow(cxDBEditorRow15);
end;

procedure TNotaSOFm.cxDBVerticalGrid1Exit(Sender: TObject);
begin
  SaveBtn.SetFocus;
end;

procedure TNotaSOFm.cxRadioButton2Click(Sender: TObject);
begin
  if cxRadioButton2.Checked then
  begin
    MasterVGridKontrak.Visible:=true;
    MasterVGridPelanggan.Properties.EditProperties.Buttons.Items[0].Visible:=false;

  end;

  KodeEditExit(sender);
end;

procedure TNotaSOFm.cxRadioButton1Click(Sender: TObject);
begin
  If cxRadioButton1.Checked then
  begin
    MasterVGridKontrak.Visible:=false;
    MasterVGridPelanggan.Properties.EditProperties.Buttons.Items[0].Visible:=true;

  end;

  KodeEditExit(sender);
end;

procedure TNotaSOFm.MasterVGridKontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{KontrakQ.Close;
    KontrakQ.ExecSQL;
    KontrakQ.Open;  }
    KontrakDropDownFm:=TKontrakDropdownfm.Create(Self,KontrakQ);
    if KontrakDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKontrak.AsString:=KontrakQKode.AsString;
      MasterQPelanggan.AsString:=KontrakQPelanggan.AsString;
      masterqrute.AsString:=kontrakqrute.AsString;
      MasterQAC.AsBoolean:=KontrakQAC.AsBoolean;
      MasterQToilet.AsBoolean:=KontrakQToilet.AsBoolean;
      MasterQAirSuspension.AsBoolean:=KontrakQAirSuspension.AsBoolean;
      MasterQKapasitasSeat.AsInteger:=KontrakQKapasitasSeat.AsInteger;
      MasterQHarga.AsCurrency:=KontrakQHarga.AsCurrency;
            MasterQPICJemput.AsString:=MasterQNamaPelanggan.AsString;
      MasterQAlamatJemput.AsString:=MasterQAlamatPelanggan.AsString;
      MasterQNoTelpPICJemput.AsString:=MasterQTelpPelanggan.AsString;
      //MasterVGrid.SetFocus;
    end;
    KontrakDropDownFm.Release;
    MasterQ.Edit;
    MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;


end;


procedure TNotaSOFm.cxDBVerticalGrid1PenerimaPembayaranAwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{  PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open; }
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerimaPembayaranAwal.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;



procedure TNotaSOFm.cxDBVerticalGrid2PenerimaPelunasanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open;  }
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerimaPelunasan.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;



procedure TNotaSOFm.cxDBVerticalGrid1HargaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQHarga.AsCurrency:=DisplayValue;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid1PembayaranAwalEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPembayaranAwal.AsCurrency:=DisplayValue;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2PelunasanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPelunasan.AsCurrency:=DisplayValue;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2StatusEditPropertiesEditValueChanged(
  Sender: TObject);
begin
 If MasterQStatus.Value ='PENDING' then
  begin
    cxDBVerticalGrid2ReminderPending.Visible:=true;
    cxDBVerticalGrid2PenerimaPending.Visible:=true;
    cxDBVerticalGrid2NamaPenerima.Visible:=true;
    cxDBVerticalGrid2JabatanPenerima.Visible:=true;
  end
  else
  begin
    cxDBVerticalGrid2ReminderPending.Visible:=false;
        cxDBVerticalGrid2PenerimaPending.Visible:=false;
    cxDBVerticalGrid2NamaPenerima.Visible:=false;
    cxDBVerticalGrid2JabatanPenerima.Visible:=false;
  end;
end;

procedure TNotaSOFm.cxDBVerticalGrid2ExtendEditPropertiesEditValueChanged(
  Sender: TObject);
begin
 If MasterQExtend.AsBoolean=true then
  begin
    cxDBVerticalGrid2BiayaExtend.Visible:=true;
    cxDBVerticalGrid2TglKembaliExtend.Visible:=true;
    cxDBVerticalGrid2PPNExtend.Visible:=true;
    MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;


  end
  else
  begin
    cxDBVerticalGrid2BiayaExtend.Visible:=false;
    cxDBVerticalGrid2TglKembaliExtend.Visible:=false;
    cxDBVerticalGrid2PPNExtend.Visible:=false;
   MasterQTglKembaliExtend.Clear;
    MasterQBiayaExtend.AsCurrency:=0;
    MasterQPPNExtend.AsCurrency:=0;
   MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;


  end;
end;

procedure TNotaSOFm.cxDBVerticalGrid2BiayaExtendEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
   MasterQBiayaExtend.AsCurrency:=DisplayValue;
   MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;
   if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid1PPNEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPPN.AsCurrency:=DisplayValue;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2PPNExtendEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPPNExtend.AsCurrency:=DisplayValue;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

if MasterQSisaPembayaran.AsInteger>0 then
begin
   MasterQStatusPembayaran.AsString:='BELUM LUNAS';
end

else
begin
   MasterQStatusPembayaran.AsString:='LUNAS';
end;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2SisaPembayaranEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    MasterQSisaPembayaran.AsCurrency:=DisplayValue;
end;

procedure TNotaSOFm.MasterQAfterEdit(DataSet: TDataSet);
begin
//MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQPPN.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.MasterQAfterPost(DataSet: TDataSet);
begin
//MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQPPN.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2Enter(Sender: TObject);
begin
  MasterQ.Edit;
MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;

end;

procedure TNotaSOFm.cxDBVerticalGrid2Exit(Sender: TObject);
begin
SaveBtn.SetFocus;
end;

procedure TNotaSOFm.cxDBVerticalGrid2PenerimaPendingEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPenerimaPending.AsString:=PegawaiDropdownFm.Kode;
    //MasterVGrid.SetFocus;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TNotaSOFm.ButtonCetakClick(Sender: TObject);
begin
  //cetak SJ
    if MessageDlg('Cetak Sales Order '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'SalesOrder.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;
    end;
    //end Cetak SJ
end;

procedure TNotaSOFm.cxDBVerticalGrid1NoKwitansiPembayaranAwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var terbilang:string;
  begin
if AButtonIndex=0 then
begin
//cetak SJ
    if MessageDlg('Cetak Kwitansi Pembayaran Awal untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    terbilang:=Spell(currtostr(MasterQPembayaranAwal.AsCurrency))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Kuitansi.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
    //end Cetak SJ
    end

 else
 begin
      if MessageDlg('Cetak Kwitansi Pembayaran Awal untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    terbilang:=Spell(currtostr(MasterQPembayaranAwal.AsCurrency))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Kuitansi2.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
    //end Cetak SJ
 end;
end;

procedure TNotaSOFm.cxDBVerticalGrid2NoKwitansiPelunasanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var terbilang:string;
  var h,p,e,pe,jumlah:currency;
begin
h:=0;
p:=0;
e:=0;
pe:=0;
if AButtonIndex=0 then
begin
//cetak SJ
    if MessageDlg('Cetak Kwitansi Pembayaran II untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    if MasterQHarga.AsCurrency<>NULL then
    begin
       h:=MasterQHarga.AsCurrency;
    end;
    if MasterQPPN.AsCurrency<>NULL then
    begin
        p:=MasterQPPN.AsCurrency;
    end;
    if MasterQBiayaExtend.AsCurrency<>NULL then
    begin
        e:=MasterQBiayaExtend.AsCurrency;
    end;
    if MasterQPPNExtend.AsCurrency<>NULL then
    begin
        pe:=MasterQPPNExtend.AsCurrency;
    end;
    jumlah:=h+p+e+pe;
    terbilang:=Spell(currtostr(jumlah))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPelunasan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end

else
begin
    if MessageDlg('Cetak Kwitansi Pembayaran II untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
     if MasterQNominalKwitansiPembayaranAwal.AsCurrency<>NULL then
    begin
       h:=MasterQNominalKwitansiPembayaranAwal.AsCurrency;
    end
    else
    begin
      if MasterQHarga.AsCurrency<>NULL then
      begin
       h:=MasterQHarga.AsCurrency;
      end;
    end;
    if MasterQPPN.AsCurrency<>NULL then
    begin
        p:=MasterQPPN.AsCurrency;
    end;
    if MasterQBiayaExtend.AsCurrency<>NULL then
    begin
        e:=MasterQBiayaExtend.AsCurrency;
    end;
    if MasterQPPNExtend.AsCurrency<>NULL then
    begin
        pe:=MasterQPPNExtend.AsCurrency;
    end;
    jumlah:=h+p+e+pe;
    terbilang:=Spell(currtostr(jumlah))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPelunasan2.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end;
    //end Cetak SJ
end;

procedure TNotaSOFm.ButtonCopyClick(Sender: TObject);
var counter,i:integer;
var a:string;
begin
  a:=KodeEdit.Text;
  KodeQ.Close;
  KodeQ.Open;
  counter:=cxSpinEdit1.Value;
  i:=1;
  for i:=1 to counter do
  begin
    CopyQ.Close;
    CopyQ.ParamByName('text').AsString:=a;
    CopyQ.Open;
    CopyQ.Insert;
    CopyQ.Edit;
    CopyQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkodenota.AsString)+i),10,'0',True);
    if MasterQTgl.AsVariant<>NULL then
    begin
      CopyQTgl.AsDateTime:=MasterQTgl.AsDateTime;
    end;
    if MasterQPelanggan.AsString<>'' then
    begin
      CopyQPelanggan.AsString:=MasterQPelanggan.AsString;
    end;
    if MasterQBerangkat.AsString<>'' then
    begin
      CopyQBerangkat.AsString:=MasterQBerangkat.AsString;
    end;
    if MasterQTiba.AsString<>'' then
    begin
      CopyQTiba.AsString:=MasterQTiba.AsString;
    end;
    if MasterQHarga.AsCurrency<>NULL then
    begin
      CopyQHarga.AsCurrency:=MasterQHarga.AsCurrency;
    end;
    if MasterQPPN.AsCurrency<>NULL then
    begin
      CopyQPPN.AsCurrency:=MasterQPPN.AsCurrency;
    end;
    if MasterQPembayaranAwal.AsCurrency<>NULL then
    begin
      CopyQPembayaranAwal.AsCurrency:=MasterQPembayaranAwal.AsCurrency;
    end;
    if MasterQTglPembayaranAwal.AsVariant<>NULL then
    begin
      CopyQTglPembayaranAwal.AsDateTime:=MasterQTglPembayaranAwal.AsDateTime;
    end;
    if MasterQCaraPembayaranAwal.AsString<>'' then
    begin
      CopyQCaraPembayaranAwal.AsString:=MasterQCaraPembayaranAwal.AsString;
    end;
    if MasterQNoKwitansiPembayaranAwal.AsString<>'' then
    begin
      CopyQNoKwitansiPembayaranAwal.AsString:=MasterQNoKwitansiPembayaranAwal.AsString;
    end;
    if MasterQNominalKwitansiPembayaranAwal.AsCurrency<>NULL then
    begin
      CopyQNominalKwitansiPembayaranAwal.AsCurrency:=MasterQNominalKwitansiPembayaranAwal.AsCurrency;
    end;
    if MasterQPenerimaPembayaranAwal.AsString <> '' then
    begin
      CopyQPenerimaPembayaranAwal.AsString:=MasterQPenerimaPembayaranAwal.AsString;
    end;
    if MasterQPelunasan.AsCurrency<>NULL then
    begin
      CopyQPelunasan.AsCurrency:=MasterQPelunasan.AsCurrency;
    end;
    if MasterQTglPelunasan.AsVariant<>NULL then
    begin
      CopyQTglPelunasan.AsDateTime:=MasterQTglPelunasan.AsDateTime;
    end;
    if MasterQCaraPembayaranPelunasan.AsString<>'' then
    begin
      CopyQCaraPembayaranPelunasan.AsString:=MasterQCaraPembayaranPelunasan.AsString;
    end;
    if MasterQNoKwitansiPelunasan.AsString<>'' then
    begin
      CopyQNoKwitansiPelunasan.AsString:=MasterQNoKwitansiPelunasan.AsString;
    end;
    if MasterQNominalKwitansiPelunasan.AsCurrency<>NULL then
    begin
      CopyQNominalKwitansiPelunasan.AsCurrency:=MasterQNominalKwitansiPelunasan.AsCurrency;
    end;
    if MasterQPenerimaPelunasan.AsString<>'' then
    begin
      CopyQPenerimaPelunasan.AsString:=MasterQPenerimaPelunasan.AsString;
    end;
    if MasterQExtend.AsBoolean<>NULL then
    begin
      CopyQExtend.AsBoolean:=MasterQExtend.AsBoolean;
    end;
    if MasterQTglKembaliExtend.AsVariant<>NULL then
    begin
      CopyQTglKembaliExtend.AsDateTime:=MasterQTglKembaliExtend.AsDateTime;
    end;
    if MasterQBiayaExtend.AsCurrency<>NULL then
    begin
      CopyQBiayaExtend.AsCurrency:=MasterQBiayaExtend.AsCurrency;
    end;
    if MasterQPPNExtend.AsCurrency<>NULL then
    begin
      CopyQPPNExtend.AsCurrency:=MasterQPPNExtend.AsCurrency;
    end;
    if MasterQKapasitasSeat.AsInteger<>NULL then
    begin
      CopyQKapasitasSeat.AsInteger:=MasterQKapasitasSeat.AsInteger;
    end;
    if MasterQAC.AsBoolean<>NULL then
    begin
      CopyQAC.AsBoolean:=MasterQAC.AsBoolean;
    end;
    if MasterQToilet.AsBoolean<>NULL then
    begin
      CopyQToilet.AsBoolean:=MasterQToilet.AsBoolean;
    end;
    if MasterQAirSuspension.AsBoolean<>NULL then
    begin
      CopyQAirSuspension.AsBoolean:=MasterQAirSuspension.AsBoolean;
    end;
    if MasterQRute.AsString<>'' then
    begin
      CopyQRute.AsString:=MasterQRute.AsString;
    end;
    if MasterQTglFollowUp.AsVariant<>NULL then
    begin
      CopyQTglFollowUp.AsDateTime:=MasterQTglFollowUp.AsDateTime;
    end;
    if MasterQArmada.AsString<>'' then
    begin
      CopyQArmada.AsString:=MasterQArmada.AsString;
    end;
    if MasterQKontrak.AsString<>'' then
    begin
      CopyQKontrak.AsString:=MasterQKontrak.AsString;
    end;
    if MasterQPICJemput.AsString<>'' then
    begin
      CopyQPICJemput.AsString:=MasterQPICJemput.AsString;
    end;
    if MasterQJamJemput.AsVariant<>NULL then
    begin
      CopyQJamJemput.AsDateTime:=MasterQJamJemput.AsDateTime;
    end;
    if MasterQNoTelpPICJemput.AsString<>'' then
    begin
      CopyQNoTelpPICJemput.AsString:=MasterQNoTelpPICJemput.AsString;
    end;
    if MasterQAlamatJemput.AsString<>'' then
    begin
      CopyQAlamatJemput.AsString:=MasterQAlamatJemput.AsString;
    end;
    if MasterQStatus.AsString<>'' then
    begin
      CopyQStatus.AsString:=MasterQStatus.AsString;
    end;
    if MasterQStatusPembayaran.AsString<>'' then
    begin
      CopyQStatusPembayaran.AsString:=MasterQStatusPembayaran.AsString;
    end;
    if MasterQReminderPending.AsVariant<>NULL then
    begin
      CopyQReminderPending.AsDateTime:=MasterQReminderPending.AsDateTime;
    end;
    if MasterQPenerimaPending.AsString<>'' then
    begin
      CopyQPenerimaPending.AsString:=MasterQPenerimaPending.AsString;
    end;
    if MasterQKeterangan.AsString<>'' then
    begin
      CopyQKeterangan.AsString:=MasterQKeterangan.AsString;
    end;
    if MasterQCreateDate.AsVariant<>NULL then
    begin
      CopyQCreateDate.AsDateTime:=MasterQCreateDate.AsDateTime;
    end;
    if MasterQCreateBy.AsString<>'' then
    begin
      CopyQCreateBy.AsString:=MasterQCreateBy.AsString;
    end;
    if MasterQOperator.AsString<>'' then
    begin
      CopyQOperator.AsString:=MasterQOperator.AsString;
    end;
    if MasterQTglEntry.AsVariant<>NULL then
    begin
      CopyQTglEntry.AsDateTime:=MasterQTglEntry.AsDateTime;
    end;
    if MasterQTglCetak.AsVariant<>NULL then
    begin
      CopyQTglCetak.AsDateTime:=MasterQTglCetak.AsDateTime;
    end;
    if MasterQKeteranganCaraPembayaranAwal.AsVariant<>NULL then
    begin
      CopyQKeteranganCaraPembayaranAwal.AsString:=MasterQKeteranganCaraPembayaranAwal.AsString;
    end;
    if MasterQKetCaraPembayaranPelunasan.AsVariant<>NULL then
    begin
      CopyQKetCaraPembayaranPelunasan.AsString:=MasterQKetCaraPembayaranPelunasan.AsString;
    end;
    if MasterQKomisiPelanggan.AsVariant<>NULL then
    begin
      CopyQKomisiPelanggan.AsCurrency:=MasterQKomisiPelanggan.AsCurrency;
    end;
    if MasterQKeteranganRute.AsVariant<>NULL then
    begin
      CopyQKeteranganRute.AsString:=MasterQKeteranganRute.AsString;
    end;
    if MasterQKeteranganHarga.AsVariant<>NULL then
    begin
      CopyQKeteranganHarga.AsString:=MasterQKeteranganHarga.AsString;
    end;
    if MasterQTitlePICJemput.AsVariant<>NULL then
    begin
      CopyQTitlePICJemput.AsString:=MasterQTitlePICJemput.AsString;
    end;
    CopyQ.Post;
    MenuUtamaFm.Database1.StartTransaction;
    CopyQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    CopyQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
  end;
  ViewSOQ.Close;
  ViewSOQ.Open;
  KodeEdit.Text:='';
  KodeQ.Close;
  MasterQ.Close;
end;

procedure TNotaSOFm.Button1Click(Sender: TObject);
begin
ShowMessage(Spell('1000000'));
ShowMessage(Spell('1234567'));
end;

procedure TNotaSOFm.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
if cxSpinEdit1.Value<1 then
begin
   cxSpinEdit1.Value:=1;
end;
end;

procedure TNotaSOFm.MasterVGridDBEditorRow6EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQPPN.AsCurrency:=DisplayValue;
  MasterQHarga.AsCurrency:=MasterqHarga.AsCurrency+MasterQPPn.AsCurrency;
end;

procedure TNotaSOFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
if cxDateEdit2.Text<>'' then
begin
  viewsoq.Close;
  viewsoq.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSOQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewSOQ.Open;
end;
end;

procedure TNotaSOFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
if cxDateEdit1.Text<>'' then
begin
  viewsoq.Close;
  viewsoq.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSOQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewSOQ.Open;
end;
end;

procedure TNotaSOFm.cxGrid1DBTableView1DblClick(Sender: TObject);
var i:integer;
var extend,ppnextend : currency;
begin
//    InsertBaru:=0;
    KodeEdit.SetFocus;
    KodeEdit.text:=viewSOQKodenota.AsString;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+ViewSOQKodenota.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      MasterQSisaPembayaran.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency-MasterQPembayaranAwal.AsCurrency-MasterQPelunasan.AsCurrency;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSO.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateSO.AsBoolean;

    if MasterQExtend.AsBoolean=TRUE then
    begin
    cxDBVerticalGrid2BiayaExtend.Visible:=true;
    cxDBVerticalGrid2TglKembaliExtend.Visible:=true;
    cxDBVerticalGrid2PPNExtend.Visible:=true;
    end

    else
    begin
    cxDBVerticalGrid2BiayaExtend.Visible:=false;
    cxDBVerticalGrid2TglKembaliExtend.Visible:=false;
    cxDBVerticalGrid2PPNExtend.Visible:=false;
    end;

    if MasterQStatus.AsString='PENDING' then
    begin
    cxDBVerticalGrid2ReminderPending.Visible:=true;
        cxDBVerticalGrid2PenerimaPending.Visible:=true;
    cxDBVerticalGrid2NamaPenerima.Visible:=true;
    cxDBVerticalGrid2JabatanPenerima.Visible:=true;
    end

    else
    begin
    cxDBVerticalGrid2ReminderPending.Visible:=false;
        cxDBVerticalGrid2PenerimaPending.Visible:=false;
    cxDBVerticalGrid2NamaPenerima.Visible:=false;
    cxDBVerticalGrid2JabatanPenerima.Visible:=false;
    end;

    if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    MasterVGrid.Enabled:=True;
   // cxDBVerticalGrid1.Enabled:=True;
    cxDBVerticalGrid2.Enabled:=True;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);

    //Audit
    for i:=1 to MasterQ.FieldCount-1 do
    begin
      MenuUtamaFm.datalama[i]:=MasterQ.Fields[i].AsString;
    end;
    AuditQ.Close;
    AuditQ.ParamByName('nt').AsString:='SO';
    AuditQ.ParamByName('kt').AsString:=Kodeedit.Text;
    AuditQ.Open;
    AuditQ.First;
    lblAudit.Caption:=IntToStr(AuditQ.RecordCount)+' times edited. Last edited on '+AuditQCreateDate.AsString;

      //label status kuitansi
      CekKuitansiQ.Close;
      CekKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
      CekKuitansiQ.Open;
      if CekKuitansiQJumKuitansi.AsInteger=0 then
      begin
         Label2.Caption:='SO belum memiliki kuitansi.';
         Label3.Caption:='';
      end

      else  //kalau ada kuitansi
      begin
          if MasterQBiayaExtend.AsCurrency = NULL then
          begin
             extend:=0;
          end
          else
          begin
              extend:=MasterQBiayaExtend.AsCurrency;
          end;

          if MasterQPPNExtend.AsCurrency = NULL then
          begin
             ppnextend:=0;
          end
          else
          begin
              ppnextend:=MasterQPPNExtend.AsCurrency;
          end;
          CekJumKuitansiQ.Close;
          CekJumKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
          CekJumKuitansiQ.Open;
          if CekJumKuitansiQJumNominal.AsCurrency<(MasterQHarga.AsCurrency+extend+ppnextend) then
          begin
              Label2.Caption:='Sudah ada kuitansi,';
              Label3.Caption:='tapi belum mencapai harga total';
          end

          else
          begin
              CekLunasKuitansiQ.Close;
              CekLunasKuitansiQ.ParamByName('text').AsString:=MasterQKodenota.AsString;
              CekLunasKuitansiQ.Open;
              if CekLunasKuitansiQJumBelumLunas.AsInteger>0 then
              begin
                 Label2.Caption:='Seluruh kuitansi telah dibuat,';
                 Label3.Caption:='tapi belum lunas';
              end
              else
              begin
                 Label2.Caption:='Seluruh kuitansi telah dibuat';
                 Label3.Caption:='dan sudah lunas';
              end;
          end;
      end;

      //akhir label status kuitansi
end;

end.
