object NotifPerawatanFm: TNotifPerawatanFm
  Left = 80
  Top = 93
  BorderStyle = bsDialog
  Caption = 'Notifikasi Perawatan'
  ClientHeight = 590
  ClientWidth = 1038
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1038
    Height = 457
    Align = alTop
    Caption = 'pnl1'
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 1
      Top = 81
      Width = 1036
      Height = 375
      Align = alClient
      ColCount = 10
      DefaultColWidth = 70
      RowCount = 10
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNone
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
      ParentFont = False
      TabOrder = 0
      OnDblClick = StringGrid1DblClick
      OnDrawCell = StringGrid1DrawCell
      OnSelectCell = StringGrid1SelectCell
    end
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 1036
      Height = 80
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -35
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 8
        Width = 122
        Height = 16
        Caption = 'Jenis Kendaraan : '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Reference Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 424
        Top = 32
        Width = 316
        Height = 37
        Caption = 'TABEL PERAWATAN'
      end
      object cxComboBox1: TcxComboBox
        Left = 904
        Top = 16
        Properties.Items.Strings = (
          'KM'
          'Waktu'
          'KM dan Waktu')
        Properties.OnChange = cxComboBox1PropertiesChange
        TabOrder = 0
        Text = 'KM dan Waktu'
        Visible = False
        Width = 121
      end
      object ComboBox1: TComboBox
        Left = 128
        Top = 8
        Width = 209
        Height = 24
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Reference Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ParentFont = False
        TabOrder = 1
        Text = 'ComboBox1'
      end
      object Button1: TButton
        Left = 192
        Top = 40
        Width = 75
        Height = 25
        Caption = 'show'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Reference Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = Button1Click
      end
      object ComboBox2: TComboBox
        Left = 8
        Top = 32
        Width = 113
        Height = 24
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Reference Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ParentFont = False
        TabOrder = 3
        Text = 'ComboBox2'
        Visible = False
      end
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 457
    Width = 1038
    Height = 133
    Align = alClient
    Caption = 'pnl3'
    TabOrder = 1
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1036
      Height = 131
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 24
        Top = 16
        Width = 88
        Height = 24
        Caption = 'Legends:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold, fsUnderline]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 96
        Top = 51
        Width = 139
        Height = 20
        Caption = 'Butuh Perawatan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 96
        Top = 84
        Width = 88
        Height = 20
        Caption = 'Masih Baik'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lperawatan: TLabel
        Left = 197
        Top = 74
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lreadynocrew: TLabel
        Left = 482
        Top = 41
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lready: TLabel
        Left = 413
        Top = 74
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ljalan: TLabel
        Left = 677
        Top = 42
        Width = 6
        Height = 20
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 376
        Top = 51
        Width = 218
        Height = 20
        Caption = 'Tidak Ada Jenis Perawatan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Shape1: TShape
        Left = 304
        Top = 48
        Width = 65
        Height = 25
        Brush.Color = clSilver
      end
      object Shape2: TShape
        Left = 24
        Top = 80
        Width = 65
        Height = 25
        Brush.Color = clLime
      end
      object Shape3: TShape
        Left = 24
        Top = 48
        Width = 65
        Height = 25
        Brush.Color = clRed
      end
    end
  end
  object DetailJadwalQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select '#39#39' as Rute, '#39#39' as NoSJ, '#39#39' as Pelanggan, '#39#39' as NoSO , '
      
        #39#39' as Sopir, '#39#39' as Lama , '#39#39' as Tgl, '#39#39' as Mekanik, '#39#39' as Ketera' +
        'ngan,'
      
        #39#39' as Masalah, '#39#39' as Tindakan ,'#39#39' as PlatNo, '#39#39' as Bongkar , '#39#39' ' +
        'as Muat')
    UpdateObject = SDUpdateSQL1
    Left = 289
    Top = 38
    object DetailJadwalQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 255
    end
    object DetailJadwalQNoSJ: TStringField
      FieldName = 'NoSJ'
      Required = True
      Size = 255
    end
    object DetailJadwalQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 255
    end
    object DetailJadwalQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 255
    end
    object DetailJadwalQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 255
    end
    object DetailJadwalQLama: TStringField
      FieldName = 'Lama'
      Required = True
      Size = 255
    end
    object DetailJadwalQTgl: TStringField
      FieldName = 'Tgl'
      Required = True
      Size = 255
    end
    object DetailJadwalQMekanik: TStringField
      FieldName = 'Mekanik'
      Required = True
      Size = 255
    end
    object DetailJadwalQKeterangan: TStringField
      FieldName = 'Keterangan'
      Required = True
      Size = 255
    end
    object DetailJadwalQMasalah: TStringField
      FieldName = 'Masalah'
      Required = True
      Size = 255
    end
    object DetailJadwalQTindakan: TStringField
      FieldName = 'Tindakan'
      Required = True
      Size = 255
    end
    object DetailJadwalQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 255
    end
    object DetailJadwalQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 255
    end
    object DetailJadwalQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 255
    end
  end
  object DetailJadwalDs: TDataSource
    DataSet = DetailJadwalQ
    Left = 763
    Top = 50
  end
  object armadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada'
      'where jeniskendaraan=:text')
    Left = 808
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object armadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object armadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object armadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object armadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object armadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object armadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object armadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object armadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object armadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object armadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object armadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object armadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object armadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object armadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object armadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object armadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object armadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object armadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object armadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object armadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object armadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object armadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object armadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object armadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object armadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object armadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object armadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object armadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object statusQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 832
    Top = 8
  end
  object perbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 776
  end
  object perawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from standarperawatan where lower(TipePerawatan)= :text' +
        ' and JenisKendaraan= :text1')
    Left = 872
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end>
    object perawatanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object perawatanQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object perawatanQTipePerawatan: TStringField
      FieldName = 'TipePerawatan'
      Required = True
      Size = 50
    end
    object perawatanQPeriodeWaktu: TIntegerField
      FieldName = 'PeriodeWaktu'
    end
    object perawatanQPeriodeKm: TIntegerField
      FieldName = 'PeriodeKm'
    end
    object perawatanQStandardWaktu: TIntegerField
      FieldName = 'StandardWaktu'
    end
  end
  object showDetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,sj.Tgl) as tgl,p.NamaPT as pelanggan,s.Nama ' +
        'as sopir,r.Muat as muat,r.Bongkar as bongkar,r.Waktu as lama fro' +
        'm MasterSJ sj,MasterSO so ,Rute r,Sopir s, Pelanggan p where sj.' +
        'sopir=s.Kode and so.Pelanggan=p.Kode and r.Kode=so.Rute and sj.N' +
        'oSO=so.Kodenota and so.Armada=:text and convert(date,sj.Tgl)<=co' +
        'nvert(date,CURRENT_TIMESTAMP+ :col) and convert(date,(sj.Tgl+r.w' +
        'aktu))>=convert(date,CURRENT_TIMESTAMP+ :col)')
    Left = 384
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object SDUpdateSQL1: TSDUpdateSQL
    Left = 840
    Top = 48
  end
  object detailPerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select u.Nama as mekanik,l.AnalisaMasalah as masalah, l.Tindakan' +
        'Perbaikan as tindakan, l.Keterangan as keterangan, j.LamaPengerj' +
        'aan as lama, l.TglEntry as tgl from PermintaanPerbaikan p, Lapor' +
        'anPerbaikan l,jenisperbaikan j, Mekanik m,[User] u,DetailPerbaik' +
        'anJenis d, DetailPerbaikanMekanik d1 where u.Kode=m.KodeKaryawan' +
        ' and d1.KodePerbaikan=l.kode and d1.Mekanik=m.Kode and l.Kode=d.' +
        'KodePerbaikan  and j.kode=d.JenisPerbaikan and p.Kode=l.PP and p' +
        '.Armada=:text and convert(date,l.WaktuMulai)<=convert(date,CURRE' +
        'NT_TIMESTAMP+:col) and convert(date,(l.WaktuSelesai+j.lamapenger' +
        'jaan))>=convert(date,CURRENT_TIMESTAMP+:col)')
    Left = 640
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select u.Nama as mekanik, l.WaktuMulai as tgl, s.StandardWaktu a' +
        's lama, l.Keterangan as keterangan from LaporanPerawatan l, Stan' +
        'darPerawatan s,Mekanik m,[User] u,DetailPerawatanJenis d , Detai' +
        'lPerawatanMekanik d1 where u.Kode=m.KodeKaryawan and l.Kode=d1.K' +
        'odePerawatan and m.Kode=d1.Mekanik and d.KodePerawatan=l.Kode an' +
        'd d.JenisPerawatan=s.Kode and l.Armada=:text and convert(date,l.' +
        'WaktuMulai)<=convert(date,CURRENT_TIMESTAMP+:col) and convert(da' +
        'te,(l.WaktuMulai+s.StandardWaktu))>=convert(date,CURRENT_TIMESTA' +
        'MP+:col)')
    Left = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object showDetail2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select convert(date,sj.Tgl) as tgl,p.NamaPT as pelanggan,s.Nama ' +
        'as sopir,r.Muat as muat,r.Bongkar as bongkar,r.Waktu as lama fro' +
        'm MasterSJ sj,MasterSO so ,Rute r,Sopir s, Pelanggan p where sj.' +
        'sopir=s.Kode and so.Pelanggan=p.Kode and r.Kode=so.Rute and sj.N' +
        'oSO=so.Kodenota and so.Armada=:text and convert(date,sj.Tgl)<=co' +
        'nvert(date,:col ) and convert(date,(sj.Tgl+r.waktu))>=convert(da' +
        'te, :col )')
    Left = 424
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerawatanQ2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select u.Nama as mekanik, l.WaktuMulai as tgl, s.StandardWaktu a' +
        's lama, l.Keterangan as keterangan from LaporanPerawatan l, Stan' +
        'darPerawatan s,Mekanik m,[User] u,DetailPerawatanJenis d , Detai' +
        'lPerawatanMekanik d1 where u.Kode=m.KodeKaryawan and l.Kode=d1.K' +
        'odePerawatan and m.Kode=d1.Mekanik and d.KodePerawatan=l.Kode an' +
        'd d.JenisPerawatan=s.Kode and l.Armada=:text and convert(date,l.' +
        'WaktuMulai)<=convert(date,:col) and convert(date,(l.WaktuMulai+s' +
        '.StandardWaktu))>=convert(date,:col)')
    Left = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object detailPerbaikanQ2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select u.Nama as mekanik,l.AnalisaMasalah as masalah, l.Tindakan' +
        'Perbaikan as tindakan, l.Keterangan as keterangan, j.LamaPengerj' +
        'aan as lama, l.TglEntry as tgl from PermintaanPerbaikan p, Lapor' +
        'anPerbaikan l,jenisperbaikan j, Mekanik m,[User] u,DetailPerbaik' +
        'anJenis d, DetailPerbaikanMekanik d1 where u.Kode=m.KodeKaryawan' +
        ' and d1.KodePerbaikan=l.kode and d1.Mekanik=m.Kode and l.Kode=d.' +
        'KodePerbaikan  and j.kode=d.JenisPerbaikan and p.Kode=l.PP  and ' +
        'p.Armada=:text and convert(date,l.WaktuMulai)<=convert(date,:col' +
        ') and convert(date,(l.WaktuSelesai+j.lamapengerjaan))>=convert(d' +
        'ate,:col)')
    Left = 680
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'col'
        ParamType = ptInput
      end>
  end
  object jenisPerawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select kode,lower(tipeperawatan) as namaKategori'
      'from standarperawatan'
      'where jeniskendaraan=:text')
    Left = 608
    Top = 40
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object jenisPerawatanQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jenisPerawatanQnamaKategori: TStringField
      FieldName = 'namaKategori'
      Size = 50
    end
  end
  object cariJenisQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select top 1 CURRENT_TIMESTAMP as tgl_hari,CASE WHEN ISNULL(tt1.' +
        'cwakturawat,tt2.cwaktuperbaikan-1)>=ISNULL(tt2.cwaktuperbaikan,t' +
        't1.cwakturawat-1) THEN tt1.cwakturawat ELSE tt2.cwaktuperbaikan ' +
        'END as kode, * from (select q1.Kode as id_rawat,q1.KmArmadaSekar' +
        'ang as km_rawat,q1.WaktuMulai as cwakturawat ,isnull(q1.WaktuSel' +
        'esai,CURRENT_TIMESTAMP) as tgl_rawat, q3.Kode as kode_rawat, q3.' +
        'TipePerawatan as nama_rawat, q1.Armada as armada_rawat from Lapo' +
        'ranPerawatan q1,DetailPerawatanJenis q2,StandarPerawatan q3 wher' +
        'e q1.Kode=q2.KodePerawatan and q2.JenisPerawatan=q3.Kode) as tt1' +
        ' full outer join (select z1.Kode as id_perbaikan,z1.KmArmadaSeka' +
        'rang as km_perbaikan,z1.WaktuSelesai as cwaktuperbaikan,isnull(z' +
        '1.WaktuSelesai,CURRENT_TIMESTAMP) as tgl_perbaikan, z3.Kode as k' +
        'ode_perbaikan, z3.Nama as nama_perbaikan, z4.Armada as armada_pe' +
        'rbaikan from LaporanPerbaikan z1,DetailPerbaikanJenis z2,JenisPe' +
        'rbaikan z3,PermintaanPerbaikan z4 where z1.Kode=z2.KodePerbaikan' +
        ' '
      
        'and z2.JenisPerbaikan=z3.Kode and z4.Kode=z1.PP) as tt2 on tt1.n' +
        'ama_rawat = tt2.nama_perbaikan where (lower(tt1.nama_rawat)=:t1 ' +
        'or lower(tt2.nama_perbaikan)= :t1) and (tt1.armada_rawat = :t2 o' +
        'r tt2.armada_perbaikan = :t2) order by tt1.tgl_rawat desc')
    Left = 920
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't2'
        ParamType = ptInput
      end>
    object cariJenisQkode: TDateTimeField
      FieldName = 'kode'
    end
    object cariJenisQid_rawat: TStringField
      FieldName = 'id_rawat'
      Size = 10
    end
    object cariJenisQkm_rawat: TIntegerField
      FieldName = 'km_rawat'
    end
    object cariJenisQkode_rawat: TStringField
      FieldName = 'kode_rawat'
      Size = 10
    end
    object cariJenisQnama_rawat: TStringField
      FieldName = 'nama_rawat'
      Size = 50
    end
    object cariJenisQarmada_rawat: TStringField
      FieldName = 'armada_rawat'
      Size = 10
    end
    object cariJenisQid_perbaikan: TStringField
      FieldName = 'id_perbaikan'
      Size = 10
    end
    object cariJenisQkm_perbaikan: TIntegerField
      FieldName = 'km_perbaikan'
    end
    object cariJenisQkode_perbaikan: TStringField
      FieldName = 'kode_perbaikan'
      Size = 10
    end
    object cariJenisQnama_perbaikan: TStringField
      FieldName = 'nama_perbaikan'
      Size = 50
    end
    object cariJenisQarmada_perbaikan: TStringField
      FieldName = 'armada_perbaikan'
      Size = 10
    end
    object cariJenisQcwakturawat: TDateTimeField
      FieldName = 'cwakturawat'
    end
    object cariJenisQcwaktuperbaikan: TDateTimeField
      FieldName = 'cwaktuperbaikan'
    end
    object cariJenisQtgl_hari: TDateTimeField
      FieldName = 'tgl_hari'
      Required = True
    end
    object cariJenisQtgl_rawat: TDateTimeField
      FieldName = 'tgl_rawat'
    end
    object cariJenisQtgl_perbaikan: TDateTimeField
      FieldName = 'tgl_perbaikan'
    end
  end
  object JenisKendaraanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 352
    Top = 8
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
