unit NotifPerawatan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, Grids,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxDropDownEdit, cxCalendar, cxMemo, cxRadioGroup;

type
  TNotifPerawatanFm = class(TForm)
    pnl1: TPanel;
    StringGrid1: TStringGrid;
    pnl2: TPanel;
    pnl3: TPanel;
    DetailJadwalQ: TSDQuery;
    DetailJadwalQRute: TStringField;
    DetailJadwalQNoSJ: TStringField;
    DetailJadwalQPelanggan: TStringField;
    DetailJadwalQNoSO: TStringField;
    DetailJadwalDs: TDataSource;
    armadaQ: TSDQuery;
    statusQ: TSDQuery;
    perbaikanQ: TSDQuery;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    perawatanQ: TSDQuery;
    DetailJadwalQSopir: TStringField;
    DetailJadwalQLama: TStringField;
    DetailJadwalQTgl: TStringField;
    DetailJadwalQMekanik: TStringField;
    DetailJadwalQKeterangan: TStringField;
    DetailJadwalQMasalah: TStringField;
    DetailJadwalQTindakan: TStringField;
    DetailJadwalQPlatNo: TStringField;
    showDetailQ: TSDQuery;
    DetailJadwalQBongkar: TStringField;
    DetailJadwalQMuat: TStringField;
    SDUpdateSQL1: TSDUpdateSQL;
    detailPerbaikanQ: TSDQuery;
    detailPerawatanQ: TSDQuery;
    cxComboBox1: TcxComboBox;
    showDetail2Q: TSDQuery;
    detailPerawatanQ2: TSDQuery;
    detailPerbaikanQ2: TSDQuery;
    lperawatan: TLabel;
    lreadynocrew: TLabel;
    lready: TLabel;
    ljalan: TLabel;
    jenisPerawatanQ: TSDQuery;
    perawatanQKode: TStringField;
    perawatanQJenisKendaraan: TStringField;
    perawatanQTipePerawatan: TStringField;
    perawatanQPeriodeWaktu: TIntegerField;
    perawatanQPeriodeKm: TIntegerField;
    perawatanQStandardWaktu: TIntegerField;
    cariJenisQ: TSDQuery;
    cariJenisQid_rawat: TStringField;
    cariJenisQkm_rawat: TIntegerField;
    cariJenisQkode_rawat: TStringField;
    cariJenisQnama_rawat: TStringField;
    cariJenisQarmada_rawat: TStringField;
    cariJenisQid_perbaikan: TStringField;
    cariJenisQkm_perbaikan: TIntegerField;
    cariJenisQkode_perbaikan: TStringField;
    cariJenisQnama_perbaikan: TStringField;
    cariJenisQarmada_perbaikan: TStringField;
    Label4: TLabel;
    cariJenisQkode: TDateTimeField;
    cariJenisQcwakturawat: TDateTimeField;
    cariJenisQcwaktuperbaikan: TDateTimeField;
    cariJenisQtgl_hari: TDateTimeField;
    cariJenisQtgl_rawat: TDateTimeField;
    cariJenisQtgl_perbaikan: TDateTimeField;
    armadaQKode: TStringField;
    armadaQPlatNo: TStringField;
    armadaQJumlahSeat: TIntegerField;
    armadaQTahunPembuatan: TStringField;
    armadaQNoBody: TStringField;
    armadaQJenisAC: TStringField;
    armadaQJenisBBM: TStringField;
    armadaQKapasitasTangkiBBM: TIntegerField;
    armadaQLevelArmada: TStringField;
    armadaQJumlahBan: TIntegerField;
    armadaQAktif: TBooleanField;
    armadaQAC: TBooleanField;
    armadaQToilet: TBooleanField;
    armadaQAirSuspension: TBooleanField;
    armadaQKmSekarang: TIntegerField;
    armadaQKeterangan: TStringField;
    armadaQSopir: TStringField;
    armadaQJenisKendaraan: TStringField;
    armadaQCreateDate: TDateTimeField;
    armadaQCreateBy: TStringField;
    armadaQOperator: TStringField;
    armadaQTglEntry: TDateTimeField;
    armadaQSTNKPajakExpired: TDateTimeField;
    armadaQSTNKPerpanjangExpired: TDateTimeField;
    armadaQKirMulai: TDateTimeField;
    armadaQKirSelesai: TDateTimeField;
    armadaQNoRangka: TStringField;
    armadaQNoMesin: TStringField;
    jenisPerawatanQkode: TStringField;
    jenisPerawatanQnamaKategori: TStringField;
    JenisKendaraanQ: TSDQuery;
    JenisKendaraanQKode: TStringField;
    JenisKendaraanQNamaJenis: TStringField;
    JenisKendaraanQTipe: TMemoField;
    JenisKendaraanQKategori: TStringField;
    JenisKendaraanQKeterangan: TMemoField;
    ComboBox1: TComboBox;
    Button1: TButton;
    Label5: TLabel;
    Label6: TLabel;
    ComboBox2: TComboBox;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure modeNormal();
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure StringGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);

  end;

var
  NotifPerawatanFm: TNotifPerawatanFm;
  kondisi :array of array of String;
  jkondisi :array of array of String;
  kodearmada : array of String;
  kodejenisperawatan : array of String;
  cektglq : array[1..50] of String;
  gambar: Boolean;
  MasterOriSQL: string;
  DaysInYear: array[1..12] of Integer =(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  kmsekarang: array of integer;
  publicrow:integer;
implementation

uses MenuUtama, DropDown, DM, Math, DateUtils, LaporanPerawatan;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotifPerawatanFm.Create(aOwner: TComponent);

begin
    inherited Create(aOwner);
    //modeNormal();
end;

procedure TNotifPerawatanFm.modeNormal();
var counterx :integer;
    countery :integer;
    i,j :integer;
    hariini : TDateTime;
    tglmasuk : TDateTime;
    kmin,kmout : Integer;
begin
  armadaQ.Close;
  armadaQ.ParamByName('text').AsString:=combobox2.Items[ComboBox1.itemindex];
  armadaQ.Open;
  jenisPerawatanQ.Close;
  jenisPerawatanQ.ParamByName('text').AsString:=combobox2.Items[ComboBox1.itemindex];
  jenisPerawatanQ.Open;
  StringGrid1.RowCount:=  armadaQ.RecordCount+1;
  StringGrid1.ColCount:=jenisPerawatanQ.RecordCount+1;
  StringGrid1.ColCount:=StringGrid1.ColCount+1;
  StringGrid1.Cells[1,0]:='KM Sekarang';
  SetLength(kondisi,(armadaQ.RecordCount+1));
  SetLength(jkondisi,(armadaQ.RecordCount+1));
  SetLength(kodearmada,(armadaQ.RecordCount+1));
  SetLength(kodejenisperawatan,(jenisPerawatanQ.RecordCount+2));

  armadaQ.Open;
  jenisPerawatanQ.Open;
  //ShowMessage('1');
  for i:=0 to armadaQ.RecordCount do
  begin
    SetLength(kondisi[i],jenisPerawatanQ.RecordCount+2);
    SetLength(jkondisi[i],jenisPerawatanQ.RecordCount+2);
    kodearmada[i]:='';
  end;

  //ShowMessage('2');
  jenisPerawatanQ.First;
  counterx:=1;
  while not jenisPerawatanQ.Eof do
  begin
    StringGrid1.Cells[counterx+1,0] :=jenisPerawatanQ.FieldByName('namaKategori').AsString;
    kodejenisperawatan[counterx+1]:=jenisPerawatanQ.FieldByName('namaKategori').AsString;
    counterx:=counterx+1;
    jenisPerawatanQ.Next;
  end;

  //ShowMessage('3');
  counterx:=1;
  countery:=1;
  armadaQ.Open;
  armadaQ.First;
  while not armadaQ.Eof do
  begin
    StringGrid1.Cells[1,countery]:=armadaQ.fieldbyname('kmsekarang').AsString;
    kodearmada[countery]:=armadaQ.FieldByName('kode').AsString;
    StringGrid1.Cells[0,countery] := armadaQ.FieldByName('platno').AsString;
    for i:=0 to jenisPerawatanQ.RecordCount-1 do
    begin
      cariJenisQ.Close;
      cariJenisQ.ParamByName('t1').AsString:=kodejenisperawatan[i+2];
      cariJenisQ.ParamByName('t2').AsString:= armadaQKode.AsString;
      cariJenisQ.ExecSQL;
      cariJenisQ.Open;
      if cariJenisQ.RecordCount> 0 then
      begin
        //ShowMessage('Rawat Masuk');
        //StrToDateTime( cariJenisQtgl_hari.AsString);
        hariini:= cariJenisQtgl_hari.AsDateTime;
        if cariJenisQkode_rawat.AsVariant<>Null then
        begin
          tglmasuk:= StrToDateTime(cariJenisQtgl_rawat.AsString);
          kmin:=cariJenisQkm_rawat.AsInteger;
          perawatanQ.Close;
          perawatanQ.ParamByName('text').AsString:=cariJenisQnama_rawat.AsString;
          perawatanQ.ParamByName('text1').AsString:=armadaQJenisKendaraan.AsString;
          perawatanQ.ExecSQL;
          perawatanQ.Open;
          //ShowMessage(DateToStr(hariini) + ' - ' + datetostr(tglmasuk) + '-' + perawatanQPeriodeWaktu.AsString);
          if perawatanQ.RecordCount>0 then
          begin
            //ShowMessage('Masuk Rawat'+ perawatanQTipePerawatan.AsString);
            jkondisi[countery,i+2] := perawatanQKode.AsString;
            kmout:=kmin+perawatanQPeriodeKm.AsInteger;
            if cxComboBox1.ItemIndex=0 then
            begin
              if (armadaQKmSekarang.AsInteger<=kmout) then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:='jalan';
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:='kosong';
              end;
            end
            else if cxComboBox1.ItemIndex=1 then
            begin
              if (hariini<tglmasuk+perawatanQPeriodeWaktu.AsInteger) then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:='jalan';
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:='kosong';
              end;
            end
            else
            begin
              //ShowMessage(DateToStr(hariini));
              //ShowMessage(DateToStr(tglmasuk+perawatanQPeriodeWaktu.AsInteger));
              if (hariini<tglmasuk+perawatanQPeriodeWaktu.AsInteger) and (armadaQKmSekarang.AsInteger<=kmout)  then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
              end;
            end;
            perawatanQ.Close;
          end
          else
          begin
            kondisi[countery,i+2]:='no';
            //StringGrid1.Cells[i+2,countery]:='no';
          end;
        end
        else
        begin
          tglmasuk:= StrToDateTime(cariJenisQtgl_perbaikan.AsString);
          kmin:=cariJenisQkm_perbaikan.AsInteger;
          perawatanQ.Close;
          perawatanQ.ParamByName('text').AsString:=cariJenisQnama_perbaikan.AsString;
          perawatanQ.ParamByName('text1').AsString:=armadaQJenisKendaraan.AsString;
          perawatanQ.ExecSQL;
          perawatanQ.Open;
          //ShowMessage(DateToStr(hariini) + ' - ' + datetostr(tglmasuk) + '-' + perawatanQPeriodeWaktu.AsString);
          if perawatanQ.RecordCount>0 then
          begin
            jkondisi[countery,i+2] := perawatanQKode.AsString;
            kmout:=kmin+perawatanQPeriodeKm.AsInteger;
            if cxComboBox1.ItemIndex=0 then
            begin
              if (armadaQKmSekarang.AsInteger<=kmout) then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:='jalan';
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:='kosong';
              end;
            end
            else if cxComboBox1.ItemIndex=1 then
            begin
              if (hariini<(tglmasuk+perawatanQPeriodeWaktu.AsInteger)) then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:='jalan';
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:='kosong';
              end;
            end
            else
            begin
              if (hariini<tglmasuk+perawatanQPeriodeWaktu.AsInteger) and (armadaQKmSekarang.AsInteger<=kmout)  then
              begin
                kondisi[countery,i+2]:='jalan';
                StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
              end
              else
              begin
                kondisi[countery,i+2]:='';
                StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
              end;
            end;
            perawatanQ.Close;
          end
          else
          begin
            kondisi[countery,i+2]:='no';
            //StringGrid1.Cells[i+2,countery]:='no';
          end;
        end;
      end
      else
      begin
        hariini:= now;
        tglmasuk:= armadaQCreateDate.AsDateTime;
        kmin:=0;
        perawatanQ.Close;
        perawatanQ.ParamByName('text').AsString:=kodejenisperawatan[i+2];
        perawatanQ.ParamByName('text1').AsString:=armadaQJenisKendaraan.AsString;
        perawatanQ.ExecSQL;
        perawatanQ.Open;
        //ShowMessage(DateToStr(hariini) + ' - ' + datetostr(tglmasuk) + '-' + perawatanQPeriodeWaktu.AsString);
        if perawatanQ.RecordCount>0 then
        begin
          jkondisi[countery,i+2] := perawatanQKode.AsString;
          kmout:=kmin+perawatanQPeriodeKm.AsInteger;
          if cxComboBox1.ItemIndex=0 then
          begin
            if (armadaQKmSekarang.AsInteger<=kmout) then
            begin
              kondisi[countery,i+2]:='jalan';
              StringGrid1.Cells[i+2,countery]:='jalan';
            end
            else
            begin
              kondisi[countery,i+2]:='';
              StringGrid1.Cells[i+2,countery]:='kosong';
            end;
          end
          else if cxComboBox1.ItemIndex=1 then
          begin
            if (hariini<(tglmasuk+perawatanQPeriodeWaktu.AsInteger)) then
            begin
              kondisi[countery,i+2]:='jalan';
              StringGrid1.Cells[i+2,countery]:='jalan';
            end
            else
            begin
              kondisi[countery][i+2]:='';
              StringGrid1.Cells[i+2,countery]:='kosong';
            end;
          end
          else
          begin
            if (hariini<tglmasuk+perawatanQPeriodeWaktu.AsInteger) and (armadaQKmSekarang.AsInteger<=kmout)  then
            begin
              kondisi[countery,i+2]:='jalan';
              StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
            end
            else
            begin
              kondisi[countery,i+2]:='';
              StringGrid1.Cells[i+2,countery]:= IntToStr(Trunc(hariini-(tglmasuk+perawatanQPeriodeWaktu.AsInteger))) + '/' + IntToStr(armadaQKmSekarang.AsInteger-kmout);
            end;
          end;
          perawatanQ.Close;
        end
        else
        begin
          kondisi[countery,i+2]:='no';
          //StringGrid1.Cells[i+2,countery]:='no';
        end;
      end;
    end;
    countery:=countery+1;
    armadaQ.Next;
  end;
  //ShowMessage('4');
  jenisPerawatanQ.Close;
  armadaQ.close;
  StringGrid1.Repaint;
end;

procedure TNotifPerawatanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotifPerawatanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotifPerawatanFm.StringGrid1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  pbaris  :integer;
  i,j     :integer;
begin
  //Dapatkan Banyaknya Baris
  armadaQ.Close;
  armadaQ.ParamByName('text').AsString:=combobox2.Items[ComboBox1.itemindex];
	armadaQ.Open;
	pbaris:=armadaQ.RecordCount;
	armadaQ.Close;
  //ShowMessage(IntToStr(StringGrid1.ColCount));
	for i:=1 to pbaris do
	begin
		for j:=2 to StringGrid1.ColCount-1 do
		begin
			if (ACol = j) and (ARow = i) then
      begin
				if kondisi[i,j]='' then
				begin
				  with TStringGrid(Sender) do
				  begin
            Canvas.Brush.Color := clRed;
            Canvas.FillRect(Rect);
            Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
				  end;
				end
        else if kondisi[i,j]='no' then
        begin
          with TStringGrid(Sender) do
				  begin
            Canvas.Brush.Color := clGray;
            Canvas.FillRect(Rect);
            Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
				  end;
        end
        else
        begin
          with TStringGrid(Sender) do
				  begin
            Canvas.Brush.Color := clLime;
            Canvas.FillRect(Rect);
            Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[j, i]);
				  end;
        end;
      end;
		end;
	end;
  if (state = [gdSelected]) then
    with TStringGrid(Sender), Canvas do
    begin
      Brush.Color := clBlack;
      FillRect(Rect);
      TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[aCol, aRow]);
    end;

end;

procedure TNotifPerawatanFm.cxComboBox1PropertiesChange(Sender: TObject);
var i,j,countery :integer;
  mydate : TDateTime;
  temp:String;
  temp2:String;
  rperbaikan,rperawatan,rreadynocrew,rready,rjalan:integer;
  trperbaikan,trperawatan,trreadynocrew,trready,trjalan:integer;
begin
  modeNormal();
end;

procedure TNotifPerawatanFm.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  publicrow:=ARow;
end;

procedure TNotifPerawatanFm.StringGrid1DblClick(Sender: TObject);
var i,j:integer;
    lempar:String;
    count:integer;
begin
    count:=0;
    lempar:='';
    lempar:=kodearmada[publicrow];
    for i:=2 to StringGrid1.ColCount-1 do
    begin
      if kondisi[publicrow,i]='' then
      begin
        lempar:=lempar+','+jkondisi[publicrow,i];
        count:=count+1;
      end;
    end;

    if MenuUtamaFm.UserQInsertNotifikasiPerawatan.AsBoolean then
      if count>0 then if MessageDlg('Buat Laporan Perawatan, Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
        LaporanPerawatanFm:= TLaporanPerawatanFm.Create(self,lempar);
      end;
end;

procedure TNotifPerawatanFm.FormActivate(Sender: TObject);
begin
  //modeNormal()
end;

procedure TNotifPerawatanFm.Button1Click(Sender: TObject);
begin
  modeNormal();
end;

procedure TNotifPerawatanFm.FormShow(Sender: TObject);
begin
  JenisKendaraanQ.Open;
  while JenisKendaraanQ.Eof=false do begin
    ComboBox1.Items.Add(JenisKendaraanQNamaJenis.AsString + ' - ' + JenisKendaraanQTipe.AsString);
    ComboBox2.Items.Add(JenisKendaraanQKode.AsString);
    JenisKendaraanQ.Next;
  end;
  ComboBox1.ItemIndex:=0;
  ComboBox2.ItemIndex:=0;
  modeNormal;
end;

end.
