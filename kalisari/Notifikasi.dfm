object NotifikasiFm: TNotifikasiFm
  Left = 480
  Top = 116
  Width = 571
  Height = 476
  Caption = 'Notifikasi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 555
    Height = 48
    Align = alTop
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 367
    Width = 555
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      Visible = False
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'REFRESH'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 418
    Width = 555
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxListBox1: TcxListBox
    Left = 192
    Top = 96
    Width = 399
    Height = 252
    Align = alCustom
    ItemHeight = 29
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -24
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 3
    Visible = False
    OnDblClick = cxListBox1DblClick
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 48
    Width = 555
    Height = 319
    Align = alClient
    TabOrder = 4
    object cxGrid1TableView1: TcxGridTableView
      OnCellClick = cxGrid1TableView1CellClick
      OnCellDblClick = cxGrid1TableView1CellDblClick
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1TableView1Column1: TcxGridColumn
        Caption = 'Jenis'
        Width = 171
      end
      object cxGrid1TableView1Column2: TcxGridColumn
        Caption = 'Keterangan'
        Width = 348
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1TableView1
    end
  end
  object cxListBox2: TcxListBox
    Left = 56
    Top = 272
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 5
    Visible = False
  end
  object cxListBox3: TcxListBox
    Left = 32
    Top = 128
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 6
    Visible = False
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 441
    Top = 9
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object SJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, sj.kodenota as surat_jalan, p.namapt, r.muat, r.bong' +
        'kar from MasterSO m left outer join'
      
        'MasterSJ sj on m.Kodenota=sj.NoSO, Pelanggan p, Rute r where m.P' +
        'elanggan = p.Kode and m.Rute=r.Kode and m.status='#39'DEAL'#39' and getd' +
        'ate()>=m.berangkat - :text and sj.kodenota is NULL'
      ' '
      ' ')
    Left = 65
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SJQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SJQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SJQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SJQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SJQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SJQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SJQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SJQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SJQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SJQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SJQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SJQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SJQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SJQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SJQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SJQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SJQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SJQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SJQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SJQAC: TBooleanField
      FieldName = 'AC'
    end
    object SJQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SJQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SJQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SJQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SJQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SJQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SJQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SJQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SJQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SJQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SJQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SJQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SJQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SJQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SJQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SJQsurat_jalan: TStringField
      FieldName = 'surat_jalan'
      Size = 10
    end
    object SJQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
    object SJQmuat: TStringField
      FieldName = 'muat'
      Required = True
      Size = 50
    end
    object SJQbongkar: TStringField
      FieldName = 'bongkar'
      Required = True
      Size = 50
    end
    object SJQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object FollowUpSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, p.namapt from masterso m, pelanggan p where m.pelang' +
        'gan=p.kode and status='#39'PENDING'#39' and getdate()>ReminderPending')
    Left = 1
    Top = 65535
    object FollowUpSOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object FollowUpSOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object FollowUpSOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object FollowUpSOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object FollowUpSOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object FollowUpSOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object FollowUpSOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object FollowUpSOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object FollowUpSOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object FollowUpSOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object FollowUpSOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object FollowUpSOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object FollowUpSOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object FollowUpSOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object FollowUpSOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object FollowUpSOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object FollowUpSOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object FollowUpSOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object FollowUpSOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object FollowUpSOQAC: TBooleanField
      FieldName = 'AC'
    end
    object FollowUpSOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object FollowUpSOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object FollowUpSOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object FollowUpSOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object FollowUpSOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object FollowUpSOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object FollowUpSOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object FollowUpSOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object FollowUpSOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object FollowUpSOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object FollowUpSOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object FollowUpSOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object FollowUpSOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object FollowUpSOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object FollowUpSOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object FollowUpSOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object FollowUpSOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object FollowUpSOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object FollowUpSOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object FollowUpSOQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
    object FollowUpSOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object CustQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select top 1 * from masterso where pelanggan like :text order by' +
        ' tgl desc'
      '')
    Left = 505
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CustQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object CustQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object CustQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object CustQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object CustQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object CustQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object CustQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object CustQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object CustQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object CustQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object CustQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object CustQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object CustQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object CustQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object CustQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object CustQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object CustQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object CustQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object CustQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object CustQAC: TBooleanField
      FieldName = 'AC'
    end
    object CustQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object CustQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object CustQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object CustQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object CustQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object CustQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object CustQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object CustQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object CustQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object CustQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object CustQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object CustQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object CustQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object CustQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CustQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CustQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CustQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CustQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CustQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object CustQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object CustomerLamaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 32
    object CustomerLamaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CustomerLamaQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object CustomerLamaQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object CustomerLamaQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object CustomerLamaQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan p where'
      
        '(select(datediff(day,(select top 1 tgl from masterso where pelan' +
        'ggan=p.kode order by kodenota desc),GETDATE())))>:text')
    Left = 473
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object BonBarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from bonbarang where status=upper('#39'APPROVED'#39')')
    Left = 97
    Top = 65535
    object BonBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BonBarangQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object BonBarangQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object BonBarangQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object BonBarangQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object BonBarangQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object BonBarangQStoring: TStringField
      FieldName = 'Storing'
      Size = 10
    end
    object BonBarangQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object BonBarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BonBarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BonBarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BonBarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BonBarangQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object BonBarangQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object LPBQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.tglterima, d.jumlah, d.barang from lpb l, detaillpb d w' +
        'here getdate()<l.tglterima+1 and d.kodelpb=l.kode')
    Left = 225
    Top = 65535
    object LPBQtglterima: TDateTimeField
      FieldName = 'tglterima'
      Required = True
    end
    object LPBQjumlah: TIntegerField
      FieldName = 'jumlah'
      Required = True
    end
    object LPBQbarang: TStringField
      FieldName = 'barang'
      Required = True
      Size = 10
    end
    object LPBQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'barang'
      Size = 50
      Lookup = True
    end
  end
  object DaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli where status='#39'NEW DB'#39)
    Left = 129
    Top = 65535
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DaftarBeliQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object DaftarBeliQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object DaftarBeliQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object DaftarBeliQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object DaftarBeliQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object DaftarBeliQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object DaftarBeliQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object DaftarBeliQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object DaftarBeliQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object DaftarBeliQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object DaftarBeliQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object DaftarBeliQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object DaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
  object PickKendaraanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, sj.kodenota as surat_jalan, p.namapt, r.muat, r.bong' +
        'kar from MasterSO m left outer join'
      
        'MasterSJ sj on m.Kodenota=sj.NoSO, Pelanggan p, Rute r where m.P' +
        'elanggan = p.Kode and m.Rute=r.Kode and m.status='#39'DEAL'#39' and m.ar' +
        'mada is NULL and getdate()>=m.berangkat - :text'
      ' ')
    Left = 192
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PickKendaraanQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object PickKendaraanQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object PickKendaraanQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object PickKendaraanQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object PickKendaraanQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object PickKendaraanQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object PickKendaraanQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object PickKendaraanQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object PickKendaraanQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object PickKendaraanQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object PickKendaraanQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object PickKendaraanQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object PickKendaraanQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object PickKendaraanQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object PickKendaraanQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object PickKendaraanQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object PickKendaraanQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object PickKendaraanQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object PickKendaraanQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object PickKendaraanQAC: TBooleanField
      FieldName = 'AC'
    end
    object PickKendaraanQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object PickKendaraanQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object PickKendaraanQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object PickKendaraanQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object PickKendaraanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PickKendaraanQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object PickKendaraanQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object PickKendaraanQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object PickKendaraanQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object PickKendaraanQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object PickKendaraanQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object PickKendaraanQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object PickKendaraanQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object PickKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PickKendaraanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PickKendaraanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PickKendaraanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PickKendaraanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PickKendaraanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PickKendaraanQsurat_jalan: TStringField
      FieldName = 'surat_jalan'
      Size = 10
    end
    object PickKendaraanQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
    object PickKendaraanQmuat: TStringField
      FieldName = 'muat'
      Required = True
      Size = 50
    end
    object PickKendaraanQbongkar: TStringField
      FieldName = 'bongkar'
      Required = True
      Size = 50
    end
    object PickKendaraanQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object PerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select pp.*, lp.kode as laporanperbaikan from permintaanperbaika' +
        'n pp left outer join'
      'laporanperbaikan lp on pp.Kode=lp.pp where lp.kode is null'
      ' ')
    Left = 160
    object PerbaikanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PerbaikanQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PerbaikanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PerbaikanQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object PerbaikanQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PerbaikanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PerbaikanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PerbaikanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PerbaikanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PerbaikanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PerbaikanQlaporanperbaikan: TStringField
      FieldName = 'laporanperbaikan'
      Size = 10
    end
  end
  object MinStokQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select b.* from barang b where b.jumlah<=b.minimumstok and'
      
        '(select count(*) from DaftarBeli where Barang=b.kode and Status=' +
        #39'ON PROCESS'#39')<1')
    Left = 257
    Top = 1
    object MinStokQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MinStokQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MinStokQJumlah: TIntegerField
      FieldName = 'Jumlah'
      Required = True
    end
    object MinStokQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object MinStokQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object MinStokQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object MinStokQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object MinStokQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object MinStokQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object MinStokQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object MinStokQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object MinStokQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object MinStokQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MinStokQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MinStokQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MinStokQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MinStokQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MinStokQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MinStokQFoto: TBlobField
      FieldName = 'Foto'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 409
    Top = 9
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object STNKPajakExpiredQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada where GETDATE() + :text >=STNKPajakExpired ')
    Left = 288
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object STNKPajakExpiredQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object STNKPajakExpiredQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object STNKPajakExpiredQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object STNKPajakExpiredQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object STNKPajakExpiredQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object STNKPajakExpiredQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object STNKPajakExpiredQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object STNKPajakExpiredQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object STNKPajakExpiredQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object STNKPajakExpiredQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object STNKPajakExpiredQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object STNKPajakExpiredQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object STNKPajakExpiredQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object STNKPajakExpiredQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object STNKPajakExpiredQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object STNKPajakExpiredQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object STNKPajakExpiredQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object STNKPajakExpiredQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object STNKPajakExpiredQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object STNKPajakExpiredQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object STNKPajakExpiredQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object STNKPajakExpiredQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object STNKPajakExpiredQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object STNKPajakExpiredQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object STNKPajakExpiredQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object STNKPajakExpiredQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object STNKPajakExpiredQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object STNKPajakExpiredQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object STNKPerpanjangExpiredQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where GETDATE()+ :text >=STNKPerpanjangExpi' +
        'red ')
    Left = 320
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object STNKPerpanjangExpiredQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object STNKPerpanjangExpiredQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object STNKPerpanjangExpiredQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object STNKPerpanjangExpiredQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object STNKPerpanjangExpiredQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object STNKPerpanjangExpiredQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object STNKPerpanjangExpiredQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object STNKPerpanjangExpiredQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object STNKPerpanjangExpiredQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object STNKPerpanjangExpiredQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object STNKPerpanjangExpiredQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object STNKPerpanjangExpiredQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object STNKPerpanjangExpiredQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object STNKPerpanjangExpiredQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object STNKPerpanjangExpiredQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object STNKPerpanjangExpiredQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object STNKPerpanjangExpiredQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object STNKPerpanjangExpiredQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object STNKPerpanjangExpiredQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object STNKPerpanjangExpiredQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object STNKPerpanjangExpiredQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object STNKPerpanjangExpiredQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object STNKPerpanjangExpiredQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object STNKPerpanjangExpiredQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object STNKPerpanjangExpiredQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object STNKPerpanjangExpiredQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object STNKPerpanjangExpiredQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object STNKPerpanjangExpiredQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object KirExpiredQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada where GETDATE() + :text >=KirSelesai')
    Left = 352
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KirExpiredQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KirExpiredQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object KirExpiredQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object KirExpiredQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object KirExpiredQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object KirExpiredQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object KirExpiredQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object KirExpiredQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object KirExpiredQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object KirExpiredQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object KirExpiredQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object KirExpiredQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object KirExpiredQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object KirExpiredQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object KirExpiredQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object KirExpiredQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object KirExpiredQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object KirExpiredQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object KirExpiredQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KirExpiredQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KirExpiredQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KirExpiredQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KirExpiredQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object KirExpiredQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object KirExpiredQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object KirExpiredQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object KirExpiredQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object KirExpiredQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object SIMExpiredQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai where GETDATE()> ExpiredSIM')
    Left = 424
    Top = 88
    object SIMExpiredQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SIMExpiredQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object SIMExpiredQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object SIMExpiredQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object SIMExpiredQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object SIMExpiredQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object SIMExpiredQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object SIMExpiredQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object SIMExpiredQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object SIMExpiredQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object SIMExpiredQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object SIMExpiredQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object SIMExpiredQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object SIMExpiredQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SIMExpiredQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select * from Kontrak where GetDate()>=DATEADD(day,:text,TglSele' +
        'sai) and Status<>'#39'FINISHED'#39)
    Left = 272
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
end
