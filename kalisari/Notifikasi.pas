unit Notifikasi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxListBox, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, DateUtils;

type
  TNotifikasiFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    BarangQ: TSDQuery;
    StatusBar: TStatusBar;
    SJQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    FollowUpSOQ: TSDQuery;
    CustQ: TSDQuery;
    CustomerLamaQ: TSDQuery;
    cxListBox1: TcxListBox;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1TableView1Column1: TcxGridColumn;
    cxGrid1TableView1Column2: TcxGridColumn;
    PelangganQ: TSDQuery;
    BonBarangQ: TSDQuery;
    LPBQ: TSDQuery;
    DaftarBeliQ: TSDQuery;
    PickKendaraanQ: TSDQuery;
    PerbaikanQ: TSDQuery;
    MinStokQ: TSDQuery;
    ArmadaQ: TSDQuery;
    cxListBox2: TcxListBox;
    FollowUpSOQKodenota: TStringField;
    FollowUpSOQTgl: TDateTimeField;
    FollowUpSOQPelanggan: TStringField;
    FollowUpSOQBerangkat: TDateTimeField;
    FollowUpSOQTiba: TDateTimeField;
    FollowUpSOQHarga: TCurrencyField;
    FollowUpSOQPembayaranAwal: TCurrencyField;
    FollowUpSOQTglPembayaranAwal: TDateTimeField;
    FollowUpSOQCaraPembayaranAwal: TStringField;
    FollowUpSOQNoKwitansiPembayaranAwal: TStringField;
    FollowUpSOQPenerimaPembayaranAwal: TStringField;
    FollowUpSOQPelunasan: TCurrencyField;
    FollowUpSOQCaraPembayaranPelunasan: TStringField;
    FollowUpSOQNoKwitansiPelunasan: TStringField;
    FollowUpSOQPenerimaPelunasan: TStringField;
    FollowUpSOQExtend: TBooleanField;
    FollowUpSOQTglKembaliExtend: TDateTimeField;
    FollowUpSOQBiayaExtend: TCurrencyField;
    FollowUpSOQKapasitasSeat: TIntegerField;
    FollowUpSOQAC: TBooleanField;
    FollowUpSOQToilet: TBooleanField;
    FollowUpSOQAirSuspension: TBooleanField;
    FollowUpSOQRute: TStringField;
    FollowUpSOQTglFollowUp: TDateTimeField;
    FollowUpSOQArmada: TStringField;
    FollowUpSOQKontrak: TStringField;
    FollowUpSOQPICJemput: TMemoField;
    FollowUpSOQJamJemput: TDateTimeField;
    FollowUpSOQNoTelpPICJemput: TStringField;
    FollowUpSOQAlamatJemput: TMemoField;
    FollowUpSOQStatus: TStringField;
    FollowUpSOQStatusPembayaran: TStringField;
    FollowUpSOQReminderPending: TDateTimeField;
    FollowUpSOQKeterangan: TMemoField;
    FollowUpSOQCreateDate: TDateTimeField;
    FollowUpSOQCreateBy: TStringField;
    FollowUpSOQOperator: TStringField;
    FollowUpSOQTglEntry: TDateTimeField;
    FollowUpSOQTglCetak: TDateTimeField;
    FollowUpSOQnamapt: TStringField;
    CustomerLamaQKode: TStringField;
    CustomerLamaQNamaJenis: TStringField;
    CustomerLamaQTipe: TMemoField;
    CustomerLamaQKategori: TStringField;
    CustomerLamaQKeterangan: TMemoField;
    SJQKodenota: TStringField;
    SJQTgl: TDateTimeField;
    SJQPelanggan: TStringField;
    SJQBerangkat: TDateTimeField;
    SJQTiba: TDateTimeField;
    SJQHarga: TCurrencyField;
    SJQPembayaranAwal: TCurrencyField;
    SJQTglPembayaranAwal: TDateTimeField;
    SJQCaraPembayaranAwal: TStringField;
    SJQNoKwitansiPembayaranAwal: TStringField;
    SJQPenerimaPembayaranAwal: TStringField;
    SJQPelunasan: TCurrencyField;
    SJQCaraPembayaranPelunasan: TStringField;
    SJQNoKwitansiPelunasan: TStringField;
    SJQPenerimaPelunasan: TStringField;
    SJQExtend: TBooleanField;
    SJQTglKembaliExtend: TDateTimeField;
    SJQBiayaExtend: TCurrencyField;
    SJQKapasitasSeat: TIntegerField;
    SJQAC: TBooleanField;
    SJQToilet: TBooleanField;
    SJQAirSuspension: TBooleanField;
    SJQRute: TStringField;
    SJQTglFollowUp: TDateTimeField;
    SJQArmada: TStringField;
    SJQKontrak: TStringField;
    SJQPICJemput: TMemoField;
    SJQJamJemput: TDateTimeField;
    SJQNoTelpPICJemput: TStringField;
    SJQAlamatJemput: TMemoField;
    SJQStatus: TStringField;
    SJQStatusPembayaran: TStringField;
    SJQReminderPending: TDateTimeField;
    SJQKeterangan: TMemoField;
    SJQCreateDate: TDateTimeField;
    SJQCreateBy: TStringField;
    SJQOperator: TStringField;
    SJQTglEntry: TDateTimeField;
    SJQTglCetak: TDateTimeField;
    SJQsurat_jalan: TStringField;
    SJQnamapt: TStringField;
    SJQmuat: TStringField;
    SJQbongkar: TStringField;
    BonBarangQKode: TStringField;
    BonBarangQPeminta: TStringField;
    BonBarangQPenyetuju: TStringField;
    BonBarangQPenerima: TStringField;
    BonBarangQLaporanPerbaikan: TStringField;
    BonBarangQLaporanPerawatan: TStringField;
    BonBarangQStoring: TStringField;
    BonBarangQStatus: TStringField;
    BonBarangQCreateDate: TDateTimeField;
    BonBarangQCreateBy: TStringField;
    BonBarangQOperator: TStringField;
    BonBarangQTglEntry: TDateTimeField;
    BonBarangQArmada: TStringField;
    BonBarangQTglCetak: TDateTimeField;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQSupplier1: TStringField;
    DaftarBeliQSupplier2: TStringField;
    DaftarBeliQSupplier3: TStringField;
    DaftarBeliQHarga1: TCurrencyField;
    DaftarBeliQHarga2: TCurrencyField;
    DaftarBeliQHarga3: TCurrencyField;
    DaftarBeliQTermin1: TIntegerField;
    DaftarBeliQTermin2: TIntegerField;
    DaftarBeliQTermin3: TIntegerField;
    DaftarBeliQKeterangan1: TMemoField;
    DaftarBeliQKeterangan2: TMemoField;
    DaftarBeliQKeterangan3: TMemoField;
    DaftarBeliQTermin: TIntegerField;
    PerbaikanQKode: TStringField;
    PerbaikanQTanggal: TDateTimeField;
    PerbaikanQArmada: TStringField;
    PerbaikanQPeminta: TStringField;
    PerbaikanQKeluhan: TMemoField;
    PerbaikanQCreateDate: TDateTimeField;
    PerbaikanQCreateBy: TStringField;
    PerbaikanQOperator: TStringField;
    PerbaikanQTglEntry: TDateTimeField;
    PerbaikanQTglCetak: TDateTimeField;
    PerbaikanQlaporanperbaikan: TStringField;
    PickKendaraanQKodenota: TStringField;
    PickKendaraanQTgl: TDateTimeField;
    PickKendaraanQPelanggan: TStringField;
    PickKendaraanQBerangkat: TDateTimeField;
    PickKendaraanQTiba: TDateTimeField;
    PickKendaraanQHarga: TCurrencyField;
    PickKendaraanQPembayaranAwal: TCurrencyField;
    PickKendaraanQTglPembayaranAwal: TDateTimeField;
    PickKendaraanQCaraPembayaranAwal: TStringField;
    PickKendaraanQNoKwitansiPembayaranAwal: TStringField;
    PickKendaraanQPenerimaPembayaranAwal: TStringField;
    PickKendaraanQPelunasan: TCurrencyField;
    PickKendaraanQCaraPembayaranPelunasan: TStringField;
    PickKendaraanQNoKwitansiPelunasan: TStringField;
    PickKendaraanQPenerimaPelunasan: TStringField;
    PickKendaraanQExtend: TBooleanField;
    PickKendaraanQTglKembaliExtend: TDateTimeField;
    PickKendaraanQBiayaExtend: TCurrencyField;
    PickKendaraanQKapasitasSeat: TIntegerField;
    PickKendaraanQAC: TBooleanField;
    PickKendaraanQToilet: TBooleanField;
    PickKendaraanQAirSuspension: TBooleanField;
    PickKendaraanQRute: TStringField;
    PickKendaraanQTglFollowUp: TDateTimeField;
    PickKendaraanQArmada: TStringField;
    PickKendaraanQKontrak: TStringField;
    PickKendaraanQPICJemput: TMemoField;
    PickKendaraanQJamJemput: TDateTimeField;
    PickKendaraanQNoTelpPICJemput: TStringField;
    PickKendaraanQAlamatJemput: TMemoField;
    PickKendaraanQStatus: TStringField;
    PickKendaraanQStatusPembayaran: TStringField;
    PickKendaraanQReminderPending: TDateTimeField;
    PickKendaraanQKeterangan: TMemoField;
    PickKendaraanQCreateDate: TDateTimeField;
    PickKendaraanQCreateBy: TStringField;
    PickKendaraanQOperator: TStringField;
    PickKendaraanQTglEntry: TDateTimeField;
    PickKendaraanQTglCetak: TDateTimeField;
    PickKendaraanQsurat_jalan: TStringField;
    PickKendaraanQnamapt: TStringField;
    PickKendaraanQmuat: TStringField;
    PickKendaraanQbongkar: TStringField;
    LPBQtglterima: TDateTimeField;
    LPBQjumlah: TIntegerField;
    LPBQbarang: TStringField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    CustQKodenota: TStringField;
    CustQTgl: TDateTimeField;
    CustQPelanggan: TStringField;
    CustQBerangkat: TDateTimeField;
    CustQTiba: TDateTimeField;
    CustQHarga: TCurrencyField;
    CustQPembayaranAwal: TCurrencyField;
    CustQTglPembayaranAwal: TDateTimeField;
    CustQCaraPembayaranAwal: TStringField;
    CustQNoKwitansiPembayaranAwal: TStringField;
    CustQPenerimaPembayaranAwal: TStringField;
    CustQPelunasan: TCurrencyField;
    CustQCaraPembayaranPelunasan: TStringField;
    CustQNoKwitansiPelunasan: TStringField;
    CustQPenerimaPelunasan: TStringField;
    CustQExtend: TBooleanField;
    CustQTglKembaliExtend: TDateTimeField;
    CustQBiayaExtend: TCurrencyField;
    CustQKapasitasSeat: TIntegerField;
    CustQAC: TBooleanField;
    CustQToilet: TBooleanField;
    CustQAirSuspension: TBooleanField;
    CustQRute: TStringField;
    CustQTglFollowUp: TDateTimeField;
    CustQArmada: TStringField;
    CustQKontrak: TStringField;
    CustQPICJemput: TMemoField;
    CustQJamJemput: TDateTimeField;
    CustQNoTelpPICJemput: TStringField;
    CustQAlamatJemput: TMemoField;
    CustQStatus: TStringField;
    CustQStatusPembayaran: TStringField;
    CustQReminderPending: TDateTimeField;
    CustQKeterangan: TMemoField;
    CustQCreateDate: TDateTimeField;
    CustQCreateBy: TStringField;
    CustQOperator: TStringField;
    CustQTglEntry: TDateTimeField;
    CustQTglCetak: TDateTimeField;
    LPBQNamaBarang: TStringField;
    STNKPajakExpiredQ: TSDQuery;
    STNKPajakExpiredQKode: TStringField;
    STNKPajakExpiredQPlatNo: TStringField;
    STNKPajakExpiredQJumlahSeat: TIntegerField;
    STNKPajakExpiredQTahunPembuatan: TStringField;
    STNKPajakExpiredQNoBody: TStringField;
    STNKPajakExpiredQJenisAC: TStringField;
    STNKPajakExpiredQJenisBBM: TStringField;
    STNKPajakExpiredQKapasitasTangkiBBM: TIntegerField;
    STNKPajakExpiredQLevelArmada: TStringField;
    STNKPajakExpiredQJumlahBan: TIntegerField;
    STNKPajakExpiredQAktif: TBooleanField;
    STNKPajakExpiredQAC: TBooleanField;
    STNKPajakExpiredQToilet: TBooleanField;
    STNKPajakExpiredQAirSuspension: TBooleanField;
    STNKPajakExpiredQKmSekarang: TIntegerField;
    STNKPajakExpiredQKeterangan: TStringField;
    STNKPajakExpiredQSopir: TStringField;
    STNKPajakExpiredQJenisKendaraan: TStringField;
    STNKPajakExpiredQCreateDate: TDateTimeField;
    STNKPajakExpiredQCreateBy: TStringField;
    STNKPajakExpiredQOperator: TStringField;
    STNKPajakExpiredQTglEntry: TDateTimeField;
    STNKPajakExpiredQSTNKPajakExpired: TDateTimeField;
    STNKPajakExpiredQSTNKPerpanjangExpired: TDateTimeField;
    STNKPajakExpiredQKirMulai: TDateTimeField;
    STNKPajakExpiredQKirSelesai: TDateTimeField;
    STNKPajakExpiredQNoRangka: TStringField;
    STNKPajakExpiredQNoMesin: TStringField;
    STNKPerpanjangExpiredQ: TSDQuery;
    STNKPerpanjangExpiredQKode: TStringField;
    STNKPerpanjangExpiredQPlatNo: TStringField;
    STNKPerpanjangExpiredQJumlahSeat: TIntegerField;
    STNKPerpanjangExpiredQTahunPembuatan: TStringField;
    STNKPerpanjangExpiredQNoBody: TStringField;
    STNKPerpanjangExpiredQJenisAC: TStringField;
    STNKPerpanjangExpiredQJenisBBM: TStringField;
    STNKPerpanjangExpiredQKapasitasTangkiBBM: TIntegerField;
    STNKPerpanjangExpiredQLevelArmada: TStringField;
    STNKPerpanjangExpiredQJumlahBan: TIntegerField;
    STNKPerpanjangExpiredQAktif: TBooleanField;
    STNKPerpanjangExpiredQAC: TBooleanField;
    STNKPerpanjangExpiredQToilet: TBooleanField;
    STNKPerpanjangExpiredQAirSuspension: TBooleanField;
    STNKPerpanjangExpiredQKmSekarang: TIntegerField;
    STNKPerpanjangExpiredQKeterangan: TStringField;
    STNKPerpanjangExpiredQSopir: TStringField;
    STNKPerpanjangExpiredQJenisKendaraan: TStringField;
    STNKPerpanjangExpiredQCreateDate: TDateTimeField;
    STNKPerpanjangExpiredQCreateBy: TStringField;
    STNKPerpanjangExpiredQOperator: TStringField;
    STNKPerpanjangExpiredQTglEntry: TDateTimeField;
    STNKPerpanjangExpiredQSTNKPajakExpired: TDateTimeField;
    STNKPerpanjangExpiredQSTNKPerpanjangExpired: TDateTimeField;
    STNKPerpanjangExpiredQKirMulai: TDateTimeField;
    STNKPerpanjangExpiredQKirSelesai: TDateTimeField;
    STNKPerpanjangExpiredQNoRangka: TStringField;
    STNKPerpanjangExpiredQNoMesin: TStringField;
    KirExpiredQ: TSDQuery;
    KirExpiredQKode: TStringField;
    KirExpiredQPlatNo: TStringField;
    KirExpiredQJumlahSeat: TIntegerField;
    KirExpiredQTahunPembuatan: TStringField;
    KirExpiredQNoBody: TStringField;
    KirExpiredQJenisAC: TStringField;
    KirExpiredQJenisBBM: TStringField;
    KirExpiredQKapasitasTangkiBBM: TIntegerField;
    KirExpiredQLevelArmada: TStringField;
    KirExpiredQJumlahBan: TIntegerField;
    KirExpiredQAktif: TBooleanField;
    KirExpiredQAC: TBooleanField;
    KirExpiredQToilet: TBooleanField;
    KirExpiredQAirSuspension: TBooleanField;
    KirExpiredQKmSekarang: TIntegerField;
    KirExpiredQKeterangan: TStringField;
    KirExpiredQSopir: TStringField;
    KirExpiredQJenisKendaraan: TStringField;
    KirExpiredQCreateDate: TDateTimeField;
    KirExpiredQCreateBy: TStringField;
    KirExpiredQOperator: TStringField;
    KirExpiredQTglEntry: TDateTimeField;
    KirExpiredQSTNKPajakExpired: TDateTimeField;
    KirExpiredQSTNKPerpanjangExpired: TDateTimeField;
    KirExpiredQKirMulai: TDateTimeField;
    KirExpiredQKirSelesai: TDateTimeField;
    KirExpiredQNoRangka: TStringField;
    KirExpiredQNoMesin: TStringField;
    SIMExpiredQ: TSDQuery;
    SIMExpiredQKode: TStringField;
    SIMExpiredQNama: TStringField;
    SIMExpiredQAlamat: TStringField;
    SIMExpiredQKota: TStringField;
    SIMExpiredQNoTelp: TStringField;
    SIMExpiredQNoHP: TStringField;
    SIMExpiredQTglLahir: TDateTimeField;
    SIMExpiredQGaji: TCurrencyField;
    SIMExpiredQJabatan: TStringField;
    SIMExpiredQMulaiBekerja: TDateTimeField;
    SIMExpiredQNomorSIM: TStringField;
    SIMExpiredQExpiredSIM: TDateTimeField;
    SIMExpiredQAktif: TBooleanField;
    SIMExpiredQKeterangan: TMemoField;
    SIMExpiredQNoKTP: TStringField;
    FollowUpSOQTglPelunasan: TDateTimeField;
    CustQTglPelunasan: TDateTimeField;
    SJQTglPelunasan: TDateTimeField;
    PickKendaraanQTglPelunasan: TDateTimeField;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    MinStokQKode: TStringField;
    MinStokQNama: TStringField;
    MinStokQJumlah: TIntegerField;
    MinStokQSatuan: TStringField;
    MinStokQMinimumStok: TIntegerField;
    MinStokQMaximumStok: TIntegerField;
    MinStokQStandardUmur: TIntegerField;
    MinStokQLokasi: TStringField;
    MinStokQClaimNWarranty: TBooleanField;
    MinStokQDurasiClaimNWarranty: TIntegerField;
    MinStokQKategori: TStringField;
    MinStokQSingleSupplier: TBooleanField;
    MinStokQHarga: TCurrencyField;
    MinStokQKeterangan: TMemoField;
    MinStokQCreateDate: TDateTimeField;
    MinStokQCreateBy: TStringField;
    MinStokQOperator: TStringField;
    MinStokQTglEntry: TDateTimeField;
    MinStokQFoto: TBlobField;
    cxListBox3: TcxListBox;
    BarangQJumlah: TFloatField;
    procedure cek();
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxListBox1DblClick(Sender: TObject);
    procedure cxGrid1TableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1TableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SaveBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
    
  end;

var
  NotifikasiFm: TNotifikasiFm;
  jumnotif :integer;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, Math, DaftarPeserta, NotaSJ,
  BonBarang, DaftarBeli, LaporanPermintaanPerbaikan, PickOrder,
  MasterArmada, MasterPegawai, Kontrak;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotifikasiFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TNotifikasiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotifikasiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotifikasiFm.ExitBtnClick(Sender: TObject);
begin
  Close;
  Release;
end;

procedure TNotifikasiFm.cek();
var i:integer;
begin
  //cxGrid1TableView1.DataController.ClearDetails;
  cxGrid1TableView1.DataController.RecordCount:=0;
  jumnotif:=0;

  if menuutamafm.UserQFollowUpCustomer.AsBoolean then
  begin
    FollowUpSOQ.Open;
    for i:=1 to FollowUpSOQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Sales Order';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Follow Up Sales Order ' + FollowUpSOQnamapt.AsString;
      cxListBox1.Items.Add('Follow Up Sales Order ' + FollowUpSOQnamapt.AsString);
      cxlistbox2.Items.Add('SO');
      cxlistbox3.Items.Add(FollowUpSOQKodenota.AsString);
      FollowUpSOQ.Next;
    end;
  end;

  if menuutamafm.UserQSTNKPajakExpired.AsBoolean then
  begin
    STNKPajakExpiredQ.Close;
    STNKPajakExpiredQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQSTNKPExpired.AsInteger;
    STNKPajakExpiredQ.Open;
    for i:=1 to STNKPajakExpiredQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='STNK Expired';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='STNK Pajak Armada ' + STNKPajakExpiredQplatno.AsString + ' akan expired pada ' + datetimetostr(STNKPajakExpiredQSTNKPajakExpired.AsDateTime);
      cxListBox1.Items.Add('STNK Pajak Expired ' + STNKPajakExpiredQPlatNo.AsString);
      cxlistbox2.Items.Add('STNKPajak');
      cxlistbox3.Items.Add(STNKPajakExpiredQKode.AsString);
      STNKPajakExpiredQ.Next;
    end;
  end;

  if menuutamafm.UserQSIMExpired.AsBoolean then
  begin
    SIMExpiredQ.Open;
    for i:=1 to SIMExpiredQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='SIM Expired';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='SIM Pegawai ' + SIMExpiredQNama.AsString + ' telah expired';
      cxListBox1.Items.Add('SIM Expired ' + SIMExpiredQNama.AsString);
      cxlistbox2.Items.Add('SIM');
      cxlistbox3.Items.Add(SIMExpiredQKode.AsString);
      SIMExpiredQ.Next;
    end;
  end;

  if menuutamafm.UserQSIMExpired.AsBoolean then
  begin
    KontrakQ.Close;
    KontrakQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQKontrak.AsInteger*-1;
    KontrakQ.Open;
    for i:=1 to KontrakQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Kontrak akan berakhir';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Kontrak ' + KontrakQKode.AsString + ' akan berakhir dalam 2 hari';
      cxListBox1.Items.Add('Kontrak Berakhir ' + KontrakQKode.AsString);
      cxlistbox2.Items.Add('Kontrak');
      cxlistbox3.Items.Add(KontrakQKode.AsString);
      KontrakQ.Next;
    end;
  end;

  if menuutamafm.UserQKirExpired.AsBoolean then
  begin
    KirExpiredQ.Close;
    KirExpiredQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQKIRExpired.AsInteger;
    KirExpiredQ.Open;
    for i:=1 to KirExpiredQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Kir Expired';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Kir Armada ' + KirExpiredQplatno.AsString + ' akan expired pada ' + datetimetostr(KirExpiredQKirSelesai.AsDateTime);
      cxListBox1.Items.Add('Kir Expired ' + KirExpiredQPlatNo.AsString);
      cxlistbox2.Items.Add('Kir Armada');
      cxlistbox3.Items.Add(KirExpiredQKode.AsString);
      KirExpiredQ.Next;
    end;
  end;

  if menuutamafm.UserQSTNKExpired.AsBoolean then
  begin
    STNKPerpanjangExpiredQ.Close;
    STNKPerpanjangExpiredQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQSTNKExpired.AsInteger;
    STNKPerpanjangExpiredQ.Open;
    for i:=1 to STNKPerpanjangExpiredQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='STNK Expired';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='STNK Perpanjang Armada ' + STNKPajakExpiredQplatno.AsString + ' akan expired pada ' + datetimetostr(STNKPerpanjangExpiredQSTNKPerpanjangExpired.AsDateTime);
      cxListBox1.Items.Add('STNK Expired ' + STNKPajakExpiredQPlatNo.AsString);
      cxlistbox2.Items.Add('STNKPerpanjang');
      cxlistbox3.Items.Add(STNKPerpanjangExpiredQKode.AsString);
      STNKPerpanjangExpiredQ.Next;
    end;
  end;

  if menuutamafm.UserQInactiveCustomer.AsBoolean then
  begin
    Pelangganq.Close;
    pelangganq.ParamByName('text').AsString:= inttostr(inactivelimit);
    pelangganq.Open;
    for i:=1 to pelangganq.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Inactive Customer';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Lama tidak order ' + PelangganQNamaPT.AsString;
      cxListBox1.Items.Add('Customer ' + PelangganQNamaPT.AsString);
      cxlistbox2.Items.Add('Inactive');
      cxlistbox3.Items.Add(PelangganQNamaPT.AsString);
      PelangganQ.Next;
    end;
  end;

  if menuutamafm.UserQPembuatanSuratJalan.AsBoolean then
  begin
    SJQ.Close;
    SJQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQSuratJalan.AsInteger;
    SJQ.Open;
    for i:=1 to SJQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Surat Jalan';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Buat Surat Jalan ' + SJQnamapt.AsString;
      cxListBox1.Items.Add('Buat Surat Jalan ' + SJQnamapt.AsString);
      cxlistbox2.Items.Add('SJ');
      cxlistbox3.Items.Add(SJQKodenota.AsString);
      SJQ.Next;
    end;
  end;

  if menuutamafm.UserQNewBonBarang.AsBoolean then
  begin
    BonBarangQ.Open;
    if BonBarangQ.RecordCount>0 then
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Bon Barang';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Bon Barang Baru ' + inttostr(BonBarangQ.RecordCount);
      cxListBox1.Items.Add('Bon Barang Baru ');
      cxlistbox2.Items.Add('Bon Barang');
      cxlistbox3.Items.Add('Bon Barang');
    end;
  end;

  if menuutamafm.UserQNewDaftarBeli.AsBoolean then
  begin
    DaftarBeliQ.Open;
    if DaftarBeliQ.RecordCount>0 then
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Daftar Beli';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Daftar Beli Baru ' + inttostr(DaftarBeliQ.RecordCount);
      cxListBox1.Items.Add('Daftar Beli Baru ');
      cxlistbox2.Items.Add('Daftar Beli');
      cxlistbox3.Items.Add('Daftar Beli');
    end;
  end;

  if menuutamafm.UserQNewPermintaanPerbaikan.AsBoolean then
  begin
    PerbaikanQ.Open;
    for i:=1 to PerbaikanQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Perbaikan';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Buat Laporan Perbaikan ' + PerbaikanQKeluhan.AsString;
      cxListBox1.Items.Add('Buat Perbaikan ');
      cxlistbox2.Items.Add('Perbaikan');
      cxlistbox3.Items.Add(PerbaikanQKode.AsString);
      PerbaikanQ.Next;
    end;
  end;

  if menuutamafm.UserQNotifPilihArmada.AsBoolean then
  begin
    PickKendaraanQ.Close;
    PickKendaraanQ.ParamByName('text').AsInteger:=MenuUtamaFm.NotifQPilihKendaraan.AsInteger;
    PickKendaraanQ.Open;
    for i:=1 to PickKendaraanQ.RecordCount do
    begin
    jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Pick Kendaraan';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Pick Kendaraan untuk SO : ' + PickKendaraanQKodenota.AsString;
      cxListBox1.Items.Add('Pick Kendaraan');
      cxlistbox2.Items.Add('Pick Kendaraan');
      cxlistbox3.Items.Add(PickKendaraanQKodenota.asstring);
      PickKendaraanQ.Next;
    end;
  end;

  if menuutamafm.UserQNewLPB.AsBoolean then
  begin
    LPBQ.Open;
    for i:=1 to LPBQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='LPB';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:='Barang Datang ' + LPBQNamaBarang.AsString + ' sejumlah ' + LPBQjumlah.AsString;
      cxListBox1.Items.Add('LPB');
      cxlistbox2.Items.Add('LPB');
      cxlistbox3.Items.Add('LPB');
      LPBQ.Next;
    end;
  end;

  if menuutamafm.UserQMinimumStok.AsBoolean then
  begin
    MinStokQ.Open;
    for i:=1 to MinStokQ.RecordCount do
    begin
      jumnotif:=jumnotif + 1;
      cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
      cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Min Stok';
      cxGrid1TableView1.DataController.Values[jumnotif-1,1]:=MinStokQNama.AsString + ' stok = ' + MinStokQJumlah.AsString + ' (min=' + MinStokQMinimumStok.AsString +')';
      cxListBox1.Items.Add('Min Stok');
      cxlistbox2.Items.Add('Min Stok');
      cxlistbox3.Items.Add('Min Stok');
      MinStokQ.Next;
    end;
  end;
end;

procedure TNotifikasiFm.FormCreate(Sender: TObject);
var i : integer;
begin
  DMFm:=TDMFm.Create(self);
  cek();
end;




procedure TNotifikasiFm.cxListBox1DblClick(Sender: TObject);
begin
  if cxListBox2.Items.Strings[cxListBox1.itemindex] = 'SO' then
  begin
    NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[cxListBox1.itemindex]);
  end
  else if cxListBox2.Items.Strings[cxListBox1.ItemIndex] = 'Kontrak' then
  begin
    //NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[cxListBox1.itemindex]);
  end;
  //ShowMessage(cxListBox3.Items.Strings[cxListBox1.ItemIndex]);
end;

procedure TNotifikasiFm.cxGrid1TableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'SO' then
  begin
    NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Inactive' then
  begin
    DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[Sender.DataController.GetFocusedRowIndex]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'SJ' then
  begin
    NotaSJFm := TNotaSJFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
    else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Kontrak' then
  begin
    //KontrakFm := TKontrakFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
   KontrakFm:=TKontrakFm.Create(self,cxlistbox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Bon Barang' then
  begin
    BonBarangFm := TBonBarangFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Daftar Beli' then
  begin
    DaftarBeliFm:=TDaftarBeliFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Perbaikan' then
  begin
    LaporanPermintaanPerbaikanFm := TLaporanPermintaanPerbaikanFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'STNKPajak' then
  begin
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
    MasterArmadaFm:=TMasterArmadaFm.Create(self,cxlistbox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);
    //MasterArmadaFm := TMasterArmadaFm.create(self,'');
  end
   else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'STNKPerpanjang' then
  begin
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
      MasterArmadaFm:=TMasterArmadaFm.Create(self,cxlistbox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);

  end
     else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Kir Armada' then
  begin
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
   // MasterArmadaFm := TMasterArmadaFm.create(self,'');
   MasterArmadaFm:=TMasterArmadaFm.Create(self,cxlistbox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);

  end
    else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'SIM' then
  begin
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  //  MasterPegawaiFm := TMasterPegawaiFm.create(self);
    MasterPegawaiFm:=TMasterPegawaiFm.Create(self,cxlistbox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);

  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Pick Kendaraan' then
  begin
    PickOrderFm := TPickOrderFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end;
end;

procedure TNotifikasiFm.cxGrid1TableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //ShowMessage(inttostr(Sender.DataController.GetFocusedRowIndex));
 { ShowMessage(cxListBox1.Items.Strings[Sender.DataController.GetFocusedRowIndex]);
    ShowMessage(cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex]);
      ShowMessage(cxListBox3.Items.Strings[Sender.DataController.GetFocusedRowIndex]);  }
end;

procedure TNotifikasiFm.SaveBtnClick(Sender: TObject);
begin
  cek();
  if jumnotif=0 then
  begin
    MenuUtamaFm.cxImage1.Visible:=false;
    MenuUtamaFm.cxLabel1.Visible:=false;
  end;
end;

end.
