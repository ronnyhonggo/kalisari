object POFm: TPOFm
  Left = 344
  Top = 62
  BorderStyle = bsDialog
  Caption = 'Purchase Order'
  ClientHeight = 689
  ClientWidth = 845
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 670
    Width = 845
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object SearchBtn: TcxButton
    Left = 712
    Top = 122
    Width = 57
    Height = 21
    Caption = 'Search'
    TabOrder = 1
    TabStop = False
    Visible = False
    OnClick = SearchBtnClick
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 672
    Top = 90
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 2
    Visible = False
    Width = 121
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 845
    Height = 41
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
  end
  object KodeEdit: TcxButtonEdit
    Left = 40
    Top = 10
    Properties.Buttons = <
      item
        Caption = '+'
        Default = True
        Kind = bkText
      end>
    Properties.OnButtonClick = KodeEditPropertiesButtonClick
    Properties.OnChange = KodeEditPropertiesChange
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    Style.ButtonStyle = bts3D
    TabOrder = 4
    OnEnter = KodeEditEnter
    OnExit = KodeEditExit
    OnKeyDown = KodeEditKeyDown
    Width = 121
  end
  object cxButton1: TcxButton
    Left = 168
    Top = 10
    Width = 57
    Height = 21
    Caption = 'Search'
    TabOrder = 5
    TabStop = False
    Visible = False
    OnClick = SearchBtnClick
  end
  object Button1: TButton
    Left = 280
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 8
    Visible = False
    OnClick = Button1Click
  end
  object pnl2: TPanel
    Left = 0
    Top = 576
    Width = 845
    Height = 94
    Align = alBottom
    TabOrder = 9
    object LblGrandTotal: TLabel
      Left = 752
      Top = 21
      Width = 12
      Height = 24
      Alignment = taRightJustify
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel
      Left = 725
      Top = 19
      Width = 27
      Height = 24
      Alignment = taRightJustify
      Caption = 'Rp'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label5: TLabel
      Left = 503
      Top = 8
      Width = 116
      Height = 24
      Alignment = taRightJustify
      Caption = 'Grand Total:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ExitBtn: TcxButton
      Left = 252
      Top = 26
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 171
      Top = 26
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 26
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object SetujuBtn: TcxButton
      Left = 88
      Top = 26
      Width = 75
      Height = 25
      Caption = 'SETUJU'
      TabOrder = 3
      OnClick = BitBtn1Click
    end
    object cxGroupBox1: TcxGroupBox
      Left = 461
      Top = 42
      Align = alCustom
      Alignment = alTopRight
      Caption = 'Tanggal History PO Finished'
      TabOrder = 4
      Height = 47
      Width = 383
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 17
        Properties.SaveTime = False
        Properties.ShowTime = False
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 17
        Properties.SaveTime = False
        Properties.ShowTime = False
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 141
        Top = 19
        Caption = 's/d'
      end
      object cxButHPOFinished: TcxButton
        Left = 304
        Top = 16
        Width = 73
        Height = 22
        Caption = 'Tampilkan'
        TabOrder = 3
        OnClick = cxButHPOFinishedClick
      end
    end
    object cxButReset: TcxButton
      Left = 384
      Top = 24
      Width = 49
      Height = 33
      Caption = 'Reset'
      TabOrder = 5
      OnClick = cxButResetClick
    end
  end
  object BtnSave: TButton
    Left = 128
    Top = 464
    Width = 83
    Height = 41
    Caption = 'Save'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    Visible = False
    OnClick = BtnSaveClick
  end
  object BitBtn1: TBitBtn
    Left = 328
    Top = 312
    Width = 161
    Height = 41
    Caption = 'Save dan Setuju'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    Visible = False
    OnClick = BitBtn1Click
    Kind = bkAll
  end
  object Button4: TButton
    Left = 0
    Top = 456
    Width = 113
    Height = 41
    Caption = 'Refresh'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    Visible = False
    OnClick = Button4Click
  end
  object TxtRupiah: TcxCurrencyEdit
    Left = 631
    Top = 578
    ParentFont = False
    Properties.DecimalPlaces = 0
    Properties.DisplayFormat = 'Rp,0.'
    Properties.EditFormat = 'Rp,0.'
    Properties.ReadOnly = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 13
    Width = 185
  end
  object ListBox1: TListBox
    Left = 448
    Top = 304
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 14
    Visible = False
  end
  object BtnDelete: TButton
    Left = 416
    Top = 176
    Width = 113
    Height = 41
    Caption = 'Delete PO ini'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    Visible = False
    OnClick = BtnDeleteClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 375
    Width = 845
    Height = 201
    Align = alBottom
    TabOrder = 16
    object Label6: TLabel
      Left = 1
      Top = 1
      Width = 843
      Height = 25
      Align = alTop
      Caption = 'Daftar PO :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 26
      Width = 843
      Height = 174
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellClick = cxGrid2DBTableView1CellClick
        DataController.DataSource = DataSourceView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 109
        end
        object cxGrid2DBTableView1namatoko: TcxGridDBColumn
          Caption = 'Supplier'
          DataBinding.FieldName = 'namatoko'
          Options.SortByDisplayText = isbtOn
          Width = 267
        end
        object cxGrid2DBTableView1Supplier: TcxGridDBColumn
          DataBinding.FieldName = 'Supplier'
          Visible = False
        end
        object cxGrid2DBTableView1Termin: TcxGridDBColumn
          DataBinding.FieldName = 'Termin'
          Width = 54
        end
        object cxGrid2DBTableView1TglKirim: TcxGridDBColumn
          DataBinding.FieldName = 'TglKirim'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ShowTime = False
          Width = 103
        end
        object cxGrid2DBTableView1GrandTotal: TcxGridDBColumn
          DataBinding.FieldName = 'GrandTotal'
          Width = 147
        end
        object cxGrid2DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 136
        end
        object cxGrid2DBTableView1CreateDate: TcxGridDBColumn
          DataBinding.FieldName = 'CreateDate'
          Visible = False
        end
        object cxGrid2DBTableView1CreateBy: TcxGridDBColumn
          DataBinding.FieldName = 'CreateBy'
          Visible = False
        end
        object cxGrid2DBTableView1Operator: TcxGridDBColumn
          DataBinding.FieldName = 'Operator'
          Visible = False
        end
        object cxGrid2DBTableView1TglEntry: TcxGridDBColumn
          DataBinding.FieldName = 'TglEntry'
          Visible = False
        end
        object cxGrid2DBTableView1TglCetak: TcxGridDBColumn
          DataBinding.FieldName = 'TglCetak'
          Visible = False
        end
        object cxGrid2DBTableView1Kirim: TcxGridDBColumn
          DataBinding.FieldName = 'Kirim'
          Visible = False
        end
        object cxGrid2DBTableView1Diskon: TcxGridDBColumn
          DataBinding.FieldName = 'Diskon'
          Visible = False
        end
        object cxGrid2DBTableView1Ambil: TcxGridDBColumn
          DataBinding.FieldName = 'Ambil'
          Visible = False
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 297
    Height = 334
    Align = alLeft
    TabOrder = 17
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 295
      Height = 137
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 119
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Supplier: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1SupplierEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Supplier'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1namatoko: TcxDBEditorRow
        Properties.Caption = 'Nama Toko'
        Properties.DataBinding.FieldName = 'namatoko'
        Properties.Options.Editing = False
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Termin: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Termin'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 2
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1TglKirim: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglKirim'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 3
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1Kirim: TcxDBEditorRow
        Properties.Caption = 'Dikirim'
        Properties.DataBinding.FieldName = 'Kirim'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 4
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1Ambil: TcxDBEditorRow
        Properties.Caption = 'Diambil di Toko'
        Properties.DataBinding.FieldName = 'Ambil'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 5
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1Diskon: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Diskon'
        Visible = False
        ID = 6
        ParentID = -1
        Index = 5
        Version = 1
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 138
      Width = 295
      Height = 195
      Align = alClient
      TabOrder = 1
      object Label7: TLabel
        Left = 1
        Top = 1
        Width = 293
        Height = 25
        Align = alTop
        Caption = 'Daftar Beli :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid1: TcxGrid
        Left = 1
        Top = 26
        Width = 293
        Height = 168
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid1DBTableView1CellDblClick
          DataController.DataSource = DataSource2
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsSelection.CellSelect = False
          OptionsSelection.MultiSelect = True
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1Kode: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
            Visible = False
          end
          object cxGrid1DBTableView1Supplier: TcxGridDBColumn
            DataBinding.FieldName = 'Supplier'
            Visible = False
          end
          object cxGrid1DBTableView1namatoko: TcxGridDBColumn
            DataBinding.FieldName = 'namatoko'
            Width = 117
          end
          object cxGrid1DBTableView1BonBarang: TcxGridDBColumn
            DataBinding.FieldName = 'BonBarang'
            Visible = False
            Options.Editing = False
            Width = 83
          end
          object cxGrid1DBTableView1nama: TcxGridDBColumn
            Caption = 'Nama Barang'
            DataBinding.FieldName = 'nama'
            Options.Editing = False
            Width = 106
          end
          object cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn
            Caption = 'Beli'
            DataBinding.FieldName = 'JumlahBeli'
            Options.Editing = False
            Width = 43
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 297
    Top = 41
    Width = 548
    Height = 334
    Align = alClient
    TabOrder = 18
    object Label8: TLabel
      Left = 1
      Top = 1
      Width = 546
      Height = 25
      Align = alTop
      Caption = 'Daftar Barang:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cxGrid3: TcxGrid
      Left = 1
      Top = 26
      Width = 546
      Height = 307
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGridDBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = True
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        OnCellDblClick = cxGridDBTableView1CellDblClick
        DataController.DataSource = DataSourceCoba
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGridDBTableView1KodePO: TcxGridDBColumn
          DataBinding.FieldName = 'KodePO'
          Visible = False
        end
        object cxGridDBTableView1KodeDaftarBeli: TcxGridDBColumn
          Caption = 'Daftar Beli'
          DataBinding.FieldName = 'KodeDaftarBeli'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Visible = False
          Options.Editing = False
          Width = 81
        end
        object cxGridDBTableView1bonbarang: TcxGridDBColumn
          Caption = 'Bon Barang'
          DataBinding.FieldName = 'bonbarang'
          Options.Editing = False
          Width = 89
        end
        object cxGridDBTableView1nama: TcxGridDBColumn
          Caption = 'Nama Barang'
          DataBinding.FieldName = 'nama'
          Options.Editing = False
          Width = 101
        end
        object cxGridDBTableView1jumlahbeli: TcxGridDBColumn
          DataBinding.FieldName = 'jumlahbeli'
          Options.Editing = False
          Width = 73
        end
        object cxGridDBTableView1hargasatuan: TcxGridDBColumn
          DataBinding.FieldName = 'hargasatuan'
          Options.Editing = False
          Width = 115
        end
        object cxGridDBTableView1supplier: TcxGridDBColumn
          DataBinding.FieldName = 'supplier'
          Visible = False
          Options.Editing = False
          Width = 86
        end
        object cxGridDBTableView1grandtotal: TcxGridDBColumn
          Caption = 'Subtotal'
          DataBinding.FieldName = 'grandtotal'
          Options.Editing = False
          Width = 142
        end
        object cxGridDBTableView1temp: TcxGridDBColumn
          DataBinding.FieldName = 'temp'
          Visible = False
          Width = 76
        end
        object cxGridDBTableView1kodebar: TcxGridDBColumn
          DataBinding.FieldName = 'kodebar'
          Visible = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object BtnKanan: TButton
    Left = 273
    Top = 255
    Width = 53
    Height = 33
    Caption = '>>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnClick = BtnKananClick
  end
  object BtnKiri: TButton
    Left = 274
    Top = 295
    Width = 53
    Height = 33
    Caption = '<<'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = BtnKiriClick
  end
  object CobaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    BeforeDelete = CobaQBeforeDelete
    SQL.Strings = (
      
        'select dpo.*,b.nama, db.jumlahbeli, db.hargasatuan, db.supplier,' +
        ' db.grandtotal, 0 as temp, '#39#39' as kodebar, db.bonbarang as bonbar' +
        'ang'
      'from detailpo dpo, daftarbeli db, barang b'
      'where dpo.kodedaftarbeli=db.kode and db.barang=b.kode'
      'and dpo.KodePO=:kodepo'
      '')
    UpdateObject = SDUpdateSQL1
    Left = 536
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'kodepo'
        ParamType = ptInput
        Value = ' '
      end>
    object CobaQKodePO: TStringField
      FieldName = 'KodePO'
      Required = True
      Size = 10
    end
    object CobaQKodeDaftarBeli: TStringField
      FieldName = 'KodeDaftarBeli'
      Required = True
      Size = 10
    end
    object CobaQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object CobaQjumlahbeli: TIntegerField
      FieldName = 'jumlahbeli'
      Required = True
    end
    object CobaQhargasatuan: TCurrencyField
      FieldName = 'hargasatuan'
    end
    object CobaQsupplier: TStringField
      FieldName = 'supplier'
      Size = 10
    end
    object CobaQgrandtotal: TCurrencyField
      FieldName = 'grandtotal'
      Required = True
    end
    object CobaQtemp: TIntegerField
      FieldName = 'temp'
      Required = True
    end
    object CobaQkodebar: TStringField
      FieldName = 'kodebar'
      Size = 10
    end
    object CobaQbonbarang: TStringField
      FieldName = 'bonbarang'
      Size = 10
    end
    object CobaQStatusPO: TStringField
      FieldName = 'StatusPO'
      Size = 50
    end
  end
  object DataSourceCoba: TDataSource
    DataSet = CobaQ
    Left = 464
    Top = 112
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodePO, KodeDaftarBeli, StatusPO'
      'from detailpo'
      'where'
      '  KodePO = :OLD_KodePO and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    ModifySQL.Strings = (
      'update detailpo'
      'set'
      '  KodePO = :KodePO,'
      '  KodeDaftarBeli = :KodeDaftarBeli,'
      '  StatusPO = :StatusPO'
      'where'
      '  KodePO = :OLD_KodePO and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    InsertSQL.Strings = (
      'insert into detailpo'
      '  (KodePO, KodeDaftarBeli, StatusPO)'
      'values'
      '  (:KodePO, :KodeDaftarBeli, :StatusPO)')
    DeleteSQL.Strings = (
      'delete from detailpo'
      'where'
      '  KodePO = :OLD_KodePO and'
      '  KodeDaftarBeli = :OLD_KodeDaftarBeli')
    Left = 504
    Top = 120
  end
  object UpdateHeaderPO: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Supplier, Termin, TglKirim, GrandTotal, Status, Cre' +
        'ateDate, CreateBy, Operator, TglEntry, TglCetak, Diskon, Kirim, ' +
        'Ambil'
      'from po'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update po'
      'set'
      '  Kode = :Kode,'
      '  Supplier = :Supplier,'
      '  Termin = :Termin,'
      '  TglKirim = :TglKirim,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  StatusKirim = :StatusKirim,'
      '  Diskon = :Diskon,'
      '  Kirim = :Kirim,'
      '  Ambil = :Ambil'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into po'
      
        '  (Kode, Supplier, Termin, TglKirim, GrandTotal, Status, CreateD' +
        'ate, CreateBy, Operator, TglEntry, TglCetak, StatusKirim, Diskon' +
        ', Kirim, Ambil)'
      'values'
      
        '  (:Kode, :Supplier, :Termin, :TglKirim, :GrandTotal, :Status, :' +
        'CreateDate, :CreateBy, :Operator, :TglEntry, :TglCetak, :StatusK' +
        'irim, :Diskon, :Kirim, :Ambil)')
    DeleteSQL.Strings = (
      'delete from po'
      'where'
      '  Kode = :OLD_Kode')
    Left = 176
    Top = 464
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select top 1 kode from po order by kode desc')
    Left = 136
    Top = 88
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = HeaderQ
    Left = 216
    Top = 448
  end
  object HeaderQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select po.*,s.namatoko from po, supplier s where po.supplier=s.k' +
        'ode'
      'and po.Kode= :kodepo')
    UpdateObject = UpdateHeaderPO
    Left = 168
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'kodepo'
        ParamType = ptInput
      end>
    object HeaderQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HeaderQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object HeaderQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object HeaderQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object HeaderQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object HeaderQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object HeaderQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object HeaderQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object HeaderQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object HeaderQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object HeaderQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object HeaderQnamatoko: TStringField
      FieldName = 'namatoko'
      Required = True
      Size = 50
    end
    object HeaderQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object HeaderQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object HeaderQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
    object HeaderQStatusKirim: TStringField
      FieldName = 'StatusKirim'
      Size = 50
    end
  end
  object DaftarBeliQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select db.*,b.nama, s.namatoko from daftarbeli db, barang b, sup' +
        'plier s'
      'where db.barang=b.kode and s.kode=db.supplier'
      'and db.status='#39'ON PROCESS'#39' and db.cashncarry=0'
      'order by db.bonbarang desc')
    UpdateObject = UpdateDaftarBeli
    Left = 40
    Top = 248
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DaftarBeliQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object DaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
    object DaftarBeliQnamatoko: TStringField
      FieldName = 'namatoko'
      Required = True
      Size = 50
    end
  end
  object JumlahQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select sum(grandtotal) as total'
      'from detailpo dpo, daftarbeli db '
      'where dpo.kodedaftarbeli=db.kode'
      'and dpo.kodepo=:kodepo')
    Left = 552
    Top = 472
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kodepo'
        ParamType = ptInput
      end>
    object JumlahQtotal: TCurrencyField
      FieldName = 'total'
    end
  end
  object DataSource2: TDataSource
    DataSet = DaftarBeliQ
    Left = 80
    Top = 248
  end
  object UpdateDaftarBeli: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, L' +
        'astSupplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, Gran' +
        'dTotal, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplie' +
        'r2, Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ke' +
        'terangan3, Termin'
      'from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update daftarbeli'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  BonBarang = :BonBarang,'
      '  HargaMin = :HargaMin,'
      '  HargaMax = :HargaMax,'
      '  HargaLast = :HargaLast,'
      '  LastSupplier = :LastSupplier,'
      '  JumlahBeli = :JumlahBeli,'
      '  HargaSatuan = :HargaSatuan,'
      '  Supplier = :Supplier,'
      '  CashNCarry = :CashNCarry,'
      '  GrandTotal = :GrandTotal,'
      '  Status = :Status,'
      '  Supplier1 = :Supplier1,'
      '  Harga1 = :Harga1,'
      '  Termin1 = :Termin1,'
      '  Keterangan1 = :Keterangan1,'
      '  Supplier2 = :Supplier2,'
      '  Harga2 = :Harga2,'
      '  Termin2 = :Termin2,'
      '  Keterangan2 = :Keterangan2,'
      '  Supplier3 = :Supplier3,'
      '  Harga3 = :Harga3,'
      '  Termin3 = :Termin3,'
      '  Keterangan3 = :Keterangan3,'
      '  Termin = :Termin'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into daftarbeli'
      
        '  (Kode, Barang, BonBarang, HargaMin, HargaMax, HargaLast, LastS' +
        'upplier, JumlahBeli, HargaSatuan, Supplier, CashNCarry, GrandTot' +
        'al, Status, Supplier1, Harga1, Termin1, Keterangan1, Supplier2, ' +
        'Harga2, Termin2, Keterangan2, Supplier3, Harga3, Termin3, Ketera' +
        'ngan3, Termin)'
      'values'
      
        '  (:Kode, :Barang, :BonBarang, :HargaMin, :HargaMax, :HargaLast,' +
        ' :LastSupplier, :JumlahBeli, :HargaSatuan, :Supplier, :CashNCarr' +
        'y, :GrandTotal, :Status, :Supplier1, :Harga1, :Termin1, :Keteran' +
        'gan1, :Supplier2, :Harga2, :Termin2, :Keterangan2, :Supplier3, :' +
        'Harga3, :Termin3, :Keterangan3, :Termin)')
    DeleteSQL.Strings = (
      'delete from daftarbeli'
      'where'
      '  Kode = :OLD_Kode')
    Left = 128
    Top = 248
  end
  object SembarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 568
    Top = 112
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 2
    Version.Windows.Build = '9200'
    TempPath = 'C:\Users\intan\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    PrintOptions.StartPage = 1
    PrintOptions.StopPage = 65535
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcMagnify
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcMagnify
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcMagnify
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 240
    Top = 448
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select po.* , s.namatoko'
      'from po , supplier s'
      'where po.supplier=s.kode and po.status<>'#39'FINISHED'#39
      'order by kode desc')
    Left = 128
    Top = 472
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object ViewQTermin: TStringField
      FieldName = 'Termin'
      Required = True
      Size = 50
    end
    object ViewQTglKirim: TDateTimeField
      FieldName = 'TglKirim'
      Required = True
    end
    object ViewQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewQKirim: TBooleanField
      FieldName = 'Kirim'
    end
    object ViewQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object ViewQAmbil: TBooleanField
      FieldName = 'Ambil'
    end
    object ViewQnamatoko: TStringField
      FieldName = 'namatoko'
      Required = True
      Size = 50
    end
  end
  object DataSourceView: TDataSource
    DataSet = ViewQ
    Left = 336
    Top = 472
  end
end
