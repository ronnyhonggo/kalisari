unit PO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  SDEngine, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxButtonEdit,
  cxVGrid, cxDBVGrid, cxInplaceContainer, StdCtrls, cxContainer, Menus,
  cxButtons, cxTextEdit, cxMaskEdit, ComCtrls, cxDropDownEdit, ExtCtrls,
  cxCheckBox, UCrpeClasses, UCrpe32, Buttons, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxLabel, cxGroupBox;

type
  TPOFm = class(TForm)
    CobaQ: TSDQuery;
    DataSourceCoba: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    UpdateHeaderPO: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    DataSource1: TDataSource;
    HeaderQ: TSDQuery;
    CobaQKodePO: TStringField;
    CobaQKodeDaftarBeli: TStringField;
    CobaQnama: TStringField;
    CobaQjumlahbeli: TIntegerField;
    CobaQhargasatuan: TCurrencyField;
    CobaQsupplier: TStringField;
    CobaQgrandtotal: TCurrencyField;
    DaftarBeliQ: TSDQuery;
    JumlahQ: TSDQuery;
    JumlahQtotal: TCurrencyField;
    CobaQtemp: TIntegerField;
    DataSource2: TDataSource;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQnama: TStringField;
    UpdateDaftarBeli: TSDUpdateSQL;
    CobaQkodebar: TStringField;
    CobaQbonbarang: TStringField;
    SembarangQ: TSDQuery;
    Crpe1: TCrpe;
    HeaderQKode: TStringField;
    HeaderQSupplier: TStringField;
    HeaderQTermin: TStringField;
    HeaderQTglKirim: TDateTimeField;
    HeaderQGrandTotal: TCurrencyField;
    HeaderQStatus: TStringField;
    HeaderQCreateDate: TDateTimeField;
    HeaderQCreateBy: TStringField;
    HeaderQOperator: TStringField;
    HeaderQTglEntry: TDateTimeField;
    HeaderQTglCetak: TDateTimeField;
    HeaderQnamatoko: TStringField;
    HeaderQKirim: TBooleanField;
    HeaderQDiskon: TCurrencyField;
    HeaderQAmbil: TBooleanField;
    ViewQ: TSDQuery;
    ViewQKode: TStringField;
    ViewQSupplier: TStringField;
    ViewQTermin: TStringField;
    ViewQTglKirim: TDateTimeField;
    ViewQGrandTotal: TCurrencyField;
    ViewQStatus: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQTglCetak: TDateTimeField;
    ViewQKirim: TBooleanField;
    ViewQDiskon: TCurrencyField;
    ViewQAmbil: TBooleanField;
    DataSourceView: TDataSource;
    ViewQnamatoko: TStringField;
    DaftarBeliQTermin: TIntegerField;
    DaftarBeliQnamatoko: TStringField;
    StatusBar: TStatusBar;
    SearchBtn: TcxButton;
    cxButtonEdit1: TcxButtonEdit;
    pnl1: TPanel;
    Label1: TLabel;
    KodeEdit: TcxButtonEdit;
    cxButton1: TcxButton;
    BtnKanan: TButton;
    BtnKiri: TButton;
    Button1: TButton;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    BtnSave: TButton;
    BitBtn1: TBitBtn;
    Button4: TButton;
    TxtRupiah: TcxCurrencyEdit;
    ListBox1: TListBox;
    BtnDelete: TButton;
    LblGrandTotal: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    SetujuBtn: TcxButton;
    HeaderQStatusKirim: TStringField;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1namatoko: TcxGridDBColumn;
    cxGrid2DBTableView1Supplier: TcxGridDBColumn;
    cxGrid2DBTableView1Termin: TcxGridDBColumn;
    cxGrid2DBTableView1TglKirim: TcxGridDBColumn;
    cxGrid2DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    cxGrid2DBTableView1CreateDate: TcxGridDBColumn;
    cxGrid2DBTableView1CreateBy: TcxGridDBColumn;
    cxGrid2DBTableView1Operator: TcxGridDBColumn;
    cxGrid2DBTableView1TglEntry: TcxGridDBColumn;
    cxGrid2DBTableView1TglCetak: TcxGridDBColumn;
    cxGrid2DBTableView1Kirim: TcxGridDBColumn;
    cxGrid2DBTableView1Diskon: TcxGridDBColumn;
    cxGrid2DBTableView1Ambil: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    Label6: TLabel;
    Panel2: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1Supplier: TcxDBEditorRow;
    cxDBVerticalGrid1namatoko: TcxDBEditorRow;
    cxDBVerticalGrid1Termin: TcxDBEditorRow;
    cxDBVerticalGrid1TglKirim: TcxDBEditorRow;
    cxDBVerticalGrid1Kirim: TcxDBEditorRow;
    cxDBVerticalGrid1Ambil: TcxDBEditorRow;
    cxDBVerticalGrid1Diskon: TcxDBEditorRow;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Supplier: TcxGridDBColumn;
    cxGrid1DBTableView1namatoko: TcxGridDBColumn;
    cxGrid1DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1nama: TcxGridDBColumn;
    cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Label7: TLabel;
    Panel4: TPanel;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1KodePO: TcxGridDBColumn;
    cxGridDBTableView1KodeDaftarBeli: TcxGridDBColumn;
    cxGridDBTableView1bonbarang: TcxGridDBColumn;
    cxGridDBTableView1nama: TcxGridDBColumn;
    cxGridDBTableView1jumlahbeli: TcxGridDBColumn;
    cxGridDBTableView1hargasatuan: TcxGridDBColumn;
    cxGridDBTableView1supplier: TcxGridDBColumn;
    cxGridDBTableView1grandtotal: TcxGridDBColumn;
    cxGridDBTableView1temp: TcxGridDBColumn;
    cxGridDBTableView1kodebar: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Label8: TLabel;
    CobaQStatusPO: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxButHPOFinished: TcxButton;
    cxButReset: TcxButton;
    procedure SearchBtnClick(Sender: TObject);
    procedure KodeEditPropertiesChange(Sender: TObject);
    procedure cxGridDBTableView1namaPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure CobaQBeforeDelete(DataSet: TDataSet);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure BtnKananClick(Sender: TObject);
    procedure BtnKiriClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure cxGrid2DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnDeleteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxButResetClick(Sender: TObject);
    procedure cxButHPOFinishedClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    print:Boolean;
  end;

var
  POFm: TPOFm;
  grandtotal:Integer;
  ctr:integer;
  HeaderOriSQL: string;
  ViewOriSQL:string;

implementation
uses MenuUtama,browse,BrowseName, StrUtils, DropDown, SupplierDropDown;

{$R *.dfm}

procedure TPOFm.SearchBtnClick(Sender: TObject);
begin
  LblGrandTotal.Caption:='0';
  TxtRupiah.Value:=0;

 { MenuUtamaFm.param_sql:='select po.kode as kode_po, s.namatoko ,po.status from po, supplier s where s.kode=po.supplier order by po.kode desc';
  BrowseFm.ShowModal;
  KodeEdit.Text:= MenuUtamaFm.param_kodebon;}
  KodeEdit.Text:=ViewQKode.AsString;

   CobaQ.Close;
  CobaQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Edit;

  while not CobaQ.Eof do
  begin
      CobaQ.Edit;
      CobaQtemp.AsInteger:= CobaQgrandtotal.AsInteger;
      CobaQ.Post;
      CobaQ.Next;
  end;
  //CobaQ.Append;
  //viewQ.Refresh;
  HeaderQ.Close;
  HeaderQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;

  DaftarBeliQ.Close;
DaftarBeliQ.SQL.Clear;
DaftarBeliQ.SQL.Add('select db.*,b.nama, s.namatoko from daftarbeli db, barang b, supplier s');
DaftarBeliQ.SQL.Add('where db.barang=b.kode and s.kode=db.supplier');
DaftarBeliQ.SQL.Add('and db.status='+QuotedStr('ONPROCESS')+' and db.cashncarry=0');
DaftarBeliQ.SQL.Add('and db.supplier='+QuotedStr(HeaderQSupplier.AsString));
DaftarBeliQ.SQL.Add('order by db.bonbarang desc');
DaftarBeliQ.ExecSQL;

 { DaftarBeliQ.Close;
  DaftarBeliQ.ParamByName('kodesup').AsString := HeaderQSupplier.AsString;
  DaftarBeliQ.ExecSQL;}
  DaftarBeliQ.Open;
   DaftarBeliQ.Edit;



  LblGrandTotal.Caption:=IntToStr(HeaderQGrandTotal.AsInteger);
  TxtRupiah.Value:= HeaderQGrandTotal.AsInteger;
  grandtotal:=HeaderQGrandTotal.AsInteger;

end;

procedure TPOFm.KodeEditPropertiesChange(Sender: TObject);
begin
{  BitBtn1.Enabled:=false;
//  KodeEdit.Text:='Insert Baru';
  CobaQ.Close;
  CobaQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Insert;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;
  LblGrandTotal.Caption:='0';
  TxtRupiah.Value:=0;
  grandtotal:=0;

  cxDBVerticalGrid1.Enabled:=false;
  cxGrid3.Enabled:=false;
  cxGrid1.Enabled:=false;
  BitBtn1.Enabled:=false;
  BtnKanan.Enabled:=false;
  BtnKiri.Enabled:=false;
  BtnDelete.Enabled:=false;
  BtnSave.Enabled:=false;

  if MenuUtamaFm.UserQUpdatePO.AsBoolean=true then
  begin
    cxDBVerticalGrid1.Enabled:=true;
    cxGrid3.Enabled:=true;
    cxGrid1.Enabled:=true;
    BtnKanan.Enabled:=true;
    BtnKiri.Enabled:=true;
    BtnSave.Enabled:=true;
 // cxDBVerticalGrid1.SetFocus;
  end;
  if MenuUtamaFm.UserQDeletePO.AsBoolean=true then
  begin
    BtnDelete.Enabled:=true;
  end;
  if MenuUtamaFm.UserQApprovalPO.AsBoolean=true then
  begin
    BitBtn1.Enabled:=true;
  end;
 }
end;

procedure TPOFm.cxGridDBTableView1namaPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
BrowseNameFm.ShowModal;
CobaQ.Edit;
CobaQ.Fields[16].AsString:=MenuUtamaFm.param_nama;
CobaQ.Fields[4].AsString:=MenuUtamaFm.param_kode;
end;

procedure TPOFm.BtnSaveClick(Sender: TObject);
var kodebon:String;
i:integer;
tdkcukup:boolean;
begin
      if (KodeEdit.Text='INSERT BARU') or (KodeEdit.Text='') then
      begin
        try
          KodeQ.Open;
          if KodeQ.IsEmpty then
          begin
            HeaderQKode.AsString:=FormatFloat('0000000000',1);
          end
          else
          begin

            HeaderQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+1);
          end;
          KodeQ.Close;

          HeaderQGrandTotal.AsInteger:=grandtotal;
          if HeaderQStatus.IsNull then
            HeaderQStatus.AsString:='NEW PO';

          kodebon:=HeaderQKode.AsString;
         // ShowMessage(kodebon);
          HeaderQ.Edit;
          HeaderQ.Post;

          //ubah kodedetail di detail==================================
          CobaQ.First;
          while not CobaQ.Eof do
          begin
             CobaQ.Edit;
             CobaQKodePO.AsString:=HeaderQKode.AsString;
             CobaQ.Post;

            CobaQ.Next;
          end;//===========================================================


            except
            on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
        end
      end;
      HeaderQ.Edit;
      HeaderQGrandTotal.AsInteger:=grandtotal;

      //PENGECEKKAN LAGI JIKA SAAT EDIT ADA INSERT BARU
      CobaQ.First;
      while not CobaQ.Eof do
      begin
          if CobaQKodePO.AsString='tes' then
          begin
             CobaQ.Edit;
             CobaQKodePO.AsString:=HeaderQKode.AsString;
             CobaQ.Post;
          end;
        CobaQ.Next;
      end;
      //=========================================================

      MenuUtamaFm.DataBase1.StartTransaction;
      try
        HeaderQ.ApplyUpdates;
        MenuUtamaFm.DataBase1.Commit;
        HeaderQ.CommitUpdates;

        MenuUtamaFm.DataBase1.StartTransaction;
        try
          CobaQ.ApplyUpdates;
          MenuUtamaFm.DataBase1.Commit;
          CobaQ.CommitUpdates;

          CobaQ.First;
          while not CobaQ.Eof do
          begin
            SembarangQ.Close;
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update daftarbeli set status ='+QuotedStr('PO')+' where kode='+QuotedStr(CobaQKodeDaftarBeli.AsString));
            SembarangQ.ExecSQL;
            CobaQ.Next;
           end;

          ViewQ.Refresh;

         if MessageDlg('PO dengan kode '+ HeaderQKode.AsString +' telah disimpan. Cetak PO ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
          begin
             SembarangQ.SQL.Clear;
             SembarangQ.SQL.Add('update po set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(HeaderQKode.AsString));
             SembarangQ.ExecSQL;

            Crpe1.Refresh;
            Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'PO.rpt';
            Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
            KodeEdit.Text:='';
            Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
              Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
              Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);


            Crpe1.Execute;
            Crpe1.WindowState:= wsMaximized;
          end;
          KodeEdit.Text:='';
           HeaderQ.Edit;
          HeaderQTglKirim.AsDateTime:=now;

          DaftarBeliQ.First;
          while not DaftarBeliQ.Eof do
          begin
              SembarangQ.SQL.Clear;
              SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ONPROCESS')+' where kode='+QuotedStr(DaftarBeliQKode.AsString));
              SembarangQ.ExecSQL;

              DaftarBeliQ.Next;
          end;
          KodeEditPropertiesButtonClick(sender,1);

        except
          MenuUtamaFm.DataBase1.Rollback;
          HeaderQ.RollbackUpdates;
          CobaQ.RollbackUpdates;
          ShowMessage('Penyimpanan detail Gagal, silahkan coba kembali');
          KodeEditPropertiesButtonClick(sender,1);

        end;

      except
        MenuUtamaFm.DataBase1.Rollback;
        HeaderQ.RollbackUpdates;
        ShowMessage('Penyimpanan header Gagal, silahkan coba kembali');
        KodeEditPropertiesButtonClick(sender,1);
      end;

end;

procedure TPOFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
var i,t1,t2,hasil:integer;
begin
 try
    grandtotal:=CobaQtemp.AsInteger;
    LblGrandTotal.Caption:=IntToStr(grandtotal);
    TxtRupiah.Value:=grandtotal;
    HeaderQGrandTotal.AsInteger:= grandtotal;

    for i:=0 to cxGridDBTableView1.DataController.RecordCount do
    begin
      grandtotal:=grandtotal+StrToInt(cxGridDBTableView1.ViewData.Rows[i].DisplayTexts[8]);
      LblGrandTotal.Caption:=IntToStr(grandtotal);
      TxtRupiah.Value:=grandtotal;
      HeaderQGrandTotal.AsInteger:= grandtotal;
    end;
    LblGrandTotal.Caption:=IntToStr(grandtotal);
    TxtRupiah.Value:=grandtotal;


  except
     //on E : Exception do
       //       ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
  end;
end;

procedure TPOFm.FormCreate(Sender: TObject);
begin

  KodeEdit.Text:='';
  CobaQ.Close;
  CobaQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  CobaQ.ExecSQL;
  CobaQ.Open;
  CobaQ.Edit;
  CobaQ.Append;


  HeaderQ.Close;
  HeaderQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  HeaderQ.ExecSQL;
  HeaderQ.Open;
  HeaderQ.Edit;
  HeaderQ.Append;
  HeaderQTglKirim.AsDateTime:=now;
  HeaderOriSQL:=HeaderQ.SQL.Text;

  DaftarBeliQ.Close;
  DaftarBeliQ.Open;
  KodeEdit.Properties.MaxLength:=HeaderQKode.Size;

  ViewOriSQL:=ViewQ.SQL.Text;
end;

procedure TPOFm.cxDBVerticalGrid1SupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin

  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self,'po');
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      HeaderQ.Open;
      HeaderQ.Edit;
      HeaderQSupplier.AsString:=SupplierDropDownFm.kode;
      HeaderQnamatoko.AsString:=SupplierDropDownFm.nama;
    end;
  SupplierDropDownFm.Release;
{
MenuUtamaFm.param_sql:='SELECT distinct s.kode, s.namatoko, s.alamat, s.notelp, s.kategori, s.namapic1 FROM daftarbeli db, supplier s where db.status='+QuotedStr('ONPROCESS')+' and db.supplier=s.kode and  db.CashNCarry=0 and db.supplier<>'+QuotedStr('0000000000')+' order by s.namatoko';
BrowseFm.ShowModal;

HeaderQ.Open;
HeaderQ.Edit;
HeaderQSupplier.AsString:=BrowseFm.kode;
HeaderQnamatoko.AsString:=BrowseFm.nama; }
HeaderQKirim.AsBoolean:=true;
HeaderQAmbil.AsBoolean:=false;
//HeaderQTglKirim.AsDateTime:=now;

DaftarBeliQ.Close;
DaftarBeliQ.SQL.Clear;
DaftarBeliQ.SQL.Add('select db.*,b.nama, s.namatoko from daftarbeli db, barang b, supplier s');
DaftarBeliQ.SQL.Add('where db.barang=b.kode and s.kode=db.supplier');
DaftarBeliQ.SQL.Add('and db.status='+QuotedStr('ON PROCESS')+' and db.cashncarry=0');
DaftarBeliQ.SQL.Add('and db.supplier='+QuotedStr(HeaderQSupplier.AsString));
DaftarBeliQ.SQL.Add('order by db.bonbarang desc');
DaftarBeliQ.ExecSQL;

//DaftarBeliQ.ParamByName('kodesup').AsString := HeaderQSupplier.AsString;
DaftarBeliQ.ExecSQL;
DaftarBeliQ.Open;
HeaderQTermin.AsInteger:=DaftarBeliQTermin.AsInteger;
DaftarBeliQ.Edit;


CobaQ.Close;
CobaQ.ParamByName('kodepo').AsString := KodeEdit.Text;
CobaQ.ExecSQL;
CobaQ.Open;
CobaQ.Edit;
CobaQ.Append;

grandtotal:=0;

end;



procedure TPOFm.CobaQBeforeDelete(DataSet: TDataSet);
var i:integer;
begin

end;

procedure TPOFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if HeaderQSupplier.IsNull then
  ShowMessage('Pilih Supplier terlebih dahulu')
  else
  begin
      CobaQ.Insert;
       CobaQ.Append;
       CobaQKodeDaftarBeli.AsString:=DaftarBeliQKode.AsString;
    //   CobaQKodeLPB.AsString:=HeaderQKode.AsString;
       CobaQnama.AsString:=DaftarBeliQnama.AsString;
       CobaQjumlahbeli.AsInteger:=DaftarBeliQJumlahBeli.AsInteger;
      CobaQhargasatuan.AsInteger:=DaftarBeliQHargaSatuan.AsInteger;
      CobaQsupplier.AsString:=DaftarBeliQSupplier.AsString;
      CobaQgrandtotal.AsString:=DaftarBeliQGrandTotal.AsString;
      CobaQKodePO.AsString:='tes';
      CobaQtemp.AsInteger:= DaftarBeliQGrandTotal.AsInteger;
      CobaQkodebar.AsString:=DaftarBeliQBarang.AsString;
      CobaQbonbarang.AsString:=DaftarBeliQBonBarang.AsString;
      CobaQStatusPO.AsString:='ON PROCESS';

       CobaQ.Post;

       DaftarBeliQ.Delete;

       grandtotal:=grandtotal+CobaQtemp.AsInteger;
       LblGrandTotal.Caption:=IntToStr(grandtotal);
       TxtRupiah.Value:=grandtotal;
       SaveBtn.Enabled:=menuutamafm.UserQUpdatePO.AsBoolean;
   end;
end;

procedure TPOFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  DaftarBeliQ.Open;
  DaftarBeliQ.Insert;
   DaftarBeliQnama.AsString:=CobaQnama.AsString;
   DaftarBeliQJumlahBeli.AsInteger:=CobaQjumlahbeli.AsInteger;
  DaftarBeliQHargaSatuan.AsInteger:=CobaQhargasatuan.AsInteger;
  DaftarBeliQSupplier.AsString:=CobaQsupplier.AsString;
  DaftarBeliQGrandTotal.AsString:=CobaQgrandtotal.AsString;
  DaftarBeliQGrandTotal.AsInteger:= CobaQtemp.AsInteger;
  DaftarBeliQnamatoko.AsString:=HeaderQnamatoko.AsString;

  DaftarBeliQKode.AsString:=CobaQKodeDaftarBeli.AsString;
  DaftarBeliQBarang.AsString:=CobaQkodebar.AsString;
  DaftarBeliQBonBarang.AsString:=CobaQbonbarang.AsString;
  DaftarBeliQStatus.AsString:='ON PROCESS';
   DaftarBeliQ.Post;

    grandtotal:=grandtotal-CobaQtemp.AsInteger;
    LblGrandTotal.Caption:=IntToStr(grandtotal);
    TxtRupiah.Value:=grandtotal;
   CobaQ.Delete;
   SaveBtn.Enabled:=menuutamafm.UserQUpdatePO.AsBoolean;
end;

procedure TPOFm.BtnKananClick(Sender: TObject);
var i:integer;
ada:Boolean;
begin
if HeaderQSupplier.IsNull then
  ShowMessage('Pilih Supplier terlebih dahulu')
  else
  begin

   ListBox1.Items.Clear;
    for i:=1 to cxGrid1DBTableView1.Controller.SelectedRecordCount do
    begin
        ListBox1.Items.Add(cxGrid1DBTableView1.Controller.SelectedRecords[i-1].Values[0]);
    end;

  DaftarBeliQ.First;
    while not DaftarBeliQ.Eof do
    begin
        for i:=0 to ListBox1.Items.Count-1 do
        begin
            ada:=false;
            if ListBox1.Items[i] = DaftarBeliQKode.AsString then
            begin
                ada:=true;
                CobaQ.Insert;
                  CobaQ.Append;
                 CobaQKodeDaftarBeli.AsString:=DaftarBeliQKode.AsString;
                 CobaQnama.AsString:=DaftarBeliQnama.AsString;
                 CobaQjumlahbeli.AsInteger:=DaftarBeliQJumlahBeli.AsInteger;
                CobaQhargasatuan.AsInteger:=DaftarBeliQHargaSatuan.AsInteger;
                CobaQsupplier.AsString:=DaftarBeliQSupplier.AsString;
                CobaQgrandtotal.AsString:=DaftarBeliQGrandTotal.AsString;
                CobaQKodePO.AsString:='tes';
                CobaQtemp.AsInteger:= DaftarBeliQGrandTotal.AsInteger;
                CobaQkodebar.AsString:=DaftarBeliQBarang.AsString;
                CobaQbonbarang.AsString:=DaftarBeliQBonBarang.AsString;
                CobaQStatusPO.AsString:='ON PROCESS';
                 CobaQ.Post;


                 grandtotal:=grandtotal+CobaQtemp.AsInteger;
                 //ShowMessage(IntToStr(grandtotal));
                 LblGrandTotal.Caption:=IntToStr(grandtotal);
                 TxtRupiah.Value:=grandtotal;

                  DaftarBeliQ.Delete;
            end;
        end;

        if ada=false then
          DaftarBeliQ.Next;
    end;
  end;
end;

procedure TPOFm.BtnKiriClick(Sender: TObject);
var i:integer;
ada:boolean;
begin
  ListBox1.Items.Clear;
    for i:=1 to cxGridDBTableView1.Controller.SelectedRecordCount do
    begin
        ListBox1.Items.Add(cxGridDBTableView1.Controller.SelectedRecords[i-1].Values[1]);
    end;

    CobaQ.First;
    while not CobaQ.Eof do
    begin
        for i:=0 to ListBox1.Items.Count-1 do
        begin
            ada:=false;
            if ListBox1.Items[i] = CobaQKodeDaftarBeli.AsString then
            begin
                ada:=true;
                DaftarBeliQ.Open;
                DaftarBeliQ.Insert;
                 DaftarBeliQnama.AsString:=CobaQnama.AsString;
                 DaftarBeliQJumlahBeli.AsInteger:=CobaQjumlahbeli.AsInteger;
                DaftarBeliQHargaSatuan.AsInteger:=CobaQhargasatuan.AsInteger;
                DaftarBeliQSupplier.AsString:=CobaQsupplier.AsString;
                DaftarBeliQGrandTotal.AsString:=CobaQgrandtotal.AsString;
                DaftarBeliQGrandTotal.AsInteger:= CobaQtemp.AsInteger;
                DaftarBeliQnamatoko.AsString:=HeaderQnamatoko.AsString;

                DaftarBeliQKode.AsString:=CobaQKodeDaftarBeli.AsString;
                DaftarBeliQBarang.AsString:=CobaQkodebar.AsString;
                DaftarBeliQBonBarang.AsString:=CobaQbonbarang.AsString;
                DaftarBeliQStatus.AsString:='ON PROCESS';
                 DaftarBeliQ.Post;

                  grandtotal:=grandtotal-CobaQtemp.AsInteger;
                  LblGrandTotal.Caption:=IntToStr(grandtotal);
                  TxtRupiah.Value:=grandtotal;

                 CobaQ.Delete;
            end;
        end;

        if ada=false then
          CobaQ.Next;
    end;

end;

procedure TPOFm.BitBtn1Click(Sender: TObject);
begin
HeaderQ.Edit;
HeaderQStatus.AsString:='ON PROCESS';
BtnSaveClick(sender);
end;

procedure TPOFm.cxGrid2DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
SearchBtnClick(sender);
grandtotal:=HeaderQGrandTotal.AsInteger;

cxDBVerticalGrid1.Enabled:=false;
cxGrid3.Enabled:=false;
cxGrid1.Enabled:=false;
BitBtn1.Enabled:=false;
BtnKanan.Enabled:=false;
BtnKiri.Enabled:=false;
BtnDelete.Enabled:=false;
BtnSave.Enabled:=false;

  DaftarBeliQ.Close;
  DaftarBeliQ.SQL.Clear;
  DaftarBeliQ.SQL.Add('select db.*,b.nama, s.namatoko from daftarbeli db, barang b, supplier s');
  DaftarBeliQ.SQL.Add('where db.barang=b.kode and s.kode=db.supplier');
  DaftarBeliQ.SQL.Add('and db.status='+QuotedStr('ON PROCESS')+' and db.cashncarry=0');
  DaftarBeliQ.SQL.Add('and db.supplier='+QuotedStr(HeaderQSupplier.AsString));
  DaftarBeliQ.SQL.Add('order by db.bonbarang desc');
  DaftarBeliQ.ExecSQL;
  DaftarBeliQ.Open;

if MenuUtamaFm.UserQUpdatePO.AsBoolean=true then
begin
  cxDBVerticalGrid1.Enabled:=true;
  cxGrid3.Enabled:=true;
  cxGrid1.Enabled:=true;
  BtnKanan.Enabled:=true;
  BtnKiri.Enabled:=true;
  BtnSave.Enabled:=true;
end
else
begin
   cxDBVerticalGrid1.Enabled:=false;
  cxGrid3.Enabled:=false;
  cxGrid1.Enabled:=false;
  BtnKanan.Enabled:=false;
  BtnKiri.Enabled:=false;
  BtnSave.Enabled:=false;
end;
if MenuUtamaFm.UserQDeletePO.AsBoolean=true then
begin
  BtnDelete.Enabled:=true;
end;
if MenuUtamaFm.UserQApprovalPO.AsBoolean=true then
begin
  BitBtn1.Enabled:=true;
end;

end;

procedure TPOFm.Button4Click(Sender: TObject);
begin
ViewQ.Refresh;
end;

procedure TPOFm.FormActivate(Sender: TObject);
begin
ViewQ.Close;
ViewQ.Open;
ViewQ.Refresh;

DaftarBeliQ.Refresh;
HeaderQ.Close;
CobaQ.Close;

cxDBVerticalGrid1.Enabled:=false;
cxGrid3.Enabled:=false;
cxGrid1.Enabled:=false;
BitBtn1.Enabled:=false;
BtnKanan.Enabled:=false;
BtnKiri.Enabled:=false;
BtnDelete.Enabled:=false;
BtnSave.Enabled:=false;

if MenuUtamaFm.UserQInsertPO.AsBoolean=true then
begin
  cxDBVerticalGrid1.Enabled:=true;
  cxGrid3.Enabled:=true;
  cxGrid1.Enabled:=true;
  BtnKanan.Enabled:=true;
  BtnKiri.Enabled:=true;

  if MenuUtamaFm.UserQApprovalPO.AsBoolean=true then
    BitBtn1.Enabled:=true
    else
    BtnSave.Enabled:=true;
end;


end;

procedure TPOFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
 KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  HeaderQ.Close;
  CobaQ.Close;

  CobaQ.Close;
  CobaQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  CobaQ.Open;
  CobaQ.Insert;
  CobaQ.Append;

  HeaderQ.Close;
  HeaderQ.ParamByName('kodepo').AsString := KodeEdit.Text;
  HeaderQ.Open;
  HeaderQ.Append;
  HeaderQTglKirim.AsDateTime:=now;

  ViewQ.Refresh;

  cxDBVerticalGrid1.Enabled:=false;
  cxGrid3.Enabled:=false;
  cxGrid1.Enabled:=false;
  BitBtn1.Enabled:=false;
  BtnKanan.Enabled:=false;
  BtnKiri.Enabled:=false;
  BtnDelete.Enabled:=false;
  BtnSave.Enabled:=false;

  if MenuUtamaFm.UserQInsertPO.AsBoolean=true then
  begin
    cxDBVerticalGrid1.Enabled:=true;
    cxGrid3.Enabled:=true;
    cxGrid1.Enabled:=true;
    BtnKanan.Enabled:=true;
    BtnKiri.Enabled:=true;
   cxDBVerticalGrid1.SetFocus;
    if MenuUtamaFm.UserQApprovalPO.AsBoolean=true then
      BitBtn1.Enabled:=true
      else
      BtnSave.Enabled:=true;
  end;

  DaftarBeliQ.close;
  DaftarBeliQ.SQL.clear;
  DaftarBeliQ.SQL.Add('select db.*,b.nama, s.namatoko from daftarbeli db, barang b, supplier s');
  DaftarBeliQ.SQL.Add('where db.barang=b.kode and s.kode=db.supplier');
  DaftarBeliQ.SQL.Add('and db.status='+QuotedStr('ON PROCESS')+' and db.cashncarry=0');
  DaftarBeliQ.SQL.Add('order by db.bonbarang desc');
  daftarbeliQ.execsql;
  DaftarBeliQ.Open;
  DaftarBeliQ.Refresh;

end;

procedure TPOFm.BtnDeleteClick(Sender: TObject);
var i:integer;
begin
  if MessageDlg('Hapus PO '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try
        ListBox1.Items.Clear;
       CobaQ.First;
        while not CobaQ.Eof do
        begin
            ListBox1.Items.Add(CobaQKodeDaftarBeli.AsString);
            CobaQ.Next;
        end;

        MenuUtamaFm.Database1.StartTransaction;
        CobaQ.First;
        while not cobaQ.eof do
        begin
          CobaQ.Delete;
        end;

        CobaQ.ApplyUpdates;
       // MenuUtamaFm.Database1.Commit;

      // MenuUtamaFm.Database1.StartTransaction;
       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       {for i:=0 to ListBox1.Items.Count-1 do
        begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ONPROCESS')+' where kode='+QuotedStr(ListBox1.Items[i]));
            SembarangQ.ExecSQL;
        end; }
       MessageDlg('PO telah dihapus.',mtInformation,[mbOK],0);

     except
       MenuUtamaFm.Database1.Rollback;
       HeaderQ.RollbackUpdates;

      // MenuUtamaFm.Database1.Rollback;
       CobaQ.RollbackUpdates;

       MessageDlg('PO pernah/sedang proses bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;

     KodeEditPropertiesButtonClick(sender,1);
  end;
end;

procedure TPOFm.Button1Click(Sender: TObject);
begin
CobaQ.First;
while not cobaQ.eof do
begin
  CobaQ.Delete;
end;
end;

procedure TPOFm.DeleteBtnClick(Sender: TObject);
var i:integer;
begin
  if MessageDlg('Hapus PO '+HeaderQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     try
        ListBox1.Items.Clear;
       CobaQ.First;
        while not CobaQ.Eof do
        begin
            ListBox1.Items.Add(CobaQKodeDaftarBeli.AsString);
            CobaQ.Next;
        end;

        MenuUtamaFm.Database1.StartTransaction;
        CobaQ.First;
        while not cobaQ.eof do
        begin
          CobaQ.Delete;
        end;

        CobaQ.ApplyUpdates;
       // MenuUtamaFm.Database1.Commit;

      // MenuUtamaFm.Database1.StartTransaction;
       HeaderQ.Delete;
       HeaderQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;

       for i:=0 to ListBox1.Items.Count-1 do
        begin
            SembarangQ.SQL.Clear;
            SembarangQ.SQL.Add('update daftarbeli set status='+QuotedStr('ON PROCESS')+' where kode='+QuotedStr(ListBox1.Items[i]));
            SembarangQ.ExecSQL;
        end;
       MessageDlg('PO telah dihapus.',mtInformation,[mbOK],0);

     except
       MenuUtamaFm.Database1.Rollback;
       HeaderQ.RollbackUpdates;

      // MenuUtamaFm.Database1.Rollback;
       CobaQ.RollbackUpdates;

       MessageDlg('PO pernah/sedang proses bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;

     KodeEditPropertiesButtonClick(sender,1);
  end;

end;

procedure TPOFm.SaveBtnClick(Sender: TObject);
var kodebon:String;
i:integer;
tdkcukup:boolean;
begin
if (KodeEdit.Text='INSERT BARU') or (KodeEdit.Text='') then
begin
  try
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      HeaderQKode.AsString:=FormatFloat('0000000000',1);
    end
    else
    begin
      HeaderQKode.AsString:=FormatFloat('0000000000',StrtoInt(KodeQkode.AsString)+1);
    end;
    KodeQ.Close;

    HeaderQGrandTotal.AsInteger:=grandtotal;
    if HeaderQStatus.IsNull then HeaderQStatus.AsString:='NEW PO';

    kodebon:=HeaderQKode.AsString;

   // ShowMessage(kodebon);
    HeaderQ.Edit;
    HeaderQ.Post;

    //ubah kodedetail di detail==================================
    CobaQ.First;
    while not CobaQ.Eof do
    begin
       CobaQ.Edit;
       CobaQKodePO.AsString:=HeaderQKode.AsString;
       CobaQ.Post;

      CobaQ.Next;
    end;//===========================================================


  except
      //on E : Exception do
       // ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
  end
end;

HeaderQ.Edit;
HeaderQGrandTotal.AsInteger:=grandtotal;
//PENGECEKKAN LAGI JIKA SAAT EDIT ADA INSERT BARU
CobaQ.First;
while not CobaQ.Eof do
begin
    if CobaQKodePO.AsString='tes' then
    begin
       CobaQ.Edit;
       CobaQKodePO.AsString:=HeaderQKode.AsString;
       CobaQ.Post;
    end;
  CobaQ.Next;
end;
      //=========================================================

MenuUtamaFm.DataBase1.StartTransaction;
try
  HeaderQ.ApplyUpdates;
  CobaQ.ApplyUpdates;
  MenuUtamaFm.DataBase1.Commit;
  HeaderQ.CommitUpdates;
  CobaQ.CommitUpdates;

  if MessageDlg('PO dengan kode '+ HeaderQKode.AsString +' telah disimpan. Cetak PO ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    SembarangQ.SQL.Clear;
    SembarangQ.SQL.Add('update po set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(HeaderQKode.AsString));
    SembarangQ.ExecSQL;
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'PO.rpt';
    Crpe1.ParamFields[0].CurrentValue:=HeaderQKode.AsString;
    KodeEdit.Text:='';
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    Crpe1.WindowState:= wsMaximized;
  end;

  except
{  on E : Exception do
  ShowMessage(E.ClassName+' error raised, with message : '+E.Message);      }
    MenuUtamaFm.Database1.Rollback;
    HeaderQ.RollbackUpdates;
    CobaQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  ViewQ.Refresh;
  CobaQ.Refresh;
end;

procedure TPOFm.ExitBtnClick(Sender: TObject);
begin
  Close;
  Release;
end;

procedure TPOFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  SetujuBtn.Enabled:=false;
  HeaderQ.Close;
end;

procedure TPOFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    //HeaderQ.SQL.Clear;
//    HeaderQ.SQL.Add('select po.*,s.namatoko from PO,supplier s where po.supplier=s.kode and PO.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    HeaderQ.ParamByName('kodepo').AsString:=KodeEdit.Text;
    //HeaderQ.SQL.Add('select * from ('+ HeaderOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    HeaderQ.Open;
    if HeaderQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      HeaderQ.Append;
      HeaderQ.Edit;
      //MasterQAktif.AsBoolean :=false;
      savebtn.Enabled:=MenuUtamaFm.UserQApprovalPO.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    SetujuBtn.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=True;
  end;

end;

procedure TPOFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    HeaderQ.SQL.Text:=HeaderOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,HeaderQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;

end;

procedure TPOFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  ViewQ.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPO.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePO.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeletePO.AsBoolean;
end;

procedure TPOFm.cxButResetClick(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.SQL.Text:=ViewOriSQL;
  ViewQ.Open;
end;

procedure TPOFm.cxButHPOFinishedClick(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.SQL.Text:='select po.* , s.namatoko from po , supplier s where po.supplier=s.kode and po.status='+QuotedStr('FINISHED')+' and (PO.TglEntry>='+ QuotedStr(DateToStr(cxDateEdit1.date)) +' and PO.TglEntry<='+ QuotedStr(DateToStr(cxDateEdit2.date)) + ') or PO.TglEntry is null order by tglentry desc';
  ViewQ.Open;
end;

end.




