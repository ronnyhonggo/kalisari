object PORebuildFm: TPORebuildFm
  Left = 329
  Top = 135
  Width = 928
  Height = 503
  Caption = 'PO Rebuild'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 445
    Width = 912
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object pnl2: TPanel
    Left = 0
    Top = 394
    Width = 912
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 300
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object cxGroupBox1: TcxGroupBox
      Left = 614
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 912
    Height = 48
    Align = alTop
    TabOrder = 2
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 278
    Width = 912
    Height = 116
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 910
      Height = 114
      Align = alClient
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid2DBTableView1CellDblClick
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid2DBTableView1BarangRebuild: TcxGridDBColumn
          DataBinding.FieldName = 'BarangRebuild'
          Options.SortByDisplayText = isbtOn
          Width = 277
        end
        object cxGrid2DBTableView1TgKeluar: TcxGridDBColumn
          DataBinding.FieldName = 'TgKeluar'
        end
        object cxGrid2DBTableView1DariPlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'DariPlatNo'
        end
        object cxGrid2DBTableView1KePlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'KePlatNo'
        end
        object cxGrid2DBTableView1NamaSupplier: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSupplier'
          Options.SortByDisplayText = isbtOn
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 457
    Height = 230
    Align = alLeft
    Caption = 'Panel2'
    TabOrder = 4
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 455
      Height = 228
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 120
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = MAsterDs
      Version = 1
      object MasterVGridRebuild: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridRebuildEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Rebuild'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridSupplier: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridSupplierEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Supplier'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridNamaSupplier: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaSupplier'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 1
        Index = 0
        Version = 1
      end
    end
  end
  object Panel3: TPanel
    Left = 457
    Top = 48
    Width = 455
    Height = 230
    Align = alClient
    Caption = 'Panel3'
    TabOrder = 5
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 453
      Height = 228
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = True
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Enabled = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DetailPORebuildDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Kegiatan: TcxGridDBColumn
          DataBinding.FieldName = 'Kegiatan'
          Width = 304
        end
        object cxGrid1DBTableView1Harga: TcxGridDBColumn
          DataBinding.FieldName = 'Harga'
          Width = 143
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from PORebuild')
    UpdateObject = MasterUs
    Left = 296
    Top = 16
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      LookupCache = True
      Size = 10
    end
    object MasterQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, Rebuild, Supplier'
      'from PORebuild'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update PORebuild'
      'set'
      '  Kode = :Kode,'
      '  Rebuild = :Rebuild,'
      '  Supplier = :Supplier'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into PORebuild'
      '  (Kode, Rebuild, Supplier)'
      'values'
      '  (:Kode, :Rebuild, :Supplier)')
    DeleteSQL.Strings = (
      'delete from PORebuild'
      'where'
      '  Kode = :OLD_Kode')
    Left = 360
    Top = 16
  end
  object MAsterDs: TDataSource
    DataSet = MasterQ
    Left = 328
    Top = 16
  end
  object DetailPORebuildQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailPORebuild'
      'where PORebuild=:text')
    UpdateObject = DetailPORebuildUs
    Left = 528
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailPORebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DetailPORebuildQPORebuild: TStringField
      FieldName = 'PORebuild'
      Size = 10
    end
    object DetailPORebuildQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object DetailPORebuildQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object DetailPORebuildDs: TDataSource
    DataSet = DetailPORebuildQ
    Left = 560
    Top = 216
  end
  object DetailPORebuildUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, PORebuild, Keterangan, Harga'#13#10'from DetailPORebuild'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update DetailPORebuild'
      'set'
      '  Kode = :Kode,'
      '  PORebuild = :PORebuild,'
      '  Keterangan = :Keterangan,'
      '  Harga = :Harga'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into DetailPORebuild'
      '  (Kode, PORebuild, Keterangan, Harga)'
      'values'
      '  (:Kode, :PORebuild, :Keterangan, :Harga)')
    DeleteSQL.Strings = (
      'delete from DetailPORebuild'
      'where'
      '  Kode = :OLD_Kode')
    Left = 592
    Top = 208
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from PORebuild order by kode desc')
    Left = 248
    Top = 16
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Supplier')
    Left = 416
    Top = 16
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 15
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object KodeDetailPORebuild: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from detailPORebuild order by kode desc')
    Left = 496
    Top = 216
    object KodeDetailPORebuildkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object KodePORQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select kode from DetailPORebuild'
      'where PORebuild=:text ')
    Left = 208
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KodePORQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DeletePORQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailPORebuild d where d.PORebuild=:text')
    Left = 408
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeletePORQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DeletePORQPORebuild: TStringField
      FieldName = 'PORebuild'
      Size = 10
    end
    object DeletePORQKegiatan: TMemoField
      FieldName = 'Kegiatan'
      BlobType = ftMemo
    end
    object DeletePORQHarga: TCurrencyField
      FieldName = 'Harga'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailPORebuild d where d.PORebuild=:text')
    UpdateObject = DeleteDBKBUpd
    Left = 408
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1PORebuild: TStringField
      FieldName = 'PORebuild'
      Size = 10
    end
    object SDQuery1Kegiatan: TMemoField
      FieldName = 'Kegiatan'
      BlobType = ftMemo
    end
    object SDQuery1Harga: TCurrencyField
      FieldName = 'Harga'
    end
  end
  object DeleteDBKBUpd: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, PORebuild, Kegiatan, Harga'
      'from DetailPORebuild'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update DetailPORebuild'
      'set'
      '  Kode = :Kode,'
      '  PORebuild = :PORebuild,'
      '  Kegiatan = :Kegiatan,'
      '  Harga = :Harga'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into DetailPORebuild'
      '  (Kode, PORebuild, Kegiatan, Harga)'
      'values'
      '  (:Kode, :PORebuild, :Kegiatan, :Harga)')
    DeleteSQL.Strings = (
      'delete from DetailPORebuild'
      'where'
      '  Kode = :OLD_Kode')
    Left = 448
    Top = 128
  end
  object ViewPORebuildQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select cast(tanggal as date) as Tgl,* '
      'from porebuild'
      'where (Tanggal>=:text1 and Tanggal<=:text2) or tanggal is null'
      'order by tglentry desc')
    Left = 464
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewPORebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewPORebuildQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object ViewPORebuildQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object ViewPORebuildQKodeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeBarang'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barang'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object ViewPORebuildQBarangRebuild: TStringField
      FieldKind = fkLookup
      FieldName = 'BarangRebuild'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 50
      Lookup = True
    end
    object ViewPORebuildQKodeDariArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeDariArmada'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'DariArmada'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object ViewPORebuildQKodeKeArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeKeArmada'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'KeArmada'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object ViewPORebuildQDariPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'DariPlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KodeDariArmada'
      Size = 10
      Lookup = True
    end
    object ViewPORebuildQKePlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'KePlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KodeKeArmada'
      Size = 10
      Lookup = True
    end
    object ViewPORebuildQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 50
      Lookup = True
    end
    object ViewPORebuildQTgKeluar: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TgKeluar'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TanggalKeluar'
      KeyFields = 'Rebuild'
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = ViewPORebuildQ
    Left = 504
    Top = 376
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Barang')
    Left = 552
    Top = 384
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object RebuildQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from rebuild')
    Left = 584
    Top = 384
    object RebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RebuildQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object RebuildQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object RebuildQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object RebuildQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object RebuildQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object RebuildQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object RebuildQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object RebuildQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object RebuildQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object RebuildQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object RebuildQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object RebuildQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object RebuildQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object RebuildQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object RebuildQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object RebuildQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object RebuildQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object RebuildQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object RebuildQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object RebuildQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RebuildQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RebuildQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RebuildQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 616
    Top = 384
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
end
