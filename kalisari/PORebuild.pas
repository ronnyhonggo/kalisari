unit PORebuild;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxControls,
  cxContainer, cxEdit, cxStyles, cxVGrid, cxDBVGrid, cxInplaceContainer,
  DB, SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit, StdCtrls, cxLabel,
  cxButtons, ExtCtrls, ComCtrls, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxCalendar, cxGroupBox;

type
  TPORebuildFm = class(TForm)
    StatusBar: TStatusBar;
    pnl2: TPanel;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    cxLabel1: TcxLabel;
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    MasterQ: TSDQuery;
    MasterUs: TSDUpdateSQL;
    MAsterDs: TDataSource;
    MasterQKode: TStringField;
    MasterQRebuild: TStringField;
    MasterQSupplier: TStringField;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridRebuild: TcxDBEditorRow;
    MasterVGridSupplier: TcxDBEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DetailPORebuildQ: TSDQuery;
    DetailPORebuildDs: TDataSource;
    DetailPORebuildUs: TSDUpdateSQL;
    DetailPORebuildQKode: TStringField;
    DetailPORebuildQPORebuild: TStringField;
    DetailPORebuildQHarga: TCurrencyField;
    cxGrid1DBTableView1Kegiatan: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQKategori: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    MasterQNamaSupplier: TStringField;
    MasterVGridNamaSupplier: TcxDBEditorRow;
    KodeDetailPORebuild: TSDQuery;
    KodeDetailPORebuildkode: TStringField;
    KodePORQ: TSDQuery;
    KodePORQkode: TStringField;
    DeletePORQ: TSDQuery;
    DeletePORQKode: TStringField;
    DeletePORQPORebuild: TStringField;
    DeletePORQKegiatan: TMemoField;
    DeletePORQHarga: TCurrencyField;
    SDQuery1: TSDQuery;
    DeleteDBKBUpd: TSDUpdateSQL;
    SDQuery1Kode: TStringField;
    SDQuery1PORebuild: TStringField;
    SDQuery1Kegiatan: TMemoField;
    SDQuery1Harga: TCurrencyField;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    ViewPORebuildQ: TSDQuery;
    DataSource1: TDataSource;
    ViewPORebuildQKode: TStringField;
    ViewPORebuildQRebuild: TStringField;
    ViewPORebuildQSupplier: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQSingleSupplier: TBooleanField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    ViewPORebuildQKodeBarang: TStringField;
    ViewPORebuildQBarangRebuild: TStringField;
    ArmadaQ: TSDQuery;
    ViewPORebuildQKodeDariArmada: TStringField;
    ViewPORebuildQKodeKeArmada: TStringField;
    ViewPORebuildQDariPlatNo: TStringField;
    ViewPORebuildQKePlatNo: TStringField;
    ViewPORebuildQNamaSupplier: TStringField;
    cxGrid2DBTableView1BarangRebuild: TcxGridDBColumn;
    cxGrid2DBTableView1DariPlatNo: TcxGridDBColumn;
    cxGrid2DBTableView1KePlatNo: TcxGridDBColumn;
    cxGrid2DBTableView1NamaSupplier: TcxGridDBColumn;
    ViewPORebuildQTgKeluar: TDateTimeField;
    cxGrid2DBTableView1TgKeluar: TcxGridDBColumn;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    DetailPORebuildQKeterangan: TMemoField;
    BarangQJumlah: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRebuildEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSupplierEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PORebuildFm: TPORebuildFm;
  MAsterOriSQL,DetailPORebuildOriSQL:string;
  IsiBaru:integer;
implementation

uses RebuildDropDown, DM, MenuUtama, DropDown, SupplierDropDown;

{$R *.dfm}

procedure TPORebuildFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TPORebuildFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailPORebuildOriSQL:=DetailPORebuildQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPORebuildFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPOUmum.AsBoolean;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewPORebuildQ.Close;
  ViewPORebuildQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewPORebuildQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewPORebuildQ.Open;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeletePOUmum.AsBoolean;
  BarangQ.Open;
  RebuildQ.Open;
  ArmadaQ.Open;
  ViewPORebuildQ.Open;
end;

procedure TPORebuildFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPORebuildFm.KodeEditExit(Sender: TObject);
begin
 if KodeEdit.Text<>'' then
  begin
    Masterq.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      DetailPORebuildQ.Open;
      DetailPORebuildQ.Append;
      DetailPORebuildQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DetailPORebuildQ.Open;
      DetailPORebuildQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeletePOUmum.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TPORebuildFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPORebuildFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  MasterVGrid.Enabled:=True;
  IsiBaru:=1;
end;

procedure TPORebuildFm.MasterVGridRebuildEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 RebuildDropDownFm:=TRebuildDropdownfm.Create(Self);
    if RebuildDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRebuild.AsString:=RebuildDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    RebuildDropDownFm.Release;
end;

procedure TPORebuildFm.MasterVGridSupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
    if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSupplier.AsString:=SupplierDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    SupplierDropDownFm.Release;
end;

procedure TPORebuildFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
if abuttonindex=10 then
  begin
  DetailPORebuildQ.Edit;
  DetailPORebuildQKode.AsString:='0';
  DetailPORebuildQ.Post;
    //cxGrid1DBTableView1.Focused:=false;
  end;
end;

procedure TPORebuildFm.SaveBtnClick(Sender: TObject);
var kode:integer;
var kodes:string;
begin
kode:=-1;
try

  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
  MasterQ.Edit;
  KodeQ.Open;

    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage('a');
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end; //untuk if mode:entry
  MenuUtamaFm.Database1.StartTransaction;
except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
end;
  try
    MasterQ.ApplyUpdates;
    DetailPORebuildQ.Open;
    DetailPORebuildQ.Edit;
    DetailPORebuildQ.First;

    while DetailPORebuildQ.Eof=FALSE do
    begin
           KodeDetailPORebuild.Close;
        KodeDetailPORebuild.Open;
        if kode=-1 then
        begin
         if IsiBaru=1 then
         begin
            if KodeDetailPORebuildkode.AsString <> '' then
            begin
              kode:=strtoint(KodeDetailPORebuildkode.AsString);
            end

            else
            begin
              kode:=0;
            end;
         end  //end insert baru

         else
         begin
          KodePORQ.Close;
          KodePORQ.ParamByName('text').AsString:=KodeEdit.Text;
          //KodeDBKBQ.ParamByName('text2').AsString:=DetailBonKeluarBarangQBarang.AsString;
          KodePORQ.Open;
          //ShowMessage(inttostr(IsiBaru));
             kode:=strtoint(KodePORQkode.AsString)-1;
         end;

    end

        else
        begin
            kode:=kode+1;
            //kode:=kode;
        end;

        kodes:=inttostr(kode);
        KodeDetailPORebuild.Close;
        DetailPORebuildQ.Edit;
        DetailPORebuildQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(kodes)+1),10,'0',True);
        DetailPORebuildQPORebuild.AsString:=KodeEdit.Text;
        DetailPORebuildQ.Post;
        DetailPORebuildQ.Next;
    end;

    DetailPORebuildQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailPORebuildQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    //FormShow(self);
    //cxButtonEdit1PropertiesButtonClick(sender,0);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailPORebuildQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
    //MenuUtamaFm.Database1.Rollback;
   // MasterQ.RollbackUpdates;
    //ShowMessage('Penyimpanan Gagal');
  //end;
  KodeEdit.SetFocus;
  ViewPORebuildQ.Close;
  ViewPORebuildQ.Open;
  DetailPORebuildQ.Close;
  DetailPORebuildQ.SQL.Text:=DetailPORebuildOriSQL;
  DetailPORebuildQ.ParamByName('text').AsString:='';
  DetailPORebuildQ.Open;
  DetailPORebuildQ.Close;


end;

procedure TPORebuildFm.DeleteBtnClick(Sender: TObject);
begin
 if MessageDlg('Hapus PO Rebuild '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
     DeletePORQ.Close;
     DeletePORQ.ParamByName('text').AsString := MasterQKode.AsString;
     DeletePORQ.Open;
     DeletePORQ.First;
     while DeletePORQ.Eof = FALSE
     do
     begin
       SDQuery1.SQL.Text:=('delete from DetailPORebuild where PORebuild='+QuotedStr(KodeEdit.Text));
       SDQuery1.ExecSQL;
       DeletePORQ.Next;
     end;
     //DeleteDBKBQ.Delete;
     //DeleteDBKBQ.ApplyUpdates;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('PO Umum telah dihapus.',mtInformation,[mbOK],0);
       DeletePORQ.Close;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('PO Umum pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     ViewPORebuildQ.Close;
     ViewPORebuildQ.Open;
     {ViewBonKeluarBarang.Close;
  ViewBonKeluarBarang.ExecSQL;
  ViewBonKeluarBarang.Open; }
  DetailPORebuildQ.Close;
  end;
end;

procedure TPORebuildFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
IsiBaru:=0;
SupplierQ.Open;
   MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewPORebuildQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;

    end;


    SaveBtn.Enabled:=menuutamafm.UserQUpdatePOUmum.AsBoolean;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    MasterVGrid.Enabled:=True;
    DetailPORebuildQ.Close;
    SupplierQ.Open;
    DetailPORebuildQ.ParamByName('text').AsString:=MasterQKode.AsString;
    DetailPORebuildQ.Open;
    DetailPORebuildQ.Edit;
   DetailPORebuildQ.First;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TPORebuildFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewPORebuildQ.Close;
  ViewPORebuildQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewPORebuildQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewPORebuildQ.Open;
end;

procedure TPORebuildFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewPORebuildQ.Close;
  ViewPORebuildQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewPORebuildQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewPORebuildQ.Open;
end;

end.
