object PasangBanFm: TPasangBanFm
  Left = 360
  Top = 26
  BorderStyle = bsDialog
  Caption = 'Lepas Pasang Ban'
  ClientHeight = 690
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 639
    Height = 690
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 49
      Width = 637
      Height = 416
      Align = alTop
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 158
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      TabStop = False
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridArmada: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridArmadaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Armada'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridplatno_armada: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'platno_armada'
        Properties.Options.Editing = False
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object MasterVGridDBEditorRow1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBody'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object MasterVGridjumlahban_armada: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'jumlahban_armada'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 0
        Index = 2
        Version = 1
      end
      object MasterVGridPeminta: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridPemintaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Peminta'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 4
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridDetailPeminta: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'DetailPeminta'
        Properties.Options.Editing = False
        ID = 5
        ParentID = 4
        Index = 0
        Version = 1
      end
      object MasterVGridPelaksana: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridPelaksanaEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Pelaksana'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 6
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridDetailPelaksana: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'DetailPelaksana'
        Properties.Options.Editing = False
        ID = 7
        ParentID = 6
        Index = 0
        Version = 1
      end
      object MasterVGridBan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridBanEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Ban'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 8
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridjenis_ban: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'jenis_ban'
        Properties.Options.Editing = False
        ID = 9
        ParentID = 8
        Index = 0
        Version = 1
      end
      object MasterVGridmerk_ban: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'merk_ban'
        Properties.Options.Editing = False
        ID = 10
        ParentID = 8
        Index = 1
        Version = 1
      end
      object MasterVGridkodeidban: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'kodeidban'
        Properties.Options.Editing = False
        ID = 11
        ParentID = 8
        Index = 2
        Version = 1
      end
      object MasterVGridKedalamanAlur: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KedalamanAlur'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 12
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridLokasiPasang: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridLokasiPasangEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'LokasiPasang'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 13
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridTglPasang: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglPasang'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 14
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Height = 46
        Properties.DataBinding.FieldName = 'Keterangan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 15
        ParentID = -1
        Index = 7
        Version = 1
      end
      object MasterVGridStatusBan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Columns = 2
        Properties.EditProperties.Items = <
          item
            Caption = 'Baru'
            Value = 'Gudang'
          end
          item
            Caption = 'Bekas'
            Value = 'Tukar'
          end>
        Properties.DataBinding.FieldName = 'StatusBan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 16
        ParentID = -1
        Index = 8
        Version = 1
      end
    end
    object pnl2: TPanel
      Left = 1
      Top = 621
      Width = 637
      Height = 48
      Align = alBottom
      TabOrder = 1
      object buttonCetak: TcxButton
        Left = 252
        Top = 8
        Width = 75
        Height = 25
        Caption = 'CETAK'
        Enabled = False
        TabOrder = 0
        OnClick = buttonCetakClick
      end
      object SaveBtn: TcxButton
        Left = 8
        Top = 10
        Width = 75
        Height = 25
        Caption = 'SAVE'
        TabOrder = 1
        OnClick = SaveBtnClick
      end
      object ExitBtn: TcxButton
        Left = 168
        Top = 8
        Width = 75
        Height = 25
        Caption = 'EXIT'
        TabOrder = 2
        OnClick = ExitBtnClick
      end
      object DeleteBtn: TcxButton
        Left = 88
        Top = 9
        Width = 75
        Height = 25
        Caption = 'DELETE'
        TabOrder = 3
        OnClick = DeleteBtnClick
      end
      object cxGroupBox1: TcxGroupBox
        Left = 339
        Top = 1
        Align = alRight
        Alignment = alTopRight
        Caption = 'Tanggal History'
        TabOrder = 4
        Height = 46
        Width = 297
        object cxDateEdit1: TcxDateEdit
          Left = 16
          Top = 16
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.OnChange = cxDateEdit1PropertiesChange
          TabOrder = 0
          Width = 121
        end
        object cxDateEdit2: TcxDateEdit
          Left = 168
          Top = 16
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.OnChange = cxDateEdit2PropertiesChange
          TabOrder = 1
          Width = 121
        end
        object cxLabel7: TcxLabel
          Left = 142
          Top = 18
          Caption = 's/d'
        end
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 669
      Width = 637
      Height = 20
      Panels = <
        item
          Width = 50
        end>
    end
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 637
      Height = 48
      Align = alTop
      TabOrder = 3
      object lbl1: TLabel
        Left = 8
        Top = 13
        Width = 25
        Height = 13
        Caption = 'Kode'
      end
      object KodeEdit: TcxButtonEdit
        Left = 40
        Top = 10
        Properties.Buttons = <
          item
            Caption = '+'
            Default = True
            Kind = bkText
          end>
        Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebs3D
        Style.HotTrack = False
        Style.ButtonStyle = bts3D
        TabOrder = 0
        OnEnter = KodeEditEnter
        OnExit = KodeEditExit
        OnKeyDown = KodeEditKeyDown
        Width = 121
      end
      object cxRadioButton2: TcxRadioButton
        Left = 250
        Top = 12
        Width = 71
        Height = 17
        Caption = 'Lepas'
        TabOrder = 1
        OnClick = cxRadioButton2Click
      end
      object cxRadioButton1: TcxRadioButton
        Left = 176
        Top = 12
        Width = 73
        Height = 17
        Caption = 'Pasang'
        Checked = True
        TabOrder = 2
        TabStop = True
        OnClick = cxRadioButton1Click
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 465
      Width = 637
      Height = 156
      Align = alClient
      Caption = 'Panel2'
      TabOrder = 4
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 635
        Height = 154
        Align = alClient
        BevelOuter = bvRaised
        BevelWidth = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid1DBTableView1CellDblClick
          DataController.DataSource = SJDS
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsSelection.CellSelect = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1Kode: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
          end
          object cxGrid1DBTableView1Armada: TcxGridDBColumn
            DataBinding.FieldName = 'NoBodyArmada'
            Width = 117
          end
          object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
            DataBinding.FieldName = 'PlatNoArmada'
            Width = 128
          end
          object cxGrid1DBTableView1Peminta: TcxGridDBColumn
            DataBinding.FieldName = 'NamaPeminta'
            Width = 143
          end
          object cxGrid1DBTableView1Keluhan: TcxGridDBColumn
            DataBinding.FieldName = 'Keterangan'
            Width = 143
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object cxGrid2: TcxGrid
        Left = 1
        Top = 1
        Width = 635
        Height = 154
        Align = alClient
        BevelOuter = bvRaised
        BevelWidth = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Visible = False
        object cxGridDBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGridDBTableView1CellDblClick
          DataController.DataSource = NViewLepasSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          object cxGridDBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
          end
          object cxGridDBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'NoBodyArmada'
            Width = 104
          end
          object cxGridDBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'PlatNoArmada'
            Options.SortByDisplayText = isbtOn
            Width = 95
          end
          object cxGridDBTableView1Column5: TcxGridDBColumn
            DataBinding.FieldName = 'NamaPeminta'
            Options.SortByDisplayText = isbtOn
            Width = 110
          end
          object cxGridDBTableView1Column4: TcxGridDBColumn
            DataBinding.FieldName = 'Permasalahan'
            Width = 221
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select pb.*,a.PlatNo as platno_armada, a.JumlahBan as jumlahban_' +
        'armada, b.Jenis as jenis_ban, b.Merk as merk_ban from PasangBan ' +
        'pb left outer join Armada a on a.Kode=pb.Armada, Ban b where pb.' +
        'Ban=b.Kode')
    UpdateObject = MasterUS
    Left = 469
    Top = 15
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQjumlahban_armada: TIntegerField
      DisplayLabel = 'JumlahBan'
      FieldKind = fkLookup
      FieldName = 'jumlahban_armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JumlahBan'
      KeyFields = 'Armada'
      Lookup = True
    end
    object MasterQplatno_armada: TStringField
      DisplayLabel = 'PlatNo'
      FieldKind = fkLookup
      FieldName = 'platno_armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object MasterQDetailPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 10
      Lookup = True
    end
    object MasterQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Size = 10
    end
    object MasterQDetailPelaksana: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPelaksana'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelaksana'
      Size = 10
      Lookup = True
    end
    object MasterQBan: TStringField
      FieldName = 'Ban'
      LookupCache = True
      Required = True
      Size = 10
    end
    object MasterQjenis_ban: TStringField
      FieldKind = fkLookup
      FieldName = 'jenis_ban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jenis'
      KeyFields = 'Ban'
      Required = True
      Size = 50
      Lookup = True
    end
    object MasterQmerk_ban: TStringField
      FieldKind = fkLookup
      FieldName = 'merk_ban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Merk'
      KeyFields = 'Ban'
      Required = True
      Size = 50
      Lookup = True
    end
    object MasterQLokasiPasang: TStringField
      FieldName = 'LokasiPasang'
      Required = True
      Size = 50
    end
    object MasterQTglPasang: TDateTimeField
      FieldName = 'TglPasang'
      Required = True
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQStatusBan: TStringField
      FieldName = 'StatusBan'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQKedalamanAlur: TFloatField
      FieldName = 'KedalamanAlur'
    end
    object MasterQkodeidban: TStringField
      FieldKind = fkLookup
      FieldName = 'kodeidban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'KodeIDBan'
      KeyFields = 'Ban'
      Size = 50
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 508
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Armada, Peminta, Pelaksana, Ban, KedalamanAlur, Lok' +
        'asiPasang, TglPasang, Keterangan, CreateDate, CreateBy, Operator' +
        ', TglEntry, TglCetak, StatusBan'#13#10'from PasangBan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update PasangBan'
      'set'
      '  Kode = :Kode,'
      '  Armada = :Armada,'
      '  Peminta = :Peminta,'
      '  Pelaksana = :Pelaksana,'
      '  Ban = :Ban,'
      '  KedalamanAlur = :KedalamanAlur,'
      '  LokasiPasang = :LokasiPasang,'
      '  TglPasang = :TglPasang,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  StatusBan = :StatusBan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into PasangBan'
      
        '  (Kode, Armada, Peminta, Pelaksana, Ban, KedalamanAlur, LokasiP' +
        'asang, TglPasang, Keterangan, CreateDate, CreateBy, Operator, Tg' +
        'lEntry, TglCetak, StatusBan)'
      'values'
      
        '  (:Kode, :Armada, :Peminta, :Pelaksana, :Ban, :KedalamanAlur, :' +
        'LokasiPasang, :TglPasang, :Keterangan, :CreateDate, :CreateBy, :' +
        'Operator, :TglEntry, :TglCetak, :StatusBan)')
    DeleteSQL.Strings = (
      'delete from PasangBan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 539
    Top = 15
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pasangban order by kode desc')
    Left = 113
    Top = 615
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 305
    Top = 87
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 337
    Top = 87
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object viewSJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select cast(pb.tglPasang as date) as Tgl,pb.*,a.PlatNo as platno' +
        '_armada, a.JumlahBan as jumlahban_armada, b.Jenis as jenis_ban, ' +
        'b.Merk as merk_ban'
      'from PasangBan pb '
      'left outer join Armada a on a.Kode=pb.Armada, Ban b '
      
        'where pb.Ban=b.Kode and (pb.tglPasang>=:text1 and pb.TglPasang<=' +
        ':text2) or Pb.TglPasang is null'
      'order by tglentry desc')
    Left = 298
    Top = 485
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewSJQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewSJQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 50
    end
    object viewSJQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewSJQBan: TStringField
      FieldName = 'Ban'
      Required = True
      Size = 10
    end
    object viewSJQLokasiPasang: TStringField
      FieldName = 'LokasiPasang'
      Required = True
      Size = 50
    end
    object viewSJQTglPasang: TDateTimeField
      FieldName = 'TglPasang'
      Required = True
    end
    object viewSJQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object viewSJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewSJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewSJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewSJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewSJQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewSJQStatusBan: TStringField
      FieldName = 'StatusBan'
      Required = True
      Size = 50
    end
    object viewSJQjumlahban_armada: TIntegerField
      FieldName = 'jumlahban_armada'
    end
    object viewSJQjenis_ban: TStringField
      FieldName = 'jenis_ban'
      Required = True
      Size = 50
    end
    object viewSJQmerk_ban: TStringField
      FieldName = 'merk_ban'
      Required = True
      Size = 50
    end
    object viewSJQNoBodyArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object viewSJQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object viewSJQPlatNoArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 12
      Lookup = True
    end
  end
  object SJDS: TDataSource
    DataSet = viewSJQ
    Left = 260
    Top = 614
  end
  object sopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from sopir')
    Left = 321
    Top = 15
    object sopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object sopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object sopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object sopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object sopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object sopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object sopirQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object sopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 572
    Top = 14
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 361
    Top = 15
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object jarakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 401
    Top = 15
    object jarakQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jarakQDari: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object jarakQKe: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object jarakQJarak: TIntegerField
      FieldName = 'Jarak'
    end
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 145
    Top = 615
    object StringField1: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object MemoField1: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object MemoField2: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object IntegerField1: TIntegerField
      FieldName = 'Jarak'
    end
  end
  object banQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from ban')
    Left = 369
    Top = 87
    object banQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object banQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object banQKodeIDBan: TStringField
      FieldName = 'KodeIDBan'
      Size = 50
    end
    object banQMerk: TStringField
      FieldName = 'Merk'
      Required = True
      Size = 50
    end
    object banQJenis: TStringField
      FieldName = 'Jenis'
      Required = True
      Size = 50
    end
    object banQRing: TStringField
      FieldName = 'Ring'
      Size = 50
    end
    object banQVelg: TStringField
      FieldName = 'Velg'
      Size = 50
    end
    object banQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object banQStandardKm: TIntegerField
      FieldName = 'StandardKm'
    end
    object banQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object banQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object banQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object banQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object banQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
  end
  object NlepasQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select pb.*,a.PlatNo as platno_armada, a.JumlahBan as jumlahban_' +
        'armada, b.Jenis as jenis_ban, b.Merk as merk_ban from lepasBan p' +
        'b left outer join Armada a on a.Kode=pb.Armada, Ban b where pb.B' +
        'an=b.Kode')
    UpdateObject = newUpdateLepasUS
    Left = 328
    Top = 608
    object NlepasQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object NlepasQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object NlepasQBan: TStringField
      FieldName = 'Ban'
      Required = True
      Size = 10
    end
    object NlepasQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object NlepasQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Size = 10
    end
    object NlepasQLokasiLepas: TStringField
      FieldName = 'LokasiLepas'
      Required = True
      Size = 50
    end
    object NlepasQTglLepas: TDateTimeField
      FieldName = 'TglLepas'
      Required = True
    end
    object NlepasQPermasalahan: TMemoField
      FieldName = 'Permasalahan'
      Required = True
      BlobType = ftMemo
    end
    object NlepasQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object NlepasQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object NlepasQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object NlepasQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object NlepasQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object NlepasQStatusBan: TStringField
      FieldName = 'StatusBan'
      Required = True
      Size = 50
    end
    object NlepasQplatno_armada: TStringField
      FieldKind = fkLookup
      FieldName = 'platno_armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object NlepasQjumlahban_armada: TIntegerField
      FieldKind = fkLookup
      FieldName = 'jumlahban_armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JumlahBan'
      KeyFields = 'Armada'
      Lookup = True
    end
    object NlepasQjenis_ban: TStringField
      FieldKind = fkLookup
      FieldName = 'jenis_ban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jenis'
      KeyFields = 'Ban'
      Required = True
      Size = 50
      Lookup = True
    end
    object NlepasQmerk_ban: TStringField
      FieldKind = fkLookup
      FieldName = 'merk_ban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Merk'
      KeyFields = 'Ban'
      Required = True
      Size = 50
      Lookup = True
    end
    object NlepasQDetailPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 10
      Lookup = True
    end
    object NlepasQDetailPelaksana: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPelaksana'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelaksana'
      Size = 10
      Lookup = True
    end
    object NlepasQKodeidban: TStringField
      FieldKind = fkLookup
      FieldName = 'Kodeidban'
      LookupDataSet = banQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'KodeIDBan'
      KeyFields = 'Ban'
      Size = 10
      Lookup = True
    end
    object NlepasQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object NlepasQKedalamanAlur: TFloatField
      FieldName = 'KedalamanAlur'
    end
  end
  object nLepasSource: TDataSource
    DataSet = NlepasQ
    Left = 360
    Top = 608
  end
  object newUpdateLepasUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Armada, Ban, KedalamanAlur, Peminta, Pelaksana, Lok' +
        'asiLepas, TglLepas, Permasalahan, CreateDate, CreateBy, Operator' +
        ', TglEntry, TglCetak, StatusBan'
      'from lepasBan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update lepasBan'
      'set'
      '  Kode = :Kode,'
      '  Armada = :Armada,'
      '  Ban = :Ban,'
      '  KedalamanAlur = :KedalamanAlur,'
      '  Peminta = :Peminta,'
      '  Pelaksana = :Pelaksana,'
      '  LokasiLepas = :LokasiLepas,'
      '  TglLepas = :TglLepas,'
      '  Permasalahan = :Permasalahan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  StatusBan = :StatusBan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into lepasBan'
      
        '  (Kode, Armada, Ban, KedalamanAlur, Peminta, Pelaksana, LokasiL' +
        'epas, TglLepas, Permasalahan, CreateDate, CreateBy, Operator, Tg' +
        'lEntry, TglCetak, StatusBan)'
      'values'
      
        '  (:Kode, :Armada, :Ban, :KedalamanAlur, :Peminta, :Pelaksana, :' +
        'LokasiLepas, :TglLepas, :Permasalahan, :CreateDate, :CreateBy, :' +
        'Operator, :TglEntry, :TglCetak, :StatusBan)')
    DeleteSQL.Strings = (
      'delete from lepasBan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 544
    Top = 616
  end
  object newKodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from lepasban order by kode desc')
    Left = 176
    Top = 616
    object newKodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object NviewLepasQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select cast(pb.TglLepas as date) as Tgl,pb.*,a.PlatNo as platno_' +
        'armada, a.JumlahBan as jumlahban_armada, b.Jenis as jenis_ban, b' +
        '.Merk as merk_ban'
      
        'from lepasBan pb left outer join Armada a on a.Kode=pb.Armada, B' +
        'an b'
      
        'where pb.Ban=b.Kode and (pb.TglLepas>=:text1 and pb.TglLepas<=:t' +
        'ext2) or pb.TglLepas is null'
      'order by tglentry desc')
    Left = 328
    Top = 488
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object NviewLepasQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object NviewLepasQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object NviewLepasQBan: TStringField
      FieldName = 'Ban'
      Required = True
      Size = 10
    end
    object NviewLepasQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object NviewLepasQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Size = 10
    end
    object NviewLepasQLokasiLepas: TStringField
      FieldName = 'LokasiLepas'
      Required = True
      Size = 50
    end
    object NviewLepasQTglLepas: TDateTimeField
      FieldName = 'TglLepas'
      Required = True
    end
    object NviewLepasQPermasalahan: TMemoField
      FieldName = 'Permasalahan'
      Required = True
      BlobType = ftMemo
    end
    object NviewLepasQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object NviewLepasQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object NviewLepasQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object NviewLepasQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object NviewLepasQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object NviewLepasQStatusBan: TStringField
      FieldName = 'StatusBan'
      Required = True
      Size = 50
    end
    object NviewLepasQjenis_ban: TStringField
      FieldName = 'jenis_ban'
      Required = True
      Size = 50
    end
    object NviewLepasQmerk_ban: TStringField
      FieldName = 'merk_ban'
      Required = True
      Size = 50
    end
    object NviewLepasQNoBodyArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 12
      Lookup = True
    end
    object NviewLepasQPlatNoArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 12
      Lookup = True
    end
    object NviewLepasQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object NviewLepasQKedalamanAlur: TFloatField
      FieldName = 'KedalamanAlur'
    end
    object NviewLepasQplatno_armada: TStringField
      FieldName = 'platno_armada'
      Size = 10
    end
    object NviewLepasQjumlahban_armada: TIntegerField
      FieldName = 'jumlahban_armada'
    end
  end
  object NViewLepasSource: TDataSource
    DataSet = NviewLepasQ
    Left = 512
    Top = 616
  end
  object DetailArmadaBanUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeBan, KodeArmada, KodeLokasiBan'
      'from detailarmadaban'
      'where'
      '  KodeBan = :OLD_KodeBan and'
      '  KodeArmada = :OLD_KodeArmada and'
      '  KodeLokasiBan = :OLD_KodeLokasiBan')
    ModifySQL.Strings = (
      'update detailarmadaban'
      'set'
      '  KodeBan = :KodeBan,'
      '  KodeArmada = :KodeArmada,'
      '  KodeLokasiBan = :KodeLokasiBan'
      'where'
      '  KodeBan = :OLD_KodeBan and'
      '  KodeArmada = :OLD_KodeArmada and'
      '  KodeLokasiBan = :OLD_KodeLokasiBan')
    InsertSQL.Strings = (
      'insert into detailarmadaban'
      '  (KodeBan, KodeArmada, KodeLokasiBan)'
      'values'
      '  (:KodeBan, :KodeArmada, :KodeLokasiBan)')
    DeleteSQL.Strings = (
      'delete from detailarmadaban'
      'where'
      '  KodeBan = :OLD_KodeBan and'
      '  KodeArmada = :OLD_KodeArmada and'
      '  KodeLokasiBan = :OLD_KodeLokasiBan')
    Left = 336
    Top = 336
  end
  object DetailArmadaBanQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailarmadaban'
      'where KodeBan=:text and KodeArmada=:text2'
      'and KodeLokasiBan=:text3')
    UpdateObject = DetailArmadaBanUS
    Left = 304
    Top = 336
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end>
    object DetailArmadaBanQKodeBan: TStringField
      FieldName = 'KodeBan'
      Required = True
      Size = 10
    end
    object DetailArmadaBanQKodeArmada: TStringField
      FieldName = 'KodeArmada'
      Required = True
      Size = 10
    end
    object DetailArmadaBanQKodeLokasiBan: TStringField
      FieldName = 'KodeLokasiBan'
      Required = True
      Size = 10
    end
  end
  object CheckPBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select * from PasangBan where Armada=:par1 AND LokasiPasang=:par' +
        '2 AND Not Kode=:par3')
    Left = 224
    Top = 536
    ParamData = <
      item
        DataType = ftString
        Name = 'par1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'par2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'par3'
        ParamType = ptInput
      end>
    object CheckPBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CheckPBQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object CheckPBQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object CheckPBQPelaksana: TStringField
      FieldName = 'Pelaksana'
      Size = 10
    end
    object CheckPBQBan: TStringField
      FieldName = 'Ban'
      Required = True
      Size = 10
    end
    object CheckPBQKedalamanAlur: TFloatField
      FieldName = 'KedalamanAlur'
    end
    object CheckPBQLokasiPasang: TStringField
      FieldName = 'LokasiPasang'
      Required = True
      Size = 50
    end
    object CheckPBQTglPasang: TDateTimeField
      FieldName = 'TglPasang'
      Required = True
    end
    object CheckPBQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CheckPBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CheckPBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CheckPBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CheckPBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CheckPBQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object CheckPBQStatusBan: TStringField
      FieldName = 'StatusBan'
      Required = True
      Size = 50
    end
  end
  object CheckLBQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *'
      'from DetailArmadaBan'
      'where (KodeArmada=:ard and KodeLokasiBan=:lokb) or KodeBan=:ban')
    Left = 192
    Top = 536
    ParamData = <
      item
        DataType = ftString
        Name = 'ard'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'lokb'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ban'
        ParamType = ptInput
      end>
  end
end
