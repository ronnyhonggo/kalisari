unit PasangBan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxBlobEdit, cxImage,
  cxDropDownEdit, cxGroupBox;

type
  TPasangBanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    sopirQ: TSDQuery;
    sopirQKode: TStringField;
    sopirQNama: TStringField;
    sopirQAlamat: TStringField;
    sopirQNotelp: TStringField;
    sopirQCreateDate: TDateTimeField;
    sopirQCreateBy: TStringField;
    sopirQOperator: TStringField;
    sopirQTglEntry: TDateTimeField;
    Crpe1: TCrpe;
    PegawaiQ: TSDQuery;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    jarakQJarak: TIntegerField;
    updateQ: TSDQuery;
    StringField1: TStringField;
    MemoField1: TMemoField;
    MemoField2: TMemoField;
    IntegerField1: TIntegerField;
    buttonCetak: TcxButton;
    KodeQkode: TStringField;
    viewSJQKode: TStringField;
    viewSJQArmada: TStringField;
    viewSJQPeminta: TStringField;
    viewSJQBan: TStringField;
    viewSJQLokasiPasang: TStringField;
    viewSJQTglPasang: TDateTimeField;
    viewSJQKeterangan: TMemoField;
    viewSJQCreateDate: TDateTimeField;
    viewSJQCreateBy: TStringField;
    viewSJQOperator: TStringField;
    viewSJQTglEntry: TDateTimeField;
    viewSJQTglCetak: TDateTimeField;
    viewSJQStatusBan: TStringField;
    viewSJQjumlahban_armada: TIntegerField;
    viewSJQjenis_ban: TStringField;
    viewSJQmerk_ban: TStringField;
    banQ: TSDQuery;
    Panel1: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton1: TcxRadioButton;
    NlepasQ: TSDQuery;
    nLepasSource: TDataSource;
    newUpdateLepasUS: TSDUpdateSQL;
    newKodeQ: TSDQuery;
    newKodeQkode: TStringField;
    NviewLepasQ: TSDQuery;
    NViewLepasSource: TDataSource;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQKode: TStringField;
    MasterQArmada: TStringField;
    MasterQPeminta: TStringField;
    MasterQPelaksana: TStringField;
    MasterQBan: TStringField;
    MasterQLokasiPasang: TStringField;
    MasterQTglPasang: TDateTimeField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatusBan: TStringField;
    MasterQplatno_armada: TStringField;
    MasterQjumlahban_armada: TIntegerField;
    MasterQjenis_ban: TStringField;
    MasterQmerk_ban: TStringField;
    NlepasQKode: TStringField;
    NlepasQArmada: TStringField;
    NlepasQBan: TStringField;
    NlepasQPeminta: TStringField;
    NlepasQPelaksana: TStringField;
    NlepasQLokasiLepas: TStringField;
    NlepasQTglLepas: TDateTimeField;
    NlepasQPermasalahan: TMemoField;
    NlepasQCreateDate: TDateTimeField;
    NlepasQCreateBy: TStringField;
    NlepasQOperator: TStringField;
    NlepasQTglEntry: TDateTimeField;
    NlepasQTglCetak: TDateTimeField;
    NlepasQStatusBan: TStringField;
    NlepasQplatno_armada: TStringField;
    NlepasQjumlahban_armada: TIntegerField;
    NlepasQjenis_ban: TStringField;
    NlepasQmerk_ban: TStringField;
    NviewLepasQKode: TStringField;
    NviewLepasQArmada: TStringField;
    NviewLepasQBan: TStringField;
    NviewLepasQPeminta: TStringField;
    NviewLepasQPelaksana: TStringField;
    NviewLepasQLokasiLepas: TStringField;
    NviewLepasQTglLepas: TDateTimeField;
    NviewLepasQPermasalahan: TMemoField;
    NviewLepasQCreateDate: TDateTimeField;
    NviewLepasQCreateBy: TStringField;
    NviewLepasQOperator: TStringField;
    NviewLepasQTglEntry: TDateTimeField;
    NviewLepasQTglCetak: TDateTimeField;
    NviewLepasQStatusBan: TStringField;
    NviewLepasQjenis_ban: TStringField;
    NviewLepasQmerk_ban: TStringField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    banQKode: TStringField;
    banQKodeBarang: TStringField;
    banQKodeIDBan: TStringField;
    banQMerk: TStringField;
    banQJenis: TStringField;
    banQRing: TStringField;
    banQVelg: TStringField;
    banQStandardUmur: TIntegerField;
    banQStandardKm: TIntegerField;
    banQCreateDate: TDateTimeField;
    banQCreateBy: TStringField;
    banQOperator: TStringField;
    banQTglEntry: TDateTimeField;
    banQStatus: TStringField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    MasterQDetailPeminta: TStringField;
    MasterQDetailPelaksana: TStringField;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridjumlahban_armada: TcxDBEditorRow;
    MasterVGridplatno_armada: TcxDBEditorRow;
    MasterVGridPeminta: TcxDBEditorRow;
    MasterVGridDetailPeminta: TcxDBEditorRow;
    MasterVGridPelaksana: TcxDBEditorRow;
    MasterVGridDetailPelaksana: TcxDBEditorRow;
    MasterVGridBan: TcxDBEditorRow;
    MasterVGridjenis_ban: TcxDBEditorRow;
    MasterVGridmerk_ban: TcxDBEditorRow;
    MasterVGridkodeidban: TcxDBEditorRow;
    MasterVGridLokasiPasang: TcxDBEditorRow;
    MasterVGridTglPasang: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridStatusBan: TcxDBEditorRow;
    NlepasQDetailPeminta: TStringField;
    NlepasQDetailPelaksana: TStringField;
    NlepasQKodeidban: TStringField;
    DeleteBtn: TcxButton;
    viewSJQNoBodyArmada: TStringField;
    viewSJQNamaPeminta: TStringField;
    viewSJQPlatNoArmada: TStringField;
    NviewLepasQNoBodyArmada: TStringField;
    NviewLepasQPlatNoArmada: TStringField;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Armada: TcxGridDBColumn;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1Peminta: TcxGridDBColumn;
    cxGrid1DBTableView1Keluhan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    NviewLepasQNamaPeminta: TStringField;
    MasterQNoBody: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterQKedalamanAlur: TFloatField;
    MasterVGridKedalamanAlur: TcxDBEditorRow;
    NlepasQNoBody: TStringField;
    DetailArmadaBanUS: TSDUpdateSQL;
    DetailArmadaBanQ: TSDQuery;
    DetailArmadaBanQKodeBan: TStringField;
    DetailArmadaBanQKodeArmada: TStringField;
    DetailArmadaBanQKodeLokasiBan: TStringField;
    NlepasQKedalamanAlur: TFloatField;
    NviewLepasQKedalamanAlur: TFloatField;
    NviewLepasQplatno_armada: TStringField;
    NviewLepasQjumlahban_armada: TIntegerField;
    CheckPBQ: TSDQuery;
    CheckPBQKode: TStringField;
    CheckPBQArmada: TStringField;
    CheckPBQPeminta: TStringField;
    CheckPBQPelaksana: TStringField;
    CheckPBQBan: TStringField;
    CheckPBQKedalamanAlur: TFloatField;
    CheckPBQLokasiPasang: TStringField;
    CheckPBQTglPasang: TDateTimeField;
    CheckPBQKeterangan: TMemoField;
    CheckPBQCreateDate: TDateTimeField;
    CheckPBQCreateBy: TStringField;
    CheckPBQOperator: TStringField;
    CheckPBQTglEntry: TDateTimeField;
    CheckPBQTglCetak: TDateTimeField;
    CheckPBQStatusBan: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    CheckLBQ: TSDQuery;
    MasterQkodeidban: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    cxGridDBTableView1Column5: TcxGridDBColumn;
    cxGridDBTableView1Column4: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridBanEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxRadioButton2Click(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure MasterVGridPemintaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPelaksanaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridLokasiPasangEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PasangBanFm: TPasangBanFm;
  MasterOriSQL,MasterOriSQL2: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, ArmadaDropDown, BanDropDown,
  PegawaiDropDown, LokasiBanDropDown, LokasiLepasBanDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPasangBanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPasangBanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPasangBanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPasangBanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPasangBanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  MasterOriSQL2:=NlepasQ.sql.Text;
  ArmadaQ.Open;
  PegawaiQ.Open;
  DetailArmadaBanQ.Open;
  banQ.Open;
  if cxRadioButton1.Checked=true then
  begin
    KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  end
  else
  begin
    KodeEdit.Properties.MaxLength:=NlepasQKode.Size;
  end;
end;

procedure TPasangBanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  MasterQ.Close;
  MasterQ.Open;
  NlepasQ.Close;
  NlepasQ.Open;
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  {DMFm.GetDateQ.Open;
  if cxRadioButton1.Checked=true then
  begin
    MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  end
  else
  begin
    NlepasQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  end;
  DMFm.GetDateQ.Close; }
  MasterVGrid.Enabled:=true;
end;

procedure TPasangBanFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
  NlepasQ.Close;
end;

procedure TPasangBanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  NviewLepasQ.Open;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertLepasPasangBan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewSJQ.Close;
  viewSjQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSjQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewSjQ.Open;
  NviewLepasQ.Close;
  NviewLepasQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  NviewLepasQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  NviewLepasQ.Open;
end;

procedure TPasangBanFm.KodeEditExit(Sender: TObject);
begin
  if cxRadioButton1.Checked=true then
  begin
    if KodeEdit.Text<>'' then
    begin
      MasterQ.SQL.Clear;
      MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
      MasterQ.Open;
      if MasterQ.IsEmpty then
      begin
        StatusBar.Panels[0].Text:= 'Mode : Entry';
        MasterQ.Append;
        MasterQ.Edit;
        MasterQStatusBan.AsString:='Tukar';
      end
      else
      begin
        StatusBar.Panels[0].Text:= 'Mode : Edit';
        DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
      end;
      SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
      MasterVGrid.Enabled:=True;
    end;
  end
  else
  begin
    if KodeEdit.Text<>'' then
    begin
      NlepasQ.SQL.Clear;
      NlepasQ.SQL.Add('select * from ('+ MasterOriSQL2 +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
      NlepasQ.Open;
      if NlepasQ.IsEmpty then
      begin
        StatusBar.Panels[0].Text:= 'Mode : Entry';
        NlepasQ.Append;
        NlepasQ.Edit;
        NlepasQStatusBan.AsString:='Tukar';
      end
      else
      begin
        StatusBar.Panels[0].Text:= 'Mode : Edit';
        DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
      end;
      SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
      MasterVGrid.Enabled:=True;
    end;
  end;

end;

procedure TPasangBanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPasangBanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  if cxRadioButton1.Checked=true then
  begin
    kodeprint :=  MasterQKode.AsString;
    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
      kodeprint :=  MasterQKode.AsString;
      if MasterQStatusBan.AsString='0' then MasterQStatusBan.AsString:='GUDANG';
      MasterQ.Post;
      KodeQ.Close;
      KodeEdit.Text:=MasterQKode.AsString;
    end;

   // DetailArmadaBanQ.Insert;
   DetailArmadaBanQ.Close;
   DetailArmadaBanQ.ParamByName('text').AsString:=MasterQBan.AsString;
   DetailArmadaBanQ.ParamByName('text2').AsString:=MasterQArmada.AsString;
   DetailArmadaBanQ.ParamByName('text3').AsString:=MasterQLokasiPasang.AsString;
   DetailArmadaBanQ.Open;
    DetailArmadaBanQ.Edit;
    DetailArmadaBanQKodeBan.AsString:=MasterQBan.AsString;
    DetailArmadaBanQKodeArmada.AsString:=MasterQArmada.AsString;
    DetailArmadaBanQKodeLokasiBan.AsString:=MasterQLokasiPasang.AsString;
    DetailArmadaBanQ.Post;

    MenuUtamaFm.Database1.StartTransaction;
    try
      DetailArmadaBanQ.ApplyUpdates;
      MasterQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      DetailArmadaBanQ.CommitUpdates;
      MasterQ.CommitUpdates;
      ShowMessage('Lepas Pasang ban dengan Kode ' + KodeEdit.Text + ' telah disimpan');
      viewSJQ.Close;
      viewSJQ.Open;
      KodeEdit.SetFocus;
    except
      on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailArmadaBanQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
    end;
  end
  else
  begin
    kodeprint :=  NlepasQKode.AsString;
    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      NlepasQ.Edit;
      newKodeQ.Open;
      if newKodeQ.IsEmpty then
      begin
        NlepasQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        NlepasQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(newKodeQkode.AsString)+1),10,'0',True);
      end;
      kodeprint :=  NlepasQKode.AsString;
      if MasterQStatusBan.AsString='0' then MasterQStatusBan.AsString:='RUSAK';
      NlepasQ.Post;
      newKodeQ.Close;
      KodeEdit.Text:=NlepasQKode.AsString;
    end;
    MenuUtamaFm.Database1.StartTransaction;
    try
      NlepasQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      NlepasQ.CommitUpdates;
      ShowMessage('Lepas Pasang Ban dengan Kode ' + KodeEdit.Text + ' telah disimpan');
      NviewLepasQ.Close;
      NviewLepasQ.Open;
      KodeEdit.SetFocus;
    except
      on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      NLepasQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
    end;
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TPasangBanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  if cxRadioButton1.Checked=true then
  begin
    //DMFm.GetDateQ.Open;
    //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
    MasterQCreateBy.AsString:=User;
    //DMFm.GetDateQ.Close;
  end
  else
  begin
    //DMFm.GetDateQ.Open;
   // NlepasQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
    NlepasQCreateBy.AsString:=User;
   // DMFm.GetDateQ.Close;
  end;

end;

procedure TPasangBanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  if cxRadioButton1.Checked=true then
  begin
   // DMFm.GetDateQ.Open;
   // MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
    MasterQOperator.AsString:=User;
   // DMFm.GetDateQ.Close;
  end
  else
  begin
   // DMFm.GetDateQ.Open;
   // NlepasQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
    NlepasQOperator.AsString:=User;
   // DMFm.GetDateQ.Close;
  end;
end;

procedure TPasangBanFm.DeleteBtnClick(Sender: TObject);
begin
  if cxRadioButton1.Checked=true then
    begin
      CheckPBQ.Close;
      CheckPBQ.ParamByName('par1').AsString:= MasterQArmada.AsString;
      CheckPBQ.ParamByName('par2').AsString:= MasterQLokasiPasang.AsString;
      CheckPBQ.ParamByName('par3').AsString:= MasterQKode.AsString;
      CheckPBQ.Open;
      if CheckPBQ.RecordCount>=1 then
        begin
          CheckPBQ.Last;
          ShowMessage(CheckPBQCreateDate.AsString+' '+MasterQCreateDate.AsString);
          if CheckPBQCreateDate.AsDateTime > MasterQCreateDate.AsDateTime then
            begin
              ShowMessage('Penghapusan gagal. Terdapat history pasang ban lebih baru.');
            end
          else
            begin
              kodeprint :=  MasterQKode.AsString;
              MenuUtamaFm.Database1.StartTransaction;
              try
                MasterQ.Delete;
                MasterQ.ApplyUpdates;
                MenuUtamaFm.Database1.Commit;
                MasterQ.CommitUpdates;
                ShowMessage('Pasang ban dengan Kode ' + KodeEdit.Text + ' telah dihapus');
                viewSJQ.Close;
                viewSJQ.Open;
                KodeEdit.SetFocus;
              except
                on E : Exception do begin
                  ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
                  MenuUtamaFm.Database1.Rollback;
                  MasterQ.RollbackUpdates;
                  MessageDlg('Pasang Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
                end;
              end;
            end;
        end
      else
        begin
          kodeprint :=  MasterQKode.AsString;
          MenuUtamaFm.Database1.StartTransaction;
          try
            MasterQ.Delete;
            MasterQ.ApplyUpdates;
            MenuUtamaFm.Database1.Commit;
            MasterQ.CommitUpdates;
            ShowMessage('Pasang ban dengan Kode ' + KodeEdit.Text + ' telah dihapus');
            viewSJQ.Close;
            viewSJQ.Open;
            KodeEdit.SetFocus;
          except
            on E : Exception do begin
              ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
              MenuUtamaFm.Database1.Rollback;
              MasterQ.RollbackUpdates;
              MessageDlg('Pasang Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
            end;
          end;
        end;
    end
  else
    begin
      CheckLBQ.Close;
      CheckLBQ.ParamByName('ard').AsString:= NlepasQArmada.AsString;
      CheckLBQ.ParamByName('lokb').AsString:= NlepasQLokasiLepas.AsString;
      CheckLBQ.ParamByName('ban').AsString:= NlepasQBan.AsString;
      CheckLBQ.Open;
      if CheckLBQ.RecordCount>=1 then
        begin
          ShowMessage('Penghapusan gagal. Lokasi lepas / ban sudah terpakai.');
        end
      else
        begin
          kodeprint :=  NlepasQKode.AsString;
          MenuUtamaFm.Database1.StartTransaction;
          try
            NlepasQ.Delete;
            NlepasQ.ApplyUpdates;
            MenuUtamaFm.Database1.Commit;
            NlepasQ.CommitUpdates;
            ShowMessage('Lepas Ban dengan Kode ' + KodeEdit.Text + ' telah dihapus.');
            NviewLepasQ.Close;
            NviewLepasQ.Open;
            KodeEdit.SetFocus;
          except
            on E : Exception do begin
              ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
              MenuUtamaFm.Database1.Rollback;
              NLepasQ.RollbackUpdates;
              MessageDlg('Lepas Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
            end;
          end;
        end;  
    end;
    //ShowMessage('akhir');
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TPasangBanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPasangBanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
{  kodeedit.Text:=viewSJQKode.AsString;
  KodeEditExit(self);
  SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
  MasterVGrid.Enabled:=True;  }
    //cxDBVerticalGrid1.Enabled:=True;


    MasterVGrid.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewSJQKode.AsString+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= viewSJQKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
   MasterVGrid.Enabled:=true;

end;

procedure TPasangBanFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    sopirQ.Close;
    sopirQ.ExecSQL;
    sopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      //MasterQnama_sopir.AsString := sopirQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPasangBanFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPasangBanFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Surat Jalan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TPasangBanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TPasangBanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    if cxRadioButton1.Checked=true then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:= ArmadaDropDownFm.kode;
    end
    else
    begin
      NlepasQ.Open;
      NlepasQ.Edit;
      NlepasQArmada.AsString:= ArmadaDropDownFm.kode;
    end;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TPasangBanFm.MasterVGridBanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BanDropDownFm:=TBanDropDownFm.Create(self);
  if BanDropDownFm.ShowModal=MrOK then
  begin
    if cxRadioButton1.Checked=true then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBan.AsString:=BanDropDownFm.kode;
    end
    else
    begin
      NlepasQ.Open;
      NlepasQ.Edit;
      NlepasQBan.AsString:=BanDropDownFm.kode;
    end;
  end;
  BanDropDownFm.Release;
  //ShowMessage(MasterQkodeidban.AsString);
end;

procedure TPasangBanFm.cxRadioButton2Click(Sender: TObject);
begin
  MasterQ.Close;
  NlepasQ.Close;
  MasterVGrid.DataController.DataSource:=nLepasSource;
  MasterVGridTglPasang.Properties.DataBinding.FieldName:='tgllepas';
  MasterVGridLokasiPasang.Properties.DataBinding.FieldName:='lokasilepas';
  MasterVGridKeterangan.Properties.DataBinding.FieldName:='permasalahan';
  MasterVGridBan.Properties.Options.Editing:=False;
  TcxRadioGroupProperties(MasterVGridStatusBan.Properties.EditProperties).Items[0].Caption:='Rusak';
  TcxRadioGroupProperties(MasterVGridStatusBan.Properties.EditProperties).Items[0].VALUE:='RUSAK';
  cxGrid1DBTableView1.DataController.DataSource:=NViewLepasSource;
  NviewLepasQ.Close;
  NviewLepasQ.open;
  KodeEditEnter(sender);
  cxGrid2.Visible:=True;
  cxGrid1.Visible:=False;
end;


procedure TPasangBanFm.cxRadioButton1Click(Sender: TObject);
begin
  MasterQ.Close;
  NlepasQ.Close;
  MasterVGrid.DataController.DataSource:=masterds;
  MasterVGridTglPasang.Properties.DataBinding.FieldName:='tglpasang';
  MasterVGridLokasiPasang.Properties.DataBinding.FieldName:='lokasipasang';
  MasterVGridKeterangan.Properties.DataBinding.FieldName:='keterangan';
  MasterVGridBan.Properties.Options.Editing:=True;
  TcxRadioGroupProperties(MasterVGridStatusBan.Properties.EditProperties).Items[0].Caption:='Gudang';
  TcxRadioGroupProperties(MasterVGridStatusBan.Properties.EditProperties).Items[0].VALUE:='GUDANG';
  cxGrid1DBTableView1.DataController.DataSource:=sjds;
  viewSJQ.Close;
  viewSJQ.Open;
  KodeEditEnter(sender);
  cxGrid2.Visible:=False;
  cxGrid1.Visible:=True;
end;

procedure TPasangBanFm.RadioButton1Click(Sender: TObject);
begin
  MasterVGridArmada.Visible:=true;

  //MasterVGridjumlahban_armada.Visible:=true;
  //MasterVGridplatno_armada.Visible:=true;
  //MasterVGridEkor.Visible:=false;
  //MasterVGridjumlahban_ekor.Visible:=False;
end;

procedure TPasangBanFm.RadioButton2Click(Sender: TObject);
begin
  MasterVGridArmada.Visible:=False;
  //MasterVGridjumlahban_armada.Visible:=False;
  //MasterVGridplatno_armada.Visible:=False;
  //MasterVGridEkor.Visible:=true;
  //MasterVGridjumlahban_ekor.Visible:=true;
end;

procedure TPasangBanFm.MasterVGridPemintaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    if cxRadioButton1.Checked=true then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPeminta.AsString:=PegawaiDropDownFm.kode;
    end
    else
    begin
      NlepasQ.Open;
      NlepasQ.Edit;
      NlepasQPeminta.AsString:=PegawaiDropDownFm.kode;
    end;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TPasangBanFm.MasterVGridPelaksanaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    if cxRadioButton1.Checked=true then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelaksana.AsString:=PegawaiDropDownFm.kode;
    end
    else
    begin
      NlepasQ.Open;
      NlepasQ.Edit;
      NlepasQPelaksana.AsString:=PegawaiDropDownFm.kode;
    end;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TPasangBanFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
{  kodeedit.Text:=NviewLepasQKode.AsString;
  KodeEditExit(self);
  SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
  MasterVGrid.Enabled:=True;   }

if cxRadioButton1.Checked=true then
begin
    MasterVGrid.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewSJQKode.AsString+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= viewSJQKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
   MasterVGrid.Enabled:=true;
end

else if cxRadioButton2.Checked=true then
begin
    MasterVGrid.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+NviewLepasQKode.AsString+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= NviewLepasQKode.AsString;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteLepasPasangBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateLepasPasangBan.AsBoolean;
   MasterVGrid.Enabled:=true;
end;
end;

procedure TPasangBanFm.MasterVGridLokasiPasangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxRadioButton1.Checked=true then
  begin
    LokasiBanDropDownFm:=TLokasiBanDropDownFm.Create(self,MasterQArmada.AsString);
    if LokasiBanDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQLokasiPasang.AsString:= LokasiBanDropDownFm.kode;
    end;
    LokasiBanDropDownFm.Release;
  end
  else if cxRadioButton2.Checked=true then
  begin
    LokasiLepasBanDropDownFm:=TLokasiLepasBanDropDownFm.Create(self,NlepasQArmada.AsString);
    if LokasiLepasBanDropDownFm.ShowModal=MrOK then
    begin
      NlepasQ.Open;
      NlepasQ.Edit;
      NlepasQLokasiLepas.AsString:= LokasiLepasBanDropDownFm.kode;
      NlepasQBan.AsString:= LokasiLepasBanDropDownFm.ban;
    end;
    LokasiLepasBanDropDownFm.Release;
  end
end;

procedure TPasangBanFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSjQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSjQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSjQ.Open;
  NviewLepasQ.Close;
  NviewLepasQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  NviewLepasQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  NviewLepasQ.Open;
end;

procedure TPasangBanFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSjQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSjQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSjQ.Open;
  NviewLepasQ.Close;
  NviewLepasQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  NviewLepasQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  NviewLepasQ.Open;
end;

end.
