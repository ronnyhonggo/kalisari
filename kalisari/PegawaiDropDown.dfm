object PegawaiDropDownFm: TPegawaiDropDownFm
  Left = 397
  Top = 108
  Width = 903
  Height = 498
  Caption = 'PegawaiDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 887
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1Nama: TcxGridDBColumn
        DataBinding.FieldName = 'Nama'
        Width = 181
      end
      object cxGrid1DBTableView1Jabatan: TcxGridDBColumn
        DataBinding.FieldName = 'Jabatan'
        Width = 116
      end
      object cxGrid1DBTableView1Alamat: TcxGridDBColumn
        DataBinding.FieldName = 'Alamat'
        Width = 260
      end
      object cxGrid1DBTableView1Kota: TcxGridDBColumn
        DataBinding.FieldName = 'Kota'
        Width = 113
      end
      object cxGrid1DBTableView1NoTelp: TcxGridDBColumn
        DataBinding.FieldName = 'NoTelp'
        Width = 100
      end
      object cxGrid1DBTableView1NoHP: TcxGridDBColumn
        DataBinding.FieldName = 'NoHP'
        Width = 119
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai where jabatan=:text')
    Left = 216
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object LPBDs: TDataSource
    DataSet = PegawaiQ
    Left = 248
    Top = 368
  end
end
