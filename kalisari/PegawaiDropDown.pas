unit PegawaiDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TPegawaiDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    PegawaiQ: TSDQuery;
    LPBDs: TDataSource;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Alamat: TcxGridDBColumn;
    cxGrid1DBTableView1Kota: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelp: TcxGridDBColumn;
    cxGrid1DBTableView1NoHP: TcxGridDBColumn;
    cxGrid1DBTableView1Jabatan: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  PegawaiDropDownFm: TPegawaiDropDownFm;
  PegawaiOriSQL,jenis:string;

implementation

{$R *.dfm}

constructor TPegawaiDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  jenis:=kd;
end;

constructor TPegawaiDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  jenis:='';
end;

procedure TPegawaiDropDownFm.FormCreate(Sender: TObject);
begin
  PegawaiOriSQL:=PegawaiQ.SQL.Text;
  if jenis='' then
    begin
      PegawaiQ.Close;
      PegawaiQ.SQL.Text:='select * from pegawai';
      PegawaiQ.Open;
    end
  else
    begin
      PegawaiQ.Close;
      PegawaiQ.ParamByName('text').AsString:= jenis;
      PegawaiQ.Open;
    end;
end;

procedure TPegawaiDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=PegawaiQKode.AsString;
  ModalResult:=mrOK;
end;

end.
