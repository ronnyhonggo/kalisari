unit PelepasanBan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox;

type
  TPelepasanBanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQPanjang: TFloatField;
    ArmadaQKapasitas: TFloatField;
    ArmadaQAktif: TBooleanField;
    ArmadaQKeterangan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSopir: TStringField;
    ArmadaQEkor: TStringField;
    ArmadaQKode_1: TStringField;
    ArmadaQNama: TStringField;
    ArmadaQAlamat: TStringField;
    ArmadaQNotelp: TStringField;
    ArmadaQCreateDate_1: TDateTimeField;
    ArmadaQCreateBy_1: TStringField;
    ArmadaQOperator_1: TStringField;
    ArmadaQTglEntry_1: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    sopirQ: TSDQuery;
    sopirQKode: TStringField;
    sopirQNama: TStringField;
    sopirQAlamat: TStringField;
    sopirQNotelp: TStringField;
    sopirQCreateDate: TDateTimeField;
    sopirQCreateBy: TStringField;
    sopirQOperator: TStringField;
    sopirQTglEntry: TDateTimeField;
    Crpe1: TCrpe;
    ekorQ: TSDQuery;
    ekorQkode: TStringField;
    ekorQpanjang: TStringField;
    ekorQberat: TStringField;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    jarakQJarak: TIntegerField;
    updateQ: TSDQuery;
    StringField1: TStringField;
    MemoField1: TMemoField;
    MemoField2: TMemoField;
    IntegerField1: TIntegerField;
    buttonCetak: TcxButton;
    SOQNOSO: TStringField;
    SOQNAMA_PELANGGAN: TStringField;
    SOQALAMAT_PELANGGAN: TStringField;
    SOQTLP_PELANGGAN: TStringField;
    SOQNAMA_SOPIR: TStringField;
    SOQEKOR_ARMADA: TStringField;
    SOQTGL_ORDER: TDateTimeField;
    SOQKODE_ARMADA: TStringField;
    SOQPLAT_ARMADA: TStringField;
    SOQKODESOPIR: TStringField;
    SOQKODE_RUTE: TStringField;
    SOQMUAT: TStringField;
    SOQBONGKAR: TStringField;
    SOQDARIPT: TMemoField;
    SOQDARIALAMAT: TMemoField;
    SOQKEPT: TMemoField;
    SOQKEALAMAT: TMemoField;
    SOQCHECKER: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Armada: TcxGridDBColumn;
    cxGrid1DBTableView1Ekor: TcxGridDBColumn;
    cxGrid1DBTableView1Peminta: TcxGridDBColumn;
    cxGrid1DBTableView1Keluhan: TcxGridDBColumn;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    viewSJQKode: TStringField;
    viewSJQArmada: TStringField;
    viewSJQEkor: TStringField;
    viewSJQPeminta: TStringField;
    viewSJQKeluhan: TMemoField;
    viewSJQkode_lp: TStringField;
    viewSJQPlatNo: TStringField;
    viewSJQNama: TStringField;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQArmada: TStringField;
    MasterQBan: TStringField;
    MasterQPeminta: TStringField;
    MasterQLokasiLepas: TStringField;
    MasterQTglLepas: TDateTimeField;
    MasterQPermasalahan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatusBan: TStringField;
    MasterQEkor: TStringField;
    MasterQplatno_armada: TStringField;
    MasterQjumlahban_armada: TIntegerField;
    MasterQjenis_ban: TStringField;
    MasterQmerk_ban: TStringField;
    MasterQjumlahban_ekor: TIntegerField;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridBan: TcxDBEditorRow;
    MasterVGridPeminta: TcxDBEditorRow;
    MasterVGridLokasiLepas: TcxDBEditorRow;
    MasterVGridTglLepas: TcxDBEditorRow;
    MasterVGridPermasalahan: TcxDBEditorRow;
    MasterVGridStatusBan: TcxDBEditorRow;
    MasterVGridEkor: TcxDBEditorRow;
    MasterVGridplatno_armada: TcxDBEditorRow;
    MasterVGridjumlahban_armada: TcxDBEditorRow;
    MasterVGridjenis_ban: TcxDBEditorRow;
    MasterVGridmerk_ban: TcxDBEditorRow;
    MasterVGridjumlahban_ekor: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PelepasanBanFm: TPelepasanBanFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPelepasanBanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPelepasanBanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPelepasanBanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPelepasanBanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPelepasanBanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPelepasanBanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TPelepasanBanFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPelepasanBanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  viewSJQ.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
end;

procedure TPelepasanBanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //MasterQKirKepala.AsBoolean :=false;
      //MasterQKirEkor.AsBoolean :=false;
      //MasterQSTNK.AsBoolean :=false;
      //MasterQPajak.AsBoolean :=false;
      //MasterQStatus.AsString :='IN PROGRESS';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TPelepasanBanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPelepasanBanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Surat Jalan dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    viewSJQ.Close;
    viewSJQ.Open;
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TPelepasanBanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TPelepasanBanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TPelepasanBanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Surat Jalan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Surat Jalan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TPelepasanBanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterQtgl_order.AsDateTime :=  SOQtgl_order.AsDateTime;
      //MasterQnama_pelanggan.AsString := SOQnama_pelanggan.AsString;
      //MasterQtlp_pelanggan.AsString :=    SOQtlp_pelanggan.AsString;
      //MasterQalamat_pelanggan.AsString :=    SOQalamat_pelanggan.asString;
      //MasterQkode_armada.AsString :=  SOQkode_armada.AsString;
      //MasterQekor.AsString :=  SOQekor_armada.AsString;
      //MasterQsopir.AsString :=  SOQkodesopir.AsString;
      //MasterQplat_armada.AsString := SOQplat_armada.AsString;
      //MasterQnama_sopir.AsString := SOQnama_sopir.AsString;
      //MasterQmuat.AsString := SOQmuat.AsString;
      //MasterQbongkar.AsString := SOQbongkar.AsString;
      //MasterQdaript.AsString := SOQdaript.AsString;
      //MasterQdarialamat.AsString := SOQdarialamat.AsString;
      //MasterQkept.AsString := SOQkept.AsString;
      //MasterQkealamat.AsString := SOQkealamat.AsString;
      //MasterQkode_rute.AsString := SOQkode_rute.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    buttonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewSJQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

procedure TPelepasanBanFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    sopirQ.Close;
    sopirQ.ExecSQL;
    sopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      //MasterQnama_sopir.AsString := sopirQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Surat Jalan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
      Crpe1.Connect.UserID:='sa';
      Crpe1.Connect.Password:='sa';
      Crpe1.Connect.ServerName:='';
      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TPelepasanBanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ekorQ.Close;
    ekorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ekorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQEkor.AsString:= ekorQkode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPelepasanBanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:= ArmadaQKode.AsString;
      //MasterQPlatNo.AsString:= ArmadaQPlatNo.AsString;
      //MasterQNama.AsString:= ArmadaQNama.AsString;
    end;
    DropDownFm.Release;
end;

end.
