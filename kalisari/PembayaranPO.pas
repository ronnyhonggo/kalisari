unit PembayaranPO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, cxVGrid, cxDBVGrid, cxInplaceContainer,
  SDEngine, cxTextEdit, cxMaskEdit, cxButtonEdit, ComCtrls, ExtCtrls,
  cxMemo, cxCalendar, cxDropDownEdit, cxCheckBox, cxLabel, cxGroupBox;

type
  TPembayaranPOFm = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel3: TPanel;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    DSPO: TDataSource;
    SDUPO: TSDUpdateSQL;
    DPOQ: TSDQuery;
    DSDPO: TDataSource;
    POQ: TSDQuery;
    POQKode: TStringField;
    POQSupplier: TStringField;
    POQTermin: TStringField;
    POQTglKirim: TDateTimeField;
    POQGrandTotal: TCurrencyField;
    POQStatus: TStringField;
    POQCreateDate: TDateTimeField;
    POQCreateBy: TStringField;
    POQOperator: TStringField;
    POQTglEntry: TDateTimeField;
    POQTglCetak: TDateTimeField;
    POQStatusKirim: TStringField;
    POQDiskon: TCurrencyField;
    POQKirim: TBooleanField;
    POQAmbil: TBooleanField;
    POQTglBayar: TDateTimeField;
    POQMetodePembayaran: TStringField;
    POQTotalDibayar: TCurrencyField;
    POQNomorBukti: TStringField;
    POQNomorFaktur: TStringField;
    POQNamaPT: TStringField;
    POQSesuaiPO: TBooleanField;
    cxGrid3DBTableView1Kode: TcxGridDBColumn;
    cxGrid3DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid3DBTableView1Status: TcxGridDBColumn;
    cxGrid3DBTableView1MetodePembayaran: TcxGridDBColumn;
    cxGrid3DBTableView1TotalDibayar: TcxGridDBColumn;
    cxGrid3DBTableView1NomorBukti: TcxGridDBColumn;
    cxGrid3DBTableView1NomorFaktur: TcxGridDBColumn;
    cxGrid3DBTableView1NamaPT: TcxGridDBColumn;
    cxGrid3DBTableView1SesuaiPO: TcxGridDBColumn;
    Panel2: TPanel;
    BtnSave: TButton;
    Button2: TButton;
    ExitBtn: TButton;
    DaftarBeliQ: TSDQuery;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQHargaLast2: TCurrencyField;
    DaftarBeliQHargaLast3: TCurrencyField;
    DaftarBeliQTglLast: TDateTimeField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQSupplier1: TStringField;
    DaftarBeliQSupplier2: TStringField;
    DaftarBeliQSupplier3: TStringField;
    DaftarBeliQHarga1: TCurrencyField;
    DaftarBeliQHarga2: TCurrencyField;
    DaftarBeliQHarga3: TCurrencyField;
    DaftarBeliQTermin1: TIntegerField;
    DaftarBeliQTermin2: TIntegerField;
    DaftarBeliQTermin3: TIntegerField;
    DaftarBeliQKeterangan1: TMemoField;
    DaftarBeliQKeterangan2: TMemoField;
    DaftarBeliQKeterangan3: TMemoField;
    DaftarBeliQTermin: TIntegerField;
    DaftarBeliQBeli1: TIntegerField;
    DaftarBeliQBeli2: TIntegerField;
    DaftarBeliQBeli3: TIntegerField;
    DaftarBeliQUrgent: TBooleanField;
    DPOQKodePO: TStringField;
    DPOQKodeDaftarBeli: TStringField;
    DPOQStatusPO: TStringField;
    DPOQKodeBarang: TStringField;
    DPOQNamaBarang: TStringField;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    DPOQJumlahBeli: TIntegerField;
    cxGrid1DBTableView1JumlahBeli: TcxGridDBColumn;
    JumlahDiterimaQ: TSDQuery;
    JumlahDiterimaQJumlahDiterima: TIntegerField;
    DPOQJumlahDiterima: TStringField;
    cxGrid1DBTableView1JumlahDiterima: TcxGridDBColumn;
    SDUDPO: TSDUpdateSQL;
    HargaBeliQ: TSDQuery;
    DPOQHargaBeli: TCurrencyField;
    cxGrid1DBTableView1HargaBeli: TcxGridDBColumn;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    POQNamaSupplier: TStringField;
    cxGrid3DBTableView1NamaSupplier: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    BarangQJumlah: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid3DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid3DBTableView1SesuaiPOPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PembayaranPOFm: TPembayaranPOFm;
  MStokOpnameOriSQL,DStokOpnameOriSQL,DetailKeluarOriSQL: string;
  paramkode :string;



implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

procedure TPembayaranPOFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPembayaranPOFm.FormShow(Sender: TObject);
begin
  //POQ.Open;
  //POQ.Edit;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  POQ.Close;
  POQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  POQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  POQ.Open;
  BtnSave.Enabled:=menuutamafm.UserQUpdateBayarPO.AsBoolean;
end;

procedure TPembayaranPOFm.ExitBtnClick(Sender: TObject);
begin
  Close;
  Release;
end;

procedure TPembayaranPOFm.BtnSaveClick(Sender: TObject);
begin
  POQ.Open;
  POQ.First;
  POQ.Edit;
  while POQ.Eof=false do begin
    POQ.Edit;
    if (POQMetodePembayaran.AsString<>'') and (POQTotalDibayar.AsInteger>0) and
       (POQNomorBukti.AsString<>'') and (POQNomorFaktur.AsString<>'') and
       (POQNamaPT.AsString<>'') then begin
       POQStatus.AsString:='PAID';
       POQTglBayar.AsDateTime:=Date;
    end;
    POQ.Next;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    POQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    POQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      POQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TPembayaranPOFm.cxGrid3DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  DPOQ.Close;
  DPOQ.ParamByName('text').AsString:=POQKode.AsString;
  DPOQ.Open;
  DPOQ.First;
  DPOQ.Edit;
  while DPOQ.Eof=False do begin
    DPOQ.Edit;
    JumlahDiterimaQ.Close;
    JumlahDiterimaQ.ParamByName('text').AsString:=POQKode.AsString;
    JumlahDiterimaQ.ParamByName('text2').AsString:=DPOQKodeBarang.AsString;
    JumlahDiterimaQ.Open;
    DPOQJumlahDiterima.AsInteger:=JumlahDiterimaQJumlahDiterima.AsInteger;
    DPOQ.Next;
  end;
end;

procedure TPembayaranPOFm.cxGrid3DBTableView1SesuaiPOPropertiesEditValueChanged(
  Sender: TObject);
begin
  if POQSesuaiPO.AsBoolean=true then
    POQTotalDibayar.AsInteger:=POQGrandTotal.AsInteger
  else
    POQTotalDibayar.AsInteger:=0;
end;

procedure TPembayaranPOFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  POQ.Close;
  POQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  POQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  POQ.Open;
end;

procedure TPembayaranPOFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  POQ.Close;
  POQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  POQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  POQ.Open;
end;

end.
