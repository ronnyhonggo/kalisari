object PembuatanKwitansiFm: TPembuatanKwitansiFm
  Left = 132
  Top = 50
  Align = alCustom
  AutoScroll = False
  Caption = 'Pembuatan Kwitansi'
  ClientHeight = 623
  ClientWidth = 1197
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  DockSite = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 598
    Width = 1197
    Height = 25
    Panels = <
      item
        Width = 50
      end>
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1197
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 16
      Caption = 'Kode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 56
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.MaxLength = 0
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxButton3: TcxButton
      Left = 360
      Top = 7
      Width = 75
      Height = 33
      Caption = 'LUNASI'
      TabOrder = 1
      OnClick = cxButton3Click
    end
    object cxTextEdit2: TcxTextEdit
      Left = 474
      Top = 14
      TabOrder = 2
      OnKeyUp = cxTextEdit1KeyUp
      Width = 137
    end
    object cxButton4: TcxButton
      Left = 616
      Top = 7
      Width = 75
      Height = 33
      Caption = 'COPY'
      TabOrder = 3
      OnClick = cxButton4Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 361
    Height = 400
    Align = alLeft
    Caption = 'Panel2'
    TabOrder = 2
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 359
      Height = 48
      Align = alTop
      Color = clBtnHighlight
      TabOrder = 0
      object RbtKontrak: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Kontrak'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = RbtKontrakClick
      end
      object RbtSO: TRadioButton
        Left = 104
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Sales Order'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        TabStop = True
        OnClick = RbtSOClick
      end
    end
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 49
      Width = 359
      Height = 350
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 177
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 1
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridTanggal: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tanggal'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridNominal: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.EditProperties.ReadOnly = True
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = MasterVGridNominalEditPropertiesValidate
        Properties.DataBinding.FieldName = 'Nominal'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridPPN: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = MasterVGridPPNEditPropertiesValidate
        Properties.DataBinding.FieldName = 'PPN'
        Visible = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MarkUpNominal: TcxDBEditorRow
        Properties.Caption = 'KomisiPelangganNominal'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = MarkUpNominalEditPropertiesValidate
        Properties.DataBinding.FieldName = 'MarkUpNominal'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridPelanggan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PelangganEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Pelanggan'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridNamaPelanggan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'NamaPelanggan'
        ID = 5
        ParentID = 4
        Index = 0
        Version = 1
      end
      object MasterVGridPenerimaPembayaran: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PenerimaPembayaranEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'PenerimaPembayaran'
        ID = 6
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridNamaPenerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'NamaPenerima'
        ID = 7
        ParentID = 6
        Index = 0
        Version = 1
      end
      object MasterVGridJabatanPenerima: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'JabatanPenerima'
        ID = 8
        ParentID = 6
        Index = 1
        Version = 1
      end
      object MasterVGridJenisKendaraan1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JenisKendaraan'
        ID = 9
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridUntuk: TcxDBEditorRow
        Properties.Caption = 'Keperluan'
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = 'PELUNASAN'
            Value = 'pelunasan'
          end
          item
            Caption = 'UANG MUKA'
            Value = 'uang muka'
          end>
        Properties.DataBinding.FieldName = 'Untuk'
        ID = 10
        ParentID = -1
        Index = 7
        Version = 1
      end
      object MasterVGridTanggalPembayaran: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxDateEditProperties'
        Properties.EditProperties.ImmediateDropDownWhenActivated = True
        Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
        Properties.EditProperties.SaveTime = False
        Properties.EditProperties.ShowTime = False
        Properties.DataBinding.FieldName = 'TanggalPembayaran'
        ID = 11
        ParentID = -1
        Index = 8
        Version = 1
      end
      object MasterVGridCaraPembayaran: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Items.Strings = (
          'Cash'
          'Transfer BCA'
          'Transfer Mandiri'
          'EDC BCA'
          'EDC Mandiri'
          'EDC Niaga'
          'Transfer BCA KCJ'
          'Transfer Mandiri KCJ'
          'Transfer Bank Jatim KCJ'
          'Giro')
        Properties.DataBinding.FieldName = 'CaraPembayaran'
        ID = 12
        ParentID = -1
        Index = 9
        Version = 1
      end
      object MasterVGridKeteranganPembayaran: TcxDBEditorRow
        Height = 45
        Properties.DataBinding.FieldName = 'KeteranganPembayaran'
        ID = 13
        ParentID = -1
        Index = 10
        Version = 1
      end
      object MasterVGridPeriode: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Periode'
        ID = 14
        ParentID = -1
        Index = 11
        Version = 1
      end
      object MasterVGridPPh: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.OnChange = MasterVGridPPhEditPropertiesChange
        Properties.DataBinding.FieldName = 'PPh'
        ID = 15
        ParentID = -1
        Index = 12
        Version = 1
      end
      object MasterVGridTitipKuitansi: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.OnEditValueChanged = MasterVGridTitipKuitansiEditPropertiesEditValueChanged
        Properties.DataBinding.FieldName = 'TitipKuitansi'
        ID = 16
        ParentID = -1
        Index = 13
        Version = 1
      end
      object MasterVGridTitipTagihan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TitipTagihan'
        ID = 17
        ParentID = -1
        Index = 14
        Version = 1
      end
      object MasterVGridDiskon: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = MasterVGridDiskonEditPropertiesValidate
        Properties.DataBinding.FieldName = 'Diskon'
        ID = 18
        ParentID = -1
        Index = 15
        Version = 1
      end
      object MasterVGridGrandTotal: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.DataBinding.FieldName = 'GrandTotal'
        Properties.Options.Editing = False
        ID = 19
        ParentID = -1
        Index = 16
        Version = 1
      end
      object MasterVGridMarkUp: TcxDBEditorRow
        Properties.Caption = 'KomisiPelanggan'
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.EditProperties.UseDisplayFormatWhenEditing = True
        Properties.EditProperties.UseThousandSeparator = True
        Properties.EditProperties.OnValidate = MasterVGridMarkUpEditPropertiesValidate
        Properties.DataBinding.FieldName = 'MarkUp'
        ID = 20
        ParentID = -1
        Index = 17
        Version = 1
      end
      object MasterVGridSisaBayar: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
        Properties.DataBinding.FieldName = 'SisaBayar'
        Properties.Options.Editing = False
        ID = 21
        ParentID = -1
        Index = 18
        Version = 1
      end
      object MasterVGridStatusKwitansi: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'StatusKwitansi'
        ID = 22
        ParentID = -1
        Index = 19
        Version = 1
      end
    end
  end
  object Panel3: TPanel
    Left = 361
    Top = 48
    Width = 836
    Height = 400
    Align = alClient
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 834
      Height = 398
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1KodeSO: TcxGridDBColumn
          DataBinding.FieldName = 'KodeSO'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KodeSOPropertiesButtonClick
        end
        object cxGrid1DBTableView1TglBerangkat: TcxGridDBColumn
          DataBinding.FieldName = 'TglBerangkat'
        end
        object cxGrid1DBTableView1Dari: TcxGridDBColumn
          DataBinding.FieldName = 'Dari'
          Width = 118
        end
        object cxGrid1DBTableView1Tujuan: TcxGridDBColumn
          DataBinding.FieldName = 'Tujuan'
          Width = 123
        end
        object cxGrid1DBTableView1JumlahBayar: TcxGridDBColumn
          DataBinding.FieldName = 'JumlahBayar'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = 'Rp,0;(Rp,0)'
          Width = 108
        end
        object cxGrid1DBTableView1SisaBayar: TcxGridDBColumn
          DataBinding.FieldName = 'SisaBayar'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = 'Rp,0;(Rp,0)'
          Width = 89
        end
        object cxGrid1DBTableView1Nominal: TcxGridDBColumn
          DataBinding.FieldName = 'Nominal'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.UseDisplayFormatWhenEditing = True
          Properties.UseThousandSeparator = True
          Properties.OnValidate = cxGrid1DBTableView1NominalPropertiesValidate
          Width = 105
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 834
      Height = 398
      Align = alClient
      TabOrder = 1
      object cxGrid2: TcxGrid
        Left = 1
        Top = 1
        Width = 832
        Height = 396
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.OnButtonClick = cxGridDBTableView1NavigatorButtonsButtonClick
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = False
          Navigator.Buttons.Next.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Visible = False
          Navigator.Buttons.Delete.Visible = True
          Navigator.Buttons.Post.Visible = True
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = True
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.Visible = True
          DataController.DataSource = DataSource2
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NewItemRow.Visible = True
          OptionsView.GroupByBox = False
          object cxGridDBTableView1KodeKontrak: TcxGridDBColumn
            DataBinding.FieldName = 'KodeKontrak'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxGridDBTableView1KodeKontrakPropertiesButtonClick
            Width = 70
          end
          object cxGridDBTableView1TglMulai: TcxGridDBColumn
            DataBinding.FieldName = 'TglMulai'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Width = 105
          end
          object cxGridDBTableView1TglSelesai: TcxGridDBColumn
            DataBinding.FieldName = 'TglSelesai'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Width = 94
          end
          object cxGridDBTableView1JumlahUnit: TcxGridDBColumn
            DataBinding.FieldName = 'JumlahUnit'
          end
          object cxGridDBTableView1Harga: TcxGridDBColumn
            DataBinding.FieldName = 'Harga'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Width = 123
          end
          object cxGridDBTableView1Status: TcxGridDBColumn
            DataBinding.FieldName = 'Status'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Width = 136
          end
          object cxGridDBTableView1Nominal: TcxGridDBColumn
            DataBinding.FieldName = 'Nominal'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = 'Rp,0;(Rp,0)'
            Properties.OnValidate = cxGridDBTableView1NominalPropertiesValidate
            Width = 125
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 448
    Width = 1197
    Height = 150
    Align = alBottom
    TabOrder = 4
    object Label3: TLabel
      Left = 507
      Top = 103
      Width = 49
      Height = 13
      Caption = 'Cari Nama'
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 112
      Width = 75
      Height = 33
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 253
      Top = 112
      Width = 75
      Height = 33
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object ExitBtn: TcxButton
      Left = 336
      Top = 112
      Width = 75
      Height = 33
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 1195
      Height = 97
      Align = alTop
      Caption = 'Panel6'
      TabOrder = 3
      object cxGrid3: TcxGrid
        Left = 1
        Top = 1
        Width = 1193
        Height = 95
        Align = alClient
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          OnCellDblClick = cxGrid3DBTableView1CellDblClick
          OnCustomDrawCell = cxGrid3DBTableView1CustomDrawCell
          DataController.DataSource = DataSource3
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1Kode: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
            Width = 66
          end
          object cxGrid3DBTableView1Tanggal: TcxGridDBColumn
            DataBinding.FieldName = 'Tanggal'
            Width = 100
          end
          object cxGrid3DBTableView1TglBerangkat: TcxGridDBColumn
            DataBinding.FieldName = 'Berangkat'
            Width = 105
          end
          object cxGrid3DBTableView1Nominal: TcxGridDBColumn
            DataBinding.FieldName = 'Nominal'
            Width = 100
          end
          object cxGrid3DBTableView1NamaPelanggan: TcxGridDBColumn
            Caption = 'NamaPT'
            DataBinding.FieldName = 'namaPT'
            Width = 198
          end
          object cxGrid3DBTableView1NamaPenerimaPembayaran: TcxGridDBColumn
            DataBinding.FieldName = 'NamaPenerimaPembayaran'
            Width = 140
          end
          object cxGrid3DBTableView1CaraPembayaran: TcxGridDBColumn
            DataBinding.FieldName = 'CaraPembayaran'
            Width = 107
          end
          object cxGrid3DBTableView1KeteranganPembayaran: TcxGridDBColumn
            DataBinding.FieldName = 'KeteranganPembayaran'
            Width = 200
          end
          object cxGrid3DBTableView1CekPrint: TcxGridDBColumn
            DataBinding.FieldName = 'CekPrint'
            Visible = False
          end
          object cxGrid3DBTableView1StatusKwitansi: TcxGridDBColumn
            DataBinding.FieldName = 'StatusKwitansi'
            Width = 103
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
    object CetakBtn: TcxButton
      Left = 418
      Top = 112
      Width = 75
      Height = 33
      Caption = 'CETAK'
      TabOrder = 4
      OnClick = CetakBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 899
      Top = 98
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 51
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
    object cxTextEdit1: TcxTextEdit
      Left = 506
      Top = 126
      TabOrder = 6
      OnKeyUp = cxTextEdit1KeyUp
      Width = 137
    end
    object ApproveBtn: TcxButton
      Left = 170
      Top = 112
      Width = 75
      Height = 33
      Caption = 'APPROVE'
      TabOrder = 7
      OnClick = ApproveBtnClick
    end
    object cxButton1: TcxButton
      Left = 584
      Top = 152
      Width = 75
      Height = 25
      Caption = 'cxButton1'
      TabOrder = 8
      Visible = False
      OnClick = cxButton1Click
    end
    object cxGroupBox2: TcxGroupBox
      Left = 648
      Top = 98
      Caption = 'Nomor Kwitansi'
      TabOrder = 9
      Height = 63
      Width = 241
      object cxSpinEdit1: TcxSpinEdit
        Left = 8
        Top = 20
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 0
        Width = 65
      end
      object cxLabel1: TcxLabel
        Left = 78
        Top = 24
        Caption = 's/d'
      end
      object cxSpinEdit2: TcxSpinEdit
        Left = 104
        Top = 20
        Properties.OnChange = cxSpinEdit2PropertiesChange
        TabOrder = 2
        Width = 65
      end
      object cxButton2: TcxButton
        Left = 184
        Top = 16
        Width = 33
        Height = 25
        Caption = 'Cari'
        TabOrder = 3
        OnClick = cxButton2Click
      end
    end
    object btnLunas: TcxButton
      Left = 88
      Top = 112
      Width = 75
      Height = 33
      Caption = 'LUNAS'
      TabOrder = 10
      OnClick = btnLunasClick
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *,cast('#39#39' as money) as GrandTotal,cast('#39#39' as money) as Si' +
        'saBayar'
      'from kwitansi')
    UpdateObject = MasterUs
    Left = 248
    Top = 16
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object MasterQPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object MasterQKeteranganPembayaran: TMemoField
      FieldName = 'KeteranganPembayaran'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaran'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPembayaran'
      Size = 50
      Lookup = True
    end
    object MasterQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object MasterQMarkUp: TCurrencyField
      FieldName = 'MarkUp'
    end
    object MasterQUangMuka: TCurrencyField
      FieldName = 'UangMuka'
    end
    object MasterQStatusKwitansi: TStringField
      FieldName = 'StatusKwitansi'
      Size = 50
    end
    object MasterQKeperluan: TStringField
      FieldName = 'Keperluan'
      Size = 10
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
    end
    object MasterQSisaBayar: TCurrencyField
      FieldName = 'SisaBayar'
    end
    object MasterQUntuk: TStringField
      FieldName = 'Untuk'
      Size = 10
    end
    object MasterQMarkUpNominal: TCurrencyField
      FieldName = 'MarkUpNominal'
    end
    object MasterQCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
    end
    object MasterQTitipKuitansi: TBooleanField
      FieldName = 'TitipKuitansi'
    end
    object MasterQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object MasterQPeriode: TStringField
      FieldName = 'Periode'
      Size = 200
    end
    object MasterQCekPrint: TBooleanField
      FieldName = 'CekPrint'
    end
    object MasterQTanggalPembayaran: TDateTimeField
      FieldName = 'TanggalPembayaran'
    end
    object MasterQPPh: TBooleanField
      FieldName = 'PPh'
    end
    object MasterQKotaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KotaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kota'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object MasterQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 50
    end
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, Nominal, PPN, MarkUp, UangMuka, Pelanggan,' +
        ' PenerimaPembayaran, CaraPembayaran, KeteranganPembayaran, Keper' +
        'luan, StatusKwitansi, CreateDate, TglEntry, CreateBy, Operator, ' +
        'Untuk, MarkUpNominal, TitipKuitansi, TitipTagihan, Periode, CekP' +
        'rint, TanggalPembayaran, PPh, Diskon, JenisKendaraan'
      'from kwitansi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update kwitansi'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  Nominal = :Nominal,'
      '  PPN = :PPN,'
      '  MarkUp = :MarkUp,'
      '  UangMuka = :UangMuka,'
      '  Pelanggan = :Pelanggan,'
      '  PenerimaPembayaran = :PenerimaPembayaran,'
      '  CaraPembayaran = :CaraPembayaran,'
      '  KeteranganPembayaran = :KeteranganPembayaran,'
      '  Keperluan = :Keperluan,'
      '  StatusKwitansi = :StatusKwitansi,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  Untuk = :Untuk,'
      '  MarkUpNominal = :MarkUpNominal,'
      '  TitipKuitansi = :TitipKuitansi,'
      '  TitipTagihan = :TitipTagihan,'
      '  Periode = :Periode,'
      '  CekPrint = :CekPrint,'
      '  TanggalPembayaran = :TanggalPembayaran,'
      '  PPh = :PPh,'
      '  Diskon = :Diskon,'
      '  JenisKendaraan = :JenisKendaraan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into kwitansi'
      
        '  (Kode, Tanggal, Nominal, PPN, MarkUp, UangMuka, Pelanggan, Pen' +
        'erimaPembayaran, CaraPembayaran, KeteranganPembayaran, Keperluan' +
        ', StatusKwitansi, CreateDate, TglEntry, CreateBy, Operator, Untu' +
        'k, MarkUpNominal, TitipKuitansi, TitipTagihan, Periode, CekPrint' +
        ', TanggalPembayaran, PPh, Diskon, JenisKendaraan)'
      'values'
      
        '  (:Kode, :Tanggal, :Nominal, :PPN, :MarkUp, :UangMuka, :Pelangg' +
        'an, :PenerimaPembayaran, :CaraPembayaran, :KeteranganPembayaran,' +
        ' :Keperluan, :StatusKwitansi, :CreateDate, :TglEntry, :CreateBy,' +
        ' :Operator, :Untuk, :MarkUpNominal, :TitipKuitansi, :TitipTagiha' +
        'n, :Periode, :CekPrint, :TanggalPembayaran, :PPh, :Diskon, :Jeni' +
        'sKendaraan)')
    DeleteSQL.Strings = (
      'delete from kwitansi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 312
    Top = 16
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 280
    Top = 16
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from kwitansi order by kode desc')
    Left = 217
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pelanggan')
    Left = 376
    Top = 40
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 100
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai')
    Left = 416
    Top = 40
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from MasterSO')
    Left = 480
    Top = 48
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailSOQ
    Left = 728
    Top = 48
  end
  object DetailSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select dso.*,so.Berangkat,cast('#39#39' as money) as JumlahBayar, cast' +
        '('#39#39' as money) as SisaBayar from DetailKwitansiSO dso'
      'left join MasterSO so on dso.KodeSO=so.kodenota '
      'where kodeKwitansi=:text'
      '')
    UpdateObject = SDUpdateSQL1
    Left = 664
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailSOQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object DetailSOQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
    object DetailSOQJumlahBayar: TCurrencyField
      FieldName = 'JumlahBayar'
    end
    object DetailSOQTanggalSO: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalSO'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Tgl'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQHarga: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'Harga'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Harga'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQPPN: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'PPN'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'PPN'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQBiayaExtend: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'BiayaExtend'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'BiayaExtend'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQPPNExtend: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'PPNExtend'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'PPNExtend'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object DetailSOQTglBerangkat: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TglBerangkat'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Berangkat'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Rute'
      KeyFields = 'KodeSO'
      Size = 10
      Lookup = True
    end
    object DetailSOQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object DetailSOQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object DetailSOQKomisiPelanggan: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'KomisiPelanggan'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'KomisiPelanggan'
      KeyFields = 'KodeSO'
      Lookup = True
    end
    object DetailSOQSisaBayar: TCurrencyField
      FieldName = 'SisaBayar'
    end
    object DetailSOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object DetailSOQKeterangan: TStringField
      FieldKind = fkLookup
      FieldName = 'Keterangan'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Keterangan'
      KeyFields = 'KodeSO'
      Size = 1000
      Lookup = True
    end
  end
  object DetailKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    BeforePost = DetailKontrakQBeforePost
    SQL.Strings = (
      'select *  from DetailKwitansiKontrak where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL2
    Left = 768
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailKontrakQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object DetailKontrakQKodeKontrak: TStringField
      FieldName = 'KodeKontrak'
      Required = True
      Size = 10
    end
    object DetailKontrakQTglMulai: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TglMulai'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TglMulai'
      KeyFields = 'KodeKontrak'
      Lookup = True
    end
    object DetailKontrakQTglSelesai: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TglSelesai'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TglSelesai'
      KeyFields = 'KodeKontrak'
      Lookup = True
    end
    object DetailKontrakQHarga: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'Harga'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Harga'
      KeyFields = 'KodeKontrak'
      Lookup = True
    end
    object DetailKontrakQStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'Status'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Status'
      KeyFields = 'KodeKontrak'
      Size = 50
      Lookup = True
    end
    object DetailKontrakQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object DetailKontrakQJumlahUnit: TIntegerField
      FieldKind = fkLookup
      FieldName = 'JumlahUnit'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JumlahUnit'
      KeyFields = 'KodeKontrak'
      Lookup = True
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO, Nominal'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeSO, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 696
    Top = 40
  end
  object DataSource2: TDataSource
    DataSet = DetailKontrakQ
    Left = 800
    Top = 16
  end
  object SDUpdateSQL2: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeKontrak, Nominal'
      'from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    ModifySQL.Strings = (
      'update DetailKwitansiKontrak'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeKontrak = :KodeKontrak,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    InsertSQL.Strings = (
      'insert into DetailKwitansiKontrak'
      '  (KodeKwitansi, KodeKontrak, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeKontrak, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    Left = 824
    Top = 16
  end
  object HitungSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Left = 664
    Top = 112
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Kontrak')
    Left = 272
    Top = 56
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object KontrakQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object KontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 200
    end
    object KontrakQJumlahUnit: TIntegerField
      FieldName = 'JumlahUnit'
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\toshiba\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'Tahoma'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'Tahoma'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'Tahoma'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'Tahoma'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'Tahoma'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'Tahoma'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'Tahoma'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'Tahoma'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'Tahoma'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'Tahoma'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'Tahoma'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'Tahoma'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 344
    Top = 56
  end
  object viewKwitansiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select cast(k.Tanggal as date) as Tgl, k.*,p.namaPT,'
      '          (select top 1 so.Berangkat '
      '           from Kwitansi k2'
      '           left join Pelanggan p on k2.Pelanggan=p.kode'
      
        '           left join DetailKwitansiSO dkso on dkso.KodeKwitansi=' +
        'k2.kode'
      '           left join MasterSO so on dkso.KodeSO=so.Kodenota '
      '           where k2.Kode=k.Kode) as Berangkat '
      'from Kwitansi k '
      'left join Pelanggan p on k.Pelanggan=p.kode'
      'where  k.Kode>=:text3 and k.Kode<=:text4 and '
      '(k.Tanggal>=:text1 and k.Tanggal<=:text2) or k.Tanggal is null'
      'order by tglentry desc')
    Left = 696
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text4'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewKwitansiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewKwitansiQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object viewKwitansiQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object viewKwitansiQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object viewKwitansiQPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object viewKwitansiQKeteranganPembayaran: TMemoField
      FieldName = 'KeteranganPembayaran'
      BlobType = ftMemo
    end
    object viewKwitansiQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewKwitansiQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewKwitansiQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewKwitansiQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewKwitansiQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object viewKwitansiQNamaPenerimaPembayaran: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerimaPembayaran'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaran'
      Size = 50
      Lookup = True
    end
    object viewKwitansiQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object viewKwitansiQMarkUp: TCurrencyField
      FieldName = 'MarkUp'
    end
    object viewKwitansiQUangMuka: TCurrencyField
      FieldName = 'UangMuka'
    end
    object viewKwitansiQKeperluan: TStringField
      FieldName = 'Keperluan'
      Size = 10
    end
    object viewKwitansiQStatusKwitansi: TStringField
      FieldName = 'StatusKwitansi'
      Size = 50
    end
    object viewKwitansiQnamaPT: TStringField
      FieldName = 'namaPT'
      Size = 100
    end
    object viewKwitansiQUntuk: TStringField
      FieldName = 'Untuk'
      Size = 10
    end
    object viewKwitansiQMarkUpNominal: TCurrencyField
      FieldName = 'MarkUpNominal'
    end
    object viewKwitansiQCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
    end
    object viewKwitansiQNoSO: TStringField
      FieldKind = fkLookup
      FieldName = 'NoSO'
      LookupDataSet = DetailKwitansiSOQ
      LookupKeyFields = 'KodeKwitansi'
      LookupResultField = 'KodeSO'
      KeyFields = 'Kode'
      Size = 10
      Lookup = True
    end
    object viewKwitansiQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object viewKwitansiQTgl: TStringField
      FieldName = 'Tgl'
    end
    object viewKwitansiQTitipKuitansi: TBooleanField
      FieldName = 'TitipKuitansi'
    end
    object viewKwitansiQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object viewKwitansiQPeriode: TStringField
      FieldName = 'Periode'
      Size = 200
    end
    object viewKwitansiQCekPrint: TBooleanField
      FieldName = 'CekPrint'
    end
  end
  object DataSource3: TDataSource
    DataSet = viewKwitansiQ
    Left = 752
    Top = 552
  end
  object CekDetailQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(*) as Cek from DetailKwitansiSO where KodeKwitansi=' +
        ':text')
    Left = 648
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekDetailQCek: TIntegerField
      FieldName = 'Cek'
    end
  end
  object ArmadaSOUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO, Nominal'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeSO, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 456
    Top = 320
  end
  object SOIsiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *,convert(varchar(11),so.Berangkat,106) as TglBerangkat,c' +
        'onvert(varchar(11),so.Tiba,106) as TglTiba,'
      
        'convert(varchar(5),so.Tiba,108) as JamSelesai,convert(varchar(5)' +
        ',so.JamJemput,108) as JamMulai from MasterSO so where kodenota=:' +
        'text')
    UpdateObject = SOIsiUs
    Left = 416
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SOIsiQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOIsiQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOIsiQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOIsiQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOIsiQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOIsiQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOIsiQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOIsiQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOIsiQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOIsiQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOIsiQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SOIsiQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOIsiQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOIsiQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOIsiQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOIsiQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOIsiQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOIsiQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SOIsiQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOIsiQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOIsiQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOIsiQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOIsiQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOIsiQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOIsiQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOIsiQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOIsiQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOIsiQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOIsiQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOIsiQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOIsiQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOIsiQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOIsiQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOIsiQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOIsiQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOIsiQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOIsiQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOIsiQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOIsiQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOIsiQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOIsiQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOIsiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOIsiQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOIsiQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOIsiQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOIsiQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOIsiQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOIsiQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object SOIsiQTglBerangkat: TStringField
      FieldName = 'TglBerangkat'
      Size = 11
    end
    object SOIsiQTglTiba: TStringField
      FieldName = 'TglTiba'
      Size = 11
    end
    object SOIsiQJamSelesai: TStringField
      FieldName = 'JamSelesai'
      Size = 5
    end
    object SOIsiQJamMulai: TStringField
      FieldName = 'JamMulai'
      Size = 5
    end
    object SOIsiQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object SOIsiQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object SOIsiQTitlePICJemput: TStringField
      FieldName = 'TitlePICJemput'
      Size = 50
    end
    object SOIsiQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object SOIsiQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Rute')
    Left = 520
    Top = 40
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object ArmadaSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select distinct(so.KapasitasSeat) from '
      'DetailKwitansiSO dso '
      'left join MasterSO so on dso.KodeSO=so.Kodenota'
      'left join Armada a on so.Armada=a.Kode'
      'where dso.KodeKwitansi=:text')
    UpdateObject = ArmadaSOUs
    Left = 480
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaSOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
  end
  object JumlahArmadaSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(so.KapasitasSeat) as JumSeat from DetailKwitansiSO ' +
        'dso left join MasterSO so on dso.KodeSO=so.Kodenota'
      'left join Armada a on so.Armada=a.Kode'
      'where dso.KodeKwitansi=:text and so.KapasitasSeat=:text2'
      'and so.Harga=:text3')
    Left = 488
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end>
    object JumlahArmadaSOQJumSeat: TIntegerField
      FieldName = 'JumSeat'
    end
  end
  object SOIsiUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganC' +
        'araPembayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPemb' +
        'ayaranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, Car' +
        'aPembayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelu' +
        'nasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglK' +
        'embaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet,' +
        ' AirSuspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemp' +
        'ut, PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status,' +
        ' StatusPembayaran, ReminderPending, PenerimaPending, Keterangan,' +
        ' CreateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelan' +
        'ggan, KeteranganRute, KeteranganHarga'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  KeteranganCaraPembayaranAwal = :KeteranganCaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  KetCaraPembayaranPelunasan = :KetCaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  TitlePICJemput = :TitlePICJemput,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KomisiPelanggan = :KomisiPelanggan,'
      '  KeteranganRute = :KeteranganRute,'
      '  KeteranganHarga = :KeteranganHarga'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganCaraP' +
        'embayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPembayar' +
        'anAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPem' +
        'bayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelunasa' +
        'n, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglKemba' +
        'liExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet, Air' +
        'Suspension, Rute, TglFollowUp, Armada, Kontrak, TitlePICJemput, ' +
        'PICJemput, JamJemput, NoTelpPICJemput, AlamatJemput, Status, Sta' +
        'tusPembayaran, ReminderPending, PenerimaPending, Keterangan, Cre' +
        'ateDate, CreateBy, Operator, TglEntry, TglCetak, KomisiPelanggan' +
        ', KeteranganRute, KeteranganHarga)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :Kete' +
        'ranganCaraPembayaranAwal, :NoKwitansiPembayaranAwal, :NominalKwi' +
        'tansiPembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPe' +
        'lunasan, :CaraPembayaranPelunasan, :KetCaraPembayaranPelunasan, ' +
        ':NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPeluna' +
        'san, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :Kapa' +
        'sitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :A' +
        'rmada, :Kontrak, :TitlePICJemput, :PICJemput, :JamJemput, :NoTel' +
        'pPICJemput, :AlamatJemput, :Status, :StatusPembayaran, :Reminder' +
        'Pending, :PenerimaPending, :Keterangan, :CreateDate, :CreateBy, ' +
        ':Operator, :TglEntry, :TglCetak, :KomisiPelanggan, :KeteranganRu' +
        'te, :KeteranganHarga)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 448
    Top = 280
  end
  object KontrakIsiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *,convert(varchar(11),TglMulai,106) as TglMulaiP,convert(' +
        'varchar(11),TglSelesai,106) as TglSelesaiP'
      'from Kontrak where kode=:text')
    UpdateObject = KontrakIsiUs
    Left = 520
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KontrakIsiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakIsiQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakIsiQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakIsiQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakIsiQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakIsiQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakIsiQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakIsiQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakIsiQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakIsiQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakIsiQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object KontrakIsiQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
    object KontrakIsiQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakIsiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakIsiQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakIsiQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KontrakIsiQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KontrakIsiQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KontrakIsiQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KontrakIsiQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object KontrakIsiQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object KontrakIsiQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object KontrakIsiQTglMulaiP: TStringField
      FieldName = 'TglMulaiP'
      Size = 11
    end
    object KontrakIsiQTglSelesaiP: TStringField
      FieldName = 'TglSelesaiP'
      Size = 11
    end
  end
  object KontrakIsiUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, ' +
        'AC, Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, St' +
        'atus, Keterangan, IntervalPenagihan, CreateDate, CreateBy, Opera' +
        'tor, TglEntry, TglCetak'
      'from Kontrak'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Kontrak'
      'set'
      '  Kode = :Kode,'
      '  Pelanggan = :Pelanggan,'
      '  TglMulai = :TglMulai,'
      '  TglSelesai = :TglSelesai,'
      '  StatusRute = :StatusRute,'
      '  Rute = :Rute,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  Harga = :Harga,'
      '  POEksternal = :POEksternal,'
      '  Status = :Status,'
      '  Keterangan = :Keterangan,'
      '  IntervalPenagihan = :IntervalPenagihan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Kontrak'
      
        '  (Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, AC, ' +
        'Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, Status' +
        ', Keterangan, IntervalPenagihan, CreateDate, CreateBy, Operator,' +
        ' TglEntry, TglCetak)'
      'values'
      
        '  (:Kode, :Pelanggan, :TglMulai, :TglSelesai, :StatusRute, :Rute' +
        ', :AC, :Toilet, :AirSuspension, :KapasitasSeat, :Harga, :POEkste' +
        'rnal, :Status, :Keterangan, :IntervalPenagihan, :CreateDate, :Cr' +
        'eateBy, :Operator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from Kontrak'
      'where'
      '  Kode = :OLD_Kode')
    Left = 552
    Top = 256
  end
  object JumlahArmadaKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(k.KapasitasSeat) as JumSeat from DetailKwitansiKont' +
        'rak dkk left join Kontrak k on dkk.KodeKontrak=k.Kode'
      'where dkk.KodeKwitansi=:text and k.KapasitasSeat=:text2')
    Left = 560
    Top = 336
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
        Value = ' '
      end>
    object JumlahArmadaKontrakQJumSeat: TIntegerField
      FieldName = 'JumSeat'
    end
  end
  object ArmadaKontrakUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeKontrak, Nominal'
      'from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    ModifySQL.Strings = (
      'update DetailKwitansiKontrak'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeKontrak = :KodeKontrak,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    InsertSQL.Strings = (
      'insert into DetailKwitansiKontrak'
      '  (KodeKwitansi, KodeKontrak, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeKontrak, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    Left = 568
    Top = 296
  end
  object ArmadaKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select distinct(k.KapasitasSeat) from DetailKwitansiKontrak dkk ' +
        'left join Kontrak k on dkk.KodeKontrak=k.Kode'
      'where dkk.KodeKwitansi=:text')
    UpdateObject = ArmadaKontrakUs
    Left = 536
    Top = 296
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaKontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
  end
  object PPNMarkupSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select sum(case when PPN is NULL then 0 else PPN end) as PPN,'
      'sum(case when MarkUp is NULL then 0 else MarkUp end) as MarkUp '
      'from Kwitansi where Pelanggan=:text'
      'and StatusKwitansi=upper('#39'BELUM LUNAS'#39')')
    Left = 320
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PPNMarkupSOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object PPNMarkupSOQMarkUp: TCurrencyField
      FieldName = 'MarkUp'
    end
  end
  object TotalNominalSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select sum(case when Nominal is NULL then 0 else Nominal end) as' +
        ' Nominal'
      'from Kwitansi where Pelanggan=:text'
      'and StatusKwitansi=upper('#39'BELUM LUNAS'#39')')
    Left = 352
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object TotalNominalSOQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
  end
  object HapusSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiSO where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL3
    Left = 432
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HapusSOQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object HapusSOQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL3: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO)'
      'values'
      '  (:KodeKwitansi, :KodeSO)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi')
    Left = 464
    Top = 184
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiSO where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL4
    Left = 496
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SDQuery1KodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object SDQuery1KodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL4: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO)'
      'values'
      '  (:KodeKwitansi, :KodeSO)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 528
    Top = 184
  end
  object HapusKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiKontrak where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL5
    Left = 432
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HapusKontrakQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object HapusKontrakQKodeKontrak: TStringField
      FieldName = 'KodeKontrak'
      Required = True
      Size = 10
    end
  end
  object SDQuery3: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiKontrak where KodeKwitansi=:text')
    UpdateObject = SDUpdateSQL6
    Left = 504
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SDQuery3KodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object SDQuery3KodeKontrak: TStringField
      FieldName = 'KodeKontrak'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL5: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeKontrak'
      'from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    ModifySQL.Strings = (
      'update DetailKwitansiKontrak'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeKontrak = :KodeKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    InsertSQL.Strings = (
      'insert into DetailKwitansiKontrak'
      '  (KodeKwitansi, KodeKontrak)'
      'values'
      '  (:KodeKwitansi, :KodeKontrak)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    Left = 464
    Top = 216
  end
  object SDUpdateSQL6: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeKontrak'
      'from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    ModifySQL.Strings = (
      'update DetailKwitansiKontrak'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeKontrak = :KodeKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    InsertSQL.Strings = (
      'insert into DetailKwitansiKontrak'
      '  (KodeKwitansi, KodeKontrak)'
      'values'
      '  (:KodeKwitansi, :KodeKontrak)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiKontrak'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeKontrak = :OLD_KodeKontrak')
    Left = 536
    Top = 216
  end
  object HitungTotalSOKlikQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Kwitansi k left join DetailKwitansiSO dso'
      'on dso.KodeKwitansi=k.Kode')
    Left = 432
    Top = 368
    object HitungTotalSOKlikQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HitungTotalSOKlikQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object HitungTotalSOKlikQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
    object HitungTotalSOKlikQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object HitungTotalSOKlikQMarkUp: TCurrencyField
      FieldName = 'MarkUp'
    end
    object HitungTotalSOKlikQUangMuka: TCurrencyField
      FieldName = 'UangMuka'
    end
    object HitungTotalSOKlikQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object HitungTotalSOKlikQPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object HitungTotalSOKlikQCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
      Size = 10
    end
    object HitungTotalSOKlikQKeteranganPembayaran: TMemoField
      FieldName = 'KeteranganPembayaran'
      BlobType = ftMemo
    end
    object HitungTotalSOKlikQKeperluan: TStringField
      FieldName = 'Keperluan'
      Size = 10
    end
    object HitungTotalSOKlikQStatusKwitansi: TStringField
      FieldName = 'StatusKwitansi'
      Size = 50
    end
    object HitungTotalSOKlikQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object HitungTotalSOKlikQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object HitungTotalSOKlikQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object HitungTotalSOKlikQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object HitungTotalSOKlikQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Size = 10
    end
    object HitungTotalSOKlikQKodeSO: TStringField
      FieldName = 'KodeSO'
      Size = 10
    end
  end
  object HitungNominalLunasQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select SUM(Nominal) as Nominal from Kwitansi k left join DetailK' +
        'witansiSO dso'
      'on dso.KodeKwitansi=k.Kode where dso.KodeSO=:text')
    Left = 288
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HitungNominalLunasQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
  end
  object IsiLunasSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select *,convert(varchar(11),so.Berangkat,106) as TglBerangkat,c' +
        'onvert(varchar(11),so.Tiba,106) as TglTiba,'
      
        'convert(varchar(5),so.Tiba,108) as JamSelesai,convert(varchar(5)' +
        ',so.JamJemput,108) as JamMulai '
      'from MasterSO so where kodenota=:text')
    Left = 576
    Top = 136
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object IsiLunasSOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object IsiLunasSOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object IsiLunasSOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object IsiLunasSOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object IsiLunasSOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object IsiLunasSOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object IsiLunasSOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object IsiLunasSOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object IsiLunasSOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object IsiLunasSOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object IsiLunasSOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object IsiLunasSOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object IsiLunasSOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object IsiLunasSOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object IsiLunasSOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object IsiLunasSOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object IsiLunasSOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object IsiLunasSOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object IsiLunasSOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object IsiLunasSOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object IsiLunasSOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object IsiLunasSOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object IsiLunasSOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object IsiLunasSOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object IsiLunasSOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object IsiLunasSOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object IsiLunasSOQAC: TBooleanField
      FieldName = 'AC'
    end
    object IsiLunasSOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object IsiLunasSOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object IsiLunasSOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object IsiLunasSOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object IsiLunasSOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object IsiLunasSOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object IsiLunasSOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object IsiLunasSOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object IsiLunasSOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object IsiLunasSOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object IsiLunasSOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object IsiLunasSOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object IsiLunasSOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object IsiLunasSOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object IsiLunasSOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object IsiLunasSOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object IsiLunasSOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object IsiLunasSOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object IsiLunasSOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object IsiLunasSOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object IsiLunasSOQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object IsiLunasSOQTglBerangkat: TStringField
      FieldName = 'TglBerangkat'
      Size = 11
    end
    object IsiLunasSOQTglTiba: TStringField
      FieldName = 'TglTiba'
      Size = 11
    end
    object IsiLunasSOQJamSelesai: TStringField
      FieldName = 'JamSelesai'
      Size = 5
    end
    object IsiLunasSOQJamMulai: TStringField
      FieldName = 'JamMulai'
      Size = 5
    end
  end
  object ArmadaSOLunasQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select distinct(a.JumlahSeat) from MasterSO so left join DetailK' +
        'witansiSO dso '
      
        'on dso.KodeSO=so.Kodenota left join Kwitansi k on dso.KodeKwitan' +
        'si=k.Kode'
      'left join Armada a on so.Armada=a.kode'
      'where k.Kode=:text')
    UpdateObject = ArmadaSOLunasUs
    Left = 664
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaSOLunasQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
  end
  object ArmadaSOLunasUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganC' +
        'araPembayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPemb' +
        'ayaranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, Car' +
        'aPembayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelu' +
        'nasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglK' +
        'embaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet,' +
        ' AirSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, J' +
        'amJemput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayara' +
        'n, ReminderPending, PenerimaPending, Keterangan, CreateDate, Cre' +
        'ateBy, Operator, TglEntry, TglCetak'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  KeteranganCaraPembayaranAwal = :KeteranganCaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  KetCaraPembayaranPelunasan = :KetCaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganCaraP' +
        'embayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPembayar' +
        'anAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPem' +
        'bayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelunasa' +
        'n, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglKemba' +
        'liExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet, Air' +
        'Suspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJe' +
        'mput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, R' +
        'eminderPending, PenerimaPending, Keterangan, CreateDate, CreateB' +
        'y, Operator, TglEntry, TglCetak)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :Kete' +
        'ranganCaraPembayaranAwal, :NoKwitansiPembayaranAwal, :NominalKwi' +
        'tansiPembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPe' +
        'lunasan, :CaraPembayaranPelunasan, :KetCaraPembayaranPelunasan, ' +
        ':NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPeluna' +
        'san, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :Kapa' +
        'sitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :A' +
        'rmada, :Kontrak, :PICJemput, :JamJemput, :NoTelpPICJemput, :Alam' +
        'atJemput, :Status, :StatusPembayaran, :ReminderPending, :Penerim' +
        'aPending, :Keterangan, :CreateDate, :CreateBy, :Operator, :TglEn' +
        'try, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 696
    Top = 224
  end
  object HitungArmadaSOLunasQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(*) as JumSeat from MasterSO so left join DetailKwit' +
        'ansiSO dso '
      
        'on dso.KodeSO=so.Kodenota left join Kwitansi k on dso.KodeKwitan' +
        'si=k.Kode'
      'left join Armada a on so.Armada=a.kode'
      'where k.Kode=:text and a.JumlahSeat=:text2')
    Left = 712
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object HitungArmadaSOLunasQJumSeat: TIntegerField
      FieldName = 'JumSeat'
    end
  end
  object HitungNominalSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select sum(Nominal) as TotalBayar from DetailKwitansiSO where Ko' +
        'deSO=:text')
    Left = 608
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object HitungNominalSOQTotalBayar: TCurrencyField
      FieldName = 'TotalBayar'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 664
    Top = 312
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object CekAirSusQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(so.AirSuspension) as JumAS from Kwitansi k left joi' +
        'n DetailKwitansiSO dso on dso.KodeKwitansi=k.Kode'
      
        'left join MasterSO so on dso.KodeSO=so.Kodenota where k.Kode=:te' +
        'xt'
      'and so.KapasitasSeat=:text2 and so.AirSuspension=1'
      'and so.Harga=:text3')
    UpdateObject = CekAirSusUs
    Left = 888
    Top = 152
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end>
    object CekAirSusQJumAS: TIntegerField
      FieldName = 'JumAS'
    end
  end
  object CekToiletQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select count(so.Toilet) as JumT from Kwitansi k left join Detail' +
        'KwitansiSO dso on dso.KodeKwitansi=k.Kode'
      
        'left join MasterSO so on dso.KodeSO=so.Kodenota where k.Kode=:te' +
        'xt'
      'and so.KapasitasSeat=:text2 and so.Toilet=1'
      'and so.Harga=:text3')
    UpdateObject = CekToiletUs
    Left = 776
    Top = 152
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end>
    object CekToiletQJumT: TIntegerField
      FieldName = 'JumT'
    end
  end
  object CekToiletUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, Nominal, PPN, MarkUp, UangMuka, Pelanggan,' +
        ' PenerimaPembayaran, CaraPembayaran, KeteranganPembayaran, Keper' +
        'luan, StatusKwitansi, CreateDate, TglEntry, CreateBy, Operator, ' +
        'Untuk, MarkUpNominal, TitipKuitansi, TitipTagihan, Periode, CekP' +
        'rint, TanggalPembayaran, PPh'
      'from Kwitansi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Kwitansi'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  Nominal = :Nominal,'
      '  PPN = :PPN,'
      '  MarkUp = :MarkUp,'
      '  UangMuka = :UangMuka,'
      '  Pelanggan = :Pelanggan,'
      '  PenerimaPembayaran = :PenerimaPembayaran,'
      '  CaraPembayaran = :CaraPembayaran,'
      '  KeteranganPembayaran = :KeteranganPembayaran,'
      '  Keperluan = :Keperluan,'
      '  StatusKwitansi = :StatusKwitansi,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  Untuk = :Untuk,'
      '  MarkUpNominal = :MarkUpNominal,'
      '  TitipKuitansi = :TitipKuitansi,'
      '  TitipTagihan = :TitipTagihan,'
      '  Periode = :Periode,'
      '  CekPrint = :CekPrint,'
      '  TanggalPembayaran = :TanggalPembayaran,'
      '  PPh = :PPh'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Kwitansi'
      
        '  (Kode, Tanggal, Nominal, PPN, MarkUp, UangMuka, Pelanggan, Pen' +
        'erimaPembayaran, CaraPembayaran, KeteranganPembayaran, Keperluan' +
        ', StatusKwitansi, CreateDate, TglEntry, CreateBy, Operator, Untu' +
        'k, MarkUpNominal, TitipKuitansi, TitipTagihan, Periode, CekPrint' +
        ', TanggalPembayaran, PPh)'
      'values'
      
        '  (:Kode, :Tanggal, :Nominal, :PPN, :MarkUp, :UangMuka, :Pelangg' +
        'an, :PenerimaPembayaran, :CaraPembayaran, :KeteranganPembayaran,' +
        ' :Keperluan, :StatusKwitansi, :CreateDate, :TglEntry, :CreateBy,' +
        ' :Operator, :Untuk, :MarkUpNominal, :TitipKuitansi, :TitipTagiha' +
        'n, :Periode, :CekPrint, :TanggalPembayaran, :PPh)')
    DeleteSQL.Strings = (
      'delete from Kwitansi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 784
    Top = 192
  end
  object CekAirSusUs: TSDUpdateSQL
    Left = 888
    Top = 192
  end
  object CekHargaArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select so.harga from DetailKwitansiSO dso '
      'left join MasterSO so on dso.KodeSO=so.Kodenota'
      'left join Armada a on so.Armada=a.Kode'
      'where dso.KodeKwitansi=:text and so.KapasitasSeat=:text2')
    Left = 376
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object CekHargaArmadaQharga: TCurrencyField
      FieldName = 'harga'
    end
  end
  object POEksternalQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select POEksternal from Kontrak where kode=:text')
    UpdateObject = POEksternalUs
    Left = 864
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object POEksternalQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
  end
  object POEksternalUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, ' +
        'AC, Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, St' +
        'atus, Keterangan, IntervalPenagihan, CreateDate, CreateBy, Opera' +
        'tor, TglEntry, TglCetak, PlatNo'
      'from Kontrak'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Kontrak'
      'set'
      '  Kode = :Kode,'
      '  Pelanggan = :Pelanggan,'
      '  TglMulai = :TglMulai,'
      '  TglSelesai = :TglSelesai,'
      '  StatusRute = :StatusRute,'
      '  Rute = :Rute,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  Harga = :Harga,'
      '  POEksternal = :POEksternal,'
      '  Status = :Status,'
      '  Keterangan = :Keterangan,'
      '  IntervalPenagihan = :IntervalPenagihan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  PlatNo = :PlatNo'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Kontrak'
      
        '  (Kode, Pelanggan, TglMulai, TglSelesai, StatusRute, Rute, AC, ' +
        'Toilet, AirSuspension, KapasitasSeat, Harga, POEksternal, Status' +
        ', Keterangan, IntervalPenagihan, CreateDate, CreateBy, Operator,' +
        ' TglEntry, TglCetak, PlatNo)'
      'values'
      
        '  (:Kode, :Pelanggan, :TglMulai, :TglSelesai, :StatusRute, :Rute' +
        ', :AC, :Toilet, :AirSuspension, :KapasitasSeat, :Harga, :POEkste' +
        'rnal, :Status, :Keterangan, :IntervalPenagihan, :CreateDate, :Cr' +
        'eateBy, :Operator, :TglEntry, :TglCetak, :PlatNo)')
    DeleteSQL.Strings = (
      'delete from Kontrak'
      'where'
      '  Kode = :OLD_Kode')
    Left = 904
    Top = 288
  end
  object DetailKwitansiSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiSO')
    Left = 816
    Top = 560
    object DetailKwitansiSOQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object DetailKwitansiSOQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
    object DetailKwitansiSOQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
  end
  object CetakanKuitansiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from CetakanKuitansi'
      'where kode=:text')
    UpdateObject = CetakanKuitansiUs
    Left = 768
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CetakanKuitansiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CetakanKuitansiQTerbilang: TMemoField
      FieldName = 'Terbilang'
      BlobType = ftMemo
    end
    object CetakanKuitansiQGrandTotal: TMemoField
      FieldName = 'GrandTotal'
      BlobType = ftMemo
    end
    object CetakanKuitansiQTujuan: TMemoField
      FieldName = 'Tujuan'
      BlobType = ftMemo
    end
    object CetakanKuitansiQJamMulai: TMemoField
      FieldName = 'JamMulai'
      BlobType = ftMemo
    end
    object CetakanKuitansiQTempat: TMemoField
      FieldName = 'Tempat'
      BlobType = ftMemo
    end
    object CetakanKuitansiQPemakaian: TMemoField
      FieldName = 'Pemakaian'
      BlobType = ftMemo
    end
    object CetakanKuitansiQTersisa: TMemoField
      FieldName = 'Tersisa'
      BlobType = ftMemo
    end
    object CetakanKuitansiQNominal: TMemoField
      FieldName = 'Nominal'
      BlobType = ftMemo
    end
    object CetakanKuitansiQTglPemakaian: TMemoField
      FieldName = 'TglPemakaian'
      BlobType = ftMemo
    end
    object CetakanKuitansiQNamaTtd: TMemoField
      FieldName = 'NamaTtd'
      BlobType = ftMemo
    end
    object CetakanKuitansiQPIC: TMemoField
      FieldName = 'PIC'
      BlobType = ftMemo
    end
    object CetakanKuitansiQKomisiPelanggan: TMemoField
      FieldName = 'KomisiPelanggan'
      BlobType = ftMemo
    end
    object CetakanKuitansiQKomisiPelangganNominal: TMemoField
      FieldName = 'KomisiPelangganNominal'
      BlobType = ftMemo
    end
    object CetakanKuitansiQDiskon: TMemoField
      FieldName = 'Diskon'
      BlobType = ftMemo
    end
  end
  object CetakanKuitansiUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Terbilang, GrandTotal, Tujuan, JamMulai, Tempat, Pe' +
        'makaian, Tersisa, Nominal, TglPemakaian, NamaTTD, PIC, KomisiPel' +
        'anggan, KomisiPelangganNominal, Diskon'
      'from CetakanKuitansi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update CetakanKuitansi'
      'set'
      '  Kode = :Kode,'
      '  Terbilang = :Terbilang,'
      '  GrandTotal = :GrandTotal,'
      '  Tujuan = :Tujuan,'
      '  JamMulai = :JamMulai,'
      '  Tempat = :Tempat,'
      '  Pemakaian = :Pemakaian,'
      '  Tersisa = :Tersisa,'
      '  Nominal = :Nominal,'
      '  TglPemakaian = :TglPemakaian,'
      '  NamaTTD = :NamaTTD,'
      '  PIC = :PIC,'
      '  KomisiPelanggan = :KomisiPelanggan,'
      '  KomisiPelangganNominal = :KomisiPelangganNominal,'
      '  Diskon = :Diskon'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into CetakanKuitansi'
      
        '  (Kode, Terbilang, GrandTotal, Tujuan, JamMulai, Tempat, Pemaka' +
        'ian, Tersisa, Nominal, TglPemakaian, NamaTTD, PIC, KomisiPelangg' +
        'an, KomisiPelangganNominal, Diskon)'
      'values'
      
        '  (:Kode, :Terbilang, :GrandTotal, :Tujuan, :JamMulai, :Tempat, ' +
        ':Pemakaian, :Tersisa, :Nominal, :TglPemakaian, :NamaTTD, :PIC, :' +
        'KomisiPelanggan, :KomisiPelangganNominal, :Diskon)')
    DeleteSQL.Strings = (
      'delete from CetakanKuitansi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 808
    Top = 360
  end
  object IsiPICQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Pelanggan where Kode=:text')
    Left = 560
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object IsiPICQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object IsiPICQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 100
    end
    object IsiPICQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object IsiPICQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object IsiPICQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object IsiPICQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object IsiPICQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object IsiPICQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object IsiPICQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object IsiPICQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object IsiPICQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object IsiPICQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object IsiPICQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object IsiPICQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object IsiPICQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object IsiPICQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object IsiPICQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object IsiPICQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object IsiPICQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object IsiPICQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object IsiPICQTitle: TStringField
      FieldName = 'Title'
      Size = 50
    end
  end
  object HapusCetakanKuitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 624
    Top = 352
  end
  object SearchNamaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select cast(k.Tanggal as date) as Tgl, k.*,p.namaPT,'
      '          (select top 1 so.Berangkat '
      '           from Kwitansi k2'
      '           left join Pelanggan p on k2.Pelanggan=p.kode'
      
        '           left join DetailKwitansiSO dkso on dkso.KodeKwitansi=' +
        'k2.kode'
      '           left join MasterSO so on dkso.KodeSO=so.Kodenota '
      '           where k2.Kode=k.Kode) as Berangkat '
      'from Kwitansi k '
      'left join Pelanggan p on k.Pelanggan=p.kode')
    Left = 576
    Top = 520
  end
  object DetailSOQ2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select *, cast('#39#39' as money) as JumlahBayar, cast('#39#39' as money) as' +
        ' SisaBayar from MasterSO where kodenota=:text'
      '')
    UpdateObject = SDUpdateSQL7
    Left = 584
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailSOQ2Kodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object DetailSOQ2Tgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object DetailSOQ2Pelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object DetailSOQ2Berangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object DetailSOQ2Tiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object DetailSOQ2Harga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object DetailSOQ2PPN: TCurrencyField
      FieldName = 'PPN'
    end
    object DetailSOQ2PembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object DetailSOQ2TglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object DetailSOQ2CaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object DetailSOQ2KeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object DetailSOQ2NoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object DetailSOQ2NominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object DetailSOQ2PenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object DetailSOQ2Pelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object DetailSOQ2TglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object DetailSOQ2CaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object DetailSOQ2KetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object DetailSOQ2NoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object DetailSOQ2NominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object DetailSOQ2PenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object DetailSOQ2Extend: TBooleanField
      FieldName = 'Extend'
    end
    object DetailSOQ2TglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object DetailSOQ2BiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object DetailSOQ2PPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object DetailSOQ2KapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object DetailSOQ2AC: TBooleanField
      FieldName = 'AC'
    end
    object DetailSOQ2Toilet: TBooleanField
      FieldName = 'Toilet'
    end
    object DetailSOQ2AirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object DetailSOQ2Rute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object DetailSOQ2TglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object DetailSOQ2Armada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object DetailSOQ2Kontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object DetailSOQ2PICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object DetailSOQ2JamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object DetailSOQ2NoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object DetailSOQ2AlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object DetailSOQ2Status: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DetailSOQ2StatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object DetailSOQ2ReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object DetailSOQ2PenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object DetailSOQ2Keterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailSOQ2CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object DetailSOQ2CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object DetailSOQ2Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object DetailSOQ2TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object DetailSOQ2TglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object DetailSOQ2KomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object DetailSOQ2KeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object DetailSOQ2KeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
    object DetailSOQ2JumlahBayar: TCurrencyField
      FieldName = 'JumlahBayar'
    end
    object DetailSOQ2SisaBayar: TCurrencyField
      FieldName = 'SisaBayar'
    end
  end
  object SDUpdateSQL7: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganC' +
        'araPembayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPemb' +
        'ayaranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, Car' +
        'aPembayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelu' +
        'nasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglK' +
        'embaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet,' +
        ' AirSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, J' +
        'amJemput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayara' +
        'n, ReminderPending, PenerimaPending, Keterangan, CreateDate, Cre' +
        'ateBy, Operator, TglEntry, TglCetak, KomisiPelanggan, Keterangan' +
        'Rute, KeteranganHarga'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  KeteranganCaraPembayaranAwal = :KeteranganCaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  KetCaraPembayaranPelunasan = :KetCaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak,'
      '  KomisiPelanggan = :KomisiPelanggan,'
      '  KeteranganRute = :KeteranganRute,'
      '  KeteranganHarga = :KeteranganHarga'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, KeteranganCaraP' +
        'embayaranAwal, NoKwitansiPembayaranAwal, NominalKwitansiPembayar' +
        'anAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPem' +
        'bayaranPelunasan, KetCaraPembayaranPelunasan, NoKwitansiPelunasa' +
        'n, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, TglKemba' +
        'liExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toilet, Air' +
        'Suspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJe' +
        'mput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, R' +
        'eminderPending, PenerimaPending, Keterangan, CreateDate, CreateB' +
        'y, Operator, TglEntry, TglCetak, KomisiPelanggan, KeteranganRute' +
        ', KeteranganHarga)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :Kete' +
        'ranganCaraPembayaranAwal, :NoKwitansiPembayaranAwal, :NominalKwi' +
        'tansiPembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPe' +
        'lunasan, :CaraPembayaranPelunasan, :KetCaraPembayaranPelunasan, ' +
        ':NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPeluna' +
        'san, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :Kapa' +
        'sitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :A' +
        'rmada, :Kontrak, :PICJemput, :JamJemput, :NoTelpPICJemput, :Alam' +
        'atJemput, :Status, :StatusPembayaran, :ReminderPending, :Penerim' +
        'aPending, :Keterangan, :CreateDate, :CreateBy, :Operator, :TglEn' +
        'try, :TglCetak, :KomisiPelanggan, :KeteranganRute, :KeteranganHa' +
        'rga)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 616
    Top = 80
  end
  object CekPICQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from MasterSO where kodenota=:text')
    Left = 448
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekPICQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object CekPICQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object CekPICQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object CekPICQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object CekPICQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object CekPICQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object CekPICQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object CekPICQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object CekPICQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object CekPICQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object CekPICQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object CekPICQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object CekPICQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object CekPICQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object CekPICQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object CekPICQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object CekPICQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object CekPICQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object CekPICQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object CekPICQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object CekPICQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object CekPICQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object CekPICQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object CekPICQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object CekPICQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object CekPICQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object CekPICQAC: TBooleanField
      FieldName = 'AC'
    end
    object CekPICQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object CekPICQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object CekPICQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object CekPICQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object CekPICQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object CekPICQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object CekPICQTitlePICJemput: TStringField
      FieldName = 'TitlePICJemput'
      Size = 50
    end
    object CekPICQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object CekPICQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object CekPICQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object CekPICQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object CekPICQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object CekPICQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object CekPICQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object CekPICQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object CekPICQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CekPICQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CekPICQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CekPICQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CekPICQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CekPICQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object CekPICQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object CekPICQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object CekPICQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
  end
  object CekPrintQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 224
    Top = 56
  end
  object TempQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 448
    Top = 416
  end
  object CekHargaSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select distinct so.Harga from DetailKwitansiSO dso '
      'left join MasterSO so on dso.KodeSO=so.Kodenota'
      'left join Armada a on so.Armada=a.Kode'
      'where dso.KodeKwitansi=:text and so.KapasitasSeat=:text2')
    UpdateObject = CekHargaSOUs
    Left = 480
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object CekHargaSOQHarga: TCurrencyField
      FieldName = 'Harga'
    end
  end
  object CekHargaSOUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO, Nominal'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeSO, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 512
    Top = 360
  end
  object GetKodeGrupQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select KodeGrup from DetailGrupPelanggan'
      'where Pelanggan=:text')
    UpdateObject = GetKodeGrupUs
    Left = 968
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object GetKodeGrupQKodeGrup: TStringField
      FieldName = 'KodeGrup'
      Required = True
      Size = 10
    end
  end
  object GetKodeGrupUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeGrup, Pelanggan'
      'from DetailGrupPelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    ModifySQL.Strings = (
      'update DetailGrupPelanggan'
      'set'
      '  KodeGrup = :KodeGrup,'
      '  Pelanggan = :Pelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    InsertSQL.Strings = (
      'insert into DetailGrupPelanggan'
      '  (KodeGrup, Pelanggan)'
      'values'
      '  (:KodeGrup, :Pelanggan)')
    DeleteSQL.Strings = (
      'delete from DetailGrupPelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    Left = 1008
    Top = 184
  end
  object GetDetailGrupQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from DetailGrupPelanggan where KodeGrup=:text'
      'and Pelanggan<>:text2')
    UpdateObject = GetDetailGrupUs
    Left = 976
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object GetDetailGrupQKodeGrup: TStringField
      FieldName = 'KodeGrup'
      Required = True
      Size = 10
    end
    object GetDetailGrupQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
  end
  object GetDetailGrupUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeGrup, Pelanggan'
      'from DetailGrupPelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    ModifySQL.Strings = (
      'update DetailGrupPelanggan'
      'set'
      '  KodeGrup = :KodeGrup,'
      '  Pelanggan = :Pelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    InsertSQL.Strings = (
      'insert into DetailGrupPelanggan'
      '  (KodeGrup, Pelanggan)'
      'values'
      '  (:KodeGrup, :Pelanggan)')
    DeleteSQL.Strings = (
      'delete from DetailGrupPelanggan'
      'where'
      '  KodeGrup = :OLD_KodeGrup and'
      '  Pelanggan = :OLD_Pelanggan')
    Left = 1016
    Top = 224
  end
  object CekCetakanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 Kode from CetakanKuitansi'
      'where Kode=:text')
    Left = 776
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CekCetakanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
  end
end
