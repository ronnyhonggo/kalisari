unit PembuatanKwitansi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, DB, SDEngine, StdCtrls, cxTextEdit, cxMaskEdit,
  cxButtonEdit, ExtCtrls, ComCtrls, cxStyles, cxRadioGroup, cxVGrid,
  cxDBVGrid, cxInplaceContainer, Menus, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, cxButtons, cxCurrencyEdit, UCrpeClasses,
  UCrpe32, cxLabel, cxDropDownEdit, cxCalendar, cxGroupBox, cxCheckBox,
  cxSpinEdit;

type
  TPembuatanKwitansiFm = class(TForm)
    StatusBar: TStatusBar;
    pnl1: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    Panel2: TPanel;
    Panel4: TPanel;
    RbtKontrak: TRadioButton;
    RbtSO: TRadioButton;
    MasterQ: TSDQuery;
    MasterUs: TSDUpdateSQL;
    MasterDs: TDataSource;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQNominal: TCurrencyField;
    MasterQPelanggan: TStringField;
    MasterQPenerimaPembayaran: TStringField;
    MasterQKeteranganPembayaran: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridNominal: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridPenerimaPembayaran: TcxDBEditorRow;
    MasterVGridCaraPembayaran: TcxDBEditorRow;
    MasterVGridKeteranganPembayaran: TcxDBEditorRow;
    Panel3: TPanel;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    MasterQNamaPelanggan: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridNamaPenerima: TcxDBEditorRow;
    MasterVGridJabatanPenerima: TcxDBEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SOQ: TSDQuery;
    DataSource1: TDataSource;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    DetailSOQ: TSDQuery;
    DetailKontrakQ: TSDQuery;
    DetailSOQKodeKwitansi: TStringField;
    DetailSOQKodeSO: TStringField;
    cxGrid1DBTableView1KodeSO: TcxGridDBColumn;
    SDUpdateSQL1: TSDUpdateSQL;
    DataSource2: TDataSource;
    SDUpdateSQL2: TSDUpdateSQL;
    DetailKontrakQKodeKwitansi: TStringField;
    DetailKontrakQKodeKontrak: TStringField;
    DetailSOQJumlahBayar: TCurrencyField;
    DetailSOQTanggalSO: TDateTimeField;
    HitungSOQ: TSDQuery;
    DetailSOQHarga: TCurrencyField;
    DetailSOQPPN: TCurrencyField;
    DetailSOQBiayaExtend: TCurrencyField;
    DetailSOQPPNExtend: TCurrencyField;
    cxGrid1DBTableView1JumlahBayar: TcxGridDBColumn;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQPOEksternal: TStringField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    DetailKontrakQTglMulai: TDateTimeField;
    DetailKontrakQTglSelesai: TDateTimeField;
    DetailKontrakQHarga: TCurrencyField;
    DetailKontrakQStatus: TStringField;
    Crpe1: TCrpe;
    Panel1: TPanel;
    SaveBtn: TcxButton;
    DeleteBtn: TcxButton;
    ExitBtn: TcxButton;
    Panel6: TPanel;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    viewKwitansiQ: TSDQuery;
    DataSource3: TDataSource;
    viewKwitansiQKode: TStringField;
    viewKwitansiQTanggal: TDateTimeField;
    viewKwitansiQNominal: TCurrencyField;
    viewKwitansiQPelanggan: TStringField;
    viewKwitansiQPenerimaPembayaran: TStringField;
    viewKwitansiQKeteranganPembayaran: TMemoField;
    viewKwitansiQCreateDate: TDateTimeField;
    viewKwitansiQTglEntry: TDateTimeField;
    viewKwitansiQCreateBy: TStringField;
    viewKwitansiQOperator: TStringField;
    cxGrid3DBTableView1Kode: TcxGridDBColumn;
    cxGrid3DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid3DBTableView1Nominal: TcxGridDBColumn;
    cxGrid3DBTableView1CaraPembayaran: TcxGridDBColumn;
    cxGrid3DBTableView1KeteranganPembayaran: TcxGridDBColumn;
    viewKwitansiQNamaPelanggan: TStringField;
    viewKwitansiQNamaPenerimaPembayaran: TStringField;
    cxGrid3DBTableView1NamaPelanggan: TcxGridDBColumn;
    cxGrid3DBTableView1NamaPenerimaPembayaran: TcxGridDBColumn;
    CekDetailQ: TSDQuery;
    CekDetailQCek: TIntegerField;
    MasterQPPN: TCurrencyField;
    MasterQMarkUp: TCurrencyField;
    MasterQUangMuka: TCurrencyField;
    MasterQStatusKwitansi: TStringField;
    ArmadaSOUs: TSDUpdateSQL;
    SOIsiQ: TSDQuery;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    ArmadaSOQ: TSDQuery;
    JumlahArmadaSOQ: TSDQuery;
    SOIsiUs: TSDUpdateSQL;
    MasterVGridPPN: TcxDBEditorRow;
    MasterVGridMarkUp: TcxDBEditorRow;
    MasterQKeperluan: TStringField;
    KontrakIsiQ: TSDQuery;
    KontrakIsiUs: TSDUpdateSQL;
    KontrakIsiQKode: TStringField;
    KontrakIsiQPelanggan: TStringField;
    KontrakIsiQTglMulai: TDateTimeField;
    KontrakIsiQTglSelesai: TDateTimeField;
    KontrakIsiQStatusRute: TStringField;
    KontrakIsiQRute: TStringField;
    KontrakIsiQAC: TBooleanField;
    KontrakIsiQToilet: TBooleanField;
    KontrakIsiQAirSuspension: TBooleanField;
    KontrakIsiQKapasitasSeat: TIntegerField;
    KontrakIsiQHarga: TCurrencyField;
    KontrakIsiQPOEksternal: TStringField;
    KontrakIsiQStatus: TStringField;
    KontrakIsiQKeterangan: TMemoField;
    KontrakIsiQIntervalPenagihan: TStringField;
    KontrakIsiQCreateDate: TDateTimeField;
    KontrakIsiQCreateBy: TStringField;
    KontrakIsiQOperator: TStringField;
    KontrakIsiQTglEntry: TDateTimeField;
    KontrakIsiQTglCetak: TDateTimeField;
    KontrakIsiQTujuan: TStringField;
    KontrakIsiQTglMulaiP: TStringField;
    KontrakIsiQTglSelesaiP: TStringField;
    JumlahArmadaKontrakQ: TSDQuery;
    ArmadaKontrakUs: TSDUpdateSQL;
    ArmadaKontrakQ: TSDQuery;
    JumlahArmadaKontrakQJumSeat: TIntegerField;
    MasterQGrandTotal: TCurrencyField;
    MasterQSisaBayar: TCurrencyField;
    MasterVGridGrandTotal: TcxDBEditorRow;
    MasterVGridSisaBayar: TcxDBEditorRow;
    PPNMarkupSOQ: TSDQuery;
    PPNMarkupSOQPPN: TCurrencyField;
    PPNMarkupSOQMarkUp: TCurrencyField;
    TotalNominalSOQ: TSDQuery;
    TotalNominalSOQNominal: TCurrencyField;
    HapusSOQ: TSDQuery;
    SDUpdateSQL3: TSDUpdateSQL;
    HapusSOQKodeKwitansi: TStringField;
    HapusSOQKodeSO: TStringField;
    SDQuery1: TSDQuery;
    SDQuery1KodeKwitansi: TStringField;
    SDQuery1KodeSO: TStringField;
    SDUpdateSQL4: TSDUpdateSQL;
    HapusKontrakQ: TSDQuery;
    SDQuery3: TSDQuery;
    SDUpdateSQL5: TSDUpdateSQL;
    SDUpdateSQL6: TSDUpdateSQL;
    HapusKontrakQKodeKwitansi: TStringField;
    HapusKontrakQKodeKontrak: TStringField;
    SDQuery3KodeKwitansi: TStringField;
    SDQuery3KodeKontrak: TStringField;
    HitungTotalSOKlikQ: TSDQuery;
    HitungTotalSOKlikQKode: TStringField;
    HitungTotalSOKlikQTanggal: TDateTimeField;
    HitungTotalSOKlikQNominal: TCurrencyField;
    HitungTotalSOKlikQPPN: TCurrencyField;
    HitungTotalSOKlikQMarkUp: TCurrencyField;
    HitungTotalSOKlikQUangMuka: TCurrencyField;
    HitungTotalSOKlikQPelanggan: TStringField;
    HitungTotalSOKlikQPenerimaPembayaran: TStringField;
    HitungTotalSOKlikQCaraPembayaran: TStringField;
    HitungTotalSOKlikQKeteranganPembayaran: TMemoField;
    HitungTotalSOKlikQKeperluan: TStringField;
    HitungTotalSOKlikQStatusKwitansi: TStringField;
    HitungTotalSOKlikQCreateDate: TDateTimeField;
    HitungTotalSOKlikQTglEntry: TDateTimeField;
    HitungTotalSOKlikQCreateBy: TStringField;
    HitungTotalSOKlikQOperator: TStringField;
    HitungTotalSOKlikQKodeKwitansi: TStringField;
    HitungTotalSOKlikQKodeSO: TStringField;
    HitungNominalLunasQ: TSDQuery;
    HitungNominalLunasQNominal: TCurrencyField;
    IsiLunasSOQ: TSDQuery;
    IsiLunasSOQKodenota: TStringField;
    IsiLunasSOQTgl: TDateTimeField;
    IsiLunasSOQPelanggan: TStringField;
    IsiLunasSOQBerangkat: TDateTimeField;
    IsiLunasSOQTiba: TDateTimeField;
    IsiLunasSOQHarga: TCurrencyField;
    IsiLunasSOQPPN: TCurrencyField;
    IsiLunasSOQPembayaranAwal: TCurrencyField;
    IsiLunasSOQTglPembayaranAwal: TDateTimeField;
    IsiLunasSOQCaraPembayaranAwal: TStringField;
    IsiLunasSOQKeteranganCaraPembayaranAwal: TStringField;
    IsiLunasSOQNoKwitansiPembayaranAwal: TStringField;
    IsiLunasSOQNominalKwitansiPembayaranAwal: TCurrencyField;
    IsiLunasSOQPenerimaPembayaranAwal: TStringField;
    IsiLunasSOQPelunasan: TCurrencyField;
    IsiLunasSOQTglPelunasan: TDateTimeField;
    IsiLunasSOQCaraPembayaranPelunasan: TStringField;
    IsiLunasSOQKetCaraPembayaranPelunasan: TStringField;
    IsiLunasSOQNoKwitansiPelunasan: TStringField;
    IsiLunasSOQNominalKwitansiPelunasan: TCurrencyField;
    IsiLunasSOQPenerimaPelunasan: TStringField;
    IsiLunasSOQExtend: TBooleanField;
    IsiLunasSOQTglKembaliExtend: TDateTimeField;
    IsiLunasSOQBiayaExtend: TCurrencyField;
    IsiLunasSOQPPNExtend: TCurrencyField;
    IsiLunasSOQKapasitasSeat: TIntegerField;
    IsiLunasSOQAC: TBooleanField;
    IsiLunasSOQToilet: TBooleanField;
    IsiLunasSOQAirSuspension: TBooleanField;
    IsiLunasSOQRute: TStringField;
    IsiLunasSOQTglFollowUp: TDateTimeField;
    IsiLunasSOQArmada: TStringField;
    IsiLunasSOQKontrak: TStringField;
    IsiLunasSOQPICJemput: TMemoField;
    IsiLunasSOQJamJemput: TDateTimeField;
    IsiLunasSOQNoTelpPICJemput: TStringField;
    IsiLunasSOQAlamatJemput: TMemoField;
    IsiLunasSOQStatus: TStringField;
    IsiLunasSOQStatusPembayaran: TStringField;
    IsiLunasSOQReminderPending: TDateTimeField;
    IsiLunasSOQPenerimaPending: TStringField;
    IsiLunasSOQKeterangan: TMemoField;
    IsiLunasSOQCreateDate: TDateTimeField;
    IsiLunasSOQCreateBy: TStringField;
    IsiLunasSOQOperator: TStringField;
    IsiLunasSOQTglEntry: TDateTimeField;
    IsiLunasSOQTglCetak: TDateTimeField;
    IsiLunasSOQTujuan: TStringField;
    IsiLunasSOQTglBerangkat: TStringField;
    IsiLunasSOQTglTiba: TStringField;
    IsiLunasSOQJamSelesai: TStringField;
    IsiLunasSOQJamMulai: TStringField;
    ArmadaSOLunasQ: TSDQuery;
    ArmadaSOLunasUs: TSDUpdateSQL;
    HitungArmadaSOLunasQ: TSDQuery;
    ArmadaSOLunasQJumlahSeat: TIntegerField;
    HitungArmadaSOLunasQJumSeat: TIntegerField;
    DetailSOQNominal: TCurrencyField;
    DetailKontrakQNominal: TCurrencyField;
    DetailSOQTglBerangkat: TDateTimeField;
    DetailSOQKodeRute: TStringField;
    DetailSOQDari: TStringField;
    DetailSOQTujuan: TStringField;
    cxGrid1DBTableView1Nominal: TcxGridDBColumn;
    cxGrid1DBTableView1TglBerangkat: TcxGridDBColumn;
    cxGrid1DBTableView1Dari: TcxGridDBColumn;
    cxGrid1DBTableView1Tujuan: TcxGridDBColumn;
    SOQKomisiPelanggan: TCurrencyField;
    DetailSOQKomisiPelanggan: TCurrencyField;
    DetailSOQSisaBayar: TCurrencyField;
    cxGrid1DBTableView1SisaBayar: TcxGridDBColumn;
    HitungNominalSOQ: TSDQuery;
    HitungNominalSOQTotalBayar: TCurrencyField;
    SOIsiQKodenota: TStringField;
    SOIsiQTgl: TDateTimeField;
    SOIsiQPelanggan: TStringField;
    SOIsiQBerangkat: TDateTimeField;
    SOIsiQTiba: TDateTimeField;
    SOIsiQHarga: TCurrencyField;
    SOIsiQPPN: TCurrencyField;
    SOIsiQPembayaranAwal: TCurrencyField;
    SOIsiQTglPembayaranAwal: TDateTimeField;
    SOIsiQCaraPembayaranAwal: TStringField;
    SOIsiQKeteranganCaraPembayaranAwal: TStringField;
    SOIsiQNoKwitansiPembayaranAwal: TStringField;
    SOIsiQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOIsiQPenerimaPembayaranAwal: TStringField;
    SOIsiQPelunasan: TCurrencyField;
    SOIsiQTglPelunasan: TDateTimeField;
    SOIsiQCaraPembayaranPelunasan: TStringField;
    SOIsiQKetCaraPembayaranPelunasan: TStringField;
    SOIsiQNoKwitansiPelunasan: TStringField;
    SOIsiQNominalKwitansiPelunasan: TCurrencyField;
    SOIsiQPenerimaPelunasan: TStringField;
    SOIsiQExtend: TBooleanField;
    SOIsiQTglKembaliExtend: TDateTimeField;
    SOIsiQBiayaExtend: TCurrencyField;
    SOIsiQPPNExtend: TCurrencyField;
    SOIsiQKapasitasSeat: TIntegerField;
    SOIsiQAC: TBooleanField;
    SOIsiQToilet: TBooleanField;
    SOIsiQAirSuspension: TBooleanField;
    SOIsiQRute: TStringField;
    SOIsiQTglFollowUp: TDateTimeField;
    SOIsiQArmada: TStringField;
    SOIsiQKontrak: TStringField;
    SOIsiQPICJemput: TMemoField;
    SOIsiQJamJemput: TDateTimeField;
    SOIsiQNoTelpPICJemput: TStringField;
    SOIsiQAlamatJemput: TMemoField;
    SOIsiQStatus: TStringField;
    SOIsiQStatusPembayaran: TStringField;
    SOIsiQReminderPending: TDateTimeField;
    SOIsiQPenerimaPending: TStringField;
    SOIsiQKeterangan: TMemoField;
    SOIsiQCreateDate: TDateTimeField;
    SOIsiQCreateBy: TStringField;
    SOIsiQOperator: TStringField;
    SOIsiQTglEntry: TDateTimeField;
    SOIsiQTglCetak: TDateTimeField;
    SOIsiQKomisiPelanggan: TCurrencyField;
    SOIsiQTglBerangkat: TStringField;
    SOIsiQTglTiba: TStringField;
    SOIsiQJamSelesai: TStringField;
    SOIsiQJamMulai: TStringField;
    SOIsiQDari: TStringField;
    SOIsiQTujuan: TStringField;
    ArmadaSOQKapasitasSeat: TIntegerField;
    JumlahArmadaSOQJumSeat: TIntegerField;
    ArmadaKontrakQKapasitasSeat: TIntegerField;
    KontrakIsiQDari: TStringField;
    viewKwitansiQPPN: TCurrencyField;
    viewKwitansiQMarkUp: TCurrencyField;
    viewKwitansiQUangMuka: TCurrencyField;
    viewKwitansiQKeperluan: TStringField;
    viewKwitansiQStatusKwitansi: TStringField;
    viewKwitansiQnamaPT: TStringField;
    MasterQUntuk: TStringField;
    MasterVGridUntuk: TcxDBEditorRow;
    MasterQMarkUpNominal: TCurrencyField;
    MarkUpNominal: TcxDBEditorRow;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    CetakBtn: TcxButton;
    cxGrid3DBTableView1TglBerangkat: TcxGridDBColumn;
    viewKwitansiQUntuk: TStringField;
    viewKwitansiQMarkUpNominal: TCurrencyField;
    CekAirSusQ: TSDQuery;
    CekToiletQ: TSDQuery;
    CekToiletUs: TSDUpdateSQL;
    CekAirSusUs: TSDUpdateSQL;
    CekToiletQJumT: TIntegerField;
    CekAirSusQJumAS: TIntegerField;
    CekHargaArmadaQ: TSDQuery;
    CekHargaArmadaQharga: TCurrencyField;
    POEksternalQ: TSDQuery;
    POEksternalUs: TSDUpdateSQL;
    POEksternalQPOEksternal: TStringField;
    MasterQCaraPembayaran: TStringField;
    viewKwitansiQCaraPembayaran: TStringField;
    DetailKwitansiSOQ: TSDQuery;
    DetailKwitansiSOQKodeKwitansi: TStringField;
    DetailKwitansiSOQKodeSO: TStringField;
    DetailKwitansiSOQNominal: TCurrencyField;
    viewKwitansiQNoSO: TStringField;
    viewKwitansiQBerangkat: TDateTimeField;
    CetakanKuitansiQ: TSDQuery;
    CetakanKuitansiUs: TSDUpdateSQL;
    CetakanKuitansiQKode: TStringField;
    CetakanKuitansiQTerbilang: TMemoField;
    CetakanKuitansiQGrandTotal: TMemoField;
    CetakanKuitansiQTujuan: TMemoField;
    CetakanKuitansiQJamMulai: TMemoField;
    CetakanKuitansiQTempat: TMemoField;
    CetakanKuitansiQPemakaian: TMemoField;
    CetakanKuitansiQTersisa: TMemoField;
    CetakanKuitansiQNominal: TMemoField;
    CetakanKuitansiQTglPemakaian: TMemoField;
    CetakanKuitansiQNamaTtd: TMemoField;
    IsiPICQ: TSDQuery;
    IsiPICQKode: TStringField;
    IsiPICQNamaPT: TStringField;
    IsiPICQAlamat: TStringField;
    IsiPICQKota: TStringField;
    IsiPICQNoTelp: TStringField;
    IsiPICQEmail: TStringField;
    IsiPICQNoFax: TStringField;
    IsiPICQNamaPIC1: TStringField;
    IsiPICQTelpPIC1: TStringField;
    IsiPICQJabatanPIC1: TStringField;
    IsiPICQNamaPIC2: TStringField;
    IsiPICQTelpPIC2: TStringField;
    IsiPICQJabatanPIC2: TStringField;
    IsiPICQNamaPIC3: TStringField;
    IsiPICQTelpPIC3: TStringField;
    IsiPICQJabatanPIC3: TStringField;
    IsiPICQCreateDate: TDateTimeField;
    IsiPICQCreateBy: TStringField;
    IsiPICQOperator: TStringField;
    IsiPICQTglEntry: TDateTimeField;
    IsiPICQTitle: TStringField;
    HapusCetakanKuitansiQ: TSDQuery;
    MasterQTitipKuitansi: TBooleanField;
    MasterQTitipTagihan: TBooleanField;
    MasterVGridTitipKuitansi: TcxDBEditorRow;
    MasterVGridTitipTagihan: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    Label3: TLabel;
    SearchNamaQ: TSDQuery;
    DetailSOQ2: TSDQuery;
    SDUpdateSQL7: TSDUpdateSQL;
    DetailSOQ2Kodenota: TStringField;
    DetailSOQ2Tgl: TDateTimeField;
    DetailSOQ2Pelanggan: TStringField;
    DetailSOQ2Berangkat: TDateTimeField;
    DetailSOQ2Tiba: TDateTimeField;
    DetailSOQ2Harga: TCurrencyField;
    DetailSOQ2PPN: TCurrencyField;
    DetailSOQ2PembayaranAwal: TCurrencyField;
    DetailSOQ2TglPembayaranAwal: TDateTimeField;
    DetailSOQ2CaraPembayaranAwal: TStringField;
    DetailSOQ2KeteranganCaraPembayaranAwal: TStringField;
    DetailSOQ2NoKwitansiPembayaranAwal: TStringField;
    DetailSOQ2NominalKwitansiPembayaranAwal: TCurrencyField;
    DetailSOQ2PenerimaPembayaranAwal: TStringField;
    DetailSOQ2Pelunasan: TCurrencyField;
    DetailSOQ2TglPelunasan: TDateTimeField;
    DetailSOQ2CaraPembayaranPelunasan: TStringField;
    DetailSOQ2KetCaraPembayaranPelunasan: TStringField;
    DetailSOQ2NoKwitansiPelunasan: TStringField;
    DetailSOQ2NominalKwitansiPelunasan: TCurrencyField;
    DetailSOQ2PenerimaPelunasan: TStringField;
    DetailSOQ2Extend: TBooleanField;
    DetailSOQ2TglKembaliExtend: TDateTimeField;
    DetailSOQ2BiayaExtend: TCurrencyField;
    DetailSOQ2PPNExtend: TCurrencyField;
    DetailSOQ2KapasitasSeat: TIntegerField;
    DetailSOQ2AC: TBooleanField;
    DetailSOQ2Toilet: TBooleanField;
    DetailSOQ2AirSuspension: TBooleanField;
    DetailSOQ2Rute: TStringField;
    DetailSOQ2TglFollowUp: TDateTimeField;
    DetailSOQ2Armada: TStringField;
    DetailSOQ2Kontrak: TStringField;
    DetailSOQ2PICJemput: TMemoField;
    DetailSOQ2JamJemput: TDateTimeField;
    DetailSOQ2NoTelpPICJemput: TStringField;
    DetailSOQ2AlamatJemput: TMemoField;
    DetailSOQ2Status: TStringField;
    DetailSOQ2StatusPembayaran: TStringField;
    DetailSOQ2ReminderPending: TDateTimeField;
    DetailSOQ2PenerimaPending: TStringField;
    DetailSOQ2Keterangan: TMemoField;
    DetailSOQ2CreateDate: TDateTimeField;
    DetailSOQ2CreateBy: TStringField;
    DetailSOQ2Operator: TStringField;
    DetailSOQ2TglEntry: TDateTimeField;
    DetailSOQ2TglCetak: TDateTimeField;
    DetailSOQ2KomisiPelanggan: TCurrencyField;
    DetailSOQ2KeteranganRute: TMemoField;
    DetailSOQ2KeteranganHarga: TMemoField;
    DetailSOQ2JumlahBayar: TCurrencyField;
    DetailSOQ2SisaBayar: TCurrencyField;
    MasterQPeriode: TStringField;
    MasterVGridPeriode: TcxDBEditorRow;
    ApproveBtn: TcxButton;
    CekPICQ: TSDQuery;
    CekPICQKodenota: TStringField;
    CekPICQTgl: TDateTimeField;
    CekPICQPelanggan: TStringField;
    CekPICQBerangkat: TDateTimeField;
    CekPICQTiba: TDateTimeField;
    CekPICQHarga: TCurrencyField;
    CekPICQPPN: TCurrencyField;
    CekPICQPembayaranAwal: TCurrencyField;
    CekPICQTglPembayaranAwal: TDateTimeField;
    CekPICQCaraPembayaranAwal: TStringField;
    CekPICQKeteranganCaraPembayaranAwal: TStringField;
    CekPICQNoKwitansiPembayaranAwal: TStringField;
    CekPICQNominalKwitansiPembayaranAwal: TCurrencyField;
    CekPICQPenerimaPembayaranAwal: TStringField;
    CekPICQPelunasan: TCurrencyField;
    CekPICQTglPelunasan: TDateTimeField;
    CekPICQCaraPembayaranPelunasan: TStringField;
    CekPICQKetCaraPembayaranPelunasan: TStringField;
    CekPICQNoKwitansiPelunasan: TStringField;
    CekPICQNominalKwitansiPelunasan: TCurrencyField;
    CekPICQPenerimaPelunasan: TStringField;
    CekPICQExtend: TBooleanField;
    CekPICQTglKembaliExtend: TDateTimeField;
    CekPICQBiayaExtend: TCurrencyField;
    CekPICQPPNExtend: TCurrencyField;
    CekPICQKapasitasSeat: TIntegerField;
    CekPICQAC: TBooleanField;
    CekPICQToilet: TBooleanField;
    CekPICQAirSuspension: TBooleanField;
    CekPICQRute: TStringField;
    CekPICQTglFollowUp: TDateTimeField;
    CekPICQArmada: TStringField;
    CekPICQKontrak: TStringField;
    CekPICQTitlePICJemput: TStringField;
    CekPICQPICJemput: TMemoField;
    CekPICQJamJemput: TDateTimeField;
    CekPICQNoTelpPICJemput: TStringField;
    CekPICQAlamatJemput: TMemoField;
    CekPICQStatus: TStringField;
    CekPICQStatusPembayaran: TStringField;
    CekPICQReminderPending: TDateTimeField;
    CekPICQPenerimaPending: TStringField;
    CekPICQKeterangan: TMemoField;
    CekPICQCreateDate: TDateTimeField;
    CekPICQCreateBy: TStringField;
    CekPICQOperator: TStringField;
    CekPICQTglEntry: TDateTimeField;
    CekPICQTglCetak: TDateTimeField;
    CekPICQKomisiPelanggan: TCurrencyField;
    CekPICQKeteranganRute: TMemoField;
    CekPICQKeteranganHarga: TMemoField;
    DetailSOQBerangkat: TDateTimeField;
    CetakanKuitansiQPIC: TMemoField;
    KontrakQPlatNo: TStringField;
    KontrakQJumlahUnit: TIntegerField;
    DetailKontrakQJumlahUnit: TIntegerField;
    DetailSOQKeterangan: TStringField;
    SOIsiQTitlePICJemput: TStringField;
    SOIsiQKeteranganRute: TMemoField;
    SOIsiQKeteranganHarga: TMemoField;
    CekPrintQ: TSDQuery;
    MasterQCekPrint: TBooleanField;
    viewKwitansiQTgl: TStringField;
    viewKwitansiQTitipKuitansi: TBooleanField;
    viewKwitansiQTitipTagihan: TBooleanField;
    viewKwitansiQPeriode: TStringField;
    viewKwitansiQCekPrint: TBooleanField;
    cxGrid3DBTableView1CekPrint: TcxGridDBColumn;
    MasterVGridTanggalPembayaran: TcxDBEditorRow;
    MasterQTanggalPembayaran: TDateTimeField;
    cxGrid3DBTableView1StatusKwitansi: TcxGridDBColumn;
    MasterQPPh: TBooleanField;
    MasterVGridPPh: TcxDBEditorRow;
    CetakanKuitansiQKomisiPelanggan: TMemoField;
    CetakanKuitansiQKomisiPelangganNominal: TMemoField;
    cxButton1: TcxButton;
    MasterQKotaPelanggan: TStringField;
    cxGroupBox2: TcxGroupBox;
    cxSpinEdit1: TcxSpinEdit;
    cxLabel1: TcxLabel;
    cxSpinEdit2: TcxSpinEdit;
    btnLunas: TcxButton;
    TempQ: TSDQuery;
    MasterVGridStatusKwitansi: TcxDBEditorRow;
    CekHargaSOQ: TSDQuery;
    CekHargaSOUs: TSDUpdateSQL;
    CekHargaSOQHarga: TCurrencyField;
    MasterQDiskon: TCurrencyField;
    MasterVGridDiskon: TcxDBEditorRow;
    CetakanKuitansiQDiskon: TMemoField;
    cxButton2: TcxButton;
    GetKodeGrupQ: TSDQuery;
    GetKodeGrupUs: TSDUpdateSQL;
    GetDetailGrupQ: TSDQuery;
    GetDetailGrupUs: TSDUpdateSQL;
    GetDetailGrupQKodeGrup: TStringField;
    GetDetailGrupQPelanggan: TStringField;
    GetKodeGrupQKodeGrup: TStringField;
    MasterQJenisKendaraan: TStringField;
    MasterVGridJenisKendaraan1: TcxDBEditorRow;
    CekCetakanQ: TSDQuery;
    CekCetakanQKode: TStringField;
    cxButton3: TcxButton;
    cxTextEdit2: TcxTextEdit;
    cxButton4: TcxButton;
    Panel5: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1KodeKontrak: TcxGridDBColumn;
    cxGridDBTableView1TglMulai: TcxGridDBColumn;
    cxGridDBTableView1TglSelesai: TcxGridDBColumn;
    cxGridDBTableView1JumlahUnit: TcxGridDBColumn;
    cxGridDBTableView1Harga: TcxGridDBColumn;
    cxGridDBTableView1Status: TcxGridDBColumn;
    cxGridDBTableView1Nominal: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PenerimaPembayaranEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure SaveBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1KodeSOPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure RbtSOClick(Sender: TObject);
    procedure RbtKontrakClick(Sender: TObject);
    procedure cxGrid2DBTableView1KodeKontrakPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1KodeKontrakPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid3DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure DeleteBtnClick(Sender: TObject);
    procedure MasterVGridPPNEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridMarkUpEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNominalEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1NominalPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MarkUpNominalEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure CetakBtnClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxTextEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ApproveBtnClick(Sender: TObject);
    procedure cxGrid3DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure DetailKontrakQBeforePost(DataSet: TDataSet);
    procedure cxGridDBTableView1NominalPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridPPhEditPropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxSpinEdit2PropertiesChange(Sender: TObject);
    procedure btnLunasClick(Sender: TObject);
    procedure MasterVGridTitipKuitansiEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure MasterVGridDiskonEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PembuatanKwitansiFm: TPembuatanKwitansiFm;
  MasterOriSQL, paramkode,NamaTtd, PIC, ViewKwitansiOriSQL,KeteranganPembayaran: string;
  Baru,cetak:integer;
  SelisihNominal,NominalAwal,SelisihPPN,PPNAwal,SelisihMarkup,MarkupAwal,MarkupNominalAwal:currency;
  pertama:integer;

implementation

uses DM, MenuUtama, DropDown, PegawaiDropDown, PelangganDropDown, SOKwitansiDropDown,
  SODropDown, KontrakDropDown, StrUtils, CetakanKuitansi;

{$R *.dfm}

procedure TPembuatanKwitansiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Release;
end;

procedure TPembuatanKwitansiFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  ViewKwitansiOriSQL:=viewKwitansiQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  //ViewKontrakQ.Open;
end;

procedure TPembuatanKwitansiFm.FormShow(Sender: TObject);
begin
  PelangganQ.Open;
  PegawaiQ.Open;
  RuteQ.Open;
  SOQ.Open;
  KontrakQ.Open;
  DetailKwitansiSOQ.Open;
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
   KodeEdit.Text:=paramkode;
   KodeEditExit(sender);
  end;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPembuatanKwitansi.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePembuatanKwitansi.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeletePembuatanKwitansi.AsBoolean;
  CetakBtn.Enabled:=menuutamafm.UserQCetakKwitansi.AsBoolean;
  btnLunas.Enabled:=menuutamafm.UserQLunasKwitansi.AsBoolean;
  ApproveBtn.Enabled:=menuutamafm.UserQApproveKwitansi.AsBoolean;
  panel5.Visible:=false;
  cxGrid2.Visible:=false;
  cxGrid1.Visible:=true;
  cxSpinEdit1.Value:=1;
  cxSpinEdit2.Value:=9999;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewKwitansiQ.Close;
  viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKwitansiQ.ParamByName('text3').AsInteger:=cxSpinEdit1.Value;
  viewKwitansiQ.ParamByName('text4').AsInteger:=cxSpinEdit2.Value;
  viewKwitansiQ.Open;
  //KodeEditPropertiesButtonClick(sender,0);
end;

procedure TPembuatanKwitansiFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPembuatanKwitansiFm.KodeEditExit(Sender: TObject);
var total,sisaBayar,totalnominal:currency;
begin
pertama:=1;
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQKeperluan.AsString='SO' then
    begin
      RbtKontrak.Checked:=false;
      RbtSO.Checked:=true;
      Panel5.Visible:=false;
      cxGrid2.Visible:=false;
      cxGrid1.Visible:=True;
      DetailSOQ.Close;
      DetailSOQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DetailSOQ.Open;
      DetailSOQ.Edit;
      DetailSOQ.First;
      while DetailSOQ.Eof=false do
      begin
        if currtostr(DetailSOQHarga.AsCurrency)='' then
        begin
          DetailSOQHarga.AsCurrency:=0;
        end;
        if currtostr(DetailSOQBiayaExtend.AsCurrency)='' then
        begin
          DetailSOQBiayaExtend.AsCurrency:=0;
        end;
        if currtostr(DetailSOQPPNExtend.AsCurrency)='' then
        begin
          DetailSOQPPNExtend.AsCurrency:=0;
        end;
        if currtostr(DetailSOQPPN.AsCurrency)='' then
        begin
          DetailSOQPPN.AsCurrency:=0;
        end;
        if currtostr(DetailSOQKomisiPelanggan.AsCurrency)='' then
        begin
          DetailSOQKomisiPelanggan.AsCurrency:=0;
        end;
        DetailSOQ.Edit;
        DetailSOQJumlahBayar.AsCurrency:=DetailSOQHarga.AsCurrency+DetailSOQBiayaExtend.AsCurrency+DetailSOQPPNExtend.AsCurrency;
        HitungNominalSOQ.Close;
        HitungNominalSOQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
        HitungNominalSOQ.Open;
        DetailSOQSisaBayar.AsCurrency:=DetailSOQJumlahBayar.AsCurrency-HitungNominalSOQTotalBayar.AsCurrency;
        total:=total+DetailSOQJumlahBayar.AsCurrency;
        sisaBayar:=sisaBayar+DetailSOQSisaBayar.AsCurrency;
        DetailSOQ.Next;
      end;
      MasterQ.Edit;
      MasterQGrandTotal.AsCurrency:=total;
      MasterQSisaBayar.AsCurrency:=sisaBayar;
    end;

    if MasterQKeperluan.AsString='KONTRAK' then
    begin
      //ShowMessage(MasterQKeperluan.AsString);
      RbtSO.Checked:=false;
      RbtKontrak.Checked:=true;
      RbtSO.Checked:=false;
      panel5.Visible:=True;
      cxGrid2.Visible:=True;
      cxGrid1.Visible:=False;
      DetailKontrakQ.Close;
      DetailKontrakQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DetailKontrakQ.Open;
      DetailKontrakQ.Edit;
      DetailKontrakQ.First;
      while DetailKontrakQ.Eof=false do
      begin
        total:=DetailKontrakQHarga.AsCurrency+total;
        totalNominal:=totalNominal+DetailKontrakQNominal.AsCurrency;
        DetailKontrakQ.Next;
      end;
      MasterQ.Edit;
      MasterQGrandTotal.AsCurrency:=total;
      MasterQSisaBayar.AsCurrency:=total-totalNominal;
    end;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
      MasterQKeteranganPembayaran.AsString:='';
      MasterQPPh.AsBoolean:=false;
      MasterQPPN.AsCurrency:=0;
      MasterQMarkUp.AsCurrency:=0;
      MasterQMarkUpNominal.AsCurrency:=0;
      MasterQDiskon.AsCurrency:=0;
      MasterQTitipKuitansi.AsBoolean:=false;
      MasterQTitipTagihan.AsBoolean:=false;
      PPNAwal:=0;
      MarkupAwal:=0;
      MarkUpNominalAwal:=0;
      NominalAwal:=0;
    end
    else
    begin
      Masterq.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeletePembuatanKwitansi.AsBoolean;
      ApproveBtn.Enabled:=menuutamafm.UserQApproveKwitansi.AsBoolean;
      btnLunas.Enabled:=menuutamafm.UserQLunasKwitansi.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePembuatanKwitansi.AsBoolean;
    MasterVGrid.Enabled:=True;
    {MasterQPPN.AsCurrency:=0;
    MasterQMarkUp.AsCurrency:=0;
    MasterQMarkUpNominal.AsCurrency:=0;
    MasterQDiskon.AsCurrency:=0;
    MasterQTitipKuitansi.AsBoolean:=false;
    MasterQTitipTagihan.AsBoolean:=false;
    }PPNAwal:=0;
    MarkupAwal:=0;
    MarkUpNominalAwal:=0;
    NominalAwal:=0;
    Baru:=1;
    cetak:=1;
    SOQ.Open;
    if KodeEdit.Text='' then
    begin
      DetailSOQ.Close;
      DetailSOQ.ParamByName('text').AsString:='';
      DetailSOQ.Open;
      DetailKontrakQ.Close;
      DetailKontrakQ.ParamByName('text').AsString:='';
      DetailKontrakQ.Open;
    end;
    viewKwitansiQ.Open;
    KeteranganPembayaran:=MasterQKeteranganPembayaran.AsString;
  end;
  cxTextEdit2.Text:='';
end;

procedure TPembuatanKwitansiFm.KodeEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPembuatanKwitansiFm.KodeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Close;
  MasterQ.Open;
  KodeEditExit(sender);
    DetailSOQ.Close;
  DetailSOQ.ParamByName('text').AsString:='';
  DetailSOQ.Open;

    DetailKontrakQ.Close;
  DetailKontrakQ.ParamByName('text').AsString:='';
  DetailKontrakQ.Open;
  Baru:=1;
  SelisihNominal:=0;
  MasterQ.Edit;
end;

procedure TPembuatanKwitansiFm.cxDBVerticalGrid1PenerimaPembayaranEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self,'Admin');
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerimaPembayaran.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TPembuatanKwitansiFm.cxDBVerticalGrid1PelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var total,sisa:currency;
begin
  PelangganDropDownFm:=TPelangganDropdownfm.Create(Self);
    if PelangganDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=PelangganDropDownFm.kode;
    end;
    PelangganDropDownFm.Release;

end;

procedure TPembuatanKwitansiFm.ExitBtnClick(Sender: TObject);
begin
close;
Release;
end;


procedure TPembuatanKwitansiFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
  var nominalSO,totalSO,sisaBayarSO,selisih : currency;
begin
  if abuttonindex=10 then
  begin
  nominalSO:=0;
  totalSO:=0;
  sisaBayarSO:=0;;
  DetailSOQ.Edit;
  DetailSOQKodeKwitansi.AsString:='0';
  DetailSOQ.Post;
  //hitung ulang
  if pertama=1 then
  begin
      KeteranganPembayaran:=MasterQKeteranganPembayaran.AsString;
      pertama:=0;
  end;
  MasterQKeteranganPembayaran.AsString:=KeteranganPembayaran;
  DetailSOQ.Edit;
  DetailSOQ.First;
  while DetailSOQ.Eof=false do
  begin
      nominalSO:=nominalSO+DetailSOQNominal.AsCurrency;
      totalSO:=totalSO+DetailSOQJumlahBayar.AsCurrency;
      sisaBayarSO:=sisaBayarSO+DetailSOQSisaBayar.AsCurrency;
      if DetailSOQKeterangan.AsString <> NULL then
      begin
        if DetailSOQKeterangan.AsString <> '' then
        begin
          MasterQ.Edit;
          if MasterQKeteranganPembayaran.AsString<>'' then
          begin
              MasterQKeteranganPembayaran.AsString:=MasterQKeteranganPembayaran.AsString+Chr(13)+Chr(10);
          end;
          MasterQKeteranganPembayaran.AsString:=MasterQKeteranganPembayaran.AsString+ 'SO '+DetailSOQKodeSO.AsString+': '+DetailSOQKeterangan.AsString;
        end;
      end;
      //MasterQKeteranganPembayaran.AsString:=MasterQKeteranganPembayaran.AsString;
      DetailSOQ.Next;
  end;
  MasterQ.Edit;
  MasterQNominal.AsCurrency:=nominalSO;
  MasterQGrandTotal.AsCurrency:=totalSO;
  //if DetailSOQ2Keterangan.AsString<>'' then
    //MasterQKeteranganPembayaran.AsString:=MasterQKeteranganPembayaran.AsString+DetailSOQ2Keterangan.AsString+' ';
  if Baru=1 then
  begin
     MasterQSisaBayar.AsCurrency:=sisaBayarSO-nominalSO;
  end
  else
  begin
      selisih:=nominalSO-NominalAwal;
      MasterQ.Edit;
      MasterQSisaBayar.AsCurrency:=sisaBayarSO-selisih;
  end;
  //akhir hitung ulang
  end;
end;


function PartialSpell( strInput : String) : String;
var strSebutan, strAngka, strHasil : String;
    sTAngka : char;
    i,sAngka : Byte;
begin
  For i := length(strInput) DownTo 1 do
   begin
    //--ambil satu digit
    sTAngka := strInput[i];
    if not (sTAngka  in ['0'..'9']) then continue;
    sAngka := strToInt(copy(strInput, i, 1));
    //--ubah angka menjadi huruf yang sesuai, kecuali angka nol
    Case sAngka of
       1: strAngka := 'satu ';
       2: strAngka := 'dua ';
       3: strAngka := 'tiga ';
       4: strAngka := 'empat ';
       5: strAngka := 'lima ';
       6: strAngka := 'enam ';
       7: strAngka := 'tujuh ';
       8: strAngka := 'delapan ';
       9: strAngka := 'sembilan ';
       0: begin
             strAngka := '';
             strSebutan := '';
          end;
    End;
    strSebutan := '';
    //--cek kondisi khusus untuk angka 1 yang bisa berubah jad 'se'
    If (sAngka = 1) And (i <> length(strInput)) Then strAngka := 'se';
    //--tambahkan satuan yang sesuai yaitu puluh, belas, ratus
    If ((length(strInput) - i) = 1) And (sAngka <> 0) Then
     begin
      strSebutan := 'puluh ';
      If (sAngka = 1) And (strHasil <> '') Then
       begin
        strSebutan := 'belas ';
        If strHasil = 'satu ' Then strAngka := 'se'
          Else strAngka := strHasil;
        strHasil := '';
       end
     End
     else if ((length(strInput) - i) = 2) And (sAngka <> 0) Then
         strSebutan := 'ratus ';
    strHasil := strAngka + strSebutan + strHasil
  end;
  PartialSpell := strHasil
end;

function Spell(strInput : String) : String ;
var i : integer;
    j : Byte;
  strPars, strOlah, strSebutan, strHasil, strSpell : String;
begin
  if length(strInput)>15 then raise Exception.Create('Nilai maksimal 999.999.999.999.999');
  strInput:=Copy(strInput,1,15);
  strPars := strInput;
  i := length(strInput);
  repeat
    //--mengambil angka 3 digit dimulai dari belakang
    j := length(strPars) Mod 3;
    If j <> 0 Then
    begin
      strOlah := copy(strPars, 1, j);
      strPars := copy(strPars, j + 1, length(strPars) - j)
    end
    Else
    begin
      strOlah := copy(strPars, 1, 3);
      strPars := copy(strPars,4,length(strPars)-3);
    End;
    //--membilang 3 digit angka yang ada
    strSpell := PartialSpell(strOlah);
    //--menambahkan satuan yang sesuai, misalnya : ribu, juta, milyar, trilyun
    If strSpell <> '' Then
      If i > 12 Then
        strSebutan := 'trilyun '
       else if i > 9 Then
         strSebutan := 'milyar '
        else if i > 6 Then
          strSebutan := 'juta '
         else if i > 3 Then
          begin
           strSebutan := 'ribu ';
           If strSpell = 'satu ' Then strSpell := 'se'
          end;
    strHasil := strHasil + strSpell + strSebutan;
    strSpell := '';
    strSebutan := '';
    dec(i,3);
  until i<1;
  if Length(StrHasil)>0 then StrHasil[1]:=UpCase(StrHasil[1]);
  Spell := strHasil;
end;

procedure TPembuatanKwitansiFm.SaveBtnClick(Sender: TObject);
var s,kode,namaPT,nominal,pemakaian,tujuan,tgl,jam,tempat,NoSO,StatusKwitansi,kodeSO,kodeKontrak,tglPemakaian,GrandTotalS,SisaS,NominalS,KodeS,JamMulaiS,PIC,titelpic,telppic,namapic,kendaraan:string;
var total,sisa,PPN,MarkUp,TotalBayar,TotalNominal,TotalSisaBayar,tersisa,NominalPassing,NominalUpdate,totalPemakaian,NominalPrint,GrandTotalPrint,NominalAsli,HargaSO:currency;
var counter,kelunasan,toilet,airsus,cariwaktuberangkat:integer;
var WaktuBerangkat:TDateTime;
begin
  cariwaktuberangkat:=1;
  PIC:='-';
  kendaraan:='';
  NamaTtd:='Dwi Kurniastuti';
  If MasterQMarkUpNominal.AsCurrency = NULL then
  begin
    MasterQ.Edit;
    MasterQMarkUpNominal.AsCurrency:=0;
  end;
  If MasterQMarkUp.AsCurrency=NULL then
  begin
    MasterQ.Edit;
    MasterQMarkUp.AsCurrency:=0;
  end;
  NominalAsli:=MasterQNominal.AsCurrency;
  if MasterQUntuk.AsString='PELUNASAN' then
  NominalPrint:=NominalAsli+MasterQMarkUpNominal.AsCurrency
  else
  NominalPrint:=NominalAsli+MasterQMarkUpNominal.AsCurrency;
  GrandTotalPrint:=MasterQGrandTotal.AsCurrency+MasterQMarkUp.AsCurrency-MasterQDiskon.AsCurrency;
  kelunasan:=0;
  total:=0;
  sisa:=0;
  tujuan:='';
  pemakaian:='';
  TotalNominal:=0;
  TotalSisaBayar:=0;
  NominalPassing:=0;
  StatusKwitansi:=MasterQStatusKwitansi.AsString;
  KodeQ.Open;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    if RbtSO.Checked=true then
    begin
      MasterQStatusKwitansi.AsString:='BELUM LUNAS';
    end;
    if RbtKontrak.Checked=true then
    begin
      MasterQStatusKwitansi.AsString:='LUNAS';
    end;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    if RbtSO.Checked=true then
      begin
         MasterQKeperluan.AsString:='SO';
      end
      else
      begin
          MasterQKeperluan.AsString:='KONTRAK';
      end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  if RbtSO.Checked=True then
  begin
    DetailSOQ.Open;
    DetailSOQ.Edit;
    DetailSOQ.First;
    while DetailSOQ.Eof=FALSE do
    begin
      NominalUpdate:=NominalUpdate+DetailSOQNominal.AsCurrency;
      DetailSOQ.Next;
    end;
  end
  else if RbtKontrak.Checked=True then
  begin
    DetailKontrakQ.Open;
    DetailKontrakQ.Edit;
    DetailKontrakQ.First;
    while DetailKontrakQ.Eof=FALSE do
    begin
      NominalUpdate:=NominalUpdate+DetailKontrakQNominal.AsCurrency;
      DetailKontrakQ.Next;
    end;
  end;

  MasterQ.Edit;

  if RbtSO.Checked=True then
  begin
    DetailSOQ.Open;
    DetailSOQ.Edit;
    DetailSOQ.First;
    while DetailSOQ.Eof=FALSE do
    begin
      NoSO:=DetailSOQKodeSO.AsString;
      DetailSOQ.Edit;
      DetailSOQKodeKwitansi.AsString:=KodeEdit.Text;
      total:=DetailSOQJumlahBayar.AsCurrency+total;
      NominalPassing:=NominalPassing+DetailSOQNominal.AsCurrency;
      DetailSOQ2.Close;
      DetailSOQ2.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
      DetailSOQ2.Open;
      if cariwaktuberangkat=1 then
      begin
        kodeSO:=DetailSOQ2Kodenota.AsString;
        WaktuBerangkat:=DetailSOQ2Berangkat.AsDateTime;
        cariwaktuberangkat:=0;
      end
      else
      begin
        if DetailSOQ2Berangkat.AsDateTime<WaktuBerangkat then
        begin
          WaktuBerangkat:=DetailSOQ2Berangkat.AsDateTime;
          kodeSO:=DetailSOQ2Kodenota.AsString;
        end;
      end;
      CekPICQ.Close;
      CekPICQ.ParamByName('text').AsString:=kodeSO;
      CekPICQ.Open;
      if CekPICQPICJemput.AsString <> NULL then
      begin
        titelpic:=CekPICQTitlePICJemput.AsString;
        if titelpic = NULL then
        begin
          titelpic:='';
        end;
        telppic:=CekPICQNoTelpPICJemput.AsString;
        if telppic = NULL then
        begin
          telppic:='';
        end;
        namapic:=CekPICQPICJemput.AsString;
        if namapic = NULL then
        begin
          namapic:='-';
        end;
        PIC:=titelpic+' '+namapic+' / '+telppic;
      end;
      TotalSisaBayar:=TotalSisaBayar+DetailSOQSisaBayar.AsCurrency;
      DetailSOQ.Post;
      DetailSOQ.Next;
    end;
    //total:=total+MasterQMarkUp.AsCurrency;
    NominalPassing:=NominalPassing+MasterQMarkUpNominal.AsCurrency;
  end;

  if RbtKontrak.Checked=True then
  begin
    DetailKontrakQ.Open;
    DetailKontrakQ.Edit;
    DetailKontrakQ.First;
    while DetailKontrakQ.Eof=FALSE do
    begin
      DetailKontrakQ.Edit;
      DetailKontrakQKodeKwitansi.AsString:=KodeEdit.Text;
      total:=DetailKontrakQHarga.AsCurrency+total;
      NominalPassing:=NominalPassing+DetailKontrakQNominal.AsCurrency;
      kodeKontrak:=DetailKontrakQKodeKontrak.AsString;
      DetailKontrakQ.Post;
      DetailKontrakQ.Next;
    end;
  end;
  kode:=KodeEdit.Text;
  namaPt:=MasterQNamaPelanggan.AsString;
  nominal:=currtostr(MasterQNominal.AsCurrency);
  menuutamafm.DataBase1.StartTransaction;

  try
    masterq.ApplyUpdates;
    if RbtKontrak.Checked=true then DetailKontrakQ.ApplyUpdates else DetailSOQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    if RbtKontrak.Checked=true then DetailKontrakQ.CommitUpdates else DetailSOQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      if RbtKontrak.Checked=true then DetailKontrakQ.RollbackUpdates else DetailSOQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;

  if cetak=1 then
  begin
    if RbtSO.Checked=True then
    begin
      if MessageDlg('Cetak Kuitansi '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
      begin
        //isi JumlahSeat
        ArmadaSOQ.Close;
        ArmadaSOQ.ParamByName('text').AsString:=kode;
        ArmadaSOQ.Open;
        ArmadaSOQ.Edit;
        ArmadaSOQ.First;
        while ArmadaSOQ.Eof=false do
        begin
          CekHargaSOQ.Close;
          CekHargaSOQ.ParamByName('text').AsString:=KodeEdit.Text;
          CekHargaSOQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
          CekHargaSOQ.Open;
          CekHargaSOQ.Edit;
          CekHargaSOQ.First;
          while CekHargaSOQ.Eof=false do
          begin
          JumlahArmadaSOQ.Close;
          JumlahArmadaSOQ.ParamByName('text').AsString:=kode;
          JumlahArmadaSOQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
          JumlahArmadaSOQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
          JumlahArmadaSOQ.Open;
          if ArmadaSOQKapasitasSeat.AsInteger=9 then
          begin
             kendaraan:=' Inova ';
          end
          else if ArmadaSOQKapasitasSeat.AsInteger=11 then
          begin
             kendaraan:=' Elf ';
          end
          else if ArmadaSOQKapasitasSeat.AsInteger=14 then
          begin
             kendaraan:=' Elf ';
          end
          else
          begin
             kendaraan:=' bus ';
          end;
            pemakaian:=pemakaian+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+kendaraan+inttostr(ArmadaSOQKapasitasSeat.AsInteger)+' seat';
            CekToiletQ.Close;
            CekToiletQ.ParamByName('text').AsString:=kode;
            CekToiletQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
            CekToiletQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
            CekToiletQ.Open;
            toilet:=CekToiletQJumT.AsInteger;
            if toilet>0 then
            begin
              pemakaian:=pemakaian+' T';
            end;
            CekAirSusQ.Close;
            CekAirSusQ.ParamByName('text').AsString:=kode;
            CekAirSusQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
            CekAirSusQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
            CekAirSusQ.Open;
            airsus:=CekAirSusQJumAS.AsInteger;
            if airsus>0 then
            begin
              if toilet>0 then
              begin
                pemakaian:=pemakaian+'S';
              end
              else
              begin
                pemakaian:=pemakaian+' S';
              end;
            end;
          {CekHargaArmadaQ.Close;
          CekHargaArmadaQ.ParamByName('text').AsString:=kode;
          CekHargaArmadaQ.ParamByName('text2').AsString:=ArmadaSOQKapasitasSeat.AsString;
          CekHargaArmadaQ.Open;}
          HargaSO:=CekHargaSOQHarga.AsCurrency;
          if MasterQMarkUp.AsCurrency>0 then
          begin
              HargaSO:=HargaSO+(HargaSO*10/100);
          end;
          totalPemakaian:=HargaSO*JumlahArmadaSOQJumSeat.AsInteger;
          pemakaian:=pemakaian+Chr(13)+Chr(10)+'@'+FloatToStrF(HargaSO,ffCurrency,10,0)+'x '+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+' = '+FloatToStrF(totalPemakaian,ffCurrency,10,0);
          pemakaian:=pemakaian+'.'+Chr(13)+Chr(10);
          CekHargaSOQ.Next;
        end;

          ArmadaSOQ.Next;
        end;
        //akhir Isi jumlah seat

        //isi Terbilang
        s:=spell(currtostr(NominalPrint))+ ' rupiah.';
        //akhir isi Terbilang

        //isi Tanggal dan jam dan alamat jemput dan tujuan
        SOIsiQ.Close;
        SOIsiQ.ParamByName('text').AsString:=kodeSO;
        SOIsiQ.Open;
        jam:=SOIsiQTglBerangkat.AsString;
        //jam:=SOIsiQJamMulai.AsString+' - '+SOIsiQJamSelesai.AsString;
        tgl:=SOIsiQTglTiba.AsString+ ' - ' + SOIsiQJamSelesai.AsString;
        tempat:=SOIsiQAlamatJemput.AsString+chr(13)+chr(10);//+MasterQKotaPelanggan.AsString;
        //tujuan:=SOIsiQDari.AsString+' - '+SOIsiQTujuan.AsString;
        tujuan:=SOIsiQKeteranganRute.AsString;
        tglPemakaian:=jam+' S/D '+tgl;
        //selesai isi tanggal dan jam dan alamat jemput dan tujuan
        //isi Sisa Bayar
        DetailSOQ.Close;
        DetailSOQ.ParamByName('text').AsString:=kode;
        DetailSOQ.Open;
        DetailSOQ.Edit;
        DetailSOQ.Edit;
        DetailSOQ.First;
        TotalNominal:=0;
        while DetailSOQ.Eof=false do
        begin
          HitungNominalSOQ.Close;
          HitungNominalSOQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          HitungNominalSOQ.Open;
          TotalNominal:=TotalNominal+HitungNominalSOQTotalBayar.AsCurrency;
          DetailSOQ.Next;
        end;
        tersisa:=total-TotalNominal-MasterQDiskon.AsCurrency;
        //selesai isi sisa bayar

        GrandTotalS:=CurrToStr(MasterQGrandTotal.AsCurrency);
        SisaS:=CurrToStr(tersisa);
        NominalS:=CurrToStr(MasterQNominal.AsCurrency);
        Kodes:=KodeEdit.Text;
        JamMulaiS:=SOIsiQJamMulai.AsString;

        CekCetakanQ.Close;
        CekCetakanQ.ParamByName('text').AsString:=MasterQKode.AsString;
        CekCetakanQ.Open;
        if CekCetakanQKode.AsString='' then
        begin
          CetakanKuitansiQ.Close;
          CetakanKuitansiQ.Open;
          CetakanKuitansiQ.Edit;
          CetakanKuitansiQKode.AsString:=KodeS;
          CetakanKuitansiQTerbilang.AsString:=s;
          CetakanKuitansiQGrandTotal.AsString:=GrandTotalS;
          CetakanKuitansiQTujuan.AsString:=tujuan;
          CetakanKuitansiQJamMulai.AsString:=JamMulaiS;
          CetakanKuitansiQTempat.AsString:=tempat;
          CetakanKuitansiQPemakaian.AsString:=pemakaian;
          CetakanKuitansiQTersisa.AsString:=SisaS;
          CetakanKuitansiQNominal.AsString:=NominalS;
          CetakanKuitansiQTglPemakaian.AsString:=tglPemakaian;
          CetakanKuitansiQNamaTtd.AsString:=NamaTtd;
          CetakanKuitansiQPIC.AsString:=PIC;
          CetakanKuitansiQKomisiPelanggan.AsString:=currtostr(MasterQMarkUp.AsCurrency);
          CetakanKuitansiQKomisiPelangganNominal.AsString:=currtostr(MasterQMarkUpNominal.AsCurrency);
          CetakanKuitansiQDiskon.AsString:=currtostr(MasterQDiskon.AsCurrency);
          MenuUtamaFm.Database1.StartTransaction;
          CetakanKuitansiQ.ApplyUpdates;
          MenuUtamaFm.Database1.Commit;
          CetakanKuitansiQ.CommitUpdates;
        end;
        if MessageDlg('Tampilkan editor?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
        begin
          CetakanKuitansiFm:=TCetakanKuitansiFm.Create(self,KodeS);
          CetakanKuitansiFm.ShowModal;
        end
        else
        begin
          CekPrintQ.SQL.Text:='update Kwitansi set CekPrint=1 where Kode='+QuotedStr(MasterQKode.AsString);
          CekPrintQ.ExecSQL;
          Crpe1.Refresh;
          Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiSO.rpt';
          Crpe1.ParamFields[0].CurrentValue:=KodeEdit.Text;
          Crpe1.ParamFields[1].CurrentValue:=s;
          Crpe1.ParamFields[2].CurrentValue:=currtostr(GrandTotalPrint);
          Crpe1.ParamFields[3].CurrentValue:=tujuan;
          Crpe1.ParamFields[4].CurrentValue:=SOIsiQJamMulai.AsString;
          Crpe1.ParamFields[5].CurrentValue:=tempat;
          Crpe1.ParamFields[6].CurrentValue:=pemakaian;
          Crpe1.ParamFields[7].CurrentValue:=currtostr(tersisa);
          Crpe1.ParamFields[8].CurrentValue:=currtostr(NominalPrint);
          Crpe1.ParamFields[9].CurrentValue:=tglPemakaian;
          Crpe1.ParamFields[10].CurrentValue:=NamaTtd;
          Crpe1.ParamFields[11].CurrentValue:=PIC;
          Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
          Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
          Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
          Crpe1.Execute;
        end;
      end;
    end;

    if RbtKontrak.Checked=True then
    begin
      if MessageDlg('Cetak Kuitansi '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
      begin
        //isi Terbilang
        s:=spell(currtostr(NominalPassing))+ ' rupiah.';
        //selesai isi Terbilang
        //isi Tujuan, tanggal
        KontrakIsiQ.Close;
        KontrakIsiQ.ParamByName('text').AsString:=kodeKontrak;
        KontrakIsiQ.Open;
        tujuan:=KontrakIsiQDari.AsString+' - '+KontrakIsiQTujuan.AsString;
        tgl:=KontrakIsiQTglMulaiP.AsString+' - '+KontrakIsiQTglSelesaiP.AsString;
        //selesai isi Tujuan,tanggal

        //isi pemakaian
        DetailKontrakQ.Close;
        DetailKontrakQ.ParamByName('text').AsString:=kode;
        DetailKontrakQ.Open;
        DetailKontrakQ.Edit;
        DetailKontrakQ.First;
        pemakaian:='Sewa Kendaraan PO No. ';
        while DetailKontrakQ.Eof=false do
        begin
            POEksternalQ.Close;
            POEksternalQ.ParamByName('text').AsString:=DetailKontrakQKodeKontrak.AsString;
            POEksternalQ.Open;
            If (POEksternalQPOEksternal.AsString<>'') then
            begin
             pemakaian:=pemakaian+POEksternalQPOEksternal.AsString+'; ';
            end;
            DetailKontrakQ.Next;
        end;
        sisa:=0;
        //selesai isi pemakaian
        Crpe1.Refresh;
        Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiKontrak.rpt';
        Crpe1.ParamFields[0].CurrentValue:=kode;
        Crpe1.ParamFields[1].CurrentValue:=currtostr(NominalPassing);
        Crpe1.ParamFields[2].CurrentValue:=s;
        Crpe1.ParamFields[3].CurrentValue:=pemakaian;
        Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
        Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
        Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
        Crpe1.Execute;
      end;
    end;
  end;

  DetailSOQ.Close;
  DetailSOQ.ParamByName('text').AsString:='';
  DetailKontrakQ.Close;
  DetailKontrakQ.ParamByName('text').AsString:='';
  viewKwitansiQ.Refresh
end;

procedure TPembuatanKwitansiFm.cxGrid1DBTableView1KodeSOPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var q:string;
  var totalan:currency;
  var i:integer;
begin
q:='select so.*,cast(0 as varchar(2)) as tanda,(select case when exists (select dkso.* from DetailKwitansiSO dkso where dkso.KodeSO=so.Kodenota) then "Sudah" else "Belum" end) ';
q:=q+'as Kwitansi from MasterSO so where StatusPembayaran=upper("BELUM LUNAS") and Status=upper("DEAL") and Pelanggan=' +QuotedStr(MasterQPelanggan.AsString);
GetKodeGrupQ.Close;
GetKodeGrupQ.ParamByName('text').AsString:=MasterQPelanggan.AsString;
GetKodeGrupQ.Open;
GetKodeGrupQ.Edit;
GetKodeGrupQ.First;
while GetKodeGrupQ.Eof=false do
begin
    GetDetailGrupQ.Close;
    GetDetailGrupQ.ParamByName('text').AsString:=GetKodeGrupQKodeGrup.AsString;
    GetDetailGrupQ.ParamByName('text2').AsString:=MasterQPelanggan.AsString;
    GetDetailGrupQ.Open;
    GetDetailGrupQ.Edit;
    GetDetailGrupQ.First;
    while GetDetailGrupQ.Eof=false do
    begin
        q:=q+' or Pelanggan='+QuotedStr(GetDetailGrupQPelanggan.AsString);
        GetDetailGrupQ.Next;
    end;
    GetKodeGrupQ.Next;
end;
q:=q+' order by so.Berangkat Asc';
     SOKwitansiDropDownFm:=TSOKwitansiDropdownfm.Create(Self,q);
    if SOKwitansiDropDownFm.ShowModal=MrOK then
    begin
      DetailSOQ.Open;
      for i:=0 to SOKwitansiDropDownFm.BanyakData-1 do
      begin
       //showmessage(SOKwitansiDropDownFm.cxGrid1DBTableView1.Controller.SelectedRows [i].Values [SOKwitansiDropDownFm.cxGrid1DBTableView1kodenota.index]);
      //showmessage(inttostr(SOKwitansiDropDownFm.cxGrid1DBTableView1.Controller.SelectedRowCount));
      //showmessage (SOKwitansiDropDownFm.cxGrid1DBTableView1.DataController.Values[i,0]);
      DetailSOQ.insert;
      //DetailSOQKodeSO.AsString:=SOKwitansiDropDownFm.cxGrid1DBTableView1.Controller.SelectedRows [i].Values [SOKwitansiDropDownFm.cxGrid1DBTableView1kodenota.index];
      DetailSOQKodeSO.AsString:=SOKwitansiDropDownFm.ListBox1.Items.ValueFromIndex[i];
      DetailSOQKodeKwitansi.AsString:='-';
      DetailSOQ2.Close;
      DetailSOQ2.ParamByName('text').AsString:=SOKwitansiDropDownFm.ListBox1.Items.ValueFromIndex[i];
      DetailSOQ2.Open;
      if currtostr(DetailSOQ2Harga.AsCurrency)='' then
      begin
          DetailSOQ2Harga.AsCurrency:=0;
      end;
      if currtostr(DetailSOQ2PPN.AsCurrency)='' then
      begin
          DetailSOQ2PPN.AsCurrency:=0;
      end;
      if currtostr(DetailSOQ2BiayaExtend.AsCurrency)='' then
      begin
          DetailSOQ2BiayaExtend.AsCurrency:=0;
      end;
      if currtostr(DetailSOQ2PPNExtend.AsCurrency)='' then
      begin
          DetailSOQ2PPNExtend.AsCurrency:=0;
      end;
      if currtostr(DetailSOQ2KomisiPelanggan.AsCurrency)='' then
      begin
          DetailSOQ2KomisiPelanggan.AsCurrency:=0;
      end;
      //showmessage(currtostr(DetailSOQ2Harga.AsCurrency));
      //showmessage(SOKwitansiDropDownFm.ListBox1.Items.ValueFromIndex[i]);
      DetailSOQJumlahBayar.AsCurrency:=DetailSOQ2Harga.AsCurrency+DetailSOQ2BiayaExtend.AsCurrency+DetailSOQ2PPNExtend.AsCurrency;
      HitungNominalSOQ.Close;
      HitungNominalSOQ.ParamByName('text').AsString:=SOKwitansiDropDownFm.ListBox1.Items.ValueFromIndex[i];
      HitungNominalSOQ.Open;
      if currtostr(HitungNominalSOQTotalBayar.AsCurrency)='' then
      begin
         totalan:=0;
      end
      else
      begin
          totalan:=HitungNominalSOQTotalBayar.AsCurrency;
      end;
      DetailSOQSisaBayar.AsCurrency:=DetailSOQJumlahBayar.AsCurrency-totalan;
      if i+1<SOKwitansiDropDownFm.BanyakData then
      begin
          DetailSOQ.Next;
      end;
      end;

    end;
    SOKwitansiDropDownFm.Release;
end;

procedure TPembuatanKwitansiFm.RbtSOClick(Sender: TObject);
begin
if MasterQKeperluan.AsString<>'KONTRAK' then
begin
RbtKontrak.Checked:=false;
RbtSO.Checked:=true;
Panel5.Visible:=false;
cxGrid2.Visible:=false;
cxGrid1.Visible:=True;
end;

if MasterQKeperluan.AsString='SO' then
begin
   RbtSO.Checked:=true;
end
else if MasterQKeperluan.AsString='KONTRAK' then
begin
   RbtKontrak.Checked:=true;
end;

end;

procedure TPembuatanKwitansiFm.RbtKontrakClick(Sender: TObject);
begin
if MasterQKeperluan.AsString<>'SO' then
begin
RbtSO.Checked:=false;
RbtKontrak.Checked:=true;
panel5.Visible:=True;
cxGrid2.Visible:=True;
cxGrid1.Visible:=False;
end;
if MasterQKeperluan.AsString='SO' then
begin
   RbtSO.Checked:=true;
end
else if MasterQKeperluan.AsString='KONTRAK' then
begin
   RbtKontrak.Checked:=true;
end;

end;

procedure TPembuatanKwitansiFm.cxGrid2DBTableView1KodeKontrakPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var q:string;
begin

end;

procedure TPembuatanKwitansiFm.cxGridDBTableView1KodeKontrakPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var q:string;
begin
 q:='select * from Kontrak where Status=upper("ONGOING") and Pelanggan=' +QuotedStr(MasterQPelanggan.AsString);
     KontrakDropDownFm:=TKontrakDropdownfm.Create(Self,q);
    if KontrakDropDownFm.ShowModal=MrOK then
    begin
      DetailKontrakQ.Open;
      DetailKontrakQ.Edit;
      DetailKontrakQKodeKontrak.AsString:=KontrakDropDownFm.kode;
    end;
    KontrakDropDownFm.Release;
end;

procedure TPembuatanKwitansiFm.cxGridDBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var aDone:boolean);
  var nominalKontrak,totalKontrak,sisaBayarKontrak : currency;
begin
  if abuttonindex=10 then
  begin
  nominalKontrak:=0;
  totalKontrak:=0;
  sisaBayarKontrak:=0;
  DetailKontrakQ.Edit;
  DetailKontrakQKodeKwitansi.AsString:='0';
  DetailKontrakQ.Post;
  //hitung ulang
  DetailKontrakQ.Edit;
  DetailKontrakQ.First;
  while DetailKontrakQ.Eof=false do
  begin
       if DetailKontrakQJumlahUnit.AsInteger = NULL then
       begin
           DetailKontrakQ.Edit;
           DetailKontrakQJumlahUnit.AsInteger:=1;
       end;
       nominalKontrak:=nominalKontrak+DetailKontrakQNominal.AsCurrency;
       totalKontrak:=totalKontrak+(DetailKontrakQHarga.AsCurrency*DetailKontrakQJumlahUnit.AsInteger);
       MasterQKeteranganPembayaran.AsString:=MasterQKeteranganPembayaran.AsString;
       DetailKontrakQ.Next;
  end;
  MasterQ.Edit;
  MasterQNominal.AsCurrency:=nominalKontrak;
  MasterQGrandTotal.AsCurrency:=totalKontrak;
  MasterQSisaBayar.AsCurrency:=MasterQGrandTotal.AsCurrency-MasterQNominal.AsCurrency;

  //akhir hitung ulang
  end;
end;

procedure TPembuatanKwitansiFm.cxGrid3DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
  var DetailSO: integer;
  var total,sisa,sisaBayar,totalNominal: currency;
  var q:string;
begin
cxTextEdit2.Text:='';
cetak:=0;
Baru:=0;
total:=0;
sisaBayar:=0;
totalNominal:=0;
q:=HitungTotalSOKlikQ.SQL.Text;
     MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewKwitansiQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeletePembuatanKwitansi.AsBoolean;
      CetakBtn.Enabled:=menuutamafm.UserQCetakKwitansi.AsBoolean;
      ApproveBtn.Enabled:=menuutamafm.UserQApproveKwitansi.AsBoolean;
      btnLunas.Enabled:=menuutamafm.UserQLunasKwitansi.AsBoolean;
    end;
    NominalAwal:=MasterQNominal.AsCurrency;
    PPNAwal:=MasterQPPN.AsCurrency;
    MarkupAwal:=MasterQMarkUp.AsCurrency;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePembuatanKwitansi.AsBoolean;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
    MasterVGrid.Enabled:=True;
    if MasterQKeperluan.AsString='SO' then
    begin
      Panel5.Visible:=false;
      cxGrid2.Visible:=false;
      cxGrid1.Visible:=True;
      RbtSO.Checked:=true;
      RbtKontrak.Checked:=false;
      DetailSOQ.Close;
      DetailSOQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DetailSOQ.Open;
      DetailSOQ.Edit;
      DetailSOQ.First;
      while DetailSOQ.Eof=false do
      begin
           if currtostr(DetailSOQHarga.AsCurrency)='' then
          begin
              DetailSOQHarga.AsCurrency:=0;
          end;
          if currtostr(DetailSOQBiayaExtend.AsCurrency)='' then
          begin
              DetailSOQBiayaExtend.AsCurrency:=0;
          end;
          if currtostr(DetailSOQPPNExtend.AsCurrency)='' then
          begin
              DetailSOQPPNExtend.AsCurrency:=0;
          end;
          if currtostr(DetailSOQPPN.AsCurrency)='' then
          begin
              DetailSOQPPN.AsCurrency:=0;
          end;
          if currtostr(DetailSOQKomisiPelanggan.AsCurrency)='' then
          begin
              DetailSOQKomisiPelanggan.AsCurrency:=0;
          end;
          DetailSOQ.Edit;
          DetailSOQJumlahBayar.AsCurrency:=DetailSOQHarga.AsCurrency+DetailSOQBiayaExtend.AsCurrency+DetailSOQPPNExtend.AsCurrency;
          HitungNominalSOQ.Close;
          HitungNominalSOQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          HitungNominalSOQ.Open;
          DetailSOQSisaBayar.AsCurrency:=DetailSOQJumlahBayar.AsCurrency-HitungNominalSOQTotalBayar.AsCurrency;
          total:=total+DetailSOQJumlahBayar.AsCurrency;
          sisaBayar:=sisaBayar+DetailSOQSisaBayar.AsCurrency;
          DetailSOQ.Next;
      end;
      MasterQ.Edit;
      MasterQGrandTotal.AsCurrency:=total;
      MasterQSisaBayar.AsCurrency:=sisaBayar;
      //MasterQSisaBayar.AsCurrency:=sisaBayar+MasterQMarkUp.AsCurrency;
     // MasterQNominal.AsCurrency:=MasterQNominal.AsCurrency+masterq

    end

    else
    begin
    RbtKontrak.Checked:=true;
    RbtSO.Checked:=false;
      panel5.Visible:=True;
      cxGrid2.Visible:=True;
      cxGrid1.Visible:=False;
      DetailKontrakQ.Close;
      DetailKontrakQ.ParamByName('text').AsString:=MasterQKode.AsString;
      DetailKontrakQ.Open;
      DetailKontrakQ.Edit;
      DetailKontrakQ.First;
      while DetailKontrakQ.Eof=false do
      begin
          total:=DetailKontrakQHarga.AsCurrency+total;
          totalNominal:=totalNominal+DetailKontrakQNominal.AsCurrency;
          DetailKontrakQ.Next;
      end;
      MasterQGrandTotal.AsCurrency:=total;
      MasterQSisaBayar.AsCurrency:=total-totalNominal;
    end;

    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TPembuatanKwitansiFm.DeleteBtnClick(Sender: TObject);
begin
 if MessageDlg('Hapus Kwitansi '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
     if RbtSO.Checked=true then
     begin
     SDQuery1.Close;
     SDQuery1.ParamByName('text').AsString:=MasterQKode.AsString;
     SDQuery1.Open;
     SDQuery1.Edit;
     SDQuery1.First;
     while SDQuery1.Eof = FALSE
     do
     begin
       HapusSOQ.SQL.Text:=('delete from DetailKwitansiSO where kodeKwitansi='+QuotedStr(KodeEdit.Text)+' and KodeSO='+QuotedStr(SDQuery1KodeSO.AsString));
       HapusSOQ.ExecSQL;
       SDQuery1.Next;
     end;
     end;
     //Hapus Detail Kontrak
      if RbtKontrak.Checked=true then
     begin
     SDQuery3.Close;
     SDQuery3.ParamByName('text').AsString:=MasterQKode.AsString;
     SDQuery3.Open;
     SDQuery3.Edit;
     SDQuery3.First;
     while SDQuery3.Eof = FALSE
     do
     begin
       HapusKontrakQ.SQL.Text:=('delete from DetailKwitansiKontrak where kodeKwitansi='+QuotedStr(KodeEdit.Text)+' and KodeKontrak='+QuotedStr(SDQuery3KodeKontrak.AsString));
       HapusKontrakQ.ExecSQL;
       SDQuery3.Next;
     end;
     end;
     HapusCetakanKuitansiQ.Close;
     HapusCetakanKuitansiQ.SQL.Text:='delete from CetakanKuitansi where Kode='+QuotedStr(MasterQKode.AsString);
     HapusCetakanKuitansiQ.ExecSQL;
     //DeleteDBKBQ.Delete;
     //DeleteDBKBQ.ApplyUpdates;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Kwitansi telah dihapus.',mtInformation,[mbOK],0);
       SDQuery1.Close;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Kwitansi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     viewKwitansiQ.Close;
  viewKwitansiQ.Open;
  HapusSOQ.Close;
    DetailSOQ.Close;
  DetailSOQ.ParamByName('text').AsString:='';
  DetailSOQ.Open;
  DetailSOQ.Close;

  DetailKontrakQ.Close;
  DetailKontrakQ.ParamByName('text').AsString:='';
  DetailKontrakQ.Open;
  DetailKontrakQ.Close;
  KodeEditPropertiesButtonClick(sender,0);
  end;
end;

procedure TPembuatanKwitansiFm.MasterVGridPPNEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPPN.AsCurrency:=DisplayValue;
//MasterQGrandTotal.AsCurrency:=MasterQGrandTotal.AsCurrency+MasterQPPN.AsCurrency-PPNAwal;
MasterQSisaBayar.AsCurrency:=MasterQSisaBayar.AsCurrency+MasterQPPN.AsCurrency-PPNAwal;
PPNAwal:=MasterQPPN.AsCurrency;
//SelisihPPN:=MasterQPPN.AsCurrency-PPNAwal;

end;

procedure TPembuatanKwitansiFm.MasterVGridMarkUpEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQMarkUp.AsCurrency:=DisplayValue;
  //MasterQGrandTotal.AsCurrency:=MasterQGrandTotal.AsCurrency+MasterQMarkUp.AsCurrency-MarkupAwal;
  //Masterqsisabayar.AsCurrency:=MasterQGrandTotal.AsCurrency-MasterQNominal.AsCurrency;
  //MasterQSisaBayar.AsCurrency:=MasterQSisaBayar.AsCurrency+MasterQMarkUp.AsCurrency-MarkupAwal;
  MarkupAwal:=MasterQMarkUp.AsCurrency
//SelisihMarkup:=MasterQMarkUp.AsCurrency-MarkupAwal;
end;

procedure TPembuatanKwitansiFm.MasterVGridNominalEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQNominal.AsCurrency:=DisplayValue;
MasterQSisaBayar.AsCurrency:=MasterQSisaBayar.AsCurrency-MasterQNominal.AsCurrency+NominalAwal;
NominalAwal:=MasterQNominal.AsCurrency;
end;

procedure TPembuatanKwitansiFm.cxGrid1DBTableView1NominalPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
  var nominalKontrak,totalKontrak,sisaBayarKontrak: currency;
begin
  DetailSOQNominal.AsCurrency:=DisplayValue;

end;

procedure TPembuatanKwitansiFm.MarkUpNominalEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQMarkUpNominal.AsCurrency:=DisplayValue;
//MasterQNominal.AsCurrency:=MasterQNominal.AsCurrency+MasterQMarkUpNominal.AsCurrency-MarkupNominalAwal;
end;

procedure TPembuatanKwitansiFm.CetakBtnClick(Sender: TObject);
var s,pemakaian,jam,tgl,tempat,tujuan,tglPemakaian,PIC,titelpic,telppic,kodeso,namapic,kendaraan,sisaS:string;
var TotalNominal, tersisa,sisa,totalPemakaian,NominalPassing,KomisiPelangganNominal,NominalGrandTotal,NominalAsli,HargaSO:currency;
var toilet,airsus,cariwaktuberangkat:integer;
var WaktuBerangkat:TDateTime;
begin
 //NamaTtd:='Vincentius Harijanto Lukito';
 cariwaktuberangkat:=1;
 PIC:='-';
 NamaTtd:='Dwi Kurniastuti';
If MasterQMarkUpNominal.AsCurrency = NULL then
begin
   MasterQ.Edit;
   MasterQMarkUpNominal.AsCurrency:=0;
end;
If MasterQMarkUp.AsCurrency=NULL then
begin
    MasterQ.Edit;
    MasterQMarkUp.AsCurrency:=0;
end;
NominalAsli:=MasterQNominal.AsCurrency;
{if MasterQPPh.AsBoolean = True then
begin
    NominalAsli:=NominalAsli*100/98;
end; }
NominalPassing:=NominalAsli+MasterQMarkUpNominal.AsCurrency;
NominalGrandTotal:=MasterQGrandTotal.AsCurrency+MasterQMarkUp.AsCurrency-MasterQDiskon.AsCurrency;
s:=spell(currtostr(NominalPassing))+' rupiah';
if RbtSO.Checked=true then
begin
  //isi JumlahSeat
    ArmadaSOQ.Close;
    ArmadaSOQ.ParamByName('text').AsString:=KodeEdit.Text;
    ArmadaSOQ.Open;
    ArmadaSOQ.Edit;
    ArmadaSOQ.First;
    while ArmadaSOQ.Eof=false do
    begin
        CekHargaSOQ.Close;
        CekHargaSOQ.ParamByName('text').AsString:=KodeEdit.Text;
        CekHargaSOQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
        CekHargaSOQ.Open;
        CekHargaSOQ.Edit;
        CekHargaSOQ.First;
        while CekHargaSOQ.Eof=false do
        begin
          JumlahArmadaSOQ.Close;
          JumlahArmadaSOQ.ParamByName('text').AsString:=KodeEdit.Text;
          JumlahArmadaSOQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
          JumlahArmadaSOQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
          JumlahArmadaSOQ.Open;
          if ArmadaSOQKapasitasSeat.AsInteger=9 then
          begin
             kendaraan:=' Inova ';
          end
          else if ArmadaSOQKapasitasSeat.AsInteger=11 then
          begin
             kendaraan:=' Elf ';
          end
          else if ArmadaSOQKapasitasSeat.AsInteger=14 then
          begin
             kendaraan:=' Elf ';
          end
          else
          begin
             kendaraan:=' bus ';
          end;
          pemakaian:=pemakaian+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+kendaraan+inttostr(ArmadaSOQKapasitasSeat.AsInteger)+' seat';
          CekToiletQ.Close;
          CekToiletQ.ParamByName('text').AsString:=KodeEdit.Text;
          CekToiletQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
          CekToiletQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
          CekToiletQ.Open;
          toilet:=CekToiletQJumT.AsInteger;
          if toilet>0 then
          begin
            pemakaian:=pemakaian+' T';
          end;
          CekAirSusQ.Close;
          CekAirSusQ.ParamByName('text').AsString:=KodeEdit.Text;
          CekAirSusQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
          CekAirSusQ.ParamByName('text3').AsCurrency:=CekHargaSOQHarga.AsCurrency;
          CekAirSusQ.Open;
          airsus:=CekAirSusQJumAS.AsInteger;
          if airsus>0 then
          begin
            if toilet>0 then
          begin
            pemakaian:=pemakaian+'S';
          end
          else
          begin
            pemakaian:=pemakaian+' S';
          end;
        end;
        {CekHargaArmadaQ.Close;
        CekHargaArmadaQ.ParamByName('text').AsString:=KodeEdit.Text;
        CekHargaArmadaQ.ParamByName('text2').AsString:=ArmadaSOQKapasitasSeat.AsString;
        CekHargaArmadaQ.Open; }
        HargaSO:=CekHargaSOQHarga.AsCurrency;
        if MasterQMarkUp.AsCurrency>0 then
        begin
            HargaSO:=HargaSO+(HargaSO*10/100);
        end;
        totalPemakaian:=HargaSO*JumlahArmadaSOQJumSeat.AsInteger;
        pemakaian:=pemakaian+Chr(13)+Chr(10)+'@'+FloatToStrF(HargaSO,ffCurrency,10,0)+'x '+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+' = '+FloatToStrF(totalPemakaian,ffCurrency,10,0);
        pemakaian:=pemakaian+'.'+Chr(13)+Chr(10);
        CekHargaSOQ.Next;
    end;
        ArmadaSOQ.Next;
    end;
    //akhir Isi jumlah seat


        //isi Sisa Bayar
     DetailSOQ.Close;
     DetailSOQ.ParamByName('text').AsString:=KodeEdit.Text;
     DetailSOQ.Open;
     DetailSOQ.Edit;
     DetailSOQ.Edit;
     DetailSOQ.First;
     TotalNominal:=0;
     while DetailSOQ.Eof=false do
     begin
     {if PIC='-' then
        begin
          CekPICQ.Close;
          CekPICQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          CekPICQ.Open;
          if CekPICQPICJemput.AsString <> NULL then
          begin
              titelpic:=CekPICQTitlePICJemput.AsString;
              if titelpic = NULL then
              begin
                  titelpic:='';
              end;
              telppic:=CekPICQNoTelpPICJemput.AsString;
              if telppic = NULL then
              begin
                  telppic:='';
              end;
              PIC:=titelpic+' '+CekPICQPICJemput.AsString+' / '+telppic;
              //showmessage(PIC);
              //showmessage(titelpic);
              //ShowMessage(telppic);
              //ShowMessage(CekPICQPICJemput.AsString);
          end;
        end; }

          HitungNominalSOQ.Close;
          HitungNominalSOQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          HitungNominalSOQ.Open;
          TotalNominal:=TotalNominal+HitungNominalSOQTotalBayar.AsCurrency;

          if cariwaktuberangkat=1 then
          begin
              kodeSO:=DetailSOQKodeSO.AsString;
              WaktuBerangkat:=DetailSOQBerangkat.AsDateTime;
              cariwaktuberangkat:=0;
          end

          else
          begin
              if DetailSOQBerangkat.AsDateTime<WaktuBerangkat then
              begin
                  WaktuBerangkat:=DetailSOQBerangkat.AsDateTime;
                  kodeSO:=DetailSOQKodeSO.AsString;
              end;
          end;
          DetailSOQ.Next;
     end;
     //showmessage(kodeso);
          CekPICQ.Close;
          CekPICQ.ParamByName('text').AsString:=kodeSO;
          CekPICQ.Open;
          if CekPICQPICJemput.AsString <> NULL then
          begin
              titelpic:=CekPICQTitlePICJemput.AsString;
              if titelpic = NULL then
              begin
                  titelpic:='';
              end;
              telppic:=CekPICQNoTelpPICJemput.AsString;
              if telppic = NULL then
              begin
                  telppic:='';
              end;
              namapic:=CekPICQPICJemput.AsString;
              if namapic = NULL then
              begin
                  namapic:='-';
              end;
              PIC:=titelpic+' '+namapic+' / '+telppic;
          end;
     tersisa:=MasterQGrandTotal.AsCurrency-TotalNominal-MasterQDiskon.AsCurrency;
     sisaS:=currtostr(tersisa);
     //selesai isi sisa bayar
     //showmessage(kodeso);
      //isi Tanggal dan jam dan alamat jemput dan tujuan
    SOIsiQ.Close;
    SOIsiQ.ParamByName('text').AsString:=kodeso;
    SOIsiQ.Open;
    jam:=SOIsiQTglBerangkat.AsString;
    //jam:=SOIsiQJamMulai.AsString+' - '+SOIsiQJamSelesai.AsString;
    tgl:=SOIsiQTglTiba.AsString+ ' - ' + SOIsiQJamSelesai.AsString;
    tempat:=SOIsiQAlamatJemput.AsString+chr(13)+chr(10)+MasterQKotaPelanggan.AsString;
    //tujuan:=SOIsiQDari.AsString+' - '+SOIsiQTujuan.AsString;
    tujuan:=SOIsiQKeteranganRute.AsString;
    tglPemakaian:=jam+' S/D '+tgl;
    //selesai isi tanggal dan jam dan alamat jemput dan tujuan

    CetakanKuitansiQ.Close;
    cetakankuitansiq.ParamByName('text').AsString:=kodeedit.text;
    CetakanKuitansiQ.Open;
    if cetakankuitansiq.RecordCount=0 then
    begin
        cetakankuitansiq.append;
        CetakanKuitansiQ.Edit;
        CetakanKuitansiQKode.AsString:=KodeEdit.Text;
        CetakanKuitansiQTerbilang.AsString:=s;
        CetakanKuitansiQGrandTotal.AsString:=CurrToStr(MasterQGrandTotal.AsCurrency);
        CetakanKuitansiQTujuan.AsString:=tujuan;
        CetakanKuitansiQJamMulai.AsString:=SOIsiQJamMulai.AsString;
        CetakanKuitansiQTempat.AsString:=tempat;
        CetakanKuitansiQPemakaian.AsString:=pemakaian;

        CetakanKuitansiQTersisa.AsString:=sisaS;
        CetakanKuitansiQNominal.AsString:=CurrToStr(MasterQNominal.AsCurrency);
        CetakanKuitansiQTglPemakaian.AsString:=tglPemakaian;
        CetakanKuitansiQNamaTtd.AsString:=NamaTtd;
        CetakanKuitansiQPIC.AsString:=PIC;
        CetakanKuitansiQKomisiPelanggan.AsString:=currtostr(MasterQMarkUp.AsCurrency);
        CetakanKuitansiQKomisiPelangganNominal.AsString:=currtostr(MasterQMarkUpNominal.AsCurrency);
        CetakanKuitansiQDiskon.AsString:=currtostr(MasterQDiskon.AsCurrency);
        cetakankuitansiq.Post;
        MenuUtamaFm.Database1.StartTransaction;
        CetakanKuitansiQ.ApplyUpdates;
        MenuUtamaFm.Database1.Commit;
        CetakanKuitansiQ.CommitUpdates;
        
    end;
    if MessageDlg('Tampilkan editor?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    CetakanKuitansiFm:=TCetakanKuitansiFm.Create(self,KodeEdit.Text);
    CetakanKuitansiFm.ShowModal;
    end

    else
    begin
      CekPrintQ.SQL.Text:='update Kwitansi set CekPrint=1 where Kode='+QuotedStr(MasterQKode.AsString);
      CekPrintQ.ExecSQL;
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiSO.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.Text;
      Crpe1.ParamFields[1].CurrentValue:=s;
      Crpe1.ParamFields[2].CurrentValue:=currtostr(NominalGrandTotal);
      Crpe1.ParamFields[3].CurrentValue:=tujuan;
      Crpe1.ParamFields[4].CurrentValue:=SOIsiQJamMulai.AsString;
      //Crpe1.ParamFields[5].CurrentValue:=jam;
      Crpe1.ParamFields[5].CurrentValue:=tempat;
      Crpe1.ParamFields[6].CurrentValue:=pemakaian;
      Crpe1.ParamFields[7].CurrentValue:=currtostr(tersisa);
      Crpe1.ParamFields[8].CurrentValue:=currtostr(NominalPassing);
      Crpe1.ParamFields[9].CurrentValue:=tglPemakaian;
      Crpe1.ParamFields[10].CurrentValue:=NamaTtd;
      Crpe1.ParamFields[11].CurrentValue:=PIC;
      Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
      Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
      Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      end;
end

else if RbtKontrak.Checked=true then
begin

   //isi Tujuan, tanggal
    KontrakIsiQ.Close;
    KontrakIsiQ.ParamByName('text').AsString:=DetailKontrakQKodeKontrak.AsString;
    KontrakIsiQ.Open;
    tujuan:=KontrakIsiQDari.AsString+' - '+KontrakIsiQTujuan.AsString;
    tgl:=KontrakIsiQTglMulaiP.AsString+' - '+KontrakIsiQTglSelesaiP.AsString;
    //selesai isi Tujuan,tanggal

    //isi pemakaian
    DetailKontrakQ.Close;
    DetailKontrakQ.ParamByName('text').AsString:=KodeEdit.Text;
    DetailKontrakQ.Open;
    DetailKontrakQ.Edit;
    DetailKontrakQ.First;
    pemakaian:='Sewa Kendaraan PO No. ';
    while DetailKontrakQ.Eof=false do
    begin
        POEksternalQ.Close;
        POEksternalQ.ParamByName('text').AsString:=DetailKontrakQKodeKontrak.AsString;
        POEksternalQ.Open;
       // showmessage(POEksternalQPOEksternal.AsString);
        If (POEksternalQPOEksternal.AsString<>'') then
        begin
         pemakaian:=pemakaian+POEksternalQPOEksternal.AsString+'; ';
        end;
        DetailKontrakQ.Next;
    end;
    {ArmadaKontrakQ.Close;
    ArmadaKontrakQ.ParamByName('text').AsString:=KodeEdit.Text;
    ArmadaKontrakQ.Open;
    ArmadaKontrakQ.Edit;
    ArmadaKontrakQ.First;
    while ArmadaKontrakQ.Eof=false do
    begin
        JumlahArmadaKontrakQ.Close;
        JumlahArmadaKontrakQ.ParamByName('text').AsString:=KodeEdit.Text;
        JumlahArmadaKontrakQ.ParamByName('text2').AsString:=inttostr(ArmadaKontrakQKapasitasSeat.AsInteger);
        JumlahArmadaKontrakQ.Open;
        pemakaian:=pemakaian+inttostr(JumlahArmadaKontrakQJumSeat.AsInteger)+' bus '+inttostr(ArmadaKontrakQKapasitasSeat.AsInteger)+' seat. ';
        ArmadaKontrakQ.Next;
    end;   }
    sisa:=0;
    //selesai isi pemakaian

      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiKontrak.rpt';
      //Crpe1.WindowButtonBar.PrintBtn:=True;
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.Text;
      Crpe1.ParamFields[1].CurrentValue:=currtostr(MasterQNominal.AsCurrency);
      Crpe1.ParamFields[2].CurrentValue:=s;
      Crpe1.ParamFields[3].CurrentValue:=pemakaian;
     { Crpe1.ParamFields[1].CurrentValue:=s;
      Crpe1.ParamFields[2].CurrentValue:=currtostr(MasterQGrandTotal.AsCurrency);
      Crpe1.ParamFields[3].CurrentValue:=tujuan;
      Crpe1.ParamFields[4].CurrentValue:=tgl;
      Crpe1.ParamFields[5].CurrentValue:=pemakaian;
      Crpe1.ParamFields[6].CurrentValue:=currtostr(MasterQNominal.AsCurrency); }
      Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
      Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
      Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
      Crpe1.Execute;
  end;
  viewKwitansiQ.Close;
  viewKwitansiQ.Open;
end;

procedure TPembuatanKwitansiFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
if cxTextEdit1.Text='' then
begin
  viewKwitansiQ.Close;
  viewKwitansiQ.SQL.Text:=ViewKwitansiOriSQL;
  viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKwitansiQ.ParamByName('text3').AsInteger:=cxSpinEdit1.Value;
  viewKwitansiQ.ParamByName('text4').AsInteger:=cxSpinEdit2.Value;
  viewKwitansiQ.Open;
end

else
begin
    viewKwitansiQ.Close;
    viewKwitansiQ.SQL.Clear;
    viewKwitansiQ.SQL.Text:=SearchNamaQ.SQL.Text;
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' where namaPT like' + QuotedStr('%'+cxTextEdit1.Text+'%');
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' and ((k.Tanggal>=:text1 and k.Tanggal<=:text2) or k.Tanggal is null) order by tglentry desc';
    viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
    viewKwitansiQ.Open;
end;
end;

procedure TPembuatanKwitansiFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
if cxTextEdit1.Text='' then
begin
  viewKwitansiQ.Close;
  viewKwitansiQ.SQL.Text:=ViewKwitansiOriSQL;
  viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKwitansiQ.ParamByName('text3').AsInteger:=cxSpinEdit1.Value;
  viewKwitansiQ.ParamByName('text4').AsInteger:=cxSpinEdit2.Value;
  viewKwitansiQ.Open;
end

else
begin
    viewKwitansiQ.Close;
    viewKwitansiQ.SQL.Clear;
    viewKwitansiQ.SQL.Text:=SearchNamaQ.SQL.Text;
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' where namaPT like' + QuotedStr('%'+cxTextEdit1.Text+'%');
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' and ((k.Tanggal>=:text1 and k.Tanggal<=:text2) or k.Tanggal is null) order by tglentry desc';
    viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
    viewKwitansiQ.Open;
end;
end;

procedure TPembuatanKwitansiFm.cxTextEdit1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var q,r,s,t,tahun1,tahun2,bulan1,bulan2,tgl1,tgl2,waktu1,waktu2:string;
begin
if cxTextEdit1.Text='' then
begin
    viewKwitansiQ.Close;
    viewKwitansiQ.SQL.Text:=ViewKwitansiOriSQL;
    //viewKwitansiQ.Close;
    viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
    viewKwitansiQ.ParamByName('text3').asstring:='0';
    viewKwitansiQ.ParamByName('text4').asstring :='9999999999';
    viewKwitansiQ.Open;

end

else
begin

    q:='select cast(k.Tanggal as date) as Tgl, k.*,p.namaPT,(select top 1 so.Berangkat';
    r:=' from Kwitansi k2 left join Pelanggan p on k2.Pelanggan=p.kode left join DetailKwitansiSO dkso on dkso.KodeKwitansi=k2.kode';
    s:=' left join MasterSO so on dkso.KodeSO=so.Kodenota where k2.Kode=k.Kode) as Berangkat from Kwitansi k';
    t:=' left join Pelanggan p on k.Pelanggan=p.kode where (k.Tanggal>=cxDateEdit1.Date and k.Tanggal<=cxDateEdit2.Date) or k.Tanggal is null';
    tahun1:=FormatDateTime('YYYY',cxDateEdit1.Date);
    bulan1:=FormatDateTime('MM',cxDateEdit1.Date);
    tgl1:=FormatDateTime('DD',cxDateEdit1.Date);
    waktu1:=tahun1+'-'+bulan1+'-'+tgl1;
    tahun2:=FormatDateTime('YYYY',cxDateEdit2.Date);
    bulan2:=FormatDateTime('MM',cxDateEdit2.Date);
    tgl2:=FormatDateTime('DD',cxDateEdit2.Date);
    waktu2:=tahun2+'-'+bulan2+'-'+tgl2;
   viewKwitansiQ.Close;
    viewKwitansiQ.SQL.Clear;
    viewKwitansiQ.SQL.Text:=SearchNamaQ.SQL.Text;
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' where namaPT like' + QuotedStr('%'+cxTextEdit1.Text+'%');
    viewKwitansiQ.SQL.Text:=viewKwitansiQ.SQL.Text+' and ((k.Tanggal>=:text1 and k.Tanggal<=:text2) or k.Tanggal is null) order by tglentry desc';
    viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
    viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
    viewKwitansiQ.Open;
end;
end;

procedure TPembuatanKwitansiFm.ApproveBtnClick(Sender: TObject);
var s,kode,namaPT,nominal,pemakaian,tujuan,tgl,jam,tempat,NoSO,StatusKwitansi,kodeSO,kodeKontrak,tglPemakaian,GrandTotalS,SisaS,NominalS,KodeS,JamMulaiS,PIC,titelpic,telppic,namapic:string;
var total,sisa,PPN,MarkUp,TotalBayar,TotalNominal,TotalSisaBayar,tersisa,NominalPassing,totalPemakaian,NominalPrint,GrandTotalPrint,NominalAsli:currency;
var counter,kelunasan,toilet,airsus,cariwaktuberangkat:integer;
var WaktuBerangkat:TDateTime;
begin
try
cariwaktuberangkat:=1;
PIC:='-';
  NamaTtd:='Dwi Kurniastuti';
  If MasterQMarkUpNominal.AsCurrency = NULL then
  begin
    MasterQ.Edit;
    MasterQMarkUpNominal.AsCurrency:=0;
  end;
  If MasterQMarkUp.AsCurrency=NULL then
  begin
    MasterQ.Edit;
    MasterQMarkUp.AsCurrency:=0;
  end;
NominalAsli:=MasterQNominal.AsCurrency;
{if MasterQPPh.AsBoolean = True then
begin
    NominalAsli:=NominalAsli*100/98;
end; }
NominalPrint:=NominalAsli+MasterQMarkUpNominal.AsCurrency;
GrandTotalPrint:=MasterQGrandTotal.AsCurrency+MasterQMarkUp.AsCurrency-MasterQDiskon.AsCurrency;
kelunasan:=0;
total:=0;
sisa:=0;
tujuan:='';
pemakaian:='';
TotalNominal:=0;
TotalSisaBayar:=0;
NominalPassing:=0;
StatusKwitansi:=MasterQStatusKwitansi.AsString;
  KodeQ.Open;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    if RbtSO.Checked=true then
    begin
    MasterQStatusKwitansi.AsString:='BELUM LUNAS';
    end;
        if RbtKontrak.Checked=true then
    begin
    MasterQStatusKwitansi.AsString:='LUNAS';
    end;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    if RbtSO.Checked=true then
      begin
         MasterQKeperluan.AsString:='SO';
      end
      else
      begin
          MasterQKeperluan.AsString:='KONTRAK';
      end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  except
  { on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);}
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');

end;
  try
      MasterQ.Edit;
    MasterQStatusKwitansi.AsString:='APPROVED';
    MasterQ.ApplyUpdates;
    if RbtSO.Checked=True then
    begin
         DetailSOQ.Open;
        DetailSOQ.Edit;
        DetailSOQ.First;
        while DetailSOQ.Eof=FALSE do
        begin
       { if PIC='-' then
        begin
          CekPICQ.Close;
          CekPICQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          CekPICQ.Open;
          if CekPICQPICJemput.AsString <> NULL then
          begin
              titelpic:=CekPICQTitlePICJemput.AsString;
              if titelpic = NULL then
              begin
                  titelpic:='';
              end;
              telppic:=CekPICQNoTelpPICJemput.AsString;
              if telppic = NULL then
              begin
                  telppic:='';
              end;
              PIC:=titelpic+' '+CekPICQPICJemput.AsString+' / '+telppic;
          end;
        end; }
          NoSO:=DetailSOQKodeSO.AsString;
          DetailSOQ.Edit;
          DetailSOQKodeKwitansi.AsString:=KodeEdit.Text;
          total:=DetailSOQJumlahBayar.AsCurrency+total;
          NominalPassing:=NominalPassing+DetailSOQNominal.AsCurrency;
          DetailSOQ2.Close;
          DetailSOQ2.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          DetailSOQ2.Open;
          if cariwaktuberangkat=1 then
          begin
              kodeSO:=DetailSOQ2Kodenota.AsString;
              WaktuBerangkat:=DetailSOQ2Berangkat.AsDateTime;
              cariwaktuberangkat:=0;
          end

          else
          begin
              if DetailSOQ2Berangkat.AsDateTime<WaktuBerangkat then
              begin
                  WaktuBerangkat:=DetailSOQ2Berangkat.AsDateTime;
                  kodeSO:=DetailSOQ2Kodenota.AsString;
              end;
          end;

          CekPICQ.Close;
          CekPICQ.ParamByName('text').AsString:=kodeSO;
          CekPICQ.Open;
          if CekPICQPICJemput.AsString <> NULL then
          begin
              titelpic:=CekPICQTitlePICJemput.AsString;
              if titelpic = NULL then
              begin
                  titelpic:='';
              end;
              telppic:=CekPICQNoTelpPICJemput.AsString;
              if telppic = NULL then
              begin
                  telppic:='';
              end;
              namapic:=CekPICQPICJemput.AsString;
              if namapic = NULL then
              begin
                  namapic:='-';
              end;
              PIC:=titelpic+' '+namapic+' / '+telppic;
          end;

          TotalSisaBayar:=TotalSisaBayar+DetailSOQSisaBayar.AsCurrency;
        // DetailSOQKodeSO.AsString
          DetailSOQ.Post;
          DetailSOQ.Next;
        end;
        //total:=total+MasterQMarkUp.AsCurrency;
        NominalPassing:=NominalPassing+MasterQMarkUpNominal.AsCurrency;
        DetailSOQ.ApplyUpdates;
    end;

    if RbtKontrak.Checked=True then
    begin
        DetailKontrakQ.Open;
        DetailKontrakQ.Edit;
        DetailKontrakQ.First;
        while DetailKontrakQ.Eof=FALSE do
        begin

          DetailKontrakQ.Edit;
          DetailKontrakQKodeKwitansi.AsString:=KodeEdit.Text;
          total:=DetailKontrakQHarga.AsCurrency+total;
          NominalPassing:=NominalPassing+DetailKontrakQNominal.AsCurrency;
          kodeKontrak:=DetailKontrakQKodeKontrak.AsString;
          DetailKontrakQ.Post;
          DetailKontrakQ.Next;
        end;
        DetailKontrakQ.ApplyUpdates;
    end;
    kode:=KodeEdit.Text;
    namaPt:=MasterQNamaPelanggan.AsString;
    nominal:=currtostr(MasterQNominal.AsCurrency);
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    //FormShow(self);
    //cxButtonEdit1PropertiesButtonClick(sender,0);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  DetailSOQ.Close;
  DetailSOQ.ParamByName('text').AsString:='';
  DetailSOQ.Open;
  DetailSOQ.Close;

  DetailKontrakQ.Close;
  DetailKontrakQ.ParamByName('text').AsString:='';
  DetailKontrakQ.Open;
  DetailKontrakQ.Close;

if cetak=1 then
begin
  if RbtSO.Checked=True then
  begin
  if MessageDlg('Cetak Kuitansi '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    //isi JumlahSeat
    ArmadaSOQ.Close;
    ArmadaSOQ.ParamByName('text').AsString:=kode;
    ArmadaSOQ.Open;
    ArmadaSOQ.Edit;
    ArmadaSOQ.First;
    while ArmadaSOQ.Eof=false do
    begin
        JumlahArmadaSOQ.Close;
        JumlahArmadaSOQ.ParamByName('text').AsString:=kode;
        JumlahArmadaSOQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
        JumlahArmadaSOQ.Open;
        pemakaian:=pemakaian+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+' bus '+inttostr(ArmadaSOQKapasitasSeat.AsInteger)+' seat';
        CekToiletQ.Close;
        CekToiletQ.ParamByName('text').AsString:=kode;
        CekToiletQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
        CekToiletQ.Open;
        toilet:=CekToiletQJumT.AsInteger;
        if toilet>0 then
        begin
            pemakaian:=pemakaian+' T';
        end;
        CekAirSusQ.Close;
        CekAirSusQ.ParamByName('text').AsString:=kode;
        CekAirSusQ.ParamByName('text2').AsString:=inttostr(ArmadaSOQKapasitasSeat.AsInteger);
        CekAirSusQ.Open;
        airsus:=CekAirSusQJumAS.AsInteger;
        if airsus>0 then
        begin
          if toilet>0 then
          begin
            pemakaian:=pemakaian+'S';
          end
          else
          begin
            pemakaian:=pemakaian+' S';
          end;
        end;
        CekHargaArmadaQ.Close;
        CekHargaArmadaQ.ParamByName('text').AsString:=kode;
        CekHargaArmadaQ.ParamByName('text2').AsString:=ArmadaSOQKapasitasSeat.AsString;
        CekHargaArmadaQ.Open;
        totalPemakaian:=CekHargaArmadaQharga.AsCurrency*JumlahArmadaSOQJumSeat.AsInteger;
        pemakaian:=pemakaian+Chr(13)+Chr(10)+'@'+currtostr(CekHargaArmadaQharga.AsCurrency)+'x '+inttostr(JumlahArmadaSOQJumSeat.AsInteger)+' = '+currtostr(totalPemakaian);
        pemakaian:=pemakaian+'.'+Chr(13)+Chr(10);
        ArmadaSOQ.Next;
    end;
    //akhir Isi jumlah seat

    //isi Terbilang

    s:=spell(currtostr(NominalPrint))+ ' rupiah.';
    //akhir isi Terbilang
    //showmessage(kodeso);
    //isi Tanggal dan jam dan alamat jemput dan tujuan
    SOIsiQ.Close;
    SOIsiQ.ParamByName('text').AsString:=kodeSO;
    SOIsiQ.Open;
    jam:=SOIsiQTglBerangkat.AsString;
    //jam:=SOIsiQJamMulai.AsString+' - '+SOIsiQJamSelesai.AsString;
    tgl:=SOIsiQTglTiba.AsString+ ' - ' + SOIsiQJamSelesai.AsString;
    tempat:=SOIsiQAlamatJemput.AsString+chr(13)+chr(10)+MasterQKotaPelanggan.AsString;
    //tujuan:=SOIsiQDari.AsString+' - '+SOIsiQTujuan.AsString;
    tujuan:=SOIsiQKeteranganRute.AsString;
    tglPemakaian:=jam+' S/D '+tgl;
    //selesai isi tanggal dan jam dan alamat jemput dan tujuan

    //isi Sisa Bayar
     DetailSOQ.Close;
     DetailSOQ.ParamByName('text').AsString:=kode;
     DetailSOQ.Open;
     DetailSOQ.Edit;
     DetailSOQ.Edit;
     DetailSOQ.First;
     TotalNominal:=0;
     while DetailSOQ.Eof=false do
     begin
          HitungNominalSOQ.Close;
          HitungNominalSOQ.ParamByName('text').AsString:=DetailSOQKodeSO.AsString;
          HitungNominalSOQ.Open;
          TotalNominal:=TotalNominal+HitungNominalSOQTotalBayar.AsCurrency;
          DetailSOQ.Next;
     end;
     tersisa:=total-TotalNominal-MasterQDiskon.AsCurrency;
     //selesai isi sisa bayar
    GrandTotalS:=currtostr(MasterQGrandTotal.AsCurrency);
    SisaS:=currtostr(tersisa);
    NominalS:=currtostr(MasterQNominal.AsCurrency);
    Kodes:=KodeEdit.Text;
    JamMulaiS:=SOIsiQJamMulai.AsString;
    CetakanKuitansiQ.Close;
    cetakankuitansiq.ParamByName('text').AsString:=kodeedit.text;
    CetakanKuitansiQ.Open;
    if cetakankuitansiq.RecordCount=0 then
    begin
      CetakanKuitansiQ.Edit;
      CetakanKuitansiQKode.AsString:=KodeS;
      CetakanKuitansiQTerbilang.AsString:=s;
      CetakanKuitansiQGrandTotal.AsString:=GrandTotalS;
      CetakanKuitansiQTujuan.AsString:=tujuan;
      CetakanKuitansiQJamMulai.AsString:=JamMulaiS;
      CetakanKuitansiQTempat.AsString:=tempat;
      CetakanKuitansiQPemakaian.AsString:=pemakaian;
      CetakanKuitansiQTersisa.AsString:=SisaS;
      CetakanKuitansiQNominal.AsString:=NominalS;
      CetakanKuitansiQTglPemakaian.AsString:=tglPemakaian;
      CetakanKuitansiQNamaTtd.AsString:=NamaTtd;
      CetakanKuitansiQPIC.AsString:=PIC;
      CetakanKuitansiQKomisiPelanggan.AsString:=currtostr(MasterQMarkUp.AsCurrency);
      CetakanKuitansiQKomisiPelangganNominal.AsString:=currtostr(MasterQMarkUpNominal.AsCurrency);
      CetakanKuitansiQDiskon.AsString:=currtostr(MasterQDiskon.AsCurrency);
      MenuUtamaFm.Database1.StartTransaction;
      CetakanKuitansiQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      CetakanKuitansiQ.CommitUpdates;
    end;
    if MessageDlg('Tampilkan editor?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    CetakanKuitansiFm:=TCetakanKuitansiFm.Create(self,KodeS);
    CetakanKuitansiFm.ShowModal;
    end

    else
    begin

      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiSO.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.Text;
      Crpe1.ParamFields[1].CurrentValue:=s;
      Crpe1.ParamFields[2].CurrentValue:=currtostr(GrandTotalPrint);
      Crpe1.ParamFields[3].CurrentValue:=tujuan;
      Crpe1.ParamFields[4].CurrentValue:=SOIsiQJamMulai.AsString;
      //Crpe1.ParamFields[5].CurrentValue:=jam;
      Crpe1.ParamFields[5].CurrentValue:=tempat;
      Crpe1.ParamFields[6].CurrentValue:=pemakaian;
      Crpe1.ParamFields[7].CurrentValue:=currtostr(tersisa);
      Crpe1.ParamFields[8].CurrentValue:=currtostr(NominalPrint);
      Crpe1.ParamFields[9].CurrentValue:=tglPemakaian;
      Crpe1.ParamFields[10].CurrentValue:=NamaTtd;
      Crpe1.ParamFields[11].CurrentValue:=PIC;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      {updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update masterso set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;  }
  end;
  end;
  end;

  if RbtKontrak.Checked=True then
  begin
  if MessageDlg('Cetak Kuitansi '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    //isi Terbilang
    s:=spell(currtostr(NominalPassing))+ ' rupiah.';
    //selesai isi Terbilang

    //isi Tujuan, tanggal
    KontrakIsiQ.Close;
    KontrakIsiQ.ParamByName('text').AsString:=kodeKontrak;
    KontrakIsiQ.Open;
    tujuan:=KontrakIsiQDari.AsString+' - '+KontrakIsiQTujuan.AsString;
    tgl:=KontrakIsiQTglMulaiP.AsString+' - '+KontrakIsiQTglSelesaiP.AsString;
    //selesai isi Tujuan,tanggal

    //isi pemakaian
     DetailKontrakQ.Close;
    DetailKontrakQ.ParamByName('text').AsString:=kode;
    DetailKontrakQ.Open;
    DetailKontrakQ.Edit;
    DetailKontrakQ.First;
    pemakaian:='Sewa Kendaraan PO No. ';
    while DetailKontrakQ.Eof=false do
    begin
        POEksternalQ.Close;
        POEksternalQ.ParamByName('text').AsString:=DetailKontrakQKodeKontrak.AsString;
        POEksternalQ.Open;
       // showmessage(POEksternalQPOEksternal.AsString);
        If (POEksternalQPOEksternal.AsString<>'') then
        begin
         pemakaian:=pemakaian+POEksternalQPOEksternal.AsString+'; ';
        end;
        DetailKontrakQ.Next;
    end;
   { ArmadaKontrakQ.Close;
    ArmadaKontrakQ.ParamByName('text').AsString:=kode;
    ArmadaKontrakQ.Open;
    ArmadaKontrakQ.Edit;
    ArmadaKontrakQ.First;
    while ArmadaKontrakQ.Eof=false do
    begin
        JumlahArmadaKontrakQ.Close;
        JumlahArmadaKontrakQ.ParamByName('text').AsString:=kode;
        JumlahArmadaKontrakQ.ParamByName('text2').AsString:=inttostr(ArmadaKontrakQKapasitasSeat.AsInteger);
        JumlahArmadaKontrakQ.Open;
        pemakaian:=pemakaian+inttostr(JumlahArmadaKontrakQJumSeat.AsInteger)+' bus '+inttostr(ArmadaKontrakQKapasitasSeat.AsInteger)+' seat. ';
        ArmadaKontrakQ.Next;
    end;  }
    sisa:=0;
    //selesai isi pemakaian
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiKontrak.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kode;
      Crpe1.ParamFields[1].CurrentValue:=currtostr(NominalPassing);
      Crpe1.ParamFields[2].CurrentValue:=s;
      Crpe1.ParamFields[3].CurrentValue:=pemakaian;

      {Crpe1.ParamFields[0].CurrentValue:=kode;
      Crpe1.ParamFields[1].CurrentValue:=s;
      Crpe1.ParamFields[2].CurrentValue:=currtostr(total);
      Crpe1.ParamFields[3].CurrentValue:=tujuan;
      Crpe1.ParamFields[4].CurrentValue:=tgl;
      Crpe1.ParamFields[5].CurrentValue:=pemakaian;
      Crpe1.ParamFields[6].CurrentValue:=currtostr(NominalPassing); }
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      {updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update masterso set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;  }
  end;
  end;
end;
    DetailSOQ.Close;
  DetailSOQ.ParamByName('text').AsString:='';
  DetailSOQ.Open;
  DetailSOQ.Close;

  DetailKontrakQ.Close;
  DetailKontrakQ.ParamByName('text').AsString:='';
  DetailKontrakQ.Open;
  DetailKontrakQ.Close;
  viewKwitansiQ.Close;
  viewKwitansiQ.Open;
  KodeEditPropertiesButtonClick(sender,0);
end;

procedure TPembuatanKwitansiFm.cxGrid3DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[8] = true) then
    ACanvas.Brush.Color := 16706935;

end;

procedure TPembuatanKwitansiFm.DetailKontrakQBeforePost(DataSet: TDataSet);
var nominalkontrak,totalkontrak:currency;
begin

end;

procedure TPembuatanKwitansiFm.cxGridDBTableView1NominalPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
  var nominalKontrak,totalKontrak,sisaBayarKontrak: currency;
begin

end;

procedure TPembuatanKwitansiFm.MasterVGridPPhEditPropertiesChange(
  Sender: TObject);
begin
{if MasterQPPh.AsBoolean=true then
begin
  MasterQ.Edit;
  MasterQNominal.AsCurrency:=MasterQNominal.AsCurrency*98/100;
end

else
begin
  MasterQ.Edit;
  MasterQNominal.AsCurrency:=MasterQNominal.AsCurrency*100/98;
end; }
end;

procedure TPembuatanKwitansiFm.cxButton1Click(Sender: TObject);
begin
RbtKontrak.Checked:=true;
end;

procedure TPembuatanKwitansiFm.cxSpinEdit1PropertiesChange(
  Sender: TObject);
begin
{if cxSpinEdit1.Value<1 then
begin
   cxSpinEdit1.Value:=1;
end;
 }
end;

procedure TPembuatanKwitansiFm.cxSpinEdit2PropertiesChange(
  Sender: TObject);
begin
{if cxSpinEdit2.Value<1 then
begin
   cxSpinEdit2.Value:=1;
end;}
end;

procedure TPembuatanKwitansiFm.btnLunasClick(Sender: TObject);
begin
try
  tempq.SQL.text:='Update kwitansi set statuskwitansi="LUNAS" where kode="'+kodeedit.text+'"';
  tempq.ExecSQL;
 { if RbtSO.Checked=true then
  begin
      DetailSOQ.Edit;
      DetailSOQ.First;
      while DetailSOQ.Eof=false do
      begin
          if DetailSOQSisaBayar.AsCurrency=0 then
          begin
              TempQ.SQL.Text:='update MasterSO set StatusPembayaran="LUNAS" where KodeNota='+QuotedStr(DetailSOQKodeSO.AsString);
              TempQ.ExecSQL;
          end;
          DetailSOQ.Next;
      end;
  end;    }
  showmessage('Pelunasan berhasil.');
except
  ShowMessage('Update Gagal');
end;
masterq.Refresh;
viewkwitansiq.Refresh;
end;

procedure TPembuatanKwitansiFm.MasterVGridTitipKuitansiEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  if MasterQTitipKuitansi.AsBoolean=true then
  begin
    MasterQStatusKwitansi.AsString:='LUNAS';
  end;
end;

procedure TPembuatanKwitansiFm.MasterVGridDiskonEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQDiskon.AsCurrency:=DisplayValue;
end;

procedure TPembuatanKwitansiFm.cxButton2Click(Sender: TObject);
begin
  viewKwitansiQ.Close;
  viewKwitansiQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKwitansiQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKwitansiQ.ParamByName('text3').AsInteger:=cxSpinEdit1.Value;
  viewKwitansiQ.ParamByName('text4').AsInteger:=cxSpinEdit2.Value;
  viewKwitansiQ.Open;
end;

procedure TPembuatanKwitansiFm.cxButton3Click(Sender: TObject);
var total:currency;
begin
total:=0;
if RbtSO.Checked=true then
begin
    DetailSOQ.Edit;
    DetailSOQ.First;
    while DetailSOQ.Eof=false do
    begin
        DetailSOQ.Edit;
        DetailSOQNominal.AsCurrency:=DetailSOQSisaBayar.AsCurrency;
        total:=total+DetailSOQNominal.AsCurrency;
        DetailSOQ.Next;
    end;
end

else if RbtKontrak.Checked=true then
begin
   DetailKontrakQ.Edit;
   DetailKontrakQ.First;
   while DetailKontrakQ.Eof=false do
   begin
       DetailKontrakQ.Edit;
       DetailKontrakQNominal.AsCurrency:=DetailKontrakQHarga.AsCurrency;
       total:=total+DetailKontrakQNominal.AsCurrency;
       DetailKontrakQ.Next;
   end;
end;

MasterQ.Edit;
MasterQNominal.AsCurrency:=total;
MasterQSisaBayar.AsCurrency:=0;

end;

procedure TPembuatanKwitansiFm.cxButton4Click(Sender: TObject);
var total,sisa:currency;
begin
total:=0;
sisa:=0;
if RbtSO.Checked=true then
begin
   DetailSOQ.Edit;
   DetailSOQ.First;
   while DetailSOQ.Eof=false do
   begin
       DetailSOQ.Edit;
       DetailSOQNominal.AsCurrency:=strtocurr(cxTextEdit2.Text);
       total:=total+DetailSOQNominal.AsCurrency;
       sisa:=sisa+(DetailSOQSisaBayar.AsCurrency-DetailSOQNominal.AsCurrency);
       DetailSOQ.Next
   end;
end

else if RbtKontrak.Checked=true then
begin
   DetailKontrakQ.Edit;
   DetailKontrakQ.First;
   while DetailKontrakQ.Eof=false do
   begin
       DetailKontrakQ.Edit;
       DetailKontrakQNominal.AsCurrency:=strtocurr(cxTextEdit2.Text);
       total:=total+DetailKontrakQNominal.AsCurrency;
       sisa:=sisa+(DetailKontrakQHarga.AsCurrency-DetailKontrakQNominal.AsCurrency);
       DetailKontrakQ.Next;
   end;
end;

MasterQ.Edit;
MasterQNominal.AsCurrency:=total;
MasterQSisaBayar.AsCurrency:=sisa;

end;

end.
