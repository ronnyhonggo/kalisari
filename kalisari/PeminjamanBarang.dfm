object PeminjamanBarangFm: TPeminjamanBarangFm
  Left = 274
  Top = 52
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Peminjaman Barang'
  ClientHeight = 629
  ClientWidth = 920
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 16
      Caption = 'Kode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 56
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.MaxLength = 0
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 341
    Width = 920
    Height = 288
    Align = alBottom
    TabOrder = 2
    object SaveBtn: TcxButton
      Left = 8
      Top = 226
      Width = 75
      Height = 25
      Caption = 'SAVE'
      Enabled = False
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 172
      Top = 226
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 3
      OnClick = ExitBtnClick
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 918
      Height = 216
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 89
        end
        object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
          DataBinding.FieldName = 'Tanggal'
          Width = 152
        end
        object cxGrid1DBTableView1NamaPegawai: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPegawai'
        end
        object cxGrid1DBTableView1Status: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 151
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 262
      Width = 918
      Height = 25
      Panels = <
        item
          Width = 50
        end>
    end
    object DeleteBtn: TcxButton
      Left = 92
      Top = 226
      Width = 75
      Height = 25
      Caption = 'DELETE'
      Enabled = False
      TabOrder = 4
      OnClick = DeleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 622
      Top = 217
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 45
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel1: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
    object ApproveBtn: TcxButton
      Left = 404
      Top = 226
      Width = 75
      Height = 25
      Caption = 'APPROVE'
      TabOrder = 6
      Visible = False
      OnClick = ApproveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 337
    Height = 293
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    LookAndFeel.SkinName = 'Darkroom'
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.GridLineColor = clBtnFace
    OptionsView.RowHeaderWidth = 155
    OptionsView.RowHeight = 12
    OptionsView.ShowEmptyRowImage = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 0
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTanggal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tanggal'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPegawai: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPegawaiEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pegawai'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridNamaPegawai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPegawai'
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      Properties.Options.Editing = False
      ID = 7
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object Panel1: TPanel
    Left = 337
    Top = 48
    Width = 583
    Height = 293
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 581
      Height = 291
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid2DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2Kembali: TcxGridDBColumn
          DataBinding.FieldName = 'Kembali'
          Width = 58
        end
        object cxGrid1DBTableView2Barang: TcxGridDBColumn
          DataBinding.FieldName = 'Barang'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView2BarangPropertiesButtonClick
          Width = 70
        end
        object cxGrid1DBTableView2NamaBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Width = 223
        end
        object cxGrid1DBTableView2Jumlah: TcxGridDBColumn
          DataBinding.FieldName = 'Jumlah'
          Width = 94
        end
        object cxGrid1DBTableView2TglKembali: TcxGridDBColumn
          DataBinding.FieldName = 'TglKembali'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ImmediatePost = True
          Width = 117
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid1DBTableView2
      end
    end
  end
  object MasterQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = [doFetchAllOnOpen]
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from PeminjamanBarang')
    UpdateObject = MasterUS
    Left = 321
    Top = 1
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQPegawai: TStringField
      FieldName = 'Pegawai'
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQNamaPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pegawai'
      Size = 50
      Lookup = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 372
    Top = 65534
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, Pegawai, Tanggal, Status'
      'from PeminjamanBarang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update PeminjamanBarang'
      'set'
      '  Kode = :Kode,'
      '  Pegawai = :Pegawai,'
      '  Tanggal = :Tanggal,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into PeminjamanBarang'
      '  (Kode, Pegawai, Tanggal, Status)'
      'values'
      '  (:Kode, :Pegawai, :Tanggal, :Status)')
    DeleteSQL.Strings = (
      'delete from PeminjamanBarang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 420
    Top = 2
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from PeminjamanBarang order by kode desc')
    Left = 281
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39)
    Left = 473
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      ''
      '')
    Left = 529
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 609
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewKontrakQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from PeminjamanBarang'
      
        'where ((Tanggal >= :text1 and Tanggal <= :text2) or Tanggal is n' +
        'ull) order by Tanggal desc')
    Left = 657
    Top = 1
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewKontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewKontrakQPegawai: TStringField
      FieldName = 'Pegawai'
      Size = 10
    end
    object ViewKontrakQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object ViewKontrakQNamaPegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPegawai'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pegawai'
      Size = 50
      Lookup = True
    end
    object ViewKontrakQStatus: TStringField
      FieldName = 'Status'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewKontrakQ
    Left = 708
    Top = 6
  end
  object DetailQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailPeminjamanBarang where'
      'PeminjamanBarang=:text')
    UpdateObject = ArmadaKontrakUpdate
    Left = 744
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailQPeminjamanBarang: TStringField
      FieldName = 'PeminjamanBarang'
      Required = True
      Size = 10
    end
    object DetailQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailQKembali: TBooleanField
      FieldName = 'Kembali'
    end
    object DetailQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object DetailQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object DetailQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 776
    Top = 8
  end
  object ArmadaKontrakUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select PeminjamanBarang, Barang, Jumlah, Kembali, TglKembali'
      'from DetailPeminjamanBarang'
      'where'
      '  PeminjamanBarang = :OLD_PeminjamanBarang and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update DetailPeminjamanBarang'
      'set'
      '  PeminjamanBarang = :PeminjamanBarang,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  Kembali = :Kembali,'
      '  TglKembali = :TglKembali'
      'where'
      '  PeminjamanBarang = :OLD_PeminjamanBarang and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into DetailPeminjamanBarang'
      '  (PeminjamanBarang, Barang, Jumlah, Kembali, TglKembali)'
      'values'
      '  (:PeminjamanBarang, :Barang, :Jumlah, :Kembali, :TglKembali)')
    DeleteSQL.Strings = (
      'delete from DetailPeminjamanBarang'
      'where'
      '  PeminjamanBarang = :OLD_PeminjamanBarang and'
      '  Barang = :OLD_Barang')
    Left = 808
    Top = 8
  end
  object DeleteArmadaKontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from ArmadaKontrak where Kontrak=:text')
    UpdateObject = DeleteArmadaKontrakUpdate
    Left = 840
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteArmadaKontrakQKontrak: TStringField
      FieldName = 'Kontrak'
      Required = True
      Size = 10
    end
    object DeleteArmadaKontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 50
    end
    object DeleteArmadaKontrakQTglExpired: TDateTimeField
      FieldName = 'TglExpired'
    end
  end
  object DeleteArmadaKontrakUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kontrak, PlatNo, TglExpired'
      'from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    ModifySQL.Strings = (
      'update ArmadaKontrak'
      'set'
      '  Kontrak = :Kontrak,'
      '  PlatNo = :PlatNo,'
      '  TglExpired = :TglExpired'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    InsertSQL.Strings = (
      'insert into ArmadaKontrak'
      '  (Kontrak, PlatNo, TglExpired)'
      'values'
      '  (:Kontrak, :PlatNo, :TglExpired)')
    DeleteSQL.Strings = (
      'delete from ArmadaKontrak'
      'where'
      '  Kontrak = :OLD_Kontrak and'
      '  PlatNo = :OLD_PlatNo')
    Left = 872
    Top = 8
  end
  object UpdatePlatQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'update Kontrak set PlatNo=:text where kode=:text2')
    Left = 512
    Top = 296
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Barang')
    Left = 464
    Top = 320
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQFoto: TBlobField
      FieldName = 'Foto'
    end
    object BarangQNoPabrikan: TStringField
      FieldName = 'NoPabrikan'
      Size = 50
    end
  end
  object HapusQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 464
    Top = 248
  end
  object BarangDetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select Kode,Nama,Jumlah from Barang '
      'where kode=:text')
    Left = 520
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object BarangDetailQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangDetailQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangDetailQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object DetailUbahQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from DetailKeluarBarangCabang where'
      'KodeKeluarBarangcabang=:text and'
      'Barang=:text2')
    Left = 408
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object DetailUbahQKodeKeluarBarangCabang: TStringField
      FieldName = 'KodeKeluarBarangCabang'
      Required = True
      Size = 10
    end
    object DetailUbahQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailUbahQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai')
    Left = 296
    Top = 248
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object StatusQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select count(*) as BelumKembali from DetailPeminjamanBarang'
      'where PeminjamanBarang=:text and Kembali=0')
    Left = 376
    Top = 192
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object StatusQBelumKembali: TIntegerField
      FieldName = 'BelumKembali'
    end
  end
  object UpdateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 416
    Top = 192
  end
end
