unit PeminjamanBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo, cxDropDownEdit, cxGroupBox;

type
  TPeminjamanBarangFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQKeterangan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSopir: TStringField;
    ExitBtn: TcxButton;
    KodeQkode: TStringField;
    ViewKontrakQ: TSDQuery;
    ViewDs: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar: TStatusBar;
    DeleteBtn: TcxButton;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    Panel1: TPanel;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    DetailQ: TSDQuery;
    DataSource1: TDataSource;
    ArmadaKontrakUpdate: TSDUpdateSQL;
    cxGrid1DBTableView2: TcxGridDBTableView;
    DeleteArmadaKontrakUpdate: TSDUpdateSQL;
    UpdatePlatQ: TSDQuery;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel1: TcxLabel;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    HapusQ: TSDQuery;
    ApproveBtn: TcxButton;
    BarangDetailQ: TSDQuery;
    BarangDetailQKode: TStringField;
    BarangDetailQNama: TStringField;
    BarangDetailQJumlah: TFloatField;
    DetailUbahQ: TSDQuery;
    DetailUbahQKodeKeluarBarangCabang: TStringField;
    DetailUbahQBarang: TStringField;
    DetailUbahQJumlah: TFloatField;
    MasterQKode: TStringField;
    MasterQPegawai: TStringField;
    MasterQTanggal: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPegawai: TStringField;
    MasterVGridPegawai: TcxDBEditorRow;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridNamaPegawai: TcxDBEditorRow;
    DetailQPeminjamanBarang: TStringField;
    DetailQBarang: TStringField;
    DetailQKembali: TBooleanField;
    DetailQTglKembali: TDateTimeField;
    DetailQNamaBarang: TStringField;
    cxGrid1DBTableView2Barang: TcxGridDBColumn;
    cxGrid1DBTableView2Kembali: TcxGridDBColumn;
    cxGrid1DBTableView2TglKembali: TcxGridDBColumn;
    cxGrid1DBTableView2NamaBarang: TcxGridDBColumn;
    ViewKontrakQKode: TStringField;
    ViewKontrakQPegawai: TStringField;
    ViewKontrakQTanggal: TDateTimeField;
    ViewKontrakQNamaPegawai: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPegawai: TcxGridDBColumn;
    DetailQJumlah: TFloatField;
    cxGrid1DBTableView2Jumlah: TcxGridDBColumn;
    MasterQStatus: TStringField;
    MasterVGridStatus: TcxDBEditorRow;
    StatusQ: TSDQuery;
    StatusQBelumKembali: TIntegerField;
    UpdateQ: TSDQuery;
    ViewKontrakQStatus: TStringField;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxDBVerticalGrid1Exit(Sender: TObject);
    procedure MasterVGridDBEditorRow1EditPropertiesEditValueChanged(
      Sender: TObject);
    procedure MasterVGridDBEditorRow1EditPropertiesChange(Sender: TObject);
    procedure cxGrid2DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1DBTableView2PlatNoPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView2BarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ApproveBtnClick(Sender: TObject);
    procedure MasterVGridPegawaiEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);



  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  PeminjamanBarangFm: TPeminjamanBarangFm;

  MasterOriSQL, DetailOriSQL, paramkode: string;
implementation

uses MenuUtama, DropDown, DM, MasterPelanggan, PelangganDropDown, RuteDropDown, ArmadaDropDown,
  BarangDropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPeminjamanBarangFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TPeminjamanBarangFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPeminjamanBarangFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPeminjamanBarangFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;
end;

procedure TPeminjamanBarangFm.FormCreate(Sender: TObject);
begin
  //MenuUtamaFm.cxListBox1.Visible:=false;
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
  ViewKontrakQ.Open;
end;

procedure TPeminjamanBarangFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Close;
  MasterQ.Open;
   DetailQ.Close;
   DetailQ.ParamByName('text').AsString:='';
    DetailQ.Open;
  KodeEditExit(sender);
end;

procedure TPeminjamanBarangFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPeminjamanBarangFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
   KodeEdit.Text:=paramkode;
   KodeEditExit(sender);
  end;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKontrak.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
  BarangQ.open;
  PegawaiQ.Open;
end;

procedure TPeminjamanBarangFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    DetailQ.Close;
    DetailQ.ParamByName('text').AsString:=MasterQKode.AsString;
    DetailQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
      MasterQStatus.AsString:='ON PROCESS';
      ApproveBtn.Enabled:=false;
      {MasterQStatusRute.AsString:='SINGLE';
      MasterQAC.AsBoolean:=FALSE;
      MasterQToilet.AsBoolean:=FALSE;
      MasterQAirSuspension.AsBoolean:=FALSE; }
    end
    else
    begin
      Masterq.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TPeminjamanBarangFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPeminjamanBarangFm.SaveBtnClick(Sender: TObject);
var plat,kode:string;
var ubah : integer;
var jumlah:double;
begin
ubah:=0;
if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
    kode:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
  kode:=MasterQKode.AsString;
    MasterQ.ApplyUpdates;
    DetailQ.Edit;
    DetailQ.First;
    while DetailQ.Eof=false do
    begin
        DetailQ.Edit;
        DetailQPeminjamanBarang.AsString:=MasterQKode.AsString;
        DetailQ.Post;
        DetailQ.Next;
    end;


    DetailQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    DetailQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');

    StatusQ.Close;
    StatusQ.ParamByName('text').AsString:=MasterQKode.AsString;
    StatusQ.Open;
    if StatusQBelumKembali.AsInteger>0 then
    begin
        UpdateQ.Close;
        UpdateQ.SQL.Text:='update PeminjamanBarang set [Status]="ON PROCESS" where Kode='+QuotedStr(MasterQKode.AsString);
        UpdateQ.ExecSQL;
    end
    else
    begin
        UpdateQ.Close;
        UpdateQ.SQL.Text:='update PeminjamanBarang set [Status]="FINISHED" where Kode='+QuotedStr(MasterQKode.AsString);
        UpdateQ.ExecSQL;
    end;

  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  KodeEdit.SetFocus;
  DetailQ.Close;
  DetailQ.ParamByName('text').AsString:='';
  DetailQ.Open;
  DetailQ.Close;
  ViewKontrakQ.Refresh;

end;

procedure TPeminjamanBarangFm.MasterQAfterInsert(DataSet: TDataSet);
begin
      //DMFm.GetDateQ.Open;
      //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
      //MasterQCreateBy.AsString:=User;
 // DMFm.GetDateQ.Close;
end;

procedure TPeminjamanBarangFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TPeminjamanBarangFm.DeleteBtnClick(Sender: TObject);
var jumlah:double;
begin
  if MessageDlg('Hapus peminjaman barang '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       DetailQ.Close;
       DetailQ.ParamByName('text').AsString:=MasterQKode.AsString;
       DetailQ.Open;
       DetailQ.First;
       while DetailQ.Eof=FALSE
       do
       begin
          HapusQ.Close;
          HapusQ.SQL.Text:='delete from DetailPeminjamanBarang where PeminjamanBarang='+QuotedStr(MasterQKode.AsString)+' and Barang='+QuotedStr(DetailQBarang.AsString);
          HapusQ.ExecSQL;
          DetailQ.Next;
       end;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Peminjaman barang telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      //DeleteDetailQ.RollbackUpdates;
      MessageDlg('Peminjaman barang pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     viewKontrakQ.Close;
     viewKontrakQ.Open;
     DetailQ.Close;
     //cxButtonEdit1PropertiesButtonClick(Sender,0);
  end;
end;

procedure TPeminjamanBarangFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPeminjamanBarangFm.MasterVGridExit(Sender: TObject);
begin
  //cxDBVerticalGrid1.SetFocus
end;

procedure TPeminjamanBarangFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{  if Abuttonindex=0 then
  begin
    PelangganDropDownFm:=TPelangganDropdownfm.Create(Self);
    if PelangganDropDownFm.ShowModal=MrOK then
    begin
      PelangganQ.Close;
      PelangganQ.open;
      MasterQPelanggan.AsString:=PelangganDropDownFm.kode;
    end;
    PelangganDropDownFm.Release;
  end
  else if abuttonindex=1 then
  begin
    MasterPelangganFm := Tmasterpelangganfm.create(MenuUtamaFm);
  end;   }
end;

procedure TPeminjamanBarangFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    {PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=PelangganQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;}
end;

procedure TPeminjamanBarangFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='';
    RuteQ.ExecSQL;
    RuteQ.Open;}
  {  RuteDropDownFm:=TRuteDropdownfm.Create(Self);
    if RuteDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteDropDownFm.kode;
    end;
    RuteDropDownFm.Release; }
end;

procedure TPeminjamanBarangFm.MasterVGridRuteEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQRute.AsString:=RuteQKode.AsString;
      //cxlabel4.Caption:='Muat : ' + RuteQMuat.AsString;
      //cxlabel5.Caption:='Bongkar : ' + RuteQBongkar.AsString;
      //cxlabel6.Caption:='Standard Harga : ' + RuteQHarga.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
      //cxDBVerticalGrid1.SetFocus;
end;

procedure TPeminjamanBarangFm.MasterVGridArmadaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
   { ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;   }
end;

procedure TPeminjamanBarangFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release; }
end;

procedure TPeminjamanBarangFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridTanggal);
end;

procedure TPeminjamanBarangFm.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
    MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +') x where x.kode like '+ QuotedStr('%'+ViewKontrakQKode.AsString+'%'));
    try
      MasterQ.Open;
    except
      ShowMessage('gagal');
    end;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      MasterQ.Edit;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteKontrak.AsBoolean;
      {if MasterQStatus.AsString='ON PROCESS' then
      begin
          ApproveBtn.Enabled:=true;
      end
      else
      begin
          ApproveBtn.Enabled:=false;
      end; }

    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateKontrak.AsBoolean;
    MasterVGrid.Enabled:=True;
    DetailQ.Close;
    DetailQ.ParamByName('text').AsString := MasterQKode.AsString;
    DetailQ.Open;
    KodeEdit.Text:=MasterQKode.AsString;
end;

procedure TPeminjamanBarangFm.cxDBVerticalGrid1Exit(Sender: TObject);
begin
SaveBtn.SetFocus;

end;

procedure TPeminjamanBarangFm.MasterVGridDBEditorRow1EditPropertiesEditValueChanged(
  Sender: TObject);
begin
 { if MasterQStatusRute.AsString='SINGLE' then
  begin

  end
  else
  begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='0000000000';
    ruteq.ExecSQL;
    ruteq.Open;
    masterQ.Open;
    masterq.Edit;
    MasterQRute.AsString:=RuteQKode.AsString;
  end;  }
end;

procedure TPeminjamanBarangFm.MasterVGridDBEditorRow1EditPropertiesChange(
  Sender: TObject);
begin
  {if MasterQStatusRute.AsString='SINGLE' then
  begin
    //MasterVGridRute.Visible:=true;
  end
  else
  begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='0000000000';
    ruteq.ExecSQL;
    ruteq.Open;
    masterQ.Open;
    masterq.Edit;
    MasterQRute.AsString:=RuteQKode.AsString;
  end;   }
end;


procedure TPeminjamanBarangFm.cxGrid2DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
  DetailQ.Edit;
  DetailQPeminjamanBarang.AsString:='';
  //DetailQTglKembali.AsDateTime:=DetailQTglKembali.AsDateTime;
  //DetailQKembali.AsBoolean:=false;
  DetailQ.Post;
  cxGrid1DBTableView1.Focused:=false;
  ViewKontrakQ.Refresh;
  end;
end;

procedure TPeminjamanBarangFm.cxGrid1DBTableView2PlatNoPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
     {ArmadaDropDownFm:=TArmadaDropdownfm.Create(Self);
    if ArmadaDropDownFm.ShowModal=MrOK then
    begin
      //DetailQ.Open;
      //DetailQ.Insert;
      DetailQ.Edit;
      DetailQKontrak.AsString:=ArmadaDropDownFm.kode;
      DetailQPlatNo.AsString:=ArmadaDropDownFm.plat;
      //DetailQ.Post;
    end;
    ArmadaDropDownFm.Release; }
end;

procedure TPeminjamanBarangFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date;
  ViewKontrakQ.Open;
end;

procedure TPeminjamanBarangFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewKontrakQ.Close;
  ViewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.Date;
  ViewKontrakQ.Open;
end;

procedure TPeminjamanBarangFm.cxGrid1DBTableView2BarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropdownfm.Create(Self);
    if BarangDropDownFm.ShowModal=MrOK then
    begin
      DetailQ.Open;
      DetailQ.Edit;
      DetailQBarang.AsString:=BarangDropDownFm.kode;
      DetailQKembali.AsBoolean:=false;
      //MasterVGrid.SetFocus;
    end;
    BarangDropDownFm.Release;
end;

procedure TPeminjamanBarangFm.ApproveBtnClick(Sender: TObject);
var jumlah:double;
begin
{if MasterQStatus.AsString='ON PROCESS' then
begin
    HapusQ.Close;
    HapusQ.SQL.Text:='update KeluarBarangCabang set [Status]="APPROVED" where Kode='+QuotedStr(MasterQKode.AsString);
    HapusQ.ExecSQL;
    DetailQ.Edit;
    DetailQ.First;
    while DetailQ.Eof=false do
    begin
      BarangDetailQ.Close;
      BarangDetailQ.ParamByName('text').AsString:=DetailQBarang.AsString;
      BarangDetailQ.Open;
      jumlah:=BarangDetailQJumlah.AsFloat-DetailQJumlah.AsFloat;
      HapusQ.Close;
      HapusQ.SQL.Text:='update Barang set Jumlah='+QuotedStr(floattostr(jumlah))+' where Kode='+QuotedStr(DetailQBarang.AsString);
      HapusQ.ExecSQL;
      DetailQ.Next;
    end;
    ShowMessage('Keluar barang cabang telah di-approve.');
    MasterQ.Refresh;
    ViewKontrakQ.Refresh;
end

else if MasterQStatus.AsString='APPROVED' then
begin
    ShowMessage('Keluar barang cabang telah di-approve sebelumnya.');
end; }
end;

procedure TPeminjamanBarangFm.MasterVGridPegawaiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPegawai.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

end.
