object PemutihanFm: TPemutihanFm
  Left = 258
  Top = 224
  Width = 892
  Height = 439
  AutoSize = True
  Caption = 'Pemutihan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxLabel1: TcxLabel
      Left = 352
      Top = 8
      Caption = 'History Pemutihan'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 330
    Width = 876
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object BtnSetuju: TBitBtn
      Left = 353
      Top = 13
      Width = 177
      Height = 23
      Caption = 'Save dan Setuju'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtnSetujuClick
      Kind = bkAll
    end
    object cxGroupBox1: TcxGroupBox
      Left = 578
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 381
    Width = 876
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 876
    Height = 282
    Align = alClient
    TabOrder = 3
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 352
      Height = 280
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 124
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnKeyDown = MasterVGridKeyDown
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridBarang: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridBarangEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Barang'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridNamaBarang: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaBarang'
        ID = 33
        ParentID = 0
        Index = 0
        Version = 1
      end
      object MasterVGridStokBarang: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'StokBarang'
        ID = 34
        ParentID = 0
        Index = 1
        Version = 1
      end
      object MasterVGridSatuan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Satuan'
        ID = 35
        ParentID = 0
        Index = 2
        Version = 1
      end
      object MasterVGridStokSebelum: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'StokSebelum'
        Properties.Options.Editing = False
        ID = 4
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridSaldo: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.OnValidate = MasterVGridSaldoEditPropertiesValidate
        Properties.DataBinding.FieldName = 'Saldo'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 5
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridStokActual: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.DataBinding.FieldName = 'StokActual'
        Properties.Options.Editing = False
        ID = 6
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Keterangan'
        ID = 7
        ParentID = -1
        Index = 4
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 353
      Top = 1
      Width = 522
      Height = 280
      Align = alClient
      TabOrder = 1
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'TglEntry'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ShowTime = False
          Width = 95
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Width = 132
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'Saldo'
          Width = 61
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 86
        end
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 142
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from pemutihan')
    UpdateObject = MasterUS
    Left = 201
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object MasterQStokActual: TFloatField
      FieldName = 'StokActual'
    end
    object MasterQStokSebelum: TFloatField
      FieldName = 'StokSebelum'
    end
    object MasterQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object MasterQStokBarang: TFloatField
      FieldKind = fkLookup
      FieldName = 'StokBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jumlah'
      KeyFields = 'Barang'
      Lookup = True
    end
    object MasterQSatuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Satuan'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Satuan'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 236
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, Saldo, Keterangan, CreateDate, CreateBy, Tg' +
        'lEntry, Operator, Status, StokActual, StokSebelum'
      'from pemutihan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update pemutihan'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  Saldo = :Saldo,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  Status = :Status,'
      '  StokActual = :StokActual,'
      '  StokSebelum = :StokSebelum'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into pemutihan'
      
        '  (Kode, Barang, Saldo, Keterangan, CreateDate, CreateBy, TglEnt' +
        'ry, Operator, Status, StokActual, StokSebelum)'
      'values'
      
        '  (:Kode, :Barang, :Saldo, :Keterangan, :CreateDate, :CreateBy, ' +
        ':TglEntry, :Operator, :Status, :StokActual, :StokSebelum)')
    DeleteSQL.Strings = (
      'delete from pemutihan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 268
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pemutihan order by kode desc')
    Left = 169
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select cast(createdate as date) as Tanggal,*,'#39#39' as namabarang '
      'from pemutihan'
      
        'where (createdate>=:text1 and createdate<=:text2) or createdate ' +
        'is null'
      'order by tglentry desc')
    Left = 553
    Top = 7
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'text2'
        ParamType = ptInput
      end>
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1Barang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object SDQuery1Keterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SDQuery1CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery1CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery1TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery1Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery1NamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = SDQuery2
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 55
      Lookup = True
    end
    object SDQuery1Status: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object SDQuery1Tanggal: TStringField
      FieldName = 'Tanggal'
    end
    object SDQuery1Saldo: TIntegerField
      FieldName = 'Saldo'
    end
    object SDQuery1StokActual: TIntegerField
      FieldName = 'StokActual'
    end
    object SDQuery1StokSebelum: TIntegerField
      FieldName = 'StokSebelum'
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 596
    Top = 14
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang'
      '')
    Left = 305
    Top = 7
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Sta' +
        'ndardUmur, Lokasi, CreateDate, CreateBy, Operator, TglEntry'#13#10'fro' +
        'm barang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  MinimumStok = :MinimumStok,'
      '  MaximumStok = :MaximumStok,'
      '  StandardUmur = :StandardUmur,'
      '  Lokasi = :Lokasi,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      
        '  (Kode, Nama, Jumlah, Satuan, MinimumStok, MaximumStok, Standar' +
        'dUmur, Lokasi, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :Nama, :Jumlah, :Satuan, :MinimumStok, :MaximumStok, :' +
        'StandardUmur, :Lokasi, :CreateDate, :CreateBy, :Operator, :TglEn' +
        'try)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 636
    Top = 10
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clSkyBlue
    end
  end
  object SDQuery2: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang'
      '')
    UpdateObject = SDUpdateSQL1
    Left = 513
    Top = 7
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object StringField3: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object IntegerField2: TIntegerField
      FieldName = 'MinimumStok'
    end
    object IntegerField3: TIntegerField
      FieldName = 'MaximumStok'
    end
    object IntegerField4: TIntegerField
      FieldName = 'StandardUmur'
    end
    object StringField4: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CreateDate'
    end
    object StringField5: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object StringField6: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery2Jumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object SDQuery3: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select *, '#39#39' as namabarang from pemutihan order by tglentry desc')
    Left = 689
    Top = 15
    object StringField7: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField8: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object IntegerField5: TIntegerField
      FieldName = 'Penambahan'
    end
    object IntegerField6: TIntegerField
      FieldName = 'Pengurangan'
    end
    object MemoField1: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'CreateDate'
    end
    object StringField9: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'TglEntry'
    end
    object StringField10: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object StringField11: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = SDQuery2
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 55
      Lookup = True
    end
  end
end
