unit Penagihan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, cxDropDownEdit, UCrpeClasses,
  UCrpe32, cxMemo;

type
  TPenagihanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    SaveBtn: TcxButton;
    SuratJalanQ: TSDQuery;
    ExitBtn: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    KodeQ: TSDQuery;
    Crpe1: TCrpe;
    KodeQkode: TStringField;
    MasterQKode: TStringField;
    MasterQSalesOrder: TStringField;
    MasterQBiayaLain: TCurrencyField;
    MasterQKetBiayaLain: TMemoField;
    MasterQClaim: TCurrencyField;
    MasterQketClaim: TMemoField;
    MasterQTerbayar: TCurrencyField;
    MasterQTglDibayar: TDateTimeField;
    MasterQCaraPembayaran: TStringField;
    MasterQNoKwitansi: TStringField;
    MasterQPenerimaPembayaran: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxDBVerticalGrid1SalesOrder: TcxDBEditorRow;
    cxDBVerticalGrid1BiayaLain: TcxDBEditorRow;
    cxDBVerticalGrid1KetBiayaLain: TcxDBEditorRow;
    cxDBVerticalGrid1Claim: TcxDBEditorRow;
    cxDBVerticalGrid1ketClaim: TcxDBEditorRow;
    MasterQKodeRute: TStringField;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    MasterQAsal: TStringField;
    MasterQTujuan: TStringField;
    cxDBVerticalGrid1Asal: TcxDBEditorRow;
    cxDBVerticalGrid1Tujuan: TcxDBEditorRow;
    MasterQKodePelanggan: TStringField;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQNoTelp: TStringField;
    cxDBVerticalGrid1KodePelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1AlamatPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1NoTelp: TcxDBEditorRow;
    MasterQHarga: TCurrencyField;
    MasterQPembayaranAwal: TCurrencyField;
    MasterQBiayaExtend: TCurrencyField;
    MasterQPelunasan: TCurrencyField;
    SuratJalanQKodenota: TStringField;
    SuratJalanQTgl: TDateTimeField;
    SuratJalanQNoSO: TStringField;
    SuratJalanQSopir: TStringField;
    SuratJalanQSopir2: TStringField;
    SuratJalanQCrew: TStringField;
    SuratJalanQTitipKwitansi: TBooleanField;
    SuratJalanQNominalKwitansi: TCurrencyField;
    SuratJalanQNoKwitansi: TStringField;
    SuratJalanQKeterangan: TStringField;
    SuratJalanQKir: TBooleanField;
    SuratJalanQSTNK: TBooleanField;
    SuratJalanQPajak: TBooleanField;
    SuratJalanQTglKembali: TDateTimeField;
    SuratJalanQPendapatan: TCurrencyField;
    SuratJalanQPengeluaran: TCurrencyField;
    SuratJalanQSisaDisetor: TCurrencyField;
    SuratJalanQSPBUAYaniLiter: TFloatField;
    SuratJalanQSPBUAYaniUang: TCurrencyField;
    SuratJalanQSPBUAYaniJam: TCurrencyField;
    SuratJalanQSPBULuarLiter: TFloatField;
    SuratJalanQSPBULuarUang: TCurrencyField;
    SuratJalanQSPBULuarUangDiberi: TCurrencyField;
    SuratJalanQSPBULuarDetail: TMemoField;
    SuratJalanQStatus: TStringField;
    SuratJalanQCreateDate: TDateTimeField;
    SuratJalanQCreateBy: TStringField;
    SuratJalanQOperator: TStringField;
    SuratJalanQTglEntry: TDateTimeField;
    SuratJalanQLaporan: TMemoField;
    SuratJalanQTglRealisasi: TDateTimeField;
    SuratJalanQAwal: TStringField;
    SuratJalanQAkhir: TStringField;
    SuratJalanQTglCetak: TDateTimeField;
    SuratJalanQClaimSopir: TCurrencyField;
    SuratJalanQKeteranganClaimSopir: TStringField;
    SuratJalanQPremiSopir: TCurrencyField;
    SuratJalanQPremiSopir2: TCurrencyField;
    SuratJalanQPremiKernet: TCurrencyField;
    SuratJalanQTabunganSopir: TCurrencyField;
    SuratJalanQTabunganSopir2: TCurrencyField;
    SuratJalanQTol: TCurrencyField;
    SuratJalanQUangJalan: TCurrencyField;
    SuratJalanQBiayaLainLain: TCurrencyField;
    SuratJalanQUangMakan: TCurrencyField;
    SuratJalanQOther: TCurrencyField;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1Harga: TcxDBEditorRow;
    cxDBVerticalGrid1BiayaExtend: TcxDBEditorRow;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2Terbayar: TcxDBEditorRow;
    cxDBVerticalGrid2TglDibayar: TcxDBEditorRow;
    cxDBVerticalGrid2CaraPembayaran: TcxDBEditorRow;
    cxDBVerticalGrid2NoKwitansi: TcxDBEditorRow;
    cxDBVerticalGrid2PenerimaPembayaran: TcxDBEditorRow;
    MasterQNominalKwitansi: TCurrencyField;
    cxDBVerticalGrid1Pelunasan: TcxDBEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    viewPenagihan: TSDQuery;
    viewPenagihanKode: TStringField;
    viewPenagihanSalesOrder: TStringField;
    viewPenagihanBiayaLain: TCurrencyField;
    viewPenagihanKetBiayaLain: TMemoField;
    viewPenagihanClaim: TCurrencyField;
    viewPenagihanketClaim: TMemoField;
    viewPenagihanTerbayar: TCurrencyField;
    viewPenagihanTglDibayar: TDateTimeField;
    viewPenagihanCaraPembayaran: TStringField;
    viewPenagihanNoKwitansi: TStringField;
    viewPenagihanPenerimaPembayaran: TStringField;
    viewPenagihanCreateDate: TDateTimeField;
    viewPenagihanCreateBy: TStringField;
    viewPenagihanOperator: TStringField;
    viewPenagihanTglEntry: TDateTimeField;
    cxGrid1DBTableView1SalesOrder: TcxGridDBColumn;
    cxGrid1DBTableView1Terbayar: TcxGridDBColumn;
    cxGrid1DBTableView1TglDibayar: TcxGridDBColumn;
    cxGrid1DBTableView1CaraPembayaran: TcxGridDBColumn;
    cxGrid1DBTableView1NoKwitansi: TcxGridDBColumn;
    cxGrid1DBTableView1PenerimaPembayaran: TcxGridDBColumn;
    KodeEdit: TcxButtonEdit;
    SOQTglPelunasan: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    cxDBVerticalGrid2NamaPenerima: TcxDBEditorRow;
    cxDBVerticalGrid2JabatanPenerima: TcxDBEditorRow;
    viewPenagihanNamaPenerima: TStringField;
    SOQPPN: TCurrencyField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    MasterQPPN: TCurrencyField;
    MasterQNominalKwitansiPembayaranAwal: TCurrencyField;
    MasterQNominalKwitansiPelunasan: TCurrencyField;
    MasterQPPNExtend: TCurrencyField;
    cxDBVerticalGrid1PPN: TcxDBEditorRow;
    cxDBVerticalGrid1NominalKwitansiPembayaranAwal: TcxDBEditorRow;
    cxDBVerticalGrid1PPNExtend: TcxDBEditorRow;
    cxDBVerticalGrid1NominalKwitansiPelunasan: TcxDBEditorRow;
    cxDBVerticalGrid1NominalKwitansi: TcxDBEditorRow;
    MasterQNominalKwitansiPenagihan: TCurrencyField;
    cxDBVerticalGrid2NominalKwitansiPenagihan: TcxDBEditorRow;
    cxButton1: TcxButton;
    MasterQKetCaraPembayaran: TStringField;
    cxDBVerticalGrid2KetCaraPembayaran: TcxDBEditorRow;
    viewPenagihanKetCaraPembayaran: TStringField;
    viewPenagihanNominalKwitansiPenagihan: TCurrencyField;
    SuratJalanQKeteranganBiayaLainLain: TStringField;
    SuratJalanQUangInap: TCurrencyField;
    SuratJalanQUangParkir: TCurrencyField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure cxDBVerticalGrid1SuratJalanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1SuratJalanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure cxDBVerticalGrid1BiayaLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1ClaimEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure cxDBVerticalGrid1SalesOrderEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid2PenerimaPembayaranEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDBVerticalGrid2NominalKwitansiPenagihanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2NoKwitansiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PenagihanFm: TPenagihanFm;

  MasterOriSQL,ViewOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, SODropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPenagihanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPenagihanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPenagihanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPenagihanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPenagihanFm.FormCreate(Sender: TObject);
begin
  //DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  masterq.Open;
  masterq.Append;
end;




procedure TPenagihanFm.FormShow(Sender: TObject);
var i : integer;
begin
  KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  ViewPenagihan.Active:=TRUE;
  {if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;     }
 // kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertSO.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSO.AsBoolean;
 // DeleteBtn.Enabled:=menuutamafm.UserQDeleteSO.AsBoolean;
end;


function PartialSpell( strInput : String) : String;
var strSebutan, strAngka, strHasil : String;
    sTAngka : char;
    i,sAngka : Byte;
begin
  For i := length(strInput) DownTo 1 do
   begin
    //--ambil satu digit
    sTAngka := strInput[i];
    if not (sTAngka  in ['0'..'9']) then continue;
    sAngka := strToInt(copy(strInput, i, 1));
    //--ubah angka menjadi huruf yang sesuai, kecuali angka nol
    Case sAngka of
       1: strAngka := 'satu ';
       2: strAngka := 'dua ';
       3: strAngka := 'tiga ';
       4: strAngka := 'empat ';
       5: strAngka := 'lima ';
       6: strAngka := 'enam ';
       7: strAngka := 'tujuh ';
       8: strAngka := 'delapan ';
       9: strAngka := 'sembilan ';
       0: begin
             strAngka := '';
             strSebutan := '';
          end;
    End;
    strSebutan := '';
    //--cek kondisi khusus untuk angka 1 yang bisa berubah jad 'se'
    If (sAngka = 1) And (i <> length(strInput)) Then strAngka := 'se';
    //--tambahkan satuan yang sesuai yaitu puluh, belas, ratus
    If ((length(strInput) - i) = 1) And (sAngka <> 0) Then
     begin
      strSebutan := 'puluh ';
      If (sAngka = 1) And (strHasil <> '') Then
       begin
        strSebutan := 'belas ';
        If strHasil = 'satu ' Then strAngka := 'se'
          Else strAngka := strHasil;
        strHasil := '';
       end
     End
     else if ((length(strInput) - i) = 2) And (sAngka <> 0) Then
         strSebutan := 'ratus ';
    strHasil := strAngka + strSebutan + strHasil
  end;
  PartialSpell := strHasil
end;

function Spell(strInput : String) : String ;
var i : integer;
    j : Byte;
  strPars, strOlah, strSebutan, strHasil, strSpell : String;
begin
  if length(strInput)>15 then raise Exception.Create('Nilai maksimal 999.999.999.999.999');
  strInput:=Copy(strInput,1,15);
  strPars := strInput;
  i := length(strInput);
  repeat
    //--mengambil angka 3 digit dimulai dari belakang
    j := length(strPars) Mod 3;
    If j <> 0 Then
    begin
      strOlah := copy(strPars, 1, j);
      strPars := copy(strPars, j + 1, length(strPars) - j)
    end
    Else
    begin
      strOlah := copy(strPars, 1, 3);
      strPars := copy(strPars,4,length(strPars)-3);
    End;
    //--membilang 3 digit angka yang ada
    strSpell := PartialSpell(strOlah);
    //--menambahkan satuan yang sesuai, misalnya : ribu, juta, milyar, trilyun
    If strSpell <> '' Then
      If i > 12 Then
        strSebutan := 'trilyun '
       else if i > 9 Then
         strSebutan := 'milyar '
        else if i > 6 Then
          strSebutan := 'juta '
         else if i > 3 Then
          begin
           strSebutan := 'ribu ';
           If strSpell = 'satu ' Then strSpell := 'se'
          end;
    strHasil := strHasil + strSpell + strSebutan;
    strSpell := '';
    strSebutan := '';
    dec(i,3);
  until i<1;
  if Length(StrHasil)>0 then StrHasil[1]:=UpCase(StrHasil[1]);
  Spell := strHasil;
end;


procedure TPenagihanFm.SaveBtnClick(Sender: TObject);
var kodeprint : String;
begin

  MenuUtamaFm.Database1.StartTransaction;
  try
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Sukses');
    kodeprint:=MasterQKode.asstring;
   { if MessageDlg('Cetak Bon BBM' + ', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonBBM.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kodeprint;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
    end;   }
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  masterq.Close;
  masterq.SQL.Clear;
  masterq.SQL.Text:=MasterOriSQL;
  masterq.Open;
  masterq.Close;
  //masterq.Append;
  KodeEdit.SetFocus;
  viewPenagihan.close;
  viewPenagihan.Open;
  //masterq.ClearFields;
end;


procedure TPenagihanFm.cxDBVerticalGrid1SuratJalanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SuratJalanQ.Close;
    SuratJalanQ.ParamByName('text').AsString:='%';
    SuratJalanQ.ExecSQL;
    SuratJalanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuratJalanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Edit;
     // MasterQSuratJalan.AsString:=SuratJalanQKodeNota.AsString;
    //  MasterQmuat.AsString := suratjalanqmuat.AsString;
     // Masterqbongkar.AsString := suratjalanqbongkar.AsString;
     // Masterqxtgl.AsString := suratjalanqxtgl.AsString;
     // MasterQxnama.AsString:= SuratJalanQxnama.AsString;
      //MasterQxnosj.AsString := SuratJalanQxnosj.AsString;
     // MasterQdaript.AsString:=SuratJalanQdaript.AsString;
    //  MasterQdarialamat.AsString:=SuratJalanQdarialamat.AsString;
     // MasterQkept.AsString:=SuratJalanQkept.AsString;
    //  MasterQkealamat.AsString:=SuratJalanQkealamat.AsString;
    //  MasterQharga.AsString:=SuratJalanQharga.AsString;
    //  masterqberat.AsString:=SuratJalanQberat.AsString;
   //   MasterQrealisasitonasebongkar.AsString:=SuratJalanQrealisasitonasebongkar.AsString;
     // MasterQGrandTotal.AsInteger:=SuratJalanQrealisasitonasebongkar.AsInteger*SuratJalanQharga.AsInteger;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPenagihanFm.cxDBVerticalGrid1SuratJalanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SuratJalanQ.Close;
    SuratJalanQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SuratJalanQ.ExecSQL;
    SuratJalanQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SuratJalanQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
    {  MasterQSuratJalan.AsString:=SuratJalanQKodeNota.AsString;
      MasterQmuat.AsString := suratjalanqmuat.AsString;
      Masterqbongkar.AsString := suratjalanqbongkar.AsString;
      Masterqxtgl.AsString := suratjalanqxtgl.AsString;
      MasterQxnama.AsString:= SuratJalanQxnama.AsString;
      MasterQxnosj.AsString := SuratJalanQxnosj.AsString;
      MasterQdaript.AsString:=SuratJalanQdaript.AsString;
      MasterQdarialamat.AsString:=SuratJalanQdarialamat.AsString;
      MasterQkept.AsString:=SuratJalanQkept.AsString;
      MasterQkealamat.AsString:=SuratJalanQkealamat.AsString;
      MasterQharga.AsString:=SuratJalanQharga.AsString;
      masterqberat.AsString:=SuratJalanQberat.AsString;
      MasterQrealisasitonasebongkar.AsString:=SuratJalanQrealisasitonasebongkar.AsString;
      //MasterVGrid.SetFocus; }
    end;
    DropDownFm.Release;
end;

procedure TPenagihanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TPenagihanFm.cxDBVerticalGrid1BiayaLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQBiayaLain.AsInteger:=DisplayValue;
  MasterQTerbayar.AsCurrency:=(MasterQHarga.AsCurrency+MasterQPPN.AsCurrency+MasterQClaim.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency+MasterQBiayaLain.AsCurrency)-(MasterQPelunasan.AsCurrency+MasterQPembayaranAwal.AsCurrency+MasterQNominalKwitansi.AsCurrency);

 // MasterQGrandTotal.AsInteger:=(MasterQrealisasitonasebongkar.AsInteger*MasterQharga.AsInteger)+MasterQBiayaLain.AsInteger+MasterQClaim.AsInteger;
end;

procedure TPenagihanFm.cxDBVerticalGrid1ClaimEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQClaim.AsInteger:=DisplayValue;
 MasterQTerbayar.AsCurrency:=(MasterQHarga.AsCurrency+MasterQPPN.AsCurrency+MasterQClaim.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency+MasterQBiayaLain.AsCurrency)-(MasterQPelunasan.AsCurrency+MasterQPembayaranAwal.AsCurrency+MasterQNominalKwitansi.AsCurrency);

   //MasterQGrandTotal.AsInteger:=(MasterQrealisasitonasebongkar.AsInteger*MasterQharga.AsInteger)+MasterQBiayaLain.AsInteger+MasterQClaim.AsInteger;
end;

procedure TPenagihanFm.MasterQBeforePost(DataSet: TDataSet);
begin
 MasterQOperator.AsString := User;
end;

procedure TPenagihanFm.cxDBVerticalGrid1SalesOrderEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{SOQ.Close;
SOQ.ExecSQL;
SOQ.Open;}
SODropDownFm:=TSODropDownFm.Create(self);
if SODropDownFm.ShowModal=MrOK then
begin
MasterQ.Open;
MasterQ.Edit;
MasterQSalesOrder.AsString:=SODropDownFm.kode;


end;
SODropDownFm.Release;
MasterQTerbayar.AsCurrency:=(MasterQHarga.AsCurrency+MasterQPPN.AsCurrency+MasterQClaim.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQPPNExtend.AsCurrency+MasterQBiayaLain.AsCurrency)-(MasterQPelunasan.AsCurrency+MasterQPembayaranAwal.AsCurrency+MasterQNominalKwitansi.AsCurrency);

if MasterQBiayaExtend.AsCurrency <= 0 then
begin
    cxDBVerticalGrid1BiayaExtend.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1BiayaExtend.Visible:=TRUE;
end;

if MasterQPelunasan.AsCurrency <= 0 then
begin
    cxDBVerticalGrid1Pelunasan.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1Pelunasan.Visible:=TRUE;
end;

if MasterQPembayaranAwal.AsCurrency <= 0 then
begin
   cxDBVerticalGrid1DBEditorRow1.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1DBEditorRow1.Visible:=TRUE;
end;

if MasterQNominalKwitansi.AsCurrency <= 0 then
begin
  cxDBVerticalGrid1NominalKwitansi.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1NominalKwitansi.Visible:=TRUE;
end;


end;

procedure TPenagihanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    KodeEdit.SetFocus;
    KodeEdit.text:=viewPenagihanKode.AsString;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewPenagihanKode.AsString+'%'));
    MasterQ.Open;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    SaveBtn.Enabled:=True;
   // MasterVGrid.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
    cxDBVerticalGrid2.Enabled:=true;

if MasterQBiayaExtend.AsCurrency <= 0 then
begin
    cxDBVerticalGrid1BiayaExtend.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1BiayaExtend.Visible:=TRUE;
end;

if MasterQPelunasan.AsCurrency <= 0 then
begin
    cxDBVerticalGrid1Pelunasan.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1Pelunasan.Visible:=TRUE;
end;

if MasterQPembayaranAwal.AsCurrency <= 0 then
begin
   cxDBVerticalGrid1DBEditorRow1.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1DBEditorRow1.Visible:=TRUE;
end;

if MasterQNominalKwitansi.AsCurrency <= 0 then
begin
  cxDBVerticalGrid1NominalKwitansi.Visible:=FALSE;
end
else
begin
cxDBVerticalGrid1NominalKwitansi.Visible:=TRUE;
end;


    //SaveBtn.Enabled:=True;
   // if MasterQKontrak.AsVariant = Null then cxRadioButton1.Checked:=true;
   // MasterVGrid.Enabled:=True;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);
end;

procedure TPenagihanFm.KodeEditEnter(Sender: TObject);
begin
 //buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
 // MasterVGrid.Enabled:=False;
  cxDBVerticalGrid1.Enabled:=False;
    cxDBVerticalGrid2.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPenagihanFm.KodeEditExit(Sender: TObject);
begin
 if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
     // MasterQStatus.AsString :='IN PROGRESS';
      //savebtn.Enabled:=MenuUtamaFm.UserQInsert.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
  cxDBVerticalGrid1.Enabled:=TRUE;
  cxDBVerticalGrid2.Enabled:=TRUE;
  end;
end;

procedure TPenagihanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPenagihanFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
 KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  //MasterVGrid.SetFocus;
  //cxDBVerticalGrid1
  //DMFm.GetDateQ.Open;
 // MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //DMFm.GetDateQ.Close;
 // MasterVGrid.FocusRow(MasterVGridNoSO);
  //MasterVGrid.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
    cxDBVerticalGrid2.Enabled:=true;
  cxDBVerticalGrid1BiayaExtend.Visible:=TRUE;
  cxDBVerticalGrid1Pelunasan.Visible:=TRUE;
  cxDBVerticalGrid1DBEditorRow1.Visible:=TRUE;
  cxDBVerticalGrid1NominalKwitansi.Visible:=TRUE;
end;

procedure TPenagihanFm.cxDBVerticalGrid2PenerimaPembayaranEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //sopirQ.Close;
   { PegawaiQ.Close;
    PegawaiQ.ExecSQL;
    PegawaiQ.Open; }
  // sopirQ.ExecSQL;
   // sopirQ.Open;
   PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
    //MasterQSopir.AsString := sopirQKode.AsString;
    MasterQPenerimaPembayaran.AsString:=PegawaiDropDownFm.kode;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TPenagihanFm.cxButton1Click(Sender: TObject);
begin
 if MessageDlg('Hapus Penagihan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Penagihan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Penagihan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewPenagihan.Close;
  viewPenagihan.ExecSQL;
  viewPenagihan.Open;
  end;
end;

procedure TPenagihanFm.cxDBVerticalGrid2NominalKwitansiPenagihanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
if AButtonIndex=0 then
begin
//cetak SJ
   if MessageDlg('Cetak Kwitansi Pembayaran II untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPelunasan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
  
end

else
begin
 showmessage(inttostr(AButtonIndex));
    if MessageDlg('Cetak Kwitansi Pembayaran II untuk SO '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPelunasan2.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end;
end;

procedure TPenagihanFm.cxDBVerticalGrid2NoKwitansiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
    var terbilang:string;
  var h,p,e,pe,c,bl,jumlah:currency;
begin
h:=0;
p:=0;
e:=0;
pe:=0;
c:=0;
bl:=0;
if AButtonIndex=0 then
begin
  if MessageDlg('Cetak Kwitansi Penagihan untuk SO '+MasterQSalesOrder.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
     if MasterQHarga.AsCurrency<>NULL then
    begin
       h:=MasterQHarga.AsCurrency;
    end;
    if MasterQPPN.AsCurrency<>NULL then
    begin
        p:=MasterQPPN.AsCurrency;
    end;
    if MasterQBiayaExtend.AsCurrency<>NULL then
    begin
        e:=MasterQBiayaExtend.AsCurrency;
    end;
    if MasterQPPNExtend.AsCurrency<>NULL then
    begin
        pe:=MasterQPPNExtend.AsCurrency;
    end;
    if MasterQClaim.AsCurrency<>NULL then
    begin
        c:=MasterQClaim.AsCurrency;
    end;
    if MasterQBiayaLain.AsCurrency<>NULL then
    begin
        bl:=MasterQBiayaLain.AsCurrency;
    end;
    jumlah:=h+p+e+pe+c+bl;
    terbilang:=Spell(currtostr(jumlah))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPenagihan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end

else
begin
    if MessageDlg('Cetak Kwitansi Penagihan untuk SO '+MasterQSalesOrder.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
    if MasterQNominalKwitansiPembayaranAwal.AsCurrency<>NULL then
    begin
       h:=MasterQNominalKwitansiPembayaranAwal.AsCurrency;
    end
    else
    begin
      if MasterQHarga.AsCurrency<>NULL then
      begin
       h:=MasterQHarga.AsCurrency;
      end;
    end;
    if MasterQPPN.AsCurrency<>NULL then
    begin
        p:=MasterQPPN.AsCurrency;
    end;
    if MasterQBiayaExtend.AsCurrency<>NULL then
    begin
        e:=MasterQBiayaExtend.AsCurrency;
    end;
    if MasterQPPNExtend.AsCurrency<>NULL then
    begin
        pe:=MasterQPPNExtend.AsCurrency;
    end;
    if MasterQClaim.AsCurrency<>NULL then
    begin
        c:=MasterQClaim.AsCurrency;
    end;
    if MasterQBiayaLain.AsCurrency<>NULL then
    begin
        bl:=MasterQBiayaLain.AsCurrency;
    end;
    jumlah:=h+p+e+pe+c+bl;
    terbilang:=Spell(currtostr(jumlah))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPenagihan2.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end;
end;

end.






