object PenagihanAnjemFm: TPenagihanAnjemFm
  Left = 269
  Top = 136
  Width = 928
  Height = 656
  Caption = 'PenagihanAnjemFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 912
    Height = 617
    Align = alClient
    TabOrder = 0
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 42
      Width = 448
      Height = 282
      Align = alLeft
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 198
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1Kontrak: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1KontrakEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Kontrak'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NamaPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPelanggan'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1AlamatPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'AlamatPelanggan'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1NoTelpPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoTelpPelanggan'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1Dari: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Dari'
        Properties.Options.Editing = False
        ID = 4
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1Ke: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Ke'
        Properties.Options.Editing = False
        ID = 5
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1Harga: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Harga'
        Properties.Options.Editing = False
        ID = 6
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1BiayaLain: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.OnValidate = cxDBVerticalGrid1BiayaLainEditPropertiesValidate
        Properties.DataBinding.FieldName = 'BiayaLain'
        ID = 7
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1KetBiayaLain: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KetBiayaLain'
        ID = 8
        ParentID = 7
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1Claim: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.EditProperties.OnValidate = cxDBVerticalGrid1ClaimEditPropertiesValidate
        Properties.DataBinding.FieldName = 'Claim'
        ID = 9
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1ketClaim: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'ketClaim'
        ID = 10
        ParentID = 9
        Index = 0
        Version = 1
      end
    end
    object cxDBVerticalGrid2: TcxDBVerticalGrid
      Left = 449
      Top = 42
      Width = 462
      Height = 282
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 197
      OptionsBehavior.GoToNextCellOnTab = True
      ParentFont = False
      TabOrder = 1
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid2Terbayar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Terbayar'
        Properties.Options.Editing = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid2TglDibayar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TglDibayar'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid2CaraPembayaran: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = 'CASH'
            Value = 'CASH'
          end
          item
            Caption = 'EDC'
            Value = 'EDC'
          end
          item
            Caption = 'TRANSFER'
            Value = 'TRANSFER'
          end>
        Properties.DataBinding.FieldName = 'CaraPembayaran'
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid2KetCaraPembayaran: TcxDBEditorRow
        Height = 52
        Properties.DataBinding.FieldName = 'KetCaraPembayaran'
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid2PembayaranBulan: TcxDBEditorRow
        Properties.Caption = 'UntukPembayaranBulan'
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Items.Strings = (
          'JANUARI'
          'FEBRUARI'
          'MARET'
          'APRIL'
          'MEI'
          'JUNI'
          'JULI'
          'AGUSTUS'
          'SEPTEMBER'
          'OKTOBER'
          'NOVEMBER'
          'DESEMBER')
        Properties.DataBinding.FieldName = 'PembayaranBulan'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid2NoKwitansi: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2NoKwitansiEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'NoKwitansi'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid2NominalKwitansiPenagihan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NominalKwitansiPenagihan'
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid2PenerimaPembayaran: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = cxDBVerticalGrid2PenerimaPembayaranEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'PenerimaPembayaran'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object cxDBVerticalGrid2NamaPenerima: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPenerima'
        Properties.Options.Editing = False
        ID = 8
        ParentID = 7
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid2JabatanPenerima: TcxDBEditorRow
        Height = 17
        Properties.DataBinding.FieldName = 'JabatanPenerima'
        Properties.Options.Editing = False
        ID = 9
        ParentID = 7
        Index = 1
        Version = 1
      end
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 324
      Width = 910
      Height = 208
      Align = alBottom
      TabOrder = 2
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
        end
        object cxGrid1DBTableView1Kontrak: TcxGridDBColumn
          DataBinding.FieldName = 'Kontrak'
          Width = 71
        end
        object cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn
          DataBinding.FieldName = 'NamaPelanggan'
          Width = 175
        end
        object cxGrid1DBTableView1Terbayar: TcxGridDBColumn
          DataBinding.FieldName = 'Terbayar'
          Width = 103
        end
        object cxGrid1DBTableView1TglDibayar: TcxGridDBColumn
          DataBinding.FieldName = 'TglDibayar'
          Width = 123
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = 'UntukPembayaranBulan'
          DataBinding.FieldName = 'PembayaranBulan'
          Width = 130
        end
        object cxGrid1DBTableView1NoKwitansi: TcxGridDBColumn
          DataBinding.FieldName = 'NoKwitansi'
          Width = 159
        end
        object cxGrid1DBTableView1PenerimaPembayaran: TcxGridDBColumn
          DataBinding.FieldName = 'PenerimaPembayaran'
          Width = 216
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 532
      Width = 910
      Height = 65
      Align = alBottom
      TabOrder = 3
      object BtnSave: TcxButton
        Left = 8
        Top = 10
        Width = 75
        Height = 25
        Caption = 'SAVE'
        TabOrder = 0
        OnClick = BtnSaveClick
      end
      object ExitBtn: TcxButton
        Left = 92
        Top = 10
        Width = 75
        Height = 25
        Caption = 'EXIT'
        TabOrder = 1
        OnClick = ExitBtnClick
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 597
      Width = 910
      Height = 19
      Panels = <
        item
          Width = 50
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 910
      Height = 41
      Align = alTop
      TabOrder = 5
      object lbl1: TLabel
        Left = 11
        Top = 14
        Width = 24
        Height = 13
        Caption = 'Kode'
      end
      object KodeEdit: TcxButtonEdit
        Left = 40
        Top = 12
        AutoSize = False
        Properties.Buttons = <
          item
            Caption = '+'
            Default = True
            Kind = bkText
          end>
        Properties.OnButtonClick = KodeEditPropertiesButtonClick
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebs3D
        Style.HotTrack = False
        Style.ButtonStyle = bts3D
        TabOrder = 0
        OnEnter = KodeEditEnter
        OnExit = KodeEditExit
        OnKeyDown = KodeEditKeyDown
        Height = 21
        Width = 121
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from PenagihanAnjem')
    UpdateObject = MasterUpdate
    Left = 176
    Top = 8
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Required = True
      Size = 10
    end
    object MasterQBiayaLain: TCurrencyField
      FieldName = 'BiayaLain'
    end
    object MasterQKetBiayaLain: TStringField
      FieldName = 'KetBiayaLain'
      Size = 50
    end
    object MasterQClaim: TCurrencyField
      FieldName = 'Claim'
    end
    object MasterQketClaim: TStringField
      FieldName = 'ketClaim'
      Size = 50
    end
    object MasterQTerbayar: TCurrencyField
      FieldName = 'Terbayar'
    end
    object MasterQTglDibayar: TDateTimeField
      FieldName = 'TglDibayar'
    end
    object MasterQCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
      Size = 50
    end
    object MasterQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object MasterQNominalKwitansiPenagihan: TCurrencyField
      FieldName = 'NominalKwitansiPenagihan'
    end
    object MasterQPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Pelanggan'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'KodePelanggan'
      Size = 100
      Lookup = True
    end
    object MasterQNoTelpPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NoTelpPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Rute'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object MasterQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQKe: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenerimaPembayaran'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PenerimaPembayaran'
      Size = 50
      Lookup = True
    end
    object MasterQHarga: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'Harga'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Harga'
      KeyFields = 'Kontrak'
      Lookup = True
    end
    object MasterQKetCaraPembayaran: TStringField
      FieldName = 'KetCaraPembayaran'
      Size = 50
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object MasterQPembayaranBulan: TStringField
      FieldName = 'PembayaranBulan'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 208
    Top = 8
  end
  object MasterUpdate: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, BiayaLain, KetBiayaLain, Claim, ketClaim, ' +
        'Terbayar, TglDibayar, CaraPembayaran, KetCaraPembayaran, NoKwita' +
        'nsi, NominalKwitansiPenagihan, PenerimaPembayaran, PembayaranBul' +
        'an, Keterangan, CreateDate, CreateBy, Operator, TglEntry'
      'from PenagihanAnjem'
      'where'
      '  Kode = :OLD_Kode and'
      '  Kontrak = :OLD_Kontrak')
    ModifySQL.Strings = (
      'update PenagihanAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  BiayaLain = :BiayaLain,'
      '  KetBiayaLain = :KetBiayaLain,'
      '  Claim = :Claim,'
      '  ketClaim = :ketClaim,'
      '  Terbayar = :Terbayar,'
      '  TglDibayar = :TglDibayar,'
      '  CaraPembayaran = :CaraPembayaran,'
      '  KetCaraPembayaran = :KetCaraPembayaran,'
      '  NoKwitansi = :NoKwitansi,'
      '  NominalKwitansiPenagihan = :NominalKwitansiPenagihan,'
      '  PenerimaPembayaran = :PenerimaPembayaran,'
      '  PembayaranBulan = :PembayaranBulan,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode and'
      '  Kontrak = :OLD_Kontrak')
    InsertSQL.Strings = (
      'insert into PenagihanAnjem'
      
        '  (Kode, Kontrak, BiayaLain, KetBiayaLain, Claim, ketClaim, Terb' +
        'ayar, TglDibayar, CaraPembayaran, KetCaraPembayaran, NoKwitansi,' +
        ' NominalKwitansiPenagihan, PenerimaPembayaran, PembayaranBulan, ' +
        'Keterangan, CreateDate, CreateBy, Operator, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :BiayaLain, :KetBiayaLain, :Claim, :ketClaim' +
        ', :Terbayar, :TglDibayar, :CaraPembayaran, :KetCaraPembayaran, :' +
        'NoKwitansi, :NominalKwitansiPenagihan, :PenerimaPembayaran, :Pem' +
        'bayaranBulan, :Keterangan, :CreateDate, :CreateBy, :Operator, :T' +
        'glEntry)')
    DeleteSQL.Strings = (
      'delete from PenagihanAnjem'
      'where'
      '  Kode = :OLD_Kode and'
      '  Kontrak = :OLD_Kontrak')
    Left = 248
    Top = 8
  end
  object viewPenagihanAnjem: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from PenagihanAnjem')
    Left = 568
    Top = 584
    object viewPenagihanAnjemKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewPenagihanAnjemKontrak: TStringField
      FieldName = 'Kontrak'
      Required = True
      Size = 10
    end
    object viewPenagihanAnjemBiayaLain: TCurrencyField
      FieldName = 'BiayaLain'
    end
    object viewPenagihanAnjemKetBiayaLain: TStringField
      FieldName = 'KetBiayaLain'
      Size = 50
    end
    object viewPenagihanAnjemClaim: TCurrencyField
      FieldName = 'Claim'
    end
    object viewPenagihanAnjemketClaim: TStringField
      FieldName = 'ketClaim'
      Size = 50
    end
    object viewPenagihanAnjemTerbayar: TCurrencyField
      FieldName = 'Terbayar'
    end
    object viewPenagihanAnjemTglDibayar: TDateTimeField
      FieldName = 'TglDibayar'
    end
    object viewPenagihanAnjemCaraPembayaran: TStringField
      FieldName = 'CaraPembayaran'
      Size = 50
    end
    object viewPenagihanAnjemNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object viewPenagihanAnjemNominalKwitansiPenagihan: TCurrencyField
      FieldName = 'NominalKwitansiPenagihan'
    end
    object viewPenagihanAnjemPenerimaPembayaran: TStringField
      FieldName = 'PenerimaPembayaran'
      Size = 10
    end
    object viewPenagihanAnjemCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewPenagihanAnjemCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewPenagihanAnjemOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewPenagihanAnjemTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewPenagihanAnjemKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Pelanggan'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object viewPenagihanAnjemNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object viewPenagihanAnjemKetCaraPembayaran: TStringField
      FieldName = 'KetCaraPembayaran'
      Size = 50
    end
    object viewPenagihanAnjemKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object viewPenagihanAnjemPembayaranBulan: TStringField
      FieldName = 'PembayaranBulan'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = viewPenagihanAnjem
    Left = 616
    Top = 568
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pelanggan')
    Left = 344
    Top = 8
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Kontrak')
    Left = 376
    Top = 8
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Rute')
    Left = 408
    Top = 8
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 440
    Top = 8
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from PenagihanAnjem order by kode desc')
    Left = 280
    Top = 8
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 2
    Version.Windows.Build = '9200'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 488
    Top = 8
  end
end
