unit PenagihanAnjem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxInplaceContainer, cxVGrid, cxDBVGrid,
  StdCtrls, cxTextEdit, cxMaskEdit, cxButtonEdit, ExtCtrls, Menus,
  cxButtons, SDEngine, cxCurrencyEdit, cxRadioGroup, UCrpeClasses, UCrpe32,
  cxDropDownEdit;

type
  TPenagihanAnjemFm = class(TForm)
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel2: TPanel;
    StatusBar: TStatusBar;
    BtnSave: TcxButton;
    ExitBtn: TcxButton;
    MasterQ: TSDQuery;
    DataSource1: TDataSource;
    MasterUpdate: TSDUpdateSQL;
    MasterQKode: TStringField;
    MasterQKontrak: TStringField;
    MasterQBiayaLain: TCurrencyField;
    MasterQKetBiayaLain: TStringField;
    MasterQClaim: TCurrencyField;
    MasterQketClaim: TStringField;
    MasterQTerbayar: TCurrencyField;
    MasterQTglDibayar: TDateTimeField;
    MasterQCaraPembayaran: TStringField;
    MasterQNoKwitansi: TStringField;
    MasterQNominalKwitansiPenagihan: TCurrencyField;
    MasterQPenerimaPembayaran: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    viewPenagihanAnjem: TSDQuery;
    DataSource2: TDataSource;
    viewPenagihanAnjemKode: TStringField;
    viewPenagihanAnjemKontrak: TStringField;
    viewPenagihanAnjemBiayaLain: TCurrencyField;
    viewPenagihanAnjemKetBiayaLain: TStringField;
    viewPenagihanAnjemClaim: TCurrencyField;
    viewPenagihanAnjemketClaim: TStringField;
    viewPenagihanAnjemTerbayar: TCurrencyField;
    viewPenagihanAnjemTglDibayar: TDateTimeField;
    viewPenagihanAnjemCaraPembayaran: TStringField;
    viewPenagihanAnjemNoKwitansi: TStringField;
    viewPenagihanAnjemNominalKwitansiPenagihan: TCurrencyField;
    viewPenagihanAnjemPenerimaPembayaran: TStringField;
    viewPenagihanAnjemCreateDate: TDateTimeField;
    viewPenagihanAnjemCreateBy: TStringField;
    viewPenagihanAnjemOperator: TStringField;
    viewPenagihanAnjemTglEntry: TDateTimeField;
    cxDBVerticalGrid1Kontrak: TcxDBEditorRow;
    cxDBVerticalGrid1BiayaLain: TcxDBEditorRow;
    cxDBVerticalGrid1KetBiayaLain: TcxDBEditorRow;
    cxDBVerticalGrid1Claim: TcxDBEditorRow;
    cxDBVerticalGrid1ketClaim: TcxDBEditorRow;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    MasterQKodePelanggan: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQNoTelpPelanggan: TStringField;
    MasterQKodeRute: TStringField;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    MasterQDari: TStringField;
    MasterQKe: TStringField;
    cxDBVerticalGrid1NamaPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1AlamatPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1NoTelpPelanggan: TcxDBEditorRow;
    cxDBVerticalGrid1Dari: TcxDBEditorRow;
    cxDBVerticalGrid1Ke: TcxDBEditorRow;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    KodeQ: TSDQuery;
    MasterQHarga: TCurrencyField;
    cxDBVerticalGrid1Harga: TcxDBEditorRow;
    cxDBVerticalGrid2Terbayar: TcxDBEditorRow;
    cxDBVerticalGrid2TglDibayar: TcxDBEditorRow;
    cxDBVerticalGrid2CaraPembayaran: TcxDBEditorRow;
    cxDBVerticalGrid2NoKwitansi: TcxDBEditorRow;
    cxDBVerticalGrid2NominalKwitansiPenagihan: TcxDBEditorRow;
    cxDBVerticalGrid2PenerimaPembayaran: TcxDBEditorRow;
    KodeQkode: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Kontrak: TcxGridDBColumn;
    cxGrid1DBTableView1Terbayar: TcxGridDBColumn;
    cxGrid1DBTableView1TglDibayar: TcxGridDBColumn;
    cxGrid1DBTableView1NoKwitansi: TcxGridDBColumn;
    cxGrid1DBTableView1PenerimaPembayaran: TcxGridDBColumn;
    viewPenagihanAnjemKodePelanggan: TStringField;
    viewPenagihanAnjemNamaPelanggan: TStringField;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    Crpe1: TCrpe;
    cxDBVerticalGrid2NamaPenerima: TcxDBEditorRow;
    cxDBVerticalGrid2JabatanPenerima: TcxDBEditorRow;
    MasterQKetCaraPembayaran: TStringField;
    MasterQKeterangan: TStringField;
    cxDBVerticalGrid2KetCaraPembayaran: TcxDBEditorRow;
    viewPenagihanAnjemKetCaraPembayaran: TStringField;
    viewPenagihanAnjemKeterangan: TStringField;
    MasterQPembayaranBulan: TStringField;
    cxDBVerticalGrid2PembayaranBulan: TcxDBEditorRow;
    viewPenagihanAnjemPembayaranBulan: TStringField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Panel3: TPanel;
    lbl1: TLabel;
    KodeEdit: TcxButtonEdit;
    procedure KodeEditEnter(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BtnSaveClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid2PenerimaPembayaranEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1BiayaLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1ClaimEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid2NoKwitansiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PenagihanAnjemFm: TPenagihanAnjemFm;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM,StrUtils, KontrakDropDown, PegawaiDropDown;

{$R *.dfm}

procedure TPenagihanAnjemFm.KodeEditEnter(Sender: TObject);
begin
KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  cxDBVerticalGrid1.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  BtnSave.Enabled:=false;
end;

procedure TPenagihanAnjemFm.KodeEditExit(Sender: TObject);
begin
if KodeEdit.Text<>'' then
  begin
   MasterQ.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;

    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //DMFm.GetDateQ.Open;
      //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;

      //DMFm.GetDateQ.Close;
      btnsave.Enabled:=menuutamafm.UserQInsertLPB.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    BtnSave.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
  end;
end;

procedure TPenagihanAnjemFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      cxDBVerticalGrid1.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

function PartialSpell( strInput : String) : String;
var strSebutan, strAngka, strHasil : String;
    sTAngka : char;
    i,sAngka : Byte;
begin
  For i := length(strInput) DownTo 1 do
   begin
    //--ambil satu digit
    sTAngka := strInput[i];
    if not (sTAngka  in ['0'..'9']) then continue;
    sAngka := strToInt(copy(strInput, i, 1));
    //--ubah angka menjadi huruf yang sesuai, kecuali angka nol
    Case sAngka of
       1: strAngka := 'satu ';
       2: strAngka := 'dua ';
       3: strAngka := 'tiga ';
       4: strAngka := 'empat ';
       5: strAngka := 'lima ';
       6: strAngka := 'enam ';
       7: strAngka := 'tujuh ';
       8: strAngka := 'delapan ';
       9: strAngka := 'sembilan ';
       0: begin
             strAngka := '';
             strSebutan := '';
          end;
    End;
    strSebutan := '';
    //--cek kondisi khusus untuk angka 1 yang bisa berubah jad 'se'
    If (sAngka = 1) And (i <> length(strInput)) Then strAngka := 'se';
    //--tambahkan satuan yang sesuai yaitu puluh, belas, ratus
    If ((length(strInput) - i) = 1) And (sAngka <> 0) Then
     begin
      strSebutan := 'puluh ';
      If (sAngka = 1) And (strHasil <> '') Then
       begin
        strSebutan := 'belas ';
        If strHasil = 'satu ' Then strAngka := 'se'
          Else strAngka := strHasil;
        strHasil := '';
       end
     End
     else if ((length(strInput) - i) = 2) And (sAngka <> 0) Then
         strSebutan := 'ratus ';
    strHasil := strAngka + strSebutan + strHasil
  end;
  PartialSpell := strHasil
end;

function Spell(strInput : String) : String ;
var i : integer;
    j : Byte;
  strPars, strOlah, strSebutan, strHasil, strSpell : String;
begin
  if length(strInput)>15 then raise Exception.Create('Nilai maksimal 999.999.999.999.999');
  strInput:=Copy(strInput,1,15);
  strPars := strInput;
  i := length(strInput);
  repeat
    //--mengambil angka 3 digit dimulai dari belakang
    j := length(strPars) Mod 3;
    If j <> 0 Then
    begin
      strOlah := copy(strPars, 1, j);
      strPars := copy(strPars, j + 1, length(strPars) - j)
    end
    Else
    begin
      strOlah := copy(strPars, 1, 3);
      strPars := copy(strPars,4,length(strPars)-3);
    End;
    //--membilang 3 digit angka yang ada
    strSpell := PartialSpell(strOlah);
    //--menambahkan satuan yang sesuai, misalnya : ribu, juta, milyar, trilyun
    If strSpell <> '' Then
      If i > 12 Then
        strSebutan := 'trilyun '
       else if i > 9 Then
         strSebutan := 'milyar '
        else if i > 6 Then
          strSebutan := 'juta '
         else if i > 3 Then
          begin
           strSebutan := 'ribu ';
           If strSpell = 'satu ' Then strSpell := 'se'
          end;
    strHasil := strHasil + strSpell + strSebutan;
    strSpell := '';
    strSebutan := '';
    dec(i,3);
  until i<1;
  if Length(StrHasil)>0 then StrHasil[1]:=UpCase(StrHasil[1]);
  Spell := strHasil;
end;


procedure TPenagihanAnjemFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
end;

procedure TPenagihanAnjemFm.BtnSaveClick(Sender: TObject);
begin
  MenuUtamaFm.Database1.StartTransaction;
  try
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Sukses');
    //kodeprint:=MasterQKode.asstring;
   { if MessageDlg('Cetak Bon BBM' + ', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'BonBBM.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kodeprint;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
    end;   }
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  masterq.Close;
  masterq.SQL.Clear;
  masterq.SQL.Text:=MasterOriSQL;
  masterq.Open;
  masterq.Close;
  KodeEdit.SetFocus;
  //masterq.Append;
  viewPenagihanAnjem.close;
  viewPenagihanAnjem.Open;
  //masterq.ClearFields;
end;

procedure TPenagihanAnjemFm.ExitBtnClick(Sender: TObject);
begin
release;
end;

procedure TPenagihanAnjemFm.cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var a:string;
begin
{KontrakQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  KontrakQ.ExecSQL;
  KontrakQ.Open; }
  a:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"';
  KontrakDropDownFm:=TKontrakDropdownfm.Create(Self,a);
    if KontrakDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKontrak.AsString:=KontrakDropDownFm.kode;;
      //MasterVGrid.SetFocus;
    end;
    KontrakDropDownFm.Release;
    MasterQTerbayar.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaLain.AsCurrency+MasterQClaim.AsCurrency;

end;

procedure TPenagihanAnjemFm.cxDBVerticalGrid2PenerimaPembayaranEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
 //PegawaiQ.ParamByName('text').AsString:='';
  PegawaiQ.ExecSQL;
  PegawaiQ.Open; }
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerimaPembayaran.AsString:=PegawaiDropDownFm.kode;
      //MasterVGrid.SetFocus;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TPenagihanAnjemFm.cxDBVerticalGrid1BiayaLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaLain.AsCurrency:=DisplayValue;
MasterQTerbayar.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaLain.AsCurrency+MasterQClaim.AsCurrency;

end;

procedure TPenagihanAnjemFm.cxDBVerticalGrid1ClaimEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQClaim.AsCurrency:=DisplayValue;
MasterQTerbayar.AsCurrency:=MasterQHarga.AsCurrency+MasterQBiayaLain.AsCurrency+MasterQClaim.AsCurrency;

end;

procedure TPenagihanAnjemFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPenagihanAnjemFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPenagihanAnjemFm.FormShow(Sender: TObject);
begin
KodeEdit.SetFocus;
  viewPenagihanAnjem.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertKasBon.AsBoolean;
  BtnSave.Enabled:=menuutamafm.UserQUpdateKasBon.AsBoolean;
  //DeleteBtn.Enabled:=menuutamafm.UserQDeleteKasBon.AsBoolean;
end;

procedure TPenagihanAnjemFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
cxDBVerticalGrid1.Enabled:=true;
    //cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewPenagihanAnjemKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      KodeEdit.Text:= viewPenagihanAnjemKode.AsString;
    end;
    BtnSave.Enabled:=True;
   cxDBVerticalGrid1.Enabled:=true;
    //MasterQ.SQL.Clear;
    //MasterQ.SQL.Add(MasterOriSQL);
  //  akhirnya:='';
end;

procedure TPenagihanAnjemFm.cxDBVerticalGrid2NoKwitansiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
      var terbilang:string;
  var h,c,bl,jumlah:currency;
  begin
if AButtonIndex=0 then
begin
//cetak SJ
    if MessageDlg('Cetak Kwitansi Penagihan Anjem untuk kode '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
     if MasterQHarga.AsCurrency<>NULL then
    begin
       h:=MasterQHarga.AsCurrency;
    end;
    if MasterQClaim.AsCurrency<>NULL then
    begin
        c:=MasterQClaim.AsCurrency;
    end;
    if MasterQBiayaLain.AsCurrency<>NULL then
    begin
        bl:=MasterQBiayaLain.AsCurrency;
    end;
    jumlah:=h+c+bl;
    terbilang:=Spell(currtostr(jumlah))+' rupiah';
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KuitansiPenagihanAnjem.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
      Crpe1.ParamFields[1].CurrentValue:=terbilang;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
    {  updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close; }
    end;
end;

end;

end.
