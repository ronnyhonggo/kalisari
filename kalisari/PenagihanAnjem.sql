/*
   11 April 201316:55:57
   User: sa
   Server: TOSHIBA-PC\SQLEXPRESS
   Database: Kalisari
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Penagihan
	DROP CONSTRAINT FK_penagihan_MasterSO
GO
ALTER TABLE dbo.MasterSO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MasterSO', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PenagihanAnjem
	DROP CONSTRAINT FK_PenagihanAnjem_PenerimaPembayaran
GO
ALTER TABLE dbo.Penagihan
	DROP CONSTRAINT FK_Penagihan_Pegawai
GO
ALTER TABLE dbo.Pegawai SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PenagihanAnjem
	DROP CONSTRAINT FK_PenagihanAnjem_Kontrak
GO
ALTER TABLE dbo.Kontrak SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Kontrak', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PenagihanAnjem
	DROP CONSTRAINT FK_PenagihanAnjem_CreateBy
GO
ALTER TABLE dbo.PenagihanAnjem
	DROP CONSTRAINT FK_PenagihanAnjem_Operator
GO
ALTER TABLE dbo.Penagihan
	DROP CONSTRAINT FK_penagihan_User
GO
ALTER TABLE dbo.Penagihan
	DROP CONSTRAINT FK_penagihan_User1
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[User]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Penagihan
	(
	Kode varchar(10) NOT NULL,
	SalesOrder varchar(10) NOT NULL,
	BiayaLain money NULL,
	KetBiayaLain varchar(MAX) NULL,
	Claim money NULL,
	ketClaim varchar(MAX) NULL,
	Terbayar money NULL,
	TglDibayar datetime NULL,
	CaraPembayaran varchar(50) NULL,
	NoKwitansi varchar(50) NULL,
	NominalKwitansiPenagihan money NULL,
	PenerimaPembayaran varchar(10) NULL,
	CreateDate datetime NULL,
	CreateBy varchar(10) NULL,
	Operator varchar(10) NULL,
	TglEntry datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Penagihan SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Penagihan)
	 EXEC('INSERT INTO dbo.Tmp_Penagihan (Kode, SalesOrder, BiayaLain, KetBiayaLain, Claim, ketClaim, Terbayar, TglDibayar, CaraPembayaran, NoKwitansi, NominalKwitansiPenagihan, PenerimaPembayaran, CreateDate, CreateBy, Operator, TglEntry)
		SELECT Kode, SalesOrder, BiayaLain, KetBiayaLain, Claim, ketClaim, Terbayar, TglDibayar, CaraPembayaran, NoKwitansi, NominalKwitansiPenagihan, PenerimaPembayaran, CreateDate, CreateBy, Operator, TglEntry FROM dbo.Penagihan WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Penagihan
GO
EXECUTE sp_rename N'dbo.Tmp_Penagihan', N'Penagihan', 'OBJECT' 
GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	PK_Penagihan PRIMARY KEY CLUSTERED 
	(
	Kode,
	SalesOrder
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	FK_Penagihan_Pegawai FOREIGN KEY
	(
	PenerimaPembayaran
	) REFERENCES dbo.Pegawai
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	FK_penagihan_User FOREIGN KEY
	(
	CreateBy
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	FK_penagihan_User1 FOREIGN KEY
	(
	Operator
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Penagihan ADD CONSTRAINT
	FK_penagihan_MasterSO FOREIGN KEY
	(
	SalesOrder
	) REFERENCES dbo.MasterSO
	(
	Kodenota
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE TRIGGER [dbo].[PENAGIHAN_DELETE] ON dbo.Penagihan
for DELETE
AS
BEGIN
	declare @NoSO varchar(10);
	select @NoSO=i.SalesOrder from deleted i;
	update MasterSO set StatusPembayaran='BELUM LUNAS' where MasterSO.kodenota=@NoSO;
END
GO
CREATE TRIGGER [dbo].[PENAGIHAN_UPDATE] ON dbo.Penagihan
for UPDATE
AS
BEGIN
	update PENAGIHAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
CREATE TRIGGER [dbo].[PENAGIHAN_INSERT] ON dbo.Penagihan
for INSERT
AS
BEGIN
	update PENAGIHAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
	declare @NoSO varchar(10);
	select @NoSO=i.SalesOrder from inserted i;
	update MasterSO set StatusPembayaran='LUNAS' where MasterSO.kodenota=@NoSO;
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Penagihan', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_PenagihanAnjem
	(
	Kode varchar(10) NOT NULL,
	Kontrak varchar(10) NOT NULL,
	BiayaLain money NULL,
	KetBiayaLain varchar(50) NULL,
	Claim money NULL,
	ketClaim varchar(50) NULL,
	Terbayar money NULL,
	TglDibayar datetime NULL,
	CaraPembayaran varchar(50) NULL,
	NoKwitansi varchar(50) NULL,
	NominalKwitansiPenagihan money NULL,
	PenerimaPembayaran varchar(10) NULL,
	Keterangan varchar(50) NULL,
	CreateDate datetime NULL,
	CreateBy varchar(10) NULL,
	Operator varchar(10) NULL,
	TglEntry datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_PenagihanAnjem SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.PenagihanAnjem)
	 EXEC('INSERT INTO dbo.Tmp_PenagihanAnjem (Kode, Kontrak, BiayaLain, KetBiayaLain, Claim, ketClaim, Terbayar, TglDibayar, CaraPembayaran, NoKwitansi, NominalKwitansiPenagihan, PenerimaPembayaran, CreateDate, CreateBy, Operator, TglEntry)
		SELECT Kode, Kontrak, BiayaLain, KetBiayaLain, Claim, ketClaim, Terbayar, TglDibayar, CaraPembayaran, NoKwitansi, NominalKwitansiPenagihan, PenerimaPembayaran, CreateDate, CreateBy, Operator, TglEntry FROM dbo.PenagihanAnjem WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.PenagihanAnjem
GO
EXECUTE sp_rename N'dbo.Tmp_PenagihanAnjem', N'PenagihanAnjem', 'OBJECT' 
GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	PK_PenagihanAnjem PRIMARY KEY CLUSTERED 
	(
	Kode,
	Kontrak
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	FK_PenagihanAnjem_CreateBy FOREIGN KEY
	(
	CreateBy
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	FK_PenagihanAnjem_Kontrak FOREIGN KEY
	(
	Kontrak
	) REFERENCES dbo.Kontrak
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	FK_PenagihanAnjem_Operator FOREIGN KEY
	(
	Operator
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PenagihanAnjem ADD CONSTRAINT
	FK_PenagihanAnjem_PenerimaPembayaran FOREIGN KEY
	(
	PenerimaPembayaran
	) REFERENCES dbo.Pegawai
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PenagihanAnjem', 'Object', 'CONTROL') as Contr_Per 