unit PenjualanSparePart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox,
  cxTimeEdit;

type
  TPenjualanSparePartFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    BarangQ: TSDQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ViewPSQ: TSDQuery;
    Crpe1: TCrpe;
    KodeQkode: TStringField;
    ViewDs: TDataSource;
    cxButton1: TcxButton;
    HargaLastQ: TSDQuery;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQKode: TStringField;
    MasterQBarang: TStringField;
    MasterQHargaLast: TCurrencyField;
    MasterQPembeli: TMemoField;
    MasterQHargaJual: TCurrencyField;
    MasterQJumlah: TFloatField;
    MasterQNamaBarang: TStringField;
    MasterQStok: TFloatField;
    MasterQSatuan: TStringField;
    MasterVGridBarang: TcxDBEditorRow;
    MasterVGridHargaLast: TcxDBEditorRow;
    MasterVGridPembeli: TcxDBEditorRow;
    MasterVGridHargaJual: TcxDBEditorRow;
    MasterVGridJumlah: TcxDBEditorRow;
    MasterVGridNamaBarang: TcxDBEditorRow;
    MasterVGridStok: TcxDBEditorRow;
    MasterVGridSatuan: TcxDBEditorRow;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    MasterQCreateBy: TStringField;
    MasterQCreateDate: TDateTimeField;
    HargaLastQHargaLast: TCurrencyField;
    ViewPSQKode: TStringField;
    ViewPSQBarang: TStringField;
    ViewPSQHargaLast: TCurrencyField;
    ViewPSQPembeli: TMemoField;
    ViewPSQHargaJual: TCurrencyField;
    ViewPSQJumlah: TFloatField;
    ViewPSQCreateBy: TStringField;
    ViewPSQCreateDate: TDateTimeField;
    ViewPSQNamaBarang: TStringField;
    ViewPSQSatuan: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1HargaLast: TcxGridDBColumn;
    cxGrid1DBTableView1Pembeli: TcxGridDBColumn;
    cxGrid1DBTableView1HargaJual: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    MasterQTanggal: TDateTimeField;
    MasterVGridTanggal: TcxDBEditorRow;
    ViewPSQTanggal: TDateTimeField;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PenjualanSparePartFm: TPenjualanSparePartFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, ArmadaDropDown,
  PegawaiDropDown, LookupRekanan, BarangDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPenjualanSparePartFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPenjualanSparePartFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPenjualanSparePartFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPenjualanSparePartFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPenjualanSparePartFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
//  MasterQTanggal.Value:=Trunc(Now());
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TPenjualanSparePartFm.KodeEditEnter(Sender: TObject);
begin
  //buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  cxButton1.Enabled:=false;
  MasterQ.Close;
end;

procedure TPenjualanSparePartFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewPSQ.Close;
  ViewPSQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewPSQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewPSQ.Open;
  //SaveBtn.Enabled:=true;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPenjualanSpr.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePenjualanSpr.AsBoolean;
  cxButton1.Enabled:=menuutamafm.UserQDeletePenjualanSpr.AsBoolean;
end;

procedure TPenjualanSparePartFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxButton1.Enabled:=menuutamafm.UserQDeletePenjualanSpr.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePenjualanSpr.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TPenjualanSparePartFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPenjualanSparePartFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Penjualan Sparepart dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    ViewPSQ.Close;
    ViewPSQ.Open;
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TPenjualanSparePartFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TPenjualanSparePartFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
//  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TPenjualanSparePartFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Surat Jalan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Surat Jalan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TPenjualanSparePartFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPenjualanSparePartFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    //buttonCetak.Enabled:=true;
    kodeedit.Text:=ViewPSQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePenjualanSpr.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

procedure TPenjualanSparePartFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus Penjualan Sparepart '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Penjualan Sparepart telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Penjualan Sparepart pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewPSQ.Refresh;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TPenjualanSparePartFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPenjualanSparePartFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  viewPSQ.Close;
  viewPSQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewPSQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewPSQ.Open;
end;

procedure TPenjualanSparePartFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  viewPSQ.Close;
  viewPSQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewPSQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewPSQ.Open;
end;

procedure TPenjualanSparePartFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarang.AsString:= BarangDropDownFm.kode;
    BarangQ.Close;
    BarangQ.ParamByName('text').AsString:=MasterQBarang.AsString;
    BarangQ.Open;
    MasterQNamaBarang.AsString:=BarangQNama.AsString;
    MasterQStok.AsFloat:=BarangQJumlah.AsFloat;
    MasterQSatuan.AsString:=BarangQSatuan.AsString;
    HargaLastQ.Close;
    HargaLastQ.ParamByName('text').AsString:=MasterQBarang.AsString;
    HargaLastQ.Open;
    MasterQHargaLast.AsCurrency:=HargaLastQHargaLast.AsCurrency;
  end;
  BarangDropDownFm.Release;
end;

end.
