unit PerbaikanBan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox;

type
  TPerbaikanBanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    PegawaiQ: TSDQuery;
    Crpe1: TCrpe;
    updateQ: TSDQuery;
    StringField1: TStringField;
    MemoField1: TMemoField;
    MemoField2: TMemoField;
    IntegerField1: TIntegerField;
    buttonCetak: TcxButton;
    SOQNOSO: TStringField;
    SOQNAMA_PELANGGAN: TStringField;
    SOQALAMAT_PELANGGAN: TStringField;
    SOQTLP_PELANGGAN: TStringField;
    SOQNAMA_SOPIR: TStringField;
    SOQEKOR_ARMADA: TStringField;
    SOQTGL_ORDER: TDateTimeField;
    SOQKODE_ARMADA: TStringField;
    SOQPLAT_ARMADA: TStringField;
    SOQKODESOPIR: TStringField;
    SOQKODE_RUTE: TStringField;
    SOQMUAT: TStringField;
    SOQBONGKAR: TStringField;
    SOQDARIPT: TMemoField;
    SOQDARIALAMAT: TMemoField;
    SOQKEPT: TMemoField;
    SOQKEALAMAT: TMemoField;
    SOQCHECKER: TStringField;
    KodeQkode: TStringField;
    banQ: TSDQuery;
    DeleteBtn: TcxButton;
    viewSJQKode: TStringField;
    viewSJQBan: TStringField;
    viewSJQPIC: TStringField;
    viewSJQTindakan: TMemoField;
    viewSJQPemeriksa: TStringField;
    viewSJQTglPeriksa: TDateTimeField;
    viewSJQStatus: TStringField;
    viewSJQCreateBy: TStringField;
    viewSJQCreateDate: TDateTimeField;
    viewSJQOperator: TStringField;
    viewSJQTglEntry: TDateTimeField;
    viewSJQTglCetak: TDateTimeField;
    viewSJQStatusBan: TStringField;
    viewSJQKeluhan: TMemoField;
    viewSJQMerk: TStringField;
    viewSJQKodeIDBan: TStringField;
    viewSJQNamaPemeriksa: TStringField;
    viewSJQNamaPIC: TStringField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    MasterQKode: TStringField;
    MasterQBan: TStringField;
    MasterQPIC: TStringField;
    MasterQTindakan: TMemoField;
    MasterQPemeriksa: TStringField;
    MasterQTglPeriksa: TDateTimeField;
    MasterQStatus: TStringField;
    MasterQCreateBy: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQStatusBan: TStringField;
    MasterQKeluhan: TMemoField;
    MasterVGridBan: TcxDBEditorRow;
    MasterVGridPIC: TcxDBEditorRow;
    MasterVGridTindakan: TcxDBEditorRow;
    MasterVGridPemeriksa: TcxDBEditorRow;
    MasterVGridTglPeriksa: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridStatusBan: TcxDBEditorRow;
    MasterVGridKeluhan: TcxDBEditorRow;
    MasterQMerk: TStringField;
    MasterQKodeIDBan: TStringField;
    MasterQNamaPIC: TStringField;
    MasterQJabatanPIC: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    MasterQNamaPemeriksa: TStringField;
    MasterVGridDBEditorRow5: TcxDBEditorRow;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    banQKode: TStringField;
    banQKodeBarang: TStringField;
    banQKodeIDBan: TStringField;
    banQMerk: TStringField;
    banQJenis: TStringField;
    banQUkuranBan: TStringField;
    banQRing: TStringField;
    banQVelg: TStringField;
    banQStandardUmur: TIntegerField;
    banQStandardKm: TIntegerField;
    banQVulkanisir: TBooleanField;
    banQTanggalVulkanisir: TDateTimeField;
    banQKedalamanAlur: TIntegerField;
    banQStatus: TStringField;
    banQCreateDate: TDateTimeField;
    banQCreateBy: TStringField;
    banQOperator: TStringField;
    banQTglEntry: TDateTimeField;
    MasterQKedalamanAlur: TIntegerField;
    MasterQVulkanisir: TBooleanField;
    MasterQSupplier: TStringField;
    MasterQHarga: TCurrencyField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    MasterQNamaSupplier: TStringField;
    MasterVGridDBEditorRow6: TcxDBEditorRow;
    MasterVGridDBEditorRow7: TcxDBEditorRow;
    MasterVGridSupplier: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridNamaSupplier: TcxDBEditorRow;
    viewSJQKedalamanAlur: TIntegerField;
    viewSJQVulkanisir: TBooleanField;
    viewSJQSupplier: TStringField;
    viewSJQHarga: TCurrencyField;
    cxGrid1DBTableView1Column9: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQTglKembali: TDateTimeField;
    MasterVGridTglKembali: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridBanEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPICEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPemeriksaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDBEditorRow8EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDBEditorRow7EditPropertiesEditValueChanged(
      Sender: TObject);
    procedure MasterVGridStatusEditPropertiesChange(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PerbaikanBanFm: TPerbaikanBanFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, BanDropDown, PegawaiDropDown,
  SupplierDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPerbaikanBanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPerbaikanBanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPerbaikanBanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPerbaikanBanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPerbaikanBanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  banQ.Open;
  PegawaiQ.Open;
  SupplierQ.Open;
end;

procedure TPerbaikanBanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TPerbaikanBanFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPerbaikanBanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewSJQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPerbaikanBan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikanBan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeletePerbaikanBan.AsBoolean;
end;

procedure TPerbaikanBanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:='ON PROCESS';
      MasterQStatusBan.AsString:='PRIMA';
      MasterQVulkanisir.AsBoolean:=False;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeletePerbaikanBan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikanBan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TPerbaikanBanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPerbaikanBanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Perbaikan Ban dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  viewSJQ.Refresh;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TPerbaikanBanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TPerbaikanBanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  if (MasterQPemeriksa.AsString<>'') and (MasterQTglPeriksa.AsString<>'') then
  begin
    MasterQStatus.AsString:='FINISHED';
  end
  else
  begin
    MasterQStatus.AsString:='ON PROCESS';
  end;
  //MasterQStatusBan.AsString:='PERBAIKAN';
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TPerbaikanBanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Perbaikan Ban '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Perbaikan Ban telah dihapus.',mtInformation,[mbOK],0);
       viewSJQ.Close;
       viewSJQ.Open;
       KodeEdit.SetFocus;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Perbaikan Ban pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TPerbaikanBanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);

    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterQtgl_order.AsDateTime :=  SOQtgl_order.AsDateTime;
      //MasterQnama_pelanggan.AsString := SOQnama_pelanggan.AsString;
      //MasterQtlp_pelanggan.AsString :=    SOQtlp_pelanggan.AsString;
      //MasterQalamat_pelanggan.AsString :=    SOQalamat_pelanggan.asString;
      //MasterQkode_armada.AsString :=  SOQkode_armada.AsString;
      //MasterQekor.AsString :=  SOQekor_armada.AsString;
      //MasterQsopir.AsString :=  SOQkodesopir.AsString;
      //MasterQplat_armada.AsString := SOQplat_armada.AsString;
      //MasterQnama_sopir.AsString := SOQnama_sopir.AsString;
      //MasterQmuat.AsString := SOQmuat.AsString;
      //MasterQbongkar.AsString := SOQbongkar.AsString;
      //MasterQdaript.AsString := SOQdaript.AsString;
      //MasterQdarialamat.AsString := SOQdarialamat.AsString;
      //MasterQkept.AsString := SOQkept.AsString;
      //MasterQkealamat.AsString := SOQkealamat.AsString;
      //MasterQkode_rute.AsString := SOQkode_rute.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPerbaikanBanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    buttonCetak.Enabled:=true;
    kodeedit.Text:=ViewSJQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePerbaikanBan.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

procedure TPerbaikanBanFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PegawaiQ.Close;
    PegawaiQ.ExecSQL;
    PegawaiQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      //MasterQnama_sopir.AsString := sopirQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPerbaikanBanFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Perbaikan Ban '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'perbaikanban.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update perbaikanban set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TPerbaikanBanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQArmada.AsString:= ArmadaQKode.AsString;
      //MasterQjumlahban_armada.AsString:=ArmadaQJumlahBan.AsString;
      //MasterQplatno_armada.AsString:=ArmadaQPlatNo.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridBanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BanDropDownFm:=TBanDropDownFm.Create(self);
  if BanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBan.AsString:= BanDropDownFm.kode;
  end;
  BanDropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridPICEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPIC.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridPemeriksaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPemeriksa.AsString:=PegawaiDropDownFm.kode;
    end;
  PegawaiDropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridDBEditorRow8EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSupplier.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TPerbaikanBanFm.MasterVGridDBEditorRow7EditPropertiesEditValueChanged(
  Sender: TObject);
begin
  If MasterQVulkanisir.AsBoolean=true then
  begin
    MasterVGridSupplier.Visible:=true;
    MasterVGridNamaSupplier.Visible:=true;
    MasterVGridHarga.Visible:=true;
    MasterVGridTglKembali.Visible:=true;
  end
  else
  begin
    MasterVGridSupplier.Visible:=false;
    MasterVGridNamaSupplier.Visible:=false;
    MasterVGridHarga.Visible:=false;
    MasterVGridTglKembali.Visible:=false;
    MasterQ.Edit;
    MasterQSupplier.AsVariant:=null;
    MasterQHarga.AsVariant:=null;
    MasterQTglKembali.AsVariant:=null;
  end;
end;

procedure TPerbaikanBanFm.MasterVGridStatusEditPropertiesChange(
  Sender: TObject);
begin
  if MasterQStatus.Value='FINISHED' then
  begin
    MasterVGridPemeriksa.Visible:=True;
    MasterVGridTglPeriksa.Visible:=True;
    MasterVGridStatusBan.Visible:=True;
  end
  else
  begin
    MasterVGridPemeriksa.Visible:=False;
    MasterVGridTglPeriksa.Visible:=False;
    MasterVGridStatusBan.Visible:=False;
  end;
end;

procedure TPerbaikanBanFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSJQ.Open;
end;

procedure TPerbaikanBanFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewSJQ.Open;
end;

end.
