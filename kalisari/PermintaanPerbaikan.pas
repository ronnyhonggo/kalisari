unit PermintaanPerbaikan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox,
  cxTimeEdit;

type
  TPermintaanPerbaikanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PegawaiQ: TSDQuery;
    ArmadaQ: TSDQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewPPQ: TSDQuery;
    SJDS: TDataSource;
    sopirQ: TSDQuery;
    sopirQKode: TStringField;
    sopirQNama: TStringField;
    sopirQAlamat: TStringField;
    sopirQNotelp: TStringField;
    sopirQCreateDate: TDateTimeField;
    sopirQCreateBy: TStringField;
    sopirQOperator: TStringField;
    sopirQTglEntry: TDateTimeField;
    Crpe1: TCrpe;
    ekorQ: TSDQuery;
    ekorQkode: TStringField;
    ekorQpanjang: TStringField;
    ekorQberat: TStringField;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    jarakQJarak: TIntegerField;
    KodeQkode: TStringField;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridPeminta: TcxDBEditorRow;
    MasterVGridKeluhan: TcxDBEditorRow;
    MasterQKode: TStringField;
    MasterQArmada: TStringField;
    MasterQPeminta: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNoBody: TStringField;
    MasterQNamaPeminta: TStringField;
    MasterQJabatanPeminta: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    ViewDs: TDataSource;
    cxButton1: TcxButton;
    MasterQTanggal: TDateTimeField;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    viewPPQKode: TStringField;
    viewPPQTanggal: TDateTimeField;
    viewPPQArmada: TStringField;
    viewPPQPeminta: TStringField;
    viewPPQKeluhan: TMemoField;
    viewPPQCreateDate: TDateTimeField;
    viewPPQCreateBy: TStringField;
    viewPPQOperator: TStringField;
    viewPPQTglEntry: TDateTimeField;
    viewPPQTglCetak: TDateTimeField;
    viewPPQNoBody: TStringField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQPlatNo: TStringField;
    MasterVGridDBEditorRow5: TcxDBEditorRow;
    viewPPQPlatNo: TStringField;
    MasterQKeluhan: TMemoField;
    MasterQRekanan: TStringField;
    RekananQ: TSDQuery;
    MasterQNamaRekanan: TStringField;
    MasterVGridRekanan: TcxDBEditorRow;
    MasterVGridNamaRekanan: TcxDBEditorRow;
    viewPPQRekanan: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQCaraMinta: TStringField;
    MasterVGridCaraMinta: TcxDBEditorRow;
    a: TStringField;
    MasterVGridNoMinta: TcxDBEditorRow;
    MasterVGridJamMinta: TcxDBEditorRow;
    MasterQJamMinta: TStringField;
    viewPPQCaraMinta: TStringField;
    viewPPQNoMinta: TStringField;
    viewPPQJamMinta: TStringField;
    viewPPQDetailPermintaan: TStringField;
    viewPPQNamaRekanan: TStringField;
    viewPPQNamaPeminta: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1Keluhan: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1DetailPermintaan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaRekanan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPeminta: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridEkorEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPemintaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton1Click(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure MasterVGridRekananEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure MasterVGridRekananEditPropertiesChange(Sender: TObject);
    procedure MasterVGridCaraMintaEditPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PermintaanPerbaikanFm: TPermintaanPerbaikanFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, StrUtils, ArmadaDropDown,
  PegawaiDropDown, LookupRekanan;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPermintaanPerbaikanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPermintaanPerbaikanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPermintaanPerbaikanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPermintaanPerbaikanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  ArmadaQ.Open;
  PegawaiQ.Open;
end;

procedure TPermintaanPerbaikanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  MasterQTanggal.Value:=Trunc(Now());
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TPermintaanPerbaikanFm.KodeEditEnter(Sender: TObject);
begin
  //buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  cxButton1.Enabled:=false;
  MasterQ.Close;
end;

procedure TPermintaanPerbaikanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewPPQ.Close;
  viewPPQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewPPQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewPPQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertPermintaanPerbaikan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePermintaanPerbaikan.AsBoolean;
  cxButton1.Enabled:=menuutamafm.UserQDeletePermintaanPerbaikan.AsBoolean;
  if MenuUtamaFm.UserQPPRekanan.AsBoolean=true then
    MasterVGridRekanan.Visible:=true
  else
    MasterVGridRekanan.Visible:=false;
end;

procedure TPermintaanPerbaikanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxButton1.Enabled:=menuutamafm.UserQDeletePermintaanPerbaikan.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePermintaanPerbaikan.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
  
  MasterVGridArmada.Visible:=true;
  MasterVGridPeminta.Visible:=true;
end;

procedure TPermintaanPerbaikanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPermintaanPerbaikanFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Permintaan Perbaikan dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    viewPPQ.Close;
    viewPPQ.Open;
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TPermintaanPerbaikanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TPermintaanPerbaikanFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TPermintaanPerbaikanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Surat Jalan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Surat Jalan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TPermintaanPerbaikanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    //buttonCetak.Enabled:=true;
    kodeedit.Text:=viewPPQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdatePermintaanPerbaikan.AsBoolean;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;

procedure TPermintaanPerbaikanFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    sopirQ.Close;
    sopirQ.ExecSQL;
    sopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      //MasterQnama_sopir.AsString := sopirQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.MasterVGridEkorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ekorQ.Close;
    ekorQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ekorQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQEkor.AsString:= ekorQkode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if MasterQRekanan.AsString<>'' then
    ArmadaDropDownFm:=TArmadaDropDownFm.Create(self,MasterQRekanan.AsString)
  else
    begin
      ArmadaDropDownFm:=TArmadaDropDownFm.Create(self,'pp');
    end;
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.MasterVGridPemintaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPeminta.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TPermintaanPerbaikanFm.cxButton1Click(Sender: TObject);
begin
  if MessageDlg('Hapus PermintaanPerbaikan '+KodeEdit.Text+' - '+MasterQNoBody.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('PermintaanPerbaikan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('PermintaanPerbaikan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     viewPPQ.Refresh;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TPermintaanPerbaikanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPermintaanPerbaikanFm.MasterVGridRekananEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LookupRekananFm:=TLookupRekananFm.Create(self);
  if LookupRekananFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQRekanan.AsString:= LookupRekananFm.kode;
    MasterVGridPeminta.Visible:=false;
    MasterQPeminta.AsVariant:=null;
  end;
  LookupRekananFm.Release;
end;

procedure TPermintaanPerbaikanFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  viewPPQ.Close;
  viewPPQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewPPQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewPPQ.Open;
end;

procedure TPermintaanPerbaikanFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  viewPPQ.Close;
  viewPPQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewPPQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewPPQ.Open;
end;

procedure TPermintaanPerbaikanFm.MasterVGridRekananEditPropertiesChange(
  Sender: TObject);
begin
  if MasterQRekanan.AsString='' then
    begin
      MasterVGridPeminta.Visible:=true;
      MasterVGridCaraMinta.Visible:=false;
    end
  else
    begin
      MasterVGridPeminta.Visible:=false;
      MasterVGridCaraMinta.Visible:=true;
    end;
end;

procedure TPermintaanPerbaikanFm.MasterVGridCaraMintaEditPropertiesChange(
  Sender: TObject);
begin
  if MasterQCaraMinta.AsString='SMS' then
    begin
      MasterVGridNoMinta.Visible:=true;
      MasterVGridJamMinta.Visible:=true;
    end
  else
    begin
      MasterVGridNoMinta.Visible:=false;
      MasterVGridJamMinta.Visible:=false;
    end;
end;

end.
