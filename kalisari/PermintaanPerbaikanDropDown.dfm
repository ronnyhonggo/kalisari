object PermintaanPerbaikanDropDownFm: TPermintaanPerbaikanDropDownFm
  Left = 303
  Top = 210
  Width = 842
  Height = 498
  Caption = 'PermintaanPerbaikanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 826
    Height = 459
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 84
      end
      object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 109
      end
      object cxGrid1DBTableView1NoBody: TcxGridDBColumn
        DataBinding.FieldName = 'NoBody'
        Options.SortByDisplayText = isbtOn
        Width = 65
      end
      object cxGrid1DBTableView1NamaPeminta: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPeminta'
        Width = 167
      end
      object cxGrid1DBTableView1Keluhan: TcxGridDBColumn
        DataBinding.FieldName = 'Keluhan'
        Width = 376
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PPQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select pp.* from permintaanperbaikan pp left outer join laporanp' +
        'erbaikan lp on pp.kode=lp.pp where lp.kode is NULL'
      'order by tglentry desc')
    Left = 216
    Top = 368
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PPQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object PPQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object PPQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
  end
  object LPBDs: TDataSource
    DataSet = PPQ
    Left = 280
    Top = 368
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 248
    Top = 368
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai')
    Left = 312
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
end
