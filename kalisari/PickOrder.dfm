object PickOrderFm: TPickOrderFm
  Left = 155
  Top = 100
  Width = 992
  Height = 606
  Caption = 'Pilih Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 976
    Height = 49
    Align = alTop
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 498
    Width = 976
    Height = 51
    Align = alBottom
    TabOrder = 1
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      Visible = False
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 92
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      Visible = False
      OnClick = ExitBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 549
    Width = 976
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxLabel1: TcxLabel
    Left = 368
    Top = 64
  end
  object cxLabel2: TcxLabel
    Left = 368
    Top = 88
  end
  object cxLabel3: TcxLabel
    Left = 368
    Top = 112
  end
  object cxLabel4: TcxLabel
    Left = 368
    Top = 136
  end
  object cxLabel5: TcxLabel
    Left = 368
    Top = 160
  end
  object cxLabel6: TcxLabel
    Left = 368
    Top = 184
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 49
    Width = 976
    Height = 248
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellClick = cxGrid1DBTableView1CellClick
      DataController.DataSource = MasterDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          FieldName = 'Kodenota'
          DisplayText = 'tes'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.CancelOnExit = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.Background = cxStyle1
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Berangkat'
        Width = 100
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'Tiba'
        Width = 111
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Asal'
        Width = 114
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'Tujuan'
        Width = 116
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPelanggan'
        Width = 146
      end
      object cxGrid1DBTableView1Kodenota: TcxGridDBColumn
        Caption = 'NoSO'
        DataBinding.FieldName = 'Kodenota'
        Width = 95
      end
      object cxGrid1DBTableView1Column8: TcxGridDBColumn
        Caption = 'Seat'
        DataBinding.FieldName = 'KapasitasSeat'
      end
      object cxGrid1DBTableView1Column9: TcxGridDBColumn
        DataBinding.FieldName = 'AC'
      end
      object cxGrid1DBTableView1Column10: TcxGridDBColumn
        DataBinding.FieldName = 'Toilet'
      end
      object cxGrid1DBTableView1Column11: TcxGridDBColumn
        Caption = 'AirSus'
        DataBinding.FieldName = 'AirSuspension'
        Width = 47
      end
      object cxGrid1DBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'Armada'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1Column6PropertiesButtonClick
        Styles.Content = MenuUtamaFm.cxStyle5
        Width = 106
      end
      object cxGrid1DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Options.Editing = False
        Width = 110
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 350
    Width = 976
    Height = 148
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    object cxGridDBTableView1: TcxGridDBTableView
      DataController.DataSource = ViewDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          FieldName = 'Kodenota'
          DisplayText = 'tes'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.CancelOnExit = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGridDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Berangkat'
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'Tiba'
      end
      object cxGridDBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Asal'
        Width = 161
      end
      object cxGridDBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'Tujuan'
        Width = 159
      end
      object cxGridDBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPelanggan'
        Options.SortByDisplayText = isbtOn
        Width = 194
      end
      object cxGridDBTableView1NoSO: TcxGridDBColumn
        DataBinding.FieldName = 'NoSO'
        Width = 103
      end
      object cxGridDBTableView1Column8: TcxGridDBColumn
        Caption = 'Seat'
        DataBinding.FieldName = 'KapasitasSeat'
        Width = 40
      end
      object cxGridDBTableView1Column9: TcxGridDBColumn
        DataBinding.FieldName = 'AC'
      end
      object cxGridDBTableView1Column10: TcxGridDBColumn
        DataBinding.FieldName = 'Toilet'
      end
      object cxGridDBTableView1Column11: TcxGridDBColumn
        Caption = 'AirSus'
        DataBinding.FieldName = 'AirSuspension'
        Width = 45
      end
      object cxGridDBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'Armada'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGridDBTableView1Column6PropertiesButtonClick
        Width = 109
      end
      object cxGridDBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 117
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cxGroupBox1: TcxGroupBox
    Left = 24
    Top = 304
    Caption = 'Suggestion'
    TabOrder = 11
    Height = 33
    Width = 289
    object cxRadioButton1: TcxRadioButton
      Left = 64
      Top = 11
      Width = 113
      Height = 17
      Caption = 'Only Suggestion'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = cxRadioButton1Click
    end
    object cxRadioButton2: TcxRadioButton
      Left = 176
      Top = 11
      Width = 89
      Height = 17
      Caption = 'Show All'
      TabOrder = 1
      OnClick = cxRadioButton2Click
    end
  end
  object cxGroupBox2: TcxGroupBox
    Left = 24
    Top = 8
    Caption = 'Sales Order'
    TabOrder = 12
    Height = 33
    Width = 289
    object cxRadioButton3: TcxRadioButton
      Left = 63
      Top = 12
      Width = 113
      Height = 17
      Caption = 'Show Current'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = cxRadioButton3Click
    end
    object cxRadioButton4: TcxRadioButton
      Left = 175
      Top = 12
      Width = 82
      Height = 17
      Caption = 'Show All'
      TabOrder = 1
      OnClick = cxRadioButton4Click
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, p.namapt as NamaPelanggan, r.muat as Asal, r.Bongkar' +
        ' as Tujuan from MasterSO m, Pelanggan p, Rute r where m.Pelangga' +
        'n = p.Kode and m.Rute=r.Kode and (m.tgl>=GETDATE() or m.armada=n' +
        'ull) and m.status='#39'DEAL'#39)
    UpdateObject = MasterUS
    Left = 337
    Top = 1
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object MasterQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object MasterQAC: TBooleanField
      FieldName = 'AC'
    end
    object MasterQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object MasterQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object MasterQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Required = True
      Size = 50
    end
    object MasterQAsal: TStringField
      FieldName = 'Asal'
      Required = True
      Size = 50
    end
    object MasterQTujuan: TStringField
      FieldName = 'Tujuan'
      Required = True
      Size = 50
    end
    object MasterQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 55
      Lookup = True
    end
    object MasterQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object MasterQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object MasterQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object MasterQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object MasterQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object MasterQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object MasterQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object MasterQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object MasterQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object MasterQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object MasterQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object MasterQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object MasterQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object MasterQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object MasterQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object MasterQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object MasterQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 684
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, Pembaya' +
        'ranAwal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiPembay' +
        'aranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraP' +
        'embayaranPelunasan, NoKwitansiPelunasan, PenerimaPelunasan, Exte' +
        'nd, TglKembaliExtend, BiayaExtend, KapasitasSeat, AC, Toilet, Ai' +
        'rSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJ' +
        'emput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, ' +
        'ReminderPending, Keterangan, CreateDate, CreateBy, Operator, Tgl' +
        'Entry, TglCetak'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PembayaranA' +
        'wal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiPembayaran' +
        'Awal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPemba' +
        'yaranPelunasan, NoKwitansiPelunasan, PenerimaPelunasan, Extend, ' +
        'TglKembaliExtend, BiayaExtend, KapasitasSeat, AC, Toilet, AirSus' +
        'pension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJempu' +
        't, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, Remi' +
        'nderPending, Keterangan, CreateDate, CreateBy, Operator, TglEntr' +
        'y, TglCetak)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :Pemb' +
        'ayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :NoKwitansi' +
        'PembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPelunas' +
        'an, :CaraPembayaranPelunasan, :NoKwitansiPelunasan, :PenerimaPel' +
        'unasan, :Extend, :TglKembaliExtend, :BiayaExtend, :KapasitasSeat' +
        ', :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :Armada, :K' +
        'ontrak, :PICJemput, :JamJemput, :NoTelpPICJemput, :AlamatJemput,' +
        ' :Status, :StatusPembayaran, :ReminderPending, :Keterangan, :Cre' +
        'ateDate, :CreateBy, :Operator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 844
    Top = 98
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from masterso order by kodenota desc')
    Left = 745
    Top = 135
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39
      '')
    Left = 769
    Top = 79
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 12
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 801
    Top = 143
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39)
    Left = 369
    Top = 65535
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, sj.kodenota as surat_jalan, p.namapt as NamaPelangga' +
        'n, r.muat as Asal, r.bongkar as Tujuan, sj.noso as NoSO from Mas' +
        'terSO m left outer join'
      
        'MasterSJ sj on m.Kodenota=sj.NoSO, Pelanggan p, Rute r where m.P' +
        'elanggan = p.Kode and m.Rute=r.Kode and m.status='#39'DEAL'#39' and r.mu' +
        'at like '#39'%'#39'+ :textmuat +'#39'%'#39' and r.bongkar like '#39'%'#39'+ :textbongkar' +
        ' +'#39'%'#39
      ' '
      ' '
      '')
    UpdateObject = ViewUS
    Left = 441
    Top = 1
    ParamData = <
      item
        DataType = ftString
        Name = 'textmuat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'textbongkar'
        ParamType = ptInput
      end>
    object ViewQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object ViewQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object ViewQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object ViewQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object ViewQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object ViewQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object ViewQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object ViewQAC: TBooleanField
      FieldName = 'AC'
    end
    object ViewQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ViewQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ViewQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object ViewQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object ViewQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object ViewQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object ViewQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object ViewQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewQsurat_jalan: TStringField
      FieldName = 'surat_jalan'
      Size = 10
    end
    object ViewQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Required = True
      Size = 50
    end
    object ViewQAsal: TStringField
      FieldName = 'Asal'
      Required = True
      Size = 50
    end
    object ViewQTujuan: TStringField
      FieldName = 'Tujuan'
      Required = True
      Size = 50
    end
    object ViewQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 55
      Lookup = True
    end
    object ViewQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object ViewQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object ViewQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object ViewQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object ViewQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object ViewQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object ViewQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object ViewQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object ViewQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object ViewQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object ViewQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object ViewQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object ViewQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object ViewQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object ViewQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object ViewQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object ViewQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object ViewQNoSO: TStringField
      FieldName = 'NoSO'
      Size = 10
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewQ
    Left = 500
    Top = 6
  end
  object ViewUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, Pembaya' +
        'ranAwal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiPembay' +
        'aranAwal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraP' +
        'embayaranPelunasan, NoKwitansiPelunasan, PenerimaPelunasan, Exte' +
        'nd, TglKembaliExtend, BiayaExtend, KapasitasSeat, AC, Toilet, Ai' +
        'rSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJ' +
        'emput, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, ' +
        'ReminderPending, Keterangan, CreateDate, CreateBy, Operator, Tgl' +
        'Entry, TglCetak'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PembayaranA' +
        'wal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiPembayaran' +
        'Awal, PenerimaPembayaranAwal, Pelunasan, TglPelunasan, CaraPemba' +
        'yaranPelunasan, NoKwitansiPelunasan, PenerimaPelunasan, Extend, ' +
        'TglKembaliExtend, BiayaExtend, KapasitasSeat, AC, Toilet, AirSus' +
        'pension, Rute, TglFollowUp, Armada, Kontrak, PICJemput, JamJempu' +
        't, NoTelpPICJemput, AlamatJemput, Status, StatusPembayaran, Remi' +
        'nderPending, Keterangan, CreateDate, CreateBy, Operator, TglEntr' +
        'y, TglCetak)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :Pemb' +
        'ayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :NoKwitansi' +
        'PembayaranAwal, :PenerimaPembayaranAwal, :Pelunasan, :TglPelunas' +
        'an, :CaraPembayaranPelunasan, :NoKwitansiPelunasan, :PenerimaPel' +
        'unasan, :Extend, :TglKembaliExtend, :BiayaExtend, :KapasitasSeat' +
        ', :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp, :Armada, :K' +
        'ontrak, :PICJemput, :JamJemput, :NoTelpPICJemput, :AlamatJemput,' +
        ' :Status, :StatusPembayaran, :ReminderPending, :Keterangan, :Cre' +
        'ateDate, :CreateBy, :Operator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 548
    Top = 2
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 600
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clSkyBlue
    end
  end
end
