unit PickOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox;

type
  TPickOrderFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    KodeQkodenota: TStringField;
    ArmadaQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    ExitBtn: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ViewQ: TSDQuery;
    ViewDs: TDataSource;
    ViewUS: TSDUpdateSQL;
    cxGroupBox1: TcxGroupBox;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGroupBox2: TcxGroupBox;
    cxRadioButton3: TcxRadioButton;
    cxRadioButton4: TcxRadioButton;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQBerangkat: TDateTimeField;
    MasterQTiba: TDateTimeField;
    MasterQHarga: TCurrencyField;
    MasterQKapasitasSeat: TIntegerField;
    MasterQAC: TBooleanField;
    MasterQToilet: TBooleanField;
    MasterQAirSuspension: TBooleanField;
    MasterQRute: TStringField;
    MasterQTglFollowUp: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQKontrak: TStringField;
    MasterQPICJemput: TMemoField;
    MasterQAlamatJemput: TMemoField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQNamaPelanggan: TStringField;
    MasterQAsal: TStringField;
    MasterQTujuan: TStringField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    MasterQPlatNo: TStringField;
    ViewQKodenota: TStringField;
    ViewQTgl: TDateTimeField;
    ViewQPelanggan: TStringField;
    ViewQBerangkat: TDateTimeField;
    ViewQTiba: TDateTimeField;
    ViewQHarga: TCurrencyField;
    ViewQKapasitasSeat: TIntegerField;
    ViewQAC: TBooleanField;
    ViewQToilet: TBooleanField;
    ViewQAirSuspension: TBooleanField;
    ViewQRute: TStringField;
    ViewQTglFollowUp: TDateTimeField;
    ViewQArmada: TStringField;
    ViewQKontrak: TStringField;
    ViewQPICJemput: TMemoField;
    ViewQAlamatJemput: TMemoField;
    ViewQStatus: TStringField;
    ViewQKeterangan: TMemoField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQTglCetak: TDateTimeField;
    ViewQsurat_jalan: TStringField;
    ViewQNamaPelanggan: TStringField;
    ViewQAsal: TStringField;
    ViewQTujuan: TStringField;
    ViewQPlatNo: TStringField;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    cxGridDBTableView1Column4: TcxGridDBColumn;
    cxGridDBTableView1Column5: TcxGridDBColumn;
    cxGridDBTableView1Column6: TcxGridDBColumn;
    cxGridDBTableView1Column7: TcxGridDBColumn;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQPembayaranAwal: TCurrencyField;
    MasterQTglPembayaranAwal: TDateTimeField;
    MasterQCaraPembayaranAwal: TStringField;
    MasterQNoKwitansiPembayaranAwal: TStringField;
    MasterQPenerimaPembayaranAwal: TStringField;
    MasterQPelunasan: TCurrencyField;
    MasterQCaraPembayaranPelunasan: TStringField;
    MasterQNoKwitansiPelunasan: TStringField;
    MasterQPenerimaPelunasan: TStringField;
    MasterQExtend: TBooleanField;
    MasterQTglKembaliExtend: TDateTimeField;
    MasterQBiayaExtend: TCurrencyField;
    MasterQJamJemput: TDateTimeField;
    MasterQNoTelpPICJemput: TStringField;
    MasterQStatusPembayaran: TStringField;
    MasterQReminderPending: TDateTimeField;
    ViewQPembayaranAwal: TCurrencyField;
    ViewQTglPembayaranAwal: TDateTimeField;
    ViewQCaraPembayaranAwal: TStringField;
    ViewQNoKwitansiPembayaranAwal: TStringField;
    ViewQPenerimaPembayaranAwal: TStringField;
    ViewQPelunasan: TCurrencyField;
    ViewQCaraPembayaranPelunasan: TStringField;
    ViewQNoKwitansiPelunasan: TStringField;
    ViewQPenerimaPelunasan: TStringField;
    ViewQExtend: TBooleanField;
    ViewQTglKembaliExtend: TDateTimeField;
    ViewQBiayaExtend: TCurrencyField;
    ViewQJamJemput: TDateTimeField;
    ViewQNoTelpPICJemput: TStringField;
    ViewQStatusPembayaran: TStringField;
    ViewQReminderPending: TDateTimeField;
    PelangganQNamaPT: TStringField;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxGrid1DBTableView1Column9: TcxGridDBColumn;
    cxGrid1DBTableView1Column10: TcxGridDBColumn;
    cxGrid1DBTableView1Column11: TcxGridDBColumn;
    cxGridDBTableView1Column8: TcxGridDBColumn;
    cxGridDBTableView1Column9: TcxGridDBColumn;
    cxGridDBTableView1Column10: TcxGridDBColumn;
    cxGridDBTableView1Column11: TcxGridDBColumn;
    MasterQTglPelunasan: TDateTimeField;
    ViewQTglPelunasan: TDateTimeField;
    ViewQNoSO: TStringField;
    cxGridDBTableView1NoSO: TcxGridDBColumn;
    cxGrid1DBTableView1Kodenota: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure cxRadioButton2Click(Sender: TObject);
    procedure cxRadioButton4Click(Sender: TObject);
    procedure cxRadioButton3Click(Sender: TObject);
    procedure cxGrid1DBTableView1Column6PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGridDBTableView1Column6PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PickOrderFm: TPickOrderFm;

  MasterOriSQL,ViewOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, ArmadaDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPickOrderFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPickOrderFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPickOrderFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPickOrderFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPickOrderFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  ViewOriSQL:=ViewQ.SQL.Text;
  MasterQ.Open;
end;




procedure TPickOrderFm.FormShow(Sender: TObject);
begin
  cxGrid1DBTableView1Column6.Properties.Buttons[0].Enabled:=menuutamafm.UserQUpdatePilihArmada.AsBoolean;
  cxGridDBTableView1Column6.Properties.Buttons[0].Enabled:=menuutamafm.UserQUpdatePilihArmada.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdatePilihArmada.AsBoolean;
end;

procedure TPickOrderFm.SaveBtnClick(Sender: TObject);
begin
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    ViewQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ViewQ.CommitUpdates;
    ShowMessage('Update Sukses');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ViewQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  masterq.RefreshRecord;
  viewq.RefreshRecord;
end;

procedure TPickOrderFm.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin

  cxRadioButton1Click(sender);
  cxRadioButton1.Checked:=true;
end;

procedure TPickOrderFm.cxRadioButton1Click(Sender: TObject);
begin
  viewq.Close;
  viewq.SQL.Text:=ViewOriSQL;
  viewq.ParamByName('textmuat').AsString:='%';
  viewq.ParamByName('textbongkar').AsString:=MasterQAsal.AsString;
  viewq.ExecSQL;
  viewq.Open;
end;

procedure TPickOrderFm.cxRadioButton2Click(Sender: TObject);
begin
  viewq.Close;
  viewq.ParamByName('textmuat').AsString:='%';
  viewq.ParamByName('textbongkar').AsString:='%';
 // viewq.SQL.Text:=MasterOriSQL;
  viewq.ExecSQL;
  viewq.Open;
end;

procedure TPickOrderFm.cxRadioButton4Click(Sender: TObject);
begin
  Masterq.Close;
  Masterq.SQL.Text:='select m.*, p.namapt as NamaPelanggan, r.muat as Asal, r.bongkar as Tujuan from MasterSO m, Pelanggan p, Rute r where m.Pelanggan = p.Kode and m.Rute=r.Kode and m.status="DEAL"';
  masterq.open;
end;

procedure TPickOrderFm.cxRadioButton3Click(Sender: TObject);
begin
  Masterq.Close;
  Masterq.SQL.Text:=MasterOriSQL;
  masterq.open;
end;

procedure TPickOrderFm.cxGrid1DBTableView1Column6PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Update Sukses');
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  masterq.RefreshRecord;
  if not ViewQ.IsEmpty then viewq.RefreshRecord;
end;

procedure TPickOrderFm.cxGridDBTableView1Column6PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      ViewQ.Open;
      ViewQ.Edit;
      ViewQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
    MenuUtamaFm.Database1.StartTransaction;
    try
      ViewQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      ViewQ.CommitUpdates;
      ShowMessage('Update Sukses');
    except
      MenuUtamaFm.Database1.Rollback;
      ViewQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
    masterq.RefreshRecord;
    viewq.RefreshRecord;
end;

end.
