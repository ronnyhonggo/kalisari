unit Pivot;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxPivotGridLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxSkinsdxBarPainter,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxClasses,
  cxCustomData, cxStyles, cxEdit, cxCustomPivotGrid, cxDBPivotGrid, DB,
  SDEngine, ImgList, dxPSCore, dxPScxCommon;

type
  TForm1 = class(TForm)
    dxPSEngineController1: TdxPSEngineController;
    dxComponentPrinter: TdxComponentPrinter;
    dxComponentPrinterLink1: TcxPivotGridReportLink;
    PaymentTypeImages: TImageList;
    ilMain: TcxImageList;
    cxDBPivotGrid1: TcxDBPivotGrid;
    SOQ: TSDQuery;
    DataSource1: TDataSource;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxDBPivotGrid1Kodenota: TcxDBPivotGridField;
    cxDBPivotGrid1Tgl: TcxDBPivotGridField;
    cxDBPivotGrid1Pelanggan: TcxDBPivotGridField;
    cxDBPivotGrid1Berangkat: TcxDBPivotGridField;
    cxDBPivotGrid1Tiba: TcxDBPivotGridField;
    cxDBPivotGrid1Harga: TcxDBPivotGridField;
    cxDBPivotGrid1PPN: TcxDBPivotGridField;
    cxDBPivotGrid1PembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1TglPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1CaraPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1KeteranganCaraPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1NoKwitansiPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1NominalKwitansiPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1PenerimaPembayaranAwal: TcxDBPivotGridField;
    cxDBPivotGrid1Pelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1TglPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1CaraPembayaranPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1KetCaraPembayaranPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1NoKwitansiPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1NominalKwitansiPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1PenerimaPelunasan: TcxDBPivotGridField;
    cxDBPivotGrid1Extend: TcxDBPivotGridField;
    cxDBPivotGrid1TglKembaliExtend: TcxDBPivotGridField;
    cxDBPivotGrid1BiayaExtend: TcxDBPivotGridField;
    cxDBPivotGrid1PPNExtend: TcxDBPivotGridField;
    cxDBPivotGrid1KapasitasSeat: TcxDBPivotGridField;
    cxDBPivotGrid1AC: TcxDBPivotGridField;
    cxDBPivotGrid1Toilet: TcxDBPivotGridField;
    cxDBPivotGrid1AirSuspension: TcxDBPivotGridField;
    cxDBPivotGrid1Rute: TcxDBPivotGridField;
    cxDBPivotGrid1TglFollowUp: TcxDBPivotGridField;
    cxDBPivotGrid1Armada: TcxDBPivotGridField;
    cxDBPivotGrid1Kontrak: TcxDBPivotGridField;
    cxDBPivotGrid1PICJemput: TcxDBPivotGridField;
    cxDBPivotGrid1JamJemput: TcxDBPivotGridField;
    cxDBPivotGrid1NoTelpPICJemput: TcxDBPivotGridField;
    cxDBPivotGrid1AlamatJemput: TcxDBPivotGridField;
    cxDBPivotGrid1Status: TcxDBPivotGridField;
    cxDBPivotGrid1StatusPembayaran: TcxDBPivotGridField;
    cxDBPivotGrid1ReminderPending: TcxDBPivotGridField;
    cxDBPivotGrid1PenerimaPending: TcxDBPivotGridField;
    cxDBPivotGrid1Keterangan: TcxDBPivotGridField;
    cxDBPivotGrid1CreateDate: TcxDBPivotGridField;
    cxDBPivotGrid1CreateBy: TcxDBPivotGridField;
    cxDBPivotGrid1Operator: TcxDBPivotGridField;
    cxDBPivotGrid1TglEntry: TcxDBPivotGridField;
    cxDBPivotGrid1TglCetak: TcxDBPivotGridField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

end.
