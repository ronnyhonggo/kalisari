object RealisasiAnjemFm: TRealisasiAnjemFm
  Left = 161
  Top = 120
  Width = 1245
  Height = 544
  Caption = 'Realisasi Anjem'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1229
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 438
    Width = 1229
    Height = 41
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 173
      Top = 11
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object buttonCetak: TcxButton
      Left = 255
      Top = 11
      Width = 75
      Height = 25
      Caption = 'CETAK'
      Enabled = False
      TabOrder = 2
      OnClick = buttonCetakClick
    end
    object deleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 3
      OnClick = deleteBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 479
    Width = 1229
    Height = 27
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 416
    Top = 48
    Width = 813
    Height = 390
    Align = alRight
    BevelOuter = bvRaised
    BevelWidth = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = SJDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsSelection.CellSelect = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalMulai'
      end
      object cxGrid1DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalSelesai'
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPelanggan'
        Width = 186
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Dari'
        Width = 115
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'Tujuan'
        Width = 124
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'NoPlat'
      end
      object cxGrid1DBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPengemudi'
        Width = 171
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 401
    Height = 390
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 206
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 4
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridKontrak: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKontrakEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kontrak'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridKodePelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KodePelanggan'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridNamaPelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPelanggan'
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamatPelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AlamatPelanggan'
      ID = 3
      ParentID = 1
      Index = 1
      Version = 1
    end
    object MasterVGridNoTelponPelanggan: TcxDBEditorRow
      Properties.Caption = 'NoTelpon'
      Properties.DataBinding.FieldName = 'NoTelponPelanggan'
      ID = 4
      ParentID = 1
      Index = 2
      Version = 1
    end
    object MasterVGridKodeRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KodeRute'
      ID = 5
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGridRuteDari: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'RuteDari'
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
    object MasterVGridRuteTujuan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'RuteTujuan'
      ID = 7
      ParentID = 5
      Index = 1
      Version = 1
    end
    object MasterVGridTanggalMulai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalMulai'
      ID = 8
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridTanggalSelesai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalSelesai'
      ID = 9
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Armada'
      ID = 10
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridKodeArmada: TcxDBEditorRow
      Properties.Caption = 'NomorPlat'
      Properties.DataBinding.FieldName = 'KodeArmada'
      ID = 11
      ParentID = 10
      Index = 0
      Version = 1
    end
    object MasterVGridPengemudi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPengemudiEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pengemudi'
      ID = 12
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridNamaPengemudi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPengemudi'
      Properties.Options.Editing = False
      ID = 13
      ParentID = 12
      Index = 0
      Version = 1
    end
    object MasterVGridKilometerAwal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KilometerAwal'
      ID = 14
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridKilometerAkhir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KilometerAkhir'
      ID = 15
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridCategoryRow4: TcxCategoryRow
      Properties.Caption = 'Pemasukan'
      ID = 16
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridPendapatan: TcxDBEditorRow
      Properties.Caption = 'UangSaku'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Pendapatan'
      Properties.Options.Editing = False
      ID = 17
      ParentID = 16
      Index = 0
      Version = 1
    end
    object MasterVGridCategoryRow5: TcxCategoryRow
      Properties.Caption = 'Pengeluaran'
      ID = 18
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridCategoryRow1: TcxCategoryRow
      Height = 17
      Properties.Caption = 'BBM'
      ID = 19
      ParentID = 18
      Index = 0
      Version = 1
    end
    object MasterVGridCategoryRow2: TcxCategoryRow
      Properties.Caption = 'SPBU A. Yani'
      ID = 20
      ParentID = 19
      Index = 0
      Version = 1
    end
    object MasterVGridSPBUAYaniLiter: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.AssignedValues.DisplayFormat = True
      Properties.EditProperties.OnValidate = MasterVGridSPBUAYaniLiterEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBUAYaniLiter'
      ID = 21
      ParentID = 20
      Index = 0
      Version = 1
    end
    object MasterVGridSPBUAYaniUang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridSPBUAYaniUangEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBUAYaniUang'
      ID = 22
      ParentID = 20
      Index = 1
      Version = 1
    end
    object MasterVGridSPBUAYaniJam: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SPBUAYaniJam'
      ID = 23
      ParentID = 20
      Index = 2
      Version = 1
    end
    object MasterVGridCategoryRow3: TcxCategoryRow
      Properties.Caption = 'SPBU Luar'
      ID = 24
      ParentID = 19
      Index = 1
      Version = 1
    end
    object MasterVGridSPBULuarLiter: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.AssignedValues.DisplayFormat = True
      Properties.EditProperties.OnValidate = MasterVGridSPBULuarLiterEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBULuarLiter'
      ID = 25
      ParentID = 24
      Index = 0
      Version = 1
    end
    object MasterVGridSPBULuarUang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridSPBULuarUangEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBULuarUang'
      ID = 26
      ParentID = 24
      Index = 1
      Version = 1
    end
    object MasterVGridSPBULuarUangDiberi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DataBinding.FieldName = 'SPBULuarUangDiberi'
      Visible = False
      ID = 27
      ParentID = 24
      Index = 2
      Version = 1
    end
    object MasterVGridSPBULuarDetail: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SPBULuarDetail'
      ID = 28
      ParentID = 24
      Index = 3
      Version = 1
    end
    object MasterVGridPremiSopir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridPremiSopirEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PremiSopir'
      ID = 29
      ParentID = 18
      Index = 1
      Version = 1
    end
    object MasterVGridPremiKernet: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridPremiKernetEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PremiKernet'
      ID = 30
      ParentID = 18
      Index = 2
      Version = 1
    end
    object MasterVGridTabunganSopir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridTabunganSopirEditPropertiesValidate
      Properties.DataBinding.FieldName = 'TabunganSopir'
      ID = 31
      ParentID = 18
      Index = 3
      Version = 1
    end
    object MasterVGridTol: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridTolEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Tol'
      ID = 32
      ParentID = 18
      Index = 4
      Version = 1
    end
    object MasterVGridBiayaLainLain: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridBiayaLainLainEditPropertiesValidate
      Properties.DataBinding.FieldName = 'BiayaLainLain'
      ID = 33
      ParentID = 18
      Index = 5
      Version = 1
    end
    object MasterVGridKeteranganBiayaLainLain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KeteranganBiayaLainLain'
      ID = 34
      ParentID = 33
      Index = 0
      Version = 1
    end
    object MasterVGridPengeluaran: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Pengeluaran'
      Properties.Options.Editing = False
      ID = 35
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridSisaDisetor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SisaDisetor'
      ID = 36
      ParentID = -1
      Index = 10
      Version = 1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    RequestLive = True
    SQL.Strings = (
      'select * from RealisasiAnjem')
    UniDirectional = True
    UpdateObject = MasterUS
    Left = 257
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      LookupCache = True
      Size = 10
    end
    object MasterQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object MasterQKilometerAwal: TIntegerField
      FieldName = 'KilometerAwal'
    end
    object MasterQKilometerAkhir: TIntegerField
      FieldName = 'KilometerAkhir'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Rute'
      KeyFields = 'Kontrak'
      LookupCache = True
      Size = 10
      Lookup = True
    end
    object MasterQRuteDari: TStringField
      FieldKind = fkLookup
      FieldName = 'RuteDari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQRuteTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'RuteTujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Pelanggan'
      KeyFields = 'Kontrak'
      LookupCache = True
      Size = 10
      Lookup = True
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQAlamatPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'KodePelanggan'
      Size = 100
      Lookup = True
    end
    object MasterQNoTelponPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NoTelponPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoTelp'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object MasterQKodeArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object MasterQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object MasterQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object MasterQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object MasterQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object MasterQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object MasterQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object MasterQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object MasterQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object MasterQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object MasterQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object MasterQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object MasterQKeteranganBiayaLainLain: TMemoField
      FieldName = 'KeteranganBiayaLainLain'
      BlobType = ftMemo
    end
    object MasterQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object MasterQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object MasterQNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object MasterQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object MasterQTanggalMulai: TDateTimeField
      FieldName = 'TanggalMulai'
    end
    object MasterQTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 340
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Peng' +
        'emudi, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, S' +
        'isaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULua' +
        'rLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiS' +
        'opir, PremiKernet, TabunganSopir, Tol, BiayaLainLain, Keterangan' +
        'BiayaLainLain, CreateBy, Operator, CreateDate, TglEntry'#13#10'from Re' +
        'alisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  TanggalMulai = :TanggalMulai,'
      '  TanggalSelesai = :TanggalSelesai,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiAnjem'
      
        '  (Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Pengemud' +
        'i, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, SisaD' +
        'isetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULuarLit' +
        'er, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiSopir' +
        ', PremiKernet, TabunganSopir, Tol, BiayaLainLain, KeteranganBiay' +
        'aLainLain, CreateBy, Operator, CreateDate, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :TanggalMulai, :TanggalSelesai, :Armada, :Pe' +
        'ngemudi, :KilometerAwal, :KilometerAkhir, :Pendapatan, :Pengelua' +
        'ran, :SisaDisetor, :SPBUAYaniLiter, :SPBUAYaniUang, :SPBUAYaniJa' +
        'm, :SPBULuarLiter, :SPBULuarUang, :SPBULuarUangDiberi, :SPBULuar' +
        'Detail, :PremiSopir, :PremiKernet, :TabunganSopir, :Tol, :BiayaL' +
        'ainLain, :KeteranganBiayaLainLain, :CreateBy, :Operator, :Create' +
        'Date, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    Left = 380
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from RealisasiAnjem order by kode desc')
    Left = 297
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from masterso where armada <> NULL')
    Left = 425
    Top = 7
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 465
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pelanggan')
    Left = 513
    Top = 7
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
  end
  object viewRealisasiAnjem: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from RealisasiAnjem')
    Left = 569
    Top = 9
    object viewRealisasiAnjemKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewRealisasiAnjemKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object viewRealisasiAnjemArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewRealisasiAnjemPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object viewRealisasiAnjemKilometerAwal: TIntegerField
      FieldName = 'KilometerAwal'
    end
    object viewRealisasiAnjemKilometerAkhir: TIntegerField
      FieldName = 'KilometerAkhir'
    end
    object viewRealisasiAnjemCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewRealisasiAnjemOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewRealisasiAnjemCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewRealisasiAnjemTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewRealisasiAnjemKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Pelanggan'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object viewRealisasiAnjemNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object viewRealisasiAnjemKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Rute'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object viewRealisasiAnjemDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object viewRealisasiAnjemTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object viewRealisasiAnjemNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object viewRealisasiAnjemTanggalMulai: TDateTimeField
      FieldName = 'TanggalMulai'
    end
    object viewRealisasiAnjemTanggalSelesai: TDateTimeField
      FieldName = 'TanggalSelesai'
    end
    object viewRealisasiAnjemSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object viewRealisasiAnjemSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object viewRealisasiAnjemSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object viewRealisasiAnjemSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object viewRealisasiAnjemSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object viewRealisasiAnjemSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object viewRealisasiAnjemSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object viewRealisasiAnjemPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object viewRealisasiAnjemPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object viewRealisasiAnjemTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object viewRealisasiAnjemTol: TCurrencyField
      FieldName = 'Tol'
    end
    object viewRealisasiAnjemBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object viewRealisasiAnjemKeteranganBiayaLainLain: TMemoField
      FieldName = 'KeteranganBiayaLainLain'
      BlobType = ftMemo
    end
    object viewRealisasiAnjemPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object viewRealisasiAnjemPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object viewRealisasiAnjemNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object viewRealisasiAnjemSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
  end
  object SJDS: TDataSource
    DataSet = viewRealisasiAnjem
    Left = 612
    Top = 6
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from rute')
    Left = 633
    Top = 55
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
  end
  object jarakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from jarak')
    Left = 721
    Top = 7
    object jarakQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jarakQDari: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object jarakQKe: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object jarakQJumlahKm: TIntegerField
      FieldName = 'JumlahKm'
      Required = True
    end
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 177
    Top = 7
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Kontrak')
    Left = 480
    Top = 72
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Visible = False
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
      Visible = False
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object KontrakQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object KontrakQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object KontrakQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Kode'
      Size = 50
      Lookup = True
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\toshiba\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'Tahoma'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'Tahoma'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'Tahoma'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'Tahoma'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'Tahoma'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'Tahoma'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'Tahoma'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'Tahoma'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'Tahoma'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'Tahoma'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'Tahoma'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'Tahoma'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 632
    Top = 240
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 672
    Top = 16
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object UangSakuQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select sum(Nominal) as UangSaku from KasBonAnjem where Kontrak=:' +
        'teks and Sopir=:teks2 and status='#39'ON PROCESS'#39)
    Left = 432
    Top = 144
    ParamData = <
      item
        DataType = ftString
        Name = 'teks'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'teks2'
        ParamType = ptInput
      end>
    object UangSakuQUangSaku: TCurrencyField
      FieldName = 'UangSaku'
    end
  end
end
