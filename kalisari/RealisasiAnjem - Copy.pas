unit RealisasiAnjem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxCurrencyEdit;

type
  TRealisasiAnjemFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewRealisasiAnjem: TSDQuery;
    SJDS: TDataSource;
    RuteQ: TSDQuery;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    updateQ: TSDQuery;
    buttonCetak: TcxButton;
    jarakQJumlahKm: TIntegerField;
    deleteBtn: TcxButton;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQKode: TStringField;
    MasterQKontrak: TStringField;
    MasterQPengemudi: TStringField;
    MasterQKilometerAwal: TIntegerField;
    MasterQKilometerAkhir: TIntegerField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    KodeQkode: TStringField;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    MasterQKodeRute: TStringField;
    MasterQRuteDari: TStringField;
    MasterQRuteTujuan: TStringField;
    MasterQKodePelanggan: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQNoTelponPelanggan: TStringField;
    MasterQKodeArmada: TStringField;
    Crpe1: TCrpe;
    viewRealisasiAnjemKode: TStringField;
    viewRealisasiAnjemKontrak: TStringField;
    viewRealisasiAnjemArmada: TStringField;
    viewRealisasiAnjemPengemudi: TStringField;
    viewRealisasiAnjemKilometerAwal: TIntegerField;
    viewRealisasiAnjemKilometerAkhir: TIntegerField;
    viewRealisasiAnjemCreateBy: TStringField;
    viewRealisasiAnjemOperator: TStringField;
    viewRealisasiAnjemCreateDate: TDateTimeField;
    viewRealisasiAnjemTglEntry: TDateTimeField;
    viewRealisasiAnjemKodePelanggan: TStringField;
    viewRealisasiAnjemNamaPelanggan: TStringField;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    viewRealisasiAnjemKodeRute: TStringField;
    viewRealisasiAnjemDari: TStringField;
    viewRealisasiAnjemTujuan: TStringField;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    viewRealisasiAnjemNoPlat: TStringField;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    KontrakQNamaPelanggan: TStringField;
    KontrakQDari: TStringField;
    KontrakQTujuan: TStringField;
    MasterQSPBUAYaniLiter: TFloatField;
    MasterQSPBUAYaniUang: TCurrencyField;
    MasterQSPBUAYaniJam: TDateTimeField;
    MasterQSPBULuarLiter: TFloatField;
    MasterQSPBULuarUang: TCurrencyField;
    MasterQSPBULuarUangDiberi: TCurrencyField;
    MasterQSPBULuarDetail: TMemoField;
    MasterQPremiSopir: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQTabunganSopir: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQBiayaLainLain: TCurrencyField;
    MasterQKeteranganBiayaLainLain: TMemoField;
    MasterQPendapatan: TCurrencyField;
    MasterQPengeluaran: TCurrencyField;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    viewRealisasiAnjemTanggalMulai: TDateTimeField;
    viewRealisasiAnjemTanggalSelesai: TDateTimeField;
    viewRealisasiAnjemSPBUAYaniLiter: TFloatField;
    viewRealisasiAnjemSPBUAYaniUang: TCurrencyField;
    viewRealisasiAnjemSPBUAYaniJam: TDateTimeField;
    viewRealisasiAnjemSPBULuarLiter: TFloatField;
    viewRealisasiAnjemSPBULuarUang: TCurrencyField;
    viewRealisasiAnjemSPBULuarUangDiberi: TCurrencyField;
    viewRealisasiAnjemSPBULuarDetail: TMemoField;
    viewRealisasiAnjemPremiSopir: TCurrencyField;
    viewRealisasiAnjemPremiKernet: TCurrencyField;
    viewRealisasiAnjemTabunganSopir: TCurrencyField;
    viewRealisasiAnjemTol: TCurrencyField;
    viewRealisasiAnjemBiayaLainLain: TCurrencyField;
    viewRealisasiAnjemKeteranganBiayaLainLain: TMemoField;
    viewRealisasiAnjemPendapatan: TCurrencyField;
    viewRealisasiAnjemPengeluaran: TCurrencyField;
    KontrakQIntervalPenagihan: TStringField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    MasterQNamaPengemudi: TStringField;
    viewRealisasiAnjemNamaPengemudi: TStringField;
    MasterQSisaDisetor: TCurrencyField;
    viewRealisasiAnjemSisaDisetor: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    MasterQTanggalMulai: TDateTimeField;
    MasterQTanggalSelesai: TDateTimeField;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridKontrak: TcxDBEditorRow;
    MasterVGridKodePelanggan: TcxDBEditorRow;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridAlamatPelanggan: TcxDBEditorRow;
    MasterVGridNoTelponPelanggan: TcxDBEditorRow;
    MasterVGridKodeRute: TcxDBEditorRow;
    MasterVGridRuteDari: TcxDBEditorRow;
    MasterVGridRuteTujuan: TcxDBEditorRow;
    MasterVGridTanggalMulai: TcxDBEditorRow;
    MasterVGridTanggalSelesai: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridKodeArmada: TcxDBEditorRow;
    MasterVGridPengemudi: TcxDBEditorRow;
    MasterVGridNamaPengemudi: TcxDBEditorRow;
    MasterVGridKilometerAwal: TcxDBEditorRow;
    MasterVGridKilometerAkhir: TcxDBEditorRow;
    MasterVGridCategoryRow4: TcxCategoryRow;
    MasterVGridPendapatan: TcxDBEditorRow;
    MasterVGridCategoryRow5: TcxCategoryRow;
    MasterVGridCategoryRow1: TcxCategoryRow;
    MasterVGridCategoryRow2: TcxCategoryRow;
    MasterVGridSPBUAYaniUang: TcxDBEditorRow;
    MasterVGridSPBUAYaniLiter: TcxDBEditorRow;
    MasterVGridSPBUAYaniJam: TcxDBEditorRow;
    MasterVGridCategoryRow3: TcxCategoryRow;
    MasterVGridSPBULuarUang: TcxDBEditorRow;
    MasterVGridSPBULuarUangDiberi: TcxDBEditorRow;
    MasterVGridSPBULuarLiter: TcxDBEditorRow;
    MasterVGridSPBULuarDetail: TcxDBEditorRow;
    MasterVGridPremiSopir: TcxDBEditorRow;
    MasterVGridPremiKernet: TcxDBEditorRow;
    MasterVGridTabunganSopir: TcxDBEditorRow;
    MasterVGridTol: TcxDBEditorRow;
    MasterVGridBiayaLainLain: TcxDBEditorRow;
    MasterVGridKeteranganBiayaLainLain: TcxDBEditorRow;
    MasterVGridPengeluaran: TcxDBEditorRow;
    MasterVGridSisaDisetor: TcxDBEditorRow;
    MasterQArmada: TStringField;
    UangSakuQ: TSDQuery;
    UangSakuQUangSaku: TCurrencyField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1SopirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKontrakEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPengemudiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridSPBUAYaniUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridSPBULuarUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridPremiSopirEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridPremiKernetEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridTabunganSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridTolEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridBiayaLainLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridSPBUAYaniLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridSPBULuarLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  RealisasiAnjemFm: TRealisasiAnjemFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, StrUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TRealisasiAnjemFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TRealisasiAnjemFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TRealisasiAnjemFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TRealisasiAnjemFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TRealisasiAnjemFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TRealisasiAnjemFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  //MasterQTanggal.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
 // MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1.Enabled:=true;
end;

procedure TRealisasiAnjemFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  //DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TRealisasiAnjemFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  viewRealisasiAnjem.Open;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertSuratJalan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSuratJalan.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteSuratJalan.AsBoolean;
end;

procedure TRealisasiAnjemFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //MasterQKir.AsBoolean :=false;
     // MasterQSTNK.AsBoolean :=false;
     // MasterQPajak.AsBoolean :=false;
     // MasterQStatus.AsString :='IN PROGRESS';
      savebtn.Enabled:=MenuUtamaFm.UserQInsertSuratJalan.AsBoolean;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TRealisasiAnjemFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TRealisasiAnjemFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
 if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;

    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    
    //cxButtonEdit1PropertiesButtonClick(sender,0);

 except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

    //MenuUtamaFm.Database1.Rollback;
   // MasterQ.RollbackUpdates;
    //ShowMessage('Penyimpanan Gagal');


  end;
  KodeEdit.SetFocus;
  viewRealisasiAnjem.Refresh;

end;

procedure TRealisasiAnjemFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TRealisasiAnjemFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;   }
  //MasterQStatus.AsString:='ON GOING';
  {if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;}
end;

procedure TRealisasiAnjemFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Surat Jalan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Surat Jalan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
        viewRealisasiAnjem.Close;
    viewRealisasiAnjem.ExecSQL;
    viewRealisasiAnjem.Open;
    KodeEdit.SetFocus;

  end;
end;

procedure TRealisasiAnjemFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
     // MasterQNoSO.AsString:=SOQkodenota.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
    // MasterQNoSO.AsString:=SOQkodenota.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    //buttonCetak.Enabled:=true;
    KodeEdit.SetFocus;
    KodeEdit.text:=viewRealisasiAnjemKode.AsString;
    //KodeEditExit(self);
    masterq.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    MasterQ.Edit;
    StatusBar.Panels[0].Text:= 'Mode : Edit';
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=true;
    {MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+viewSJQKodenota.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=True; }
end;

procedure TRealisasiAnjemFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //  sopirQ.Close;
   // sopirQ.ExecSQL;
  //  sopirQ.Open;
   // DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQAwal.AsString := jarakQkode.AsString;
      MasterQjarak_dari.AsString := jarakQDari.AsString;
      MasterQjarak_ke.AsString := jarakQKe.AsString;
      MasterQjarak_jarak.AsString := jarakQJumlahKM.AsString;
    end;
    DropDownFm.Release; }
end;

procedure TRealisasiAnjemFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Surat Jalan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      viewRealisasiAnjem.Close;
      viewRealisasiAnjem.Open;
    end;
    //end Cetak SJ
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1SopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   //sopirQ.Close;
   // sopirQ.ExecSQL;
  //  sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
     // MasterQSopir.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //  sopirQ.Close;
   // sopirQ.ExecSQL;
  //  sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir2.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridKontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL : string;
  begin
KontrakQ.Close;
tempSQL:=KontrakQ.SQL.Text;
KontrakQ.SQL.Text:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="ANJEM"';
//KontrakQ.ExecSQL;
KontrakQ.Open;
DropDownFm:=TDropDownFm.Create(self,KontrakQ);
if DropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakQKode.AsString;
end;
DropDownFm.Release;
KontrakQ.SQL.Text:=tempSQL;

if MasterQPengemudi.AsString<>'' then
begin
    UangSakuQ.Close;
    UangSakuQ.ParamByName('teks').AsString:=MasterQKontrak.AsString;
    UangSakuQ.ParamByName('teks2').AsString:=MasterQPengemudi.AsString;
    UangSakuQ.ExecSQL;
    UangSakuQ.Open;
    MasterQPendapatan.AsCurrency:=UangSakuQUangSaku.AsCurrency;
end;

end;

procedure TRealisasiAnjemFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
ArmadaQ.Close;
ArmadaQ.ExecSQL;
ArmadaQ.Open;
DropDownFm:=TDropDownFm.Create(self,ArmadaQ);
if DropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:=ArmadaQKode.AsString;
end;
DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridPengemudiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL : string;
  begin
 pegawaiQ.Close;
  tempSQL:=PegawaiQ.SQL.Text;
  PegawaiQ.SQL.Text:= 'select * from pegawai where upper(jabatan)="PENGEMUDI"';
  pegawaiQ.Open;
//sopirQ.Close;
//sopirQ.ExecSQL;
//sopirQ.Open;
DropDownFm:=TDropDownFm.Create(self,PegawaiQ);
if DropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
   MasterQPengemudi.AsString:=PegawaiQKode.AsString;
end;
DropDownFm.Release;
pegawaiQ.SQL.Text:=tempSQL;

if MasterQKontrak.AsString<>'' then
begin
    UangSakuQ.Close;
    UangSakuQ.ParamByName('teks').AsString:=MasterQKontrak.AsString;
    UangSakuQ.ParamByName('teks2').AsString:=MasterQPengemudi.AsString;
    UangSakuQ.ExecSQL;
    UangSakuQ.Open;
    MasterQPendapatan.AsCurrency:=UangSakuQUangSaku.AsCurrency;
end;


end;

procedure TRealisasiAnjemFm.MasterVGridSPBUAYaniUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniUang.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridSPBULuarUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarUang.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridPremiSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiSopir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridPremiKernetEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiKernet.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridTabunganSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTabunganSopir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridTolEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTol.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridBiayaLainLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaLainLain.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL:string;
  begin
KontrakQ.Close;
tempSQL:=KontrakQ.SQL.Text;
KontrakQ.SQL.Text:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="ANJEM"';
//KontrakQ.ExecSQL;
KontrakQ.Open;
DropDownFm:=TDropDownFm.Create(self,KontrakQ);
if DropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakQKode.AsString;
end;
DropDownFm.Release;
KontrakQ.SQL.Text:=tempSQL;
end;

procedure TRealisasiAnjemFm.MasterVGridSPBUAYaniLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniLiter.AsInteger:=DisplayValue;
MasterQSPBUAYaniUang.AsCurrency:=MasterQSPBUAYaniLiter.AsInteger*4500;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridSPBULuarLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarLiter.AsInteger:=DisplayValue;
MasterQSPBULuarUang.AsCurrency:=MasterQSPBULuarLiter.AsInteger*4500;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

end.
