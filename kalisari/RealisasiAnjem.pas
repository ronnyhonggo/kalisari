unit RealisasiAnjem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxCurrencyEdit,
  cxDropDownEdit, cxGroupBox, cxCalc;

type
  TRealisasiAnjemFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewRealisasiAnjem: TSDQuery;
    SJDS: TDataSource;
    RuteQ: TSDQuery;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    updateQ: TSDQuery;
    buttonCetak: TcxButton;
    jarakQJumlahKm: TIntegerField;
    deleteBtn: TcxButton;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    KodeQkode: TStringField;
    KontrakQ: TSDQuery;
    Crpe1: TCrpe;
    viewRealisasiAnjemKode: TStringField;
    viewRealisasiAnjemKontrak: TStringField;
    viewRealisasiAnjemArmada: TStringField;
    viewRealisasiAnjemPengemudi: TStringField;
    viewRealisasiAnjemKilometerAwal: TIntegerField;
    viewRealisasiAnjemKilometerAkhir: TIntegerField;
    viewRealisasiAnjemCreateBy: TStringField;
    viewRealisasiAnjemOperator: TStringField;
    viewRealisasiAnjemCreateDate: TDateTimeField;
    viewRealisasiAnjemTglEntry: TDateTimeField;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    viewRealisasiAnjemTanggalMulai: TDateTimeField;
    viewRealisasiAnjemTanggalSelesai: TDateTimeField;
    viewRealisasiAnjemSPBUAYaniLiter: TFloatField;
    viewRealisasiAnjemSPBUAYaniUang: TCurrencyField;
    viewRealisasiAnjemSPBUAYaniJam: TDateTimeField;
    viewRealisasiAnjemSPBULuarLiter: TFloatField;
    viewRealisasiAnjemSPBULuarUang: TCurrencyField;
    viewRealisasiAnjemSPBULuarUangDiberi: TCurrencyField;
    viewRealisasiAnjemSPBULuarDetail: TMemoField;
    viewRealisasiAnjemPremiSopir: TCurrencyField;
    viewRealisasiAnjemPremiKernet: TCurrencyField;
    viewRealisasiAnjemTabunganSopir: TCurrencyField;
    viewRealisasiAnjemTol: TCurrencyField;
    viewRealisasiAnjemBiayaLainLain: TCurrencyField;
    viewRealisasiAnjemKeteranganBiayaLainLain: TMemoField;
    viewRealisasiAnjemPendapatan: TCurrencyField;
    viewRealisasiAnjemPengeluaran: TCurrencyField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    viewRealisasiAnjemSisaDisetor: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridKontrak: TcxDBEditorRow;
    MasterVGridKodePelanggan: TcxDBEditorRow;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridAlamatPelanggan: TcxDBEditorRow;
    MasterVGridNoTelponPelanggan: TcxDBEditorRow;
    MasterVGridKodeRute: TcxDBEditorRow;
    MasterVGridRuteDari: TcxDBEditorRow;
    MasterVGridRuteTujuan: TcxDBEditorRow;
    MasterVGridTanggalMulai: TcxDBEditorRow;
    MasterVGridTanggalSelesai: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridKodeArmada: TcxDBEditorRow;
    MasterVGridPengemudi: TcxDBEditorRow;
    MasterVGridNamaPengemudi: TcxDBEditorRow;
    MasterVGridKilometerAwal: TcxDBEditorRow;
    MasterVGridKilometerAkhir: TcxDBEditorRow;
    MasterVGridCategoryRow4: TcxCategoryRow;
    MasterVGridCategoryRow5: TcxCategoryRow;
    MasterVGridCategoryRow1: TcxCategoryRow;
    MasterVGridCategoryRow2: TcxCategoryRow;
    MasterVGridSPBUAYaniUang: TcxDBEditorRow;
    MasterVGridSPBUAYaniLiter: TcxDBEditorRow;
    MasterVGridSPBUAYaniJam: TcxDBEditorRow;
    MasterVGridCategoryRow3: TcxCategoryRow;
    MasterVGridSPBULuarUang: TcxDBEditorRow;
    MasterVGridSPBULuarUangDiberi: TcxDBEditorRow;
    MasterVGridSPBULuarLiter: TcxDBEditorRow;
    MasterVGridSPBULuarDetail: TcxDBEditorRow;
    MasterVGridPremiSopir: TcxDBEditorRow;
    MasterVGridPremiKernet: TcxDBEditorRow;
    MasterVGridTabunganSopir: TcxDBEditorRow;
    MasterVGridTol: TcxDBEditorRow;
    MasterVGridBiayaLainLain: TcxDBEditorRow;
    MasterVGridKeteranganBiayaLainLain: TcxDBEditorRow;
    MasterVGridPengeluaran: TcxDBEditorRow;
    MasterVGridSisaDisetor: TcxDBEditorRow;
    UangSakuQ: TSDQuery;
    UangSakuQUangSaku: TCurrencyField;
    MasterVGridPendapatan1: TcxDBEditorRow;
    MasterVGridUangSaku: TcxDBEditorRow;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    viewRealisasiAnjemnamaPT: TStringField;
    MasterVGridSPBUAYaniDetail: TcxDBEditorRow;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQPOEksternal: TStringField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    KontrakQPlatNo: TStringField;
    MasterVGridHargaBBM: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxGrid1DBTableView1Pembuat: TcxGridDBColumn;
    viewRealisasiAnjemPembuat: TStringField;
    viewRealisasiAnjemTanggal: TStringField;
    viewRealisasiAnjemSPBUAYaniDetail: TStringField;
    MasterVGridVerifikasi: TcxDBEditorRow;
    MasterVGridKeteranganVerifikasi: TcxDBEditorRow;
    viewRealisasiAnjemVerifikasi: TCurrencyField;
    viewRealisasiAnjemKeteranganVerifikasi: TMemoField;
    cxGrid1DBTableView1Verifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganVerifikasi: TcxGridDBColumn;
    updateQUangSaku: TCurrencyField;
    viewRealisasiAnjemKodePelanggan: TStringField;
    viewRealisasiAnjemNamaPelanggan: TStringField;
    viewRealisasiAnjemKodeRute: TStringField;
    viewRealisasiAnjemDari: TStringField;
    viewRealisasiAnjemTujuan: TStringField;
    viewRealisasiAnjemNoPlat: TStringField;
    viewRealisasiAnjemNamaPengemudi: TStringField;
    HargaBBMQ: TSDQuery;
    HargaBBMQKode: TStringField;
    HargaBBMQHargaSolar: TCurrencyField;
    HargaBBMQHargaBensin: TCurrencyField;
    MasterVGridDanaKebersihan: TcxDBEditorRow;
    VeriBtn: TcxButton;
    MasterVGridStatus: TcxDBEditorRow;
    MasterQKode: TStringField;
    MasterQKontrak: TStringField;
    MasterQTanggalMulai: TDateTimeField;
    MasterQTanggalSelesai: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQPengemudi: TStringField;
    MasterQKilometerAwal: TIntegerField;
    MasterQKilometerAkhir: TIntegerField;
    MasterQPendapatan: TCurrencyField;
    MasterQPengeluaran: TCurrencyField;
    MasterQSisaDisetor: TCurrencyField;
    MasterQSPBUAYaniLiter: TFloatField;
    MasterQSPBUAYaniUang: TCurrencyField;
    MasterQSPBUAYaniJam: TDateTimeField;
    MasterQSPBULuarLiter: TFloatField;
    MasterQSPBULuarUang: TCurrencyField;
    MasterQSPBULuarUangDiberi: TCurrencyField;
    MasterQSPBULuarDetail: TMemoField;
    MasterQPremiSopir: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQTabunganSopir: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQBiayaLainLain: TCurrencyField;
    MasterQKeteranganBiayaLainLain: TMemoField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    MasterQSPBUAYaniDetail: TStringField;
    MasterQVerifikasi: TCurrencyField;
    MasterQKeteranganVerifikasi: TMemoField;
    MasterQDanaKebersihan: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQUangSaku: TCurrencyField;
    MasterQHargaBBM: TCurrencyField;
    MasterQKodeRute: TStringField;
    MasterQRuteDari: TStringField;
    MasterQRuteTujuan: TStringField;
    MasterQKodePelanggan: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQNoTelponPelanggan: TStringField;
    MasterQKodeArmada: TStringField;
    MasterQNamaPengemudi: TStringField;
    MasterQJenisBBM: TStringField;
    MasterQKernet: TStringField;
    MasterQKeterangan: TMemoField;
    MasterVGridKernet: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    cxGrid1DBTableView1CreateDate: TcxGridDBColumn;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    cxButton1: TcxButton;
   // procedure CreateParams(var Params: TCreateParams); overload;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure buttonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1SopirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKontrakEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPengemudiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridSPBUAYaniUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridSPBULuarUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridPremiSopirEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridPremiKernetEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridTabunganSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridTolEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridBiayaLainLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridSPBUAYaniLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridSPBULuarLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridUangSakuEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure MasterVGridTanggalSelesaiEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridDanaKebersihanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure VeriBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
   procedure CreateParams(var Params: TCreateParams); override;
   procedure WMSize(var Msg: TMessage); message WM_SIZE;
   //Procedure sizeMove (var msg: TWMSize); message WM_SIZE;
   //Procedure resizeQ();
    //Procedure sizeMove (var msg: TWMSize); message WM_SIZE;

      { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
    //procedure CreateParams(var Params: TCreateParams); override;
  end;

var
  RealisasiAnjemFm: TRealisasiAnjemFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, StrUtils, KontrakDropDown, ArmadaDropDown, PengemudiDropDown,
  Pivot;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TRealisasiAnjemFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TRealisasiAnjemFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  //FormStyle:=fsNormal;
  if (FormStyle = fsStayOnTop) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end;

procedure TRealisasiAnjemFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(RealisasiAnjemFm.Handle, SW_RESTORE) ;
end;

{procedure TRealisasiAnjemFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    ExStyle := ExStyle or WS_EX_APPWINDOW;
    WndParent := GetDesktopWindow;
  end;
end;         }

{procedure TRealisasiAnjemFm.WMSysCommand(var Msg: TWMSysCommand);
begin
  if Msg.CmdType = SC_MINIMIZE then
    ShowWindow(Handle, SW_MINIMIZE);
        Application.Initialize;
Application.CreateForm(TRealisasiAnjemFm, RealisasiAnjemFm);
ShowWindow(Application.Handle, SW_HIDE);
Application.Run;
  else
    inherited;

end; }

{Procedure TRealisasiAnjemFm.sizeMove (var msg: TWMSize);
begin 
 inherited; 
 if (msg.SizeType = SIZE_MAXIMIZED) OR (msg.SizeType = SIZE_RESTORED)then   
  resizeQlikViewReports(); 
end;  }

{procedure TRealisasiAnjemFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;    }

procedure TRealisasiAnjemFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TRealisasiAnjemFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;
end;

procedure TRealisasiAnjemFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TRealisasiAnjemFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  //MasterQTanggal.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
 // MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  //cxDBVerticalGrid1.Enabled:=true;
end;

procedure TRealisasiAnjemFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TRealisasiAnjemFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-3;
  viewRealisasiAnjem.Close;
  viewRealisasiAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewRealisasiAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewRealisasiAnjem.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertRealisasiAnjem.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiAnjem.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiAnjem.AsBoolean;
  VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRKaryawan.AsBoolean;
  HargaBBMQ.Open;
end;

procedure TRealisasiAnjemFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQPendapatan.AsCurrency:=0;
      MasterQPengeluaran.AsCurrency:=0;
      MasterQSisaDisetor.AsCurrency:=0;
      MasterQHargaBBM.AsCurrency:=0;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiAnjem.AsBoolean;
      MasterQ.Edit;
      if currtostr(MasterQTabunganSopir.AsCurrency)='' then
      begin
          MasterQTabunganSopir.AsCurrency:=0;
      end;
      MasterQUangSaku.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQTabunganSopir.AsCurrency;

    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiAnjem.AsBoolean;
    MasterVGrid.Enabled:=True;
    if MasterQStatus.AsString='VERIFIED' then
    begin
      MasterVGrid.OptionsData.Editing:=False;
      SaveBtn.Enabled:=False;
      deleteBtn.Enabled:=False;
      VeriBtn.Enabled:=False;
    end
    else
    begin
      MasterVGrid.OptionsData.Editing:=True;
      SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiAnjem.AsBoolean;
      deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiAnjem.AsBoolean;
      VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRKaryawan.AsBoolean;
    end;
  end;
end;

procedure TRealisasiAnjemFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TRealisasiAnjemFm.SaveBtnClick(Sender: TObject);
var sKode,kode :String;
begin
 if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(MasterQStatus.AsString);
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  kode:=MasterQKode.AsString;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;

    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');

    //cxButtonEdit1PropertiesButtonClick(sender,0);

 except
  on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
 end;
  KodeEdit.SetFocus;
     //cetak SJ
    if MessageDlg('Cetak Realisasi Karyawan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiAnjem.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kode;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     { Crpe1.Print;
      Crpe1.CloseWindow;}
     { updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;  }
      viewRealisasiAnjem.Close;
      viewRealisasiAnjem.Open;
    end;
    //end Cetak SJ
  viewRealisasiAnjem.Refresh;

end;

procedure TRealisasiAnjemFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TRealisasiAnjemFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;   }
  //MasterQStatus.AsString:='ON GOING';
  {if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;}
end;

procedure TRealisasiAnjemFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Surat Jalan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Realisasi telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Realisasi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
        viewRealisasiAnjem.Close;
    viewRealisasiAnjem.ExecSQL;
    viewRealisasiAnjem.Open;
    KodeEdit.SetFocus;

  end;
end;

procedure TRealisasiAnjemFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
     // MasterQNoSO.AsString:=SOQkodenota.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
    // MasterQNoSO.AsString:=SOQkodenota.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //buttonCetak.Enabled:=true;
  KodeEdit.SetFocus;
  KodeEdit.text:=viewRealisasiAnjemKode.AsString;
  KodeEditExit(self);

  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiAnjem.AsBoolean;
  buttonCetak.Enabled:=true;
  MasterVGrid.Enabled:=True;
  MasterQ.Edit;
  if currtostr(MasterQTabunganSopir.AsCurrency)='' then
  begin
    MasterQTabunganSopir.AsCurrency:=0;
  end;
  MasterQUangSaku.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQTabunganSopir.AsCurrency;
  if MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency<0 then
  begin
   MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
  end;
  if MasterQStatus.AsString='VERIFIED' then
  begin
    MasterVGrid.OptionsData.Editing:=False;
    SaveBtn.Enabled:=False;
    deleteBtn.Enabled:=False;
    VeriBtn.Enabled:=False;
  end
  else
  begin
    MasterVGrid.OptionsData.Editing:=True;
    SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiAnjem.AsBoolean;
    deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiAnjem.AsBoolean;
    VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRKaryawan.AsBoolean;
  end;
end;

procedure TRealisasiAnjemFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Realisasi Karyawan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiAnjem.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
      {Crpe1.Print;
      Crpe1.CloseWindow;}
      viewRealisasiAnjem.Close;
      viewRealisasiAnjem.Open;
    end;
    //end Cetak SJ
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1SopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   //sopirQ.Close;
   // sopirQ.ExecSQL;
  //  sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
     // MasterQSopir.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //  sopirQ.Close;
   // sopirQ.ExecSQL;
  //  sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir2.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiAnjemFm.MasterVGridKontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL,a : string;
  begin
{KontrakQ.Close;
tempSQL:=KontrakQ.SQL.Text;
KontrakQ.SQL.Text:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"';
//KontrakQ.ExecSQL;
KontrakQ.Open;  }
a:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="KARYAWAN"';
KontrakDropDownFm:=TKontrakDropDownFm.Create(self,a);
if KontrakDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakDropDownFm.kode;
    KontrakQ.Close;
    KontrakQ.ParamByName('text').AsString:=MasterQKontrak.AsString;
    KontrakQ.Open;
    MasterQKodePelanggan.AsString:=KontrakQPelanggan.AsString;
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=MasterQKodePelanggan.AsString;
    PelangganQ.Open;
    MasterQNamaPelanggan.AsString:=PelangganQNamaPT.AsString;
    MasterQAlamatPelanggan.AsString:=PelangganQAlamat.AsString;
    MasterQNoTelponPelanggan.AsString:=PelangganQNoTelp.AsString;
    MasterQKodeRute.AsString:=KontrakQRute.AsString;
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=MasterQKodeRute.AsString;
    RuteQ.Open;
    MasterQRuteDari.AsString:=RuteQMuat.AsString;
    MasterQRuteTujuan.AsString:=RuteQBongkar.AsString;
end;
KontrakDropDownFm.Release;
//KontrakQ.SQL.Text:=tempSQL;

if MasterQPengemudi.AsString<>'' then
begin
    UangSakuQ.Close;
    UangSakuQ.ParamByName('teks').AsString:=MasterQKontrak.AsString;
    UangSakuQ.ParamByName('teks2').AsString:=MasterQPengemudi.AsString;
    //UangSakuQ.ExecSQL;
    UangSakuQ.Open;
      MasterQ.Edit;
    MasterQUangSaku.AsCurrency:=UangSakuQUangSaku.AsCurrency+0;
    MasterQPendapatan.AsCurrency:=MasterQUangSaku.AsCurrency+MasterQTabunganSopir.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

end;

procedure TRealisasiAnjemFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{ArmadaQ.Close;
ArmadaQ.ExecSQL;
ArmadaQ.Open;  }
ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
if ArmadaDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:=ArmadaDropDownFm.Kode;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=MasterQArmada.AsString;
    ArmadaQ.Open;
    MasterQKodeArmada.AsString:=ArmadaQPlatNo.AsString;
    MasterQJenisBBM.AsString:=ArmadaQJenisBBM.AsString;
end;
ArmadaDropDownFm.Release;
      if MasterQJenisBBM.AsString='BENSIN' then
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=HargaBBMQHargaBensin.AsCurrency
      end

      else if MasterQJenisBBM.AsString='SOLAR' then
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=HargaBBMQHargaSolar.AsCurrency;
      end

      else
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=0;
      end;
end;

procedure TRealisasiAnjemFm.MasterVGridPengemudiEditPropertiesButtonClick(
Sender: TObject; AButtonIndex: Integer);
var tempSQL : string;
begin
  PengemudiDropDownFm:=TPengemudiDropDownFm.Create(self);
  if PengemudiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPengemudi.AsString:=PengemudiDropDownFm.kode;
    PegawaiQ.Close;
    PegawaiQ.ParamByName('text').AsString:=MasterQPengemudi.AsString;
    PegawaiQ.Open;
    MasterQNamaPengemudi.AsString:=PegawaiQNama.AsString;
  end;
  PengemudiDropDownFm.Release;
  if (MasterQTanggalMulai.AsString<>'') AND (MasterQTanggalSelesai.AsString<>'') then
    begin
      updateQ.Close;
      updateQ.ParamByName('kon').AsString:=MasterQKontrak.AsString;
      updateQ.ParamByName('tgl1').AsDateTime:=MasterQTanggalMulai.AsDateTime;
      updateQ.ParamByName('tgl2').AsDateTime:=MasterQTanggalSelesai.AsDateTime;
      updateQ.ParamByName('spr').AsString:=MasterQPengemudi.AsString;
      updateQ.Open;
      MasterQUangSaku.AsCurrency:=updateQUangSaku.AsCurrency+0;
      MasterQPendapatan.AsCurrency:=MasterQUangSaku.AsCurrency+MasterQTabunganSopir.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiAnjemFm.MasterVGridSPBUAYaniUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniUang.AsCurrency:=DisplayValue;
//MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

//MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridSPBULuarUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarUang.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.MasterVGridPremiSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiSopir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.MasterVGridPremiKernetEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQPremiKernet.AsCurrency:=DisplayValue;
  MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.MasterVGridTabunganSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTabunganSopir.AsCurrency:=DisplayValue;
  MasterQPendapatan.AsCurrency:=MasterQUangSaku.AsCurrency+MasterQTabunganSopir.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;

end;

procedure TRealisasiAnjemFm.MasterVGridTolEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTol.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.MasterVGridBiayaLainLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaLainLain.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.cxDBVerticalGrid1KontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL:string;
  begin
KontrakQ.Close;
tempSQL:=KontrakQ.SQL.Text;
KontrakQ.SQL.Text:='select k.* from Kontrak k left join Rute r on k.Rute=r.Kode where  upper(r.Kategori)="ANJEM"';
//KontrakQ.ExecSQL;
KontrakQ.Open;
DropDownFm:=TDropDownFm.Create(self,KontrakQ);
if DropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakQKode.AsString;
end;
DropDownFm.Release;
KontrakQ.SQL.Text:=tempSQL;
end;

procedure TRealisasiAnjemFm.MasterVGridSPBUAYaniLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniLiter.AsFloat:=DisplayValue;
MasterQSPBUAYaniUang.AsCurrency:=MasterQSPBUAYaniLiter.AsFloat*MasterQHargaBBM.AsCurrency;
//MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

//MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiAnjemFm.MasterVGridSPBULuarLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarLiter.AsFloat:=DisplayValue;
MasterQSPBULuarUang.AsCurrency:=MasterQSPBULuarLiter.AsFloat*MasterQHargaBBM.AsCurrency;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.MasterVGridUangSakuEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQUangSaku.AsCurrency:=DisplayValue;
end;

procedure TRealisasiAnjemFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewRealisasiAnjem.Close;
  viewRealisasiAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewRealisasiAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewRealisasiAnjem.Open;
end;

procedure TRealisasiAnjemFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewRealisasiAnjem.Close;
  viewRealisasiAnjem.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewRealisasiAnjem.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewRealisasiAnjem.Open;
end;

procedure TRealisasiAnjemFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[8] > 0) then
  begin
    ACanvas.Brush.Color := 16706935;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

procedure TRealisasiAnjemFm.MasterVGridTanggalSelesaiEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQTanggalSelesai.AsString:=DisplayValue;
  if MasterQTanggalMulai.AsString='' then
    begin
      ShowMessage('Masukkan tanggal mulai terlebih dahulu.');
      MasterQTanggalSelesai.Clear;
    end
  else if MasterQPengemudi.AsString<>'' then
    begin
      updateQ.Close;
      updateQ.ParamByName('kon').AsString:=MasterQKontrak.AsString;
      updateQ.ParamByName('tgl1').AsString:=MasterQTanggalMulai.AsString;
      updateQ.ParamByName('tgl2').AsString:=MasterQTanggalSelesai.AsString;
      updateQ.ParamByName('spr').AsString:=MasterQPengemudi.AsString;
      updateQ.Open;
      MasterQUangSaku.AsCurrency:=updateQUangSaku.AsCurrency+0;
      MasterQPendapatan.AsCurrency:=MasterQUangSaku.AsCurrency+MasterQTabunganSopir.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiAnjemFm.MasterVGridDanaKebersihanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQDanaKebersihan.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
end

else
begin
MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiAnjemFm.VeriBtnClick(Sender: TObject);
begin
  MasterQ.Edit;
  MasterQStatus.AsString:='VERIFIED';
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Verifikasi Berhasil');
 except
  on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Verifikasi Gagal');
    end;
 end;
  KodeEdit.SetFocus;
end;

procedure TRealisasiAnjemFm.Button1Click(Sender: TObject);
begin
if ComboBox1.ItemIndex=0 then
begin
   MasterQ.Edit;
   MasterQPremiSopir.AsCurrency:=strtoint(Edit1.Text)*105000;
   MasterQPremiKernet.AsCurrency:=strtoint(Edit1.Text)*57000;
   MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
   if MasterQSisaDisetor.AsCurrency<0 then
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
   end

   else
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
   end;
end

else if ComboBox1.ItemIndex=1 then
begin
   MasterQ.Edit;
   MasterQPremiSopir.AsCurrency:=strtoint(Edit1.Text)*105000;
   MasterQPremiKernet.AsCurrency:=0;
   MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
   if MasterQSisaDisetor.AsCurrency<0 then
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
   end

   else
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
   end;
end

else
begin
   MasterQ.Edit;
   MasterQPremiSopir.AsCurrency:=strtoint(Edit1.Text)*130000;
   MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQDanaKebersihan.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
   if MasterQSisaDisetor.AsCurrency<0 then
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clRed;
   end

   else
   begin
      MasterVGridSisaDisetor.Styles.Content.TextColor:=clDefault;
   end;
end;
end;

procedure TRealisasiAnjemFm.cxButton1Click(Sender: TObject);
begin
if MessageDlg('Cetak Kitir Realisasi Karyawan '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
begin
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KitirRealisasiAnjem.rpt';
    Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    end;
end;

{procedure TRealisasiAnjemFm.CreateParams(var Params: TCreateParams);
begin
  INHERITED;
  if (FormStyle = fsNormal) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end; }

end.
