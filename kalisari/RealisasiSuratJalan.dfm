object RealisasiSuratJalanFm: TRealisasiSuratJalanFm
  Left = 139
  Top = 47
  AutoScroll = False
  Caption = 'Realisasi Surat Jalan'
  ClientHeight = 595
  ClientWidth = 1087
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1087
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 16
      Caption = 'Kode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 56
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
          Visible = False
        end>
      Properties.MaxLength = 0
      Properties.ReadOnly = False
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      OnKeyPress = KodeEditKeyPress
      Width = 121
    end
    object Button1: TButton
      Left = 200
      Top = 10
      Width = 75
      Height = 21
      Caption = 'Hitung Premi'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 519
    Width = 1087
    Height = 51
    Align = alBottom
    TabOrder = 1
    object SaveBtn: TcxButton
      Left = 17
      Top = 8
      Width = 80
      Height = 33
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 536
      Top = 8
      Width = 97
      Height = 32
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object cxButtonCetak: TcxButton
      Left = 328
      Top = 9
      Width = 97
      Height = 32
      Caption = 'CETAK'
      TabOrder = 2
      OnClick = cxButtonCetakClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 789
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 3
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
    object VeriBtn: TcxButton
      Left = 213
      Top = 9
      Width = 97
      Height = 32
      Caption = 'VERIFIKASI'
      TabOrder = 4
      OnClick = VeriBtnClick
    end
    object cxButton1: TcxButton
      Left = 432
      Top = 9
      Width = 97
      Height = 32
      Caption = 'CETAK KITIR'
      TabOrder = 5
      OnClick = cxButton1Click
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 339
    Width = 1087
    Height = 180
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    TabStop = False
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      DataController.DataSource = ViewDs
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Position = spFooter
          Column = cxGrid1DBTableView1Column7
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Tgl'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.ShowTime = False
        Width = 91
      end
      object cxGrid1DBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'NoSO'
        Width = 82
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        Caption = 'NamaPT'
        DataBinding.FieldName = 'namaPT'
        Options.SortByDisplayText = isbtOn
        Width = 197
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 133
      end
      object cxGrid1DBTableView1KeteranganRute: TcxGridDBColumn
        DataBinding.FieldName = 'KeteranganRute'
        Width = 125
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'TglMulai'
        Width = 107
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'TglKembali'
        Width = 98
      end
      object cxGrid1DBTableView1TglEntry: TcxGridDBColumn
        DataBinding.FieldName = 'TglEntry'
        Width = 102
      end
      object cxGrid1DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'platno'
        Width = 127
      end
      object cxGrid1DBTableView1Column8: TcxGridDBColumn
        DataBinding.FieldName = 'JumlahSeat'
        Width = 88
      end
      object cxGrid1DBTableView1Pembuat: TcxGridDBColumn
        DataBinding.FieldName = 'LastEditor'
        Width = 112
      end
      object cxGrid1DBTableView1NamaPembuat: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPembuat'
        Visible = False
      end
      object cxGrid1DBTableView1Verifikasi: TcxGridDBColumn
        DataBinding.FieldName = 'Verifikasi'
        Visible = False
      end
      object cxGrid1DBTableView1KeteranganVerifikasi: TcxGridDBColumn
        DataBinding.FieldName = 'KeteranganVerifikasi'
        Visible = False
      end
      object cxGrid1DBTableView1SudahPrint: TcxGridDBColumn
        DataBinding.FieldName = 'SudahPrint'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 570
    Width = 1087
    Height = 25
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1087
    Height = 291
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 4
    object Panel3: TPanel
      Left = 305
      Top = 1
      Width = 781
      Height = 289
      Align = alClient
      Caption = 'Panel1'
      TabOrder = 0
      object cxDBVerticalGrid2: TcxDBVerticalGrid
        Left = 305
        Top = 1
        Width = 475
        Height = 287
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsView.RowHeaderWidth = 210
        OptionsBehavior.GoToNextCellOnTab = True
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = MasterDs
        Version = 1
        object cxDBVerticalGrid2CategoryRow1: TcxCategoryRow
          Properties.Caption = 'Pemasukan'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2UangJalan: TcxDBEditorRow
          Properties.Caption = 'UangSaku'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.ReadOnly = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2UangJalanEditPropertiesValidate
          Properties.DataBinding.FieldName = 'UangJalan2'
          Properties.Options.Editing = False
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2NominalKwitansi: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2NominalKwitansiEditPropertiesValidate
          Properties.DataBinding.FieldName = 'NominalKwitansi'
          Properties.Options.Editing = False
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2Pendapatan: TcxDBEditorRow
          Properties.Caption = 'TotalPendapatan'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.DataBinding.FieldName = 'Pendapatan'
          Properties.Options.Editing = False
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2CategoryRow2: TcxCategoryRow
          Properties.Caption = 'Pengeluaran'
          ID = 4
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2CategoryRow4: TcxCategoryRow
          Properties.Caption = 'BBM'
          ID = 5
          ParentID = 4
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2HargaBBM: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2HargaBBMEditPropertiesValidate
          Properties.DataBinding.FieldName = 'HargaBBM'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 6
          ParentID = 5
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2CategoryRow3: TcxCategoryRow
          Properties.Caption = 'SPBU A. Yani'
          ID = 7
          ParentID = 5
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2SPBUAYaniLiter: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCalcEditProperties'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2SPBUAYaniLiterEditPropertiesValidate
          Properties.DataBinding.FieldName = 'SPBUAYaniLiter'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 8
          ParentID = 7
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2SPBUAYaniUang: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2SPBUAYaniUangEditPropertiesValidate
          Properties.DataBinding.FieldName = 'SPBUAYaniUang'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 9
          ParentID = 7
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2SPBUAYaniJam: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxDateEditProperties'
          Properties.EditProperties.Kind = ckDateTime
          Properties.DataBinding.FieldName = 'SPBUAYaniJam'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 10
          ParentID = 7
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2SPBUAYaniDetail: TcxDBEditorRow
          Height = 53
          Properties.DataBinding.FieldName = 'SPBUAYaniDetail'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 11
          ParentID = 7
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid2CategoryRow5: TcxCategoryRow
          Properties.Caption = 'SPBU Luar'
          ID = 12
          ParentID = 5
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2SPBULuarLiter: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCalcEditProperties'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2SPBULuarLiterEditPropertiesValidate
          Properties.DataBinding.FieldName = 'SPBULuarLiter'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 13
          ParentID = 12
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2SPBULuarUang: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2SPBULuarUangEditPropertiesValidate
          Properties.DataBinding.FieldName = 'SPBULuarUang'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 14
          ParentID = 12
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2SPBULuarUangDiberi: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2SPBULuarUangDiberiEditPropertiesValidate
          Properties.DataBinding.FieldName = 'SPBULuarUangDiberi'
          Visible = False
          ID = 15
          ParentID = 12
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2SPBULuarDetail: TcxDBEditorRow
          Height = 52
          Properties.DataBinding.FieldName = 'SPBULuarDetail'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 16
          ParentID = 12
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid2PremiSopir: TcxDBEditorRow
          Properties.Caption = 'PremiPengemudi'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2PremiSopirEditPropertiesValidate
          Properties.DataBinding.FieldName = 'PremiSopir'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 17
          ParentID = 4
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2TabunganSopir: TcxDBEditorRow
          Properties.Caption = 'TabunganPengemudi'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2TabunganSopirEditPropertiesValidate
          Properties.DataBinding.FieldName = 'TabunganSopir'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 18
          ParentID = 4
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2PremiSopir2: TcxDBEditorRow
          Properties.Caption = 'PremiPengemudi2'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2PremiSopir2EditPropertiesValidate
          Properties.DataBinding.FieldName = 'PremiSopir2'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 19
          ParentID = 4
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid2TabunganSopir2: TcxDBEditorRow
          Properties.Caption = 'TabunganPengemudi2'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2TabunganSopir2EditPropertiesValidate
          Properties.DataBinding.FieldName = 'TabunganSopir2'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 20
          ParentID = 4
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid2PremiKernet: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2PremiKernetEditPropertiesValidate
          Properties.DataBinding.FieldName = 'PremiKernet'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 21
          ParentID = 4
          Index = 5
          Version = 1
        end
        object cxDBVerticalGrid2tol: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2tolEditPropertiesValidate
          Properties.DataBinding.FieldName = 'tol'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 22
          ParentID = 4
          Index = 6
          Version = 1
        end
        object cxDBVerticalGrid2DanaKebersihan: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2DanaKebersihanEditPropertiesValidate
          Properties.DataBinding.FieldName = 'DanaKebersihan'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 23
          ParentID = 4
          Index = 7
          Version = 1
        end
        object cxDBVerticalGrid2ClaimSopir: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2ClaimSopirEditPropertiesValidate
          Properties.DataBinding.FieldName = 'ClaimSopir'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 24
          ParentID = 4
          Index = 8
          Version = 1
        end
        object cxDBVerticalGrid2KeteranganClaimSopir: TcxDBEditorRow
          Height = 49
          Properties.EditPropertiesClassName = 'TcxMemoProperties'
          Properties.DataBinding.FieldName = 'KeteranganClaimSopir'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 25
          ParentID = 24
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2BiayaLainLain: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2BiayaLainLainEditPropertiesValidate
          Properties.DataBinding.FieldName = 'BiayaLainLain'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 26
          ParentID = 4
          Index = 9
          Version = 1
        end
        object cxDBVerticalGrid2KeteranganBiayaLainLain: TcxDBEditorRow
          Height = 57
          Properties.EditPropertiesClassName = 'TcxMemoProperties'
          Properties.DataBinding.FieldName = 'KeteranganBiayaLainLain'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 27
          ParentID = 26
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2UangMakan: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2UangMakanEditPropertiesValidate
          Properties.DataBinding.FieldName = 'UangMakan'
          Visible = False
          ID = 28
          ParentID = 4
          Index = 10
          Version = 1
        end
        object cxDBVerticalGrid2UangInap: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2UangInapEditPropertiesValidate
          Properties.DataBinding.FieldName = 'UangInap'
          ID = 29
          ParentID = 4
          Index = 11
          Version = 1
        end
        object cxDBVerticalGrid2UangParkir: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2UangParkirEditPropertiesValidate
          Properties.DataBinding.FieldName = 'UangParkir'
          ID = 30
          ParentID = 4
          Index = 12
          Version = 1
        end
        object cxDBVerticalGrid2Other: TcxDBEditorRow
          Height = 17
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.EditProperties.UseDisplayFormatWhenEditing = True
          Properties.EditProperties.UseThousandSeparator = True
          Properties.EditProperties.OnValidate = cxDBVerticalGrid2OtherEditPropertiesValidate
          Properties.DataBinding.FieldName = 'Other'
          ID = 31
          ParentID = 4
          Index = 13
          Version = 1
        end
        object cxDBVerticalGrid2Pengeluaran: TcxDBEditorRow
          Properties.Caption = 'TotalPengeluaran'
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.DataBinding.FieldName = 'Pengeluaran'
          Properties.Options.Editing = False
          ID = 32
          ParentID = 4
          Index = 14
          Version = 1
        end
        object cxDBVerticalGrid2SisaSetor: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
          Properties.DataBinding.FieldName = 'SisaDisetor'
          Styles.Content = cxStyle1
          ID = 33
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid2Verifikasi: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Verifikasi'
          ID = 34
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid2KeteranganVerifikasi: TcxDBEditorRow
          Height = 116
          Properties.DataBinding.FieldName = 'KeteranganVerifikasi'
          ID = 35
          ParentID = -1
          Index = 4
          Version = 1
        end
      end
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 1
        Top = 1
        Width = 304
        Height = 287
        Align = alLeft
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        LookAndFeel.SkinName = 'Darkroom'
        OptionsView.ShowEditButtons = ecsbFocused
        OptionsView.GridLineColor = clBtnFace
        OptionsView.RowHeaderWidth = 111
        OptionsView.RowHeight = 12
        OptionsView.ShowEmptyRowImage = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.GoToNextCellOnTab = True
        OptionsData.CancelOnExit = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        ParentFont = False
        TabOrder = 1
        OnEnter = cxDBVerticalGrid1Enter
        OnExit = cxDBVerticalGrid1Exit
        DataController.DataSource = MasterDs
        Version = 1
        object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxTextEditProperties'
          Properties.EditProperties.ReadOnly = True
          Properties.DataBinding.FieldName = 'TglExtend'
          Properties.Options.Editing = False
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1TglKembali: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'TglKembali'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1Status: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
          Properties.EditProperties.Items = <
            item
              Caption = 'ON TIME'
              Value = 'ON TIME'
            end
            item
              Caption = 'EXTEND'
              Value = 'EXTEND'
            end>
          Properties.DataBinding.FieldName = 'Status'
          Properties.Options.Editing = False
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid1Laporan: TcxDBEditorRow
          Height = 47
          Properties.DataBinding.FieldName = 'Laporan'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 3
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid1Keterangan: TcxDBEditorRow
          Height = 99
          Properties.DataBinding.FieldName = 'Keterangan'
          Styles.Content = MenuUtamaFm.cxStyle5
          ID = 4
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1VerifikasiStatus: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'VerifikasiStatus'
          Properties.Options.Editing = False
          ID = 5
          ParentID = -1
          Index = 5
          Version = 1
        end
        object cxDBVerticalGrid1KmSekarang: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'KmSekarang'
          ID = 6
          ParentID = -1
          Index = 6
          Version = 1
        end
      end
    end
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 304
      Height = 289
      Align = alLeft
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      LookAndFeel.SkinName = 'Darkroom'
      OptionsView.ShowEditButtons = ecsbFocused
      OptionsView.GridLineColor = clBtnFace
      OptionsView.RowHeaderWidth = 146
      OptionsView.RowHeight = 12
      OptionsView.ShowEmptyRowImage = True
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 1
      OnEnter = MasterVGridEnter
      OnExit = MasterVGridExit
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridTgl: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tgl'
        Properties.Options.Editing = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridNoSO: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.DataBinding.FieldName = 'NoSO'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Pelanggan'
        Properties.Options.Editing = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridTelpPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'TelpPelanggan'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 2
        Index = 0
        Version = 1
      end
      object MasterVGridNamaPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPelanggan'
        Properties.Options.Editing = False
        ID = 4
        ParentID = 2
        Index = 1
        Version = 1
      end
      object MasterVGridAlamatPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'AlamatPelanggan'
        Properties.Options.Editing = False
        ID = 5
        ParentID = 2
        Index = 2
        Version = 1
      end
      object MasterVGridRute: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Rute'
        Properties.Options.Editing = False
        ID = 6
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridDari: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Dari'
        Properties.Options.Editing = False
        ID = 7
        ParentID = 6
        Index = 0
        Version = 1
      end
      object MasterVGridKe: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Ke'
        Properties.Options.Editing = False
        ID = 8
        ParentID = 6
        Index = 1
        Version = 1
      end
      object MasterVGridKapasitasSeat: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KapasitasSeat'
        Properties.Options.Editing = False
        ID = 9
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridAC: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DataBinding.FieldName = 'AC'
        Properties.Options.Editing = False
        ID = 10
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridToilet: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DataBinding.FieldName = 'Toilet'
        Properties.Options.Editing = False
        ID = 11
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridAirSuspension: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DataBinding.FieldName = 'AirSuspension'
        Properties.Options.Editing = False
        ID = 12
        ParentID = -1
        Index = 7
        Version = 1
      end
      object MasterVGridTglBerangkat: TcxDBEditorRow
        Properties.Caption = 'TglMulai'
        Properties.DataBinding.FieldName = 'TglBerangkat'
        Properties.Options.Editing = False
        ID = 13
        ParentID = -1
        Index = 8
        Version = 1
      end
      object MasterVGridTiba: TcxDBEditorRow
        Properties.Caption = 'TglSelesai'
        Properties.DataBinding.FieldName = 'TglKembaliSO'
        Properties.Options.Editing = False
        ID = 14
        ParentID = -1
        Index = 9
        Version = 1
      end
      object MasterVGridPICJemput: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PICJemput'
        Properties.Options.Editing = False
        ID = 15
        ParentID = -1
        Index = 10
        Version = 1
      end
      object MasterVGridAlamatJemput: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'AlamatJemput'
        Properties.Options.Editing = False
        ID = 16
        ParentID = -1
        Index = 11
        Version = 1
      end
      object MasterVGridArmada: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Armada'
        Properties.Options.Editing = False
        ID = 17
        ParentID = -1
        Index = 12
        Version = 1
      end
      object MasterVGridPlatNo: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNo'
        ID = 18
        ParentID = 17
        Index = 0
        Version = 1
      end
      object MasterVGridKeteranganSO: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KeteranganSO'
        Properties.Options.Editing = False
        ID = 19
        ParentID = -1
        Index = 13
        Version = 1
      end
      object MasterVGridSopir: TcxDBEditorRow
        Properties.Caption = 'Pengemudi'
        Properties.DataBinding.FieldName = 'Sopir'
        Properties.Options.Editing = False
        ID = 20
        ParentID = -1
        Index = 14
        Version = 1
      end
      object MasterVGridNamaSopir: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPengemudi'
        ID = 21
        ParentID = 20
        Index = 0
        Version = 1
      end
      object MasterVGridSopir2: TcxDBEditorRow
        Properties.Caption = 'Pengemudi2'
        Properties.DataBinding.FieldName = 'Sopir2'
        Properties.Options.Editing = False
        ID = 22
        ParentID = -1
        Index = 15
        Version = 1
      end
      object MasterVGridNamaSopir2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaPengemudi2'
        ID = 23
        ParentID = 22
        Index = 0
        Version = 1
      end
      object MasterVGridCrew: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Crew'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 24
        ParentID = -1
        Index = 16
        Version = 1
      end
      object MasterVGridSopirLuar: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'SopirLuar'
        ID = 25
        ParentID = -1
        Index = 17
        Version = 1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select sj.*,cast('#39#39' as money) as HargaBBM,p.Nama as NamaPengemud' +
        'i,p2.Nama as NamaPengemudi2,'
      
        'so.Rute,r.Muat as Dari,r.Bongkar as Ke,pe.Kode as Pelanggan,pe.N' +
        'amaPT as NamaPelanggan,pe.NoTelp as TelpPelanggan,'
      'pe.Alamat as AlamatPelanggan,so.KapasitasSeat,r.Waktu,'
      'cast(so.PICJemput as varchar(500)) as PICJemput,'
      'cast(so.AlamatJemput as varchar(500)) as AlamatJemput,'
      'cast(so.Keterangan as varchar(500)) as KeteranganSO,so.Armada,'
      'a.PlatNo,so.Berangkat as TglBerangkat,so.BiayaExtend,'
      'so.TglKembaliExtend as TglExtend,'
      'so.Tiba as TglKembaliSO,bs.Nominal as UangJalan2,so.Extend,'
      'so.AC,so.Toilet,so.AirSuspension,a.JenisBBM'
      'from mastersj sj'
      'left join Pegawai p on sj.Sopir=p.Kode'
      'left join Pegawai p2 on sj.Sopir2=p2.Kode '
      'left join MasterSO so on sj.NoSO=so.Kodenota'
      'left join Rute r on so.Rute=r.Kode'
      'left join Pelanggan pe on so.Pelanggan=pe.Kode'
      'left join Armada a on so.Armada=a.Kode'
      'left join BonSopir bs on bs.SuratJalan=sj.KodeNota')
    UpdateObject = masterus
    Left = 284
    Top = 7
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQSopir2: TStringField
      FieldName = 'Sopir2'
      Size = 10
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object MasterQTitipKwitansi: TBooleanField
      FieldName = 'TitipKwitansi'
    end
    object MasterQNominalKwitansi: TCurrencyField
      FieldName = 'NominalKwitansi'
    end
    object MasterQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQKir: TBooleanField
      FieldName = 'Kir'
      Required = True
    end
    object MasterQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object MasterQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object MasterQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object MasterQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object MasterQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object MasterQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object MasterQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object MasterQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object MasterQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object MasterQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object MasterQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object MasterQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object MasterQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQLaporan: TMemoField
      FieldName = 'Laporan'
      BlobType = ftMemo
    end
    object MasterQTglRealisasi: TDateTimeField
      FieldName = 'TglRealisasi'
    end
    object MasterQAwal: TStringField
      FieldName = 'Awal'
      Size = 10
    end
    object MasterQAkhir: TStringField
      FieldName = 'Akhir'
      Size = 10
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object MasterQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 200
    end
    object MasterQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object MasterQPremiSopir2: TCurrencyField
      FieldName = 'PremiSopir2'
    end
    object MasterQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object MasterQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object MasterQTabunganSopir2: TCurrencyField
      FieldName = 'TabunganSopir2'
    end
    object MasterQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object MasterQUangJalan: TCurrencyField
      FieldName = 'UangJalan'
    end
    object MasterQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object MasterQKeteranganBiayaLainLain: TStringField
      FieldName = 'KeteranganBiayaLainLain'
      Size = 200
    end
    object MasterQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object MasterQUangInap: TCurrencyField
      FieldName = 'UangInap'
    end
    object MasterQUangParkir: TCurrencyField
      FieldName = 'UangParkir'
    end
    object MasterQOther: TCurrencyField
      FieldName = 'Other'
    end
    object MasterQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object MasterQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object MasterQSudahPrint: TBooleanField
      FieldName = 'SudahPrint'
    end
    object MasterQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object MasterQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object MasterQDanaKebersihan: TCurrencyField
      FieldName = 'DanaKebersihan'
    end
    object MasterQHargaBBM: TCurrencyField
      FieldName = 'HargaBBM'
    end
    object MasterQNamaPengemudi: TStringField
      FieldName = 'NamaPengemudi'
      Size = 50
    end
    object MasterQNamaPengemudi2: TStringField
      FieldName = 'NamaPengemudi2'
      Size = 50
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object MasterQDari: TStringField
      FieldName = 'Dari'
      Size = 250
    end
    object MasterQKe: TStringField
      FieldName = 'Ke'
      Size = 250
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object MasterQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Size = 100
    end
    object MasterQTelpPelanggan: TStringField
      FieldName = 'TelpPelanggan'
      Size = 50
    end
    object MasterQAlamatPelanggan: TStringField
      FieldName = 'AlamatPelanggan'
      Size = 100
    end
    object MasterQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object MasterQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object MasterQPICJemput: TStringField
      FieldName = 'PICJemput'
      Size = 255
    end
    object MasterQAlamatJemput: TStringField
      FieldName = 'AlamatJemput'
      Size = 255
    end
    object MasterQKeteranganSO: TStringField
      FieldName = 'KeteranganSO'
      Size = 255
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object MasterQTglBerangkat: TDateTimeField
      FieldName = 'TglBerangkat'
    end
    object MasterQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object MasterQTglExtend: TDateTimeField
      FieldName = 'TglExtend'
    end
    object MasterQTglKembaliSO: TDateTimeField
      FieldName = 'TglKembaliSO'
    end
    object MasterQUangJalan2: TCurrencyField
      FieldName = 'UangJalan2'
    end
    object MasterQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object MasterQAC: TBooleanField
      FieldName = 'AC'
    end
    object MasterQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object MasterQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object MasterQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object MasterQVerifikasiStatus: TStringField
      FieldName = 'VerifikasiStatus'
      Size = 50
    end
    object MasterQKmSekarang: TFloatField
      FieldName = 'KmSekarang'
    end
    object MasterQSopirLuar: TStringField
      FieldName = 'SopirLuar'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 316
    Top = 6
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from mastersj order by kodenota desc')
    Left = 233
    Top = 47
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
      Size = 10
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'aPT like '#39'%'#39' + :text + '#39'%'#39)
    Left = 390
    Top = 4
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from rute where kode like '#39'%'#39' + :text + '#39'%'#39' or muat lik' +
        'e '#39'%'#39' + :text + '#39'%'#39' or bongkar like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 465
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 609
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object ViewKontrakQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select cast(sj.Tgl as date) as Tanggal,sj.*,a.platno,so.Berangka' +
        't as TglMulai,so.Tiba as TglSelesai,'
      'a.JumlahSeat,p.namaPT,pg.Nama as Pembuat,so.Pelanggan,'
      
        'p.NamaPT as NamaPelanggan,so.Armada as KodeArmada, so.Keterangan' +
        'Rute, pgs.nama as LastEditor'
      
        'from mastersj sj left join masterso so on sj.noso=so.kodenota le' +
        'ft join armada a on so.armada=a.kode'
      'left join pelanggan p on so.Pelanggan=p.kode'
      'left join [user] u on sj.createby=u.kode'
      'left join pegawai pg on u.kodepegawai=pg.kode'
      'left join [user] us on sj.operator=us.kode'
      'left join pegawai pgs on us.kodepegawai=pgs.kode'
      'where (sj.Tgl>=:text1 and sj.Tgl<=:text2)'
      'order by tglentry desc')
    Left = 744
    Top = 9
    ParamData = <
      item
        DataType = ftDate
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewKontrakQSudahPrint: TBooleanField
      FieldName = 'SudahPrint'
    end
    object ViewKontrakQTanggal: TStringField
      FieldName = 'Tanggal'
    end
    object ViewKontrakQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object ViewKontrakQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object ViewKontrakQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 10
    end
    object ViewKontrakQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ViewKontrakQSopir2: TStringField
      FieldName = 'Sopir2'
      Size = 10
    end
    object ViewKontrakQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object ViewKontrakQTitipKwitansi: TBooleanField
      FieldName = 'TitipKwitansi'
    end
    object ViewKontrakQNominalKwitansi: TCurrencyField
      FieldName = 'NominalKwitansi'
    end
    object ViewKontrakQNoKwitansi: TStringField
      FieldName = 'NoKwitansi'
      Size = 50
    end
    object ViewKontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object ViewKontrakQKir: TBooleanField
      FieldName = 'Kir'
      Required = True
    end
    object ViewKontrakQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object ViewKontrakQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object ViewKontrakQTglKembali: TDateTimeField
      FieldName = 'TglKembali'
    end
    object ViewKontrakQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object ViewKontrakQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object ViewKontrakQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object ViewKontrakQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object ViewKontrakQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object ViewKontrakQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object ViewKontrakQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object ViewKontrakQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object ViewKontrakQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object ViewKontrakQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object ViewKontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewKontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewKontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewKontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewKontrakQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object ViewKontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewKontrakQLaporan: TMemoField
      FieldName = 'Laporan'
      BlobType = ftMemo
    end
    object ViewKontrakQTglRealisasi: TDateTimeField
      FieldName = 'TglRealisasi'
    end
    object ViewKontrakQAwal: TStringField
      FieldName = 'Awal'
      Size = 10
    end
    object ViewKontrakQAkhir: TStringField
      FieldName = 'Akhir'
      Size = 10
    end
    object ViewKontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object ViewKontrakQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object ViewKontrakQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 200
    end
    object ViewKontrakQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object ViewKontrakQPremiSopir2: TCurrencyField
      FieldName = 'PremiSopir2'
    end
    object ViewKontrakQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object ViewKontrakQTabunganSopir2: TCurrencyField
      FieldName = 'TabunganSopir2'
    end
    object ViewKontrakQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object ViewKontrakQUangJalan: TCurrencyField
      FieldName = 'UangJalan'
    end
    object ViewKontrakQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object ViewKontrakQKeteranganBiayaLainLain: TStringField
      FieldName = 'KeteranganBiayaLainLain'
      Size = 200
    end
    object ViewKontrakQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object ViewKontrakQUangInap: TCurrencyField
      FieldName = 'UangInap'
    end
    object ViewKontrakQUangParkir: TCurrencyField
      FieldName = 'UangParkir'
    end
    object ViewKontrakQOther: TCurrencyField
      FieldName = 'Other'
    end
    object ViewKontrakQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object ViewKontrakQTitipTagihan: TBooleanField
      FieldName = 'TitipTagihan'
    end
    object ViewKontrakQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object ViewKontrakQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object ViewKontrakQDanaKebersihan: TCurrencyField
      FieldName = 'DanaKebersihan'
    end
    object ViewKontrakQplatno: TStringField
      FieldName = 'platno'
      Size = 10
    end
    object ViewKontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
    end
    object ViewKontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
    end
    object ViewKontrakQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ViewKontrakQnamaPT: TStringField
      FieldName = 'namaPT'
      Size = 100
    end
    object ViewKontrakQPembuat: TStringField
      FieldName = 'Pembuat'
      Size = 50
    end
    object ViewKontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object ViewKontrakQNamaPelanggan: TStringField
      FieldName = 'NamaPelanggan'
      Size = 100
    end
    object ViewKontrakQKodeArmada: TStringField
      FieldName = 'KodeArmada'
      Size = 10
    end
    object ViewKontrakQKodePegawai: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePegawai'
      LookupDataSet = UserQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'KodePegawai'
      KeyFields = 'Operator'
      Size = 10
      Lookup = True
    end
    object ViewKontrakQLastEditor: TStringField
      FieldKind = fkLookup
      FieldName = 'LastEditor'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodePegawai'
      Size = 50
      Lookup = True
    end
    object ViewKontrakQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewKontrakQ
    Left = 700
    Top = 14
  end
  object masterus: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other' +
        ', SPBUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, Keteran' +
        'ganVerifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang, Sop' +
        'irLuar'
      'from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update mastersj'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other,'
      '  SPBUAYaniDetail = :SPBUAYaniDetail,'
      '  TitipTagihan = :TitipTagihan,'
      '  SudahPrint = :SudahPrint,'
      '  Verifikasi = :Verifikasi,'
      '  KeteranganVerifikasi = :KeteranganVerifikasi,'
      '  DanaKebersihan = :DanaKebersihan,'
      '  VerifikasiStatus = :VerifikasiStatus,'
      '  KmSekarang = :KmSekarang,'
      '  SopirLuar = :SopirLuar'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into mastersj'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other, SP' +
        'BUAYaniDetail, TitipTagihan, SudahPrint, Verifikasi, KeteranganV' +
        'erifikasi, DanaKebersihan, VerifikasiStatus, KmSekarang, SopirLu' +
        'ar)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other, :SPBUAYaniDetail,' +
        ' :TitipTagihan, :SudahPrint, :Verifikasi, :KeteranganVerifikasi,' +
        ' :DanaKebersihan, :VerifikasiStatus, :KmSekarang, :SopirLuar)')
    DeleteSQL.Strings = (
      'delete from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 352
    Top = 8
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\Jaya\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 192
    Top = 48
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 793
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object updateQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object updateQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object updateQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object updateQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object updateQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object updateQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object updateQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object updateQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object updateQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object updateQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object updateQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object updateQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object updateQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object updateQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object updateQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object updateQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object updateQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object updateQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object updateQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object updateQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object updateQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object updateQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object updateQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object updateQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object updateQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object updateQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object updateQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object updateQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterso')
    Left = 569
    Top = 7
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Visible = False
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Visible = False
      Size = 10
    end
    object SOQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 656
    Top = 8
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object BonSopirQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from BonSopir')
    Left = 840
    Top = 8
    object BonSopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BonSopirQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Required = True
      Size = 10
    end
    object BonSopirQNominal: TCurrencyField
      FieldName = 'Nominal'
      Required = True
    end
    object BonSopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BonSopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BonSopirQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BonSopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BonSopirQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
    object cxStyle2: TcxStyle
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 945
    Top = 15
  end
  object UserQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from [user]')
    Left = 896
    Top = 8
    object UserQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object UserQUsername: TStringField
      FieldName = 'Username'
      Required = True
    end
    object UserQPassword: TStringField
      FieldName = 'Password'
      Required = True
    end
    object UserQJenis: TStringField
      FieldName = 'Jenis'
      Size = 50
    end
    object UserQKodePegawai: TStringField
      FieldName = 'KodePegawai'
      Size = 10
    end
  end
  object HargaBBMQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from HargaBBM')
    Left = 520
    Top = 304
    object HargaBBMQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HargaBBMQHargaSolar: TCurrencyField
      FieldName = 'HargaSolar'
      Required = True
    end
    object HargaBBMQHargaBensin: TCurrencyField
      FieldName = 'HargaBensin'
      Required = True
    end
  end
  object KwitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from DetailKwitansiSO where KodeSO=:text')
    UpdateObject = KwitansiUs
    Left = 400
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object KwitansiQKodeKwitansi: TStringField
      FieldName = 'KodeKwitansi'
      Required = True
      Size = 10
    end
    object KwitansiQKodeSO: TStringField
      FieldName = 'KodeSO'
      Required = True
      Size = 10
    end
    object KwitansiQNominal: TCurrencyField
      FieldName = 'Nominal'
    end
  end
  object KwitansiUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeKwitansi, KodeSO, Nominal'
      'from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    ModifySQL.Strings = (
      'update DetailKwitansiSO'
      'set'
      '  KodeKwitansi = :KodeKwitansi,'
      '  KodeSO = :KodeSO,'
      '  Nominal = :Nominal'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    InsertSQL.Strings = (
      'insert into DetailKwitansiSO'
      '  (KodeKwitansi, KodeSO, Nominal)'
      'values'
      '  (:KodeKwitansi, :KodeSO, :Nominal)')
    DeleteSQL.Strings = (
      'delete from DetailKwitansiSO'
      'where'
      '  KodeKwitansi = :OLD_KodeKwitansi and'
      '  KodeSO = :OLD_KodeSO')
    Left = 440
    Top = 376
  end
  object UpdateKwitansiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 480
    Top = 384
  end
end
