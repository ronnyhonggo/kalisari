unit RealisasiSuratJalan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo, cxCheckBox, UCrpeClasses, UCrpe32,
  cxDropDownEdit, cxGroupBox;

type
  TRealisasiSuratJalanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    KodeQ: TSDQuery;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    PelangganQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQ: TSDQuery;
    ExitBtn: TcxButton;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    ViewKontrakQ: TSDQuery;
    ViewDs: TDataSource;
    KodeQkodenota: TStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar: TStatusBar;
    masterus: TSDUpdateSQL;
    Crpe1: TCrpe;
    cxButtonCetak: TcxButton;
    updateQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    updateQKode: TStringField;
    updateQPlatNo: TStringField;
    updateQJumlahSeat: TIntegerField;
    updateQTahunPembuatan: TStringField;
    updateQNoBody: TStringField;
    updateQJenisAC: TStringField;
    updateQJenisBBM: TStringField;
    updateQKapasitasTangkiBBM: TIntegerField;
    updateQLevelArmada: TStringField;
    updateQJumlahBan: TIntegerField;
    updateQAktif: TBooleanField;
    updateQAC: TBooleanField;
    updateQToilet: TBooleanField;
    updateQAirSuspension: TBooleanField;
    updateQKmSekarang: TIntegerField;
    updateQKeterangan: TStringField;
    updateQSopir: TStringField;
    updateQJenisKendaraan: TStringField;
    updateQCreateDate: TDateTimeField;
    updateQCreateBy: TStringField;
    updateQOperator: TStringField;
    updateQTglEntry: TDateTimeField;
    updateQSTNKPajakExpired: TDateTimeField;
    updateQSTNKPerpanjangExpired: TDateTimeField;
    updateQKirMulai: TDateTimeField;
    updateQKirSelesai: TDateTimeField;
    updateQNoRangka: TStringField;
    updateQNoMesin: TStringField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    SOQTglPelunasan: TDateTimeField;
    BonSopirQ: TSDQuery;
    BonSopirQKode: TStringField;
    BonSopirQSuratJalan: TStringField;
    BonSopirQNominal: TCurrencyField;
    BonSopirQCreateDate: TDateTimeField;
    BonSopirQCreateBy: TStringField;
    BonSopirQOperator: TStringField;
    BonSopirQTglEntry: TDateTimeField;
    BonSopirQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    SOQNamaPelanggan: TStringField;
    Panel1: TPanel;
    Panel3: TPanel;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid2UangJalan: TcxDBEditorRow;
    cxDBVerticalGrid2NominalKwitansi: TcxDBEditorRow;
    cxDBVerticalGrid2TabunganSopir: TcxDBEditorRow;
    cxDBVerticalGrid2TabunganSopir2: TcxDBEditorRow;
    cxDBVerticalGrid2Pendapatan: TcxDBEditorRow;
    cxDBVerticalGrid2CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid2CategoryRow4: TcxCategoryRow;
    cxDBVerticalGrid2CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid2SPBUAYaniLiter: TcxDBEditorRow;
    cxDBVerticalGrid2SPBUAYaniUang: TcxDBEditorRow;
    cxDBVerticalGrid2SPBUAYaniJam: TcxDBEditorRow;
    cxDBVerticalGrid2CategoryRow5: TcxCategoryRow;
    cxDBVerticalGrid2SPBULuarLiter: TcxDBEditorRow;
    cxDBVerticalGrid2SPBULuarUang: TcxDBEditorRow;
    cxDBVerticalGrid2SPBULuarUangDiberi: TcxDBEditorRow;
    cxDBVerticalGrid2SPBULuarDetail: TcxDBEditorRow;
    cxDBVerticalGrid2tol: TcxDBEditorRow;
    cxDBVerticalGrid2PremiSopir: TcxDBEditorRow;
    cxDBVerticalGrid2PremiSopir2: TcxDBEditorRow;
    cxDBVerticalGrid2PremiKernet: TcxDBEditorRow;
    cxDBVerticalGrid2ClaimSopir: TcxDBEditorRow;
    cxDBVerticalGrid2KeteranganClaimSopir: TcxDBEditorRow;
    cxDBVerticalGrid2BiayaLainLain: TcxDBEditorRow;
    cxDBVerticalGrid2KeteranganBiayaLainLain: TcxDBEditorRow;
    cxDBVerticalGrid2UangMakan: TcxDBEditorRow;
    cxDBVerticalGrid2UangInap: TcxDBEditorRow;
    cxDBVerticalGrid2UangParkir: TcxDBEditorRow;
    cxDBVerticalGrid2Other: TcxDBEditorRow;
    cxDBVerticalGrid2Pengeluaran: TcxDBEditorRow;
    cxDBVerticalGrid2SisaSetor: TcxDBEditorRow;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridTgl: TcxDBEditorRow;
    MasterVGridNoSO: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridTelpPelanggan: TcxDBEditorRow;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterVGridAlamatPelanggan: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridDari: TcxDBEditorRow;
    MasterVGridKe: TcxDBEditorRow;
    MasterVGridKapasitasSeat: TcxDBEditorRow;
    MasterVGridAC: TcxDBEditorRow;
    MasterVGridToilet: TcxDBEditorRow;
    MasterVGridAirSuspension: TcxDBEditorRow;
    MasterVGridTglBerangkat: TcxDBEditorRow;
    MasterVGridTiba: TcxDBEditorRow;
    MasterVGridPICJemput: TcxDBEditorRow;
    MasterVGridAlamatJemput: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterVGridKeteranganSO: TcxDBEditorRow;
    MasterVGridSopir: TcxDBEditorRow;
    MasterVGridNamaSopir: TcxDBEditorRow;
    MasterVGridSopir2: TcxDBEditorRow;
    MasterVGridNamaSopir2: TcxDBEditorRow;
    MasterVGridCrew: TcxDBEditorRow;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1TglKembali: TcxDBEditorRow;
    cxDBVerticalGrid1Status: TcxDBEditorRow;
    cxDBVerticalGrid1Laporan: TcxDBEditorRow;
    SDQuery1: TSDQuery;
    cxDBVerticalGrid1Keterangan: TcxDBEditorRow;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxStyle2: TcxStyle;
    cxDBVerticalGrid2SPBUAYaniDetail: TcxDBEditorRow;
    cxDBVerticalGrid2HargaBBM: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxGrid1DBTableView1Pembuat: TcxGridDBColumn;
    UserQ: TSDQuery;
    UserQKode: TStringField;
    UserQUsername: TStringField;
    UserQPassword: TStringField;
    UserQJenis: TStringField;
    UserQKodePegawai: TStringField;
    cxGrid1DBTableView1NamaPembuat: TcxGridDBColumn;
    cxDBVerticalGrid2Verifikasi: TcxDBEditorRow;
    cxDBVerticalGrid2KeteranganVerifikasi: TcxDBEditorRow;
    cxGrid1DBTableView1Verifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganVerifikasi: TcxGridDBColumn;
    HargaBBMQ: TSDQuery;
    HargaBBMQKode: TStringField;
    HargaBBMQHargaSolar: TCurrencyField;
    HargaBBMQHargaBensin: TCurrencyField;
    cxGrid1DBTableView1TglEntry: TcxGridDBColumn;
    cxDBVerticalGrid2DanaKebersihan: TcxDBEditorRow;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQNoSO: TStringField;
    MasterQSopir: TStringField;
    MasterQSopir2: TStringField;
    MasterQCrew: TStringField;
    MasterQTitipKwitansi: TBooleanField;
    MasterQNominalKwitansi: TCurrencyField;
    MasterQNoKwitansi: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQKir: TBooleanField;
    MasterQSTNK: TBooleanField;
    MasterQPajak: TBooleanField;
    MasterQTglKembali: TDateTimeField;
    MasterQPendapatan: TCurrencyField;
    MasterQPengeluaran: TCurrencyField;
    MasterQSisaDisetor: TCurrencyField;
    MasterQSPBUAYaniLiter: TFloatField;
    MasterQSPBUAYaniUang: TCurrencyField;
    MasterQSPBUAYaniJam: TDateTimeField;
    MasterQSPBULuarLiter: TFloatField;
    MasterQSPBULuarUang: TCurrencyField;
    MasterQSPBULuarUangDiberi: TCurrencyField;
    MasterQSPBULuarDetail: TMemoField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQLaporan: TMemoField;
    MasterQTglRealisasi: TDateTimeField;
    MasterQAwal: TStringField;
    MasterQAkhir: TStringField;
    MasterQTglCetak: TDateTimeField;
    MasterQClaimSopir: TCurrencyField;
    MasterQKeteranganClaimSopir: TStringField;
    MasterQPremiSopir: TCurrencyField;
    MasterQPremiSopir2: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQTabunganSopir: TCurrencyField;
    MasterQTabunganSopir2: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQUangJalan: TCurrencyField;
    MasterQBiayaLainLain: TCurrencyField;
    MasterQKeteranganBiayaLainLain: TStringField;
    MasterQUangMakan: TCurrencyField;
    MasterQUangInap: TCurrencyField;
    MasterQUangParkir: TCurrencyField;
    MasterQOther: TCurrencyField;
    MasterQSPBUAYaniDetail: TStringField;
    MasterQTitipTagihan: TBooleanField;
    MasterQSudahPrint: TBooleanField;
    MasterQVerifikasi: TCurrencyField;
    MasterQKeteranganVerifikasi: TMemoField;
    MasterQDanaKebersihan: TCurrencyField;
    MasterQHargaBBM: TCurrencyField;
    MasterQNamaPengemudi: TStringField;
    MasterQNamaPengemudi2: TStringField;
    MasterQRute: TStringField;
    MasterQDari: TStringField;
    MasterQKe: TStringField;
    MasterQPelanggan: TStringField;
    MasterQNamaPelanggan: TStringField;
    MasterQTelpPelanggan: TStringField;
    MasterQAlamatPelanggan: TStringField;
    MasterQKapasitasSeat: TIntegerField;
    MasterQWaktu: TIntegerField;
    MasterQPICJemput: TStringField;
    MasterQAlamatJemput: TStringField;
    MasterQKeteranganSO: TStringField;
    MasterQArmada: TStringField;
    MasterQPlatNo: TStringField;
    MasterQTglBerangkat: TDateTimeField;
    MasterQBiayaExtend: TCurrencyField;
    MasterQTglExtend: TDateTimeField;
    MasterQTglKembaliSO: TDateTimeField;
    MasterQUangJalan2: TCurrencyField;
    MasterQExtend: TBooleanField;
    MasterQAC: TBooleanField;
    MasterQToilet: TBooleanField;
    MasterQAirSuspension: TBooleanField;
    MasterQJenisBBM: TStringField;
    ViewKontrakQTanggal: TStringField;
    ViewKontrakQKodenota: TStringField;
    ViewKontrakQTgl: TDateTimeField;
    ViewKontrakQNoSO: TStringField;
    ViewKontrakQSopir: TStringField;
    ViewKontrakQSopir2: TStringField;
    ViewKontrakQCrew: TStringField;
    ViewKontrakQTitipKwitansi: TBooleanField;
    ViewKontrakQNominalKwitansi: TCurrencyField;
    ViewKontrakQNoKwitansi: TStringField;
    ViewKontrakQKeterangan: TMemoField;
    ViewKontrakQKir: TBooleanField;
    ViewKontrakQSTNK: TBooleanField;
    ViewKontrakQPajak: TBooleanField;
    ViewKontrakQTglKembali: TDateTimeField;
    ViewKontrakQPendapatan: TCurrencyField;
    ViewKontrakQPengeluaran: TCurrencyField;
    ViewKontrakQSisaDisetor: TCurrencyField;
    ViewKontrakQSPBUAYaniLiter: TFloatField;
    ViewKontrakQSPBUAYaniUang: TCurrencyField;
    ViewKontrakQSPBUAYaniJam: TDateTimeField;
    ViewKontrakQSPBULuarLiter: TFloatField;
    ViewKontrakQSPBULuarUang: TCurrencyField;
    ViewKontrakQSPBULuarUangDiberi: TCurrencyField;
    ViewKontrakQSPBULuarDetail: TMemoField;
    ViewKontrakQStatus: TStringField;
    ViewKontrakQCreateDate: TDateTimeField;
    ViewKontrakQCreateBy: TStringField;
    ViewKontrakQOperator: TStringField;
    ViewKontrakQTglEntry: TDateTimeField;
    ViewKontrakQLaporan: TMemoField;
    ViewKontrakQTglRealisasi: TDateTimeField;
    ViewKontrakQAwal: TStringField;
    ViewKontrakQAkhir: TStringField;
    ViewKontrakQTglCetak: TDateTimeField;
    ViewKontrakQClaimSopir: TCurrencyField;
    ViewKontrakQKeteranganClaimSopir: TStringField;
    ViewKontrakQPremiSopir: TCurrencyField;
    ViewKontrakQPremiSopir2: TCurrencyField;
    ViewKontrakQPremiKernet: TCurrencyField;
    ViewKontrakQTabunganSopir: TCurrencyField;
    ViewKontrakQTabunganSopir2: TCurrencyField;
    ViewKontrakQTol: TCurrencyField;
    ViewKontrakQUangJalan: TCurrencyField;
    ViewKontrakQBiayaLainLain: TCurrencyField;
    ViewKontrakQKeteranganBiayaLainLain: TStringField;
    ViewKontrakQUangMakan: TCurrencyField;
    ViewKontrakQUangInap: TCurrencyField;
    ViewKontrakQUangParkir: TCurrencyField;
    ViewKontrakQOther: TCurrencyField;
    ViewKontrakQSPBUAYaniDetail: TStringField;
    ViewKontrakQTitipTagihan: TBooleanField;
    ViewKontrakQSudahPrint: TBooleanField;
    ViewKontrakQVerifikasi: TCurrencyField;
    ViewKontrakQKeteranganVerifikasi: TMemoField;
    ViewKontrakQDanaKebersihan: TCurrencyField;
    ViewKontrakQplatno: TStringField;
    ViewKontrakQTglMulai: TDateTimeField;
    ViewKontrakQTglSelesai: TDateTimeField;
    ViewKontrakQJumlahSeat: TIntegerField;
    ViewKontrakQnamaPT: TStringField;
    ViewKontrakQPembuat: TStringField;
    ViewKontrakQPelanggan: TStringField;
    ViewKontrakQNamaPelanggan: TStringField;
    ViewKontrakQKodeArmada: TStringField;
    MasterQVerifikasiStatus: TStringField;
    cxDBVerticalGrid1VerifikasiStatus: TcxDBEditorRow;
    VeriBtn: TcxButton;
    ViewKontrakQKodePegawai: TStringField;
    ViewKontrakQLastEditor: TStringField;
    KwitansiQ: TSDQuery;
    KwitansiQKodeKwitansi: TStringField;
    KwitansiQKodeSO: TStringField;
    KwitansiQNominal: TCurrencyField;
    KwitansiUs: TSDUpdateSQL;
    UpdateKwitansiQ: TSDQuery;
    MasterQKmSekarang: TFloatField;
    cxDBVerticalGrid1KmSekarang: TcxDBEditorRow;
    ViewKontrakQKeteranganRute: TMemoField;
    cxGrid1DBTableView1KeteranganRute: TcxGridDBColumn;
    cxGrid1DBTableView1SudahPrint: TcxGridDBColumn;
    Button1: TButton;
    cxButton1: TcxButton;
    MasterQSopirLuar: TStringField;
    MasterVGridSopirLuar: TcxDBEditorRow;
    //procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxDBEditorRow10EditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxDBEditorRow10EditPropertiesChange(Sender: TObject);
    procedure cxDBEditorRow10PropertiesGetEditProperties(
      Sender: TcxCustomEditorRowProperties; ARecordIndex: Integer;
      var AProperties: TcxCustomEditProperties);
    procedure cxDBEditorRow10PropertiesGetEditingProperties(
      Sender: TcxCustomEditorRowProperties; ARecordIndex: Integer;
      var AProperties: TcxCustomEditProperties);
    procedure cxDBEditorRow10PropertiesGetDisplayText(
      Sender: TcxCustomEditorRowProperties; ARecord: Integer;
      var AText: String);
    procedure cxDBVerticalGrid1Exit(Sender: TObject);
    procedure cxDBVerticalGrid1Enter(Sender: TObject);
    procedure cxButtonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1DBAkhirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure KodeEditKeyPress(Sender: TObject; var Key: Char);
    procedure cxDBVerticalGrid2UangJalanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SPBULuarUangDiberiEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2DBBiayaExtendEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2NominalKwitansiEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SPBUAYaniUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SPBULuarUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2tolEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2PremiSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2PremiSopir2EditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2TabunganSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2TabunganSopir2EditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2PremiKernetEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2ClaimSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2BiayaLainLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2UangMakanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2OtherEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SPBUAYaniLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2SPBULuarLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2UangInapEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid2UangParkirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxDBVerticalGrid2HargaBBMEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cxDBVerticalGrid2DanaKebersihanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure VeriBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  

  private
  procedure CreateParams(var Params: TCreateParams); override;
  procedure WMSize(var Msg: TMessage); message WM_SIZE;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  RealisasiSuratJalanFm: TRealisasiSuratJalanFm;

  MasterOriSQL, kodeprint: string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, IsianPremi;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TRealisasiSuratJalanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TRealisasiSuratJalanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  if (FormStyle = fsStayOnTop) then begin
    Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;
  end;
end;

procedure TRealisasiSuratJalanFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(RealisasiSuratJalanFm.Handle, SW_RESTORE) ;
end;

procedure TRealisasiSuratJalanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TRealisasiSuratJalanFm.ExitBtnClick(Sender: TObject);
var
myDate : TDateTime;
begin
  close;
  Release;
end;

procedure TRealisasiSuratJalanFm.FormCreate(Sender: TObject);
begin
  //MenuUtamaFm.cxListBox1.Visible:=false;
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKodenota.Size;
end;

procedure TRealisasiSuratJalanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  //MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
end;

procedure TRealisasiSuratJalanFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
//  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TRealisasiSuratJalanFm.FormShow(Sender: TObject);
begin
  //KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewKontrakQ.Close;
  viewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewKontrakQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertRealisasiSuratJalan.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiSuratJalan.AsBoolean;
//  DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiSuratJalan.AsBoolean;
  HargaBBMQ.Open;
end;

procedure TRealisasiSuratJalanFm.KodeEditExit(Sender: TObject);
var akhirnya:string;
begin
  if KodeEdit.Text<>'' then
  begin
    MasterVGrid.Enabled:=true;
    cxDBVerticalGrid1.Enabled:=true;
    cxButtonCetak.Enabled:=true;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+kodeedit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      //KodeEdit.Text:= ViewKontrakQKodenota.AsString;
      MasterQ.Edit;
      //DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiSuratJalan.AsBoolean;
    end;
    kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertRealisasiSuratJalan.AsBoolean;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiSuratJalan.AsBoolean;
    MasterVGrid.Enabled:=True;
    akhirnya:='';
    if (MasterQTglKembali.AsString='') or (MasterQTglKembali.AsVariant=null) then
    begin
      MasterQTglKembali.AsDateTime:= now;
    end;

    if(MasterQTgl.AsDateTime<MasterQTglRealisasi.AsDateTime) then
    begin
      akhirnya:='START LATE'
    end;


    if (MasterQTglKembali.AsDateTime>(MasterQTglRealisasi.AsDateTime+MasterQwaktu.AsInteger)) then
    begin
      akhirnya:= akhirnya+'FINISHED LATE';
    end
    else
    begin
      akhirnya:= akhirnya+'FINISHED';

    end;

    if (akhirnya='') then akhirnya:='ON GOING';

    MasterQStatus.AsString:= akhirnya;
  end
  else
  begin
    StatusBar.Panels[0].Text:= 'Mode : Edit';
    //DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiSuratJalan.AsBoolean;
  end;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiSuratJalan.AsBoolean;
  MasterVGrid.Enabled:=True;
  if MasterQVerifikasiStatus.AsString='VERIFIED' then
  begin
    MasterVGrid.OptionsData.Editing:=False;
    cxDBVerticalGrid1.OptionsData.Editing:=False;
    cxDBVerticalGrid2.OptionsData.Editing:=False;
    SaveBtn.Enabled:=False;
    //deleteBtn.Enabled:=False;
    VeriBtn.Enabled:=False;
  end
  else
  begin
    MasterVGrid.OptionsData.Editing:=True;
    cxDBVerticalGrid1.OptionsData.Editing:=True;
    cxDBVerticalGrid2.OptionsData.Editing:=True;
    SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiSuratJalan.AsBoolean;
    //deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiSuratJalan.AsBoolean;
    VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRSJ.AsBoolean;
  end;
end;

procedure TRealisasiSuratJalanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TRealisasiSuratJalanFm.SaveBtnClick(Sender: TObject);
begin
  {if ((MasterQxNama.IsNull) or (MasterQxTgl.IsNull) or (MasterQxNoSJ.IsNull)) then
  begin
    ShowMessage('Lengkapi field surat jalan external');
  end
  else if ((MasterQStatus.AsString<>'FINISHED') and (MasterQStatus.AsString<>'ON GOING'))  and (MasterQLaporan.AsString='') then
  begin
    ShowMessage('Anda Harus Mengisi Field Laporan');
  end
  else  }
  begin
    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      KodeQ.Open;
      if KodeEdit.Text = 'INSERT BARU' then
      begin
        if KodeQ.IsEmpty then
        begin
          MasterQKodenota.AsString:=DMFm.Fill('1',10,'0',True);
        end
        else
        begin
          MasterQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkodenota.AsString)+1),10,'0',True);
        end;
      end
      else
      begin
        MasterQKodenota.AsString:=KodeEdit.Text;
      end;
      MasterQ.Post;
      KodeQ.Close;
      KodeEdit.Text:=MasterQKodenota.AsString;
    end;
    //ShowMessage(MasterQKodenota.AsString);
    MenuUtamaFm.Database1.StartTransaction;
    try
      MasterQ.Edit;
      MasterQOperator.AsString:=User;
      MasterQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      MasterQ.CommitUpdates;
      //SDQuery1.SQL.Text:='update armada set kmsekarang=kmsekarang+' + MasterQakhir_jarak.AsString + '+' + MasterQjarak_jarak.AsString + '+' + MasterQrute_jarak.AsString + ' where kode=' + MasterQkode_armada.AsString;
      //SDQuery1.ExecSQL;
      ShowMessage('Simpan Berhasil');

        //cetak SJ
    if MessageDlg('Cetak Realisasi Surat Jalan '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiSuratJalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;

    end;
    //end Cetak SJ

      //cxButtonEdit1PropertiesButtonClick(sender,0);
      MasterQ.Close;
      KodeEdit.Text:='';
      MasterVGrid.Enabled:=false;
      cxDBVerticalGrid1.Enabled:=false;


    except
      on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
    end;
    ViewKontrakQ.Close;
    ViewKontrakQ.Open;
  end;

end;

procedure TRealisasiSuratJalanFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
 // MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TRealisasiSuratJalanFm.MasterQBeforePost(DataSet: TDataSet);
var
akhirnya  :String;
begin

 // DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
  {if MasterQEkor.Value='' then MasterQEkor.AsVariant:=null;
  if MasterQAkhir.Value='' then MasterQAkhir.AsVariant:=null;
  if MasterQAwal.Value='' then MasterQAwal.AsVariant:=null;   }
end;

procedure TRealisasiSuratJalanFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiSuratJalanFm.MasterVGridExit(Sender: TObject);
begin
  cxDBVerticalGrid1.SetFocus
end;

procedure TRealisasiSuratJalanFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:='';
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      {MasterQPelanggan.AsString:=PelangganQKode.AsString;
      MasterQnamapt.AsString := PelangganQNamaPT.AsString;
      MasterQalamat.AsString := PelangganQAlamat.AsString;
      MasterQnotelp.AsString := PelangganQNoTelp.AsString;
      MasterQnamapic1.AsString := PelangganQNamaPIC1.AsString;
      MasterQjabatanpic1.AsString := PelangganQJabatanPIC1.AsString;
      MasterQtelppic1.AsString :=PelangganQTelpPIC1.AsString;  }
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiSuratJalanFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    PelangganQ.Close;
    PelangganQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    PelangganQ.ExecSQL;
    PelangganQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      {MasterQPelanggan.AsString:=PelangganQKode.AsString;
      MasterQnamapt.AsString := PelangganQNamaPT.AsString;
      MasterQalamat.AsString := PelangganQAlamat.AsString;
      MasterQnotelp.AsString := PelangganQNoTelp.AsString;   }
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiSuratJalanFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:='';
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      {MasterQRute.AsString:=RuteQKode.AsString;
      MasterQmuat.AsString := RuteQMuat.AsString;
      MasterQbongkar.AsString := RuteQBongkar.AsString;
      MasterQstandardharga.AsString := RuteQstandardHarga.AsString; }
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiSuratJalanFm.MasterVGridRuteEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    RuteQ.Close;
    RuteQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    RuteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQRute.AsString:=RuteQKode.AsString;
      //cxlabel4.Caption:='Muat : ' + RuteQMuat.AsString;
      //cxlabel5.Caption:='Bongkar : ' + RuteQBongkar.AsString;
      //cxlabel6.Caption:='Standard Harga : ' + RuteQHarga.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
      cxDBVerticalGrid1.SetFocus;
end;

procedure TRealisasiSuratJalanFm.MasterVGridArmadaEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
   { ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;   }
end;

procedure TRealisasiSuratJalanFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:='';
    ArmadaQ.ExecSQL;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQArmada.AsString:=ArmadaQKode.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release; }
end;

procedure TRealisasiSuratJalanFm.MasterVGridEnter(Sender: TObject);
begin
  mastervgrid.FocusRow(MasterVGridTgl);
end;

procedure TRealisasiSuratJalanFm.cxGrid1DBTableView1DblClick(Sender: TObject);
var akhirnya :string;
begin
  MasterVGrid.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  cxButtonCetak.Enabled:=true;
  MasterQ.SQL.Clear;
  MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+ViewKontrakQKodenota.AsString+'%'));
  MasterQ.Open;
  if MasterQ.IsEmpty then
  begin
    StatusBar.Panels[0].Text:= 'Mode : Entry';
    MasterQ.Append;
    MasterQ.Edit;
  end
  else
  begin
    StatusBar.Panels[0].Text:= 'Mode : Edit';
    KodeEdit.Text:= ViewKontrakQKodenota.AsString;
    MasterQ.Edit;
    if MasterQJenisBBM.AsString='BENSIN' then
    begin
        MasterQ.Edit;
        MasterQHargaBBM.AsCurrency:=HargaBBMQHargaBensin.AsCurrency
    end

    else if MasterQJenisBBM.AsString='SOLAR' then
    begin
        MasterQ.Edit;
        MasterQHargaBBM.AsCurrency:=HargaBBMQHargaSolar.AsCurrency;
    end
    else
    begin
        MasterQ.Edit;
        MasterQHargaBBM.AsCurrency:=0;
    end;
    MasterQPendapatan.AsCurrency:=MasterQUangJalan2.AsCurrency+MasterQNominalKwitansi.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganSopir2.AsCurrency;
    MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQSPBULuarUangDiberi.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;
    MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

    if MasterQSisaDisetor.AsCurrency<0 then
    begin
      cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
    end
    else
    begin
      cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
    end;
    if (Pos('FINISHED',MasterQStatus.AsString)>0) then
    begin
      //MasterQstatusfinish.AsString:=MasterQStatus.AsString;
    end;
   // DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiSuratJalan.AsBoolean;
  end;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiSuratJalan.AsBoolean;
  MasterVGrid.Enabled:=True;
  if MasterQExtend.AsBoolean = TRUE then
  begin
     MasterQStatus.Value:='EXTEND';
     //cxDBVerticalGrid1Status.Properties.ItemIndex:=0;
  end
  else
  begin
      MasterQStatus.Value:='ON TIME';
  end;
  if (MasterQTglKembali.AsString='') or (MasterQTglKembali.AsVariant=null) then
  begin
  MasterQTglKembali.AsDateTime:= now;
  end;
  if MasterQVerifikasiStatus.AsString='VERIFIED' then
  begin
    MasterVGrid.OptionsData.Editing:=False;
    cxDBVerticalGrid1.OptionsData.Editing:=False;
    cxDBVerticalGrid2.OptionsData.Editing:=False;
    SaveBtn.Enabled:=False;
   // deleteBtn.Enabled:=False;
    VeriBtn.Enabled:=False;
  end
  else
  begin
    MasterVGrid.OptionsData.Editing:=True;
    cxDBVerticalGrid1.OptionsData.Editing:=True;
    cxDBVerticalGrid2.OptionsData.Editing:=True;
    SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiSuratJalan.AsBoolean;
    //deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiSuratJalan.AsBoolean;
    VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRSJ.AsBoolean;
  end;
end;

procedure TRealisasiSuratJalanFm.cxDBEditorRow10EditPropertiesEditValueChanged(
  Sender: TObject);
begin
  //if cxDBEditorRow10.Properties.Value='PENDING' then  cxDBEditorRow11.Visible:=true else cxDBEditorRow11.Visible:=false;

end;

procedure TRealisasiSuratJalanFm.cxDBEditorRow10EditPropertiesChange(Sender: TObject);
begin
  //if cxDBEditorRow10.Properties.Value='PENDING' then  cxDBEditorRow11.Visible:=true else cxDBEditorRow11.Visible:=false;
end;

procedure TRealisasiSuratJalanFm.cxDBEditorRow10PropertiesGetEditProperties(
  Sender: TcxCustomEditorRowProperties; ARecordIndex: Integer;
  var AProperties: TcxCustomEditProperties);
begin
  //if cxDBEditorRow10.Properties.Value='PENDING' then  cxDBEditorRow11.Visible:=true else cxDBEditorRow11.Visible:=false;
end;

procedure TRealisasiSuratJalanFm.cxDBEditorRow10PropertiesGetEditingProperties(
  Sender: TcxCustomEditorRowProperties; ARecordIndex: Integer;
  var AProperties: TcxCustomEditProperties);
begin
  //if cxDBEditorRow10.Properties.Value='PENDING' then  cxDBEditorRow11.Visible:=true else cxDBEditorRow11.Visible:=false;
end;

procedure TRealisasiSuratJalanFm.cxDBEditorRow10PropertiesGetDisplayText(
  Sender: TcxCustomEditorRowProperties; ARecord: Integer;
  var AText: String);
begin
  //if cxDBEditorRow10.Properties.Value='PENDING' then  cxDBEditorRow11.Visible:=true else cxDBEditorRow11.Visible:=false;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid1Exit(Sender: TObject);
begin
SaveBtn.SetFocus;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid1Enter(Sender: TObject);
begin
//cxDBVerticalGrid1.FocusRow(cxDBEditorRow6);
end;

procedure TRealisasiSuratJalanFm.cxButtonCetakClick(Sender: TObject);
var UangSaku: currency;
begin
if currtostr(MasterQUangJalan2.AsCurrency)='' then
begin
    UangSaku:=0;
end
else
begin
    UangSaku:=MasterQUangJalan2.AsCurrency;
end;
    //cetak SJ
    if MessageDlg('Cetak Realisasi Surat Jalan '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiSuratJalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP,sudahprint=1 where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;
      ViewKontrakQ.Close;
      ViewKontrakQ.Open;
    end;
    //end Cetak SJ
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid1DBAkhirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    {akhirQ.Close;
    akhirQ.ExecSQL;
    akhirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,akhirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQAkhir.AsString := akhirQkode.AsString;
      MasterQakhir_dari.AsString := akhirQDari.AsString;
      MasterQakhir_ke.AsString := akhirQKe.AsString;
      MasterQakhir_jarak.AsString := akhirQJumlahKm.AsString;
    end;
    DropDownFm.Release;   }
end;

procedure TRealisasiSuratJalanFm.KodeEditKeyPress(Sender: TObject;
  var Key: Char);
var akhirnya:String;
begin
  If Key = #13 Then Begin
      MasterVGrid.Enabled:=true;
      cxDBVerticalGrid1.Enabled:=true;
      cxButtonCetak.Enabled:=true;
      MasterQ.SQL.Clear;
      MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
      MasterQ.Open;
      if MasterQ.IsEmpty then
      begin
        StatusBar.Panels[0].Text:= 'Mode : Entry';
        MasterQ.Append;
        MasterQ.Edit;
      end
      else
      begin
        StatusBar.Panels[0].Text:= 'Mode : Edit';
        //KodeEdit.Text:= ViewKontrakQKodenota.AsString;
        MasterQ.Edit;
        if (Pos('FINISHED',MasterQStatus.AsString)>0) then
        begin
          //ShowMessage('sapiman');
         // MasterQstatusfinish.AsString:=MasterQStatus.AsString;
        end;
        //DeleteBtn.Enabled:=True;
      end;
      SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiSuratJalan.AsBoolean;
      MasterVGrid.Enabled:=True;
      //MasterQ.SQL.Clear;
      //MasterQ.SQL.Add(MasterOriSQL);
      akhirnya:='';
      if (MasterQTglKembali.AsString='') or (MasterQTglKembali.AsVariant=null) then
      begin
        MasterQTglKembali.AsDateTime:= now;
      end;

      if(MasterQTgl.AsDateTime<MasterQTglRealisasi.AsDateTime) then
      begin
        akhirnya:='START LATE'
      end;


      if (MasterQTglKembali.AsDateTime>(MasterQTglRealisasi.AsDateTime+MasterQwaktu.AsInteger)) then
      begin
        akhirnya:= akhirnya+'FINISHED LATE';
      end
      else
        akhirnya:= akhirnya+'FINISHED';
      begin
      end;

      if (akhirnya='') then akhirnya:='ON GOING';

      MasterQStatus.AsString:= akhirnya;

  end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2UangJalanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQUangJalan.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQUangJalan2.AsCurrency+MasterQNominalKwitansi.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganSopir2.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2SPBULuarUangDiberiEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarUangDiberi.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBUAYaniUang.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQSPBULuarUangDiberi.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQUangMakan.AsCurrency+MasterQOther.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2DBBiayaExtendEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaExtend.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQUangJalan.AsCurrency+MasterQSPBULuarUangDiberi.AsCurrency+MasterQBiayaExtend.AsCurrency+MasterQNominalKwitansi.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2NominalKwitansiEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQNominalKwitansi.AsVariant:=null
  else
    begin
      MasterQNominalKwitansi.AsCurrency:=DisplayValue;
      MasterQPendapatan.AsCurrency:=MasterQUangJalan2.AsCurrency+MasterQNominalKwitansi.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganSopir2.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2SPBUAYaniUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQSPBUAYaniUang.AsVariant:=null
  else
    MasterQSPBUAYaniUang.AsCurrency:=DisplayValue;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2SPBULuarUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQSPBULuarUang.AsVariant:=null
  else
    begin
      MasterQSPBULuarUang.AsCurrency:=DisplayValue;
      MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2tolEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQtol.AsVariant:=null
  else
    begin
      MasterQtol.AsCurrency:=DisplayValue;
      MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2PremiSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQPremiSopir.AsVariant:=null
  else
    begin
      MasterQPremiSopir.AsCurrency:=DisplayValue;
      MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;    
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2PremiSopir2EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiSopir2.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2TabunganSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQTabunganSopir.AsVariant:=null
  else
    begin
      MasterQTabunganSopir.AsCurrency:=DisplayValue;
      MasterQPendapatan.AsCurrency:=MasterQUangJalan2.AsCurrency+MasterQNominalKwitansi.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganSopir2.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
        cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
        cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2TabunganSopir2EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQTabunganSopir2.AsVariant:=null
  else
    begin
      MasterQTabunganSopir2.AsCurrency:=DisplayValue;
      MasterQPendapatan.AsCurrency:=MasterQUangJalan2.AsCurrency+MasterQNominalKwitansi.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganSopir2.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;    
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2PremiKernetEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiKernet.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2ClaimSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQClaimSopir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2BiayaLainLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaLainLain.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2UangMakanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
//MasterQUangMakan.AsCurrency:=DisplayValue;
//MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQSPBULuarUangDiberi.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQUangMakan.AsCurrency+MasterQOther.AsCurrency;

//MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2OtherEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQOther.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;


procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2SPBUAYaniLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin

MasterQSPBUAYaniLiter.AsFloat:=DisplayValue;
MasterQSPBUAYaniUang.AsCurrency:=MasterQSPBUAYaniLiter.AsFloat*MasterQHargaBBM.AsCurrency;
//MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency;

//MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2SPBULuarLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarLiter.AsFloat:=DisplayValue;
MasterQSPBULuarUang.AsCurrency:=MasterQSPBULuarLiter.AsFloat*MasterQHargaBBM.AsCurrency;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;

end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2UangInapEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQUangInap.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2UangParkirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQUangParkir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

if MasterQSisaDisetor.AsCurrency<0 then
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
end

else
begin
cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
end;
end;

procedure TRealisasiSuratJalanFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  viewKontrakQ.Close;
  viewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKontrakQ.Open;
end;

procedure TRealisasiSuratJalanFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  viewKontrakQ.Close;
  viewKontrakQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewKontrakQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewKontrakQ.Open;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2HargaBBMEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQHargaBBM.AsVariant:=null;
end;

procedure TRealisasiSuratJalanFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[14] = true) then
  begin
    ACanvas.Brush.Color :=clGradientInactiveCaption;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

procedure TRealisasiSuratJalanFm.cxDBVerticalGrid2DanaKebersihanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQDanaKebersihan.AsVariant:=null
  else
    begin
      MasterQDanaKebersihan.AsCurrency:=DisplayValue;
      MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUang.AsCurrency+MasterQtol.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiSopir2.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQClaimSopir.AsCurrency+MasterQBiayaLainLain.AsCurrency+MasterQOther.AsCurrency+MasterQUangInap.AsCurrency+MasterQUangParkir.AsCurrency+MasterQDanaKebersihan.AsCurrency;
      MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
      if MasterQSisaDisetor.AsCurrency<0 then
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clRed;
        end
      else
        begin
          cxDBVerticalGrid2SisaSetor.Styles.Content.TextColor:=clDefault;
        end;
    end;
end;

procedure TRealisasiSuratJalanFm.VeriBtnClick(Sender: TObject);
begin
  MasterQ.Edit;
  MasterQVerifikasiStatus.AsString:='VERIFIED';
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Verifikasi Berhasil');
      //set statuskwitansi ke Lunas jika ada TitipTagihan
      if MasterQNominalKwitansi.AsCurrency>0 then
      begin
          KwitansiQ.Close;
          KwitansiQ.ParamByName('text').AsString:=MasterQNoSO.AsString;
          KwitansiQ.Open;
          KwitansiQ.Edit;
          KwitansiQ.First;
          while KwitansiQ.Eof=false do
          begin
            UpdateKwitansiQ.Close;
            UpdateKwitansiQ.SQL.Text:='update Kwitansi set StatusKwitansi="LUNAS" where Kode='+QuotedStr(KwitansiQKodeKwitansi.AsString);
            UpdateKwitansiQ.ExecSQL;
            KwitansiQ.Next;
          end;
      end;
      //akhir set statuskwitansi

      //set km sekarang
      if (MasterQKmSekarang.AsFloat<>NULL) and (MasterQKmSekarang.AsFloat>0) then
      begin
          UpdateKwitansiQ.Close;
          UpdateKwitansiQ.SQL.Text:='update Armada set KmSekarang='+QuotedStr(floattostr(MasterQKmSekarang.AsFloat))+' where Kode='+QuotedStr(MasterQArmada.AsString);
          UpdateKwitansiQ.ExecSQL;
      end;

      //akhir set km sekarang
 except
  on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Verifikasi Gagal');
    end;
 end;
  KodeEdit.SetFocus;
end;

procedure TRealisasiSuratJalanFm.Button1Click(Sender: TObject);
begin
IsianPremiFm:=TIsianPremiFm.create(self);
end;

procedure TRealisasiSuratJalanFm.cxButton1Click(Sender: TObject);
begin
if MessageDlg('Cetak Kitir Realisasi Surat Jalan '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
begin
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KitirRealisasiSJ.rpt';
    Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    end;

end;
end.

