object RealisasiTrayekFm: TRealisasiTrayekFm
  Left = 289
  Top = 0
  Width = 1018
  Height = 744
  Caption = 'Realisasi Trayek'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1002
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object Button1: TButton
      Left = 232
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Patas'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 336
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Boomel'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 424
      Top = 8
      Width = 75
      Height = 25
      Caption = 'TOW'
      TabOrder = 3
      OnClick = Button3Click
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 640
    Width = 1002
    Height = 48
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 333
      Top = 11
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object buttonCetak: TcxButton
      Left = 255
      Top = 11
      Width = 75
      Height = 25
      Caption = 'CETAK'
      TabOrder = 2
      OnClick = buttonCetakClick
    end
    object deleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 3
      OnClick = deleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 704
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 46
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
    object VeriBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'VERIFIKASI'
      TabOrder = 5
      OnClick = VeriBtnClick
    end
    object cxButton1: TcxButton
      Left = 415
      Top = 11
      Width = 75
      Height = 25
      Caption = 'CETAK KITIR'
      TabOrder = 6
      OnClick = cxButton1Click
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 361
    Height = 416
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 158
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTanggal: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.OnValidate = MasterVGridTanggalEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Tanggal'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridKontrak: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKontrakEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kontrak'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridKodeRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KodeRute'
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridDari: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Dari'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object MasterVGridTujuan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Tujuan'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Armada'
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridNoPlat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoPlat'
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
    object MasterVGridPengemudi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPengemudiEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pengemudi'
      ID = 7
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridNamaPengemudi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPengemudi'
      Properties.Options.Editing = False
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object MasterVGridKondektur: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKondekturEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Kondektur'
      ID = 9
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridNamaKondektur: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaKondektur'
      Properties.Options.Editing = False
      ID = 10
      ParentID = 9
      Index = 0
      Version = 1
    end
    object MasterVGridKilometerAwal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KilometerAwal'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridKilometerAkhir: TcxDBEditorRow
      Properties.Caption = 'KilometerSekarang'
      Properties.DataBinding.FieldName = 'KilometerAkhir'
      ID = 12
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridDBCrew: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Crew'
      ID = 13
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridVerifikasi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Verifikasi'
      ID = 14
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridKeteranganVerifikasi: TcxDBEditorRow
      Height = 96
      Properties.DataBinding.FieldName = 'KeteranganVerifikasi'
      ID = 15
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      Properties.Options.Editing = False
      ID = 16
      ParentID = -1
      Index = 10
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 688
    Width = 1002
    Height = 18
    Panels = <
      item
        Width = 50
      end>
  end
  object cxLabel1: TcxLabel
    Left = 360
    Top = 64
  end
  object cxLabel2: TcxLabel
    Left = 360
    Top = 88
  end
  object cxLabel3: TcxLabel
    Left = 360
    Top = 112
  end
  object cxLabel4: TcxLabel
    Left = 360
    Top = 136
  end
  object cxLabel5: TcxLabel
    Left = 360
    Top = 160
  end
  object cxLabel6: TcxLabel
    Left = 360
    Top = 184
  end
  object MasterVGrid2: TcxDBVerticalGrid
    Left = 361
    Top = 48
    Width = 641
    Height = 416
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 204
    OptionsBehavior.GoToNextCellOnTab = True
    ParentFont = False
    TabOrder = 10
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGrid2CategoryRow1: TcxCategoryRow
      Properties.Caption = 'Pendapatan'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGrid2Rit1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1Rit1EditPropertiesValidate
      Properties.DataBinding.FieldName = 'Rit1'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGrid2Rit2: TcxDBEditorRow
      Height = 17
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1Rit2EditPropertiesValidate
      Properties.DataBinding.FieldName = 'Rit2'
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGrid2Rit3: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1Rit3EditPropertiesValidate
      Properties.DataBinding.FieldName = 'Rit3'
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object MasterVGrid2Rit4: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1Rit4EditPropertiesValidate
      Properties.DataBinding.FieldName = 'Rit4'
      ID = 4
      ParentID = 0
      Index = 3
      Version = 1
    end
    object MasterVGrid2Pendapatan: TcxDBEditorRow
      Properties.Caption = 'TotalPendapatan'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.DataBinding.FieldName = 'Pendapatan'
      Properties.Options.Editing = False
      ID = 5
      ParentID = 0
      Index = 4
      Version = 1
    end
    object MasterVGrid2CategoryRow2: TcxCategoryRow
      Properties.Caption = 'Pengeluaran'
      ID = 6
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGrid2CategoryRow3: TcxCategoryRow
      Properties.Caption = 'BBM'
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object MasterVGrid2HargaBBM: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.OnValidate = MasterVGrid2HargaBBMEditPropertiesValidate
      Properties.DataBinding.FieldName = 'HargaBBM'
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object MasterVGrid2CategoryRow4: TcxCategoryRow
      Properties.Caption = 'SPBU A. Yani'
      ID = 9
      ParentID = 7
      Index = 1
      Version = 1
    end
    object MasterVGrid2SPBUAYaniLiter: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCalcEditProperties'
      Properties.EditProperties.OnValidate = MasterVGrid2SPBUAYaniLiterEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBUAYaniLiter'
      ID = 10
      ParentID = 9
      Index = 0
      Version = 1
    end
    object MasterVGrid2SPBUAYaniUang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1SPBUAYaniUangEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBUAYaniUang'
      ID = 11
      ParentID = 9
      Index = 1
      Version = 1
    end
    object MasterVGrid2SPBUAYaniJam: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.Kind = ckDateTime
      Properties.DataBinding.FieldName = 'SPBUAYaniJam'
      ID = 12
      ParentID = 9
      Index = 2
      Version = 1
    end
    object MasterVGrid2SPBUAYaniDetail: TcxDBEditorRow
      Height = 48
      Properties.DataBinding.FieldName = 'SPBUAYaniDetail'
      ID = 13
      ParentID = 7
      Index = 2
      Version = 1
    end
    object MasterVGrid2CategoryRow5: TcxCategoryRow
      Properties.Caption = 'SPBU Luar'
      ID = 14
      ParentID = 7
      Index = 3
      Version = 1
    end
    object MasterVGrid2SPBULuarLiter: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCalcEditProperties'
      Properties.EditProperties.OnValidate = MasterVGrid2SPBULuarLiterEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBULuarLiter'
      ID = 15
      ParentID = 14
      Index = 0
      Version = 1
    end
    object MasterVGrid2SPBULuarUang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1SPBULuarUangEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBULuarUang'
      ID = 16
      ParentID = 14
      Index = 1
      Version = 1
    end
    object MasterVGrid2SPBULuarUangDiberi: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1SPBULuarUangDiberiEditPropertiesValidate
      Properties.DataBinding.FieldName = 'SPBULuarUangDiberi'
      Visible = False
      ID = 17
      ParentID = 14
      Index = 2
      Version = 1
    end
    object MasterVGrid2SPBULuarDetail: TcxDBEditorRow
      Height = 42
      Properties.DataBinding.FieldName = 'SPBULuarDetail'
      ID = 18
      ParentID = 14
      Index = 3
      Version = 1
    end
    object MasterVGrid2PremiSopir: TcxDBEditorRow
      Properties.Caption = 'PremiPengemudi'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1PremiSopirEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PremiSopir'
      ID = 19
      ParentID = 6
      Index = 1
      Version = 1
    end
    object MasterVGrid2TabunganSopir: TcxDBEditorRow
      Height = 17
      Properties.Caption = 'TabunganPengemudi'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1TabunganSopirEditPropertiesValidate
      Properties.DataBinding.FieldName = 'TabunganSopir'
      ID = 20
      ParentID = 6
      Index = 2
      Version = 1
    end
    object MasterVGrid2PremiKondektur: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1PremiKondekturEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PremiKondektur'
      ID = 21
      ParentID = 6
      Index = 3
      Version = 1
    end
    object MasterVGrid2TabunganKondektur: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1TabunganKondekturEditPropertiesValidate
      Properties.DataBinding.FieldName = 'TabunganKondektur'
      ID = 22
      ParentID = 6
      Index = 4
      Version = 1
    end
    object MasterVGrid2PremiKernet: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1PremiKernetEditPropertiesValidate
      Properties.DataBinding.FieldName = 'PremiKernet'
      ID = 23
      ParentID = 6
      Index = 5
      Version = 1
    end
    object MasterVGrid2UangMakan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1UangMakanEditPropertiesValidate
      Properties.DataBinding.FieldName = 'UangMakan'
      ID = 24
      ParentID = 6
      Index = 6
      Version = 1
    end
    object MasterVGrid2RetribusiTerminal: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1RetribusiTerminalEditPropertiesValidate
      Properties.DataBinding.FieldName = 'RetribusiTerminal'
      ID = 25
      ParentID = 6
      Index = 7
      Version = 1
    end
    object MasterVGrid2Parkir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1ParkirEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Parkir'
      ID = 26
      ParentID = 6
      Index = 8
      Version = 1
    end
    object MasterVGrid2Tol: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1TolEditPropertiesValidate
      Properties.DataBinding.FieldName = 'Tol'
      ID = 27
      ParentID = 6
      Index = 9
      Version = 1
    end
    object MasterVGrid2BiayaLainLain: TcxDBEditorRow
      Expanded = False
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.EditProperties.OnValidate = cxDBVerticalGrid1BiayaLainLainEditPropertiesValidate
      Properties.DataBinding.FieldName = 'BiayaLainLain'
      ID = 28
      ParentID = 6
      Index = 10
      Version = 1
    end
    object MasterVGrid2KeteranganBiayaLainLain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KeteranganBiayaLainLain'
      ID = 29
      ParentID = 28
      Index = 0
      Version = 1
    end
    object MasterVGrid2Pengeluaran: TcxDBEditorRow
      Height = 20
      Properties.Caption = 'TotalPengeluaran'
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Pengeluaran'
      Properties.Options.Editing = False
      ID = 30
      ParentID = 6
      Index = 11
      Version = 1
    end
    object MasterVGrid2SisaDisetor: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.DisplayFormat = 'Rp,0;(Rp,0)'
      Properties.EditProperties.UseThousandSeparator = True
      Properties.DataBinding.FieldName = 'SisaDisetor'
      Properties.Options.Editing = False
      ID = 31
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGrid2Keterangan: TcxDBEditorRow
      Height = 38
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 32
      ParentID = -1
      Index = 3
      Version = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 464
    Width = 1002
    Height = 176
    Align = alBottom
    BevelOuter = bvRaised
    BevelWidth = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      DataController.DataSource = SJDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'Dari'
        Width = 183
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Tujuan'
        Width = 144
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPengemudi'
        Options.SortByDisplayText = isbtOn
        Width = 209
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNomor'
        Width = 91
      end
      object cxGrid1DBTableView1Pembuat: TcxGridDBColumn
        DataBinding.FieldName = 'Pembuat'
        Width = 109
      end
      object cxGrid1DBTableView1Verifikasi: TcxGridDBColumn
        DataBinding.FieldName = 'Verifikasi'
        Visible = False
      end
      object cxGrid1DBTableView1KeteranganVerifikasi: TcxGridDBColumn
        DataBinding.FieldName = 'KeteranganVerifikasi'
        Visible = False
      end
      object cxGrid1DBTableView1CreateDate: TcxGridDBColumn
        DataBinding.FieldName = 'CreateDate'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *,cast('#39#39' as money) as HargaBBM from RealisasiTrayek')
    UpdateObject = MasterUS
    Left = 249
    Top = 33
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object MasterQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object MasterQKilometerAwal: TIntegerField
      FieldName = 'KilometerAwal'
    end
    object MasterQKilometerAkhir: TIntegerField
      FieldName = 'KilometerAkhir'
    end
    object MasterQRit1: TCurrencyField
      FieldName = 'Rit1'
    end
    object MasterQRit2: TCurrencyField
      FieldName = 'Rit2'
    end
    object MasterQRit3: TCurrencyField
      FieldName = 'Rit3'
    end
    object MasterQRit4: TCurrencyField
      FieldName = 'Rit4'
    end
    object MasterQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object MasterQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object MasterQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object MasterQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object MasterQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object MasterQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object MasterQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object MasterQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object MasterQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object MasterQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object MasterQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object MasterQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object MasterQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object MasterQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object MasterQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object MasterQTabunganKondektur: TCurrencyField
      FieldName = 'TabunganKondektur'
    end
    object MasterQRetribusiTerminal: TCurrencyField
      FieldName = 'RetribusiTerminal'
    end
    object MasterQParkir: TCurrencyField
      FieldName = 'Parkir'
    end
    object MasterQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object MasterQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object MasterQKeteranganBiayaLainLain: TMemoField
      FieldName = 'KeteranganBiayaLainLain'
      BlobType = ftMemo
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Rute'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object MasterQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object MasterQNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object MasterQNamaKondektur: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKondektur'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kondektur'
      Size = 50
      Lookup = True
    end
    object MasterQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object MasterQHargaBBM: TCurrencyField
      FieldName = 'HargaBBM'
    end
    object MasterQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object MasterQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
    object MasterQJenisBBM: TStringField
      FieldKind = fkLookup
      FieldName = 'JenisBBM'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'JenisBBM'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 340
    Top = 30
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, Tanggal, Armada, Pengemudi, Kondektur, Cre' +
        'w, KilometerAwal, KilometerAkhir, Rit1, Rit2, Rit3, Rit4, Pendap' +
        'atan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYaniUang, S' +
        'PBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangDiberi, SP' +
        'BULuarDetail, UangMakan, PremiSopir, PremiKondektur, PremiKernet' +
        ', TabunganSopir, TabunganKondektur, RetribusiTerminal, Parkir, T' +
        'ol, BiayaLainLain, KeteranganBiayaLainLain, Keterangan, CreateBy' +
        ', Operator, CreateDate, TglEntry, SPBUAYaniDetail, Verifikasi, K' +
        'eteranganVerifikasi, Status'#13#10'from RealisasiTrayek'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiTrayek'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  Tanggal = :Tanggal,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  Kondektur = :Kondektur,'
      '  Crew = :Crew,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Rit1 = :Rit1,'
      '  Rit2 = :Rit2,'
      '  Rit3 = :Rit3,'
      '  Rit4 = :Rit4,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  UangMakan = :UangMakan,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKondektur = :PremiKondektur,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganKondektur = :TabunganKondektur,'
      '  RetribusiTerminal = :RetribusiTerminal,'
      '  Parkir = :Parkir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  Keterangan = :Keterangan,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry,'
      '  SPBUAYaniDetail = :SPBUAYaniDetail,'
      '  Verifikasi = :Verifikasi,'
      '  KeteranganVerifikasi = :KeteranganVerifikasi,'
      '  Status = :Status'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiTrayek'
      
        '  (Kode, Kontrak, Tanggal, Armada, Pengemudi, Kondektur, Crew, K' +
        'ilometerAwal, KilometerAkhir, Rit1, Rit2, Rit3, Rit4, Pendapatan' +
        ', Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUA' +
        'YaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULu' +
        'arDetail, UangMakan, PremiSopir, PremiKondektur, PremiKernet, Ta' +
        'bunganSopir, TabunganKondektur, RetribusiTerminal, Parkir, Tol, ' +
        'BiayaLainLain, KeteranganBiayaLainLain, Keterangan, CreateBy, Op' +
        'erator, CreateDate, TglEntry, SPBUAYaniDetail, Verifikasi, Keter' +
        'anganVerifikasi, Status)'
      'values'
      
        '  (:Kode, :Kontrak, :Tanggal, :Armada, :Pengemudi, :Kondektur, :' +
        'Crew, :KilometerAwal, :KilometerAkhir, :Rit1, :Rit2, :Rit3, :Rit' +
        '4, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUAYaniLiter, :SP' +
        'BUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBULuarUang, :SPBU' +
        'LuarUangDiberi, :SPBULuarDetail, :UangMakan, :PremiSopir, :Premi' +
        'Kondektur, :PremiKernet, :TabunganSopir, :TabunganKondektur, :Re' +
        'tribusiTerminal, :Parkir, :Tol, :BiayaLainLain, :KeteranganBiaya' +
        'LainLain, :Keterangan, :CreateBy, :Operator, :CreateDate, :TglEn' +
        'try, :SPBUAYaniDetail, :Verifikasi, :KeteranganVerifikasi, :Stat' +
        'us)')
    DeleteSQL.Strings = (
      'delete from RealisasiTrayek'
      'where'
      '  Kode = :OLD_Kode')
    Left = 380
    Top = 42
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from RealisasiTrayek order by kode desc')
    Left = 297
    Top = 23
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterso where armada <> NULL')
    Left = 417
    Top = 31
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 465
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pelanggan')
    Left = 513
    Top = 7
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object viewSJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select p.nama as Pembuat,cast(rt.tanggal as date) as Tgl,rt.* '
      'from RealisasiTrayek rt'
      'left join [user] u on rt.createBy=u.kode'
      'left join pegawai p on p.kode=u.kodepegawai'
      'where (tanggal>=:text1 and tanggal<=:text2) or tanggal is null'
      'order by tglentry desc')
    Left = 569
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewSJQKodePelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'KodePelanggan'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Pelanggan'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object viewSJQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'KodePelanggan'
      Size = 50
      Lookup = True
    end
    object viewSJQKodeRute: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeRute'
      LookupDataSet = KontrakQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Rute'
      KeyFields = 'Kontrak'
      Size = 10
      Lookup = True
    end
    object viewSJQDari: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object viewSJQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'KodeRute'
      Size = 50
      Lookup = True
    end
    object viewSJQNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object viewSJQPlatNomor: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNomor'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object viewSJQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewSJQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object viewSJQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object viewSJQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewSJQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object viewSJQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object viewSJQCrew: TStringField
      FieldName = 'Crew'
      Size = 50
    end
    object viewSJQKilometerAwal: TIntegerField
      FieldName = 'KilometerAwal'
    end
    object viewSJQKilometerAkhir: TIntegerField
      FieldName = 'KilometerAkhir'
    end
    object viewSJQRit1: TCurrencyField
      FieldName = 'Rit1'
    end
    object viewSJQRit2: TCurrencyField
      FieldName = 'Rit2'
    end
    object viewSJQRit3: TCurrencyField
      FieldName = 'Rit3'
    end
    object viewSJQRit4: TCurrencyField
      FieldName = 'Rit4'
    end
    object viewSJQPendapatan: TCurrencyField
      FieldName = 'Pendapatan'
    end
    object viewSJQPengeluaran: TCurrencyField
      FieldName = 'Pengeluaran'
    end
    object viewSJQSisaDisetor: TCurrencyField
      FieldName = 'SisaDisetor'
    end
    object viewSJQSPBUAYaniLiter: TFloatField
      FieldName = 'SPBUAYaniLiter'
    end
    object viewSJQSPBUAYaniUang: TCurrencyField
      FieldName = 'SPBUAYaniUang'
    end
    object viewSJQSPBUAYaniJam: TDateTimeField
      FieldName = 'SPBUAYaniJam'
    end
    object viewSJQSPBULuarLiter: TFloatField
      FieldName = 'SPBULuarLiter'
    end
    object viewSJQSPBULuarUang: TCurrencyField
      FieldName = 'SPBULuarUang'
    end
    object viewSJQSPBULuarUangDiberi: TCurrencyField
      FieldName = 'SPBULuarUangDiberi'
    end
    object viewSJQSPBULuarDetail: TMemoField
      FieldName = 'SPBULuarDetail'
      BlobType = ftMemo
    end
    object viewSJQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object viewSJQPremiSopir: TCurrencyField
      FieldName = 'PremiSopir'
    end
    object viewSJQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object viewSJQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object viewSJQTabunganSopir: TCurrencyField
      FieldName = 'TabunganSopir'
    end
    object viewSJQTabunganKondektur: TCurrencyField
      FieldName = 'TabunganKondektur'
    end
    object viewSJQRetribusiTerminal: TCurrencyField
      FieldName = 'RetribusiTerminal'
    end
    object viewSJQParkir: TCurrencyField
      FieldName = 'Parkir'
    end
    object viewSJQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object viewSJQBiayaLainLain: TCurrencyField
      FieldName = 'BiayaLainLain'
    end
    object viewSJQKeteranganBiayaLainLain: TMemoField
      FieldName = 'KeteranganBiayaLainLain'
      BlobType = ftMemo
    end
    object viewSJQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object viewSJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewSJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewSJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewSJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewSJQPembuat: TStringField
      FieldName = 'Pembuat'
      Size = 50
    end
    object viewSJQTgl: TStringField
      FieldName = 'Tgl'
    end
    object viewSJQSPBUAYaniDetail: TStringField
      FieldName = 'SPBUAYaniDetail'
      Size = 200
    end
    object viewSJQVerifikasi: TCurrencyField
      FieldName = 'Verifikasi'
    end
    object viewSJQKeteranganVerifikasi: TMemoField
      FieldName = 'KeteranganVerifikasi'
      BlobType = ftMemo
    end
  end
  object SJDS: TDataSource
    DataSet = viewSJQ
    Left = 612
    Top = 6
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from rute')
    Left = 633
    Top = 55
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
      Required = True
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object jarakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from jarak')
    Left = 721
    Top = 7
    object jarakQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jarakQDari: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object jarakQKe: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object jarakQJumlahKm: TIntegerField
      FieldName = 'JumlahKm'
      Required = True
    end
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Left = 177
    Top = 7
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 1
    Version.Windows.Build = '7600'
    TempPath = 'C:\Users\toshiba\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'Tahoma'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'Tahoma'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'Tahoma'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'Tahoma'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'Tahoma'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'Tahoma'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'Tahoma'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'Tahoma'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'Tahoma'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'Tahoma'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'Tahoma'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'Tahoma'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'Tahoma'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'Tahoma'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 720
    Top = 56
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select k.*,r.Muat as Dari, r.Bongkar as Tujuan from Kontrak k, r' +
        'ute r where k.rute=r.kode and r.kategori like '#39'%TRAYEK%'#39)
    Left = 680
    Top = 16
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object KontrakQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaPT'
      KeyFields = 'Pelanggan'
      Size = 50
      Lookup = True
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Visible = False
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
      Visible = False
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
      Visible = False
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Visible = False
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Visible = False
      Size = 10
    end
    object KontrakQAC: TBooleanField
      FieldName = 'AC'
      Visible = False
    end
    object KontrakQToilet: TBooleanField
      FieldName = 'Toilet'
      Visible = False
    end
    object KontrakQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Visible = False
    end
    object KontrakQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
      Visible = False
    end
    object KontrakQHarga: TCurrencyField
      FieldName = 'Harga'
      Visible = False
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object KontrakQDari: TStringField
      FieldName = 'Dari'
      Required = True
      Size = 50
    end
    object KontrakQTujuan: TStringField
      FieldName = 'Tujuan'
      Required = True
      Size = 50
    end
    object KontrakQIntervalPenagihan: TStringField
      FieldName = 'IntervalPenagihan'
      Size = 50
    end
    object KontrakQPOEksternal: TStringField
      FieldName = 'POEksternal'
      Size = 200
    end
    object KontrakQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 200
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 673
    Top = 63
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object NotifQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select sj.tgl, so.Armada from MasterSJ sj, MasterSO so where sj.' +
        'NoSO=so.Kodenota and so.Armada=:text and sj.Tgl=(select convert(' +
        'datetime,:text2))')
    Left = 312
    Top = 56
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object NotifQtgl: TDateTimeField
      FieldName = 'tgl'
      Required = True
    end
    object NotifQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
  end
  object HargaBBMQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from HargaBBM')
    Left = 384
    Top = 384
    object HargaBBMQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object HargaBBMQHargaSolar: TCurrencyField
      FieldName = 'HargaSolar'
      Required = True
    end
    object HargaBBMQHargaBensin: TCurrencyField
      FieldName = 'HargaBensin'
      Required = True
    end
  end
  object PersenPremiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from MasterPersenPremi')
    Left = 288
    Top = 104
    object PersenPremiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PersenPremiQPatas1: TFloatField
      FieldName = 'Patas1'
    end
    object PersenPremiQPatas2: TFloatField
      FieldName = 'Patas2'
    end
    object PersenPremiQPatas3: TFloatField
      FieldName = 'Patas3'
    end
    object PersenPremiQBoomel1: TFloatField
      FieldName = 'Boomel1'
    end
    object PersenPremiQBoomel2: TFloatField
      FieldName = 'Boomel2'
    end
    object PersenPremiQBoomel3: TFloatField
      FieldName = 'Boomel3'
    end
    object PersenPremiQPatasKernet1: TFloatField
      FieldName = 'PatasKernet1'
    end
    object PersenPremiQPatasKernet2: TFloatField
      FieldName = 'PatasKernet2'
    end
    object PersenPremiQPatasKernet3: TFloatField
      FieldName = 'PatasKernet3'
    end
    object PersenPremiQBoomelKernet1: TFloatField
      FieldName = 'BoomelKernet1'
    end
    object PersenPremiQBoomelKernet2: TFloatField
      FieldName = 'BoomelKernet2'
    end
    object PersenPremiQBoomeKernet3: TFloatField
      FieldName = 'BoomeKernet3'
    end
    object PersenPremiQBoomelKondektur1: TFloatField
      FieldName = 'BoomelKondektur1'
    end
    object PersenPremiQBoomelKondektur2: TFloatField
      FieldName = 'BoomelKondektur2'
    end
    object PersenPremiQBoomelKondektur3: TFloatField
      FieldName = 'BoomelKondektur3'
    end
  end
end
