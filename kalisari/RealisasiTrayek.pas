unit RealisasiTrayek;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxCurrencyEdit,
  cxDropDownEdit, cxGroupBox, cxCalc;

type
  TRealisasiTrayekFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    MasterVGrid2: TcxDBVerticalGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    RuteQ: TSDQuery;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    updateQ: TSDQuery;
    buttonCetak: TcxButton;
    jarakQJumlahKm: TIntegerField;
    deleteBtn: TcxButton;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    Crpe1: TCrpe;
    KodeQkode: TStringField;
    MasterVGridKontrak: TcxDBEditorRow;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridPengemudi: TcxDBEditorRow;
    MasterVGridKondektur: TcxDBEditorRow;
    MasterVGridKilometerAwal: TcxDBEditorRow;
    MasterVGridKilometerAkhir: TcxDBEditorRow;
    MasterVGrid2Tol: TcxDBEditorRow;
    MasterVGrid2Keterangan: TcxDBEditorRow;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    MasterVGridNoPlat: TcxDBEditorRow;
    MasterVGridNamaPengemudi: TcxDBEditorRow;
    MasterVGridNamaKondektur: TcxDBEditorRow;
    MasterVGrid2Pengeluaran: TcxDBEditorRow;
    MasterVGrid2SisaDisetor: TcxDBEditorRow;
    MasterVGrid2UangMakan: TcxDBEditorRow;
    MasterVGrid2RetribusiTerminal: TcxDBEditorRow;
    MasterVGrid2Parkir: TcxDBEditorRow;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    viewSJQKodePelanggan: TStringField;
    viewSJQNamaPelanggan: TStringField;
    MasterVGridKodeRute: TcxDBEditorRow;
    MasterVGridDari: TcxDBEditorRow;
    MasterVGridTujuan: TcxDBEditorRow;
    MasterVGrid2Rit1: TcxDBEditorRow;
    MasterVGrid2Rit2: TcxDBEditorRow;
    MasterVGrid2Rit3: TcxDBEditorRow;
    MasterVGrid2Rit4: TcxDBEditorRow;
    MasterVGrid2Pendapatan: TcxDBEditorRow;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    viewSJQKodeRute: TStringField;
    viewSJQDari: TStringField;
    viewSJQTujuan: TStringField;
    viewSJQNamaPengemudi: TStringField;
    viewSJQPlatNomor: TStringField;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    KontrakQDari: TStringField;
    KontrakQTujuan: TStringField;
    MasterQKode: TStringField;
    MasterQKontrak: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQPengemudi: TStringField;
    MasterQKondektur: TStringField;
    MasterQCrew: TStringField;
    MasterQKilometerAwal: TIntegerField;
    MasterQKilometerAkhir: TIntegerField;
    MasterQRit1: TCurrencyField;
    MasterQRit2: TCurrencyField;
    MasterQRit3: TCurrencyField;
    MasterQRit4: TCurrencyField;
    MasterQPendapatan: TCurrencyField;
    MasterQPengeluaran: TCurrencyField;
    MasterQSisaDisetor: TCurrencyField;
    MasterQSPBUAYaniLiter: TFloatField;
    MasterQSPBUAYaniUang: TCurrencyField;
    MasterQSPBUAYaniJam: TDateTimeField;
    MasterQSPBULuarLiter: TFloatField;
    MasterQSPBULuarUang: TCurrencyField;
    MasterQSPBULuarUangDiberi: TCurrencyField;
    MasterQSPBULuarDetail: TMemoField;
    MasterQUangMakan: TCurrencyField;
    MasterQPremiSopir: TCurrencyField;
    MasterQPremiKondektur: TCurrencyField;
    MasterQPremiKernet: TCurrencyField;
    MasterQTabunganSopir: TCurrencyField;
    MasterQTabunganKondektur: TCurrencyField;
    MasterQRetribusiTerminal: TCurrencyField;
    MasterQParkir: TCurrencyField;
    MasterQTol: TCurrencyField;
    MasterQBiayaLainLain: TCurrencyField;
    MasterQKeteranganBiayaLainLain: TMemoField;
    MasterQKeterangan: TMemoField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    viewSJQKode: TStringField;
    viewSJQKontrak: TStringField;
    viewSJQTanggal: TDateTimeField;
    viewSJQArmada: TStringField;
    viewSJQPengemudi: TStringField;
    viewSJQKondektur: TStringField;
    viewSJQCrew: TStringField;
    viewSJQKilometerAwal: TIntegerField;
    viewSJQKilometerAkhir: TIntegerField;
    viewSJQRit1: TCurrencyField;
    viewSJQRit2: TCurrencyField;
    viewSJQRit3: TCurrencyField;
    viewSJQRit4: TCurrencyField;
    viewSJQPendapatan: TCurrencyField;
    viewSJQPengeluaran: TCurrencyField;
    viewSJQSisaDisetor: TCurrencyField;
    viewSJQSPBUAYaniLiter: TFloatField;
    viewSJQSPBUAYaniUang: TCurrencyField;
    viewSJQSPBUAYaniJam: TDateTimeField;
    viewSJQSPBULuarLiter: TFloatField;
    viewSJQSPBULuarUang: TCurrencyField;
    viewSJQSPBULuarUangDiberi: TCurrencyField;
    viewSJQSPBULuarDetail: TMemoField;
    viewSJQUangMakan: TCurrencyField;
    viewSJQPremiSopir: TCurrencyField;
    viewSJQPremiKondektur: TCurrencyField;
    viewSJQPremiKernet: TCurrencyField;
    viewSJQTabunganSopir: TCurrencyField;
    viewSJQTabunganKondektur: TCurrencyField;
    viewSJQRetribusiTerminal: TCurrencyField;
    viewSJQParkir: TCurrencyField;
    viewSJQTol: TCurrencyField;
    viewSJQBiayaLainLain: TCurrencyField;
    viewSJQKeteranganBiayaLainLain: TMemoField;
    viewSJQKeterangan: TMemoField;
    viewSJQCreateBy: TStringField;
    viewSJQOperator: TStringField;
    viewSJQCreateDate: TDateTimeField;
    viewSJQTglEntry: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    KontrakQIntervalPenagihan: TStringField;
    MasterQKodeRute: TStringField;
    MasterQDari: TStringField;
    MasterQTujuan: TStringField;
    MasterQNoPlat: TStringField;
    MasterQNamaPengemudi: TStringField;
    MasterQNamaKondektur: TStringField;
    MasterVGridDBCrew: TcxDBEditorRow;
    MasterVGrid2CategoryRow1: TcxCategoryRow;
    MasterVGrid2CategoryRow2: TcxCategoryRow;
    MasterVGrid2SPBUAYaniLiter: TcxDBEditorRow;
    MasterVGrid2SPBUAYaniUang: TcxDBEditorRow;
    MasterVGrid2SPBUAYaniJam: TcxDBEditorRow;
    MasterVGrid2SPBULuarLiter: TcxDBEditorRow;
    MasterVGrid2SPBULuarUang: TcxDBEditorRow;
    MasterVGrid2SPBULuarUangDiberi: TcxDBEditorRow;
    MasterVGrid2SPBULuarDetail: TcxDBEditorRow;
    MasterVGrid2PremiSopir: TcxDBEditorRow;
    MasterVGrid2PremiKondektur: TcxDBEditorRow;
    MasterVGrid2PremiKernet: TcxDBEditorRow;
    MasterVGrid2TabunganSopir: TcxDBEditorRow;
    MasterVGrid2TabunganKondektur: TcxDBEditorRow;
    MasterVGrid2BiayaLainLain: TcxDBEditorRow;
    MasterVGrid2KeteranganBiayaLainLain: TcxDBEditorRow;
    MasterVGrid2CategoryRow3: TcxCategoryRow;
    MasterVGrid2CategoryRow4: TcxCategoryRow;
    MasterVGrid2CategoryRow5: TcxCategoryRow;
    SOQTglPelunasan: TDateTimeField;
    NotifQ: TSDQuery;
    NotifQtgl: TDateTimeField;
    NotifQArmada: TStringField;
    KontrakQNamaPelanggan: TStringField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    MasterQSPBUAYaniDetail: TStringField;
    MasterVGrid2SPBUAYaniDetail: TcxDBEditorRow;
    KontrakQPOEksternal: TStringField;
    KontrakQPlatNo: TStringField;
    MasterQHargaBBM: TCurrencyField;
    MasterVGrid2HargaBBM: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxGrid1DBTableView1Pembuat: TcxGridDBColumn;
    viewSJQPembuat: TStringField;
    viewSJQTgl: TStringField;
    viewSJQSPBUAYaniDetail: TStringField;
    MasterQVerifikasi: TCurrencyField;
    MasterQKeteranganVerifikasi: TMemoField;
    MasterVGridVerifikasi: TcxDBEditorRow;
    MasterVGridKeteranganVerifikasi: TcxDBEditorRow;
    viewSJQVerifikasi: TCurrencyField;
    viewSJQKeteranganVerifikasi: TMemoField;
    cxGrid1DBTableView1Verifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganVerifikasi: TcxGridDBColumn;
    MasterQJenisBBM: TStringField;
    HargaBBMQ: TSDQuery;
    HargaBBMQKode: TStringField;
    HargaBBMQHargaSolar: TCurrencyField;
    HargaBBMQHargaBensin: TCurrencyField;
    VeriBtn: TcxButton;
    MasterQStatus: TStringField;
    MasterVGridStatus: TcxDBEditorRow;
    cxGrid1DBTableView1CreateDate: TcxGridDBColumn;
    Button1: TButton;
    Button2: TButton;
    PersenPremiQ: TSDQuery;
    PersenPremiQKode: TStringField;
    PersenPremiQPatas1: TFloatField;
    PersenPremiQPatas2: TFloatField;
    PersenPremiQPatas3: TFloatField;
    PersenPremiQBoomel1: TFloatField;
    PersenPremiQBoomel2: TFloatField;
    PersenPremiQBoomel3: TFloatField;
    PersenPremiQPatasKernet1: TFloatField;
    PersenPremiQPatasKernet2: TFloatField;
    PersenPremiQPatasKernet3: TFloatField;
    PersenPremiQBoomelKernet1: TFloatField;
    PersenPremiQBoomelKernet2: TFloatField;
    PersenPremiQBoomeKernet3: TFloatField;
    PersenPremiQBoomelKondektur1: TFloatField;
    PersenPremiQBoomelKondektur2: TFloatField;
    PersenPremiQBoomelKondektur3: TFloatField;
    cxButton1: TcxButton;
    Button3: TButton;
    //procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure cxDBVerticalGrid1SopirEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKontrakEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPengemudiEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKondekturEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1Rit1EditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1Rit2EditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1Rit3EditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1Rit4EditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1SPBUAYaniUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1SPBULuarUangDiberiEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1SPBULuarUangEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PremiSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PremiKondekturEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1PremiKernetEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1TabunganSopirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1TabunganKondekturEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1UangMakanEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1RetribusiTerminalEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1ParkirEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1TolEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1BiayaLainLainEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGrid2SPBUAYaniLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGrid2SPBULuarLiterEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridTanggalEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure VeriBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure MasterVGrid2HargaBBMEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure Button3Click(Sender: TObject);
  private
  procedure CreateParams(var Params: TCreateParams); override;
  procedure WMSize(var Msg: TMessage); message WM_SIZE;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent; kd:string);
  end;

var
  RealisasiTrayekFm: TRealisasiTrayekFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, StrUtils, DateUtils, KontrakDropDown, ArmadaDropDown, PengemudiDropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TRealisasiTrayekFm.Create(aOwner: TComponent; kd:string);
begin
  inherited Create(aOwner);
end;

procedure TRealisasiTrayekFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  if (FormStyle = fsNormal) then begin
    {Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
    Params.WndParent := GetDesktopWindow;    }
    WindowState:=wsMinimized;
  end;
end;

procedure TRealisasiTrayekFm.WMSize(var Msg: TMessage);
begin
  if Msg.WParam  = SIZE_MAXIMIZED then
     ShowWindow(RealisasiTrayekFm.Handle, SW_RESTORE) ;
end;

{procedure TRealisasiTrayekFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;  }

procedure TRealisasiTrayekFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TRealisasiTrayekFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;
end;

procedure TRealisasiTrayekFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;   
end;

procedure TRealisasiTrayekFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Edit;
  MasterQHargaBBM.AsCurrency:=0;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  //MasterQTanggal.AsDateTime:=DMFm.GetDateQNow.asDate;
  DMFm.GetDateQ.Close;
 // MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  MasterVGrid2.Enabled:=true;
end;

procedure TRealisasiTrayekFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  MasterVGrid2.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TRealisasiTrayekFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  //cxGrid1DBTableView1.OptionsView.ColumnAutoWidth:=true;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewSJQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertRealisasiTrayek.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiTrayek.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiTrayek.AsBoolean;
  HargaBBMQ.Open;
  PersenPremiQ.Open;
end;

procedure TRealisasiTrayekFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteRealisasiTrayek.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiTrayek.AsBoolean;
    MasterVGrid.Enabled:=True;
    buttonCetak.Enabled:=true;
    if MasterQStatus.AsString='VERIFIED' then
    begin
      MasterVGrid.OptionsData.Editing:=False;
      SaveBtn.Enabled:=False;
      deleteBtn.Enabled:=False;
      VeriBtn.Enabled:=False;
    end
    else
    begin
      MasterVGrid.OptionsData.Editing:=True;
      SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiTrayek.AsBoolean;
      deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiTrayek.AsBoolean;
      VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRTrayek.AsBoolean;
    end;
  end;
end;

procedure TRealisasiTrayekFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TRealisasiTrayekFm.SaveBtnClick(Sender: TObject);
var sKode,kode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    //MasterQStatus.AsString :='IN PROGRESS';
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  kode:=MasterQKode.AsString;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    sKode := MasterQKode.AsString;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Realisasi Trayek dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    viewSJQ.Close;
    viewSJQ.Open;
    KodeEdit.SetFocus;

    //cetak SJ
    if MessageDlg('Cetak Realisasi Trayek '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiTrayek.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kode;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     { Crpe1.Print;
      Crpe1.CloseWindow;}
      {updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;   }
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
  //except
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
        viewSJQ.Close;
      viewSJQ.Open;
end;

procedure TRealisasiTrayekFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TRealisasiTrayekFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;   }
  //MasterQStatus.AsString:='ON GOING';
  {if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;}
end;

procedure TRealisasiTrayekFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Realisasi Trayek '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Realisasi telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Realisasi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     viewSJQ.Close;
     viewSJQ.Open;
  end;
end;

procedure TRealisasiTrayekFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQkodenota.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQkodenota.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   // sopirQ.Close;
  //  sopirQ.ExecSQL;
   // sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQAwal.AsString := jarakQkode.AsString;
      MasterQjarak_dari.AsString := jarakQDari.AsString;
      MasterQjarak_ke.AsString := jarakQKe.AsString;
      MasterQjarak_jarak.AsString := jarakQJumlahKM.AsString;
    end;
    DropDownFm.Release; }
end;

procedure TRealisasiTrayekFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Realisasi Trayek '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RealisasiTrayek.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     { Crpe1.Print;
      Crpe1.CloseWindow;}
      {updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;   }
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1SopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    //sopirQ.Close;
    //sopirQ.ExecSQL;
    //sopirQ.Open;
  //  DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //sterQSopir.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1Sopir2EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    //sopirQ.Close;
   // sopirQ.ExecSQL;
   // sopirQ.Open;
   // DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //sterQSopir2.AsString := sopirQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TRealisasiTrayekFm.MasterVGridKontrakEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var a:string;
begin
{KontrakQ.Close;
KontrakQ.ExecSQL;
KontrakQ.Open; }
a:=KontrakQ.SQL.Text;
  //RealisasiTrayekFm.FormStyle:=fsNormal;
  //RealisasiTrayekFm.SendToBack;
KontrakDropDownFm:=TKontrakDropDownFm.Create(self,a);
KontrakDropDownFm.BringToFront;
if KontrakDropDownFm.ShowModal=MrOK then
begin

    MasterQ.Open;
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakDropDownFm.kode;
end;
KontrakDropDownFm.Release;
  //RealisasiTrayekFm.FormStyle:=fsStayOnTop;
  //RealisasiTrayekFm.BringToFront;
end;

procedure TRealisasiTrayekFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var mydate : TDateTime;
  var tempSQL,temp:string;

begin
{ArmadaQ.Close;
ArmadaQ.ExecSQL;
ArmadaQ.Open; }
  RealisasiTrayekFm.SendToBack;
ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
if ArmadaDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQArmada.AsString:=ArmadaDropDownFm.kode;
    if MasterQJenisBBM.AsString='BENSIN' then
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=HargaBBMQHargaBensin.AsCurrency
      end

      else if MasterQJenisBBM.AsString='SOLAR' then
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=HargaBBMQHargaSolar.AsCurrency;
      end

      else
      begin
          MasterQ.Edit;
          MasterQHargaBBM.AsCurrency:=0;
      end;
end;
ArmadaDropDownFm.Release;
  RealisasiTrayekFm.BringToFront;
mydate:=EncodeDateTime(StrToInt (FormatDateTime('yyyy',MasterQTanggal.AsDateTime)),StrToInt( FormatDateTime('mm',MasterQTanggal.AsDateTime)),StrToInt( FormatDateTime('dd',MasterQTanggal.AsDateTime)),0,0,0,0);
temp:=FormatDateTime('mm/dd/yyyy',mydate);
NotifQ.Close;
NotifQ.SQL.Clear;
NotifQ.SQL.Add('select sj.tgl, so.Armada from MasterSJ sj, MasterSO so where sj.NoSO=so.Kodenota and so.Armada=' +QuotedStr(ArmadaQKode.AsString)+ ' and sj.Tgl=(convert(datetime,' +QuotedStr(temp) +'))');
NotifQ.Open;
if NotifQArmada.AsString <> '' then
begin
    ShowMessage ('Armada '+MasterQArmada.AsString+' memiliki surat jalan untuk tanggal '+MasterQTanggal.AsString);
end;
end;

procedure TRealisasiTrayekFm.MasterVGridPengemudiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL : string;
  begin
{  pegawaiQ.Close;
  tempSQL:=PegawaiQ.SQL.Text;
  PegawaiQ.SQL.Text:= 'select * from pegawai where upper(jabatan)="PENGEMUDI"';
  pegawaiQ.Open; }
//sopirQ.Close;
//sopirQ.ExecSQL;
//sopirQ.Open;
  //RealisasiTrayekFm.SendToBack;
  //FormStyle:=fsNormal;
PengemudiDropDownFm:=TPengemudiDropDownFm.Create(self);
if PengemudiDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
   MasterQPengemudi.AsString:=PengemudiDropDownFm.kode;
end;
PengemudiDropDownFm.Release;
  //RealisasiTrayekFm.BringToFront;
  //FormStyle:=fsStayOnTop;
//pegawaiQ.SQL.Text:=tempSQL;
end;

procedure TRealisasiTrayekFm.MasterVGridKondekturEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{PegawaiQ.Close;
PegawaiQ.ExecSQL;
PegawaiQ.Open;}
  RealisasiTrayekFm.SendToBack;
PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
if PegawaiDropDownFm.ShowModal=MrOK then
begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKondektur.AsString:=PegawaiDropDownFm.kode;
end;
PegawaiDropDownFm.Release;
  RealisasiTrayekFm.BringToFront;
end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1Rit1EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQRit1.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1Rit2EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQRit2.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1Rit3EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQRit3.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1Rit4EditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQRit4.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1SPBUAYaniUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniUang.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1SPBULuarUangDiberiEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
//MasterQSPBULuarUangDiberi.AsCurrency:=DisplayValue;
//MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

//MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1SPBULuarUangEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarUang.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1PremiSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiSopir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1PremiKondekturEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiKondektur.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1PremiKernetEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQPremiKernet.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1TabunganSopirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTabunganSopir.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1TabunganKondekturEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTabunganKondektur.AsCurrency:=DisplayValue;
MasterQPendapatan.AsCurrency:=MasterQRit1.AsCurrency+MasterQRit2.AsCurrency+MasterQRit3.AsCurrency+masterQRit4.AsCurrency+MasterQTabunganSopir.AsCurrency+MasterQTabunganKondektur.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1UangMakanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQUangMakan.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1RetribusiTerminalEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQRetribusiTerminal.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1ParkirEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQParkir.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1TolEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQTol.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.cxDBVerticalGrid1BiayaLainLainEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQBiayaLainLain.AsCurrency:=DisplayValue;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;

MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;


procedure TRealisasiTrayekFm.MasterVGrid2SPBUAYaniLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBUAYaniLiter.AsFloat:=DisplayValue;
MasterQSPBUAYaniUang.AsCurrency:=MasterQSPBUAYaniLiter.AsFloat*MasterQHargaBBM.AsCurrency;
end;

procedure TRealisasiTrayekFm.MasterVGrid2SPBULuarLiterEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
MasterQSPBULuarLiter.AsFloat:=DisplayValue;
MasterQSPBULuarUang.AsCurrency:=MasterQSPBULuarLiter.AsFloat*MasterQHargaBBM.AsCurrency;
MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

procedure TRealisasiTrayekFm.MasterVGridTanggalEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
    var mydate : TDateTime;
  var tempSQL,temp:string;
begin
mydate:=EncodeDateTime(StrToInt (FormatDateTime('yyyy',MasterQTanggal.AsDateTime)),StrToInt( FormatDateTime('mm',MasterQTanggal.AsDateTime)),StrToInt( FormatDateTime('dd',MasterQTanggal.AsDateTime)),0,0,0,0);
temp:=FormatDateTime('mm/dd/yyyy',mydate);
NotifQ.Close;
NotifQ.SQL.Clear;
NotifQ.SQL.Add('select sj.tgl, so.Armada from MasterSJ sj, MasterSO so where sj.NoSO=so.Kodenota and so.Armada=' +QuotedStr(ArmadaQKode.AsString)+ ' and sj.Tgl=(convert(datetime,' +QuotedStr(temp) +'))');
NotifQ.Open;
if NotifQArmada.AsString <> '' then
begin
    ShowMessage ('Armada '+MasterQArmada.AsString+' memiliki surat jalan untuk tanggal '+MasterQTanggal.AsString);
end;
end;

procedure TRealisasiTrayekFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSJQ.Open;
end;

procedure TRealisasiTrayekFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  viewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewSJQ.Open;
end;

procedure TRealisasiTrayekFm.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[6] > 0) then
  begin
    ACanvas.Brush.Color := 16706935;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

procedure TRealisasiTrayekFm.VeriBtnClick(Sender: TObject);
begin
  MasterQ.Edit;
  MasterQStatus.AsString:='VERIFIED';
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    //sKode := MasterQKode.AsString;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Verifikasi dengan Kode ' + KodeEdit.Text + ' telah disimpan');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Verifikasi Gagal');
    end;
  end;
  viewSJQ.Close;
  viewSJQ.Open;
  KodeEdit.SetFocus;
end;

procedure TRealisasiTrayekFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  buttonCetak.Enabled:=true;
  KodeEdit.SetFocus;
  KodeEdit.text:=viewSJQKode.AsString;
  KodeEditExit(self);
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRealisasiTrayek.AsBoolean;
  //StatusBar.Panels[0].Text:= 'Mode : Edit';
  buttonCetak.Enabled:=true;
  MasterVGrid.Enabled:=True;
  MasterVGrid2.Enabled:=True;
  //MasterVGrid.SetFocus;
  if MasterQStatus.AsString='VERIFIED' then
  begin
    MasterVGrid.OptionsData.Editing:=False;
    SaveBtn.Enabled:=False;
    deleteBtn.Enabled:=False;
    VeriBtn.Enabled:=False;
  end
  else
  begin
    MasterVGrid.OptionsData.Editing:=True;
    SaveBtn.Enabled:=MenuUtamaFm.UserQUpdateRealisasiTrayek.AsBoolean;
    deleteBtn.Enabled:=MenuUtamaFm.UserQDeleteRealisasiTrayek.AsBoolean;
    VeriBtn.Enabled:=MenuUtamaFm.UserQVerifikasiRTrayek.AsBoolean;
  end;

end;

procedure TRealisasiTrayekFm.Button1Click(Sender: TObject);
var JumRit:integer;
begin
JumRit:=0;
if MasterQRit1.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit2.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit3.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit4.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;
if (MasterQPendapatan.AsCurrency>=975000) and (MasterQPendapatan.AsCurrency<1200000) then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatas1.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatasKernet1.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatas1.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatasKernet1.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatas1.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatasKernet1.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatas1.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatasKernet1.AsFloat/100;
   MasterQPremiKondektur.AsCurrency:=0;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end

else if (MasterQPendapatan.AsCurrency>=1200000) and (MasterQPendapatan.AsCurrency<1700000) then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatas2.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatasKernet2.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatas2.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatasKernet2.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatas2.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatasKernet2.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatas2.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatasKernet2.AsFloat/100;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end

else if MasterQPendapatan.AsCurrency>1700000 then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatas3.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQPatasKernet3.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatas3.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQPatasKernet3.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatas3.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQPatasKernet3.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=0;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatas3.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQPatasKernet3.AsFloat/100;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end;
end;

procedure TRealisasiTrayekFm.Button2Click(Sender: TObject);
var JumRit:integer;
begin
JumRit:=0;
if MasterQRit1.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit2.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit3.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit4.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;
if (MasterQPendapatan.AsCurrency>=1000000) and (MasterQPendapatan.AsCurrency<1200000) then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomel1.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomelKernet1.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomelKondektur1.AsFloat/100/4;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomel1.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomelKernet1.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomelKondektur1.AsFloat/100/2;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomel1.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomelKernet1.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomelKondektur1.AsFloat/100*3/4;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomel1.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomelKernet1.AsFloat/100;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomelKondektur1.AsFloat/100;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end

else if (MasterQPendapatan.AsCurrency>=1200000) and (MasterQPendapatan.AsCurrency<1450000) then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomel2.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomelKernet2.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomelKondektur2.AsFloat/100/4;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomel2.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomelKernet2.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomelKondektur2.AsFloat/100/2;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomel2.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomelKernet2.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomelKondektur2.AsFloat/100*3/4;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomel2.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomelKernet2.AsFloat/100;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomelKondektur2.AsFloat/100;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end

else if MasterQPendapatan.AsCurrency>1450000 then
begin
   MasterQ.Edit;
if JumRit=1 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomel3.AsFloat/100/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomeKernet3.AsFloat/100/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4*PersenPremiQBoomelKondektur3.AsFloat/100/4;
end

else if JumRit=2 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomel3.AsFloat/100/2;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomeKernet3.AsFloat/100/2;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*2*PersenPremiQBoomelKondektur3.AsFloat/100/2;
end

else if JumRit=3 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomel3.AsFloat/100*3/4;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomeKernet3.AsFloat/100*3/4;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*4/3*PersenPremiQBoomelKondektur3.AsFloat/100*3/4;
end

else if JumRit=4 then
begin
   MasterQPremiSopir.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomel3.AsFloat/100;
   MasterQPremiKernet.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomeKernet3.AsFloat/100;
   MasterQPremiKondektur.AsCurrency:=MasterQPendapatan.AsCurrency*PersenPremiQBoomelKondektur3.AsFloat/100;
end;

MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;
end;
end;

procedure TRealisasiTrayekFm.cxButton1Click(Sender: TObject);
begin
if MessageDlg('Cetak Kitir Realisasi Trayek '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
begin
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'KitirRealisasiTrayek.rpt';
    Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    end;
end;

procedure TRealisasiTrayekFm.MasterVGrid2HargaBBMEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue<>null then if VarToStr(DisplayValue)='' then
    MasterQHargaBBM.AsVariant:=null;
end;

procedure TRealisasiTrayekFm.Button3Click(Sender: TObject);
var JumRit:integer;
begin
JumRit:=0;
if MasterQRit1.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit2.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit3.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if MasterQRit4.AsCurrency>0 then
begin
    JumRit:=JumRit+1;
end;

if JumRit=2 then
begin
   MasterQ.Edit;
   MasterQPremiSopir.AsCurrency:=67500;
   MasterQPremiKernet.AsCurrency:=32500;
   MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end

else if JumRit=4 then
begin
   MasterQ.Edit;
   MasterQPremiSopir.AsCurrency:=135000;
   MasterQPremiKernet.AsCurrency:=65000;
   MasterQPengeluaran.AsCurrency:=MasterQSPBULuarUangDiberi.AsCurrency+MasterQSPBULuarUang.AsCurrency+MasterQPremiSopir.AsCurrency+MasterQPremiKondektur.AsCurrency+MasterQPremiKernet.AsCurrency+MasterQUangMakan.AsCurrency+MasterQRetribusiTerminal.AsCurrency+MasterQParkir.AsCurrency+MasterQTol.AsCurrency+MasterQBiayaLainLain.AsCurrency;
   MasterQSisaDisetor.AsCurrency:=MasterQPendapatan.AsCurrency-MasterQPengeluaran.AsCurrency;

end;

end;

end.
