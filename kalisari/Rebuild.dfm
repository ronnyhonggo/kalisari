object RebuildFm: TRebuildFm
  Left = 227
  Top = 117
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Rebuild'
  ClientHeight = 657
  ClientWidth = 986
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 986
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 587
    Width = 986
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 416
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object cxGroupBox1: TcxGroupBox
      Left = 688
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 337
    Height = 539
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 183
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridBarang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = MasterVGridBarangEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Barang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaBarang'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridIDBarang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IDBarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridDariArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = MasterVGridDariArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'DariArmada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 3
      Index = 0
      Version = 1
    end
    object MasterVGridTanggalMasuk: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalMasuk'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridAnalisaMasalah: TcxDBEditorRow
      Height = 49
      Properties.DataBinding.FieldName = 'AnalisaMasalah'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridJasaLuar: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.OnEditValueChanged = MasterVGridJasaLuarEditPropertiesEditValueChanged
      Properties.DataBinding.FieldName = 'JasaLuar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 7
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridSupplier: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridSupplierEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Supplier'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 8
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridDBEditorRow4: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaSupplier'
      Properties.Options.Editing = False
      ID = 9
      ParentID = 8
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow5: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AlamatSupplier'
      Properties.Options.Editing = False
      ID = 10
      ParentID = 8
      Index = 1
      Version = 1
    end
    object MasterVGridDBEditorRow6: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpSupplier'
      Properties.Options.Editing = False
      ID = 11
      ParentID = 8
      Index = 2
      Version = 1
    end
    object MasterVGridHarga: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Harga'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 12
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridTanggalKirim: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalKirim'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 13
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridPICKirim: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPICKirimEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PICKirim'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 14
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridDBEditorRow7: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPIC'
      Properties.Options.Editing = False
      ID = 15
      ParentID = 14
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow8: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanPIC'
      Properties.Options.Editing = False
      ID = 16
      ParentID = 14
      Index = 1
      Version = 1
    end
    object MasterVGridTanggalKembali: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalKembali'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 17
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridPenerima: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPenerimaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Penerima'
      Styles.Content = MenuUtamaFm.cxStyle5
      Visible = False
      ID = 18
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridDBEditorRow9: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPenerima'
      Properties.Options.Editing = False
      ID = 19
      ParentID = 18
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow10: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanPenerima'
      Properties.Options.Editing = False
      ID = 20
      ParentID = 18
      Index = 1
      Version = 1
    end
    object MasterVGridPerbaikanInternal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PerbaikanInternal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 21
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridVerifikator: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridVerifikatorEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Verifikator'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 22
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridDBEditorRow11: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaVerifikator'
      Properties.Options.Editing = False
      ID = 23
      ParentID = 22
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow12: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JabatanVerifikator'
      Properties.Options.Editing = False
      ID = 24
      ParentID = 22
      Index = 1
      Version = 1
    end
    object MasterVGridTglVerifikasi: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglVerifikasi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 25
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridKanibal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kanibal'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 26
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridTanggalKeluar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TanggalKeluar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 27
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridKeArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridKeArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'KeArmada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 28
      ParentID = -1
      Index = 17
      Version = 1
    end
    object MasterVGridDBEditorRow3: TcxDBEditorRow
      Properties.Caption = 'NoBody'
      Properties.DataBinding.FieldName = 'NoBodyKe'
      Properties.Options.Editing = False
      ID = 29
      ParentID = 28
      Index = 0
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Items = <
        item
          Caption = 'ON PROCESS'
          Value = 'ON PROCESS'
        end
        item
          Caption = 'PERBAIKAN LUAR'
          Value = 'PERBAIKAN LUAR'
        end
        item
          Caption = 'READY'
          Value = 'READY'
        end
        item
          Caption = 'KANIBAL'
          Value = 'KANIBAL'
        end
        item
          Caption = 'KELUAR'
          Value = 'KELUAR'
        end>
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 30
      ParentID = -1
      Index = 18
      Version = 1
    end
    object MasterVGridDBEditorRow13: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PersentaseCosting'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 31
      ParentID = -1
      Index = 19
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 638
    Width = 986
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 337
    Top = 48
    Width = 649
    Height = 539
    Align = alClient
    TabOrder = 4
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 647
      Height = 329
      Align = alTop
      Caption = 'Panel2'
      TabOrder = 0
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 341
        Height = 327
        Align = alClient
        Caption = 'Panel3'
        TabOrder = 0
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 339
          Height = 160
          Align = alTop
          Caption = 'Panel4'
          TabOrder = 0
          object Label2: TLabel
            Left = 1
            Top = 1
            Width = 337
            Height = 24
            Align = alTop
            Caption = 'SPK :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid1: TcxGrid
            Left = 1
            Top = 24
            Width = 337
            Height = 135
            Align = alBottom
            TabOrder = 0
            object cxGrid1DBTableView1: TcxGridDBTableView
              DataController.DataSource = SPKDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGrid1DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'Kode'
              end
              object cxGrid1DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'Tanggal'
                Width = 67
              end
              object cxGrid1DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'Mekanik'
                Width = 68
              end
              object cxGrid1DBTableView1Column4: TcxGridDBColumn
                DataBinding.FieldName = 'DetailTindakan'
                Width = 218
              end
              object cxGrid1DBTableView1Column5: TcxGridDBColumn
                DataBinding.FieldName = 'Status'
                Width = 87
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBTableView1
            end
          end
        end
        object Panel5: TPanel
          Left = 1
          Top = 160
          Width = 339
          Height = 166
          Align = alBottom
          Caption = 'Panel5'
          TabOrder = 1
          object Label3: TLabel
            Left = 1
            Top = 1
            Width = 337
            Height = 24
            Align = alTop
            Caption = 'Bon Barang :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid3: TcxGrid
            Left = 1
            Top = 24
            Width = 337
            Height = 141
            Align = alBottom
            TabOrder = 0
            object cxGrid3DBTableView1: TcxGridDBTableView
              DataController.DataSource = BonDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Editing = False
              OptionsView.GroupByBox = False
              object cxGrid3DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'BonBarang'
              end
              object cxGrid3DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'NamaBarang'
                Options.SortByDisplayText = isbtOn
                Width = 163
              end
              object cxGrid3DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'JumlahDiminta'
                Width = 74
              end
              object cxGrid3DBTableView1Column4: TcxGridDBColumn
                DataBinding.FieldName = 'JumlahKeluar'
                Width = 69
              end
            end
            object cxGrid3Level1: TcxGridLevel
              GridView = cxGrid3DBTableView1
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 342
        Top = 1
        Width = 304
        Height = 327
        Align = alRight
        Caption = 'Panel6'
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 302
          Height = 160
          Align = alTop
          Caption = 'Panel7'
          TabOrder = 0
          object Label4: TLabel
            Left = 1
            Top = 1
            Width = 300
            Height = 24
            Align = alTop
            Caption = 'Kanibal :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid4: TcxGrid
            Left = 1
            Top = 24
            Width = 300
            Height = 135
            Align = alBottom
            TabOrder = 0
            object cxGrid4DBTableView1: TcxGridDBTableView
              DataController.DataSource = KnbalDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Editing = False
              OptionsView.GroupByBox = False
              object cxGrid4DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'Kode'
              end
              object cxGrid4DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'Nama'
              end
              object cxGrid4DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'KeBarang'
              end
            end
            object cxGrid4Level1: TcxGridLevel
              GridView = cxGrid4DBTableView1
            end
          end
        end
        object Panel8: TPanel
          Left = 1
          Top = 160
          Width = 302
          Height = 166
          Align = alBottom
          Caption = 'Panel8'
          TabOrder = 1
          object Label5: TLabel
            Left = 1
            Top = 1
            Width = 300
            Height = 24
            Align = alTop
            Caption = 'Tukar Komponen :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid5: TcxGrid
            Left = 1
            Top = 24
            Width = 300
            Height = 141
            Align = alBottom
            TabOrder = 0
            object cxGrid5DBTableView1: TcxGridDBTableView
              DataController.DataSource = TKDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGrid5DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'Kode'
                Width = 71
              end
              object cxGrid5DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'TglTukar'
                Width = 85
              end
              object cxGrid5DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'Dari Armada'
                Width = 87
              end
              object cxGrid5DBTableView1Column4: TcxGridDBColumn
                DataBinding.FieldName = 'Ke Armada'
                Width = 101
              end
              object cxGrid5DBTableView1Column5: TcxGridDBColumn
                DataBinding.FieldName = 'Barang Dari'
                Width = 97
              end
              object cxGrid5DBTableView1Column6: TcxGridDBColumn
                DataBinding.FieldName = 'Barang Ke'
                Width = 103
              end
            end
            object cxGrid5Level1: TcxGridLevel
              GridView = cxGrid5DBTableView1
            end
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 330
      Width = 647
      Height = 208
      Align = alClient
      Caption = 'Panel9'
      TabOrder = 1
      object Label1: TLabel
        Left = 1
        Top = 1
        Width = 645
        Height = 24
        Align = alTop
        Caption = 'Rebuild :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cxGrid2: TcxGrid
        Left = 1
        Top = 25
        Width = 645
        Height = 182
        Align = alClient
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid2DBTableView1DblClick
          DataController.DataSource = ViewDs
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Kode'
          end
          object cxGrid2DBTableView1Column2: TcxGridDBColumn
            DataBinding.FieldName = 'Dari Armada'
            Options.SortByDisplayText = isbtOn
            Width = 82
          end
          object cxGrid2DBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'TanggalMasuk'
            Width = 81
          end
          object cxGrid2DBTableView1Column4: TcxGridDBColumn
            DataBinding.FieldName = 'AnalisaMasalah'
            Width = 227
          end
          object cxGrid2DBTableView1Column5: TcxGridDBColumn
            DataBinding.FieldName = 'JasaLuar'
            Width = 51
          end
          object cxGrid2DBTableView1Column6: TcxGridDBColumn
            DataBinding.FieldName = 'PerbaikanInternal'
            Width = 89
          end
          object cxGrid2DBTableView1Column7: TcxGridDBColumn
            DataBinding.FieldName = 'NamaVerifikator'
            Options.SortByDisplayText = isbtOn
            Width = 81
          end
          object cxGrid2DBTableView1Column8: TcxGridDBColumn
            DataBinding.FieldName = 'TglVerifikasi'
            Width = 63
          end
          object cxGrid2DBTableView1Column10: TcxGridDBColumn
            DataBinding.FieldName = 'Status'
            Width = 131
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    AfterEdit = MasterQAfterEdit
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from rebuild')
    UpdateObject = MasterUS
    Left = 289
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBarang: TStringField
      FieldName = 'Barang'
      Visible = False
      Size = 10
    end
    object MasterQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object MasterQDariArmada: TStringField
      FieldName = 'DariArmada'
      Visible = False
      Size = 10
    end
    object MasterQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object MasterQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object MasterQKeArmada: TStringField
      FieldName = 'KeArmada'
      Visible = False
      Size = 10
    end
    object MasterQNoBodyKe: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyKe'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
    object MasterQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object MasterQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object MasterQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      Visible = False
      Size = 10
    end
    object MasterQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Size = 50
      Lookup = True
    end
    object MasterQAlamatSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'AlamatSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Alamat'
      KeyFields = 'Supplier'
      Size = 50
      Lookup = True
    end
    object MasterQTelpSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'TelpSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'kode'
      LookupResultField = 'notelp'
      KeyFields = 'supplier'
      Size = 50
      Lookup = True
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object MasterQPICKirim: TStringField
      FieldName = 'PICKirim'
      Visible = False
      Size = 10
    end
    object MasterQNamaPIC: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPIC'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PICKirim'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPIC: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPIC'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PICKirim'
      Size = 50
      Lookup = True
    end
    object MasterQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object MasterQPenerima: TStringField
      FieldName = 'Penerima'
      Visible = False
      Size = 10
    end
    object MasterQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Penerima'
      Size = 50
      Lookup = True
    end
    object MasterQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object MasterQVerifikator: TStringField
      FieldName = 'Verifikator'
      Visible = False
      Size = 10
    end
    object MasterQNamaVerifikator: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaVerifikator'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Verifikator'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanVerifikator: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanVerifikator'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'Verifikator'
      Size = 50
      Lookup = True
    end
    object MasterQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object MasterQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object MasterQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object MasterQIDBarang: TStringField
      FieldName = 'IDBarang'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 324
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Barang, DariArmada, TanggalMasuk, KeArmada, Tanggal' +
        'Keluar, AnalisaMasalah, JasaLuar, Supplier, Harga, TanggalKirim,' +
        ' PICKirim, TanggalKembali, Penerima, PerbaikanInternal, Verifika' +
        'tor, TglVerifikasi, Kanibal, PersentaseCosting, Status, CreateDa' +
        'te, TglEntry, Operator, CreateBy, IDBarang'#13#10'from rebuild'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update rebuild'
      'set'
      '  Kode = :Kode,'
      '  Barang = :Barang,'
      '  DariArmada = :DariArmada,'
      '  TanggalMasuk = :TanggalMasuk,'
      '  KeArmada = :KeArmada,'
      '  TanggalKeluar = :TanggalKeluar,'
      '  AnalisaMasalah = :AnalisaMasalah,'
      '  JasaLuar = :JasaLuar,'
      '  Supplier = :Supplier,'
      '  Harga = :Harga,'
      '  TanggalKirim = :TanggalKirim,'
      '  PICKirim = :PICKirim,'
      '  TanggalKembali = :TanggalKembali,'
      '  Penerima = :Penerima,'
      '  PerbaikanInternal = :PerbaikanInternal,'
      '  Verifikator = :Verifikator,'
      '  TglVerifikasi = :TglVerifikasi,'
      '  Kanibal = :Kanibal,'
      '  PersentaseCosting = :PersentaseCosting,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  CreateBy = :CreateBy,'
      '  IDBarang = :IDBarang'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into rebuild'
      
        '  (Kode, Barang, DariArmada, TanggalMasuk, KeArmada, TanggalKelu' +
        'ar, AnalisaMasalah, JasaLuar, Supplier, Harga, TanggalKirim, PIC' +
        'Kirim, TanggalKembali, Penerima, PerbaikanInternal, Verifikator,' +
        ' TglVerifikasi, Kanibal, PersentaseCosting, Status, CreateDate, ' +
        'TglEntry, Operator, CreateBy, IDBarang)'
      'values'
      
        '  (:Kode, :Barang, :DariArmada, :TanggalMasuk, :KeArmada, :Tangg' +
        'alKeluar, :AnalisaMasalah, :JasaLuar, :Supplier, :Harga, :Tangga' +
        'lKirim, :PICKirim, :TanggalKembali, :Penerima, :PerbaikanInterna' +
        'l, :Verifikator, :TglVerifikasi, :Kanibal, :PersentaseCosting, :' +
        'Status, :CreateDate, :TglEntry, :Operator, :CreateBy, :IDBarang)')
    DeleteSQL.Strings = (
      'delete from rebuild'
      'where'
      '  Kode = :OLD_Kode')
    Left = 364
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from rebuild order by kode desc')
    Left = 233
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 545
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
    object ArmadaQRekanan: TBooleanField
      FieldName = 'Rekanan'
    end
    object ArmadaQKodeRekanan: TStringField
      FieldName = 'KodeRekanan'
      Size = 10
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from supplier'
      '')
    Left = 601
    Top = 15
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 504
    Top = 16
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQFoto: TBlobField
      FieldName = 'Foto'
    end
    object BarangQNoPabrikan: TStringField
      FieldName = 'NoPabrikan'
      Size = 50
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai'
      '')
    Left = 649
    Top = 15
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(tanggalMasuk as date) as Tgl,* '
      'from rebuild'
      
        'where (TanggalMasuk>=:text1 and TanggalMasuk<=:text2) or tanggal' +
        'Masuk is null'
      'order by tglentry desc')
    Left = 417
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object ViewQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object ViewQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object ViewQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object ViewQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object ViewQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object ViewQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object ViewQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object ViewQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object ViewQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object ViewQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object ViewQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object ViewQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object ViewQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object ViewQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object ViewQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object ViewQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object ViewQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQDariArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object ViewQNamaVerifikator: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaVerifikator'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Verifikator'
      Size = 50
      Lookup = True
    end
    object ViewQKeArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewQ
    Left = 452
    Top = 6
  end
  object VSPKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from suratperintahkerja where rebuild = :text')
    Left = 705
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VSPKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VSPKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object VSPKQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object VSPKQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object VSPKQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object VSPKQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object VSPKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object VSPKQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object VSPKQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object VSPKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object VSPKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object VSPKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object VSPKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object VSPKQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object VSPKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object SPKDs: TDataSource
    DataSet = VSPKQ
    Left = 740
    Top = 6
  end
  object VBonQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select bb.kode as BonBarang, b.nama as NamaBarang, dbb.JumlahDim' +
        'inta, (select sum(dbkb.jumlahkeluar) from bonkeluarbarang bkb, d' +
        'etailbkb dbkb where bkb.kode=dbkb.kodebkb AND bkb.bonbarang = bb' +
        '.kode AND dbb.KodeBonBarang=bb.Kode AND dbkb.Barang=dbb.KodeBara' +
        'ng) as JumlahKeluar'
      
        'from bonbarang bb, barang b, detailbonbarang dbb, suratperintahk' +
        'erja spk'
      
        'where spk.rebuild = :text AND bb.spk = spk.kode AND dbb.kodebonb' +
        'arang = bb.kode AND b.kode = dbb.kodebarang')
    Left = 777
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VBonQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object VBonQNamaBarang: TStringField
      FieldName = 'NamaBarang'
      Required = True
      Size = 50
    end
    object VBonQJumlahDiminta: TFloatField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object VBonQJumlahKeluar: TFloatField
      FieldName = 'JumlahKeluar'
    end
  end
  object BonDs: TDataSource
    DataSet = VBonQ
    Left = 812
    Top = 6
  end
  object VKnbalQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select abk.Kode, b.Nama, abk.barang'
      
        'from ambilbarangkanibal abk, barangkanibal bk, barang b, suratpe' +
        'rintahkerja spk'
      
        'where spk.rebuild= :text AND abk.spk = spk.kode and bk.kode=abk.' +
        'daribarang and b.kode = bk.barang')
    Left = 849
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VKnbalQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VKnbalQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object VKnbalQbarang: TStringField
      FieldName = 'barang'
      Size = 10
    end
    object VKnbalQKeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KeBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'barang'
      Size = 50
      Lookup = True
    end
  end
  object KnbalDs: TDataSource
    DataSet = VKnbalQ
    Left = 884
    Top = 6
  end
  object VTKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *'
      'from tukarkomponen tk, suratperintahkerja spk'
      'where spk.rebuild= :text AND tk.spk = spk.kode')
    Left = 921
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VTKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VTKQDariArmada: TStringField
      FieldName = 'DariArmada'
      Required = True
      Size = 10
    end
    object VTKQKeArmada: TStringField
      FieldName = 'KeArmada'
      Required = True
      Size = 10
    end
    object VTKQBarangDari: TStringField
      FieldName = 'BarangDari'
      Required = True
      Size = 10
    end
    object VTKQBarangKe: TStringField
      FieldName = 'BarangKe'
      Size = 10
    end
    object VTKQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
      Required = True
    end
    object VTKQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object VTKQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object VTKQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object VTKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object VTKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object VTKQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object VTKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object VTKQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object VTKQKode_1: TStringField
      FieldName = 'Kode_1'
      Required = True
      Size = 10
    end
    object VTKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object VTKQLaporanPerbaikan_1: TStringField
      FieldName = 'LaporanPerbaikan_1'
      Size = 10
    end
    object VTKQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object VTKQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object VTKQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object VTKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object VTKQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object VTKQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object VTKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object VTKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object VTKQCreateDate_1: TDateTimeField
      FieldName = 'CreateDate_1'
    end
    object VTKQCreateBy_1: TStringField
      FieldName = 'CreateBy_1'
      Size = 10
    end
    object VTKQOperator_1: TStringField
      FieldName = 'Operator_1'
      Size = 10
    end
    object VTKQTglEntry_1: TDateTimeField
      FieldName = 'TglEntry_1'
    end
    object VTKQDariArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object VTKQKeArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
    object VTKQBarangDari2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Dari'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangDari'
      Size = 50
      Lookup = True
    end
    object VTKQBarangKe2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Ke'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangKe'
      Size = 50
      Lookup = True
    end
  end
  object TKDs: TDataSource
    DataSet = VTKQ
    Left = 956
    Top = 6
  end
end
