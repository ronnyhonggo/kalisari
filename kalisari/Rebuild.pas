unit Rebuild;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxClasses, cxMRUEdit, cxLabel,
  cxCheckBox, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxGroupBox, cxMemo;

type
  TRebuildFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    ArmadaQ: TSDQuery;
    SupplierQ: TSDQuery;
    BarangQ: TSDQuery;
    cxLabel1: TcxLabel;
    MasterQKode: TStringField;
    MasterQBarang: TStringField;
    MasterQDariArmada: TStringField;
    MasterQTanggalMasuk: TDateTimeField;
    MasterQKeArmada: TStringField;
    MasterQTanggalKeluar: TDateTimeField;
    MasterQAnalisaMasalah: TMemoField;
    MasterQJasaLuar: TBooleanField;
    MasterQSupplier: TStringField;
    MasterQHarga: TCurrencyField;
    MasterQTanggalKirim: TDateTimeField;
    MasterQPICKirim: TStringField;
    MasterQTanggalKembali: TDateTimeField;
    MasterQPenerima: TStringField;
    MasterQPerbaikanInternal: TBooleanField;
    MasterQVerifikator: TStringField;
    MasterQTglVerifikasi: TDateTimeField;
    MasterQKanibal: TBooleanField;
    MasterQPersentaseCosting: TFloatField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQCreateBy: TStringField;
    MasterVGridBarang: TcxDBEditorRow;
    MasterVGridDariArmada: TcxDBEditorRow;
    MasterVGridTanggalMasuk: TcxDBEditorRow;
    MasterVGridKeArmada: TcxDBEditorRow;
    MasterVGridTanggalKeluar: TcxDBEditorRow;
    MasterVGridAnalisaMasalah: TcxDBEditorRow;
    MasterVGridJasaLuar: TcxDBEditorRow;
    MasterVGridSupplier: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridTanggalKirim: TcxDBEditorRow;
    MasterVGridPICKirim: TcxDBEditorRow;
    MasterVGridTanggalKembali: TcxDBEditorRow;
    MasterVGridPenerima: TcxDBEditorRow;
    MasterVGridPerbaikanInternal: TcxDBEditorRow;
    MasterVGridVerifikator: TcxDBEditorRow;
    MasterVGridTglVerifikasi: TcxDBEditorRow;
    MasterVGridKanibal: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterQNamaBarang: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    MasterQNoBody: TStringField;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterQNoBodyKe: TStringField;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    MasterQNamaSupplier: TStringField;
    MasterQAlamatSupplier: TStringField;
    MasterQTelpSupplier: TStringField;
    MasterVGridDBEditorRow4: TcxDBEditorRow;
    MasterVGridDBEditorRow5: TcxDBEditorRow;
    MasterVGridDBEditorRow6: TcxDBEditorRow;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPIC: TStringField;
    MasterQJabatanPIC: TStringField;
    MasterQNamaPenerima: TStringField;
    MasterQJabatanPenerima: TStringField;
    MasterQNamaVerifikator: TStringField;
    MasterQJabatanVerifikator: TStringField;
    MasterVGridDBEditorRow7: TcxDBEditorRow;
    MasterVGridDBEditorRow8: TcxDBEditorRow;
    MasterVGridDBEditorRow9: TcxDBEditorRow;
    MasterVGridDBEditorRow10: TcxDBEditorRow;
    MasterVGridDBEditorRow11: TcxDBEditorRow;
    MasterVGridDBEditorRow12: TcxDBEditorRow;
    ViewQ: TSDQuery;
    ViewDs: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel6: TPanel;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    Panel7: TPanel;
    Panel8: TPanel;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5: TcxGrid;
    Panel9: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ViewQKode: TStringField;
    ViewQBarang: TStringField;
    ViewQDariArmada: TStringField;
    ViewQTanggalMasuk: TDateTimeField;
    ViewQKeArmada: TStringField;
    ViewQTanggalKeluar: TDateTimeField;
    ViewQAnalisaMasalah: TMemoField;
    ViewQJasaLuar: TBooleanField;
    ViewQSupplier: TStringField;
    ViewQHarga: TCurrencyField;
    ViewQTanggalKirim: TDateTimeField;
    ViewQPICKirim: TStringField;
    ViewQTanggalKembali: TDateTimeField;
    ViewQPenerima: TStringField;
    ViewQPerbaikanInternal: TBooleanField;
    ViewQVerifikator: TStringField;
    ViewQTglVerifikasi: TDateTimeField;
    ViewQKanibal: TBooleanField;
    ViewQPersentaseCosting: TFloatField;
    ViewQStatus: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQTglEntry: TDateTimeField;
    ViewQOperator: TStringField;
    ViewQCreateBy: TStringField;
    ViewQDariArmada2: TStringField;
    ViewQNamaVerifikator: TStringField;
    ViewQKeArmada2: TStringField;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    cxGrid2DBTableView1Column5: TcxGridDBColumn;
    cxGrid2DBTableView1Column6: TcxGridDBColumn;
    cxGrid2DBTableView1Column7: TcxGridDBColumn;
    cxGrid2DBTableView1Column8: TcxGridDBColumn;
    cxGrid2DBTableView1Column10: TcxGridDBColumn;
    VSPKQ: TSDQuery;
    SPKDs: TDataSource;
    VSPKQKode: TStringField;
    VSPKQTanggal: TDateTimeField;
    VSPKQLaporanPerbaikan: TStringField;
    VSPKQLaporanPerawatan: TStringField;
    VSPKQRebuild: TStringField;
    VSPKQMekanik: TStringField;
    VSPKQDetailTindakan: TMemoField;
    VSPKQWaktuMulai: TDateTimeField;
    VSPKQWaktuSelesai: TDateTimeField;
    VSPKQStatus: TStringField;
    VSPKQKeterangan: TStringField;
    VSPKQCreateDate: TDateTimeField;
    VSPKQCreateBy: TStringField;
    VSPKQOperator: TStringField;
    VSPKQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    VBonQ: TSDQuery;
    BonDs: TDataSource;
    cxGrid3DBTableView1Column1: TcxGridDBColumn;
    cxGrid3DBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBTableView1Column3: TcxGridDBColumn;
    cxGrid3DBTableView1Column4: TcxGridDBColumn;
    VKnbalQ: TSDQuery;
    KnbalDs: TDataSource;
    VKnbalQKode: TStringField;
    VKnbalQNama: TStringField;
    VKnbalQbarang: TStringField;
    VKnbalQKeBarang: TStringField;
    cxGrid4DBTableView1Column1: TcxGridDBColumn;
    cxGrid4DBTableView1Column2: TcxGridDBColumn;
    cxGrid4DBTableView1Column3: TcxGridDBColumn;
    VTKQ: TSDQuery;
    TKDs: TDataSource;
    VTKQKode: TStringField;
    VTKQDariArmada: TStringField;
    VTKQKeArmada: TStringField;
    VTKQBarangDari: TStringField;
    VTKQBarangKe: TStringField;
    VTKQTglTukar: TDateTimeField;
    VTKQPeminta: TStringField;
    VTKQSPK: TStringField;
    VTKQLaporanPerbaikan: TStringField;
    VTKQCreateDate: TDateTimeField;
    VTKQCreateBy: TStringField;
    VTKQOperator: TStringField;
    VTKQTglEntry: TDateTimeField;
    VTKQTglCetak: TDateTimeField;
    VTKQKode_1: TStringField;
    VTKQTanggal: TDateTimeField;
    VTKQLaporanPerbaikan_1: TStringField;
    VTKQLaporanPerawatan: TStringField;
    VTKQRebuild: TStringField;
    VTKQMekanik: TStringField;
    VTKQDetailTindakan: TMemoField;
    VTKQWaktuMulai: TDateTimeField;
    VTKQWaktuSelesai: TDateTimeField;
    VTKQStatus: TStringField;
    VTKQKeterangan: TStringField;
    VTKQCreateDate_1: TDateTimeField;
    VTKQCreateBy_1: TStringField;
    VTKQOperator_1: TStringField;
    VTKQTglEntry_1: TDateTimeField;
    VTKQDariArmada2: TStringField;
    VTKQKeArmada2: TStringField;
    VTKQBarangDari2: TStringField;
    VTKQBarangKe2: TStringField;
    cxGrid5DBTableView1Column1: TcxGridDBColumn;
    cxGrid5DBTableView1Column2: TcxGridDBColumn;
    cxGrid5DBTableView1Column3: TcxGridDBColumn;
    cxGrid5DBTableView1Column4: TcxGridDBColumn;
    cxGrid5DBTableView1Column5: TcxGridDBColumn;
    cxGrid5DBTableView1Column6: TcxGridDBColumn;
    VBonQBonBarang: TStringField;
    VBonQNamaBarang: TStringField;
    MasterVGridDBEditorRow13: TcxDBEditorRow;
    MasterVGridIDBarang: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    ArmadaQLayoutBan: TStringField;
    ArmadaQRekanan: TBooleanField;
    ArmadaQKodeRekanan: TStringField;
    VBonQJumlahDiminta: TFloatField;
    VBonQJumlahKeluar: TFloatField;
    MasterQIDBarang: TStringField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQFoto: TBlobField;
    BarangQNoPabrikan: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridDariArmadaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKeArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSupplierEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPICKirimEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPenerimaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridVerifikatorEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterQAfterEdit(DataSet: TDataSet);
    procedure MasterVGridJasaLuarEditPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  //  procedure cxGrid2DBTableView1Customization(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  RebuildFm: TRebuildFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, Math, ArmadaDropDown, BarangDropDown,
  PegawaiDropDown, SupplierDropDown, BarangBekasDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TRebuildFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TRebuildFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TRebuildFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TRebuildFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TRebuildFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  BarangQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  SupplierQ.Open;
end;

procedure TRebuildFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  MasterVGrid.Enabled:=True;
  VSPKQ.Close;
  VBonQ.Close;
  VKnbalQ.Close;
  VTKQ.Close;
end;

procedure TRebuildFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TRebuildFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertRebuild.AsBoolean;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateRebuild.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteRebuild.AsBoolean;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TRebuildFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    Masterq.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQJasaLuar.AsBoolean:=false;
      MasterQPerbaikanInternal.AsBoolean:=false;
      MasterQKanibal.AsBoolean:=false;
      MasterQStatus.Value:='ON PROCESS';
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteRebuild.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateRebuild.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;

end;

procedure TRebuildFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TRebuildFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Rebuild dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  viewq.Refresh;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TRebuildFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;}
end;

procedure TRebuildFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TRebuildFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Rebuild '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Rebuild telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Rebuild pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     viewQ.Close;
     viewQ.Open;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TRebuildFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TRebuildFm.MasterVGridEnter(Sender: TObject);
begin
 // mastervgrid.FocusRow(MasterVGridMerk);
end;

procedure TRebuildFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarang.AsString:= BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridDariArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQDariArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridKeArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQKeArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridSupplierEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SupplierDropDownFm:=TSupplierDropdownfm.Create(Self);
  if SupplierDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSupplier.AsString:=SupplierDropDownFm.kode;
    end;
  SupplierDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridPICKirimEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPICKirim.AsString:=PegawaiDropDownFm.kode;
    end;
  PegawaiDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridPenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPenerima.AsString:=PegawaiDropDownFm.kode;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TRebuildFm.MasterVGridVerifikatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQVerifikator.AsString:=PegawaiDropDownFm.kode;
    end;
    PegawaiDropDownFm.Release;
end;

procedure TRebuildFm.MasterQAfterEdit(DataSet: TDataSet);
begin
  If MasterQJasaLuar.AsBoolean=true then
  begin
    MasterVGridSupplier.Visible:=true;
    MasterVGridHarga.Visible:=true;
    MasterVGridTanggalKirim.Visible:=true;
    MasterVGridTanggalKembali.Visible:=true;
    MasterVGridPenerima.Visible:=true;
  end
  else
  begin
    MasterVGridSupplier.Visible:=false;
    MasterVGridHarga.Visible:=false;
    MasterVGridTanggalKirim.Visible:=false;
    MasterVGridTanggalKembali.Visible:=false;
    MasterVGridPenerima.Visible:=false;
  end;
end;

procedure TRebuildFm.MasterVGridJasaLuarEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  If MasterQJasaLuar.AsBoolean=true then
  begin
    MasterVGridSupplier.Visible:=true;
    MasterVGridHarga.Visible:=true;
    MasterVGridTanggalKirim.Visible:=true;
    MasterVGridTanggalKembali.Visible:=true;
    MasterVGridPenerima.Visible:=true;
  end
  else
  begin
    MasterQSupplier.AsVariant:=null;
    MasterQHarga.AsVariant:=null;
    MasterQTanggalKirim.AsVariant:=null;
    MasterQTanggalKembali.AsVariant:=null;
    MasterQPenerima.AsVariant:=null;
    MasterVGridSupplier.Visible:=false;
    MasterVGridHarga.Visible:=false;
    MasterVGridTanggalKirim.Visible:=false;
    MasterVGridTanggalKembali.Visible:=false;
    MasterVGridPenerima.Visible:=false;
  end;
end;

procedure TRebuildFm.cxGrid2DBTableView1DblClick(Sender: TObject);
begin
    //buttonCetak.Enabled:=true;
    kodeedit.Text:=ViewQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdateRebuild.AsBoolean;
    MasterVGrid.Enabled:=True;

    //cxDBVerticalGrid1.Enabled:=True;
    VSPKQ.Close;
    VSPKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VSPKQ.Open;
    VBonQ.Close;
    VBonQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VBonQ.Open;
    VKnbalQ.Close;
    VKnbalQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VKnbalQ.Open;
    VTKQ.Close;
    VTKQ.ParamByName('text').AsString:= ViewQKode.AsString;
    VTKQ.Open;
end;



procedure TRebuildFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewQ.Open;
end;

procedure TRebuildFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewQ.Open;
end;

end.
