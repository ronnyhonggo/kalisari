/*
   Tuesday, April 30, 201311:43:25 AM
   User: sa
   Server: TOMY-PC
   Database: Kalisari
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_User
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_User1
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[User]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[User]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Pegawai
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Pegawai1
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Pegawai2
GO
ALTER TABLE dbo.Pegawai SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Pegawai', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Supplier
GO
ALTER TABLE dbo.Supplier SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Supplier', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Supplier', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Supplier', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Barang
GO
ALTER TABLE dbo.Barang SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Barang', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Barang', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Barang', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Armada
GO
ALTER TABLE dbo.Rebuild
	DROP CONSTRAINT FK_Rebuild_Armada1
GO
ALTER TABLE dbo.Armada SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Armada', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Armada', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Armada', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Rebuild
	(
	Kode varchar(10) NOT NULL,
	Barang varchar(10) NULL,
	IDBarang varchar(50) NULL,
	DariArmada varchar(10) NULL,
	TanggalMasuk datetime NULL,
	KeArmada varchar(10) NULL,
	TanggalKeluar datetime NULL,
	AnalisaMasalah varchar(MAX) NULL,
	JasaLuar bit NULL,
	Supplier varchar(10) NULL,
	Harga money NULL,
	TanggalKirim datetime NULL,
	PICKirim varchar(10) NULL,
	TanggalKembali datetime NULL,
	Penerima varchar(10) NULL,
	PerbaikanInternal bit NULL,
	Verifikator varchar(10) NULL,
	TglVerifikasi datetime NULL,
	Kanibal bit NULL,
	PersentaseCosting numeric(18, 4) NULL,
	Status varchar(50) NULL,
	CreateDate datetime NULL,
	TglEntry datetime NULL,
	Operator varchar(10) NULL,
	CreateBy varchar(10) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Rebuild SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Rebuild)
	 EXEC('INSERT INTO dbo.Tmp_Rebuild (Kode, Barang, DariArmada, TanggalMasuk, KeArmada, TanggalKeluar, AnalisaMasalah, JasaLuar, Supplier, Harga, TanggalKirim, PICKirim, TanggalKembali, Penerima, PerbaikanInternal, Verifikator, TglVerifikasi, Kanibal, PersentaseCosting, Status, CreateDate, TglEntry, Operator, CreateBy)
		SELECT Kode, Barang, DariArmada, TanggalMasuk, KeArmada, TanggalKeluar, AnalisaMasalah, JasaLuar, Supplier, Harga, TanggalKirim, PICKirim, TanggalKembali, Penerima, PerbaikanInternal, Verifikator, TglVerifikasi, Kanibal, PersentaseCosting, Status, CreateDate, TglEntry, Operator, CreateBy FROM dbo.Rebuild WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.SuratPerintahKerja
	DROP CONSTRAINT FK_SuratPerintahKerja_Rebuild
GO
DROP TABLE dbo.Rebuild
GO
EXECUTE sp_rename N'dbo.Tmp_Rebuild', N'Rebuild', 'OBJECT' 
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	PK_Rebuild PRIMARY KEY CLUSTERED 
	(
	Kode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Armada FOREIGN KEY
	(
	DariArmada
	) REFERENCES dbo.Armada
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Armada1 FOREIGN KEY
	(
	KeArmada
	) REFERENCES dbo.Armada
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Barang FOREIGN KEY
	(
	Barang
	) REFERENCES dbo.Barang
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Supplier FOREIGN KEY
	(
	Supplier
	) REFERENCES dbo.Supplier
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Pegawai FOREIGN KEY
	(
	PICKirim
	) REFERENCES dbo.Pegawai
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Pegawai1 FOREIGN KEY
	(
	Penerima
	) REFERENCES dbo.Pegawai
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_Pegawai2 FOREIGN KEY
	(
	Verifikator
	) REFERENCES dbo.Pegawai
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_User FOREIGN KEY
	(
	CreateBy
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Rebuild ADD CONSTRAINT
	FK_Rebuild_User1 FOREIGN KEY
	(
	Operator
	) REFERENCES dbo.[User]
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
CREATE TRIGGER [dbo].[REBUILD_INSERT] ON dbo.Rebuild
for INSERT
AS
BEGIN
	update Rebuild set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
CREATE TRIGGER [dbo].[REBUILD_UPDATE] ON dbo.Rebuild
for UPDATE
AS
BEGIN
	update Rebuild set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Rebuild', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Rebuild', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Rebuild', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SuratPerintahKerja ADD CONSTRAINT
	FK_SuratPerintahKerja_Rebuild FOREIGN KEY
	(
	Rebuild
	) REFERENCES dbo.Rebuild
	(
	Kode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.SuratPerintahKerja SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SuratPerintahKerja', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SuratPerintahKerja', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SuratPerintahKerja', 'Object', 'CONTROL') as Contr_Per 