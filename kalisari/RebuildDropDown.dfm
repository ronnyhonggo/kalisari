object RebuildDropDownFm: TRebuildDropDownFm
  Left = 328
  Top = 38
  Width = 999
  Height = 498
  Caption = 'RebuildDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 983
    Height = 460
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 96
      end
      object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 149
      end
      object cxGrid1DBTableView1DariPlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'DariPlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 86
      end
      object cxGrid1DBTableView1DariNoBody: TcxGridDBColumn
        DataBinding.FieldName = 'DariNoBody'
        Options.SortByDisplayText = isbtOn
        Width = 93
      end
      object cxGrid1DBTableView1TanggalMasuk: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalMasuk'
        Width = 119
      end
      object cxGrid1DBTableView1KePlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'KePlatNo'
        Options.SortByDisplayText = isbtOn
        Width = 99
      end
      object cxGrid1DBTableView1KeNoBody: TcxGridDBColumn
        DataBinding.FieldName = 'KeNoBody'
        Options.SortByDisplayText = isbtOn
        Width = 87
      end
      object cxGrid1DBTableView1TanggalKeluar: TcxGridDBColumn
        DataBinding.FieldName = 'TanggalKeluar'
      end
      object cxGrid1DBTableView1AnalisaMasalah: TcxGridDBColumn
        DataBinding.FieldName = 'AnalisaMasalah'
        Width = 323
      end
      object cxGrid1DBTableView1JasaLuar: TcxGridDBColumn
        DataBinding.FieldName = 'JasaLuar'
        Width = 72
      end
      object cxGrid1DBTableView1NamaSupplier: TcxGridDBColumn
        DataBinding.FieldName = 'NamaSupplier'
      end
      object cxGrid1DBTableView1Harga: TcxGridDBColumn
        DataBinding.FieldName = 'Harga'
        Width = 87
      end
      object cxGrid1DBTableView1PerbaikanInternal: TcxGridDBColumn
        DataBinding.FieldName = 'PerbaikanInternal'
        Width = 88
      end
      object cxGrid1DBTableView1Kanibal: TcxGridDBColumn
        DataBinding.FieldName = 'Kanibal'
        Width = 58
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 114
      end
      object cxGrid1DBTableView1PersentaseCosting: TcxGridDBColumn
        DataBinding.FieldName = 'PersentaseCosting'
        Width = 93
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object RBQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from rebuild where status<>"READY"'
      'order by tglentry desc')
    Left = 216
    Top = 368
    object RBQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RBQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object RBQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 50
      Lookup = True
    end
    object RBQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object RBQDariPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'DariPlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'DariArmada'
      Lookup = True
    end
    object RBQDariNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'DariNoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Lookup = True
    end
    object RBQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object RBQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object RBQKePlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'KePlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KeArmada'
      Lookup = True
    end
    object RBQKeNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'KeNoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Lookup = True
    end
    object RBQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object RBQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object RBQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object RBQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object RBQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'Supplier'
      Lookup = True
    end
    object RBQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object RBQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object RBQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object RBQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object RBQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object RBQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object RBQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object RBQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object RBQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object RBQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object RBQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object RBQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RBQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RBQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RBQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
  object LPBDs: TDataSource
    DataSet = RBQ
    Left = 280
    Top = 368
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 248
    Top = 368
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQLayoutBan: TStringField
      FieldName = 'LayoutBan'
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 312
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from supplier')
    Left = 344
    Top = 368
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 376
    Top = 368
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQFoto: TBlobField
      FieldName = 'Foto'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
end
