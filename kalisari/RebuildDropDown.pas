unit RebuildDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TRebuildDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    RBQ: TSDQuery;
    LPBDs: TDataSource;
    ArmadaQ: TSDQuery;
    PegawaiQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    SupplierQ: TSDQuery;
    BarangQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    BarangQMaximumStok: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    RBQKode: TStringField;
    RBQBarang: TStringField;
    RBQDariArmada: TStringField;
    RBQTanggalMasuk: TDateTimeField;
    RBQKeArmada: TStringField;
    RBQTanggalKeluar: TDateTimeField;
    RBQAnalisaMasalah: TMemoField;
    RBQJasaLuar: TBooleanField;
    RBQSupplier: TStringField;
    RBQHarga: TCurrencyField;
    RBQTanggalKirim: TDateTimeField;
    RBQPICKirim: TStringField;
    RBQTanggalKembali: TDateTimeField;
    RBQPenerima: TStringField;
    RBQPerbaikanInternal: TBooleanField;
    RBQVerifikator: TStringField;
    RBQTglVerifikasi: TDateTimeField;
    RBQKanibal: TBooleanField;
    RBQPersentaseCosting: TFloatField;
    RBQStatus: TStringField;
    RBQCreateDate: TDateTimeField;
    RBQTglEntry: TDateTimeField;
    RBQOperator: TStringField;
    RBQCreateBy: TStringField;
    RBQNamaBarang: TStringField;
    RBQDariPlatNo: TStringField;
    RBQDariNoBody: TStringField;
    RBQKePlatNo: TStringField;
    RBQKeNoBody: TStringField;
    RBQNamaSupplier: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1DariPlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1DariNoBody: TcxGridDBColumn;
    cxGrid1DBTableView1TanggalMasuk: TcxGridDBColumn;
    cxGrid1DBTableView1KePlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1KeNoBody: TcxGridDBColumn;
    cxGrid1DBTableView1TanggalKeluar: TcxGridDBColumn;
    cxGrid1DBTableView1AnalisaMasalah: TcxGridDBColumn;
    cxGrid1DBTableView1JasaLuar: TcxGridDBColumn;
    cxGrid1DBTableView1NamaSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1PerbaikanInternal: TcxGridDBColumn;
    cxGrid1DBTableView1Kanibal: TcxGridDBColumn;
    cxGrid1DBTableView1PersentaseCosting: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    BarangQFoto: TBlobField;
    ArmadaQLayoutBan: TStringField;
    BarangQJumlah: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  RebuildDropDownFm: TRebuildDropDownFm;

implementation

{$R *.dfm}

procedure TRebuildDropDownFm.FormCreate(Sender: TObject);
begin
  BarangQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  SupplierQ.Open;
  RBQ.Open;
end;

procedure TRebuildDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=RBQKode.AsString;
  ModalResult:=mrOK;
end;

end.
