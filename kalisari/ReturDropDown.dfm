�
 TRETURDROPDOWNFM 0�  TPF0TReturDropDownFmReturDropDownFmLeftzTop9Width�HeightLCaptionReturDropDownColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TPanelpnl1Left Top�Width}Height)AlignalBottomTabOrder   TcxGridcxGrid1Left Top Width}Height�AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TcxGridDBTableViewcxGrid1DBTableView1Navigator.Buttons.First.Visible#Navigator.Buttons.PriorPage.VisibleNavigator.Buttons.Prior.VisibleNavigator.Buttons.Next.Visible"Navigator.Buttons.NextPage.VisibleNavigator.Buttons.Last.Visible Navigator.Buttons.Append.VisibleNavigator.Buttons.Edit.Visible	Navigator.Buttons.Post.Visible	&Navigator.Buttons.SaveBookmark.Visible&Navigator.Buttons.GotoBookmark.Visible Navigator.Buttons.Filter.VisibleNavigator.Visible	OnCellDblClickcxGrid1DBTableView1CellDblClickDataController.DataSourceDataSource1/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups OptionsBehavior.FocusCellOnTab	OptionsBehavior.IncSearch	OptionsData.EditingOptionsView.GroupByBox TcxGridDBColumncxGrid1DBTableView1KodeDataBinding.FieldNameKodeWidth|  TcxGridDBColumncxGrid1DBTableView1ReturDataBinding.FieldNameReturWidthF  TcxGridDBColumncxGrid1DBTableView1ComplaintDataBinding.FieldName	ComplaintWidthM  TcxGridDBColumncxGrid1DBTableView1TglReturDataBinding.FieldNameTglReturWidth�   TcxGridDBColumn cxGrid1DBTableView1NamaPelaksanaDataBinding.FieldNameNamaPelaksana  TcxGridDBColumncxGrid1DBTableView1KeteranganDataBinding.FieldName
KeteranganWidthL  TcxGridDBColumncxGrid1DBTableView1KodePODataBinding.FieldNameKodePOWidthk  TcxGridDBColumncxGrid1DBTableView1TglKirimPODataBinding.FieldName
TglKirimPOWidth}  TcxGridDBColumn!cxGrid1DBTableView1NamaSupplierPODataBinding.FieldNameNamaSupplierPO  TcxGridDBColumncxGrid1DBTableView1StatusDataBinding.FieldNameStatusWidth�    TcxGridLevelcxGrid1Level1GridViewcxGrid1DBTableView1   TDataSourceDataSource1DataSetReturQLeftHTop\  TSDQueryReturQDatabaseNameDataOptions SQL.Strings,select * from Retur where status<>"FINISHED"order by tglentry desc LeftTopP TStringField
ReturQKode	FieldNameKodeRequired	Size
  TDateTimeFieldReturQTglRetur	FieldNameTglReturRequired	  TStringFieldReturQPelaksana	FieldName	PelaksanaRequired	Size
  
TMemoFieldReturQKeterangan	FieldName
KeteranganBlobTypeftMemo  TDateTimeFieldReturQCreateDate	FieldName
CreateDate  TStringFieldReturQCreateBy	FieldNameCreateBySize
  TStringFieldReturQOperator	FieldNameOperatorSize
  TDateTimeFieldReturQTglEntry	FieldNameTglEntry  TDateTimeFieldReturQTglCetak	FieldNameTglCetak  TStringFieldReturQKodePO	FieldNameKodePOSize
  TStringFieldReturQStatus	FieldNameStatusSize
  TBooleanFieldReturQRetur	FieldNameRetur  TBooleanFieldReturQComplaint	FieldName	Complaint  TStringFieldReturQNamaPelaksana	FieldKindfkLookup	FieldNameNamaPelaksanaLookupDataSetPegawaiQLookupKeyFieldsKodeLookupResultFieldNama	KeyFields	PelaksanaSize2Lookup	  TStringFieldReturQKodeSupplier	FieldKindfkLookup	FieldNameKodeSupplierLookupDataSetPOQLookupKeyFieldsKodeLookupResultFieldSupplier	KeyFieldsKodePOSize
Lookup	  TStringFieldReturQNamaSupplierPO	FieldKindfkLookup	FieldNameNamaSupplierPOLookupDataSet	SupplierQLookupKeyFieldsKodeLookupResultFieldNamaToko	KeyFieldsKodeSupplierSize2Lookup	  TDateTimeFieldReturQTglKirimPO	FieldKindfkLookup	FieldName
TglKirimPOLookupDataSetPOQLookupKeyFieldsKodeLookupResultFieldTglKirim	KeyFieldsKodePOLookup	   TSDQueryPegawaiQDatabaseNameDataOptions SQL.Stringsselect * from Pegawai Left(Topx TStringFieldPegawaiQKode	FieldNameKodeRequired	Size
  TStringFieldPegawaiQNama	FieldNameNamaSize2  TStringFieldPegawaiQAlamat	FieldNameAlamatSize2  TStringFieldPegawaiQKota	FieldNameKotaSize2  TStringFieldPegawaiQNoTelp	FieldNameNoTelpSize2  TStringFieldPegawaiQNoHP	FieldNameNoHPSize2  TDateTimeFieldPegawaiQTglLahir	FieldNameTglLahir  TCurrencyFieldPegawaiQGaji	FieldNameGaji  TStringFieldPegawaiQJabatan	FieldNameJabatanSize2  TDateTimeFieldPegawaiQMulaiBekerja	FieldNameMulaiBekerja  TStringFieldPegawaiQNomorSIM	FieldNameNomorSIMSize2  TDateTimeFieldPegawaiQExpiredSIM	FieldName
ExpiredSIM  TBooleanFieldPegawaiQAktif	FieldNameAktif  
TMemoFieldPegawaiQKeterangan	FieldName
KeteranganBlobTypeftMemo  TStringFieldPegawaiQNoKTP	FieldNameNoKTPSize2   TSDQuery	SupplierQDatabaseNameDataOptions SQL.Stringsselect * from Supplier LeftpTopx TStringFieldSupplierQKode	FieldNameKodeRequired	Size
  TStringFieldSupplierQNamaToko	FieldNameNamaTokoRequired	Size2  TStringFieldSupplierQAlamat	FieldNameAlamatRequired	Size�   TStringFieldSupplierQNoTelp	FieldNameNoTelpRequired	Size2  TStringFieldSupplierQFax	FieldNameFaxSize2  TStringFieldSupplierQEmail	FieldNameEmailSize2  TStringFieldSupplierQKategori	FieldNameKategoriSize2  TStringFieldSupplierQStandarTermOfPayment	FieldNameStandarTermOfPaymentSize2  TBooleanFieldSupplierQCashNCarry	FieldName
CashNCarry  TStringFieldSupplierQNPWP	FieldNameNPWPSize2  TStringFieldSupplierQNamaPIC1	FieldNameNamaPIC1Required	Size2  TStringFieldSupplierQTelpPIC1	FieldNameTelpPIC1Required	Size  TStringFieldSupplierQJabatanPIC1	FieldNameJabatanPIC1Required	  TStringFieldSupplierQNamaPIC2	FieldNameNamaPIC2Size2  TStringFieldSupplierQTelpPIC2	FieldNameTelpPIC2Size  TStringFieldSupplierQJabatanPIC2	FieldNameJabatanPIC2  TStringFieldSupplierQNamaPIC3	FieldNameNamaPIC3Size2  TStringFieldSupplierQTelpPIC3	FieldNameTelpPIC3Size  TStringFieldSupplierQJabatanPIC3	FieldNameJabatanPIC3  TDateTimeFieldSupplierQCreateDate	FieldName
CreateDate  TStringFieldSupplierQCreateBy	FieldNameCreateBySize
  TStringFieldSupplierQOperator	FieldNameOperatorSize
  TDateTimeFieldSupplierQTglEntry	FieldNameTglEntry   TSDQueryPOQDatabaseNameDataOptions SQL.Stringsselect * from PO LeftPTop� TStringFieldPOQKode	FieldNameKodeRequired	Size
  TStringFieldPOQSupplier	FieldNameSupplierRequired	Size
  TStringField	POQTermin	FieldNameTerminRequired	Size2  TDateTimeFieldPOQTglKirim	FieldNameTglKirimRequired	  TCurrencyFieldPOQGrandTotal	FieldName
GrandTotalRequired	  TStringField	POQStatus	FieldNameStatusRequired	Size2  TDateTimeFieldPOQCreateDate	FieldName
CreateDate  TStringFieldPOQCreateBy	FieldNameCreateBySize
  TStringFieldPOQOperator	FieldNameOperatorSize
  TDateTimeFieldPOQTglEntry	FieldNameTglEntry  TDateTimeFieldPOQTglCetak	FieldNameTglCetak  TStringFieldPOQStatusKirim	FieldNameStatusKirimSize2  TCurrencyField	POQDiskon	FieldNameDiskon  TBooleanFieldPOQKirim	FieldNameKirim  TBooleanFieldPOQAmbil	FieldNameAmbil    