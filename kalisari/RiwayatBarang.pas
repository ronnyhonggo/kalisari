unit RiwayatBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinsDefaultPainters, DB, SDEngine, UCrpeClasses, UCrpe32,
  StdCtrls, cxButtons, ComCtrls, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TRiwayatBarangFm = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblstatus: TLabel;
    RadioButtonBln: TRadioButton;
    RadioButtonTgl: TRadioButton;
    DateTimePickerDay: TDateTimePicker;
    DateTimePickerMonth: TDateTimePicker;
    cxButton1: TcxButton;
    Crpe1: TCrpe;
    masterq: TSDQuery;
    masterds: TDataSource;
    DateTimePickerMonth2: TDateTimePicker;
    LblTgl2: TLabel;
    LblBln2: TLabel;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RiwayatBarangFm: TRiwayatBarangFm;

implementation
uses DateUtils, MenuUtama, StrUtils,riwayat;
{$R *.dfm}

procedure TRiwayatBarangFm.cxButton1Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
  Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Riwayat.rpt';
  Crpe1.ParamFields[0].CurrentValue:=RiwayatFm.kodebar;
  Crpe1.ParamFields[4].CurrentValue:=RiwayatFm.namaBarang;
  Crpe1.ParamFields[5].CurrentValue:=inttostr(RiwayatFm.stokSekarang);
  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[3].CurrentValue:='date';
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', Now);
    end
    else
    begin
      Crpe1.ParamFields[3].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

  Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
  //ShowMessage(Crpe1.Connect.ServerName);

  Crpe1.Execute;
  Crpe1.WindowState:= wsMaximized;
  self.Visible:=false;
end;

procedure TRiwayatBarangFm.FormShow(Sender: TObject);
begin
  DateTimePickerDay.DateTime:=Today;

end;

procedure TRiwayatBarangFm.FormCreate(Sender: TObject);
begin
DateTimePickerDay.Format:='';
DateTimePickerDay.Date:=now;

LblTgl2.Caption:=DateToStr(now);
LblBln2.Caption:=FormatDateTime('mmmm yyyy' ,Now);

  DateTimePickerMonth.Date:=now;
  DateTimePickerMonth2.Date:=now;
end;

procedure TRiwayatBarangFm.cxButton2Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
  Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'RiwayatHarga.rpt';
  Crpe1.ParamFields[0].CurrentValue:=RiwayatFm.kodebar;
  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[3].CurrentValue:='date';
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', Now);
    end
    else
    begin
      Crpe1.ParamFields[3].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

  Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
  //ShowMessage(Crpe1.Connect.ServerName);

  Crpe1.Execute;
  Crpe1.WindowState:= wsMaximized;
  self.Visible:=false;
end;

end.
