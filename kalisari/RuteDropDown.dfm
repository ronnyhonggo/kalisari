object RuteDropDownFm: TRuteDropDownFm
  Left = 101
  Top = 143
  Width = 1165
  Height = 588
  Caption = 'Rute Drop Down'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 20
  object pnl1: TPanel
    Left = 0
    Top = 508
    Width = 1149
    Height = 41
    Align = alBottom
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1149
    Height = 508
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 112
      end
      object cxGrid1DBTableView1Muat: TcxGridDBColumn
        Caption = 'Asal'
        DataBinding.FieldName = 'Muat'
        Width = 138
      end
      object cxGrid1DBTableView1Bongkar: TcxGridDBColumn
        Caption = 'Tujuan'
        DataBinding.FieldName = 'Bongkar'
        Width = 217
      end
      object cxGrid1DBTableView1Jarak: TcxGridDBColumn
        DataBinding.FieldName = 'Jarak'
      end
      object cxGrid1DBTableView1Kategori: TcxGridDBColumn
        DataBinding.FieldName = 'Kategori'
        Width = 173
      end
      object cxGrid1DBTableView1LevelRute: TcxGridDBColumn
        DataBinding.FieldName = 'LevelRute'
        Width = 89
      end
      object cxGrid1DBTableView1Poin: TcxGridDBColumn
        DataBinding.FieldName = 'Poin'
      end
      object cxGrid1DBTableView1PremiPengemudi: TcxGridDBColumn
        DataBinding.FieldName = 'PremiPengemudi'
        Width = 126
      end
      object cxGrid1DBTableView1PremiKernet: TcxGridDBColumn
        DataBinding.FieldName = 'PremiKernet'
        Width = 110
      end
      object cxGrid1DBTableView1PremiKondektur: TcxGridDBColumn
        DataBinding.FieldName = 'PremiKondektur'
        Width = 136
      end
      object cxGrid1DBTableView1Mel: TcxGridDBColumn
        DataBinding.FieldName = 'Mel'
      end
      object cxGrid1DBTableView1Tol: TcxGridDBColumn
        DataBinding.FieldName = 'Tol'
      end
      object cxGrid1DBTableView1UangJalanBesar: TcxGridDBColumn
        DataBinding.FieldName = 'UangJalanBesar'
      end
      object cxGrid1DBTableView1UangJalanKecil: TcxGridDBColumn
        DataBinding.FieldName = 'UangJalanKecil'
      end
      object cxGrid1DBTableView1UangBBM: TcxGridDBColumn
        DataBinding.FieldName = 'UangBBM'
      end
      object cxGrid1DBTableView1UangMakan: TcxGridDBColumn
        DataBinding.FieldName = 'UangMakan'
      end
      object cxGrid1DBTableView1Waktu: TcxGridDBColumn
        DataBinding.FieldName = 'Waktu'
      end
      object cxGrid1DBTableView1StandarHargaMax: TcxGridDBColumn
        DataBinding.FieldName = 'StandarHargaMax'
      end
      object cxGrid1DBTableView1StandarHarga: TcxGridDBColumn
        DataBinding.FieldName = 'StandarHarga'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DataSource1: TDataSource
    DataSet = RuteQ
    Left = 328
    Top = 348
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 216
    Top = 376
    object cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView
      OnKeyDown = cxGridViewRepository1DBBandedTableView1KeyDown
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCellDblClick = cxGridViewRepository1DBBandedTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded, dcoImmediatePost, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.Indicator = True
      OptionsView.BandCaptionsInColumnAlternateCaption = True
      OptionsView.BandHeaderEndEllipsis = True
      Bands = <
        item
        end>
      object cxGridViewRepository1DBBandedTableView1Kode: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kode'
        Width = 78
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1PlatNo: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PlatNo'
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoBody: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoBody'
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoRangka: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoRangka'
        Width = 145
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1NoMesin: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NoMesin'
        Width = 107
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JenisKendaraan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JenisKendaraan'
        Width = 90
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JumlahSeat: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JumlahSeat'
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JenisBBM: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JenisBBM'
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1TahunPembuatan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'TahunPembuatan'
        Width = 40
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JenisAC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JenisAC'
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Toilet: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Toilet'
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1AirSuspension: TcxGridDBBandedColumn
        DataBinding.FieldName = 'AirSuspension'
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Sopir: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Sopir'
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KapasitasTangkiBBM: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KapasitasTangkiBBM'
        Width = 112
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1STNKPajakExpired: TcxGridDBBandedColumn
        DataBinding.FieldName = 'STNKPajakExpired'
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KirSelesai: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KirSelesai'
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1LevelArmada: TcxGridDBBandedColumn
        DataBinding.FieldName = 'LevelArmada'
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1JumlahBan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'JumlahBan'
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Keterangan: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Keterangan'
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Aktif: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Aktif'
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1AC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'AC'
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KmSekarang: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KmSekarang'
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1STNKPerpanjangExpired: TcxGridDBBandedColumn
        DataBinding.FieldName = 'STNKPerpanjangExpired'
        Position.BandIndex = 0
        Position.ColIndex = 22
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1KirMulai: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KirMulai'
        Position.BandIndex = 0
        Position.ColIndex = 23
        Position.RowIndex = 0
      end
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from rute')
    Left = 280
    Top = 336
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
end
