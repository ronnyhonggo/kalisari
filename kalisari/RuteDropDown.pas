unit RuteDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TRuteDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1: TcxGrid;
    RuteQ: TSDQuery;
    cxGridViewRepository1DBBandedTableView1Kode: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1PlatNo: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1NoBody: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1NoRangka: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1NoMesin: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1JenisKendaraan: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1JumlahSeat: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1JenisBBM: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1TahunPembuatan: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1JenisAC: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Toilet: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1AirSuspension: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Sopir: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1KapasitasTangkiBBM: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1STNKPajakExpired: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1KirSelesai: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1LevelArmada: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1JumlahBan: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Keterangan: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Aktif: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1AC: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1KmSekarang: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1STNKPerpanjangExpired: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1KirMulai: TcxGridDBBandedColumn;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Muat: TcxGridDBColumn;
    cxGrid1DBTableView1Bongkar: TcxGridDBColumn;
    cxGrid1DBTableView1Jarak: TcxGridDBColumn;
    cxGrid1DBTableView1Kategori: TcxGridDBColumn;
    cxGrid1DBTableView1LevelRute: TcxGridDBColumn;
    cxGrid1DBTableView1Poin: TcxGridDBColumn;
    cxGrid1DBTableView1PremiPengemudi: TcxGridDBColumn;
    cxGrid1DBTableView1PremiKernet: TcxGridDBColumn;
    cxGrid1DBTableView1PremiKondektur: TcxGridDBColumn;
    cxGrid1DBTableView1Mel: TcxGridDBColumn;
    cxGrid1DBTableView1Tol: TcxGridDBColumn;
    cxGrid1DBTableView1UangJalanBesar: TcxGridDBColumn;
    cxGrid1DBTableView1UangJalanKecil: TcxGridDBColumn;
    cxGrid1DBTableView1UangBBM: TcxGridDBColumn;
    cxGrid1DBTableView1UangMakan: TcxGridDBColumn;
    cxGrid1DBTableView1Waktu: TcxGridDBColumn;
    cxGrid1DBTableView1StandarHargaMax: TcxGridDBColumn;
    cxGrid1DBTableView1StandarHarga: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  RuteDropDownFm: TRuteDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TRuteDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TRuteDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=RuteQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TRuteDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=RuteQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TRuteDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=RuteQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TRuteDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=RuteQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

end.
