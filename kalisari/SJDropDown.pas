unit SJDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TSJDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    SJQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    SOQ: TSDQuery;
    PelangganQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    SJQKodenota: TStringField;
    SJQTgl: TDateTimeField;
    SJQNoSO: TStringField;
    SJQSopir: TStringField;
    SJQSopir2: TStringField;
    SJQCrew: TStringField;
    SJQTitipKwitansi: TBooleanField;
    SJQNominalKwitansi: TCurrencyField;
    SJQNoKwitansi: TStringField;
    SJQKeterangan: TMemoField;
    SJQKir: TBooleanField;
    SJQSTNK: TBooleanField;
    SJQPajak: TBooleanField;
    SJQTglKembali: TDateTimeField;
    SJQPendapatan: TCurrencyField;
    SJQPengeluaran: TCurrencyField;
    SJQSisaDisetor: TCurrencyField;
    SJQSPBUAYaniLiter: TFloatField;
    SJQSPBUAYaniUang: TCurrencyField;
    SJQSPBULuarLiter: TFloatField;
    SJQSPBULuarUang: TCurrencyField;
    SJQSPBULuarUangDiberi: TCurrencyField;
    SJQSPBULuarDetail: TMemoField;
    SJQStatus: TStringField;
    SJQCreateDate: TDateTimeField;
    SJQCreateBy: TStringField;
    SJQOperator: TStringField;
    SJQTglEntry: TDateTimeField;
    SJQLaporan: TMemoField;
    SJQTglRealisasi: TDateTimeField;
    SJQAwal: TStringField;
    SJQAkhir: TStringField;
    SJQTglCetak: TDateTimeField;
    SJQClaimSopir: TCurrencyField;
    SJQKeteranganClaimSopir: TStringField;
    SJQPremiSopir: TCurrencyField;
    SJQPremiSopir2: TCurrencyField;
    SJQPremiKernet: TCurrencyField;
    SJQTabunganSopir: TCurrencyField;
    SJQTabunganSopir2: TCurrencyField;
    SJQTol: TCurrencyField;
    SJQUangJalan: TCurrencyField;
    SJQBiayaLainLain: TCurrencyField;
    SJQKeteranganBiayaLainLain: TStringField;
    SJQUangMakan: TCurrencyField;
    SJQUangInap: TCurrencyField;
    SJQUangParkir: TCurrencyField;
    SJQOther: TCurrencyField;
    SJQSPBUAYaniDetail: TStringField;
    SJQTitipTagihan: TBooleanField;
    SJQSudahPrint: TBooleanField;
    SJQNamaPelanggan: TStringField;
    SJQNamaSopir: TStringField;
    SJQTglSO: TDateTimeField;
    cxGrid1DBTableView1Kodenota: TcxGridDBColumn;
    cxGrid1DBTableView1Tgl: TcxGridDBColumn;
    cxGrid1DBTableView1Crew: TcxGridDBColumn;
    cxGrid1DBTableView1TitipKwitansi: TcxGridDBColumn;
    cxGrid1DBTableView1NominalKwitansi: TcxGridDBColumn;
    cxGrid1DBTableView1NoKwitansi: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView1Kir: TcxGridDBColumn;
    cxGrid1DBTableView1STNK: TcxGridDBColumn;
    cxGrid1DBTableView1Pajak: TcxGridDBColumn;
    cxGrid1DBTableView1TglKembali: TcxGridDBColumn;
    cxGrid1DBTableView1Pendapatan: TcxGridDBColumn;
    cxGrid1DBTableView1Pengeluaran: TcxGridDBColumn;
    cxGrid1DBTableView1SisaDisetor: TcxGridDBColumn;
    cxGrid1DBTableView1SPBUAYaniLiter: TcxGridDBColumn;
    cxGrid1DBTableView1SPBUAYaniUang: TcxGridDBColumn;
    cxGrid1DBTableView1SPBUAYaniJam: TcxGridDBColumn;
    cxGrid1DBTableView1SPBULuarLiter: TcxGridDBColumn;
    cxGrid1DBTableView1SPBULuarUang: TcxGridDBColumn;
    cxGrid1DBTableView1SPBULuarUangDiberi: TcxGridDBColumn;
    cxGrid1DBTableView1SPBULuarDetail: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1Laporan: TcxGridDBColumn;
    cxGrid1DBTableView1TglRealisasi: TcxGridDBColumn;
    cxGrid1DBTableView1Awal: TcxGridDBColumn;
    cxGrid1DBTableView1Akhir: TcxGridDBColumn;
    cxGrid1DBTableView1TglCetak: TcxGridDBColumn;
    cxGrid1DBTableView1ClaimSopir: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganClaimSopir: TcxGridDBColumn;
    cxGrid1DBTableView1PremiSopir: TcxGridDBColumn;
    cxGrid1DBTableView1PremiSopir2: TcxGridDBColumn;
    cxGrid1DBTableView1PremiKernet: TcxGridDBColumn;
    cxGrid1DBTableView1TabunganSopir: TcxGridDBColumn;
    cxGrid1DBTableView1TabunganSopir2: TcxGridDBColumn;
    cxGrid1DBTableView1Tol: TcxGridDBColumn;
    cxGrid1DBTableView1UangJalan: TcxGridDBColumn;
    cxGrid1DBTableView1BiayaLainLain: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganBiayaLainLain: TcxGridDBColumn;
    cxGrid1DBTableView1UangMakan: TcxGridDBColumn;
    cxGrid1DBTableView1UangInap: TcxGridDBColumn;
    cxGrid1DBTableView1UangParkir: TcxGridDBColumn;
    cxGrid1DBTableView1Other: TcxGridDBColumn;
    cxGrid1DBTableView1SPBUAYaniDetail: TcxGridDBColumn;
    cxGrid1DBTableView1TitipTagihan: TcxGridDBColumn;
    cxGrid1DBTableView1SudahPrint: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaSopir: TcxGridDBColumn;
    cxGrid1DBTableView1TglSO: TcxGridDBColumn;
    SJQSPBUAYaniJam: TDateTimeField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  SJDropDownFm: TSJDropDownFm;
  pelanggan:string;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TSJDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  pelanggan:=kd;
end;

constructor TSJDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  pelanggan:='';
end;

procedure TSJDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSJDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=SJQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSJDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SJQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSJDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=SJQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TSJDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SJQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSJDropDownFm.FormCreate(Sender: TObject);
begin
  if pelanggan='' then
    begin
      SJQ.Close;
      SJQ.SQL.Text:='select sj.*, so.tgl as TglSO, pl.NamaPT as NamaPelanggan, pg.Nama as NamaSopir from MasterSJ sj left join MasterSO so on sj.NoSO=so.Kodenota left join Pegawai pg on sj.Sopir=pg.Kode left join Pelanggan pl on so.Pelanggan=pl.Kode order by sj.tglentry desc';
      SJQ.Open;
    end
  else
    begin
      SJQ.Close;
      SJQ.ParamByName('text').AsString:= pelanggan;
      SJQ.Open;
    end;
end;

end.
