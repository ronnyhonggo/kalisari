unit SOSJDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TSOSJDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    SOQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    cxGrid1DBTableView1Kodenota: TcxGridDBColumn;
    cxGrid1DBTableView1Tgl: TcxGridDBColumn;
    cxGrid1DBTableView1Berangkat: TcxGridDBColumn;
    cxGrid1DBTableView1Tiba: TcxGridDBColumn;
    cxGrid1DBTableView1Harga: TcxGridDBColumn;
    cxGrid1DBTableView1PPN: TcxGridDBColumn;
    cxGrid1DBTableView1PembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1TglPembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1CaraPembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1KeteranganCaraPembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1NoKwitansiPembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1NominalKwitansiPembayaranAwal: TcxGridDBColumn;
    cxGrid1DBTableView1Pelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1TglPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1CaraPembayaranPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1KetCaraPembayaranPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1NoKwitansiPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1NominalKwitansiPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1Extend: TcxGridDBColumn;
    cxGrid1DBTableView1TglKembaliExtend: TcxGridDBColumn;
    cxGrid1DBTableView1BiayaExtend: TcxGridDBColumn;
    cxGrid1DBTableView1PPNExtend: TcxGridDBColumn;
    cxGrid1DBTableView1KapasitasSeat: TcxGridDBColumn;
    cxGrid1DBTableView1AC: TcxGridDBColumn;
    cxGrid1DBTableView1Toilet: TcxGridDBColumn;
    cxGrid1DBTableView1AirSuspension: TcxGridDBColumn;
    cxGrid1DBTableView1TglFollowUp: TcxGridDBColumn;
    cxGrid1DBTableView1PICJemput: TcxGridDBColumn;
    cxGrid1DBTableView1JamJemput: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelpPICJemput: TcxGridDBColumn;
    cxGrid1DBTableView1AlamatJemput: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1StatusPembayaran: TcxGridDBColumn;
    cxGrid1DBTableView1ReminderPending: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    PegawaiQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    SOQNamaPelanggan: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    ArmadaQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    KontrakQ: TSDQuery;
    KontrakQKode: TStringField;
    KontrakQPelanggan: TStringField;
    KontrakQTglMulai: TDateTimeField;
    KontrakQTglSelesai: TDateTimeField;
    KontrakQStatusRute: TStringField;
    KontrakQRute: TStringField;
    KontrakQAC: TBooleanField;
    KontrakQToilet: TBooleanField;
    KontrakQAirSuspension: TBooleanField;
    KontrakQKapasitasSeat: TIntegerField;
    KontrakQHarga: TCurrencyField;
    KontrakQPOEksternal: TStringField;
    KontrakQStatus: TStringField;
    KontrakQKeterangan: TMemoField;
    KontrakQIntervalPenagihan: TStringField;
    KontrakQCreateDate: TDateTimeField;
    KontrakQCreateBy: TStringField;
    KontrakQOperator: TStringField;
    KontrakQTglEntry: TDateTimeField;
    KontrakQTglCetak: TDateTimeField;
    SOQNamaPenerima: TStringField;
    SOQNamaPenerimaPelunasan: TStringField;
    SOQDari: TStringField;
    SOQKe: TStringField;
    SOQNoBody: TStringField;
    SOQKontrakDengan: TStringField;
    SOQNamaPenerimaPending: TStringField;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenerima: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenerimaPelunasan: TcxGridDBColumn;
    cxGrid1DBTableView1Dari: TcxGridDBColumn;
    cxGrid1DBTableView1Ke: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    cxGrid1DBTableView1KontrakDengan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenerimaPending: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  SOSJDropDownFm: TSOSJDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TSOSJDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSOSJDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=SOQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSOSJDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SOQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSOSJDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=SOQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TSOSJDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SOQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TSOSJDropDownFm.FormCreate(Sender: TObject);
begin
KontrakQ.Open;
ruteq.Open;
ArmadaQ.Open;
PegawaiQ.Open;
PelangganQ.Open;
SOQ.Open;
end;

end.
