object SPKDropDownFm: TSPKDropDownFm
  Left = 209
  Top = 146
  Width = 1170
  Height = 510
  Caption = 'SPKDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1154
    Height = 471
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 110
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 100
      end
      object cxGrid1DBTableView1KodeReferensi: TcxGridDBColumn
        DataBinding.FieldName = 'KodeReferensi'
        Width = 129
      end
      object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Width = 103
      end
      object cxGrid1DBTableView1NoBody: TcxGridDBColumn
        DataBinding.FieldName = 'NoBody'
        Width = 61
      end
      object cxGrid1DBTableView1StatusReferensi: TcxGridDBColumn
        DataBinding.FieldName = 'StatusReferensi'
        Width = 122
      end
      object cxGrid1DBTableView1NamaMekanik: TcxGridDBColumn
        DataBinding.FieldName = 'NamaMekanik'
        Width = 182
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 196
      end
      object cxGrid1DBTableView1DetailTindakan: TcxGridDBColumn
        DataBinding.FieldName = 'DetailTindakan'
        Width = 415
      end
      object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
        DataBinding.FieldName = 'Keterangan'
        Width = 350
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object SPKQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'LP '#39' + spk.lap' +
        'oranperbaikan AS KodeReferensi, pp.armada AS Armada, lpb.status ' +
        'AS StatusReferensi, spk.mekanik AS Mekanik, spk.status AS Status' +
        ', spk.detailtindakan AS DetailTindakan, spk.keterangan AS Ketera' +
        'ngan '
      
        'from suratperintahkerja spk, laporanperbaikan lpb, permintaanper' +
        'baikan pp'
      
        'where spk.laporanperbaikan is not NULL AND spk.laporanperbaikan=' +
        'lpb.kode AND lpb.status='#39'ON PROCESS'#39' AND lpb.pp=pp.kode'
      'UNION'
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'RB '#39' + spk.reb' +
        'uild AS KodeReferensi, rb.dariarmada AS Armada, rb.status AS Sta' +
        'tusReferensi, spk.mekanik AS Mekanik, spk.status AS Status, spk.' +
        'detailtindakan AS DetailTindakan, spk.keterangan AS Keterangan '
      'from suratperintahkerja spk, rebuild rb'
      
        'where spk.rebuild is not NULL AND spk.rebuild=rb.kode AND rb.sta' +
        'tus='#39'ON PROCESS'#39
      'UNION'
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'LR '#39' + spk.Lap' +
        'oranPerawatan AS KodeReferensi, lpr.Armada AS Armada, lpr.Status' +
        ' AS StatusReferensi, spk.mekanik AS Mekanik, spk.status AS Statu' +
        's, spk.detailtindakan AS DetailTindakan, spk.keterangan AS Keter' +
        'angan '
      'from SuratPerintahKerja spk, LaporanPerawatan lpr'
      
        'where spk.LaporanPerawatan is not NULL AND spk.LaporanPerawatan=' +
        'lpr.Kode AND lpr.status='#39'ON PROCESS'#39)
    Left = 216
    Top = 368
    object SPKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SPKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object SPKQKodeReferensi: TStringField
      FieldName = 'KodeReferensi'
      Size = 13
    end
    object SPKQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SPKQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object SPKQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object SPKQStatusReferensi: TStringField
      FieldName = 'StatusReferensi'
      Size = 50
    end
    object SPKQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object SPKQNamaMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 50
      Lookup = True
    end
    object SPKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object SPKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object SPKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
  end
  object LPBDs: TDataSource
    DataSet = SPKQ
    Left = 280
    Top = 368
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 248
    Top = 368
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 312
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
end
