object SettingFm: TSettingFm
  Left = 651
  Top = 113
  Width = 273
  Height = 167
  Caption = 'Setting'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 110
    Width = 257
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGroupBox2: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    Caption = 'Parameter Setting'
    TabOrder = 1
    Height = 110
    Width = 257
    object cxButton1: TcxButton
      Left = 120
      Top = 64
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = cxButton1Click
    end
    object cxLabel1: TcxLabel
      Left = 16
      Top = 32
      Caption = 'Automatic Logout : '
    end
    object cxDBSpinEdit1: TcxDBSpinEdit
      Left = 120
      Top = 32
      DataBinding.DataField = 'AutoLogOut'
      DataBinding.DataSource = DataSource1
      TabOrder = 2
      Width = 121
    end
  end
  object DataSource1: TDataSource
    DataSet = MenuUtamaFm.SettingQ
    Left = 56
    Top = 72
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select AutoLogOut, Kode'
      'from setting'
      '')
    ModifySQL.Strings = (
      'update setting'
      'set'
      '  AutoLogOut = :AutoLogOut')
    InsertSQL.Strings = (
      'insert into setting'
      '  (AutoLogOut)'
      'values'
      '  (:AutoLogOut)')
    DeleteSQL.Strings = (
      'delete from setting'
      '')
    Left = 24
    Top = 72
  end
end
