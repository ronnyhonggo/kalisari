unit Setting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, cxDropDownEdit, UCrpeClasses,
  UCrpe32, cxSpinEdit, cxDBEdit, cxTimeEdit, cxTrackBar, cxDBTrackBar;

type
  TSettingFm = class(TForm)
    StatusBar: TStatusBar;
    cxGroupBox2: TcxGroupBox;
    DataSource1: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    cxButton1: TcxButton;
    cxLabel1: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  SettingFm: TSettingFm;

  MasterOriSQL,ViewOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, StrUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TSettingFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TSettingFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSettingFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TSettingFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;


procedure TSettingFm.FormShow(Sender: TObject);
var
  F: Textfile;
  str: string;
begin
  cxDBSpinEdit1.Enabled:=menuutamafm.UserQSettingParameterNotifikasi.AsBoolean;
  MenuUtamaFm.SettingQ.Edit;
end;

procedure TSettingFm.cxButton1Click(Sender: TObject);
var
  F: Textfile;
  str: string;
begin
MenuUtamaFm.Database1.StartTransaction;
 try
    MenuUtamaFm.SettingQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MenuUtamaFm.SettingQ.CommitUpdates;
    ShowMessage('Setting telah disimpan');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MenuUtamaFm.SettingQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

end.
