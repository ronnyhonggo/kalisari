object SettingNotifikasiFm: TSettingNotifikasiFm
  Left = 482
  Top = 193
  Width = 402
  Height = 395
  Caption = 'Setting Notifikasi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 338
    Width = 386
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGroupBox1: TcxGroupBox
    Left = 16
    Top = 16
    Caption = 'Interval Pengecekan Notifikasi'
    TabOrder = 1
    Height = 73
    Width = 345
    object cxLabel1: TcxLabel
      Left = 10
      Top = 32
      Caption = 'Pengecekan Keseluruhan (menit)'
    end
    object cxDBSpinEdit1: TcxDBSpinEdit
      Left = 192
      Top = 28
      DataBinding.DataField = 'WaktuPengecekan'
      DataBinding.DataSource = DataSource1
      TabOrder = 1
      Width = 121
    end
  end
  object cxButton1: TcxButton
    Left = 152
    Top = 304
    Width = 75
    Height = 25
    Caption = 'SAVE'
    TabOrder = 2
    OnClick = cxButton1Click
  end
  object cxGroupBox2: TcxGroupBox
    Left = 16
    Top = 96
    Caption = 'Parameter Notifikasi'
    TabOrder = 3
    Height = 193
    Width = 345
    object cxLabel2: TcxLabel
      Left = 10
      Top = 32
      Caption = 'Customer Tidak Aktif (hari)'
    end
    object cxLabel3: TcxLabel
      Left = 10
      Top = 72
      Caption = 'Pembuatan Surat Jalan (hari)'
    end
    object cxLabel4: TcxLabel
      Left = 11
      Top = 111
      Caption = 'Pemilihan Armada (hari)'
    end
    object cxLabel5: TcxLabel
      Left = 11
      Top = 151
      Caption = 'Pemilihan Armada (hari)'
    end
  end
  object cxDBSpinEdit2: TcxDBSpinEdit
    Left = 208
    Top = 124
    DataBinding.DataField = 'InactiveCustomer'
    DataBinding.DataSource = DataSource1
    TabOrder = 4
    Width = 121
  end
  object cxDBSpinEdit3: TcxDBSpinEdit
    Left = 208
    Top = 167
    DataBinding.DataField = 'SuratJalan'
    DataBinding.DataSource = DataSource1
    TabOrder = 5
    Width = 121
  end
  object cxDBSpinEdit4: TcxDBSpinEdit
    Left = 208
    Top = 204
    DataBinding.DataField = 'PilihKendaraan'
    DataBinding.DataSource = DataSource1
    TabOrder = 6
    Width = 121
  end
  object cxDBSpinEdit5: TcxDBSpinEdit
    Left = 208
    Top = 248
    DataBinding.DataField = 'Kontrak'
    DataBinding.DataSource = DataSource1
    TabOrder = 7
    Width = 121
  end
  object DataSource1: TDataSource
    DataSet = MenuUtamaFm.NotifQ
    Left = 280
    Top = 8
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, ' +
        'SuratJalan, Kontrak'
      'from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Notifikasi'
      'set'
      '  Kode = :Kode,'
      '  WaktuPengecekan = :WaktuPengecekan,'
      '  InactiveCustomer = :InactiveCustomer,'
      '  PilihKendaraan = :PilihKendaraan,'
      '  SuratJalan = :SuratJalan,'
      '  Kontrak = :Kontrak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Notifikasi'
      
        '  (Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, Sura' +
        'tJalan, Kontrak)'
      'values'
      
        '  (:Kode, :WaktuPengecekan, :InactiveCustomer, :PilihKendaraan, ' +
        ':SuratJalan, :Kontrak)')
    DeleteSQL.Strings = (
      'delete from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 328
    Top = 8
  end
end
