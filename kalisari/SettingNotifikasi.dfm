object SettingNotifikasiFm: TSettingNotifikasiFm
  Left = 615
  Top = 67
  Width = 402
  Height = 523
  Caption = 'Setting Notifikasi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 466
    Width = 386
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGroupBox1: TcxGroupBox
    Left = 16
    Top = 16
    Caption = 'Interval Pengecekan Notifikasi'
    TabOrder = 1
    Height = 73
    Width = 353
    object cxLabel1: TcxLabel
      Left = 10
      Top = 32
      Caption = 'Pengecekan Keseluruhan (menit)'
    end
    object cxDBSpinEdit1: TcxDBSpinEdit
      Left = 192
      Top = 28
      DataBinding.DataField = 'WaktuPengecekan'
      DataBinding.DataSource = DataSource1
      TabOrder = 1
      Width = 121
    end
  end
  object cxButton1: TcxButton
    Left = 152
    Top = 432
    Width = 75
    Height = 25
    Caption = 'SAVE'
    TabOrder = 2
    OnClick = cxButton1Click
  end
  object cxGroupBox2: TcxGroupBox
    Left = 16
    Top = 96
    Caption = 'Parameter Notifikasi'
    TabOrder = 3
    Height = 329
    Width = 353
    object cxLabel2: TcxLabel
      Left = 10
      Top = 32
      Caption = 'Customer Tidak Aktif (hari)'
    end
    object cxLabel3: TcxLabel
      Left = 10
      Top = 72
      Caption = 'Pembuatan Surat Jalan (hari)'
    end
    object cxLabel4: TcxLabel
      Left = 11
      Top = 111
      Caption = 'Pemilihan Armada (hari)'
    end
    object cxLabel5: TcxLabel
      Left = 11
      Top = 151
      Caption = 'Pemilihan Armada (hari)'
    end
    object cxLabel6: TcxLabel
      Left = 11
      Top = 191
      Caption = 'STNK Expired (hari)'
    end
    object cxDBSpinEdit6: TcxDBSpinEdit
      Left = 192
      Top = 189
      DataBinding.DataField = 'STNKExpired'
      DataBinding.DataSource = DataSource1
      TabOrder = 5
      Width = 121
    end
    object cxLabel7: TcxLabel
      Left = 11
      Top = 231
      Caption = 'STNK Pajak Expired (hari)'
    end
    object cxLabel8: TcxLabel
      Left = 11
      Top = 271
      Caption = 'KIR Expired (hari)'
    end
    object cxDBSpinEdit7: TcxDBSpinEdit
      Left = 192
      Top = 229
      DataBinding.DataField = 'STNKPExpired'
      DataBinding.DataSource = DataSource1
      TabOrder = 8
      Width = 121
    end
    object cxDBSpinEdit8: TcxDBSpinEdit
      Left = 192
      Top = 269
      DataBinding.DataField = 'KIRExpired'
      DataBinding.DataSource = DataSource1
      TabOrder = 9
      Width = 121
    end
  end
  object cxDBSpinEdit2: TcxDBSpinEdit
    Left = 208
    Top = 132
    DataBinding.DataField = 'InactiveCustomer'
    DataBinding.DataSource = DataSource1
    TabOrder = 4
    Width = 121
  end
  object cxDBSpinEdit3: TcxDBSpinEdit
    Left = 208
    Top = 175
    DataBinding.DataField = 'SuratJalan'
    DataBinding.DataSource = DataSource1
    TabOrder = 5
    Width = 121
  end
  object cxDBSpinEdit4: TcxDBSpinEdit
    Left = 208
    Top = 212
    DataBinding.DataField = 'PilihKendaraan'
    DataBinding.DataSource = DataSource1
    TabOrder = 6
    Width = 121
  end
  object cxDBSpinEdit5: TcxDBSpinEdit
    Left = 208
    Top = 256
    DataBinding.DataField = 'Kontrak'
    DataBinding.DataSource = DataSource1
    TabOrder = 7
    Width = 121
  end
  object DataSource1: TDataSource
    DataSet = MenuUtamaFm.NotifQ
    Left = 280
    Top = 8
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, ' +
        'SuratJalan, Kontrak, STNKExpired, STNKPExpired, KIRExpired'
      'from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Notifikasi'
      'set'
      '  Kode = :Kode,'
      '  WaktuPengecekan = :WaktuPengecekan,'
      '  InactiveCustomer = :InactiveCustomer,'
      '  PilihKendaraan = :PilihKendaraan,'
      '  SuratJalan = :SuratJalan,'
      '  Kontrak = :Kontrak,'
      '  STNKExpired = :STNKExpired,'
      '  STNKPExpired = :STNKPExpired,'
      '  KIRExpired = :KIRExpired'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Notifikasi'
      
        '  (Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, Sura' +
        'tJalan, Kontrak, STNKExpired, STNKPExpired, KIRExpired)'
      'values'
      
        '  (:Kode, :WaktuPengecekan, :InactiveCustomer, :PilihKendaraan, ' +
        ':SuratJalan, :Kontrak, :STNKExpired, :STNKPExpired, :KIRExpired)')
    DeleteSQL.Strings = (
      'delete from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 328
    Top = 8
  end
end
