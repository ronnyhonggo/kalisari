unit SettingNotifikasi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, cxDropDownEdit, UCrpeClasses,
  UCrpe32, cxSpinEdit, cxDBEdit, cxTimeEdit, cxTrackBar, cxDBTrackBar;

type
  TSettingNotifikasiFm = class(TForm)
    StatusBar: TStatusBar;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxButton1: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxDBSpinEdit4: TcxDBSpinEdit;
    DataSource1: TDataSource;
    SDUpdateSQL1: TSDUpdateSQL;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel5: TcxLabel;
    cxDBSpinEdit5: TcxDBSpinEdit;
    cxLabel6: TcxLabel;
    cxDBSpinEdit6: TcxDBSpinEdit;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxDBSpinEdit7: TcxDBSpinEdit;
    cxDBSpinEdit8: TcxDBSpinEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  SettingNotifikasiFm: TSettingNotifikasiFm;

  MasterOriSQL,ViewOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, StrUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TSettingNotifikasiFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TSettingNotifikasiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSettingNotifikasiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TSettingNotifikasiFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;


procedure TSettingNotifikasiFm.FormShow(Sender: TObject);
var
  F: Textfile;
  str: string;
begin
  cxDBSpinEdit2.Enabled:=menuutamafm.UserQSettingParameterNotifikasi.AsBoolean;
  cxDBSpinEdit3.Enabled:=menuutamafm.UserQSettingParameterNotifikasi.AsBoolean;
  cxDBSpinEdit4.Enabled:=menuutamafm.UserQSettingParameterNotifikasi.AsBoolean;
  cxDBSpinEdit1.Enabled:=menuutamafm.UserQSettingWaktuNotifikasi.AsBoolean;
  cxDBSpinEdit5.Enabled:=menuutamafm.UserQSettingParameterNotifikasi.AsBoolean;
  MenuUtamaFm.NotifQ.Edit;
end;

procedure TSettingNotifikasiFm.cxButton1Click(Sender: TObject);
var
  F: Textfile;
  str: string;
begin
MenuUtamaFm.Database1.StartTransaction;
 try
    MenuUtamaFm.NotifQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MenuUtamaFm.NotifQ.CommitUpdates;
    ShowMessage('Setting telah disimpan');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MenuUtamaFm.NotifQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

end.
