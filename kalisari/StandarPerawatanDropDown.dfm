object StandarPerawatanDropDownFm: TStandarPerawatanDropDownFm
  Left = 417
  Top = 103
  Width = 732
  Height = 459
  Caption = 'StandarPerawatanDropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 716
    Height = 421
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 114
      end
      object cxGrid1DBTableView1NamaJenis: TcxGridDBColumn
        DataBinding.FieldName = 'NamaJenis'
        Width = 186
      end
      object cxGrid1DBTableView1TipeJenis: TcxGridDBColumn
        DataBinding.FieldName = 'TipeJenis'
        Width = 136
      end
      object cxGrid1DBTableView1TipePerawatan: TcxGridDBColumn
        DataBinding.FieldName = 'TipePerawatan'
        Width = 234
      end
      object cxGrid1DBTableView1PeriodeWaktu: TcxGridDBColumn
        DataBinding.FieldName = 'PeriodeWaktu'
        Width = 73
      end
      object cxGrid1DBTableView1PeriodeKm: TcxGridDBColumn
        DataBinding.FieldName = 'PeriodeKm'
      end
      object cxGrid1DBTableView1StandardWaktu: TcxGridDBColumn
        DataBinding.FieldName = 'StandardWaktu'
        Width = 80
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object SPQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from standarperawatan'
      'where jeniskendaraan=:text'
      '')
    Left = 216
    Top = 368
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SPQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object SPQTipePerawatan: TStringField
      FieldName = 'TipePerawatan'
      Required = True
      Size = 50
    end
    object SPQPeriodeWaktu: TIntegerField
      FieldName = 'PeriodeWaktu'
    end
    object SPQPeriodeKm: TIntegerField
      FieldName = 'PeriodeKm'
    end
    object SPQStandardWaktu: TIntegerField
      FieldName = 'StandardWaktu'
    end
    object SPQNamaJenis: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaJenis'
      LookupDataSet = JKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaJenis'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
    object SPQTipeJenis: TStringField
      FieldKind = fkLookup
      FieldName = 'TipeJenis'
      LookupDataSet = JKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tipe'
      KeyFields = 'JenisKendaraan'
      Size = 50
      Lookup = True
    end
  end
  object LPBDs: TDataSource
    DataSet = SPQ
    Left = 280
    Top = 368
  end
  object JKQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 248
    Top = 368
    object JKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JKQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JKQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JKQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object JKQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
