unit StandarPerawatanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine;

type
  TStandarPerawatanDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SPQ: TSDQuery;
    LPBDs: TDataSource;
    JKQ: TSDQuery;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1TipePerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1PeriodeWaktu: TcxGridDBColumn;
    cxGrid1DBTableView1PeriodeKm: TcxGridDBColumn;
    cxGrid1DBTableView1StandardWaktu: TcxGridDBColumn;
    JKQKode: TStringField;
    JKQNamaJenis: TStringField;
    JKQTipe: TMemoField;
    JKQKategori: TStringField;
    JKQKeterangan: TMemoField;
    cxGrid1DBTableView1NamaJenis: TcxGridDBColumn;
    cxGrid1DBTableView1TipeJenis: TcxGridDBColumn;
    SPQKode: TStringField;
    SPQJenisKendaraan: TStringField;
    SPQTipePerawatan: TStringField;
    SPQPeriodeWaktu: TIntegerField;
    SPQPeriodeKm: TIntegerField;
    SPQStandardWaktu: TIntegerField;
    SPQNamaJenis: TStringField;
    SPQTipeJenis: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  StandarPerawatanDropDownFm: TStandarPerawatanDropDownFm;
  jeniskendaraan:string;
  SPQOriSQL:string;

implementation

{$R *.dfm}

constructor TStandarPerawatanDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  jeniskendaraan:=kd;
end;

procedure TStandarPerawatanDropDownFm.FormCreate(Sender: TObject);
begin
  JKQ.Open;
  if jeniskendaraan='all' then
    begin
      SPQ.Close;
      SPQOriSQL:=SPQ.SQL.Text;
      SPQ.SQL.Text:='select * from standarperawatan';
      SPQ.ExecSQL;
      SPQ.Open;
    end
  else
    begin
      SPQ.Close;
      SPQ.ParamByName('text').AsString:= jeniskendaraan;
      SPQ.Open;
    end;
end;

procedure TStandarPerawatanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SPQKode.AsString;
  SPQ.SQL.Text:=SPQOriSQL;
  ModalResult:=mrOK;
end;

end.
