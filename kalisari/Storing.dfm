object StoringFm: TStoringFm
  Left = 231
  Top = 83
  Width = 920
  Height = 621
  AutoSize = True
  Caption = 'Verpal'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
      Visible = False
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 509
    Width = 904
    Height = 48
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 188
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object deleteBtn: TcxButton
      Left = 100
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = deleteBtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 606
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 3
      Height = 46
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 425
    Height = 261
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 159
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTanggal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tanggal'
      ID = 30
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Armada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridPlatNo: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridNoBody: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGridSopir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridSopirEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pengemudi'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridNamaSopir: TcxDBEditorRow
      Properties.Caption = 'NamaPengemudi'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaSopir'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 3
      Index = 0
      Version = 1
    end
    object MasterVGridPICJemput: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPICJemputEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'PICJemput'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridNamaPIC: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NamaPIC'
      Properties.Options.Editing = False
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
    object MasterVGridJabatanPIC: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'JabatanPIC'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 5
      Index = 1
      Version = 1
    end
    object MasterVGridJenisStoring: TcxDBEditorRow
      Properties.Caption = 'JenisVerpal'
      Properties.DataBinding.FieldName = 'JenisStoring'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridTindakanPerbaikan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TindakanPerbaikan'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 9
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridBiaya: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.UseDisplayFormatWhenEditing = True
      Properties.EditProperties.UseThousandSeparator = True
      Properties.DataBinding.FieldName = 'Biaya'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 10
      ParentID = -1
      Index = 6
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 557
    Width = 904
    Height = 26
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 309
    Width = 904
    Height = 200
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DSStoring
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 95
      end
      object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
        DataBinding.FieldName = 'PlatNo'
        Options.SortByDisplayText = isbtOn
      end
      object cxGrid1DBTableView1NoBody: TcxGridDBColumn
        DataBinding.FieldName = 'NoBody'
        Options.SortByDisplayText = isbtOn
        Width = 57
      end
      object cxGrid1DBTableView1NamaPengemudi: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPengemudi'
        Options.SortByDisplayText = isbtOn
        Width = 221
      end
      object cxGrid1DBTableView1PICJemput: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPIC'
        Width = 204
      end
      object cxGrid1DBTableView1TglEntry: TcxGridDBColumn
        DataBinding.FieldName = 'TglEntry'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.ShowTime = False
        Width = 154
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 425
    Top = 48
    Width = 479
    Height = 261
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object cxGrid2DBTableView1: TcxGridDBTableView
      DataController.DataSource = TampilBarangDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid2DBTableView1Nama: TcxGridDBColumn
        DataBinding.FieldName = 'Nama'
        Width = 121
      end
      object cxGrid2DBTableView1JumlahDiminta: TcxGridDBColumn
        DataBinding.FieldName = 'JumlahDiminta'
      end
      object cxGrid2DBTableView1JumlahBeli: TcxGridDBColumn
        DataBinding.FieldName = 'JumlahBeli'
        Width = 72
      end
      object cxGrid2DBTableView1StatusBeli: TcxGridDBColumn
        DataBinding.FieldName = 'StatusBeli'
        Width = 72
      end
      object cxGrid2DBTableView1StatusMinta: TcxGridDBColumn
        DataBinding.FieldName = 'StatusMinta'
        Width = 92
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from storing')
    UpdateObject = MasterUS
    Left = 177
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBiaya: TCurrencyField
      FieldName = 'Biaya'
      Required = True
    end
    object MasterQPICJemput: TStringField
      FieldName = 'PICJemput'
      Required = True
      Size = 50
    end
    object MasterQJenisStoring: TMemoField
      FieldName = 'JenisStoring'
      Required = True
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object MasterQNamaPIC: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPIC'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PICJemput'
      Size = 50
      Lookup = True
    end
    object MasterQJabatanPIC: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanPIC'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jabatan'
      KeyFields = 'PICJemput'
      Size = 50
      Lookup = True
    end
    object MasterQKategoriRute: TStringField
      FieldName = 'KategoriRute'
      Size = 50
    end
    object MasterQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object MasterQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object MasterQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object MasterQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 244
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, SuratJalan, Armada, Pengemudi, KategoriRut' +
        'e, Biaya, PICJemput, JenisStoring, CreateDate, CreateBy, Operato' +
        'r, TglEntry, TindakanPerbaikan, TglCetak'#13#10'from storing'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update storing'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  SuratJalan = :SuratJalan,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KategoriRute = :KategoriRute,'
      '  Biaya = :Biaya,'
      '  PICJemput = :PICJemput,'
      '  JenisStoring = :JenisStoring,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TindakanPerbaikan = :TindakanPerbaikan,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into storing'
      
        '  (Kode, Tanggal, SuratJalan, Armada, Pengemudi, KategoriRute, B' +
        'iaya, PICJemput, JenisStoring, CreateDate, CreateBy, Operator, T' +
        'glEntry, TindakanPerbaikan, TglCetak)'
      'values'
      
        '  (:Kode, :Tanggal, :SuratJalan, :Armada, :Pengemudi, :KategoriR' +
        'ute, :Biaya, :PICJemput, :JenisStoring, :CreateDate, :CreateBy, ' +
        ':Operator, :TglEntry, :TindakanPerbaikan, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from storing'
      'where'
      '  Kode = :OLD_Kode')
    Left = 276
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from storing order by kode desc')
    Left = 209
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select * from masterso where status='#39'DEAL'#39' and armada!=null and ' +
        '(kodenota like '#39'%'#39' + :text + '#39'%'#39' or tgl like '#39'%'#39' + :text + '#39'%'#39')'
      ''
      ''
      '')
    Left = 313
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 353
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39)
    Left = 385
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object PelangganQNoFax: TStringField
      FieldName = 'NoFax'
      Size = 50
    end
  end
  object viewStoringQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(s.tanggal as date) as Tanggal,s.* '
      'from Storing s'
      
        'where (s.Tanggal>=:text1 and s.Tanggal<=:text2) or s.tanggal is ' +
        'null'
      'order by tglentry desc')
    Left = 489
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewStoringQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewStoringQSuratJalan: TStringField
      FieldName = 'SuratJalan'
      Required = True
      Size = 10
    end
    object viewStoringQBiaya: TCurrencyField
      FieldName = 'Biaya'
      Required = True
    end
    object viewStoringQPICJemput: TStringField
      FieldName = 'PICJemput'
      Required = True
      Size = 10
    end
    object viewStoringQJenisStoring: TMemoField
      FieldName = 'JenisStoring'
      Required = True
      BlobType = ftMemo
    end
    object viewStoringQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewStoringQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewStoringQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewStoringQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewStoringQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object viewStoringQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewStoringQNamaPIC: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPIC'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PICJemput'
      Size = 50
      Lookup = True
    end
    object viewStoringQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object viewStoringQPengemudi: TStringField
      FieldName = 'Pengemudi'
      Size = 10
    end
    object viewStoringQKategoriRute: TStringField
      FieldName = 'KategoriRute'
      Size = 50
    end
    object viewStoringQNamaPengemudi: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPengemudi'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pengemudi'
      Size = 50
      Lookup = True
    end
    object viewStoringQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 50
      Lookup = True
    end
    object viewStoringQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
  end
  object DSStoring: TDataSource
    DataSet = viewStoringQ
    Left = 524
    Top = 6
  end
  object tampilBarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select b.Nama,dbb.JumlahBeli,dbb.JumlahDiminta, dbb.StatusBeli,d' +
        'bb.StatusMinta from BonBarang bb, DetailBonBarang dbb, Barang b ' +
        'where dbb.KodeBonBarang=bb.Kode and b.Kode=dbb.KodeBarang and bb' +
        '.Storing= :text ')
    Left = 608
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object tampilBarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object tampilBarangQStatusBeli: TStringField
      FieldName = 'StatusBeli'
      Size = 50
    end
    object tampilBarangQStatusMinta: TStringField
      FieldName = 'StatusMinta'
      Size = 50
    end
    object tampilBarangQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
    end
    object tampilBarangQJumlahDiminta: TFloatField
      FieldName = 'JumlahDiminta'
      Required = True
    end
  end
  object TampilBarangDS: TDataSource
    DataSet = tampilBarangQ
    Left = 656
    Top = 8
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 456
    Top = 16
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
end
