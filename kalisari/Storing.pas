unit Storing;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, cxCurrencyEdit,
  cxRichEdit, cxMemo, dxSkinscxPCPainter, cxDropDownEdit, cxGroupBox;

type
  TStoringFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    KodeQkode: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    viewStoringQ: TSDQuery;
    DSStoring: TDataSource;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1PICJemput: TcxGridDBColumn;
    cxGrid1DBTableView1TglEntry: TcxGridDBColumn;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQHarga: TCurrencyField;
    SOQRute: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQStatus: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQTglCetak: TDateTimeField;
    deleteBtn: TcxButton;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    tampilBarangQ: TSDQuery;
    TampilBarangDS: TDataSource;
    cxGrid2DBTableView1Nama: TcxGridDBColumn;
    cxGrid2DBTableView1JumlahBeli: TcxGridDBColumn;
    cxGrid2DBTableView1StatusBeli: TcxGridDBColumn;
    cxGrid2DBTableView1StatusMinta: TcxGridDBColumn;
    MasterQKode: TStringField;
    MasterQBiaya: TCurrencyField;
    MasterQPICJemput: TStringField;
    MasterQJenisStoring: TMemoField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTindakanPerbaikan: TMemoField;
    MasterQTglCetak: TDateTimeField;
    MasterVGridBiaya: TcxDBEditorRow;
    MasterVGridPICJemput: TcxDBEditorRow;
    MasterVGridJenisStoring: TcxDBEditorRow;
    MasterVGridTindakanPerbaikan: TcxDBEditorRow;
    MasterVGridArmada: TcxDBEditorRow;
    MasterVGridSopir: TcxDBEditorRow;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    PelangganQKota: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    viewStoringQKode: TStringField;
    viewStoringQSuratJalan: TStringField;
    viewStoringQBiaya: TCurrencyField;
    viewStoringQPICJemput: TStringField;
    viewStoringQJenisStoring: TMemoField;
    viewStoringQCreateDate: TDateTimeField;
    viewStoringQCreateBy: TStringField;
    viewStoringQOperator: TStringField;
    viewStoringQTglEntry: TDateTimeField;
    viewStoringQTindakanPerbaikan: TMemoField;
    viewStoringQTglCetak: TDateTimeField;
    MasterQNamaSopir: TStringField;
    MasterVGridNamaSopir: TcxDBEditorRow;
    MasterQNamaPIC: TStringField;
    MasterQJabatanPIC: TStringField;
    MasterVGridNamaPIC: TcxDBEditorRow;
    MasterVGridJabatanPIC: TcxDBEditorRow;
    tampilBarangQNama: TStringField;
    tampilBarangQStatusBeli: TStringField;
    tampilBarangQStatusMinta: TStringField;
    SOQTglPelunasan: TDateTimeField;
    viewStoringQNamaPIC: TStringField;
    MasterQKategoriRute: TStringField;
    MasterQNoBody: TStringField;
    MasterQPlatNo: TStringField;
    MasterVGridNoBody: TcxDBEditorRow;
    MasterVGridPlatNo: TcxDBEditorRow;
    MasterQArmada: TStringField;
    MasterQPengemudi: TStringField;
    MasterQSuratJalan: TStringField;
    viewStoringQArmada: TStringField;
    viewStoringQPengemudi: TStringField;
    viewStoringQKategoriRute: TStringField;
    viewStoringQNamaPengemudi: TStringField;
    viewStoringQNoBody: TStringField;
    cxGrid1DBTableView1NamaPengemudi: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    viewStoringQPlatNo: TStringField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    tampilBarangQJumlahBeli: TIntegerField;
    tampilBarangQJumlahDiminta: TFloatField;
    cxGrid2DBTableView1JumlahDiminta: TcxGridDBColumn;
    MasterQTanggal: TDateTimeField;
    MasterVGridTanggal: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);

  

    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridPICJemputEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridSopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
 
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  StoringFm: TStoringFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, ArmadaDropDown, PengemudiDropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TStoringFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TStoringFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TStoringFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TStoringFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TStoringFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TStoringFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridSuratJalan);
end;

procedure TStoringFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TStoringFm.FormShow(Sender: TObject);
begin
  //KodeEdit.SetFocus;
  //cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewStoringQ.Close;
  viewStoringQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewStoringQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  viewStoringQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertStoring.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateStoring.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteStoring.AsBoolean;
end;

procedure TStoringFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      tampilBarangQ.Close;
      tampilBarangQ.ParamByName('text').AsString:=KodeEdit.Text;
      tampilBarangQ.Open;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteStoring.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateStoring.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TStoringFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TStoringFm.SaveBtnClick(Sender: TObject);
begin

  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    //MasterQStatus.AsString :='IN PROGRESS';
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Surat Jalan dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
    viewStoringQ.Close;
    viewStoringQ.Open;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

procedure TStoringFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TStoringFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TStoringFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Storing '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Verpal telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Verpal pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TStoringFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;



procedure TStoringFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewStoringQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      KodeEdit.Text:= MasterQKode.AsString;
      tampilBarangQ.Close;
      tampilBarangQ.ParamByName('text').AsString:=KodeEdit.Text;
      tampilBarangQ.Open;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateStoring.AsBoolean;
    deleteBtn.Enabled:=menuutamafm.UserQDeleteStoring.AsBoolean;
    MasterVGrid.Enabled:=True;
end;

procedure TStoringFm.MasterVGridPICJemputEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
//  PegawaiQ.Close;
  //  PegawaiQ.Open;
    PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPICJemput.AsString:=PegawaiDropDownFm.kode;
    end;
    PegawaiDropDownFm.Release;
end;


procedure TStoringFm.MasterVGridArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //sopirQ.Close;
  //  ArmadaQ.Close;
  //  ArmadaQ.Open;
  // sopirQ.ExecSQL;
   // sopirQ.Open;
   ArmadaDropDownFm:=TArmadaDropdownfm.Create(Self);
    if ArmadaDropDownFm.ShowModal=MrOK then
    begin
     // MasterQ.Open;
      //MasterQ.Edit;
    //MasterQSopir.AsString := sopirQKode.AsString;
    MasterQArmada.AsString:=ArmadaDropDownFm.kode;

    end;
    ArmadaDropDownFm.Release;
  
end;

procedure TStoringFm.MasterVGridSopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var tempSQL:string;
  begin
  //sopirQ.Close;
   { PegawaiQ.Close;
    tempSQL:=PegawaiQ.SQL.Text;
    PegawaiQ.SQL.Text:='select * from Pegawai where upper(Jabatan)="PENGEMUDI"';
    //PegawaiQ.ExecSQL;
    PegawaiQ.Open; }
  // sopirQ.ExecSQL;
   // sopirQ.Open;
   PengemudiDropDownFm:=TPengemudiDropdownfm.Create(Self);
    if PengemudiDropDownFm.ShowModal=MrOK then
    begin
     // MasterQ.Open;
      //MasterQ.Edit;
    //MasterQSopir.AsString := sopirQKode.AsString;
    MasterQPengemudi.AsString:=PengemudiDropDownFm.kode;

    end;
    PengemudiDropDownFm.Release;
    //PegawaiQ.SQL.Text:=tempSQL;
end;

procedure TStoringFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewStoringQ.Close;
  viewStoringQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewStoringQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewStoringQ.Open;
end;

procedure TStoringFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewStoringQ.Close;
  viewStoringQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  viewStoringQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  viewStoringQ.Open;
end;

end.

