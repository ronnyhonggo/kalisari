�
 TSTORINGDROPDOWNFM 0�9  TPF0TStoringDropDownFmStoringDropDownFmLeft� Top�Width�HeightLCaptionVerpalDropDownColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TPanelpnl1Left Top�Width}Height)AlignalBottomTabOrder   TcxGridcxGrid1Left Top Width}Height�AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TcxGridDBTableViewcxGrid1DBTableView1Navigator.Buttons.First.Visible#Navigator.Buttons.PriorPage.VisibleNavigator.Buttons.Prior.VisibleNavigator.Buttons.Next.Visible"Navigator.Buttons.NextPage.VisibleNavigator.Buttons.Last.Visible Navigator.Buttons.Append.VisibleNavigator.Buttons.Edit.Visible	Navigator.Buttons.Post.Visible	&Navigator.Buttons.SaveBookmark.Visible&Navigator.Buttons.GotoBookmark.Visible Navigator.Buttons.Filter.VisibleNavigator.Visible	OnCellDblClickcxGrid1DBTableView1CellDblClickDataController.DataSourceDataSource1/DataController.Summary.DefaultGroupSummaryItems )DataController.Summary.FooterSummaryItems $DataController.Summary.SummaryGroups OptionsBehavior.FocusCellOnTab	OptionsBehavior.IncSearch	OptionsData.EditingOptionsView.GroupByBox TcxGridDBColumncxGrid1DBTableView1KodeDataBinding.FieldNameKodeWidthk  TcxGridDBColumncxGrid1DBTableView1TglDataBinding.FieldNameTanggalWidth�   TcxGridDBColumncxGrid1DBTableView1NoBodyDataBinding.FieldNameNoBodyWidthS  TcxGridDBColumncxGrid1DBTableView1PlatNoDataBinding.FieldNamePlatNo  TcxGridDBColumn cxGrid1DBTableView1NamaPengemudiDataBinding.FieldNameNamaPengemudiWidth�   TcxGridDBColumncxGrid1DBTableView1BiayaDataBinding.FieldNameBiayaWidth�   TcxGridDBColumncxGrid1DBTableView1JenisStoringDataBinding.FieldNameJenisStoring  TcxGridDBColumn$cxGrid1DBTableView1TindakanPerbaikanDataBinding.FieldNameTindakanPerbaikanWidth1   TcxGridLevelcxGrid1Level1GridViewcxGrid1DBTableView1   TDataSourceDataSource1DataSetStoringQLeftHTop\  TSDQueryStoringQDatabaseNameDataOptions Active	SQL.Strings]select st.*, a.NoBody, a.PlatNo, p.nama as NamaPengemudi from storing st, pegawai p, armada a.where a.kode=st.armada and p.kode=st.pengemudiorder by tglentry desc LeftTopP TStringFieldStoringQKode	FieldNameKodeRequired	Size
  TDateTimeFieldStoringQTanggal	FieldNameTanggal  TStringFieldStoringQSuratJalan	FieldName
SuratJalanSize
  TStringFieldStoringQArmada	FieldNameArmadaSize
  TStringFieldStoringQPengemudi	FieldName	PengemudiSize
  TStringFieldStoringQKategoriRute	FieldNameKategoriRuteSize2  TCurrencyFieldStoringQBiaya	FieldNameBiayaRequired	  TStringFieldStoringQPICJemput	FieldName	PICJemputRequired	Size
  
TMemoFieldStoringQJenisStoring	FieldNameJenisStoringRequired	BlobTypeftMemo  TDateTimeFieldStoringQCreateDate	FieldName
CreateDate  TStringFieldStoringQCreateBy	FieldNameCreateBySize
  TStringFieldStoringQOperator	FieldNameOperatorSize
  TDateTimeFieldStoringQTglEntry	FieldNameTglEntry  
TMemoFieldStoringQTindakanPerbaikan	FieldNameTindakanPerbaikanBlobTypeftMemo  TDateTimeFieldStoringQTglCetak	FieldNameTglCetak  TStringFieldStoringQNoBody	FieldNameNoBodySize2  TStringFieldStoringQPlatNo	FieldNamePlatNoSize
  TStringFieldStoringQNamaPengemudi	FieldNameNamaPengemudiRequired	Size2   TSDQuerySJQDatabaseNameDataOptions Active	SQL.Stringsselect * from MasterSJ LeftHTop8 TStringFieldSJQKodenota	FieldNameKodenotaRequired	Size
  TDateTimeFieldSJQTgl	FieldNameTglRequired	  TStringFieldSJQNoSO	FieldNameNoSORequired	Size
  TStringFieldSJQSopir	FieldNameSopirSize
  TStringField	SJQSopir2	FieldNameSopir2Size
  TStringFieldSJQCrew	FieldNameCrewSize2  TBooleanFieldSJQTitipKwitansi	FieldNameTitipKwitansi  TCurrencyFieldSJQNominalKwitansi	FieldNameNominalKwitansi  TStringFieldSJQNoKwitansi	FieldName
NoKwitansiSize2  TBooleanFieldSJQKir	FieldNameKirRequired	  TBooleanFieldSJQSTNK	FieldNameSTNKRequired	  TBooleanFieldSJQPajak	FieldNamePajakRequired	  TDateTimeFieldSJQTglKembali	FieldName
TglKembali  TCurrencyFieldSJQPendapatan	FieldName
Pendapatan  TCurrencyFieldSJQPengeluaran	FieldNamePengeluaran  TCurrencyFieldSJQSisaDisetor	FieldNameSisaDisetor  TFloatFieldSJQSPBUAYaniLiter	FieldNameSPBUAYaniLiter  TCurrencyFieldSJQSPBUAYaniUang	FieldNameSPBUAYaniUang  TFloatFieldSJQSPBULuarLiter	FieldNameSPBULuarLiter  TCurrencyFieldSJQSPBULuarUang	FieldNameSPBULuarUang  TCurrencyFieldSJQSPBULuarUangDiberi	FieldNameSPBULuarUangDiberi  
TMemoFieldSJQSPBULuarDetail	FieldNameSPBULuarDetailBlobTypeftMemo  TStringField	SJQStatus	FieldNameStatusRequired	Size2  TDateTimeFieldSJQCreateDate	FieldName
CreateDate  TStringFieldSJQCreateBy	FieldNameCreateBySize
  TStringFieldSJQOperator	FieldNameOperatorSize
  TDateTimeFieldSJQTglEntry	FieldNameTglEntry  
TMemoField
SJQLaporan	FieldNameLaporanBlobTypeftMemo  TDateTimeFieldSJQTglRealisasi	FieldNameTglRealisasi  TStringFieldSJQAwal	FieldNameAwalSize
  TStringFieldSJQAkhir	FieldNameAkhirSize
  TDateTimeFieldSJQTglCetak	FieldNameTglCetak  TCurrencyFieldSJQClaimSopir	FieldName
ClaimSopir  TStringFieldSJQKeteranganClaimSopir	FieldNameKeteranganClaimSopirSize2  TCurrencyFieldSJQPremiSopir	FieldName
PremiSopir  TCurrencyFieldSJQPremiSopir2	FieldNamePremiSopir2  TCurrencyFieldSJQPremiKernet	FieldNamePremiKernet  TCurrencyFieldSJQTabunganSopir	FieldNameTabunganSopir  TCurrencyFieldSJQTabunganSopir2	FieldNameTabunganSopir2  TCurrencyFieldSJQTol	FieldNameTol  TCurrencyFieldSJQUangJalan	FieldName	UangJalan  TCurrencyFieldSJQBiayaLainLain	FieldNameBiayaLainLain  TStringFieldSJQKeteranganBiayaLainLain	FieldNameKeteranganBiayaLainLainSize2  TCurrencyFieldSJQUangMakan	FieldName	UangMakan  TCurrencyFieldSJQUangInap	FieldNameUangInap  TCurrencyFieldSJQUangParkir	FieldName
UangParkir  TCurrencyFieldSJQOther	FieldNameOther  
TMemoFieldSJQKeterangan	FieldName
KeteranganBlobTypeftMemo  TDateTimeFieldSJQSPBUAYaniJam	FieldNameSPBUAYaniJam   TSDQueryRuteQDatabaseNameDataOptions SQL.Stringsselect * from Rute LeftpTop� TStringField	RuteQKode	FieldNameKodeRequired	Size
  TStringField	RuteQMuat	FieldNameMuatRequired	Size2  TStringFieldRuteQBongkar	FieldNameBongkarRequired	Size2  TIntegerField
RuteQJarak	FieldNameJarak  TStringFieldRuteQKategori	FieldNameKategoriSize2  TStringFieldRuteQLevelRute	FieldName	LevelRuteSize2  TFloatField	RuteQPoin	FieldNamePoin  TCurrencyFieldRuteQPremiPengemudi	FieldNamePremiPengemudi  TCurrencyFieldRuteQPremiKernet	FieldNamePremiKernet  TCurrencyFieldRuteQPremiKondektur	FieldNamePremiKondektur  TCurrencyFieldRuteQMel	FieldNameMel  TCurrencyFieldRuteQTol	FieldNameTol  TCurrencyFieldRuteQUangJalanBesar	FieldNameUangJalanBesar  TCurrencyFieldRuteQUangJalanKecil	FieldNameUangJalanKecil  TCurrencyFieldRuteQUangBBM	FieldNameUangBBM  TCurrencyFieldRuteQUangMakan	FieldName	UangMakan  TIntegerField
RuteQWaktu	FieldNameWaktu  TCurrencyFieldRuteQStandarHargaMax	FieldNameStandarHargaMax  TCurrencyFieldRuteQStandarHarga	FieldNameStandarHarga  TDateTimeFieldRuteQCreateDate	FieldName
CreateDate  TStringFieldRuteQCreateBy	FieldNameCreateBySize
  TStringFieldRuteQOperator	FieldNameOperatorSize
  TDateTimeFieldRuteQTglEntry	FieldNameTglEntry   TSDQueryArmadaQDatabaseNameDataOptions Active	SQL.Stringsselect * from Armada Left�TopP TStringFieldArmadaQKode	FieldNameKodeRequired	Size
  TStringFieldArmadaQPlatNo	FieldNamePlatNoSize
  TStringFieldArmadaQNoBody	FieldNameNoBodySize2  TStringFieldArmadaQNoRangka	FieldNameNoRangkaSize2  TStringFieldArmadaQNoMesin	FieldNameNoMesinSize2  TStringFieldArmadaQJenisKendaraan	FieldNameJenisKendaraanSize
  TIntegerFieldArmadaQJumlahSeat	FieldName
JumlahSeat  TStringFieldArmadaQJenisBBM	FieldNameJenisBBMSize2  TStringFieldArmadaQTahunPembuatan	FieldNameTahunPembuatanSize  TStringFieldArmadaQJenisAC	FieldNameJenisACSize
  TBooleanFieldArmadaQToilet	FieldNameToilet  TBooleanFieldArmadaQAirSuspension	FieldNameAirSuspension  TStringFieldArmadaQSopir	FieldNameSopirSize
  TIntegerFieldArmadaQKapasitasTangkiBBM	FieldNameKapasitasTangkiBBM  TDateTimeFieldArmadaQSTNKPajakExpired	FieldNameSTNKPajakExpired  TDateTimeFieldArmadaQKirSelesai	FieldName
KirSelesai  TStringFieldArmadaQLevelArmada	FieldNameLevelArmadaSize2  TIntegerFieldArmadaQJumlahBan	FieldName	JumlahBan  TStringFieldArmadaQKeterangan	FieldName
KeteranganSize2  TBooleanFieldArmadaQAktif	FieldNameAktif  TBooleanField	ArmadaQAC	FieldNameAC  TIntegerFieldArmadaQKmSekarang	FieldName
KmSekarang  TDateTimeFieldArmadaQCreateDate	FieldName
CreateDate  TStringFieldArmadaQCreateBy	FieldNameCreateBySize
  TStringFieldArmadaQOperator	FieldNameOperatorSize
  TDateTimeFieldArmadaQTglEntry	FieldNameTglEntry  TDateTimeFieldArmadaQSTNKPerpanjangExpired	FieldNameSTNKPerpanjangExpired  TDateTimeFieldArmadaQKirMulai	FieldNameKirMulai   TSDQueryPegawaiQDatabaseNameDataOptions Active	SQL.Stringsselect * from Pegawai Left�Topp TStringFieldPegawaiQKode	FieldNameKodeRequired	Size
  TStringFieldPegawaiQNama	FieldNameNamaSize2  TStringFieldPegawaiQAlamat	FieldNameAlamatSize2  TStringFieldPegawaiQKota	FieldNameKotaSize2  TStringFieldPegawaiQNoTelp	FieldNameNoTelpSize2  TStringFieldPegawaiQNoHP	FieldNameNoHPSize2  TDateTimeFieldPegawaiQTglLahir	FieldNameTglLahir  TCurrencyFieldPegawaiQGaji	FieldNameGaji  TStringFieldPegawaiQJabatan	FieldNameJabatanSize2  TDateTimeFieldPegawaiQMulaiBekerja	FieldNameMulaiBekerja  TStringFieldPegawaiQNomorSIM	FieldNameNomorSIMSize2  TDateTimeFieldPegawaiQExpiredSIM	FieldName
ExpiredSIM  TBooleanFieldPegawaiQAktif	FieldNameAktif  
TMemoFieldPegawaiQKeterangan	FieldName
KeteranganBlobTypeftMemo  TStringFieldPegawaiQNoKTP	FieldNameNoKTPSize2   TSDQuery
PelangganQDatabaseNameDataOptions Active	SQL.Stringsselect * from Pelanggan Left�Top( TStringFieldPelangganQKode	FieldNameKodeRequired	Size
  TStringFieldPelangganQNamaPT	FieldNameNamaPTRequired	Sized  TStringFieldPelangganQAlamat	FieldNameAlamatSized  TStringFieldPelangganQKota	FieldNameKotaSize2  TStringFieldPelangganQNoTelp	FieldNameNoTelpSize2  TStringFieldPelangganQEmail	FieldNameEmailSize2  TStringFieldPelangganQNoFax	FieldNameNoFaxSize2  TStringFieldPelangganQNamaPIC1	FieldNameNamaPIC1Size2  TStringFieldPelangganQTelpPIC1	FieldNameTelpPIC1Size  TStringFieldPelangganQJabatanPIC1	FieldNameJabatanPIC1  TStringFieldPelangganQNamaPIC2	FieldNameNamaPIC2Size2  TStringFieldPelangganQTelpPIC2	FieldNameTelpPIC2Size  TStringFieldPelangganQJabatanPIC2	FieldNameJabatanPIC2  TStringFieldPelangganQNamaPIC3	FieldNameNamaPIC3Size2  TStringFieldPelangganQTelpPIC3	FieldNameTelpPIC3Size  TStringFieldPelangganQJabatanPIC3	FieldNameJabatanPIC3  TDateTimeFieldPelangganQCreateDate	FieldName
CreateDate  TStringFieldPelangganQCreateBy	FieldNameCreateBySize
  TStringFieldPelangganQOperator	FieldNameOperatorSize
  TDateTimeFieldPelangganQTglEntry	FieldNameTglEntry   TSDQuerySOQDatabaseNameDataOptions Active	SQL.Stringsselect * from MasterSO Left�Top� TStringFieldSOQKodenota	FieldNameKodenotaRequired	Size
  TDateTimeFieldSOQTgl	FieldNameTglRequired	  TStringFieldSOQPelanggan	FieldName	PelangganRequired	Size
  TDateTimeFieldSOQBerangkat	FieldName	Berangkat  TDateTimeFieldSOQTiba	FieldNameTiba  TCurrencyFieldSOQHarga	FieldNameHargaRequired	  TCurrencyFieldSOQPPN	FieldNamePPN  TCurrencyFieldSOQPembayaranAwal	FieldNamePembayaranAwal  TDateTimeFieldSOQTglPembayaranAwal	FieldNameTglPembayaranAwal  TStringFieldSOQCaraPembayaranAwal	FieldNameCaraPembayaranAwalSize2  TStringFieldSOQKeteranganCaraPembayaranAwal	FieldNameKeteranganCaraPembayaranAwalSize2  TStringFieldSOQNoKwitansiPembayaranAwal	FieldNameNoKwitansiPembayaranAwalSize2  TCurrencyField SOQNominalKwitansiPembayaranAwal	FieldNameNominalKwitansiPembayaranAwal  TStringFieldSOQPenerimaPembayaranAwal	FieldNamePenerimaPembayaranAwalSize
  TCurrencyFieldSOQPelunasan	FieldName	Pelunasan  TDateTimeFieldSOQTglPelunasan	FieldNameTglPelunasan  TStringFieldSOQCaraPembayaranPelunasan	FieldNameCaraPembayaranPelunasanSize2  TStringFieldSOQKetCaraPembayaranPelunasan	FieldNameKetCaraPembayaranPelunasanSize2  TStringFieldSOQNoKwitansiPelunasan	FieldNameNoKwitansiPelunasanSize2  TCurrencyFieldSOQNominalKwitansiPelunasan	FieldNameNominalKwitansiPelunasan  TStringFieldSOQPenerimaPelunasan	FieldNamePenerimaPelunasanSize
  TBooleanField	SOQExtend	FieldNameExtend  TDateTimeFieldSOQTglKembaliExtend	FieldNameTglKembaliExtend  TCurrencyFieldSOQBiayaExtend	FieldNameBiayaExtend  TCurrencyFieldSOQPPNExtend	FieldName	PPNExtend  TIntegerFieldSOQKapasitasSeat	FieldNameKapasitasSeat  TBooleanFieldSOQAC	FieldNameAC  TBooleanField	SOQToilet	FieldNameToilet  TBooleanFieldSOQAirSuspension	FieldNameAirSuspension  TStringFieldSOQRute	FieldNameRuteRequired	Size
  TDateTimeFieldSOQTglFollowUp	FieldNameTglFollowUp  TStringField	SOQArmada	FieldNameArmadaSize
  TStringField
SOQKontrak	FieldNameKontrakSize
  
TMemoFieldSOQPICJemput	FieldName	PICJemputBlobTypeftMemo  TDateTimeFieldSOQJamJemput	FieldName	JamJemput  TStringFieldSOQNoTelpPICJemput	FieldNameNoTelpPICJemputSize2  
TMemoFieldSOQAlamatJemput	FieldNameAlamatJemputBlobTypeftMemo  TStringField	SOQStatus	FieldNameStatusRequired	Size2  TStringFieldSOQStatusPembayaran	FieldNameStatusPembayaranSize2  TDateTimeFieldSOQReminderPending	FieldNameReminderPending  TStringFieldSOQPenerimaPending	FieldNamePenerimaPendingSize
  
TMemoFieldSOQKeterangan	FieldName
KeteranganBlobTypeftMemo  TDateTimeFieldSOQCreateDate	FieldName
CreateDate  TStringFieldSOQCreateBy	FieldNameCreateBySize
  TStringFieldSOQOperator	FieldNameOperatorSize
  TDateTimeFieldSOQTglEntry	FieldNameTglEntry  TDateTimeFieldSOQTglCetak	FieldNameTglCetak    