unit StoringDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TStoringDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    StoringQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    SJQ: TSDQuery;
    RuteQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    SJQKodenota: TStringField;
    SJQTgl: TDateTimeField;
    SJQNoSO: TStringField;
    SJQSopir: TStringField;
    SJQSopir2: TStringField;
    SJQCrew: TStringField;
    SJQTitipKwitansi: TBooleanField;
    SJQNominalKwitansi: TCurrencyField;
    SJQNoKwitansi: TStringField;
    SJQKir: TBooleanField;
    SJQSTNK: TBooleanField;
    SJQPajak: TBooleanField;
    SJQTglKembali: TDateTimeField;
    SJQPendapatan: TCurrencyField;
    SJQPengeluaran: TCurrencyField;
    SJQSisaDisetor: TCurrencyField;
    SJQSPBUAYaniLiter: TFloatField;
    SJQSPBUAYaniUang: TCurrencyField;
    SJQSPBULuarLiter: TFloatField;
    SJQSPBULuarUang: TCurrencyField;
    SJQSPBULuarUangDiberi: TCurrencyField;
    SJQSPBULuarDetail: TMemoField;
    SJQStatus: TStringField;
    SJQCreateDate: TDateTimeField;
    SJQCreateBy: TStringField;
    SJQOperator: TStringField;
    SJQTglEntry: TDateTimeField;
    SJQLaporan: TMemoField;
    SJQTglRealisasi: TDateTimeField;
    SJQAwal: TStringField;
    SJQAkhir: TStringField;
    SJQTglCetak: TDateTimeField;
    SJQClaimSopir: TCurrencyField;
    SJQKeteranganClaimSopir: TStringField;
    SJQPremiSopir: TCurrencyField;
    SJQPremiSopir2: TCurrencyField;
    SJQPremiKernet: TCurrencyField;
    SJQTabunganSopir: TCurrencyField;
    SJQTabunganSopir2: TCurrencyField;
    SJQTol: TCurrencyField;
    SJQUangJalan: TCurrencyField;
    SJQBiayaLainLain: TCurrencyField;
    SJQKeteranganBiayaLainLain: TStringField;
    SJQUangMakan: TCurrencyField;
    SJQUangInap: TCurrencyField;
    SJQUangParkir: TCurrencyField;
    SJQOther: TCurrencyField;
    PelangganQ: TSDQuery;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQEmail: TStringField;
    PelangganQNoFax: TStringField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Biaya: TcxGridDBColumn;
    cxGrid1DBTableView1JenisStoring: TcxGridDBColumn;
    cxGrid1DBTableView1TindakanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPengemudi: TcxGridDBColumn;
    cxGrid1DBTableView1Tgl: TcxGridDBColumn;
    SJQKeterangan: TMemoField;
    SJQSPBUAYaniJam: TDateTimeField;
    StoringQKode: TStringField;
    StoringQTanggal: TDateTimeField;
    StoringQSuratJalan: TStringField;
    StoringQArmada: TStringField;
    StoringQPengemudi: TStringField;
    StoringQKategoriRute: TStringField;
    StoringQBiaya: TCurrencyField;
    StoringQPICJemput: TStringField;
    StoringQJenisStoring: TMemoField;
    StoringQCreateDate: TDateTimeField;
    StoringQCreateBy: TStringField;
    StoringQOperator: TStringField;
    StoringQTglEntry: TDateTimeField;
    StoringQTindakanPerbaikan: TMemoField;
    StoringQTglCetak: TDateTimeField;
    StoringQNoBody: TStringField;
    StoringQPlatNo: TStringField;
    StoringQNamaPengemudi: TStringField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  StoringDropDownFm: TStoringDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TStoringDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TStoringDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=StoringQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TStoringDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=StoringQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TStoringDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=StoringQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TStoringDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=StoringQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TStoringDropDownFm.FormCreate(Sender: TObject);
begin
PelangganQ.Open;
ArmadaQ.Open;
PegawaiQ.Open;
SOQ.Open;
RuteQ.Open;
SJQ.Open;
StoringQ.Open;
end;

end.
