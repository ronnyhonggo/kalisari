unit SupplierDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TSupplierDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    SupplierQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQKategori: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaToko: TcxGridDBColumn;
    cxGrid1DBTableView1Alamat: TcxGridDBColumn;
    cxGrid1DBTableView1NoTelp: TcxGridDBColumn;
    cxGrid1DBTableView1Kategori: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPIC1: TcxGridDBColumn;
    cxGrid1DBTableView1TelpPIC1: TcxGridDBColumn;
    cxGrid1DBTableView1JabatanPIC1: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPIC2: TcxGridDBColumn;
    cxGrid1DBTableView1TelpPIC2: TcxGridDBColumn;
    cxGrid1DBTableView1JabatanPIC2: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPIC3: TcxGridDBColumn;
    cxGrid1DBTableView1TelpPIC3: TcxGridDBColumn;
    cxGrid1DBTableView1JabatanPIC3: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode,nama,jenissup,SupOriSQL:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  SupplierDropDownFm: TSupplierDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TSupplierDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  jenissup:=kd;
end;

constructor TSupplierDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TSupplierDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSupplierDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=SupplierQ.Fields[0].AsString;
  nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TSupplierDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SupplierQ.Fields[0].AsString;
  nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TSupplierDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=SupplierQ.Fields[0].AsString;
  nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;
end;

procedure TSupplierDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SupplierQ.Fields[0].AsString;
  nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TSupplierDropDownFm.FormShow(Sender: TObject);
begin
  SupOriSQL:=SupplierQ.SQL.Text;
  if jenissup='po' then
    begin
      SupplierQ.Close;
      SupplierQ.SQL.Text:='SELECT DISTINCT s.* FROM daftarbeli db, supplier s where db.status='+QuotedStr('ON PROCESS')+' and db.supplier=s.kode and  db.CashNCarry=0 and db.supplier<>'+QuotedStr('0000000000')+' order by s.namatoko';
      SupplierQ.ExecSQL;
      SupplierQ.Open;
    end
  else
    begin
      SupplierQ.Close;
      SupplierQ.SQL.Text:=SupOriSQL;
      SupplierQ.ExecSQL;
      SupplierQ.Open;
    end;
end;

end.
