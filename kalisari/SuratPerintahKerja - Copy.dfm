object SuratPerintahKerjaFm: TSuratPerintahKerjaFm
  Left = 183
  Top = 148
  Width = 1196
  Height = 635
  BorderIcons = [biSystemMenu]
  Caption = 'Surat Perintah Kerja'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1180
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxRadioButton1: TcxRadioButton
      Left = 168
      Top = 12
      Width = 113
      Height = 17
      Caption = 'Perbaikan'
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = cxRadioButton1Click
    end
    object cxRadioButton2: TcxRadioButton
      Left = 245
      Top = 14
      Width = 115
      Height = 13
      Caption = 'Perawatan'
      TabOrder = 2
      OnClick = cxRadioButton2Click
    end
    object cxRadioButton3: TcxRadioButton
      Left = 324
      Top = 12
      Width = 117
      Height = 17
      Caption = 'Rebuild'
      TabOrder = 3
      OnClick = cxRadioButton3Click
    end
    object cxRadioButton4: TcxRadioButton
      Left = 388
      Top = 12
      Width = 113
      Height = 17
      Caption = 'Lain-Lain'
      TabOrder = 4
      OnClick = cxRadioButton4Click
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 526
    Width = 1180
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 416
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object buttonCetak: TcxButton
      Left = 744
      Top = 10
      Width = 75
      Height = 25
      Caption = 'CETAK'
      Enabled = False
      TabOrder = 4
      OnClick = buttonCetakClick
    end
    object BtnCetak: TcxButton
      Left = 256
      Top = 10
      Width = 75
      Height = 25
      Caption = 'CETAK'
      TabOrder = 5
      OnClick = BtnCetakClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 882
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 6
      Height = 49
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 577
    Width = 1180
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 313
    Width = 1180
    Height = 213
    Align = alClient
    TabOrder = 3
    object Label4: TLabel
      Left = 1
      Top = 1
      Width = 1178
      Height = 25
      Align = alTop
      Caption = 'Surat Perintah Kerja :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 26
      Width = 1178
      Height = 186
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 66
        end
        object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
          DataBinding.FieldName = 'Tanggal'
          Width = 87
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'LaporanPerbaikan'
          Width = 93
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'LaporanPerawatan'
          Width = 96
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          DataBinding.FieldName = 'Rebuild'
          Width = 94
        end
        object cxGrid1DBTableView1LainLain: TcxGridDBColumn
          DataBinding.FieldName = 'LainLain'
          Width = 46
        end
        object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'PlatNo'
          Width = 89
        end
        object cxGrid1DBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Width = 56
        end
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          DataBinding.FieldName = 'NamaMekanik'
          Width = 112
        end
        object cxGrid1DBTableView1Column6: TcxGridDBColumn
          DataBinding.FieldName = 'DetailTindakan'
          Width = 310
        end
        object cxGrid1DBTableView1Column7: TcxGridDBColumn
          DataBinding.FieldName = 'WaktuMulai'
          Width = 70
        end
        object cxGrid1DBTableView1Column8: TcxGridDBColumn
          DataBinding.FieldName = 'WaktuSelesai'
          Width = 71
        end
        object cxGrid1DBTableView1Column9: TcxGridDBColumn
          DataBinding.FieldName = 'Status'
          Width = 137
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1180
    Height = 265
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 4
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 400
      Height = 263
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.ScrollBars = ssVertical
      OptionsView.RowHeaderWidth = 177
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      OnEnter = MasterVGridEnter
      OnExit = MasterVGridExit
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridDBEditorRow14: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Tanggal'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridLaporanPerbaikan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridLaporanPerbaikanEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'LaporanPerbaikan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridDBEditorRow13: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNoPerbaikan'
        Properties.Options.Editing = False
        ID = 2
        ParentID = 1
        Index = 0
        Version = 1
      end
      object MasterVGridNoBodyPerbaikan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBodyPerbaikan'
        Properties.Options.Editing = False
        ID = 3
        ParentID = 1
        Index = 1
        Version = 1
      end
      object MasterVGridAnalisaPerbaikan: TcxDBEditorRow
        Height = 67
        Properties.EditPropertiesClassName = 'TcxMemoProperties'
        Properties.DataBinding.FieldName = 'AnalisaPerbaikan'
        Properties.Options.Editing = False
        ID = 4
        ParentID = 1
        Index = 2
        Version = 1
      end
      object MasterVGridLaporanPerawatan: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridLaporanPerawatanEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'LaporanPerawatan'
        Visible = False
        ID = 5
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridDBEditorRow3: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBodyPerawatan'
        Properties.Options.Editing = False
        ID = 6
        ParentID = 5
        Index = 0
        Version = 1
      end
      object MasterVGridDBEditorRow6: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNoPerawatan'
        Properties.Options.Editing = False
        ID = 7
        ParentID = 5
        Index = 1
        Version = 1
      end
      object MasterVGridDBEditorRow7: TcxDBEditorRow
        Height = 65
        Properties.EditPropertiesClassName = 'TcxMemoProperties'
        Properties.DataBinding.FieldName = 'KeteranganPerawatan'
        Properties.Options.Editing = False
        ID = 8
        ParentID = 5
        Index = 2
        Version = 1
      end
      object MasterVGridRebuild: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridRebuildEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Rebuild'
        Visible = False
        ID = 9
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridDBEditorRow8: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'BarangRebuild'
        Properties.Options.Editing = False
        ID = 10
        ParentID = 9
        Index = 0
        Version = 1
      end
      object MasterVGridDBEditorRow9: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBodyDAR'
        Properties.Options.Editing = False
        ID = 11
        ParentID = 9
        Index = 1
        Version = 1
      end
      object MasterVGridDBEditorRow10: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNoDAR'
        Properties.Options.Editing = False
        ID = 12
        ParentID = 9
        Index = 2
        Version = 1
      end
      object MasterVGridDBEditorRow11: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoBodyKAR'
        Properties.Options.Editing = False
        ID = 13
        ParentID = 9
        Index = 3
        Version = 1
      end
      object MasterVGridDBEditorRow12: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'PlatNoKAR'
        Properties.Options.Editing = False
        ID = 14
        ParentID = 9
        Index = 4
        Version = 1
      end
      object MasterVGridLainLain: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'LainLain'
        Properties.Options.Editing = False
        Visible = False
        ID = 15
        ParentID = -1
        Index = 4
        Version = 1
      end
      object MasterVGridMekanik: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridMekanikEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'Mekanik'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 16
        ParentID = -1
        Index = 5
        Version = 1
      end
      object MasterVGridDBEditorRow1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaMekanik'
        Properties.Options.Editing = False
        ID = 17
        ParentID = 16
        Index = 0
        Version = 1
      end
      object MasterVGridDBEditorRow2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'JabatanMekanik'
        Properties.Options.Editing = False
        ID = 18
        ParentID = 16
        Index = 1
        Version = 1
      end
      object MasterVGridDetailTindakan: TcxDBEditorRow
        Height = 54
        Properties.DataBinding.FieldName = 'DetailTindakan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 19
        ParentID = -1
        Index = 6
        Version = 1
      end
      object MasterVGridWaktuMulai: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxDateEditProperties'
        Properties.EditProperties.Kind = ckDateTime
        Properties.DataBinding.FieldName = 'WaktuMulai'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 20
        ParentID = -1
        Index = 7
        Version = 1
      end
      object MasterVGridWaktuSelesai: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxDateEditProperties'
        Properties.EditProperties.Kind = ckDateTime
        Properties.DataBinding.FieldName = 'WaktuSelesai'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 21
        ParentID = -1
        Index = 8
        Version = 1
      end
      object MasterVGridStatus: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Items = <
          item
            Caption = 'ON PROCESS'
            Value = 'ON PROCESS'
          end
          item
            Caption = 'FINISHED COMPLETE'
            Value = 'FINISHED COMPLETE'
          end
          item
            Caption = 'FINISHED UNCOMPLETE'
            Value = 'FINISHED UNCOMPLETE'
          end>
        Properties.DataBinding.FieldName = 'Status'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 22
        ParentID = -1
        Index = 9
        Version = 1
      end
      object MasterVGridHasilKerja: TcxDBEditorRow
        Height = 35
        Properties.EditPropertiesClassName = 'TcxMemoProperties'
        Properties.DataBinding.FieldName = 'HasilKerja'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 23
        ParentID = -1
        Index = 10
        Version = 1
      end
      object MasterVGridKategoriPerbaikan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KategoriPerbaikan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 24
        ParentID = -1
        Index = 11
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Height = 57
        Properties.DataBinding.FieldName = 'Keterangan'
        Styles.Content = MenuUtamaFm.cxStyle5
        ID = 25
        ParentID = -1
        Index = 12
        Version = 1
      end
    end
    object Panel3: TPanel
      Left = 401
      Top = 1
      Width = 778
      Height = 263
      Align = alClient
      Caption = 'Panel3'
      TabOrder = 1
      object Panel5: TPanel
        Left = 391
        Top = 1
        Width = 326
        Height = 261
        Align = alCustom
        Caption = 'Panel5'
        TabOrder = 1
        Visible = False
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 324
          Height = 128
          Align = alTop
          Caption = 'Panel6'
          TabOrder = 0
          object Label2: TLabel
            Left = 1
            Top = 1
            Width = 322
            Height = 25
            Align = alTop
            Caption = 'Kanibal :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid4: TcxGrid
            Left = 1
            Top = 24
            Width = 322
            Height = 103
            Align = alBottom
            TabOrder = 0
            object cxGrid4DBTableView1: TcxGridDBTableView
              DataController.DataSource = KnbalDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Editing = False
              OptionsView.GroupByBox = False
              object cxGrid4DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'Kode'
              end
              object cxGrid4DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'Nama'
                Options.SortByDisplayText = isbtOn
                Width = 157
              end
              object cxGrid4DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'KeBarang'
              end
            end
            object cxGrid4Level1: TcxGridLevel
              GridView = cxGrid4DBTableView1
            end
          end
        end
        object Panel7: TPanel
          Left = 1
          Top = 128
          Width = 324
          Height = 132
          Align = alBottom
          Caption = 'Panel7'
          TabOrder = 1
          object Label3: TLabel
            Left = 1
            Top = 1
            Width = 322
            Height = 25
            Align = alTop
            Caption = 'Tukar Komponen :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cxGrid3: TcxGrid
            Left = 1
            Top = 24
            Width = 322
            Height = 107
            Align = alBottom
            TabOrder = 0
            object cxGrid3DBTableView1: TcxGridDBTableView
              DataController.DataSource = TKDs
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Editing = False
              OptionsView.GroupByBox = False
              object cxGrid3DBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'Kode'
              end
              object cxGrid3DBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'TglTukar'
              end
              object cxGrid3DBTableView1Column3: TcxGridDBColumn
                DataBinding.FieldName = 'Dari Armada'
              end
              object cxGrid3DBTableView1Column4: TcxGridDBColumn
                DataBinding.FieldName = 'Ke Armada'
              end
              object cxGrid3DBTableView1Column5: TcxGridDBColumn
                DataBinding.FieldName = 'Barang Dari'
              end
              object cxGrid3DBTableView1Column6: TcxGridDBColumn
                DataBinding.FieldName = 'Barang Ke'
              end
            end
            object cxGrid3Level1: TcxGridLevel
              GridView = cxGrid3DBTableView1
            end
          end
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 1
        Width = 776
        Height = 261
        Align = alClient
        Caption = 'Panel4'
        TabOrder = 0
        object Label1: TLabel
          Left = 1
          Top = 1
          Width = 774
          Height = 25
          Align = alTop
          Caption = 'Bon Barang :'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cxGrid2: TcxGrid
          Left = 1
          Top = 26
          Width = 774
          Height = 234
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            DataController.DataSource = BonDs
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Editing = False
            OptionsView.GroupByBox = False
            object cxGrid2DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'BonBarang'
              Width = 78
            end
            object cxGrid2DBTableView1Column4: TcxGridDBColumn
              DataBinding.FieldName = 'NamaBarang'
              Options.SortByDisplayText = isbtOn
              Width = 141
            end
            object cxGrid2DBTableView1Column2: TcxGridDBColumn
              DataBinding.FieldName = 'JumlahDiminta'
              Width = 75
            end
            object cxGrid2DBTableView1Column3: TcxGridDBColumn
              DataBinding.FieldName = 'JumlahKeluar'
              Width = 75
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from suratperintahkerja'
      '')
    UpdateObject = MasterUS
    Left = 561
    Top = 9
    object MasterQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      LookupCache = True
      Size = 10
    end
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      LookupCache = True
      Size = 10
    end
    object MasterQRebuild: TStringField
      FieldName = 'Rebuild'
      LookupCache = True
      Size = 10
    end
    object MasterQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object MasterQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object MasterQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object MasterQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQNamaMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 55
      Lookup = True
    end
    object MasterQJabatanMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'kode'
      LookupResultField = 'jabatan'
      KeyFields = 'mekanik'
      Size = 55
      Lookup = True
    end
    object MasterQKodeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeBarang'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barang'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object MasterQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'kode'
      LookupResultField = 'nama'
      KeyFields = 'kodebarang'
      Size = 50
      Lookup = True
    end
    object MasterQAnalisa: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Analisa'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'Rebuild'
      Size = 500
      Lookup = True
    end
    object MasterQTanggalMasuk: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalMasuk'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TanggalMasuk'
      KeyFields = 'Rebuild'
      Lookup = True
    end
    object MasterQTanggalPerbaikan: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerbaikan'
      Lookup = True
    end
    object MasterQTanggalPerawatan: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerawatan'
      LookupDataSet = PerawatanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerawatan'
      Lookup = True
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQPP: TStringField
      FieldKind = fkLookup
      FieldName = 'PP'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PP'
      KeyFields = 'LaporanPerbaikan'
      Size = 50
      Lookup = True
    end
    object MasterQArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'Armada'
      LookupDataSet = PPQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Armada'
      KeyFields = 'PP'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQNoBodyPerbaikan: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyPerbaikan'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQPlatNoPerbaikan: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoPerbaikan'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQAnalisaPerbaikan: TStringField
      FieldKind = fkLookup
      FieldName = 'AnalisaPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'LaporanPerbaikan'
      Size = 200
      Lookup = True
    end
    object MasterQArmadaPerawatan: TStringField
      FieldKind = fkLookup
      FieldName = 'ArmadaPerawatan'
      LookupDataSet = PerawatanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Armada'
      KeyFields = 'LaporanPerawatan'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQNoBodyPerawatan: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyPerawatan'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'ArmadaPerawatan'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQPlatNoPerawatan: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoPerawatan'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'ArmadaPerawatan'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQBarangRebuild: TStringField
      FieldKind = fkLookup
      FieldName = 'BarangRebuild'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barang'
      KeyFields = 'Rebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQDariArmadaRebuild: TStringField
      FieldKind = fkLookup
      FieldName = 'DariArmadaRebuild'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'DariArmada'
      KeyFields = 'Rebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQNoBodyDAR: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyDAR'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmadaRebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQPlatNoDAR: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoDAR'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'DariArmadaRebuild'
      Size = 50
      Lookup = True
    end
    object MasterQKeArmadaRebuild: TStringField
      FieldKind = fkLookup
      FieldName = 'KeArmadaRebuild'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'KeArmada'
      KeyFields = 'Rebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQNoBodyKAR: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyKAR'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmadaRebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQPlatNoKAR: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoKAR'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KeArmadaRebuild'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQKeteranganPerawatan: TStringField
      FieldKind = fkLookup
      FieldName = 'KeteranganPerawatan'
      LookupDataSet = PerawatanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Keterangan'
      KeyFields = 'LaporanPerawatan'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object MasterQPlatNo: TStringField
      FieldName = 'PlatNo'
    end
    object MasterQNoBody: TStringField
      FieldName = 'NoBody'
    end
    object MasterQLainLain: TBooleanField
      FieldName = 'LainLain'
    end
    object MasterQHasilKerja: TMemoField
      FieldName = 'HasilKerja'
      BlobType = ftMemo
    end
    object MasterQKategoriPerbaikan: TStringField
      FieldName = 'KategoriPerbaikan'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 594
    Top = 10
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, LaporanPerbaikan, LaporanPerawatan, Rebuil' +
        'd, LainLain, PlatNo, NoBody, Mekanik, DetailTindakan, WaktuMulai' +
        ', WaktuSelesai, Status, Keterangan, CreateDate, CreateBy, Operat' +
        'or, TglEntry, HasilKerja, KategoriPerbaikan'
      'from suratperintahkerja'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update suratperintahkerja'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  LaporanPerbaikan = :LaporanPerbaikan,'
      '  LaporanPerawatan = :LaporanPerawatan,'
      '  Rebuild = :Rebuild,'
      '  LainLain = :LainLain,'
      '  PlatNo = :PlatNo,'
      '  NoBody = :NoBody,'
      '  Mekanik = :Mekanik,'
      '  DetailTindakan = :DetailTindakan,'
      '  WaktuMulai = :WaktuMulai,'
      '  WaktuSelesai = :WaktuSelesai,'
      '  Status = :Status,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  HasilKerja = :HasilKerja,'
      '  KategoriPerbaikan = :KategoriPerbaikan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into suratperintahkerja'
      
        '  (Kode, Tanggal, LaporanPerbaikan, LaporanPerawatan, Rebuild, L' +
        'ainLain, PlatNo, NoBody, Mekanik, DetailTindakan, WaktuMulai, Wa' +
        'ktuSelesai, Status, Keterangan, CreateDate, CreateBy, Operator, ' +
        'TglEntry, HasilKerja, KategoriPerbaikan)'
      'values'
      
        '  (:Kode, :Tanggal, :LaporanPerbaikan, :LaporanPerawatan, :Rebui' +
        'ld, :LainLain, :PlatNo, :NoBody, :Mekanik, :DetailTindakan, :Wak' +
        'tuMulai, :WaktuSelesai, :Status, :Keterangan, :CreateDate, :Crea' +
        'teBy, :Operator, :TglEntry, :HasilKerja, :KategoriPerbaikan)')
    DeleteSQL.Strings = (
      'delete from suratperintahkerja'
      'where'
      '  Kode = :OLD_Kode')
    Left = 620
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from suratperintahkerja order by kode desc')
    Left = 533
    Top = 11
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada where kode=:text')
    Left = 745
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from supplier'
      '')
    Left = 777
    Top = 7
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNamaToko: TStringField
      FieldName = 'NamaToko'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Required = True
      Size = 200
    end
    object SupplierQNoTelp: TStringField
      FieldName = 'NoTelp'
      Required = True
      Size = 50
    end
    object SupplierQFax: TStringField
      FieldName = 'Fax'
      Size = 50
    end
    object SupplierQEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
    object SupplierQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object SupplierQStandarTermOfPayment: TStringField
      FieldName = 'StandarTermOfPayment'
      Size = 50
    end
    object SupplierQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object SupplierQNPWP: TStringField
      FieldName = 'NPWP'
      Size = 50
    end
    object SupplierQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Required = True
      Size = 50
    end
    object SupplierQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Required = True
      Size = 15
    end
    object SupplierQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
      Required = True
    end
    object SupplierQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object SupplierQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object SupplierQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object SupplierQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object SupplierQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object SupplierQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
    object SupplierQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SupplierQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SupplierQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SupplierQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object BarangQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 712
    Top = 8
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 10
    end
    object BarangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
    object BarangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
    end
    object BarangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
    end
    object BarangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Size = 50
    end
    object BarangQClaimNWarranty: TBooleanField
      FieldName = 'ClaimNWarranty'
    end
    object BarangQDurasiClaimNWarranty: TIntegerField
      FieldName = 'DurasiClaimNWarranty'
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQSingleSupplier: TBooleanField
      FieldName = 'SingleSupplier'
    end
    object BarangQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai'
      '')
    Left = 808
    Top = 7
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object RebuildQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from rebuild where kode=:text')
    Left = 841
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object RebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RebuildQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object RebuildQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object RebuildQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object RebuildQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object RebuildQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object RebuildQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object RebuildQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object RebuildQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object RebuildQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object RebuildQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object RebuildQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object RebuildQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object RebuildQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object RebuildQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object RebuildQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object RebuildQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object RebuildQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object RebuildQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object RebuildQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object RebuildQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RebuildQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RebuildQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RebuildQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
  object PerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from laporanperbaikan where kode=:text'
      ''
      '')
    Left = 873
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PerbaikanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PerbaikanQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object PerbaikanQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object PerbaikanQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      Required = True
      BlobType = ftMemo
    end
    object PerbaikanQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object PerbaikanQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object PerbaikanQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object PerbaikanQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
    end
    object PerbaikanQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Size = 10
    end
    object PerbaikanQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
    end
    object PerbaikanQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object PerbaikanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PerbaikanQClaimSopir: TCurrencyField
      FieldName = 'ClaimSopir'
    end
    object PerbaikanQSopirClaim: TStringField
      FieldName = 'SopirClaim'
      Size = 10
    end
    object PerbaikanQKeteranganClaimSopir: TStringField
      FieldName = 'KeteranganClaimSopir'
      Size = 50
    end
    object PerbaikanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PerbaikanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PerbaikanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PerbaikanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PerbaikanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PerbaikanQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object PerawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from laporanperawatan where kode=:text'
      ''
      '')
    Left = 905
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PerawatanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PerawatanQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PerawatanQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object PerawatanQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object PerawatanQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object PerawatanQButuhPerbaikan: TBooleanField
      FieldName = 'ButuhPerbaikan'
    end
    object PerawatanQKmArmadaSekarang: TIntegerField
      FieldName = 'KmArmadaSekarang'
      Required = True
    end
    object PerawatanQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object PerawatanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PerawatanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PerawatanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PerawatanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PerawatanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PerawatanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PerawatanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(tanggal as date) as Tgl,* '
      'from suratperintahkerja'
      'where (Tanggal>=:text1 and Tanggal<=:text2) or tanggal is null'
      'order by tglentry desc')
    Left = 649
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object StringField10: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 55
      Lookup = True
    end
    object StringField11: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'kode'
      LookupResultField = 'jabatan'
      KeyFields = 'mekanik'
      Size = 55
      Lookup = True
    end
    object StringField12: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeBarang'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barang'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object StringField13: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'kode'
      LookupResultField = 'nama'
      KeyFields = 'kodebarang'
      Size = 50
      Lookup = True
    end
    object WideStringField1: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Analisa'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'Rebuild'
      Size = 500
      Lookup = True
    end
    object DateTimeField5: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalMasuk'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TanggalMasuk'
      KeyFields = 'Rebuild'
      Lookup = True
    end
    object DateTimeField6: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerbaikan'
      Lookup = True
    end
    object DateTimeField7: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerawatan'
      LookupDataSet = PerawatanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerawatan'
      Lookup = True
    end
    object WideStringField2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'AnalisaPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'LaporanPerbaikan'
      Size = 500
      Lookup = True
    end
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object ViewQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object ViewQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object ViewQRebuild: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object ViewQMekanik: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object ViewQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object ViewQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object ViewQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object ViewQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQPlatNo: TStringField
      FieldName = 'PlatNo'
    end
    object ViewQNoBody: TStringField
      FieldName = 'NoBody'
    end
    object ViewQLainLain: TBooleanField
      FieldName = 'LainLain'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewQ
    Left = 676
    Top = 6
  end
  object VBonQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select bb.kode as BonBarang, b.nama as NamaBarang, dbb.JumlahDim' +
        'inta, (select sum(dbkb.jumlahkeluar) from bonkeluarbarang bkb, d' +
        'etailbkb dbkb where bkb.kode=dbkb.kodebkb AND bkb.bonbarang = bb' +
        '.kode AND dbb.KodeBonBarang=bb.Kode AND dbkb.Barang=dbb.KodeBara' +
        'ng) as JumlahKeluar'
      'from bonbarang bb, barang b, detailbonbarang dbb'
      
        'where bb.spk = :text AND dbb.kodebonbarang = bb.kode AND b.kode ' +
        '= dbb.kodebarang')
    Left = 937
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VBonQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object VBonQNamaBarang: TStringField
      FieldName = 'NamaBarang'
      Required = True
      Size = 50
    end
    object VBonQJumlahDiminta: TIntegerField
      FieldName = 'JumlahDiminta'
      Required = True
    end
    object VBonQJumlahKeluar: TIntegerField
      FieldName = 'JumlahKeluar'
    end
  end
  object BonDs: TDataSource
    DataSet = VBonQ
    Left = 964
    Top = 6
  end
  object VKnbalQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select abk.Kode, b.Nama, abk.barang'
      'from ambilbarangkanibal abk, barangkanibal bk, barang b'
      
        'where abk.spk = :text and bk.kode=abk.daribarang and b.kode = bk' +
        '.barang')
    Left = 1001
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VKnbalQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VKnbalQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object VKnbalQbarang: TStringField
      FieldName = 'barang'
      Size = 10
    end
    object VKnbalQKeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KeBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'barang'
      Size = 50
      Lookup = True
    end
  end
  object KnbalDs: TDataSource
    DataSet = VKnbalQ
    Left = 1028
    Top = 6
  end
  object VTKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *'
      'from tukarkomponen tk'
      'where tk.spk = :text')
    Left = 1057
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object VTKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VTKQDariArmada: TStringField
      FieldName = 'DariArmada'
      Required = True
      Size = 10
    end
    object VTKQKeArmada: TStringField
      FieldName = 'KeArmada'
      Required = True
      Size = 10
    end
    object VTKQBarangDari: TStringField
      FieldName = 'BarangDari'
      Required = True
      Size = 10
    end
    object VTKQBarangKe: TStringField
      FieldName = 'BarangKe'
      Size = 10
    end
    object VTKQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
      Required = True
    end
    object VTKQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object VTKQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object VTKQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object VTKQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object VTKQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object VTKQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object VTKQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object VTKQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object VTKQDariArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Dari Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object VTKQKeArmada2: TStringField
      FieldKind = fkLookup
      FieldName = 'Ke Armada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
    object VTKQBarangDari2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Dari'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangDari'
      Size = 50
      Lookup = True
    end
    object VTKQBarangKe2: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang Ke'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangKe'
      Size = 50
      Lookup = True
    end
  end
  object TKDs: TDataSource
    DataSet = VTKQ
    Left = 1084
    Top = 6
  end
  object PPQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select *'
      'from permintaanperbaikan'
      'where kode=:text')
    Left = 1121
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 2
    Version.Windows.Build = '9200'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 446
    Top = 12
  end
  object UpdateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from suratperintahkerja'
      '')
    Left = 503
    Top = 11
    object StringField1: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 55
      Lookup = True
    end
    object StringField2: TStringField
      FieldKind = fkLookup
      FieldName = 'JabatanMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'kode'
      LookupResultField = 'jabatan'
      KeyFields = 'mekanik'
      Size = 55
      Lookup = True
    end
    object StringField3: TStringField
      FieldKind = fkLookup
      FieldName = 'KodeBarang'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barang'
      KeyFields = 'Rebuild'
      Size = 10
      Lookup = True
    end
    object StringField4: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'kode'
      LookupResultField = 'nama'
      KeyFields = 'kodebarang'
      Size = 50
      Lookup = True
    end
    object WideStringField3: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Analisa'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'Rebuild'
      Size = 500
      Lookup = True
    end
    object DateTimeField1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalMasuk'
      LookupDataSet = RebuildQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TanggalMasuk'
      KeyFields = 'Rebuild'
      Lookup = True
    end
    object DateTimeField2: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerbaikan'
      Lookup = True
    end
    object DateTimeField3: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalPerawatan'
      LookupDataSet = PerawatanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tanggal'
      KeyFields = 'LaporanPerawatan'
      Lookup = True
    end
    object WideStringField4: TWideStringField
      FieldKind = fkLookup
      FieldName = 'AnalisaPerbaikan'
      LookupDataSet = PerbaikanQ
      LookupKeyFields = 'kode'
      LookupResultField = 'AnalisaMasalah'
      KeyFields = 'LaporanPerbaikan'
      Size = 500
      Lookup = True
    end
    object StringField5: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'Tanggal'
    end
    object StringField6: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object StringField7: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object StringField8: TStringField
      FieldName = 'Rebuild'
      Size = 10
    end
    object StringField9: TStringField
      FieldName = 'Mekanik'
      Size = 10
    end
    object MemoField1: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object DateTimeField8: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object DateTimeField9: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object StringField14: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object StringField15: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object DateTimeField10: TDateTimeField
      FieldName = 'CreateDate'
    end
    object StringField16: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object StringField17: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object DateTimeField11: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from armada where kode like '#39'%'#39' + :text + '#39'%'#39' or platno' +
        ' like '#39'%'#39' + :text + '#39'%'#39
      ''
      '')
    Left = 472
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object updateQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object updateQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object updateQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object updateQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object updateQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object updateQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object updateQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object updateQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object updateQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object updateQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object updateQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object updateQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object updateQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object updateQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object updateQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object updateQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object updateQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object updateQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object updateQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object updateQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object updateQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object updateQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object updateQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object updateQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object updateQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object updateQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object updateQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object updateQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
end
