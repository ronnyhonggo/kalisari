unit SuratPerintahKerja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxClasses, cxMRUEdit, cxLabel,
  cxCheckBox, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid,
  cxDropDownEdit, UCrpeClasses, UCrpe32, cxMemo, cxGroupBox;

type
  TSuratPerintahKerjaFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    ArmadaQ: TSDQuery;
    SupplierQ: TSDQuery;
    BarangQ: TSDQuery;
    cxLabel1: TcxLabel;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQSatuan: TStringField;
    BarangQMinimumStok: TIntegerField;
    j: TIntegerField;
    BarangQStandardUmur: TIntegerField;
    BarangQLokasi: TStringField;
    BarangQClaimNWarranty: TBooleanField;
    BarangQDurasiClaimNWarranty: TIntegerField;
    BarangQKategori: TStringField;
    BarangQSingleSupplier: TBooleanField;
    BarangQHarga: TCurrencyField;
    BarangQKeterangan: TMemoField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQOperator: TStringField;
    BarangQTglEntry: TDateTimeField;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    SupplierQKode: TStringField;
    SupplierQNamaToko: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQNoTelp: TStringField;
    SupplierQFax: TStringField;
    SupplierQEmail: TStringField;
    SupplierQKategori: TStringField;
    SupplierQStandarTermOfPayment: TStringField;
    SupplierQCashNCarry: TBooleanField;
    SupplierQNPWP: TStringField;
    SupplierQNamaPIC1: TStringField;
    SupplierQTelpPIC1: TStringField;
    SupplierQJabatanPIC1: TStringField;
    SupplierQNamaPIC2: TStringField;
    SupplierQTelpPIC2: TStringField;
    SupplierQJabatanPIC2: TStringField;
    SupplierQNamaPIC3: TStringField;
    SupplierQTelpPIC3: TStringField;
    SupplierQJabatanPIC3: TStringField;
    SupplierQCreateDate: TDateTimeField;
    SupplierQCreateBy: TStringField;
    SupplierQOperator: TStringField;
    SupplierQTglEntry: TDateTimeField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    Panel1: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    MasterQKode: TStringField;
    MasterQLaporanPerbaikan: TStringField;
    MasterQLaporanPerawatan: TStringField;
    MasterQRebuild: TStringField;
    MasterQMekanik: TStringField;
    MasterQDetailTindakan: TMemoField;
    MasterQWaktuMulai: TDateTimeField;
    MasterQWaktuSelesai: TDateTimeField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    MasterQNamaMekanik: TStringField;
    MasterQJabatanMekanik: TStringField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    PerbaikanQ: TSDQuery;
    PerbaikanQKode: TStringField;
    PerbaikanQPP: TStringField;
    PerbaikanQAnalisaMasalah: TMemoField;
    PerbaikanQTindakanPerbaikan: TMemoField;
    PerbaikanQWaktuMulai: TDateTimeField;
    PerbaikanQWaktuSelesai: TDateTimeField;
    PerbaikanQVerifikator: TStringField;
    PerbaikanQTglSerahTerima: TDateTimeField;
    PerbaikanQPICSerahTerima: TStringField;
    PerbaikanQKmArmadaSekarang: TIntegerField;
    PerbaikanQStatus: TStringField;
    PerbaikanQKeterangan: TMemoField;
    PerbaikanQClaimSopir: TCurrencyField;
    PerbaikanQSopirClaim: TStringField;
    PerbaikanQKeteranganClaimSopir: TStringField;
    PerbaikanQCreateDate: TDateTimeField;
    PerbaikanQCreateBy: TStringField;
    PerbaikanQOperator: TStringField;
    PerbaikanQTglEntry: TDateTimeField;
    PerbaikanQTglCetak: TDateTimeField;
    PerawatanQ: TSDQuery;
    MasterQKodeBarang: TStringField;
    MasterQNamaBarang: TStringField;
    MasterQAnalisa: TWideStringField;
    PerbaikanQTanggal: TDateTimeField;
    PerawatanQKode: TStringField;
    PerawatanQTanggal: TDateTimeField;
    PerawatanQWaktuMulai: TDateTimeField;
    PerawatanQWaktuSelesai: TDateTimeField;
    PerawatanQVerifikator: TStringField;
    PerawatanQButuhPerbaikan: TBooleanField;
    PerawatanQKmArmadaSekarang: TIntegerField;
    PerawatanQStatus: TStringField;
    PerawatanQKeterangan: TMemoField;
    PerawatanQCreateDate: TDateTimeField;
    PerawatanQCreateBy: TStringField;
    PerawatanQOperator: TStringField;
    PerawatanQTglEntry: TDateTimeField;
    PerawatanQTglCetak: TDateTimeField;
    PerawatanQArmada: TStringField;
    MasterQTanggalMasuk: TDateTimeField;
    MasterQTanggalPerbaikan: TDateTimeField;
    MasterQTanggalPerawatan: TDateTimeField;
    ViewQ: TSDQuery;
    StringField10: TStringField;
    StringField11: TStringField;
    WideStringField1: TWideStringField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    DateTimeField7: TDateTimeField;
    WideStringField2: TWideStringField;
    ViewDs: TDataSource;
    MasterQTanggal: TDateTimeField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxGrid1DBTableView1Column9: TcxGridDBColumn;
    Panel2: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    MasterVGridLaporanPerbaikan: TcxDBEditorRow;
    MasterVGridLaporanPerawatan: TcxDBEditorRow;
    MasterVGridRebuild: TcxDBEditorRow;
    MasterVGridMekanik: TcxDBEditorRow;
    MasterVGridDetailTindakan: TcxDBEditorRow;
    MasterVGridWaktuMulai: TcxDBEditorRow;
    MasterVGridWaktuSelesai: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    VBonQ: TSDQuery;
    BonDs: TDataSource;
    ViewQKode: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQLaporanPerbaikan: TStringField;
    ViewQLaporanPerawatan: TStringField;
    ViewQRebuild: TStringField;
    ViewQMekanik: TStringField;
    ViewQDetailTindakan: TMemoField;
    ViewQWaktuMulai: TDateTimeField;
    ViewQWaktuSelesai: TDateTimeField;
    ViewQStatus: TStringField;
    ViewQKeterangan: TStringField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    VKnbalQ: TSDQuery;
    KnbalDs: TDataSource;
    VKnbalQKode: TStringField;
    VKnbalQNama: TStringField;
    VKnbalQbarang: TStringField;
    VKnbalQKeBarang: TStringField;
    cxGrid4DBTableView1Column1: TcxGridDBColumn;
    cxGrid4DBTableView1Column2: TcxGridDBColumn;
    cxGrid4DBTableView1Column3: TcxGridDBColumn;
    VTKQ: TSDQuery;
    TKDs: TDataSource;
    VTKQKode: TStringField;
    VTKQDariArmada: TStringField;
    VTKQKeArmada: TStringField;
    VTKQBarangDari: TStringField;
    VTKQBarangKe: TStringField;
    VTKQTglTukar: TDateTimeField;
    VTKQPeminta: TStringField;
    VTKQSPK: TStringField;
    VTKQLaporanPerbaikan: TStringField;
    VTKQCreateDate: TDateTimeField;
    VTKQCreateBy: TStringField;
    VTKQOperator: TStringField;
    VTKQTglEntry: TDateTimeField;
    VTKQTglCetak: TDateTimeField;
    VTKQDariArmada2: TStringField;
    VTKQKeArmada2: TStringField;
    VTKQBarangDari2: TStringField;
    VTKQBarangKe2: TStringField;
    cxGrid3DBTableView1Column1: TcxGridDBColumn;
    cxGrid3DBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBTableView1Column3: TcxGridDBColumn;
    cxGrid3DBTableView1Column4: TcxGridDBColumn;
    cxGrid3DBTableView1Column5: TcxGridDBColumn;
    cxGrid3DBTableView1Column6: TcxGridDBColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    MasterQPP: TStringField;
    PPQ: TSDQuery;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQArmada: TStringField;
    PPQPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    MasterQArmada: TStringField;
    MasterQArmadaPerawatan: TStringField;
    MasterQDariArmadaRebuild: TStringField;
    MasterQKeArmadaRebuild: TStringField;
    MasterVGridDBEditorRow6: TcxDBEditorRow;
    MasterQKeteranganPerawatan: TStringField;
    MasterVGridDBEditorRow7: TcxDBEditorRow;
    MasterVGridDBEditorRow8: TcxDBEditorRow;
    MasterVGridDBEditorRow9: TcxDBEditorRow;
    MasterVGridDBEditorRow10: TcxDBEditorRow;
    MasterVGridDBEditorRow11: TcxDBEditorRow;
    MasterVGridDBEditorRow12: TcxDBEditorRow;
    MasterVGridDBEditorRow13: TcxDBEditorRow;
    MasterQAnalisaPerbaikan: TStringField;
    MasterVGridDBEditorRow14: TcxDBEditorRow;
    buttonCetak: TcxButton;
    Crpe1: TCrpe;
    UpdateQ: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    WideStringField3: TWideStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    WideStringField4: TWideStringField;
    StringField5: TStringField;
    DateTimeField4: TDateTimeField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    MemoField1: TMemoField;
    DateTimeField8: TDateTimeField;
    DateTimeField9: TDateTimeField;
    StringField14: TStringField;
    StringField15: TStringField;
    DateTimeField10: TDateTimeField;
    StringField16: TStringField;
    StringField17: TStringField;
    DateTimeField11: TDateTimeField;
    SDQuery1: TSDQuery;
    updateQKode: TStringField;
    updateQPlatNo: TStringField;
    updateQJumlahSeat: TIntegerField;
    updateQTahunPembuatan: TStringField;
    updateQNoBody: TStringField;
    updateQJenisAC: TStringField;
    updateQJenisBBM: TStringField;
    updateQKapasitasTangkiBBM: TIntegerField;
    updateQLevelArmada: TStringField;
    updateQJumlahBan: TIntegerField;
    updateQAktif: TBooleanField;
    updateQAC: TBooleanField;
    updateQToilet: TBooleanField;
    updateQAirSuspension: TBooleanField;
    updateQKmSekarang: TIntegerField;
    updateQKeterangan: TStringField;
    updateQSopir: TStringField;
    updateQJenisKendaraan: TStringField;
    updateQCreateDate: TDateTimeField;
    updateQCreateBy: TStringField;
    updateQOperator: TStringField;
    updateQTglEntry: TDateTimeField;
    updateQSTNKPajakExpired: TDateTimeField;
    updateQSTNKPerpanjangExpired: TDateTimeField;
    updateQKirMulai: TDateTimeField;
    updateQKirSelesai: TDateTimeField;
    updateQNoRangka: TStringField;
    updateQNoMesin: TStringField;
    BtnCetak: TcxButton;
    VBonQBonBarang: TStringField;
    VBonQNamaBarang: TStringField;
    MasterVGridNoBodyPerbaikan: TcxDBEditorRow;
    MasterVGridAnalisaPerbaikan: TcxDBEditorRow;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    MasterQPlatNo: TStringField;
    MasterQNoBody: TStringField;
    ViewQPlatNo: TStringField;
    ViewQNoBody: TStringField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    cxGrid1DBTableView1NoBody: TcxGridDBColumn;
    cxRadioButton4: TcxRadioButton;
    MasterQLainLain: TBooleanField;
    MasterVGridLainLain: TcxDBEditorRow;
    ViewQLainLain: TBooleanField;
    cxGrid1DBTableView1LainLain: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQHasilKerja: TMemoField;
    MasterQKategoriPerbaikan: TStringField;
    MasterVGridHasilKerja: TcxDBEditorRow;
    MasterVGridKategoriPerbaikan: TcxDBEditorRow;
    MasterQBarangRebuild: TStringField;
    MasterQNoBodyDAR: TStringField;
    MasterQPlatNoDAR: TStringField;
    MasterQNoBodyKAR: TStringField;
    MasterQPlatNoKAR: TStringField;
    MasterQNoBodyPerbaikan: TStringField;
    MasterQPlatNoPerbaikan: TStringField;
    MasterQNoBodyPerawatan: TStringField;
    MasterQPlatNoPerawatan: TStringField;
    VBonQJumlahDiminta: TFloatField;
    VBonQJumlahKeluar: TFloatField;
    ArmadaQLayoutBan: TStringField;
    ArmadaQRekanan: TBooleanField;
    ArmadaQKodeRekanan: TStringField;
    BarangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridDariArmadaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKeArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPICKirimEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPenerimaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridVerifikatorEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure cxRadioButton2Click(Sender: TObject);
    procedure cxRadioButton3Click(Sender: TObject);
    procedure MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridMekanikEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridLaporanPerawatanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridRebuildEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure BtnCetakClick(Sender: TObject);
    procedure cxRadioButton4Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  SuratPerintahKerjaFm: TSuratPerintahKerjaFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, Math, StrUtils,
  LaporanPerbaikanDropDown, PegawaiDropDown, LaporanPerawatanDropDown,
  RebuildDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TSuratPerintahKerjaFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TSuratPerintahKerjaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TSuratPerintahKerjaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TSuratPerintahKerjaFm.ExitBtnClick(Sender: TObject);
begin
  close;
  Release;
end;

procedure TSuratPerintahKerjaFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  PerbaikanQ.Close;
  PerbaikanQ.ParamByName('text').AsString:='';
  PerbaikanQ.Open;
  PerawatanQ.Close;
  PerawatanQ.ParamByName('text').AsString:='';
  PerawatanQ.Open;
  RebuildQ.Close;
  RebuildQ.ParamByName('text').AsString:='';
  RebuildQ.Open;
  PPQ.Close;
  PPQ.ParamByName('text').AsString:='';
  PPQ.Open;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:='';
  ArmadaQ.Open;
//  ArmadaQ.Open;
  PegawaiQ.Open;

//  PPQ.Open;
  BarangQ.Close;
  BarangQ.ParamByName('text').AsString:='';
  BarangQ.Open;
//  SupplierQ.Open;
end;

procedure TSuratPerintahKerjaFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  cxRadioButton1.Enabled:=true;
  cxRadioButton2.Enabled:=true;
  cxRadioButton3.Enabled:=true;
end;

procedure TSuratPerintahKerjaFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TSuratPerintahKerjaFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertSPK.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSPK.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteSPK.AsBoolean;
  //cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TSuratPerintahKerjaFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    Masterq.Close;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      MasterQStatus.AsString:='ON PROCESS';
    end
  else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      MasterQ.Edit;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSPK.AsBoolean;
      
      PerbaikanQ.Close;
      PerbaikanQ.ParamByName('text').AsString:=MasterQLaporanPerbaikan.AsString;
      PerbaikanQ.Open;
      PerawatanQ.Close;
      PerawatanQ.ParamByName('text').AsString:=MasterQLaporanPerawatan.AsString;
      PerawatanQ.Open;
      RebuildQ.Close;
      RebuildQ.ParamByName('text').AsString:=MasterQRebuild.AsString;
      RebuildQ.Open;
    end;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateSPK.AsBoolean;
  MasterVGrid.Enabled:=True;
  end;

end;

procedure TSuratPerintahKerjaFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TSuratPerintahKerjaFm.SaveBtnClick(Sender: TObject);
var mspk,kode:string;
begin
  if cxRadioButton1.Checked then
    mspk:='Perbaikan'
  else if cxRadioButton2.Checked then
      mspk:='Perawatan'
  else
    mspk:='Rebuild';
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;

  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    kode:=MasterQKode.AsString;
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage(mspk+' dengan Kode ' + KodeEdit.Text + ' telah disimpan');

       //cetak SJ
    if MessageDlg('Cetak SPK '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'SPK.rpt';
      Crpe1.ParamFields[0].CurrentValue:=kode;
{      if cxRadioButton1.Checked=true then
      begin
        ShowMessage(MasterQNoBodyPerbaikan.AsString+' '+MasterQPlatNoPerbaikan.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyPerbaikan.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoPerbaikan.AsString;
      end

      else if cxRadioButton2.Checked=true then
      begin
        ShowMessage(MasterQNoBodyPerawatan.AsString+' '+MasterQPlatNoPerawatan.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyPerawatan.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoPerawatan.AsString;
      end

      else
      begin
        ShowMessage(MasterQNoBodyDAR.AsString+' '+MasterQPlatNoDAR.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyDAR.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoDAR.AsString;
      end;            }
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
    end;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  ViewQ.Close;
  ViewQ.Open;
  //cxButtonEdit1PropertiesButtonClick(sender,0);
  VBonQ.Close;
  VBonQ.ParamByName('text').AsString:='';
  VBonQ.Open;
  vbonq.Close;
  VKnbalQ.Close;
  VKnbalQ.ParamByName('text').AsString:='';
  VKnbalQ.Open;
  VKnbalQ.Close;
  VTKQ.Close;
  VTKQ.ParamByName('text').AsString:='';
  VTKQ.Open;
  VTKQ.Close;
  MasterQ.Close;
end;

procedure TSuratPerintahKerjaFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;}
end;

procedure TSuratPerintahKerjaFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TSuratPerintahKerjaFm.DeleteBtnClick(Sender: TObject);
var mspk:string;
begin
    if MasterQLaporanPerbaikan.AsString <> '' then
    mspk:='Perbaikan '
    else if MasterQLaporanPerawatan.AsString <> '' then
    mspk:='Perawatan'
    else
    mspk:='Rebuild';
 // ShowMessage(mspk);
  if MessageDlg('Hapus '+mspk+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg(mspk+'telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('SPK pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteSPK.AsBoolean;
     viewQ.Close;
     viewQ.Open;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TSuratPerintahKerjaFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TSuratPerintahKerjaFm.MasterVGridEnter(Sender: TObject);
begin
 // mastervgrid.FocusRow(MasterVGridMerk);
end;

procedure TSuratPerintahKerjaFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQBarang.AsString:=BarangQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridDariArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQDariArmada.AsString:=ArmadaQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridKeArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQKeArmada.AsString:=ArmadaQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridPICKirimEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQPICKirim.AsString:=PegawaiQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridPenerimaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQPenerima.AsString:=PegawaiQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.MasterVGridVerifikatorEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQVerifikator.AsString:=PegawaiQKode.AsString;
    end;
    DropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.cxRadioButton1Click(Sender: TObject);
begin
  if cxRadioButton1.Checked then
  begin
    MasterVGridLaporanPerbaikan.Visible:=true;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridRebuild.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQLaporanPerbaikan.Visible:=true;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQRebuild.Visible:=false;
    MasterQLainLain.Visible:=false;
    MasterQ.open;
    MasterQ.edit;
    MasterQLaporanPerawatan.AsVariant:=null;
    MasterQRebuild.AsVariant:=null;
    MasterQLainLain.AsVariant:=null;
  end;
end;

procedure TSuratPerintahKerjaFm.cxRadioButton2Click(Sender: TObject);
begin
  if cxRadioButton2.Checked then
  begin
    MasterVGridLaporanPerawatan.Visible:=true;
    mastervgridlaporanperbaikan.visible:=false;
    mastervgridrebuild.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQLaporanPerawatan.Visible:=true;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQRebuild.Visible:=false;
    MasterQLainLain.Visible:=false;
    MasterQ.edit;
    MasterQLaporanPerbaikan.AsVariant:=null;
    MasterQRebuild.AsVariant:=null;
    MasterQLainLain.AsVariant:=null;
  end;
end;

procedure TSuratPerintahKerjaFm.cxRadioButton3Click(Sender: TObject);
begin
  if cxRadioButton3.Checked then
  begin
    MasterVGridRebuild.Visible:=true;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridLaporanPerbaikan.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQRebuild.Visible:=true;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQLainLain.Visible:=false;
    MasterQ.edit;
    MasterQLaporanPerawatan.AsVariant:=null;
    MasterQLaporanPerbaikan.AsVariant:=null;
    MasterQLainLain.AsVariant:=null;
  end;
end;

procedure TSuratPerintahKerjaFm.MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LaporanPerbaikanDropDownFm:=TLaporanPerbaikanDropdownfm.Create(Self);
  if LaporanPerbaikanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    PerbaikanQ.Close;
    PerbaikanQ.ParamByName('text').AsString:=LaporanPerbaikanDropDownFm.kode;
    PerbaikanQ.Open;
    MasterQLaporanPerbaikan.AsString:= LaporanPerbaikanDropDownFm.kode;
    PPQ.Close;
    PPQ.ParamByName('text').AsString:=PerbaikanQPP.AsString;
    PPQ.Open;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=PPQArmada.AsString;
    ArmadaQ.Open;
    MasterQPlatNoPerbaikan.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBodyPerbaikan.AsString:=ArmadaQNoBody.AsString;
  end;
  LaporanPerbaikanDropDownFm.Release;
  MasterQPlatNo.AsString:=MasterQPlatNoPerbaikan.AsString;
  MasterQNoBody.AsString:=MasterQNoBodyPerbaikan.AsString;
end;

procedure TSuratPerintahKerjaFm.MasterVGridMekanikEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self,'mekanik');
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQMekanik.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TSuratPerintahKerjaFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
   //buttonCetak.Enabled:=true;
  kodeedit.Text:=ViewQKode.AsString;
  KodeEditExit(self);

  if MasterQLaporanPerbaikan.AsString <> '' then
  begin
    MasterVGridLaporanPerbaikan.Visible:=true;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridRebuild.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQLaporanPerbaikan.Visible:=true;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQRebuild.Visible:=false;
    MasterQLainLain.Visible:=false;
    cxRadioButton1.Checked:=True;
  end
  else if MasterQLaporanPerawatan.AsString <> '' then
  begin
    MasterVGridLaporanPerawatan.Visible:=true;
    mastervgridlaporanperbaikan.visible:=false;
    mastervgridrebuild.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQLaporanPerawatan.Visible:=true;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQRebuild.Visible:=false;
    MasterQLainLain.Visible:=false;
    cxRadioButton2.Checked:=True;
  end
  else if MasterQRebuild.AsString <> '' then
  begin
    MasterVGridRebuild.Visible:=true;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridLaporanPerbaikan.Visible:=false;
    MasterVGridLainLain.Visible:=false;
    MasterQRebuild.Visible:=true;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQLainLain.Visible:=false;
    cxRadioButton3.Checked:=True;
  end
  else if MasterQLainLain.AsBoolean = True then
  begin
    MasterVGridRebuild.Visible:=false;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridLaporanPerbaikan.Visible:=false;
    MasterVGridLainLain.Visible:=true;
    MasterQRebuild.Visible:=false;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQLainLain.Visible:=true;
    cxRadioButton4.Checked:=True;
  end;

  SaveBtn.Enabled:=menuutamafm.UserQUpdateSPK.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteSPK.AsBoolean;
  MasterVGrid.Enabled:=True;
  MasterQ.Edit;

  PerbaikanQ.Close;
  PerbaikanQ.ParamByName('text').AsString:=MasterQLaporanPerbaikan.AsString;
  PerbaikanQ.Open;
  MasterQPP.AsString:=PerbaikanQPP.AsString;
  PPQ.Close;
  PPQ.ParamByName('text').AsString:=MasterQPP.AsString;
  PPQ.Open;
  MasterQArmada.AsString:=PPQArmada.AsString;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:=PPQArmada.AsString;
  ArmadaQ.Open;
  MasterQPlatNoPerbaikan.AsString:=ArmadaQPlatNo.AsString;
  MasterQNoBodyPerbaikan.AsString:=ArmadaQNoBody.AsString;

  PerawatanQ.Close;
  PerawatanQ.ParamByName('text').AsString:=MasterQLaporanPerawatan.AsString;
  PerawatanQ.Open;
  MasterQArmadaPerawatan.AsString:=PerawatanQArmada.AsString;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:=MasterQArmadaPerawatan.AsString;
  ArmadaQ.Open;
  MasterQPlatNoPerawatan.AsString:=ArmadaQPlatNo.AsString;
  MasterQNoBodyPerawatan.AsString:=ArmadaQNoBody.AsString;

  RebuildQ.Close;
  RebuildQ.ParamByName('text').AsString:=MasterQRebuild.AsString;
  RebuildQ.Open;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:=RebuildQDariArmada.AsString;
  ArmadaQ.Open;
  MasterQDariArmadaRebuild.AsString:=ArmadaQKode.AsString;
  MasterQPlatNoDAR.AsString:=ArmadaQPlatNo.AsString;
  MasterQNoBodyDAR.AsString:=ArmadaQNoBody.AsString;
  ArmadaQ.Close;
  ArmadaQ.ParamByName('text').AsString:=RebuildQKeArmada.AsString;
  ArmadaQ.Open;
  MasterQKeArmadaRebuild.AsString:=ArmadaQKode.AsString;
  MasterQPlatNoKAR.AsString:=ArmadaQPlatNo.AsString;
  MasterQNoBodyKAR.AsString:=ArmadaQNoBody.AsString;
  BarangQ.Close;
  BarangQ.ParamByName('text').AsString:=RebuildQBarang.AsString;
  BarangQ.Open;
  MasterQBarangRebuild.AsString:=BarangQNama.AsString;


  VBonQ.Close;
  VBonQ.ParamByName('text').AsString:= ViewQKode.AsString;
  VBonQ.Open;
{  VKnbalQ.Close;
  VKnbalQ.ParamByName('text').AsString:= ViewQKode.AsString;
  VKnbalQ.Open;
  VTKQ.Close;
  VTKQ.ParamByName('text').AsString:= ViewQKode.AsString;
  VTKQ.Open;  }
  buttonCetak.Enabled:=True;
  cxRadioButton1.Enabled:=false;
  cxRadioButton2.Enabled:=false;
  cxRadioButton3.Enabled:=false;
  cxRadioButton4.Enabled:=false;
end;

procedure TSuratPerintahKerjaFm.MasterVGridLaporanPerawatanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  LaporanPerawatanDropDownFm:=TLaporanPerawatanDropdownfm.Create(Self);
  if LaporanPerawatanDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    PerawatanQ.Close;
    PerawatanQ.ParamByName('text').AsString:=LaporanPerawatanDropDownFm.kode;
    PerawatanQ.Open;
    MasterQLaporanPerawatan.AsString:= LaporanPerawatanDropDownFm.kode;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=MasterQArmadaPerawatan.AsString;
    ArmadaQ.Open;
    MasterQPlatNoPerawatan.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBodyPerawatan.AsString:=ArmadaQNoBody.AsString;
  end;
  LaporanPerawatanDropDownFm.Release;
  MasterQPlatNo.AsString:=MasterQPlatNoPerawatan.AsString;
  MasterQNoBody.AsString:=MasterQNoBodyPerawatan.AsString;
end;

procedure TSuratPerintahKerjaFm.MasterVGridRebuildEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  RebuildDropDownFm:=TRebuildDropdownfm.Create(Self);
  if RebuildDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    RebuildQ.Close;
    RebuildQ.ParamByName('text').AsString:=RebuildDropDownFm.kode;
    RebuildQ.Open;
    MasterQRebuild.AsString:= RebuildDropDownFm.kode;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=RebuildQDariArmada.AsString;
    ArmadaQ.Open;
    MasterQDariArmadaRebuild.AsString:=ArmadaQKode.AsString;
    MasterQPlatNoDAR.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBodyDAR.AsString:=ArmadaQNoBody.AsString;
    ArmadaQ.Close;
    ArmadaQ.ParamByName('text').AsString:=RebuildQKeArmada.AsString;
    ArmadaQ.Open;
    MasterQKeArmadaRebuild.AsString:=ArmadaQKode.AsString;
    MasterQPlatNoKAR.AsString:=ArmadaQPlatNo.AsString;
    MasterQNoBodyKAR.AsString:=ArmadaQNoBody.AsString;
    BarangQ.Close;
    BarangQ.ParamByName('text').AsString:=RebuildQBarang.AsString;
    BarangQ.Open;
    MasterQBarangRebuild.AsString:=BarangQNama.AsString;
  end;
  RebuildDropDownFm.Release;
  MasterQPlatNo.AsString:=MasterQPlatNoDAR.AsString;
  MasterQNoBody.AsString:=MasterQNoBodyDAR.AsString;
end;

procedure TSuratPerintahKerjaFm.buttonCetakClick(Sender: TObject);
begin
 if MessageDlg('Cetak Laporan Pekerjaan Teknik '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'spk.rpt';
    Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;

    Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
    Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
    Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
    Crpe1.Execute;
    Crpe1.WindowState:= wsMaximized;
    Crpe1.Print;

    //Crpe1.CloseWindow;
   { updateQ.Close;
    updateQ.SQL.Clear;
    updateQ.SQL.Add('update suratperintahkerja set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
    updateQ.ExecSQL;
    updateQ.Close; }
    //viewSJQ.Close;
    //viewSJQ.Open;
  end;
end;

procedure TSuratPerintahKerjaFm.BtnCetakClick(Sender: TObject);
begin
   //cetak SJ
    if MessageDlg('Cetak SPK '+KodeEdit.Text+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'SPK.rpt';
      Crpe1.ParamFields[0].CurrentValue:=KodeEdit.text;
{      if cxRadioButton1.Checked=true then
      begin
        ShowMessage(MasterQNoBodyPerbaikan.AsString+' '+MasterQPlatNoPerbaikan.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyPerbaikan.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoPerbaikan.AsString;
      end

      else if cxRadioButton2.Checked=true then
      begin
        ShowMessage(MasterQNoBodyPerawatan.AsString+' '+MasterQPlatNoPerbaikan.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyPerawatan.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoPerawatan.AsString;
      end

      else
      begin
        ShowMessage(MasterQNoBodyDAR.AsString+' '+MasterQPlatNoDAR.AsString);
        Crpe1.ParamFields[1].CurrentValue:=MasterQNoBodyDAR.AsString;
        Crpe1.ParamFields[2].CurrentValue:=MasterQPlatNoDAR.AsString;
      end;   }
 Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

      Crpe1.Execute;
     // Crpe1.Print;
      //Crpe1.CloseWindow;
     { updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kodenota='+QuotedStr(KodeEdit.text));
      updateQ.ExecSQL;
      updateQ.Close;
      ViewKontrakQ.Close;
      ViewKontrakQ.Open;       }
    end;
end;

procedure TSuratPerintahKerjaFm.cxRadioButton4Click(Sender: TObject);
begin
  if cxRadioButton4.Checked then
  begin
    MasterVGridRebuild.Visible:=false;
    MasterVGridLaporanPerawatan.Visible:=false;
    MasterVGridLaporanPerbaikan.Visible:=false;
    MasterVGridLainLain.Visible:=true;
    MasterQRebuild.Visible:=false;
    MasterQLaporanPerbaikan.Visible:=false;
    MasterQLaporanPerawatan.Visible:=false;
    MasterQLainLain.Visible:=true;
    MasterQ.edit;
    MasterQLaporanPerawatan.AsVariant:=null;
    MasterQLaporanPerbaikan.AsVariant:=null;
    MasterQRebuild.AsVariant:=null;
    MasterQLainLain.AsBoolean:=true;
  end;
end;

procedure TSuratPerintahKerjaFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewQ.Open;
end;

procedure TSuratPerintahKerjaFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  ViewQ.Close;
  ViewQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewQ.Open;
end;

end.
