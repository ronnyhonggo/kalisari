object TabelArmadaFm: TTabelArmadaFm
  Left = 260
  Top = 127
  Width = 1207
  Height = 498
  Caption = 'Tabel Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 337
    Height = 345
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 335
      Height = 343
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        OnCellClick = cxGridDBTableView1CellClick
        OnCustomDrawCell = cxGridDBTableView1CustomDrawCell
        DataController.DataSource = DSMTArmada
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGridDBTableView1cek: TcxGridDBColumn
          DataBinding.FieldName = 'cek'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = False
          Properties.ValueUnchecked = 'True'
        end
        object cxGridDBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Options.Editing = False
        end
        object cxGridDBTableView1NoPlat: TcxGridDBColumn
          DataBinding.FieldName = 'NoPlat'
          Options.Editing = False
        end
        object cxGridDBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Options.Editing = False
        end
        object cxGridDBTableView1Trayek: TcxGridDBColumn
          DataBinding.FieldName = 'Trayek'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBTableView1JamTrayek: TcxGridDBColumn
          DataBinding.FieldName = 'JamTrayek'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBTableView1Kontrak: TcxGridDBColumn
          DataBinding.FieldName = 'Kontrak'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBTableView1NamaSopir: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSopir'
          Visible = False
          Options.Editing = False
          Width = 126
        end
        object cxGridDBTableView1NamaKondektur: TcxGridDBColumn
          DataBinding.FieldName = 'NamaKondektur'
          Visible = False
          Options.Editing = False
          Width = 123
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 440
    Width = 1191
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel8: TPanel
    Left = 337
    Top = 41
    Width = 854
    Height = 345
    Align = alClient
    Caption = 'Panel8'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 852
      Height = 343
      Align = alClient
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataSource = MasterDS
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1SO: TcxGridDBColumn
          DataBinding.FieldName = 'SO'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1SOPropertiesButtonClick
          Width = 106
        end
        object cxGrid1DBTableView1Kontrak: TcxGridDBColumn
          DataBinding.FieldName = 'Kontrak'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KontrakPropertiesButtonClick
          Width = 102
        end
        object cxGrid1DBTableView1Trayek: TcxGridDBColumn
          DataBinding.FieldName = 'Trayek'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.OnValidate = cxGrid1DBTableView1TrayekPropertiesValidate
          Width = 102
        end
        object cxGrid1DBTableView1JamTrayek: TcxGridDBColumn
          DataBinding.FieldName = 'JamTrayek'
          Width = 165
        end
        object cxGrid1DBTableView1Sopir: TcxGridDBColumn
          DataBinding.FieldName = 'Sopir'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1SopirPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaSopir: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSopir'
          Options.Editing = False
          Width = 117
        end
        object cxGrid1DBTableView1Kernet: TcxGridDBColumn
          DataBinding.FieldName = 'Kernet'
          Width = 119
        end
        object cxGrid1DBTableView1Kondektur: TcxGridDBColumn
          DataBinding.FieldName = 'Kondektur'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1KondekturPropertiesButtonClick
        end
        object cxGrid1DBTableView1NamaKondektur: TcxGridDBColumn
          DataBinding.FieldName = 'NamaKondektur'
          Width = 168
        end
        object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 97
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 386
    Width = 1191
    Height = 54
    Align = alBottom
    TabOrder = 3
    object BtnSave: TButton
      Left = 8
      Top = 8
      Width = 129
      Height = 45
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      WordWrap = True
      OnClick = BtnSaveClick
    end
    object Button3: TButton
      Left = 141
      Top = 8
      Width = 121
      Height = 45
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button3Click
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1191
    Height = 41
    Align = alTop
    TabOrder = 4
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 56
      Height = 13
      Caption = 'TANGGAL : '
    end
    object cxDateEdit1: TcxDateEdit
      Left = 64
      Top = 13
      Properties.OnChange = cxDateEdit1PropertiesChange
      TabOrder = 0
      Width = 121
    end
  end
  object KodeQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from DetailTabelArmada order by kode desc')
    Left = 600
    Top = 152
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai')
    Left = 552
    Top = 120
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object MasterTabelArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterTabelArmada')
    UpdateObject = SDUMTArmada
    Left = 344
    Top = 272
    object MasterTabelArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterTabelArmadaQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterTabelArmadaQTrayek: TStringField
      FieldName = 'Trayek'
      Size = 10
    end
    object MasterTabelArmadaQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterTabelArmadaQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object MasterTabelArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterTabelArmadaQcek: TBooleanField
      FieldName = 'cek'
    end
    object MasterTabelArmadaQNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterTabelArmadaQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterTabelArmadaQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Size = 50
      Lookup = True
    end
    object MasterTabelArmadaQNamaKondektur: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKondektur'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kondektur'
      Size = 50
      Lookup = True
    end
    object MasterTabelArmadaQJamTrayek: TMemoField
      FieldName = 'JamTrayek'
      BlobType = ftMemo
    end
  end
  object MasterDS: TDataSource
    DataSet = MasterQ
    Left = 632
    Top = 120
  end
  object SDUMaster: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, TabelArmada, Sopir, Kernet, SO, Keterangan, Kontrak' +
        ', Trayek, JamTrayek, Kondektur, Tanggal'#13#10'from DetailTabelArmada'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    ModifySQL.Strings = (
      'update DetailTabelArmada'
      'set'
      '  Kode = :Kode,'
      '  TabelArmada = :TabelArmada,'
      '  Sopir = :Sopir,'
      '  Kernet = :Kernet,'
      '  SO = :SO,'
      '  Keterangan = :Keterangan,'
      '  Kontrak = :Kontrak,'
      '  Trayek = :Trayek,'
      '  JamTrayek = :JamTrayek,'
      '  Kondektur = :Kondektur,'
      '  Tanggal = :Tanggal'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    InsertSQL.Strings = (
      'insert into DetailTabelArmada'
      
        '  (Kode, TabelArmada, Sopir, Kernet, SO, Keterangan, Kontrak, Tr' +
        'ayek, JamTrayek, Kondektur, Tanggal)'
      'values'
      
        '  (:Kode, :TabelArmada, :Sopir, :Kernet, :SO, :Keterangan, :Kont' +
        'rak, :Trayek, :JamTrayek, :Kondektur, :Tanggal)')
    DeleteSQL.Strings = (
      'delete from DetailTabelArmada'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    Left = 664
    Top = 120
  end
  object DSMTArmada: TDataSource
    DataSet = MasterTabelArmadaQ
    Left = 312
    Top = 272
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 552
    Top = 152
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object StringField6: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object StringField7: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object StringField8: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object StringField9: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object BooleanField1: TBooleanField
      FieldName = 'Toilet'
    end
    object BooleanField2: TBooleanField
      FieldName = 'AirSuspension'
    end
    object StringField10: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object IntegerField2: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object StringField11: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object IntegerField3: TIntegerField
      FieldName = 'JumlahBan'
    end
    object StringField12: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object BooleanField3: TBooleanField
      FieldName = 'Aktif'
    end
    object BooleanField4: TBooleanField
      FieldName = 'AC'
    end
    object IntegerField4: TIntegerField
      FieldName = 'KmSekarang'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object StringField13: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object StringField14: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object SDUMTArmada: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopi' +
        'r, cek'#13#10'from masterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update masterTabelArmada'
      'set'
      '  Kode = :Kode,'
      '  Armada = :Armada,'
      '  Trayek = :Trayek,'
      '  JamTrayek = :JamTrayek,'
      '  Kontrak = :Kontrak,'
      '  Kondektur = :Kondektur,'
      '  Sopir = :Sopir,'
      '  cek = :cek'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into masterTabelArmada'
      
        '  (Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopir, c' +
        'ek)'
      'values'
      
        '  (:Kode, :Armada, :Trayek, :JamTrayek, :Kontrak, :Kondektur, :S' +
        'opir, :cek)')
    DeleteSQL.Strings = (
      'delete from masterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 376
    Top = 272
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    BeforeDelete = MasterQBeforeDelete
    AfterDelete = MasterQAfterDelete
    SQL.Strings = (
      'select dta.* '
      'from DetailTabelArmada dta, masterSO so'
      'where dta.tabelarmada = :text and dta.so=so.kodenota'
      'and so.berangkat<=:text2 and so.tiba>=:text3'
      'union'
      'select dta.*'
      'from DetailTabelArmada dta'
      'where dta.tabelarmada =:text and '
      '(dta.trayek is not null or dta.kontrak is not null)'
      'and dta.Tanggal=:text4')
    UpdateObject = SDUMaster
    Left = 600
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text4'
        ParamType = ptInput
      end>
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTabelArmada: TStringField
      FieldName = 'TabelArmada'
      Required = True
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQKernet: TStringField
      FieldName = 'Kernet'
      Size = 50
    end
    object MasterQSO: TStringField
      FieldName = 'SO'
      Size = 10
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Size = 50
      Lookup = True
    end
    object MasterQRute: TStringField
      FieldKind = fkLookup
      FieldName = 'Rute'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Rute'
      KeyFields = 'SO'
      Size = 10
      Lookup = True
    end
    object MasterQAsal: TStringField
      FieldKind = fkLookup
      FieldName = 'Asal'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object MasterQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object MasterQTanggalSO: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalSO'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Tgl'
      KeyFields = 'SO'
      Lookup = True
    end
    object MasterQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterQTrayek: TStringField
      FieldName = 'Trayek'
      Size = 10
    end
    object MasterQJamTrayek: TStringField
      FieldName = 'JamTrayek'
      Size = 50
    end
    object MasterQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object MasterQNamaKondektur: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKondektur'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kondektur'
      Size = 50
      Lookup = True
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object DataMaster: TSDQuery
    DatabaseName = 'Data'
    Options = []
    AfterDelete = MasterQAfterDelete
    SQL.Strings = (
      'select dta.* '
      'from DetailTabelArmada dta, masterSO so'
      'where dta.tabelarmada = :text and dta.so=so.kodenota'
      'and so.berangkat<=:text2 and so.tiba>=:text3'
      'union'
      'select dta.*'
      'from DetailTabelArmada dta'
      'where dta.tabelarmada =:text and '
      '(dta.trayek is not null or dta.kontrak is not null)'
      'and dta.Tanggal=:text4')
    Left = 632
    Top = 152
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text4'
        ParamType = ptInput
      end>
    object DataMasterKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DataMasterTabelArmada: TStringField
      FieldName = 'TabelArmada'
      Required = True
      Size = 10
    end
    object DataMasterSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object DataMasterKernet: TStringField
      FieldName = 'Kernet'
      Size = 50
    end
    object DataMasterSO: TStringField
      FieldName = 'SO'
      Size = 10
    end
    object DataMasterKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DataMasterKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object DataMasterTrayek: TStringField
      FieldName = 'Trayek'
      Size = 10
    end
    object DataMasterJamTrayek: TStringField
      FieldName = 'JamTrayek'
      Size = 50
    end
    object DataMasterKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object DataMasterTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
  end
  object UpdateArmadaSOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'update MasterSO set Armada=:text'
      'where kodenota=:text2')
    Left = 664
    Top = 152
    ParamData = <
      item
        DataType = ftVariant
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Rute')
    Left = 552
    Top = 184
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 250
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 250
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from masterso')
    Left = 552
    Top = 216
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object SOQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object SOQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
  end
end
