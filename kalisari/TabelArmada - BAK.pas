unit TabelArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxTimeEdit, cxCheckBox, Buttons, cxDropDownEdit,
  cxGroupBox;

type
  TTabelArmadaFm = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterTabelArmadaQ: TSDQuery;
    Panel8: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel2: TPanel;
    BtnSave: TButton;
    Button3: TButton;
    MasterDS: TDataSource;
    SDUMaster: TSDUpdateSQL;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    DSMTArmada: TDataSource;
    ArmadaQ: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField1: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    StringField10: TStringField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    StringField11: TStringField;
    IntegerField3: TIntegerField;
    StringField12: TStringField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    IntegerField4: TIntegerField;
    DateTimeField3: TDateTimeField;
    StringField13: TStringField;
    StringField14: TStringField;
    DateTimeField4: TDateTimeField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    MasterTabelArmadaQKode: TStringField;
    MasterTabelArmadaQArmada: TStringField;
    MasterTabelArmadaQTrayek: TStringField;
    MasterTabelArmadaQKontrak: TStringField;
    MasterTabelArmadaQKondektur: TStringField;
    MasterTabelArmadaQSopir: TStringField;
    MasterTabelArmadaQcek: TBooleanField;
    cxGridDBTableView1Kode: TcxGridDBColumn;
    cxGridDBTableView1Trayek: TcxGridDBColumn;
    cxGridDBTableView1Kontrak: TcxGridDBColumn;
    cxGridDBTableView1cek: TcxGridDBColumn;
    MasterTabelArmadaQNoPlat: TStringField;
    MasterTabelArmadaQNoBody: TStringField;
    MasterTabelArmadaQNamaSopir: TStringField;
    MasterTabelArmadaQNamaKondektur: TStringField;
    cxGridDBTableView1NoPlat: TcxGridDBColumn;
    cxGridDBTableView1NoBody: TcxGridDBColumn;
    cxGridDBTableView1NamaSopir: TcxGridDBColumn;
    cxGridDBTableView1NamaKondektur: TcxGridDBColumn;
    cxGrid1DBTableView1Sopir: TcxGridDBColumn;
    cxGrid1DBTableView1Kernet: TcxGridDBColumn;
    cxGrid1DBTableView1SO: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid1DBTableView1NamaSopir: TcxGridDBColumn;
    SDUMTArmada: TSDUpdateSQL;
    MasterTabelArmadaQJamTrayek: TMemoField;
    cxGridDBTableView1JamTrayek: TcxGridDBColumn;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQTabelArmada: TStringField;
    MasterQSopir: TStringField;
    MasterQKernet: TStringField;
    MasterQSO: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQNamaSopir: TStringField;
    DataMaster: TSDQuery;
    DataMasterKode: TStringField;
    DataMasterTabelArmada: TStringField;
    DataMasterSopir: TStringField;
    DataMasterKernet: TStringField;
    DataMasterSO: TStringField;
    DataMasterKeterangan: TMemoField;
    UpdateArmadaSOQ: TSDQuery;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    SOQKomisiPelanggan: TCurrencyField;
    SOQKeteranganRute: TMemoField;
    SOQKeteranganHarga: TMemoField;
    MasterQRute: TStringField;
    MasterQAsal: TStringField;
    MasterQTujuan: TStringField;
    MasterQTanggalSO: TDateTimeField;
    Panel3: TPanel;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    MasterQKontrak: TStringField;
    MasterQTrayek: TStringField;
    MasterQJamTrayek: TStringField;
    MasterQKondektur: TStringField;
    DataMasterKontrak: TStringField;
    DataMasterTrayek: TStringField;
    DataMasterJamTrayek: TStringField;
    DataMasterKondektur: TStringField;
    cxGrid1DBTableView1Kontrak: TcxGridDBColumn;
    cxGrid1DBTableView1Trayek: TcxGridDBColumn;
    cxGrid1DBTableView1JamTrayek: TcxGridDBColumn;
    cxGrid1DBTableView1Kondektur: TcxGridDBColumn;
    MasterQNamaKondektur: TStringField;
    cxGrid1DBTableView1NamaKondektur: TcxGridDBColumn;
    MasterQTanggal: TDateTimeField;
    DataMasterTanggal: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure cxGrid1DBTableView1SopirPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1SOPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGridDBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterQAfterDelete(DataSet: TDataSet);
    procedure cxGridDBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure MasterQBeforeDelete(DataSet: TDataSet);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView1KontrakPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1KondekturPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1TrayekPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TabelArmadaFm: TTabelArmadaFm;
  MasterOriSQL: string;
  DetailBonBarangOriSQL: string;
  paramkode,paramkode2 :string;
  baru,index:integer;
  sopir,kernet,so,keterangan : string;
  SOhapus : array [1..100] of string;

implementation

uses DM, MenuUtama, DropDown, BarangDropDown, SPKDropDown, StoringDropDown, PegawaiDropDown,
  ArmadaDropDown, KontrakDropDown, SODropDown, SOTabelArmadaDropDown;

{$R *.dfm}

procedure TTabelArmadaFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
end;

procedure TTabelArmadaFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TTabelArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TTabelArmadaFm.FormShow(Sender: TObject);
begin
  index:=1;
  SOQ.Open;
  RuteQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  MasterTabelArmadaQ.Open;
  MasterQ.Close;
  MasterQ.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  MasterQ.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  MasterQ.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  MasterQ.ParamByName('text4').AsDate:=cxDateEdit1.Date;
  MasterQ.Open;
  MasterQ.Edit;
  DataMaster.Close;
  DataMaster.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  DataMaster.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  DataMaster.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  DataMaster.ParamByName('text4').AsDate:=cxDateEdit1.Date;
  DataMaster.Open;
  cxGrid1.Enabled:=true;
  BtnSave.Enabled:=true;//menuutamafm.UserQUpdateBonBarang.AsBoolean;
  cxDateEdit1.Date:=Date;
end;

procedure TTabelArmadaFm.BtnSaveClick(Sender: TObject);
var kode:string;
    i,ind:integer;
    berubah : boolean;
begin
  KodeQ.Open;
  if KodeQ.IsEmpty then begin
    //kode:=DMFm.Fill('1',10,'0',True);
    i:=1;
  end
  else begin
    //kode:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    i:=StrToInt(KodeQkode.AsString)+1;
  end;
  KodeQ.Close;
  MasterQ.Edit;
  MasterQ.First;
  while MasterQ.Eof=false do begin
    MasterQ.Edit;
    MasterQTanggal.AsDateTime:=cxDateEdit1.Date;
    if MasterQKode.AsString='new' then begin
      MasterQKode.AsString:=DMFm.Fill(IntToStr(i),10,'0',True);
      MasterQ.Post;
      i:=i+1;
    end;
    UpdateArmadaSOQ.Close;
    UpdateArmadaSOQ.ParamByName('text').AsString:=MasterTabelArmadaQArmada.AsString;
    UpdateArmadaSOQ.ParamByName('text2').AsString:=MasterQSO.AsString;
    UpdateArmadaSOQ.ExecSQL;
    MasterQ.Next;
  end;
  berubah:=false;
  MasterQ.First;
  DataMaster.First;
  while (berubah=false) and (DataMaster.Eof=false) and (DataMaster.RecordCount>0) do begin
    if (MasterQSopir.AsString<>DataMasterSopir.AsString) or
       (MasterQSO.AsString<>DataMasterSO.AsString) or
       (MasterQKernet.AsString<>DataMasterKernet.AsString) or
       (MasterQKeterangan.AsString<>DataMasterKeterangan.AsString) then
    begin
      MasterTabelArmadaQ.Edit;
      MasterTabelArmadaQcek.AsBoolean:=True;
      if (MasterQSO.AsString<>DataMasterSO.AsString) then begin
        UpdateArmadaSOQ.Close;
        UpdateArmadaSOQ.ParamByName('text').Value:=null;
        UpdateArmadaSOQ.ParamByName('text2').AsString:=DataMasterSO.AsString;
        UpdateArmadaSOQ.ExecSQL;
      end;
      berubah:=true;
    end;
    MasterQ.Next;
    DataMaster.Next;
  end;
  //hapus
  for ind:=1 to index do begin
    UpdateArmadaSOQ.Close;
    UpdateArmadaSOQ.ParamByName('text').Value:=null;
    UpdateArmadaSOQ.ParamByName('text2').AsString:=SOhapus[ind];
    UpdateArmadaSOQ.ExecSQL;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MasterTabelArmadaQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    MasterTabelArmadaQ.CommitUpdates;
    ShowMessage('Simpan Berhasil');
    cxGrid1.Enabled:=false;
    index:=1;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  ArmadaQ.Refresh;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin
    if MasterQKode.AsString='' then begin
      MasterQKode.AsString:='new';
      MasterQTabelArmada.AsString:=MasterTabelArmadaQKode.AsString;
      MasterQ.Post;
    end;
  end;
end;

procedure TTabelArmadaFm.Button3Click(Sender: TObject);
begin
  Release;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1SopirPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQSopir.AsString:=PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1SOPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SOTabelArmadaDropDownFm:=TSOTabelArmadaDropdownfm.Create(Self,cxDateEdit1.Date);
  if SOTabelArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQSO.AsString:=SOTabelArmadaDropDownFm.kode;
    MasterQTrayek.AsVariant:=null;
    MasterQKontrak.AsVariant:=null;
    MasterQJamTrayek.AsVariant:=null;
  end;
  SOTabelArmadaDropDownFm.Release;
end;

procedure TTabelArmadaFm.cxGridDBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  MasterQ.Close;
  MasterQ.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  MasterQ.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  MasterQ.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  MasterQ.ParamByName('text4').AsDate:=cxDateEdit1.Date;
  MasterQ.Open;
  MasterQ.Edit;
  DataMaster.Close;
  DataMaster.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  DataMaster.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  DataMaster.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  DataMaster.ParamByName('text4').AsDate:=cxDateEdit1.Date;
  DataMaster.Open;
  cxGrid1.Enabled:=true;
end;

procedure TTabelArmadaFm.MasterQAfterDelete(DataSet: TDataSet);
begin
  MasterTabelArmadaQ.Edit;
  MasterTabelArmadaQcek.AsBoolean:=true;
end;

procedure TTabelArmadaFm.cxGridDBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[0] = true) then
  begin
    ACanvas.Brush.Color := clRed;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

procedure TTabelArmadaFm.MasterQBeforeDelete(DataSet: TDataSet);
begin
  SOhapus[index]:=DataSet.FieldByName('SO').Value;
  index:=index+1;
end;

procedure TTabelArmadaFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  MasterQ.Close;
  MasterQ.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  MasterQ.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  MasterQ.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  MasterQ.Open;
  MasterQ.Edit;
  DataMaster.Close;
  DataMaster.ParamByName('text').AsString:=MasterTabelArmadaQKode.AsString;
  DataMaster.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  DataMaster.ParamByName('text3').AsDate:=cxDateEdit1.Date;
  DataMaster.Open;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1KontrakPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KontrakDropDownFm:=TKontrakDropDownFm.create(self,'select * from kontrak');
  if KontrakDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQKontrak.AsString:=KontrakDropDownFm.kode;
    MasterQSO.AsVariant:=null;
    MasterQTrayek.AsVariant:=null;
    MasterQJamTrayek.AsVariant:=null;
  end;
  KontrakDropDownFm.Release;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1KondekturPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Edit;
    MasterQKondektur.AsString:=PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TTabelArmadaFm.cxGrid1DBTableView1TrayekPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  MasterQTrayek.AsString:=DisplayValue;
  MasterQSO.AsVariant:=null;
  MasterQKontrak.AsVariant:=null;
end;

end.
