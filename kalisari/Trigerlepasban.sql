ALTER TRIGGER [dbo].[LEPASBAN_INSERT] ON [dbo].[LepasBan] 
   FOR  INSERT 
AS
Declare @status varchar(20);
Declare @Ban varchar(20);
Set @Ban=(select ban from inserted);
Set @Status=(select statusban from inserted); 
BEGIN
  if @Status='Rusak'
  begin	
	update Ban set Status='RUSAK' where Kode=@Ban;
  end;
END
GO