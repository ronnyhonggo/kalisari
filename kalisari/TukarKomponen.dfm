object TukarKomponenFm: TTukarKomponenFm
  Left = 292
  Top = 65
  Width = 808
  Height = 644
  Caption = 'TukarKomponen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 517
    Width = 792
    Height = 49
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 248
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 7
      Top = 10
      Width = 77
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object buttonCetak: TcxButton
      Left = 528
      Top = 10
      Width = 75
      Height = 25
      Caption = 'CETAK'
      Enabled = False
      TabOrder = 2
      Visible = False
      OnClick = buttonCetakClick
    end
    object DELETEbtn: TcxButton
      Left = 169
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 3
      OnClick = DeleteBtnClick
    end
    object AppButton: TcxButton
      Left = 89
      Top = 10
      Width = 75
      Height = 25
      Caption = 'APPROVE'
      TabOrder = 4
      OnClick = AppButtonClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 494
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 5
      Height = 47
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 353
    Height = 225
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 142
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridDBEditorRow1EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'SPK'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Height = 40
      Properties.DataBinding.FieldName = 'DetailTindakan'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridDariArmada: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = MasterVGridDariArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'DariArmada'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridNoBody_dari: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody_dari'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo_dari'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object MasterVGridBarangDari: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = MasterVGridBarangDariEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'BarangDari'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridnama_dari: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'nama_dari'
      Properties.Options.Editing = False
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 566
    Width = 792
    Height = 39
    Panels = <
      item
        Width = 50
      end>
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 353
    Top = 48
    Width = 439
    Height = 225
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 131
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 4
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBEditorRow3: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.EditProperties.OnButtonClick = MasterVGridKeArmadaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'KeArmada'
      Properties.Options.Editing = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1NoBody_ke: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBody_ke'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNo_ke'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object cxDBEditorRow7: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'BarangKe'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBEditorRow8: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'nama_ke'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 3
      Index = 0
      Version = 1
    end
    object cxDBEditorRow9: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.DataBinding.FieldName = 'TglTukar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBEditorRow10: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBEditorRow10EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Peminta'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 6
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1DetailPeminta: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailPeminta'
      Properties.Options.Editing = False
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
      Properties.EditProperties.Columns = 2
      Properties.EditProperties.Items = <
        item
          Caption = 'PENDING'
          Value = 'PENDING'
        end
        item
          Caption = 'APPROVED'
          Value = 'APPROVED'
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'Status'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 8
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 273
    Width = 792
    Height = 244
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 5
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 790
      Height = 25
      Align = alTop
      Caption = 'Tukar Komponen :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 24
      Width = 790
      Height = 219
      Align = alBottom
      BevelOuter = bvRaised
      BevelWidth = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = SJDS
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Width = 82
        end
        object cxGrid1DBTableView1DariArmada: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody_dari'
          Width = 91
        end
        object cxGrid1DBTableView1KeArmada: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody_ke'
          Width = 80
        end
        object cxGrid1DBTableView1BarangDari: TcxGridDBColumn
          DataBinding.FieldName = 'Barang_Dari'
          Options.SortByDisplayText = isbtOn
          Width = 111
        end
        object cxGrid1DBTableView1BarangKe: TcxGridDBColumn
          DataBinding.FieldName = 'Barang_Ke'
          Options.SortByDisplayText = isbtOn
          Width = 112
        end
        object cxGrid1DBTableView1TglTukar: TcxGridDBColumn
          DataBinding.FieldName = 'TglTukar'
          Width = 89
        end
        object cxGrid1DBTableView1Peminta: TcxGridDBColumn
          DataBinding.FieldName = 'DetailPeminta'
          Options.SortByDisplayText = isbtOn
          Width = 133
        end
        object cxGrid1DBTableView1LaporanPerbaikan: TcxGridDBColumn
          DataBinding.FieldName = 'TindakanPerbaikan'
          Width = 200
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select t.*, a1.NoBody as NoBody_dari,a2.NoBody as NoBody_ke,b1.N' +
        'ama as nama_dari,b2.Nama as nama_ke, lpb.WaktuMulai as waktumula' +
        'i_lpb,lpb.WaktuSelesai as waktuselesai_lpb from TukarKomponen t ' +
        'left outer join LaporanPerbaikan lpb on lpb.Kode=t.LaporanPerbai' +
        'kan, Armada a1, Armada a2, Barang b1, Barang b2 where t.DariArma' +
        'da=a1.Kode and t.KeArmada=a2.Kode and t.BarangDari=b1.Kode and t' +
        '.BarangKe=b2.Kode')
    UpdateObject = MasterUS
    Left = 243
    Top = 7
    object MasterQDetailPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 10
      Lookup = True
    end
    object MasterQTindakanPerbaikan: TStringField
      FieldKind = fkLookup
      FieldName = 'TindakanPerbaikan'
      LookupDataSet = lpbQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TindakanPerbaikan'
      KeyFields = 'LaporanPerbaikan'
      Size = 10
      Lookup = True
    end
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQDariArmada: TStringField
      FieldName = 'DariArmada'
      Required = True
      Size = 10
    end
    object MasterQKeArmada: TStringField
      FieldName = 'KeArmada'
      Required = True
      Size = 10
    end
    object MasterQBarangDari: TStringField
      FieldName = 'BarangDari'
      Required = True
      Size = 10
    end
    object MasterQBarangKe: TStringField
      FieldName = 'BarangKe'
      Required = True
      Size = 10
    end
    object MasterQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
      Required = True
    end
    object MasterQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object MasterQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object MasterQNoBody_dari: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody_dari'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'DariArmada'
      Size = 50
      Lookup = True
    end
    object MasterQNoBody_ke: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody_ke'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'KeArmada'
      Size = 50
      Lookup = True
    end
    object MasterQnama_dari: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang_Dari'
      LookupDataSet = barangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangDari'
      Required = True
      Size = 50
      Lookup = True
    end
    object MasterQnama_ke: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang_ke'
      LookupDataSet = barangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangKe'
      Required = True
      Size = 50
      Lookup = True
    end
    object MasterQwaktumulai_lpb: TDateTimeField
      FieldName = 'waktumulai_lpb'
    end
    object MasterQwaktuselesai_lpb: TDateTimeField
      FieldName = 'waktuselesai_lpb'
    end
    object MasterQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object MasterQDetailTindakan: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailTindakan'
      LookupDataSet = VSPKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'DetailTindakan'
      KeyFields = 'SPK'
      Size = 200
      Lookup = True
    end
    object MasterQPlatNo_dari: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo_dari'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'DariArmada'
      Size = 30
      Lookup = True
    end
    object MasterQPlatNo_ke: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo_ke'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KeArmada'
      Size = 30
      Lookup = True
    end
    object MasterQPP: TStringField
      FieldKind = fkLookup
      FieldName = 'PP'
      LookupDataSet = lpbQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PP'
      KeyFields = 'LaporanPerbaikan'
      Size = 10
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 348
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, DariArmada, KeArmada, BarangDari, BarangKe, TglTuka' +
        'r, Peminta, SPK, LaporanPerbaikan, Status, CreateDate, CreateBy,' +
        ' Operator, TglEntry, TglCetak'#13#10'from TukarKomponen'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update TukarKomponen'
      'set'
      '  Kode = :Kode,'
      '  DariArmada = :DariArmada,'
      '  KeArmada = :KeArmada,'
      '  BarangDari = :BarangDari,'
      '  BarangKe = :BarangKe,'
      '  TglTukar = :TglTukar,'
      '  Peminta = :Peminta,'
      '  SPK = :SPK,'
      '  LaporanPerbaikan = :LaporanPerbaikan,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into TukarKomponen'
      
        '  (Kode, DariArmada, KeArmada, BarangDari, BarangKe, TglTukar, P' +
        'eminta, SPK, LaporanPerbaikan, Status, CreateDate, CreateBy, Ope' +
        'rator, TglEntry, TglCetak)'
      'values'
      
        '  (:Kode, :DariArmada, :KeArmada, :BarangDari, :BarangKe, :TglTu' +
        'kar, :Peminta, :SPK, :LaporanPerbaikan, :Status, :CreateDate, :C' +
        'reateBy, :Operator, :TglEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from TukarKomponen'
      'where'
      '  Kode = :OLD_Kode')
    Left = 308
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from tukarkomponen order by kode desc')
    Left = 278
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 481
    Top = 7
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39)
    Left = 753
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object viewSJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select cast(tglTukar as date) as Tgl,'
      '          t.*, '
      '          a1.NoBody as NoBody_dari,'
      '          a2.NoBody as NoBody_ke,'
      '          b1.Nama as nama_dari,'
      '          b2.Nama as nama_ke, '
      '          lpb.WaktuMulai as waktumulai_lpb,'
      '          lpb.WaktuSelesai as waktuselesai_lpb '
      'from TukarKomponen t '
      
        'left outer join LaporanPerbaikan lpb on lpb.Kode=t.LaporanPerbai' +
        'kan, Armada a1, Armada a2, Barang b1, Barang b2 '
      
        'where t.DariArmada=a1.Kode and t.KeArmada=a2.Kode and t.BarangDa' +
        'ri=b1.Kode and t.BarangKe=b2.Kode and'
      '(TglTukar>=:text1 and TglTukar<=:text2) or TglTukar is null'
      'order by tglentry desc')
    Left = 393
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object viewSJQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object viewSJQDariArmada: TStringField
      FieldName = 'DariArmada'
      Required = True
      Size = 10
    end
    object viewSJQKeArmada: TStringField
      FieldName = 'KeArmada'
      Required = True
      Size = 10
    end
    object viewSJQBarangDari: TStringField
      FieldName = 'BarangDari'
      Required = True
      Size = 10
    end
    object viewSJQBarangKe: TStringField
      FieldName = 'BarangKe'
      Required = True
      Size = 10
    end
    object viewSJQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
      Required = True
    end
    object viewSJQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 10
    end
    object viewSJQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object viewSJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object viewSJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object viewSJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object viewSJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object viewSJQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object viewSJQNoBody_dari: TStringField
      FieldName = 'NoBody_dari'
      Size = 50
    end
    object viewSJQNoBody_ke: TStringField
      FieldName = 'NoBody_ke'
      Size = 50
    end
    object viewSJQnama_dari: TStringField
      FieldName = 'nama_dari'
      Required = True
      Size = 50
    end
    object viewSJQnama_ke: TStringField
      FieldName = 'nama_ke'
      Required = True
      Size = 50
    end
    object viewSJQwaktumulai_lpb: TDateTimeField
      FieldName = 'waktumulai_lpb'
    end
    object viewSJQwaktuselesai_lpb: TDateTimeField
      FieldName = 'waktuselesai_lpb'
    end
    object viewSJQBarang_Dari: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang_Dari'
      LookupDataSet = barangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangDari'
      Size = 10
      Lookup = True
    end
    object viewSJQBarang_Ke: TStringField
      FieldKind = fkLookup
      FieldName = 'Barang_Ke'
      LookupDataSet = barangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'BarangKe'
      Size = 10
      Lookup = True
    end
    object viewSJQDetailPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 10
      Lookup = True
    end
    object viewSJQTindakanPerbaikan: TStringField
      FieldKind = fkLookup
      FieldName = 'TindakanPerbaikan'
      LookupDataSet = lpbQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'TindakanPerbaikan'
      KeyFields = 'LaporanPerbaikan'
      Size = 10
      Lookup = True
    end
  end
  object SJDS: TDataSource
    DataSet = viewSJQ
    Left = 436
    Top = 14
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 2
    Version.Windows.Build = '9200'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 208
    Top = 8
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 177
    Top = 7
    object StringField1: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object MemoField1: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object MemoField2: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object IntegerField1: TIntegerField
      FieldName = 'Jarak'
    end
  end
  object barangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang;')
    Left = 609
    Top = 7
    object barangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object barangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object barangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Visible = False
      Size = 10
    end
    object barangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
      Visible = False
    end
    object barangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
      Visible = False
    end
    object barangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
      Visible = False
    end
    object barangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Visible = False
      Size = 50
    end
    object barangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object barangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object barangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object barangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object barangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object lpbQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from laporanperbaikan')
    Left = 569
    Top = 7
    object lpbQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object lpbQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object lpbQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object lpbQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      Required = True
      BlobType = ftMemo
    end
    object lpbQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
      Visible = False
    end
    object lpbQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
      Visible = False
    end
    object lpbQVerifikator: TStringField
      FieldName = 'Verifikator'
      Visible = False
      Size = 50
    end
    object lpbQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
      Visible = False
    end
    object lpbQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Visible = False
      Size = 50
    end
    object lpbQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
    object lpbQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
    object lpbQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object lpbQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object lpbQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object lpbQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object lpbQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Pegawai')
    Left = 528
    Top = 8
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object VSPKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'LP '#39' + spk.lap' +
        'oranperbaikan AS KodeReferensi, pp.armada AS Armada, lpb.status ' +
        'AS StatusReferensi, spk.mekanik AS Mekanik, spk.status AS Status' +
        ', spk.detailtindakan AS DetailTindakan, spk.keterangan AS Ketera' +
        'ngan '
      
        'from suratperintahkerja spk, laporanperbaikan lpb, permintaanper' +
        'baikan pp'
      
        'where spk.laporanperbaikan<>NULL AND spk.laporanperbaikan=lpb.ko' +
        'de AND lpb.status='#39'ON PROCESS'#39' AND lpb.pp=pp.kode'
      'UNION'
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'RB '#39' + spk.reb' +
        'uild AS KodeReferensi, rb.dariarmada AS Armada, rb.status AS Sta' +
        'tusReferensi, spk.mekanik AS Mekanik, spk.status AS Status, spk.' +
        'detailtindakan AS DetailTindakan, spk.keterangan AS Keterangan '
      'from suratperintahkerja spk, rebuild rb'
      
        'where spk.rebuild<>NULL AND spk.rebuild=rb.kode AND rb.status='#39'O' +
        'N PROCESS'#39)
    Left = 721
    Top = 7
    object VSPKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VSPKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object VSPKQKodeReferensi: TStringField
      FieldName = 'KodeReferensi'
      Size = 13
    end
    object VSPKQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object VSPKQPlatNoArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object VSPKQNoBodyArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object VSPKQStatusReferensi: TStringField
      FieldName = 'StatusReferensi'
      Size = 50
    end
    object VSPKQMekanik: TStringField
      FieldName = 'Mekanik'
      Visible = False
      Size = 10
    end
    object VSPKQNamaMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 50
      Lookup = True
    end
    object VSPKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object VSPKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object VSPKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
  end
  object PPQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select pp.* from permintaanperbaikan pp left outer join laporanp' +
        'erbaikan lp on pp.kode=lp.pp')
    Left = 649
    Top = 7
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object PPQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object PPQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Visible = False
      Size = 10
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object RebuildQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from rebuild')
    Left = 689
    Top = 7
    object RebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RebuildQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object RebuildQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object RebuildQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object RebuildQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object RebuildQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object RebuildQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object RebuildQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object RebuildQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object RebuildQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object RebuildQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object RebuildQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object RebuildQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object RebuildQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object RebuildQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object RebuildQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object RebuildQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object RebuildQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object RebuildQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object RebuildQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object RebuildQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RebuildQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RebuildQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RebuildQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
end
