unit TukarKomponen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxDropDownEdit, cxGroupBox;

type
  TTukarKomponenFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    viewSJQ: TSDQuery;
    SJDS: TDataSource;
    Crpe1: TCrpe;
    updateQ: TSDQuery;
    StringField1: TStringField;
    MemoField1: TMemoField;
    MemoField2: TMemoField;
    IntegerField1: TIntegerField;
    buttonCetak: TcxButton;
    KodeQkode: TStringField;
    MasterVGridDariArmada: TcxDBEditorRow;
    MasterVGridBarangDari: TcxDBEditorRow;
    MasterVGridnama_dari: TcxDBEditorRow;
    barangQ: TSDQuery;
    barangQKode: TStringField;
    barangQNama: TStringField;
    barangQSatuan: TStringField;
    barangQMinimumStok: TIntegerField;
    barangQMaximumStok: TIntegerField;
    barangQStandardUmur: TIntegerField;
    barangQLokasi: TStringField;
    barangQCreateDate: TDateTimeField;
    barangQCreateBy: TStringField;
    barangQOperator: TStringField;
    barangQTglEntry: TDateTimeField;
    lpbQ: TSDQuery;
    lpbQKode: TStringField;
    lpbQPP: TStringField;
    lpbQAnalisaMasalah: TMemoField;
    lpbQTindakanPerbaikan: TMemoField;
    lpbQWaktuMulai: TDateTimeField;
    lpbQWaktuSelesai: TDateTimeField;
    lpbQVerifikator: TStringField;
    lpbQTglSerahTerima: TDateTimeField;
    lpbQPICSerahTerima: TStringField;
    lpbQStatus: TStringField;
    lpbQKeterangan: TMemoField;
    lpbQCreateDate: TDateTimeField;
    lpbQCreateBy: TStringField;
    lpbQOperator: TStringField;
    lpbQTglEntry: TDateTimeField;
    lpbQTglCetak: TDateTimeField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBEditorRow3: TcxDBEditorRow;
    cxDBEditorRow7: TcxDBEditorRow;
    cxDBEditorRow8: TcxDBEditorRow;
    cxDBEditorRow9: TcxDBEditorRow;
    cxDBEditorRow10: TcxDBEditorRow;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQDetailPeminta: TStringField;
    cxDBVerticalGrid1DetailPeminta: TcxDBEditorRow;
    MasterQTindakanPerbaikan: TStringField;
    MasterQKode: TStringField;
    MasterQDariArmada: TStringField;
    MasterQKeArmada: TStringField;
    MasterQBarangDari: TStringField;
    MasterQBarangKe: TStringField;
    MasterQTglTukar: TDateTimeField;
    MasterQPeminta: TStringField;
    MasterQLaporanPerbaikan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQTglCetak: TDateTimeField;
    MasterQNoBody_dari: TStringField;
    MasterQNoBody_ke: TStringField;
    MasterQnama_dari: TStringField;
    MasterQnama_ke: TStringField;
    MasterQwaktumulai_lpb: TDateTimeField;
    MasterQwaktuselesai_lpb: TDateTimeField;
    MasterVGridNoBody_dari: TcxDBEditorRow;
    cxDBVerticalGrid1NoBody_ke: TcxDBEditorRow;
    viewSJQKode: TStringField;
    viewSJQDariArmada: TStringField;
    viewSJQKeArmada: TStringField;
    viewSJQBarangDari: TStringField;
    viewSJQBarangKe: TStringField;
    viewSJQTglTukar: TDateTimeField;
    viewSJQPeminta: TStringField;
    viewSJQLaporanPerbaikan: TStringField;
    viewSJQCreateDate: TDateTimeField;
    viewSJQCreateBy: TStringField;
    viewSJQOperator: TStringField;
    viewSJQTglEntry: TDateTimeField;
    viewSJQTglCetak: TDateTimeField;
    viewSJQNoBody_dari: TStringField;
    viewSJQNoBody_ke: TStringField;
    viewSJQnama_dari: TStringField;
    viewSJQnama_ke: TStringField;
    viewSJQwaktumulai_lpb: TDateTimeField;
    viewSJQwaktuselesai_lpb: TDateTimeField;
    viewSJQBarang_Dari: TStringField;
    viewSJQBarang_Ke: TStringField;
    viewSJQDetailPeminta: TStringField;
    viewSJQTindakanPerbaikan: TStringField;
    DELETEbtn: TcxButton;
    Panel1: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1DariArmada: TcxGridDBColumn;
    cxGrid1DBTableView1KeArmada: TcxGridDBColumn;
    cxGrid1DBTableView1BarangDari: TcxGridDBColumn;
    cxGrid1DBTableView1BarangKe: TcxGridDBColumn;
    cxGrid1DBTableView1TglTukar: TcxGridDBColumn;
    cxGrid1DBTableView1Peminta: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPerbaikan: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Label1: TLabel;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    MasterQSPK: TStringField;
    VSPKQ: TSDQuery;
    MasterQDetailTindakan: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterQPlatNo_dari: TStringField;
    MasterVGridDBEditorRow3: TcxDBEditorRow;
    MasterQPlatNo_ke: TStringField;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    PPQ: TSDQuery;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQPlatNo: TStringField;
    PPQNoBody: TStringField;
    PPQNamaPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQArmada: TStringField;
    PPQPeminta: TStringField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    MasterQPP: TStringField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    VSPKQKode: TStringField;
    VSPKQTanggal: TDateTimeField;
    VSPKQKodeReferensi: TStringField;
    VSPKQArmada: TStringField;
    VSPKQStatusReferensi: TStringField;
    VSPKQMekanik: TStringField;
    VSPKQStatus: TStringField;
    VSPKQKeterangan: TStringField;
    VSPKQPlatNoArmada: TStringField;
    VSPKQNoBodyArmada: TStringField;
    VSPKQNamaMekanik: TStringField;
    VSPKQDetailTindakan: TMemoField;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    AppButton: TcxButton;
    MasterQStatus: TStringField;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    barangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure MasterVGridDariArmadaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKeArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridBarangDariEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridBarangKeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridLaporanPerawatanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBEditorRow10EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure AppButtonClick(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  TukarKomponenFm: TTukarKomponenFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, ArmadaDropDown, StrUtils,
  BarangDropDown, PegawaiDropDown, SPKDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TTukarKomponenFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TTukarKomponenFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TTukarKomponenFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TTukarKomponenFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TTukarKomponenFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  ArmadaQ.Open;
  barangQ.Open;
  PegawaiQ.Open;
  lpbQ.Open;
  VSPKQ.Open;
  RebuildQ.Open;
  PPQ.Open;
  AppButton.Enabled:=False;
end;

procedure TTukarKomponenFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
  MasterQStatus.Value:='PENDING';
end;

procedure TTukarKomponenFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TTukarKomponenFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  viewSJQ.Close;
  ViewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  ViewSJQ.Open;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertTukarKomponen.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateTukarKomponen.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteTukarKomponen.AsBoolean;
  AppButton.Enabled:=menuutamafm.UserQApprovalTukarKomponen.AsBoolean;
end;

procedure TTukarKomponenFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteTukarKomponen.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateTukarKomponen.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TTukarKomponenFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TTukarKomponenFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Tukar Komponen dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    viewSJQ.Close;
    viewSJQ.Open;
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TTukarKomponenFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TTukarKomponenFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TTukarKomponenFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Tukar Komponen '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Tukar Komponen telah dihapus.',mtInformation,[mbOK],0);
    ViewSJQ.Close;
    ViewSJQ.Open;
    KodeEdit.SetFocus;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Tukar Komponen pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TTukarKomponenFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  {  SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;   }
end;

procedure TTukarKomponenFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterQtgl_order.AsDateTime :=  SOQtgl_order.AsDateTime;
      //MasterQnama_pelanggan.AsString := SOQnama_pelanggan.AsString;
      //MasterQtlp_pelanggan.AsString :=    SOQtlp_pelanggan.AsString;
      //MasterQalamat_pelanggan.AsString :=    SOQalamat_pelanggan.asString;
      //MasterQkode_armada.AsString :=  SOQkode_armada.AsString;
      //MasterQekor.AsString :=  SOQekor_armada.AsString;
      //MasterQsopir.AsString :=  SOQkodesopir.AsString;
      //MasterQplat_armada.AsString := SOQplat_armada.AsString;
      //MasterQnama_sopir.AsString := SOQnama_sopir.AsString;
      //MasterQmuat.AsString := SOQmuat.AsString;
      //MasterQbongkar.AsString := SOQbongkar.AsString;
      //MasterQdaript.AsString := SOQdaript.AsString;
      //MasterQdarialamat.AsString := SOQdarialamat.AsString;
      //MasterQkept.AsString := SOQkept.AsString;
      //MasterQkealamat.AsString := SOQkealamat.AsString;
      //MasterQkode_rute.AsString := SOQkode_rute.AsString;
    end;
    DropDownFm.Release;     }
end;

procedure TTukarKomponenFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    buttonCetak.Enabled:=true;
    kodeedit.Text:=viewSJQKode.AsString;
    KodeEditExit(self);
    MasterVGrid.Enabled:=True;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateTukarKomponen.AsBoolean;
    If MasterQStatus.Value='PENDING' then
      AppButton.Enabled:=menuutamafm.UserQApprovalTukarKomponen.AsBoolean
    else
      AppButton.Enabled:=menuutamafm.UserQApprovalTukarKomponen.AsBoolean;
end;

procedure TTukarKomponenFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {   jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;  }
    DropDownFm.Release;
end;

procedure TTukarKomponenFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Surat Jalan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
      Crpe1.Connect.UserID:='sa';
      Crpe1.Connect.Password:='sa';
      Crpe1.Connect.ServerName:='';
      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      viewSJQ.Close;
      viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TTukarKomponenFm.MasterVGridDariArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  ArmadaDropDownFm:=TArmadaDropDownFm.Create(self);
  if ArmadaDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;

    if MasterQKeArmada.AsString<>''then
      begin
        if ArmadaDropDownFm.kode=MasterQKeArmada.AsString then
        begin
          ShowMessage('Armada Tidak Boleh Sama');
        end
        else
        begin
          MasterQDariArmada.AsString:= ArmadaDropDownFm.kode;
          //MasterQNoBody_dari.AsString:= ArmadaQNoBody.AsString;
        end;
      end
      else
      begin
        MasterQDariArmada.AsString:= ArmadaDropDownFm.kode;
        //MasterQNoBody_dari.AsString:= ArmadaQNoBody.AsString;
      end;

//    MasterQArmada.AsString:= ArmadaDropDownFm.kode;
  end;
  ArmadaDropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridKeArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      if MasterQDariArmada.AsString<>'' then
      begin
        if ArmadaQKode.AsString=MasterQDariArmada.AsString then
        begin
          ShowMessage('Armada Harus Berbeda');
        end
        else
        begin
          MasterQkeArmada.AsString:= ArmadaQKode.AsString;
          MasterQNoBody_ke.AsString:= ArmadaQNoBody.AsString;
        end;
      end
      else
      begin
        MasterQkeArmada.AsString:= ArmadaQKode.AsString;
        MasterQNoBody_ke.AsString:= ArmadaQNoBody.AsString;
      end;

    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridBarangDariEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarangDari.AsString:= BarangDropDownFm.kode;
    MasterQBarangKe.AsString:= BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridBarangKeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarangKe.AsString:= BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    lpbQ.Close;
    lpbQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,lpbQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQLaporanPerbaikan.AsString:= lpbQKode.AsString;
      MasterQwaktumulai_lpb.AsString := lpbQWaktuMulai.AsString;
      MasterQwaktuselesai_lpb.AsString := lpbQWaktuSelesai.AsString;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridLaporanPerawatanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  lprQ.Close;
    lprQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,lprQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQLaporanPerawatan.AsString:= lprQKode.AsString;
      //MasterQwaktumulai_lpr.AsString := lprQWaktuMulai.AsString;
      //MasterQwaktuselesai_lpr.AsString := lprQWaktuSelesai.AsString;
    end;
    DropDownFm.Release;   }
end;

procedure TTukarKomponenFm.cxDBEditorRow10EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPeminta.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TTukarKomponenFm.cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SPKDropDownFm:=TSPKDropDownFm.Create(self);
  if SPKDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSPK.AsString:= SPKDropDownFm.kode;
    MasterQKeArmada.AsString:= SPKDropDownFm.armada;
  end;
  SPKDropDownFm.Release;
end;

procedure TTukarKomponenFm.MasterVGridDBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SPKDropDownFm:=TSPKDropDownFm.Create(self);
  if SPKDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSPK.AsString:= SPKDropDownFm.kode;
    MasterQKeArmada.AsString:= SPKDropDownFm.armada;
  end;
  SPKDropDownFm.Release;
end;

procedure TTukarKomponenFm.AppButtonClick(Sender: TObject);
begin
  //MasterVGrid.Enabled:=True;
  MasterQ.Edit;
  MasterQStatus.AsString:='APPROVED';
  SaveBtnClick(self);
  AppButton.Enabled:=menuutamafm.UserQApprovalTukarKomponen.AsBoolean;
end;

procedure TTukarKomponenFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  ViewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewSJQ.Open;
end;

procedure TTukarKomponenFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
  viewSJQ.Close;
  ViewSJQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  ViewSJQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  ViewSJQ.Open;
end;

end.
