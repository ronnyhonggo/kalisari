object TukarKomponenKanibalFm: TTukarKomponenKanibalFm
  Left = 322
  Top = 85
  Width = 818
  Height = 620
  Caption = 'Tukar Komponen kanibal'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 802
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 495
    Width = 802
    Height = 47
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 188
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object buttonCetak: TcxButton
      Left = 424
      Top = 10
      Width = 75
      Height = 25
      Caption = 'CETAK'
      Enabled = False
      TabOrder = 2
      Visible = False
      OnClick = buttonCetakClick
    end
    object DELETEbtn: TcxButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 3
      OnClick = DELETEbtnClick
    end
    object cxGroupBox1: TcxGroupBox
      Left = 504
      Top = 1
      Align = alRight
      Alignment = alTopRight
      Caption = 'Tanggal History'
      TabOrder = 4
      Height = 45
      Width = 297
      object cxDateEdit1: TcxDateEdit
        Left = 16
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit1PropertiesChange
        TabOrder = 0
        Width = 121
      end
      object cxDateEdit2: TcxDateEdit
        Left = 168
        Top = 16
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.OnChange = cxDateEdit2PropertiesChange
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 142
        Top = 18
        Caption = 's/d'
      end
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 425
    Height = 175
    Align = alLeft
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 201
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridDBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridDBEditorRow1EditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'SPK'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridDBEditorRow2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailTindakan'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridDariBarang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridDariBarangEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'DariBarang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridID_BarangKanibal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ID_BarangKanibal'
      Properties.Options.Editing = False
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object MasterVGridDetailBarangKanibal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DetailBarangKanibal'
      Properties.Options.Editing = False
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object MasterVGridBarang: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridBarangEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Barang'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridNamaBarangAsal: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaBarangAsal'
      Properties.Options.Editing = False
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 542
    Width = 802
    Height = 39
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 223
    Width = 802
    Height = 272
    Align = alBottom
    BevelOuter = bvRaised
    BevelWidth = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = SJDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableViewKode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 84
      end
      object cxGrid1DBTableViewTglTukar: TcxGridDBColumn
        DataBinding.FieldName = 'TglTukar'
        Width = 89
      end
      object cxGrid1DBTableViewBarangKanibal: TcxGridDBColumn
        DataBinding.FieldName = 'ID_BarangKanibal'
        Width = 133
      end
      object cxGrid1DBTableViewBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarangAsal'
        Options.SortByDisplayText = isbtOn
        Width = 224
      end
    end
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Bands = <
        item
        end>
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 425
    Top = 48
    Width = 377
    Height = 175
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 201
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 5
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object cxDBVerticalGrid1KeArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KeArmada'
      Properties.Options.Editing = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1NoBodyArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NoBodyArmada'
      Properties.Options.Editing = False
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PlatNoArmada'
      Properties.Options.Editing = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1TglTukar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglTukar'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 3
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1Peminta: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1PemintaEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Peminta'
      Styles.Content = MenuUtamaFm.cxStyle5
      ID = 4
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1NamaPeminta: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPeminta'
      Properties.Options.Editing = False
      ID = 5
      ParentID = 4
      Index = 0
      Version = 1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select k.*, a.NoBody as NoBodyArmada,b1.IDBarang as ID_BarangKan' +
        'ibal, b2.nama as NamaBarangAsal, lp.TindakanPerbaikan as Tindaka' +
        'nPerbaikan'
      
        'from AmbilBarangKanibal k left outer join LaporanPerbaikan lp on' +
        ' lp.Kode=k.LaporanPerbaikan, Armada a, BarangKanibal b1, Barang ' +
        'b2'
      
        'where k.KeArmada=a.Kode and k.DariBarang=b1.Kode and k.barang = ' +
        'b2.kode')
    UpdateObject = MasterUS
    Left = 259
    Top = 15
    object MasterQID_BarangKanibal: TStringField
      FieldKind = fkLookup
      FieldName = 'ID_BarangKanibal'
      LookupDataSet = BarangKanibalQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'IDBarang'
      KeyFields = 'DariBarang'
      Size = 50
      Lookup = True
    end
    object MasterQDetailBarangKanibal: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailBarangKanibal'
      LookupDataSet = BarangKanibalQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'DetailBarangKanibal'
      KeyFields = 'DariBarang'
      Lookup = True
    end
    object MasterQNamaBarangAsal: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarangAsal'
      LookupDataSet = barangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Required = True
      Size = 50
      Lookup = True
    end
    object MasterQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
    end
    object MasterQPeminta: TStringField
      FieldName = 'Peminta'
      Size = 10
    end
    object MasterQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object MasterQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Lookup = True
    end
    object MasterQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object MasterQDetailTindakan: TStringField
      FieldKind = fkLookup
      FieldName = 'DetailTindakan'
      LookupDataSet = VSPKQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'DetailTindakan'
      KeyFields = 'SPK'
      Size = 200
      Lookup = True
    end
    object MasterQPlatNoArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'KeArmada'
      Size = 30
      Lookup = True
    end
    object MasterQPP: TStringField
      FieldKind = fkLookup
      FieldName = 'PP'
      LookupDataSet = lpbQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PP'
      KeyFields = 'LaporanPerbaikan'
      Size = 10
      Lookup = True
    end
    object MasterQArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'Armada'
      LookupDataSet = PPQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Armada'
      KeyFields = 'PP'
      Size = 10
      Lookup = True
    end
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQDariBarang: TStringField
      FieldName = 'DariBarang'
      Size = 10
    end
    object MasterQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object MasterQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object MasterQNoBodyArmada: TStringField
      FieldName = 'NoBodyArmada'
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 292
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, DariBarang, KeArmada, Barang, TglTukar, Peminta, SP' +
        'K, LaporanPerbaikan, CreateDate, TglEntry, Operator, CreateBy'#13#10'f' +
        'rom AmbilBarangKanibal'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update AmbilBarangKanibal'
      'set'
      '  Kode = :Kode,'
      '  DariBarang = :DariBarang,'
      '  KeArmada = :KeArmada,'
      '  Barang = :Barang,'
      '  TglTukar = :TglTukar,'
      '  Peminta = :Peminta,'
      '  SPK = :SPK,'
      '  LaporanPerbaikan = :LaporanPerbaikan,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  CreateBy = :CreateBy'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into AmbilBarangKanibal'
      
        '  (Kode, DariBarang, KeArmada, Barang, TglTukar, Peminta, SPK, L' +
        'aporanPerbaikan, CreateDate, TglEntry, Operator, CreateBy)'
      'values'
      
        '  (:Kode, :DariBarang, :KeArmada, :Barang, :TglTukar, :Peminta, ' +
        ':SPK, :LaporanPerbaikan, :CreateDate, :TglEntry, :Operator, :Cre' +
        'ateBy)')
    DeleteSQL.Strings = (
      'delete from AmbilBarangKanibal'
      'where'
      '  Kode = :OLD_Kode')
    Left = 324
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from ambilbarangkanibal order by kode desc')
    Left = 222
    Top = 15
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'SELECT SO.KODENOTA AS NOSO, P.NAMAPT AS NAMA_PELANGGAN, P.ALAMAT' +
        ' AS ALAMAT_PELANGGAN, P.NOTELP AS TLP_PELANGGAN , SOPIR.NAMA AS ' +
        'NAMA_SOPIR, A.EKOR AS EKOR_ARMADA , SO.TGL AS TGL_ORDER, A.KODE ' +
        'AS KODE_ARMADA, A.PLATNO AS PLAT_ARMADA, SOPIR.KODE AS KODESOPIR' +
        ' , R.KODE AS KODE_RUTE, R.MUAT, R.BONGKAR , SO.DARIPT , SO.DARIA' +
        'LAMAT, SO.KEPT, SO.KEALAMAT, SJ.KODENOTA AS CHECKER FROM RUTE R,' +
        ' PELANGGAN P , MASTERSO SO LEFT OUTER JOIN MASTERSJ SJ ON SJ.NOS' +
        'O=SO.KODENOTA, ARMADA A , SOPIR WHERE P.KODE=SO.PELANGGAN AND SO' +
        '.ARMADA=A.KODE AND A.SOPIR=SOPIR.KODE AND R.KODE=SO.RUTE AND SO.' +
        'KODENOTA<>'#39'FINISHED'#39' AND SJ.KODENOTA IS NULL')
    Left = 721
    Top = 327
    object SOQNOSO: TStringField
      FieldName = 'NOSO'
      Required = True
      Size = 10
    end
    object SOQNAMA_PELANGGAN: TStringField
      FieldName = 'NAMA_PELANGGAN'
      Required = True
      Size = 50
    end
    object SOQALAMAT_PELANGGAN: TStringField
      FieldName = 'ALAMAT_PELANGGAN'
      Size = 100
    end
    object SOQTLP_PELANGGAN: TStringField
      FieldName = 'TLP_PELANGGAN'
      Size = 15
    end
    object SOQNAMA_SOPIR: TStringField
      FieldName = 'NAMA_SOPIR'
      Required = True
      Size = 50
    end
    object SOQEKOR_ARMADA: TStringField
      FieldName = 'EKOR_ARMADA'
      Visible = False
      Size = 10
    end
    object SOQTGL_ORDER: TDateTimeField
      FieldName = 'TGL_ORDER'
      Required = True
    end
    object SOQKODE_ARMADA: TStringField
      FieldName = 'KODE_ARMADA'
      Required = True
      Visible = False
      Size = 10
    end
    object SOQPLAT_ARMADA: TStringField
      FieldName = 'PLAT_ARMADA'
      Required = True
      Size = 10
    end
    object SOQKODESOPIR: TStringField
      FieldName = 'KODESOPIR'
      Required = True
      Visible = False
      Size = 10
    end
    object SOQKODE_RUTE: TStringField
      FieldName = 'KODE_RUTE'
      Required = True
      Visible = False
      Size = 10
    end
    object SOQMUAT: TStringField
      FieldName = 'MUAT'
      Required = True
      Visible = False
      Size = 50
    end
    object SOQBONGKAR: TStringField
      FieldName = 'BONGKAR'
      Required = True
      Visible = False
      Size = 50
    end
    object SOQDARIPT: TMemoField
      FieldName = 'DARIPT'
      Visible = False
      BlobType = ftMemo
    end
    object SOQDARIALAMAT: TMemoField
      FieldName = 'DARIALAMAT'
      Visible = False
      BlobType = ftMemo
    end
    object SOQKEPT: TMemoField
      FieldName = 'KEPT'
      Visible = False
      BlobType = ftMemo
    end
    object SOQKEALAMAT: TMemoField
      FieldName = 'KEALAMAT'
      Visible = False
      BlobType = ftMemo
    end
    object SOQCHECKER: TStringField
      FieldName = 'CHECKER'
      Visible = False
      Size = 10
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select a.*,p.nama from armada a left outer join pegawai p on p.k' +
        'ode=a.sopir')
    Left = 81
    Top = 311
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
      Required = True
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
      Required = True
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQnama: TStringField
      FieldName = 'nama'
      Size = 50
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39)
    Left = 761
    Top = 327
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object SJDS: TDataSource
    DataSet = AmbilBarangKanibalQ
    Left = 612
    Top = 6
  end
  object sopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from sopir')
    Left = 657
    Top = 7
    object sopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object sopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object sopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object sopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object sopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object sopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object sopirQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object sopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object Crpe1: TCrpe
    About = 'Version and Credits...'
    DesignControls = 'Design-Time Window Controls'
    Version.Crpe.Major = 0
    Version.Crpe.Minor = 0
    Version.Crpe.Release = 0
    Version.Crpe.Build = 0
    Version.Report.Major = 0
    Version.Report.Minor = 0
    Version.Report.Letter = #0
    Version.Windows.Platform = 'NT'
    Version.Windows.Major = 6
    Version.Windows.Minor = 2
    Version.Windows.Build = '9200'
    TempPath = 'C:\Users\Ronny\AppData\Local\Temp\'
    PrintDate.Day = 0
    PrintDate.Month = 0
    PrintDate.Year = 0
    Subreports.Number = 0
    Subreports.Item.Top = -1
    Subreports.Item.Left = -1
    Subreports.Item.Width = -1
    Subreports.Item.Height = -1
    Subreports.Item.Border.Left = lsNone
    Subreports.Item.Border.Right = lsNone
    Subreports.Item.Border.Top = lsNone
    Subreports.Item.Border.Bottom = lsNone
    Subreports.Item.Border.TightHorizontal = False
    Subreports.Item.Border.DropShadow = False
    Subreports.Item.Border.ForeColor = clNone
    Subreports.Item.Border.BackgroundColor = clNone
    Subreports.Item.NLinks = 0
    GroupSortFields.Number = -1
    Groups.Number = -1
    Groups.Item.CustomizeGroupName = False
    ParamFields.AllowDialog = True
    ParamFields.Item.Top = -1
    ParamFields.Item.Left = -1
    ParamFields.Item.Width = -1
    ParamFields.Item.Height = -1
    ParamFields.Item.Border.Left = lsNone
    ParamFields.Item.Border.Right = lsNone
    ParamFields.Item.Border.Top = lsNone
    ParamFields.Item.Border.Bottom = lsNone
    ParamFields.Item.Border.TightHorizontal = False
    ParamFields.Item.Border.DropShadow = False
    ParamFields.Item.Border.ForeColor = clNone
    ParamFields.Item.Border.BackgroundColor = clNone
    ParamFields.Item.Format.Alignment = haDefault
    ParamFields.Item.Format.SuppressIfDuplicated = False
    ParamFields.Item.Format.CanGrow = False
    ParamFields.Item.Format.MaxNLines = 0
    ParamFields.Item.Format.Field.Number.CurrencySymbol = '$'
    ParamFields.Item.Format.Field.Number.ThousandSymbol = ','
    ParamFields.Item.Format.Field.Number.DecimalSymbol = '.'
    ParamFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    ParamFields.Item.Format.Field.Date.FirstSeparator = '/'
    ParamFields.Item.Format.Field.Date.SecondSeparator = '/'
    ParamFields.Item.Format.Field.Time.AMString = 'AM'
    ParamFields.Item.Format.Field.Time.PMString = 'PM'
    ParamFields.Item.Format.Field.Time.HourMinSeparator = ':'
    ParamFields.Item.Format.Field.Time.MinSecSeparator = ':'
    ParamFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    ParamFields.Item.Font.Charset = DEFAULT_CHARSET
    ParamFields.Item.Font.Color = clWindowText
    ParamFields.Item.Font.Height = -11
    ParamFields.Item.Font.Name = 'MS Sans Serif'
    ParamFields.Item.Font.Style = []
    ParamFields.Item.HiliteConditions.Item.FontColor = clNone
    ParamFields.Item.HiliteConditions.Item.Background = clNone
    ParamFields.Item.ParamType = pfNoValue
    ParamFields.Item.ParamSource = psReport
    ParamFields.Item.Info.AllowNull = True
    ParamFields.Item.Info.AllowEditing = True
    ParamFields.Item.Info.AllowMultipleValues = False
    ParamFields.Item.Info.ValueType = vtDiscrete
    ParamFields.Item.Info.PartOfGroup = False
    ParamFields.Item.Info.MutuallyExclusiveGroup = True
    ParamFields.Item.Info.GroupNum = -1
    ParamFields.Item.ValueLimit = False
    ParamFields.Item.Ranges.Item.Bounds = IncludeStartAndEnd
    ParamFields.Item.NeedsCurrentValue = False
    ParamFields.Item.IsLinked = False
    Formulas.Item.Top = -1
    Formulas.Item.Left = -1
    Formulas.Item.Width = -1
    Formulas.Item.Height = -1
    Formulas.Item.Border.Left = lsNone
    Formulas.Item.Border.Right = lsNone
    Formulas.Item.Border.Top = lsNone
    Formulas.Item.Border.Bottom = lsNone
    Formulas.Item.Border.TightHorizontal = False
    Formulas.Item.Border.DropShadow = False
    Formulas.Item.Border.ForeColor = clNone
    Formulas.Item.Border.BackgroundColor = clNone
    Formulas.Item.Format.Alignment = haDefault
    Formulas.Item.Format.SuppressIfDuplicated = False
    Formulas.Item.Format.CanGrow = False
    Formulas.Item.Format.MaxNLines = 0
    Formulas.Item.Format.Field.Number.CurrencySymbol = '$'
    Formulas.Item.Format.Field.Number.ThousandSymbol = ','
    Formulas.Item.Format.Field.Number.DecimalSymbol = '.'
    Formulas.Item.Format.Field.Number.ShowZeroValueAs = '0'
    Formulas.Item.Format.Field.Date.FirstSeparator = '/'
    Formulas.Item.Format.Field.Date.SecondSeparator = '/'
    Formulas.Item.Format.Field.Time.AMString = 'AM'
    Formulas.Item.Format.Field.Time.PMString = 'PM'
    Formulas.Item.Format.Field.Time.HourMinSeparator = ':'
    Formulas.Item.Format.Field.Time.MinSecSeparator = ':'
    Formulas.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    Formulas.Item.Font.Charset = DEFAULT_CHARSET
    Formulas.Item.Font.Color = clWindowText
    Formulas.Item.Font.Height = -11
    Formulas.Item.Font.Name = 'MS Sans Serif'
    Formulas.Item.Font.Style = []
    Formulas.Item.HiliteConditions.Item.FontColor = clNone
    Formulas.Item.HiliteConditions.Item.Background = clNone
    AreaFormat.Item.NSections = 0
    SectionSize.Item.Height = -1
    SectionSize.Item.Width = -1
    SQLExpressions.Item.Top = -1
    SQLExpressions.Item.Left = -1
    SQLExpressions.Item.Width = -1
    SQLExpressions.Item.Height = -1
    SQLExpressions.Item.Border.Left = lsNone
    SQLExpressions.Item.Border.Right = lsNone
    SQLExpressions.Item.Border.Top = lsNone
    SQLExpressions.Item.Border.Bottom = lsNone
    SQLExpressions.Item.Border.TightHorizontal = False
    SQLExpressions.Item.Border.DropShadow = False
    SQLExpressions.Item.Border.ForeColor = clNone
    SQLExpressions.Item.Border.BackgroundColor = clNone
    SQLExpressions.Item.Format.Alignment = haDefault
    SQLExpressions.Item.Format.SuppressIfDuplicated = False
    SQLExpressions.Item.Format.CanGrow = False
    SQLExpressions.Item.Format.MaxNLines = 0
    SQLExpressions.Item.Format.Field.Number.CurrencySymbol = '$'
    SQLExpressions.Item.Format.Field.Number.ThousandSymbol = ','
    SQLExpressions.Item.Format.Field.Number.DecimalSymbol = '.'
    SQLExpressions.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SQLExpressions.Item.Format.Field.Date.FirstSeparator = '/'
    SQLExpressions.Item.Format.Field.Date.SecondSeparator = '/'
    SQLExpressions.Item.Format.Field.Time.AMString = 'AM'
    SQLExpressions.Item.Format.Field.Time.PMString = 'PM'
    SQLExpressions.Item.Format.Field.Time.HourMinSeparator = ':'
    SQLExpressions.Item.Format.Field.Time.MinSecSeparator = ':'
    SQLExpressions.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SQLExpressions.Item.Font.Charset = DEFAULT_CHARSET
    SQLExpressions.Item.Font.Color = clWindowText
    SQLExpressions.Item.Font.Height = -11
    SQLExpressions.Item.Font.Name = 'MS Sans Serif'
    SQLExpressions.Item.Font.Style = []
    SQLExpressions.Item.HiliteConditions.Item.FontColor = clNone
    SQLExpressions.Item.HiliteConditions.Item.Background = clNone
    LogOnInfo.Item.Table = -1
    SessionInfo.Table = -1
    SessionInfo.Item.Propagate = True
    ExportOptions.Excel.Constant = 36.000000000000000000
    ExportOptions.Excel.WorksheetFunctions = False
    ExportOptions.Excel.FirstPage = 1
    ExportOptions.Excel.LastPage = 1
    ExportOptions.HTML.PageNavigator = True
    ExportOptions.HTML.SeparatePages = True
    ExportOptions.HTML.FirstPage = 1
    ExportOptions.HTML.LastPage = 1
    ExportOptions.RTF.FirstPage = 1
    ExportOptions.RTF.LastPage = 1
    ExportOptions.Word.FirstPage = 1
    ExportOptions.Word.LastPage = 1
    ExportOptions.PDF.FirstPage = 1
    ExportOptions.PDF.LastPage = 1
    ExportOptions.Text.StringDelimiter = '"'
    ExportOptions.Text.FieldSeparator = ','
    Lines.Item.LineStyle = lsNone
    Lines.Item.Left = -1
    Lines.Item.Right = -1
    Lines.Item.Width = -1
    Lines.Item.Top = -1
    Lines.Item.Bottom = -1
    Lines.Item.Color = clNone
    Lines.Item.Extend = False
    Lines.Item.Suppress = False
    Boxes.Number = -1
    Pictures.Item.Top = -1
    Pictures.Item.Left = -1
    Pictures.Item.Width = -1
    Pictures.Item.Height = -1
    Pictures.Item.Border.Left = lsNone
    Pictures.Item.Border.Right = lsNone
    Pictures.Item.Border.Top = lsNone
    Pictures.Item.Border.Bottom = lsNone
    Pictures.Item.Border.TightHorizontal = False
    Pictures.Item.Border.DropShadow = False
    Pictures.Item.Border.ForeColor = clNone
    Pictures.Item.Border.BackgroundColor = clNone
    Pictures.Item.CropLeft = -1
    Pictures.Item.CropRight = -1
    Pictures.Item.CropTop = -1
    Pictures.Item.CropBottom = -1
    TextObjects.Item.Top = -1
    TextObjects.Item.Left = -1
    TextObjects.Item.Width = -1
    TextObjects.Item.Height = -1
    TextObjects.Item.Border.Left = lsNone
    TextObjects.Item.Border.Right = lsNone
    TextObjects.Item.Border.Top = lsNone
    TextObjects.Item.Border.Bottom = lsNone
    TextObjects.Item.Border.TightHorizontal = False
    TextObjects.Item.Border.DropShadow = False
    TextObjects.Item.Border.ForeColor = clNone
    TextObjects.Item.Border.BackgroundColor = clNone
    TextObjects.Item.Format.Alignment = haDefault
    TextObjects.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.Format.CanGrow = False
    TextObjects.Item.Format.MaxNLines = 0
    TextObjects.Item.Format.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.Font.Charset = DEFAULT_CHARSET
    TextObjects.Item.Font.Color = clWindowText
    TextObjects.Item.Font.Height = -11
    TextObjects.Item.Font.Name = 'MS Sans Serif'
    TextObjects.Item.Font.Style = []
    TextObjects.Item.Paragraphs.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.IndentFirstLine = 0
    TextObjects.Item.Paragraphs.Item.IndentLeft = 0
    TextObjects.Item.Paragraphs.Item.IndentRight = 0
    TextObjects.Item.Paragraphs.Item.TextStart = 0
    TextObjects.Item.Paragraphs.Item.TextEnd = 0
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Alignment = haDefault
    TextObjects.Item.Paragraphs.Item.TabStops.Item.Offset = -1
    TextObjects.Item.EmbeddedFields.Item.FieldObjectType = oftNone
    TextObjects.Item.EmbeddedFields.Item.FieldType = fvUnknown
    TextObjects.Item.EmbeddedFields.Item.TextStart = 0
    TextObjects.Item.EmbeddedFields.Item.TextEnd = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Alignment = haDefault
    TextObjects.Item.EmbeddedFields.Item.Format.SuppressIfDuplicated = False
    TextObjects.Item.EmbeddedFields.Item.Format.CanGrow = False
    TextObjects.Item.EmbeddedFields.Item.Format.MaxNLines = 0
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.CurrencySymbol = '$'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ThousandSymbol = ','
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.DecimalSymbol = '.'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.FirstSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Date.SecondSeparator = '/'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.AMString = 'AM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.PMString = 'PM'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.HourMinSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Time.MinSecSeparator = ':'
    TextObjects.Item.EmbeddedFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    TextObjects.Item.EmbeddedFields.Item.Border.Left = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Right = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Top = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.Bottom = lsNone
    TextObjects.Item.EmbeddedFields.Item.Border.TightHorizontal = False
    TextObjects.Item.EmbeddedFields.Item.Border.DropShadow = False
    TextObjects.Item.EmbeddedFields.Item.Border.ForeColor = clNone
    TextObjects.Item.EmbeddedFields.Item.Border.BackgroundColor = clNone
    TextObjects.Item.TextSize = -1
    TextObjects.Item.TextHeight = -1
    OleObjects.Item.Top = -1
    OleObjects.Item.Left = -1
    OleObjects.Item.Width = -1
    OleObjects.Item.Height = -1
    OleObjects.Item.Border.Left = lsNone
    OleObjects.Item.Border.Right = lsNone
    OleObjects.Item.Border.Top = lsNone
    OleObjects.Item.Border.Bottom = lsNone
    OleObjects.Item.Border.TightHorizontal = False
    OleObjects.Item.Border.DropShadow = False
    OleObjects.Item.Border.ForeColor = clNone
    OleObjects.Item.Border.BackgroundColor = clNone
    CrossTabs.Item.Top = -1
    CrossTabs.Item.Left = -1
    CrossTabs.Item.Width = -1
    CrossTabs.Item.Height = -1
    CrossTabs.Item.Border.Left = lsNone
    CrossTabs.Item.Border.Right = lsNone
    CrossTabs.Item.Border.Top = lsNone
    CrossTabs.Item.Border.Bottom = lsNone
    CrossTabs.Item.Border.TightHorizontal = False
    CrossTabs.Item.Border.DropShadow = False
    CrossTabs.Item.Border.ForeColor = clNone
    CrossTabs.Item.Border.BackgroundColor = clNone
    Maps.Item.Top = -1
    Maps.Item.Left = -1
    Maps.Item.Width = -1
    Maps.Item.Height = -1
    Maps.Item.Border.Left = lsNone
    Maps.Item.Border.Right = lsNone
    Maps.Item.Border.Top = lsNone
    Maps.Item.Border.Bottom = lsNone
    Maps.Item.Border.TightHorizontal = False
    Maps.Item.Border.DropShadow = False
    Maps.Item.Border.ForeColor = clNone
    Maps.Item.Border.BackgroundColor = clNone
    OLAPCubes.Item.Top = -1
    OLAPCubes.Item.Left = -1
    OLAPCubes.Item.Width = -1
    OLAPCubes.Item.Height = -1
    OLAPCubes.Item.Border.Left = lsNone
    OLAPCubes.Item.Border.Right = lsNone
    OLAPCubes.Item.Border.Top = lsNone
    OLAPCubes.Item.Border.Bottom = lsNone
    OLAPCubes.Item.Border.TightHorizontal = False
    OLAPCubes.Item.Border.DropShadow = False
    OLAPCubes.Item.Border.ForeColor = clNone
    OLAPCubes.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Top = -1
    DatabaseFields.Item.Left = -1
    DatabaseFields.Item.Width = -1
    DatabaseFields.Item.Height = -1
    DatabaseFields.Item.Border.Left = lsNone
    DatabaseFields.Item.Border.Right = lsNone
    DatabaseFields.Item.Border.Top = lsNone
    DatabaseFields.Item.Border.Bottom = lsNone
    DatabaseFields.Item.Border.TightHorizontal = False
    DatabaseFields.Item.Border.DropShadow = False
    DatabaseFields.Item.Border.ForeColor = clNone
    DatabaseFields.Item.Border.BackgroundColor = clNone
    DatabaseFields.Item.Format.Alignment = haDefault
    DatabaseFields.Item.Format.SuppressIfDuplicated = False
    DatabaseFields.Item.Format.CanGrow = False
    DatabaseFields.Item.Format.MaxNLines = 0
    DatabaseFields.Item.Format.Field.Number.CurrencySymbol = '$'
    DatabaseFields.Item.Format.Field.Number.ThousandSymbol = ','
    DatabaseFields.Item.Format.Field.Number.DecimalSymbol = '.'
    DatabaseFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    DatabaseFields.Item.Format.Field.Date.FirstSeparator = '/'
    DatabaseFields.Item.Format.Field.Date.SecondSeparator = '/'
    DatabaseFields.Item.Format.Field.Time.AMString = 'AM'
    DatabaseFields.Item.Format.Field.Time.PMString = 'PM'
    DatabaseFields.Item.Format.Field.Time.HourMinSeparator = ':'
    DatabaseFields.Item.Format.Field.Time.MinSecSeparator = ':'
    DatabaseFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    DatabaseFields.Item.Font.Charset = DEFAULT_CHARSET
    DatabaseFields.Item.Font.Color = clWindowText
    DatabaseFields.Item.Font.Height = -11
    DatabaseFields.Item.Font.Name = 'MS Sans Serif'
    DatabaseFields.Item.Font.Style = []
    DatabaseFields.Item.HiliteConditions.Item.FontColor = clNone
    DatabaseFields.Item.HiliteConditions.Item.Background = clNone
    SummaryFields.Item.Top = -1
    SummaryFields.Item.Left = -1
    SummaryFields.Item.Width = -1
    SummaryFields.Item.Height = -1
    SummaryFields.Item.Border.Left = lsNone
    SummaryFields.Item.Border.Right = lsNone
    SummaryFields.Item.Border.Top = lsNone
    SummaryFields.Item.Border.Bottom = lsNone
    SummaryFields.Item.Border.TightHorizontal = False
    SummaryFields.Item.Border.DropShadow = False
    SummaryFields.Item.Border.ForeColor = clNone
    SummaryFields.Item.Border.BackgroundColor = clNone
    SummaryFields.Item.Format.Alignment = haDefault
    SummaryFields.Item.Format.SuppressIfDuplicated = False
    SummaryFields.Item.Format.CanGrow = False
    SummaryFields.Item.Format.MaxNLines = 0
    SummaryFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SummaryFields.Item.Format.Field.Number.ThousandSymbol = ','
    SummaryFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SummaryFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SummaryFields.Item.Format.Field.Date.FirstSeparator = '/'
    SummaryFields.Item.Format.Field.Date.SecondSeparator = '/'
    SummaryFields.Item.Format.Field.Time.AMString = 'AM'
    SummaryFields.Item.Format.Field.Time.PMString = 'PM'
    SummaryFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SummaryFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SummaryFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SummaryFields.Item.Font.Charset = DEFAULT_CHARSET
    SummaryFields.Item.Font.Color = clWindowText
    SummaryFields.Item.Font.Height = -11
    SummaryFields.Item.Font.Name = 'MS Sans Serif'
    SummaryFields.Item.Font.Style = []
    SummaryFields.Item.HiliteConditions.Item.FontColor = clNone
    SummaryFields.Item.HiliteConditions.Item.Background = clNone
    SpecialFields.Number = -1
    SpecialFields.Item.Top = -1
    SpecialFields.Item.Left = -1
    SpecialFields.Item.Width = -1
    SpecialFields.Item.Height = -1
    SpecialFields.Item.Border.Left = lsNone
    SpecialFields.Item.Border.Right = lsNone
    SpecialFields.Item.Border.Top = lsNone
    SpecialFields.Item.Border.Bottom = lsNone
    SpecialFields.Item.Border.TightHorizontal = False
    SpecialFields.Item.Border.DropShadow = False
    SpecialFields.Item.Border.ForeColor = clNone
    SpecialFields.Item.Border.BackgroundColor = clNone
    SpecialFields.Item.Format.Alignment = haDefault
    SpecialFields.Item.Format.SuppressIfDuplicated = False
    SpecialFields.Item.Format.CanGrow = False
    SpecialFields.Item.Format.MaxNLines = 0
    SpecialFields.Item.Format.Field.Number.CurrencySymbol = '$'
    SpecialFields.Item.Format.Field.Number.ThousandSymbol = ','
    SpecialFields.Item.Format.Field.Number.DecimalSymbol = '.'
    SpecialFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    SpecialFields.Item.Format.Field.Date.FirstSeparator = '/'
    SpecialFields.Item.Format.Field.Date.SecondSeparator = '/'
    SpecialFields.Item.Format.Field.Time.AMString = 'AM'
    SpecialFields.Item.Format.Field.Time.PMString = 'PM'
    SpecialFields.Item.Format.Field.Time.HourMinSeparator = ':'
    SpecialFields.Item.Format.Field.Time.MinSecSeparator = ':'
    SpecialFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    SpecialFields.Item.Font.Charset = DEFAULT_CHARSET
    SpecialFields.Item.Font.Color = clWindowText
    SpecialFields.Item.Font.Height = -11
    SpecialFields.Item.Font.Name = 'MS Sans Serif'
    SpecialFields.Item.Font.Style = []
    SpecialFields.Item.HiliteConditions.Item.FontColor = clNone
    SpecialFields.Item.HiliteConditions.Item.Background = clNone
    GroupNameFields.Number = -1
    GroupNameFields.Item.Top = -1
    GroupNameFields.Item.Left = -1
    GroupNameFields.Item.Width = -1
    GroupNameFields.Item.Height = -1
    GroupNameFields.Item.Border.Left = lsNone
    GroupNameFields.Item.Border.Right = lsNone
    GroupNameFields.Item.Border.Top = lsNone
    GroupNameFields.Item.Border.Bottom = lsNone
    GroupNameFields.Item.Border.TightHorizontal = False
    GroupNameFields.Item.Border.DropShadow = False
    GroupNameFields.Item.Border.ForeColor = clNone
    GroupNameFields.Item.Border.BackgroundColor = clNone
    GroupNameFields.Item.Format.Alignment = haDefault
    GroupNameFields.Item.Format.SuppressIfDuplicated = False
    GroupNameFields.Item.Format.CanGrow = False
    GroupNameFields.Item.Format.MaxNLines = 0
    GroupNameFields.Item.Format.Field.Number.CurrencySymbol = '$'
    GroupNameFields.Item.Format.Field.Number.ThousandSymbol = ','
    GroupNameFields.Item.Format.Field.Number.DecimalSymbol = '.'
    GroupNameFields.Item.Format.Field.Number.ShowZeroValueAs = '0'
    GroupNameFields.Item.Format.Field.Date.FirstSeparator = '/'
    GroupNameFields.Item.Format.Field.Date.SecondSeparator = '/'
    GroupNameFields.Item.Format.Field.Time.AMString = 'AM'
    GroupNameFields.Item.Format.Field.Time.PMString = 'PM'
    GroupNameFields.Item.Format.Field.Time.HourMinSeparator = ':'
    GroupNameFields.Item.Format.Field.Time.MinSecSeparator = ':'
    GroupNameFields.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    GroupNameFields.Item.Font.Charset = DEFAULT_CHARSET
    GroupNameFields.Item.Font.Color = clWindowText
    GroupNameFields.Item.Font.Height = -11
    GroupNameFields.Item.Font.Name = 'MS Sans Serif'
    GroupNameFields.Item.Font.Style = []
    GroupNameFields.Item.HiliteConditions.Item.FontColor = clNone
    GroupNameFields.Item.HiliteConditions.Item.Background = clNone
    RunningTotals.Number = -1
    RunningTotals.Item.Top = -1
    RunningTotals.Item.Left = -1
    RunningTotals.Item.Width = -1
    RunningTotals.Item.Height = -1
    RunningTotals.Item.Border.Left = lsNone
    RunningTotals.Item.Border.Right = lsNone
    RunningTotals.Item.Border.Top = lsNone
    RunningTotals.Item.Border.Bottom = lsNone
    RunningTotals.Item.Border.TightHorizontal = False
    RunningTotals.Item.Border.DropShadow = False
    RunningTotals.Item.Border.ForeColor = clNone
    RunningTotals.Item.Border.BackgroundColor = clNone
    RunningTotals.Item.Format.Alignment = haDefault
    RunningTotals.Item.Format.SuppressIfDuplicated = False
    RunningTotals.Item.Format.CanGrow = False
    RunningTotals.Item.Format.MaxNLines = 0
    RunningTotals.Item.Format.Field.Number.CurrencySymbol = '$'
    RunningTotals.Item.Format.Field.Number.ThousandSymbol = ','
    RunningTotals.Item.Format.Field.Number.DecimalSymbol = '.'
    RunningTotals.Item.Format.Field.Number.ShowZeroValueAs = '0'
    RunningTotals.Item.Format.Field.Date.FirstSeparator = '/'
    RunningTotals.Item.Format.Field.Date.SecondSeparator = '/'
    RunningTotals.Item.Format.Field.Time.AMString = 'AM'
    RunningTotals.Item.Format.Field.Time.PMString = 'PM'
    RunningTotals.Item.Format.Field.Time.HourMinSeparator = ':'
    RunningTotals.Item.Format.Field.Time.MinSecSeparator = ':'
    RunningTotals.Item.Format.Field.Paragraph.LineSpacing = 1.000000000000000000
    RunningTotals.Item.Font.Charset = DEFAULT_CHARSET
    RunningTotals.Item.Font.Color = clWindowText
    RunningTotals.Item.Font.Height = -11
    RunningTotals.Item.Font.Name = 'MS Sans Serif'
    RunningTotals.Item.Font.Style = []
    RunningTotals.Item.HiliteConditions.Item.FontColor = clNone
    RunningTotals.Item.HiliteConditions.Item.Background = clNone
    WindowZoom.Preview = pwDefault
    WindowZoom.Magnification = -1
    WindowCursor.GroupArea = wcDefault
    WindowCursor.GroupAreaField = wcDefault
    WindowCursor.DetailArea = wcDefault
    WindowCursor.DetailAreaField = wcDefault
    WindowCursor.Graph = wcDefault
    WindowCursor.OnDemandSubreport = wcMagnify
    WindowCursor.HyperLink = wcDefault
    Graphs.Number = -1
    Graphs.Item.Top = -1
    Graphs.Item.Left = -1
    Graphs.Item.Width = -1
    Graphs.Item.Height = -1
    Graphs.Item.Border.Left = lsNone
    Graphs.Item.Border.Right = lsNone
    Graphs.Item.Border.Top = lsNone
    Graphs.Item.Border.Bottom = lsNone
    Graphs.Item.Border.TightHorizontal = False
    Graphs.Item.Border.DropShadow = False
    Graphs.Item.Border.ForeColor = clNone
    Graphs.Item.Border.BackgroundColor = clNone
    Graphs.Item.Style = unknownGraphType
    Graphs.Item.Text.TitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.TitleFont.Color = clWindowText
    Graphs.Item.Text.TitleFont.Height = -11
    Graphs.Item.Text.TitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.TitleFont.Style = []
    Graphs.Item.Text.SubTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.SubTitleFont.Color = clWindowText
    Graphs.Item.Text.SubTitleFont.Height = -11
    Graphs.Item.Text.SubTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.SubTitleFont.Style = []
    Graphs.Item.Text.FootNoteFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.FootNoteFont.Color = clWindowText
    Graphs.Item.Text.FootNoteFont.Height = -11
    Graphs.Item.Text.FootNoteFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.FootNoteFont.Style = []
    Graphs.Item.Text.GroupsTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupsTitleFont.Color = clWindowText
    Graphs.Item.Text.GroupsTitleFont.Height = -11
    Graphs.Item.Text.GroupsTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupsTitleFont.Style = []
    Graphs.Item.Text.DataTitleFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataTitleFont.Color = clWindowText
    Graphs.Item.Text.DataTitleFont.Height = -11
    Graphs.Item.Text.DataTitleFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataTitleFont.Style = []
    Graphs.Item.Text.LegendFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.LegendFont.Color = clWindowText
    Graphs.Item.Text.LegendFont.Height = -11
    Graphs.Item.Text.LegendFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.LegendFont.Style = []
    Graphs.Item.Text.GroupLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.GroupLabelsFont.Color = clWindowText
    Graphs.Item.Text.GroupLabelsFont.Height = -11
    Graphs.Item.Text.GroupLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.GroupLabelsFont.Style = []
    Graphs.Item.Text.DataLabelsFont.Charset = DEFAULT_CHARSET
    Graphs.Item.Text.DataLabelsFont.Color = clWindowText
    Graphs.Item.Text.DataLabelsFont.Height = -11
    Graphs.Item.Text.DataLabelsFont.Name = 'MS Sans Serif'
    Graphs.Item.Text.DataLabelsFont.Style = []
    Graphs.Item.Axis.GridLineX = gglNone
    Graphs.Item.Axis.GridLineY = gglMajor
    Graphs.Item.Axis.GridLineY2 = gglNone
    Graphs.Item.Axis.GridLineZ = gglNone
    Graphs.Item.Axis.DataValuesY = gdvAutomatic
    Graphs.Item.Axis.DataValuesY2 = gdvAutomatic
    Graphs.Item.Axis.DataValuesZ = gdvAutomatic
    Graphs.Item.Axis.MinY = -1.000000000000000000
    Graphs.Item.Axis.MaxY = -1.000000000000000000
    Graphs.Item.Axis.MinY2 = -1.000000000000000000
    Graphs.Item.Axis.MaxY2 = -1.000000000000000000
    Graphs.Item.Axis.MinZ = -1.000000000000000000
    Graphs.Item.Axis.MaxZ = -1.000000000000000000
    Graphs.Item.Axis.NumberFormatY = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatY2 = gnfNoDecimal
    Graphs.Item.Axis.NumberFormatZ = gnfNoDecimal
    Graphs.Item.Axis.DivisionTypeY = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeY2 = gdvAutomatic
    Graphs.Item.Axis.DivisionTypeZ = gdvAutomatic
    Graphs.Item.Axis.DivisionsY = -1
    Graphs.Item.Axis.DivisionsY2 = -1
    Graphs.Item.Axis.DivisionsZ = -1
    SummaryInfo.SavePreviewPicture = False
    Left = 360
    Top = 16
  end
  object ekorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select kode,panjang,berat from ekor')
    Left = 689
    Top = 7
    object ekorQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object ekorQpanjang: TStringField
      FieldName = 'panjang'
      Size = 50
    end
    object ekorQberat: TStringField
      FieldName = 'berat'
      Size = 50
    end
  end
  object jarakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 721
    Top = 7
    object jarakQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object jarakQDari: TMemoField
      FieldName = 'Dari'
      BlobType = ftMemo
    end
    object jarakQKe: TMemoField
      FieldName = 'Ke'
      BlobType = ftMemo
    end
    object jarakQJarak: TIntegerField
      FieldName = 'Jarak'
    end
  end
  object updateQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jarak')
    Left = 185
    Top = 15
    object updateQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
    object updateQDari: TMemoField
      FieldName = 'Dari'
      Required = True
      BlobType = ftMemo
    end
    object updateQKe: TMemoField
      FieldName = 'Ke'
      Required = True
      BlobType = ftMemo
    end
    object updateQJumlahKm: TIntegerField
      FieldName = 'JumlahKm'
      Required = True
    end
  end
  object barangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang;')
    Left = 145
    Top = 135
    object barangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object barangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object barangQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Visible = False
      Size = 10
    end
    object barangQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
      Visible = False
    end
    object barangQMaximumStok: TIntegerField
      FieldName = 'MaximumStok'
      Visible = False
    end
    object barangQStandardUmur: TIntegerField
      FieldName = 'StandardUmur'
      Visible = False
    end
    object barangQLokasi: TStringField
      FieldName = 'Lokasi'
      Required = True
      Visible = False
      Size = 50
    end
    object barangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object barangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object barangQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object barangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object barangQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
  end
  object lpbQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from laporanperbaikan')
    Left = 593
    Top = 191
    object lpbQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object lpbQPP: TStringField
      FieldName = 'PP'
      Required = True
      Size = 10
    end
    object lpbQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      Required = True
      BlobType = ftMemo
    end
    object lpbQTindakanPerbaikan: TMemoField
      FieldName = 'TindakanPerbaikan'
      Required = True
      BlobType = ftMemo
    end
    object lpbQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
      Visible = False
    end
    object lpbQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
      Visible = False
    end
    object lpbQVerifikator: TStringField
      FieldName = 'Verifikator'
      Visible = False
      Size = 50
    end
    object lpbQTglSerahTerima: TDateTimeField
      FieldName = 'TglSerahTerima'
      Visible = False
    end
    object lpbQPICSerahTerima: TStringField
      FieldName = 'PICSerahTerima'
      Visible = False
      Size = 50
    end
    object lpbQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
    object lpbQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
    object lpbQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object lpbQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object lpbQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object lpbQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object lpbQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object lprQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from laporanperawatan')
    Left = 689
    Top = 327
    object lprQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object lprQMekanik: TStringField
      FieldName = 'Mekanik'
      Required = True
      Size = 10
    end
    object lprQJenisPerawatan: TStringField
      FieldName = 'JenisPerawatan'
      Required = True
      Size = 10
    end
    object lprQWaktuMulai: TDateTimeField
      FieldName = 'WaktuMulai'
    end
    object lprQWaktuSelesai: TDateTimeField
      FieldName = 'WaktuSelesai'
    end
    object lprQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Visible = False
      Size = 50
    end
    object lprQKeterangan: TMemoField
      FieldName = 'Keterangan'
      Visible = False
      BlobType = ftMemo
    end
    object lprQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object lprQCreateBy: TStringField
      FieldName = 'CreateBy'
      Required = True
      Visible = False
      Size = 10
    end
    object lprQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object lprQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object lprQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
    object lprQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object lprQEkor: TStringField
      FieldName = 'Ekor'
      Visible = False
      Size = 10
    end
  end
  object BarangKanibalQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'Select BarangKanibal.*, barang.nama as DetailBarangKanibal from ' +
        'BarangKanibal, barang where Barangkanibal.barang = barang.kode')
    Left = 152
    Top = 56
    object BarangKanibalQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangKanibalQIDBarang: TStringField
      FieldName = 'IDBarang'
      Size = 50
    end
    object BarangKanibalQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object BarangKanibalQTglMasuk: TDateTimeField
      FieldName = 'TglMasuk'
    end
    object BarangKanibalQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object BarangKanibalQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object BarangKanibalQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object BarangKanibalQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object BarangKanibalQDetailBarangKanibal: TStringField
      FieldName = 'DetailBarangKanibal'
      Required = True
      Size = 50
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 576
    Top = 120
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
  end
  object AmbilBarangKanibalQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select cast(k.tglTukar as date) as Tgl,k.*, a.NoBody as NoBodyAr' +
        'mada, b1.IDBarang as ID_BarangKanibal, b2.nama as NamaBarangAsal' +
        ' '
      
        'from AmbilBarangKanibal k left outer join LaporanPerbaikan lp on' +
        ' lp.Kode=k.LaporanPerbaikan, Armada a, BarangKanibal b1, Barang ' +
        'b2'
      
        'where k.KeArmada=a.Kode and k.DariBarang=b1.Kode and k.barang = ' +
        'b2.kode and'
      
        '(k.TglTukar>=:text1 and k.TglTukar<=:text2) or k.TglTukar is nul' +
        'l'
      'order by tglentry desc')
    Left = 288
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object AmbilBarangKanibalQTgl: TStringField
      FieldName = 'Tgl'
    end
    object AmbilBarangKanibalQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object AmbilBarangKanibalQDariBarang: TStringField
      FieldName = 'DariBarang'
      Size = 10
    end
    object AmbilBarangKanibalQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object AmbilBarangKanibalQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object AmbilBarangKanibalQTglTukar: TDateTimeField
      FieldName = 'TglTukar'
    end
    object AmbilBarangKanibalQPeminta: TStringField
      FieldName = 'Peminta'
      Size = 10
    end
    object AmbilBarangKanibalQSPK: TStringField
      FieldName = 'SPK'
      Size = 10
    end
    object AmbilBarangKanibalQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object AmbilBarangKanibalQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object AmbilBarangKanibalQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object AmbilBarangKanibalQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object AmbilBarangKanibalQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object AmbilBarangKanibalQNoBodyArmada: TStringField
      FieldName = 'NoBodyArmada'
      Size = 50
    end
    object AmbilBarangKanibalQID_BarangKanibal: TStringField
      FieldName = 'ID_BarangKanibal'
      Size = 50
    end
    object AmbilBarangKanibalQNamaBarangAsal: TStringField
      FieldName = 'NamaBarangAsal'
      Required = True
      Size = 50
    end
  end
  object VSPKQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'LP '#39' + spk.lap' +
        'oranperbaikan AS KodeReferensi, pp.armada AS Armada, lpb.status ' +
        'AS StatusReferensi, spk.mekanik AS Mekanik, spk.status AS Status' +
        ', spk.detailtindakan AS DetailTindakan, spk.keterangan AS Ketera' +
        'ngan '
      
        'from suratperintahkerja spk, laporanperbaikan lpb, permintaanper' +
        'baikan pp'
      
        'where spk.laporanperbaikan<>NULL AND spk.laporanperbaikan=lpb.ko' +
        'de AND lpb.status='#39'ON PROCESS'#39' AND lpb.pp=pp.kode'
      'UNION'
      
        'select spk.kode AS Kode, spk.tanggal AS Tanggal, '#39'RB '#39' + spk.reb' +
        'uild AS KodeReferensi, rb.dariarmada AS Armada, rb.status AS Sta' +
        'tusReferensi, spk.mekanik AS Mekanik, spk.status AS Status, spk.' +
        'detailtindakan AS DetailTindakan, spk.keterangan AS Keterangan '
      'from suratperintahkerja spk, rebuild rb'
      
        'where spk.rebuild<>NULL AND spk.rebuild=rb.kode AND rb.status='#39'O' +
        'N PROCESS'#39)
    Left = 473
    Top = 15
    object VSPKQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object VSPKQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object VSPKQKodeReferensi: TStringField
      FieldName = 'KodeReferensi'
      Size = 13
    end
    object VSPKQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object VSPKQPlatNoArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNoArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object VSPKQNoBodyArmada: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBodyArmada'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object VSPKQStatusReferensi: TStringField
      FieldName = 'StatusReferensi'
      Size = 50
    end
    object VSPKQMekanik: TStringField
      FieldName = 'Mekanik'
      Visible = False
      Size = 10
    end
    object VSPKQNamaMekanik: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMekanik'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Mekanik'
      Size = 50
      Lookup = True
    end
    object VSPKQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object VSPKQDetailTindakan: TMemoField
      FieldName = 'DetailTindakan'
      BlobType = ftMemo
    end
    object VSPKQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
  end
  object PPQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select pp.* from permintaanperbaikan pp left outer join laporanp' +
        'erbaikan lp on pp.kode=lp.pp')
    Left = 529
    Top = 7
    object PPQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PPQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object PPQPlatNo: TStringField
      FieldKind = fkLookup
      FieldName = 'PlatNo'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Lookup = True
    end
    object PPQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Lookup = True
    end
    object PPQNamaPeminta: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPeminta'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Peminta'
      Size = 50
      Lookup = True
    end
    object PPQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PPQArmada: TStringField
      FieldName = 'Armada'
      Visible = False
      Size = 10
    end
    object PPQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Visible = False
      Size = 10
    end
    object PPQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object PPQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object PPQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object PPQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object PPQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
      Visible = False
    end
  end
  object RebuildQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from rebuild')
    Left = 425
    Top = 7
    object RebuildQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RebuildQBarang: TStringField
      FieldName = 'Barang'
      Size = 10
    end
    object RebuildQDariArmada: TStringField
      FieldName = 'DariArmada'
      Size = 10
    end
    object RebuildQTanggalMasuk: TDateTimeField
      FieldName = 'TanggalMasuk'
    end
    object RebuildQKeArmada: TStringField
      FieldName = 'KeArmada'
      Size = 10
    end
    object RebuildQTanggalKeluar: TDateTimeField
      FieldName = 'TanggalKeluar'
    end
    object RebuildQAnalisaMasalah: TMemoField
      FieldName = 'AnalisaMasalah'
      BlobType = ftMemo
    end
    object RebuildQJasaLuar: TBooleanField
      FieldName = 'JasaLuar'
    end
    object RebuildQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object RebuildQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object RebuildQTanggalKirim: TDateTimeField
      FieldName = 'TanggalKirim'
    end
    object RebuildQPICKirim: TStringField
      FieldName = 'PICKirim'
      Size = 10
    end
    object RebuildQTanggalKembali: TDateTimeField
      FieldName = 'TanggalKembali'
    end
    object RebuildQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object RebuildQPerbaikanInternal: TBooleanField
      FieldName = 'PerbaikanInternal'
    end
    object RebuildQVerifikator: TStringField
      FieldName = 'Verifikator'
      Size = 10
    end
    object RebuildQTglVerifikasi: TDateTimeField
      FieldName = 'TglVerifikasi'
    end
    object RebuildQKanibal: TBooleanField
      FieldName = 'Kanibal'
    end
    object RebuildQPersentaseCosting: TFloatField
      FieldName = 'PersentaseCosting'
    end
    object RebuildQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object RebuildQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RebuildQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object RebuildQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RebuildQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
  end
end
