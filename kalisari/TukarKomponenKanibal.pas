unit TukarKomponenKanibal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxLabel, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxRadioGroup, dxSkinscxPCPainter,
  UCrpeClasses, UCrpe32, cxMemo, cxCheckBox, cxGridBandedTableView,
  cxGridDBBandedTableView, cxDropDownEdit, cxGroupBox;

type
  TTukarKomponenKanibalFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SOQ: TSDQuery;
    ArmadaQ: TSDQuery;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SJDS: TDataSource;
    sopirQ: TSDQuery;
    sopirQKode: TStringField;
    sopirQNama: TStringField;
    sopirQAlamat: TStringField;
    sopirQNotelp: TStringField;
    sopirQCreateDate: TDateTimeField;
    sopirQCreateBy: TStringField;
    sopirQOperator: TStringField;
    sopirQTglEntry: TDateTimeField;
    Crpe1: TCrpe;
    ekorQ: TSDQuery;
    ekorQkode: TStringField;
    ekorQpanjang: TStringField;
    ekorQberat: TStringField;
    jarakQ: TSDQuery;
    jarakQkode: TStringField;
    jarakQDari: TMemoField;
    jarakQKe: TMemoField;
    jarakQJarak: TIntegerField;
    updateQ: TSDQuery;
    buttonCetak: TcxButton;
    SOQNOSO: TStringField;
    SOQNAMA_PELANGGAN: TStringField;
    SOQALAMAT_PELANGGAN: TStringField;
    SOQTLP_PELANGGAN: TStringField;
    SOQNAMA_SOPIR: TStringField;
    SOQEKOR_ARMADA: TStringField;
    SOQTGL_ORDER: TDateTimeField;
    SOQKODE_ARMADA: TStringField;
    SOQPLAT_ARMADA: TStringField;
    SOQKODESOPIR: TStringField;
    SOQKODE_RUTE: TStringField;
    SOQMUAT: TStringField;
    SOQBONGKAR: TStringField;
    SOQDARIPT: TMemoField;
    SOQDARIALAMAT: TMemoField;
    SOQKEPT: TMemoField;
    SOQKEALAMAT: TMemoField;
    SOQCHECKER: TStringField;
    barangQ: TSDQuery;
    barangQKode: TStringField;
    barangQNama: TStringField;
    barangQSatuan: TStringField;
    barangQMinimumStok: TIntegerField;
    barangQMaximumStok: TIntegerField;
    barangQStandardUmur: TIntegerField;
    barangQLokasi: TStringField;
    barangQCreateDate: TDateTimeField;
    barangQCreateBy: TStringField;
    barangQOperator: TStringField;
    barangQTglEntry: TDateTimeField;
    lpbQ: TSDQuery;
    lpbQKode: TStringField;
    lpbQPP: TStringField;
    lpbQAnalisaMasalah: TMemoField;
    lpbQTindakanPerbaikan: TMemoField;
    lpbQWaktuMulai: TDateTimeField;
    lpbQWaktuSelesai: TDateTimeField;
    lpbQVerifikator: TStringField;
    lpbQTglSerahTerima: TDateTimeField;
    lpbQPICSerahTerima: TStringField;
    lpbQStatus: TStringField;
    lpbQKeterangan: TMemoField;
    lpbQCreateDate: TDateTimeField;
    lpbQCreateBy: TStringField;
    lpbQOperator: TStringField;
    lpbQTglEntry: TDateTimeField;
    lpbQTglCetak: TDateTimeField;
    lprQ: TSDQuery;
    lprQKode: TStringField;
    lprQMekanik: TStringField;
    lprQJenisPerawatan: TStringField;
    lprQWaktuMulai: TDateTimeField;
    lprQWaktuSelesai: TDateTimeField;
    lprQStatus: TStringField;
    lprQKeterangan: TMemoField;
    lprQCreateDate: TDateTimeField;
    lprQCreateBy: TStringField;
    lprQOperator: TStringField;
    lprQTglEntry: TDateTimeField;
    lprQTglCetak: TDateTimeField;
    lprQArmada: TStringField;
    lprQEkor: TStringField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    BarangKanibalQ: TSDQuery;
    BarangKanibalQKode: TStringField;
    BarangKanibalQIDBarang: TStringField;
    BarangKanibalQBarang: TStringField;
    BarangKanibalQTglMasuk: TDateTimeField;
    BarangKanibalQCreateDate: TDateTimeField;
    BarangKanibalQTglEntry: TDateTimeField;
    BarangKanibalQOperator: TStringField;
    BarangKanibalQCreateBy: TStringField;
    BarangKanibalQDetailBarangKanibal: TStringField;
    MasterQTglTukar: TDateTimeField;
    MasterQPeminta: TStringField;
    MasterQLaporanPerbaikan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQCreateBy: TStringField;
    MasterQID_BarangKanibal: TStringField;
    MasterQNamaBarangAsal: TStringField;
    MasterQTindakanPerbaikan: TMemoField;
    MasterQDetailBarangKanibal: TStringField;
    MasterVGridDariBarang: TcxDBEditorRow;
    MasterVGridID_BarangKanibal: TcxDBEditorRow;
    MasterVGridDetailBarangKanibal: TcxDBEditorRow;
    MasterVGridBarang: TcxDBEditorRow;
    MasterVGridNamaBarangAsal: TcxDBEditorRow;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQnama: TStringField;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterQNamaPeminta: TStringField;
    cxDBVerticalGrid1KeArmada: TcxDBEditorRow;
    cxDBVerticalGrid1NoBodyArmada: TcxDBEditorRow;
    cxDBVerticalGrid1TglTukar: TcxDBEditorRow;
    cxDBVerticalGrid1Peminta: TcxDBEditorRow;
    cxDBVerticalGrid1NamaPeminta: TcxDBEditorRow;
    AmbilBarangKanibalQ: TSDQuery;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBTableViewKode: TcxGridDBColumn;
    cxGrid1DBTableViewTglTukar: TcxGridDBColumn;
    cxGrid1DBTableViewBarangKanibal: TcxGridDBColumn;
    cxGrid1DBTableViewBarang: TcxGridDBColumn;
    KodeQkode: TStringField;
    updateQkode: TStringField;
    updateQDari: TMemoField;
    updateQKe: TMemoField;
    updateQJumlahKm: TIntegerField;
    DELETEbtn: TcxButton;
    MasterQSPK: TStringField;
    VSPKQ: TSDQuery;
    MasterQDetailTindakan: TStringField;
    MasterVGridDBEditorRow1: TcxDBEditorRow;
    MasterVGridDBEditorRow2: TcxDBEditorRow;
    MasterQPlatNoArmada: TStringField;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    PPQ: TSDQuery;
    PPQKode: TStringField;
    PPQTanggal: TDateTimeField;
    PPQPlatNo: TStringField;
    PPQNoBody: TStringField;
    PPQNamaPeminta: TStringField;
    PPQKeluhan: TMemoField;
    PPQArmada: TStringField;
    PPQPeminta: TStringField;
    PPQCreateDate: TDateTimeField;
    PPQCreateBy: TStringField;
    PPQOperator: TStringField;
    PPQTglEntry: TDateTimeField;
    PPQTglCetak: TDateTimeField;
    MasterQPP: TStringField;
    MasterQArmada: TStringField;
    RebuildQ: TSDQuery;
    RebuildQKode: TStringField;
    RebuildQBarang: TStringField;
    RebuildQDariArmada: TStringField;
    RebuildQTanggalMasuk: TDateTimeField;
    RebuildQKeArmada: TStringField;
    RebuildQTanggalKeluar: TDateTimeField;
    RebuildQAnalisaMasalah: TMemoField;
    RebuildQJasaLuar: TBooleanField;
    RebuildQSupplier: TStringField;
    RebuildQHarga: TCurrencyField;
    RebuildQTanggalKirim: TDateTimeField;
    RebuildQPICKirim: TStringField;
    RebuildQTanggalKembali: TDateTimeField;
    RebuildQPenerima: TStringField;
    RebuildQPerbaikanInternal: TBooleanField;
    RebuildQVerifikator: TStringField;
    RebuildQTglVerifikasi: TDateTimeField;
    RebuildQKanibal: TBooleanField;
    RebuildQPersentaseCosting: TFloatField;
    RebuildQStatus: TStringField;
    RebuildQCreateDate: TDateTimeField;
    RebuildQTglEntry: TDateTimeField;
    RebuildQOperator: TStringField;
    RebuildQCreateBy: TStringField;
    VSPKQKode: TStringField;
    VSPKQTanggal: TDateTimeField;
    VSPKQKodeReferensi: TStringField;
    VSPKQArmada: TStringField;
    VSPKQStatusReferensi: TStringField;
    VSPKQMekanik: TStringField;
    VSPKQStatus: TStringField;
    VSPKQDetailTindakan: TMemoField;
    VSPKQKeterangan: TStringField;
    VSPKQPlatNoArmada: TStringField;
    VSPKQNoBodyArmada: TStringField;
    VSPKQNamaMekanik: TStringField;
    cxGroupBox1: TcxGroupBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel7: TcxLabel;
    AmbilBarangKanibalQTgl: TStringField;
    AmbilBarangKanibalQKode: TStringField;
    AmbilBarangKanibalQDariBarang: TStringField;
    AmbilBarangKanibalQKeArmada: TStringField;
    AmbilBarangKanibalQBarang: TStringField;
    AmbilBarangKanibalQTglTukar: TDateTimeField;
    AmbilBarangKanibalQPeminta: TStringField;
    AmbilBarangKanibalQSPK: TStringField;
    AmbilBarangKanibalQLaporanPerbaikan: TStringField;
    AmbilBarangKanibalQCreateDate: TDateTimeField;
    AmbilBarangKanibalQTglEntry: TDateTimeField;
    AmbilBarangKanibalQOperator: TStringField;
    AmbilBarangKanibalQCreateBy: TStringField;
    AmbilBarangKanibalQNoBodyArmada: TStringField;
    AmbilBarangKanibalQID_BarangKanibal: TStringField;
    AmbilBarangKanibalQNamaBarangAsal: TStringField;
    MasterQKode: TStringField;
    MasterQDariBarang: TStringField;
    MasterQKeArmada: TStringField;
    MasterQBarang: TStringField;
    MasterQNoBodyArmada: TStringField;
    barangQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridNoSOEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridNoSOEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterVGridsopirEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1AwalEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure buttonCetakClick(Sender: TObject);
    procedure MasterVGridDariArmadaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridKeArmadaEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridBarangDariEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridBarangKeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridLaporanPerawatanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDariBarangEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBVerticalGrid1PemintaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridDBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  TukarKomponenKanibalFm: TTukarKomponenKanibalFm;
  MasterOriSQL: string;
  kodeprint :String;
implementation

uses MenuUtama, DropDown, DM, Math, BarangKanibalDropDown, PegawaiDropDown,
  BarangDropDown, SPKDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TTukarKomponenKanibalFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TTukarKomponenKanibalFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TTukarKomponenKanibalFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TTukarKomponenKanibalFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TTukarKomponenKanibalFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  BarangKanibalQ.Open;
  barangQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  lpbQ.Open;
  PPQ.Open;
  RebuildQ.Open;
end;

procedure TTukarKomponenKanibalFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
  //MasterVGrid.FocusRow(MasterVGridNoSO);
  MasterVGrid.Enabled:=true;
  cxDBVerticalGrid1.Enabled:=true;
  //cxDBVerticalGrid1Enabled:=true;
end;

procedure TTukarKomponenKanibalFm.KodeEditEnter(Sender: TObject);
begin
  buttonCetak.Enabled:=false;
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=False;
  //cxDBVerticalGrid1.Enabled:=False;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TTukarKomponenKanibalFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertAmbilBarangKanibal.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateAmbilBarangKanibal.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteAmbilBarangKanibal.AsBoolean;
  cxButtonEdit1PropertiesButtonClick(Sender,0);
  cxDateEdit2.Date:=Now;
  cxDateEdit1.Date:=Now-1;
  AmbilBarangKanibalQ.Close;
  AmbilBarangKanibalQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  AmbilBarangKanibalQ.ParamByName('text2').AsDate:=cxDateEdit2.date+1;
  AmbilBarangKanibalQ.Open;
end;

procedure TTukarKomponenKanibalFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=menuutamafm.UserQDeleteAmbilBarangKanibal.AsBoolean;
    end;
    SaveBtn.Enabled:=menuutamafm.UserQUpdateAmbilBarangKanibal.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TTukarKomponenKanibalFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TTukarKomponenKanibalFm.SaveBtnClick(Sender: TObject);
var sKode :String;
begin
  kodeprint :=  MasterQKode.AsString;
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    kodeprint :=  MasterQKode.AsString;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Tukar Komponen dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    AmbilBarangKanibalQ.Close;
    AmbilBarangKanibalQ.Open;
    KodeEdit.SetFocus;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  cxButtonEdit1PropertiesButtonClick(sender,0);
end;

procedure TTukarKomponenKanibalFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TTukarKomponenKanibalFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
  //MasterQStatus.AsString:='ON GOING';
  //if(MasterQAwal.Value='') then MasterQAwal.AsVariant:= null;
  //if(MasterQEkor.Value='') then MasterQEkor.AsVariant:= null;
  //if(MasterQAkhir.Value='') then MasterQAkhir.AsVariant:= null;
end;

procedure TTukarKomponenKanibalFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Tukar Komponen Kanibal '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Tukar Komponen Kanibal telah dihapus.',mtInformation,[mbOK],0);
    AmbilBarangKanibalQ.Close;
    AmbilBarangKanibalQ.Open;
    KodeEdit.SetFocus;
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      MessageDlg('Tukar Komponen Kanibal pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
     cxButtonEdit1PropertiesButtonClick(sender,0);
  end;
end;

procedure TTukarKomponenKanibalFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.MasterVGridNoSOEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    SOQ.Close;
    SOQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.MasterVGridNoSOEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SOQ.Close;
    SOQ.ExecSQL;
    SOQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SOQ);

    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQNoSO.AsString:=SOQnoso.AsString;
      //MasterQtgl_order.AsDateTime :=  SOQtgl_order.AsDateTime;
      //MasterQnama_pelanggan.AsString := SOQnama_pelanggan.AsString;
      //MasterQtlp_pelanggan.AsString :=    SOQtlp_pelanggan.AsString;
      //MasterQalamat_pelanggan.AsString :=    SOQalamat_pelanggan.asString;
      //MasterQkode_armada.AsString :=  SOQkode_armada.AsString;
      //MasterQekor.AsString :=  SOQekor_armada.AsString;
      //MasterQsopir.AsString :=  SOQkodesopir.AsString;
      //MasterQplat_armada.AsString := SOQplat_armada.AsString;
      //MasterQnama_sopir.AsString := SOQnama_sopir.AsString;
      //MasterQmuat.AsString := SOQmuat.AsString;
      //MasterQbongkar.AsString := SOQbongkar.AsString;
      //MasterQdaript.AsString := SOQdaript.AsString;
      //MasterQdarialamat.AsString := SOQdarialamat.AsString;
      //MasterQkept.AsString := SOQkept.AsString;
      //MasterQkealamat.AsString := SOQkealamat.AsString;
      //MasterQkode_rute.AsString := SOQkode_rute.AsString;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    buttonCetak.Enabled:=true;
    kodeedit.Text:=AmbilBarangKanibalQKode.AsString;
    KodeEditExit(self);
    SaveBtn.Enabled:=menuutamafm.UserQUpdateAmbilBarangKanibal.AsBoolean;
    MasterVGrid.Enabled:=True;
    //DBVerticalGrid1.Enabled:=True;
end;

procedure TTukarKomponenKanibalFm.MasterVGridsopirEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    sopirQ.Close;
    sopirQ.ExecSQL;
    sopirQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,sopirQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQSopir.AsString := sopirQKode.AsString;
      //MasterQnama_sopir.AsString := sopirQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.cxDBVerticalGrid1AwalEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {   jarakQ.Close;
    jarakQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,jarakQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQAwal.AsString := jarakQkode.AsString;
      //MasterQjarak_dari.AsString := jarakQDari.AsString;
      //MasterQjarak_ke.AsString := jarakQKe.AsString;
      //MasterQjarak_jarak.AsString := jarakQJarak.AsString;
    end;
    DropDownFm.Release;      }
end;

procedure TTukarKomponenKanibalFm.buttonCetakClick(Sender: TObject);
begin
    //cetak SJ
    if MessageDlg('Cetak Surat Jalan '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      Crpe1.Refresh;
      Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'suratjalan.rpt';
      Crpe1.ParamFields[0].CurrentValue:=MasterQKode.AsString;
      Crpe1.Connect.UserID:='sa';
      Crpe1.Connect.Password:='sa';
      Crpe1.Connect.ServerName:='';
      Crpe1.Execute;
      Crpe1.Print;
      Crpe1.CloseWindow;
      updateQ.Close;
      updateQ.SQL.Clear;
      updateQ.SQL.Add('update mastersj set tglcetak=CURRENT_TIMESTAMP where kode='+QuotedStr(MasterQKode.AsString));
      updateQ.ExecSQL;
      updateQ.Close;
      //viewSJQ.Close;
      //viewSJQ.Open;
    end;
    //end Cetak SJ
end;

procedure TTukarKomponenKanibalFm.MasterVGridDariArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  {  ArmadaQ.Close;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      if MasterQKeArmada.AsString<>''then
      begin
        if ArmadaQKode.AsString=MasterQKeArmada.AsString then
        begin
          ShowMessage('Armada Tidak Boleh Sama');
        end
        else
        begin
          MasterQDariArmada.AsString:= ArmadaQKode.AsString;
          MasterQplatno_dari.AsString:= ArmadaQPlatNo.AsString;
        end;
      end
      else
      begin
        MasterQDariArmada.AsString:= ArmadaQKode.AsString;
        MasterQplatno_dari.AsString:= ArmadaQPlatNo.AsString;
      end;
    end;
    DropDownFm.Release;}
end;

procedure TTukarKomponenKanibalFm.MasterVGridKeArmadaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    ArmadaQ.Close;
    ArmadaQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,ArmadaQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQKeArmada.AsString:=ArmadaQKode.AsString;
      MasterQNoBodyArmada.AsString:=ArmadaQNoBody.AsString;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.MasterVGridBarangDariEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {   barangQ.Close;
    barangQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,barangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBarangDari.AsString:= barangQKode.AsString;
      MasterQnama_dari.AsString:= barangQNama.AsString;
      MasterQBarangKe.AsString:= barangQKode.AsString;
      MasterQnama_ke.AsString:= barangQNama.AsString;
    end;
    DropDownFm.Release; }
end;

procedure TTukarKomponenKanibalFm.MasterVGridBarangKeEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
   { barangQ.Close;
    barangQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,barangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQBarangke.AsString:= barangQKode.AsString;
      MasterQnama_ke.AsString:= barangQNama.AsString;
    end;
    DropDownFm.Release; }
end;

procedure TTukarKomponenKanibalFm.MasterVGridLaporanPerbaikanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 {   lpbQ.Close;
    lpbQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,lpbQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQLaporanPerbaikan.AsString:= lpbQKode.AsString;
      MasterQTindakanPerbaikan.AsString:= lpbQTindakanPerbaikan.AsString;
    end;
    DropDownFm.Release;       }
end;

procedure TTukarKomponenKanibalFm.MasterVGridLaporanPerawatanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{    lprQ.Close;
    lprQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,lprQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      //MasterQLaporanPerawatan.AsString:= lprQKode.AsString;
      //MasterQwaktumulai_lpr.AsString := lprQWaktuMulai.AsString;
      //MasterQwaktuselesai_lpr.AsString := lprQWaktuSelesai.AsString;
    end;
    DropDownFm.Release;     }
end;

procedure TTukarKomponenKanibalFm.MasterVGridDariBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangKanibalDropDownFm:=TBarangKanibalDropDownFm.Create(self);
  if BarangKanibalDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQDariBarang.AsString:= BarangKanibalDropDownFm.kode;
  end;
  BarangKanibalDropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQBarang.AsString:= BarangDropDownFm.kode;
  end;
  BarangDropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.cxDBVerticalGrid1PemintaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.Create(self);
  if PegawaiDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQPeminta.AsString:= PegawaiDropDownFm.kode;
  end;
  PegawaiDropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    VSPKQ.Close;
    VSPKQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,VSPKQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQSPK.AsString:= VSPKQKode.AsString;
      //MasterQLaporanPerbaikan.AsString:= VSPKQLaporanPerbaikan.AsString;
    end;
    DropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.MasterVGridDBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SPKDropDownFm:=TSPKDropDownFm.Create(self);
  if SPKDropDownFm.ShowModal=MrOK then
  begin
    MasterQ.Open;
    MasterQ.Edit;
    MasterQSPK.AsString:= SPKDropDownFm.kode;
    MasterQKeArmada.AsString:= SPKDropDownFm.armada;
  end;
  SPKDropDownFm.Release;
end;

procedure TTukarKomponenKanibalFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  AmbilBarangKanibalQ.Close;
  AmbilBarangKanibalQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  AmbilBarangKanibalQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  AmbilBarangKanibalQ.Open;
end;

procedure TTukarKomponenKanibalFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  AmbilBarangKanibalQ.Close;
  AmbilBarangKanibalQ.ParamByName('text1').AsDate:=cxDateEdit1.Date;
  AmbilBarangKanibalQ.ParamByName('text2').AsDate:=cxDateEdit2.date;
  AmbilBarangKanibalQ.Open;
end;

end.
