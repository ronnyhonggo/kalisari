object UpdateKilometerFm: TUpdateKilometerFm
  Left = 330
  Top = 29
  Width = 899
  Height = 680
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Update Kilometer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl2: TPanel
    Left = 0
    Top = 48
    Width = 883
    Height = 594
    Align = alClient
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 360
      Height = 509
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = ViewDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1PlatNo: TcxGridDBColumn
          DataBinding.FieldName = 'PlatNo'
          Width = 354
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object StatusBar: TStatusBar
      Left = 1
      Top = 571
      Width = 881
      Height = 22
      Panels = <
        item
          Width = 50
        end>
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 361
      Top = 1
      Width = 521
      Height = 509
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 223
      ParentFont = False
      TabOrder = 2
      DataController.DataSource = DataSource1
      Version = 1
      object cxDBVerticalGrid1PlatNo: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxTextEditProperties'
        Properties.EditProperties.ReadOnly = True
        Properties.DataBinding.FieldName = 'PlatNo'
        Properties.Options.Editing = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1KmSekarang: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'KmSekarang'
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 510
      Width = 881
      Height = 61
      Align = alBottom
      TabOrder = 3
      object SaveBtn: TcxButton
        Left = 8
        Top = 10
        Width = 75
        Height = 25
        Caption = 'SAVE'
        TabOrder = 0
        OnClick = SaveBtnClick
      end
      object ExitBtn: TcxButton
        Left = 92
        Top = 10
        Width = 75
        Height = 25
        Caption = 'EXIT'
        TabOrder = 1
        OnClick = ExitBtnClick
      end
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 883
    Height = 48
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 21
      Width = 43
      Height = 16
      Caption = 'Nama :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cxTextEdit1: TcxTextEdit
      Left = 56
      Top = 16
      TabOrder = 0
      OnKeyUp = cxTextEdit1KeyUp
      Width = 121
    end
  end
  object ViewPelangganQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 265
    Top = 1
    object ViewPelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewPelangganQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ViewPelangganQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ViewPelangganQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ViewPelangganQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ViewPelangganQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ViewPelangganQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ViewPelangganQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ViewPelangganQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ViewPelangganQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ViewPelangganQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ViewPelangganQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ViewPelangganQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ViewPelangganQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ViewPelangganQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ViewPelangganQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ViewPelangganQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ViewPelangganQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ViewPelangganQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ViewPelangganQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ViewPelangganQAC: TBooleanField
      FieldName = 'AC'
    end
    object ViewPelangganQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ViewPelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewPelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewPelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewPelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewPelangganQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ViewPelangganQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewPelangganQ
    Left = 300
    Top = 6
  end
  object cekpanjangQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select Isnull(max(len(NamaPT)),1)as panjang1,Isnull(max(len(Alam' +
        'at)),1)as panjang2,Isnull(max(len(NoTelp)),1)as panjang3,Isnull(' +
        'max(len(NamaPIC1)),1)as panjang4,Isnull(max(len(TelpPIC1)),1)as ' +
        'panjang5,Isnull(max(len(JabatanPIC1)),1)as panjang6 from Pelangg' +
        'an')
    Left = 233
    Top = 1
    object cekpanjangQpanjang1: TIntegerField
      FieldName = 'panjang1'
    end
    object cekpanjangQpanjang2: TIntegerField
      FieldName = 'panjang2'
    end
    object cekpanjangQpanjang3: TIntegerField
      FieldName = 'panjang3'
    end
    object cekpanjangQpanjang4: TIntegerField
      FieldName = 'panjang4'
    end
    object cekpanjangQpanjang5: TIntegerField
      FieldName = 'panjang5'
    end
    object cekpanjangQpanjang6: TIntegerField
      FieldName = 'panjang6'
    end
  end
  object DataSource1: TDataSource
    DataSet = masterQ
    Left = 544
    Top = 8
  end
  object masterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    UpdateObject = SDUpdateSQL1
    Left = 504
    Top = 8
    object masterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object masterQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object masterQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object masterQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object masterQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object masterQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object masterQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object masterQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object masterQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object masterQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object masterQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object masterQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object masterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object masterQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object masterQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object masterQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object masterQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object masterQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object masterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object masterQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object masterQAC: TBooleanField
      FieldName = 'AC'
    end
    object masterQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object masterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object masterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object masterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object masterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object masterQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object masterQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, ' +
        'JumlahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspen' +
        'sion, Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, L' +
        'evelArmada, JumlahBan, Keterangan, Aktif, AC, KmSekarang, Create' +
        'Date, CreateBy, Operator, TglEntry, STNKPerpanjangExpired, KirMu' +
        'lai'
      'from Armada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Armada'
      'set'
      '  Kode = :Kode,'
      '  PlatNo = :PlatNo,'
      '  NoBody = :NoBody,'
      '  NoRangka = :NoRangka,'
      '  NoMesin = :NoMesin,'
      '  JenisKendaraan = :JenisKendaraan,'
      '  JumlahSeat = :JumlahSeat,'
      '  JenisBBM = :JenisBBM,'
      '  TahunPembuatan = :TahunPembuatan,'
      '  JenisAC = :JenisAC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Sopir = :Sopir,'
      '  KapasitasTangkiBBM = :KapasitasTangkiBBM,'
      '  STNKPajakExpired = :STNKPajakExpired,'
      '  KirSelesai = :KirSelesai,'
      '  LevelArmada = :LevelArmada,'
      '  JumlahBan = :JumlahBan,'
      '  Keterangan = :Keterangan,'
      '  Aktif = :Aktif,'
      '  AC = :AC,'
      '  KmSekarang = :KmSekarang,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  STNKPerpanjangExpired = :STNKPerpanjangExpired,'
      '  KirMulai = :KirMulai'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Armada'
      
        '  (Kode, PlatNo, NoBody, NoRangka, NoMesin, JenisKendaraan, Juml' +
        'ahSeat, JenisBBM, TahunPembuatan, JenisAC, Toilet, AirSuspension' +
        ', Sopir, KapasitasTangkiBBM, STNKPajakExpired, KirSelesai, Level' +
        'Armada, JumlahBan, Keterangan, Aktif, AC, KmSekarang, CreateDate' +
        ', CreateBy, Operator, TglEntry, STNKPerpanjangExpired, KirMulai)'
      'values'
      
        '  (:Kode, :PlatNo, :NoBody, :NoRangka, :NoMesin, :JenisKendaraan' +
        ', :JumlahSeat, :JenisBBM, :TahunPembuatan, :JenisAC, :Toilet, :A' +
        'irSuspension, :Sopir, :KapasitasTangkiBBM, :STNKPajakExpired, :K' +
        'irSelesai, :LevelArmada, :JumlahBan, :Keterangan, :Aktif, :AC, :' +
        'KmSekarang, :CreateDate, :CreateBy, :Operator, :TglEntry, :STNKP' +
        'erpanjangExpired, :KirMulai)')
    DeleteSQL.Strings = (
      'delete from Armada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 584
    Top = 8
  end
end
