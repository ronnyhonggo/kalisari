unit UpdateKilometer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMemo;

type
  TUpdateKilometerFm = class(TForm)
    pnl2: TPanel;
    ViewPelangganQ: TSDQuery;
    ViewDs: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar: TStatusBar;
    pnl1: TPanel;
    lbl1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cekpanjangQ: TSDQuery;
    cekpanjangQpanjang1: TIntegerField;
    cekpanjangQpanjang2: TIntegerField;
    cekpanjangQpanjang3: TIntegerField;
    cekpanjangQpanjang4: TIntegerField;
    cekpanjangQpanjang5: TIntegerField;
    cekpanjangQpanjang6: TIntegerField;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    DataSource1: TDataSource;
    masterQ: TSDQuery;
    ViewPelangganQKode: TStringField;
    ViewPelangganQPlatNo: TStringField;
    ViewPelangganQNoBody: TStringField;
    ViewPelangganQNoRangka: TStringField;
    ViewPelangganQNoMesin: TStringField;
    ViewPelangganQJenisKendaraan: TStringField;
    ViewPelangganQJumlahSeat: TIntegerField;
    ViewPelangganQJenisBBM: TStringField;
    ViewPelangganQTahunPembuatan: TStringField;
    ViewPelangganQJenisAC: TStringField;
    ViewPelangganQToilet: TBooleanField;
    ViewPelangganQAirSuspension: TBooleanField;
    ViewPelangganQSopir: TStringField;
    ViewPelangganQKapasitasTangkiBBM: TIntegerField;
    ViewPelangganQSTNKPajakExpired: TDateTimeField;
    ViewPelangganQKirSelesai: TDateTimeField;
    ViewPelangganQLevelArmada: TStringField;
    ViewPelangganQJumlahBan: TIntegerField;
    ViewPelangganQKeterangan: TStringField;
    ViewPelangganQAktif: TBooleanField;
    ViewPelangganQAC: TBooleanField;
    ViewPelangganQKmSekarang: TIntegerField;
    ViewPelangganQCreateDate: TDateTimeField;
    ViewPelangganQCreateBy: TStringField;
    ViewPelangganQOperator: TStringField;
    ViewPelangganQTglEntry: TDateTimeField;
    ViewPelangganQSTNKPerpanjangExpired: TDateTimeField;
    ViewPelangganQKirMulai: TDateTimeField;
    cxGrid1DBTableView1PlatNo: TcxGridDBColumn;
    masterQKode: TStringField;
    masterQPlatNo: TStringField;
    masterQNoBody: TStringField;
    masterQNoRangka: TStringField;
    masterQNoMesin: TStringField;
    masterQJenisKendaraan: TStringField;
    masterQJumlahSeat: TIntegerField;
    masterQJenisBBM: TStringField;
    masterQTahunPembuatan: TStringField;
    masterQJenisAC: TStringField;
    masterQToilet: TBooleanField;
    masterQAirSuspension: TBooleanField;
    masterQSopir: TStringField;
    masterQKapasitasTangkiBBM: TIntegerField;
    masterQSTNKPajakExpired: TDateTimeField;
    masterQKirSelesai: TDateTimeField;
    masterQLevelArmada: TStringField;
    masterQJumlahBan: TIntegerField;
    masterQKeterangan: TStringField;
    masterQAktif: TBooleanField;
    masterQAC: TBooleanField;
    masterQKmSekarang: TIntegerField;
    masterQCreateDate: TDateTimeField;
    masterQCreateBy: TStringField;
    masterQOperator: TStringField;
    masterQTglEntry: TDateTimeField;
    masterQSTNKPerpanjangExpired: TDateTimeField;
    masterQKirMulai: TDateTimeField;
    cxDBVerticalGrid1PlatNo: TcxDBEditorRow;
    cxDBVerticalGrid1KmSekarang: TcxDBEditorRow;
    Panel1: TPanel;
    SaveBtn: TcxButton;
    ExitBtn: TcxButton;
    SDUpdateSQL1: TSDUpdateSQL;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxTextEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SaveBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  UpdateKilometerFm: TUpdateKilometerFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TUpdateKilometerFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TUpdateKilometerFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TUpdateKilometerFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TUpdateKilometerFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TUpdateKilometerFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=ViewPelangganQ.SQL.Text;
  ViewPelangganQ.Open;
  cekpanjangQ.Close;
  cekpanjangQ.Open;
  cekpanjangQ.First;
  //cxGrid1DBTableView1NamaToko.Width:= cekpanjangQpanjang1.AsInteger*8;
  {cxGrid1DBTableView1Alamat.Width:= cekpanjangQpanjang2.AsInteger*8;
  cxGrid1DBTableView1NoTelp.Width:= cekpanjangQpanjang3.AsInteger*8;
  cxGrid1DBTableView1NamaPIC1.Width := cekpanjangQpanjang4.AsInteger*8;
  cxGrid1DBTableView1TelpPIC1.Width := cekpanjangQpanjang5.AsInteger*8;
  cxGrid1DBTableView1JabatanPIC1.Width := cekpanjangQpanjang6.AsInteger*8; }
end;


procedure TUpdateKilometerFm.cxTextEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ViewPelangganQ.Close;
    ViewPelangganQ.SQL.Clear;
    ViewPelangganQ.SQL.Add('select * from Armada where PlatNo like '+ QuotedStr('%'+cxTextEdit1.Text+'%'));
    ViewPelangganQ.ExecSQL;
    ViewPelangganQ.Open;
end;

procedure TUpdateKilometerFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    SaveBtn.Enabled:=menuutamafm.UserQUpdateUpdateKm.AsBoolean;
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+ViewPelangganQkode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      {if inttostr(masterQKmSekarang.AsInteger) = '' then
      begin
          MasterQ.edit;
          masterQKmSekarang.AsInteger:=0;
      end; }
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    cxDBVerticalGrid1.Enabled:=TRUE;
    //MasterVGrid.Enabled:=True;
    //cxDBVerticalGrid1.Enabled:=True;
end;



procedure TUpdateKilometerFm.SaveBtnClick(Sender: TObject);
begin
  MenuUtamaFm.Database1.StartTransaction;
  MasterQ.ApplyUpdates;
  MenuUtamaFm.Database1.Commit;
  MasterQ.CommitUpdates;
  ShowMessage('Kilometer telah disimpan');
end;

procedure TUpdateKilometerFm.FormShow(Sender: TObject);
begin
  SaveBtn.Enabled:=menuutamafm.UserQUpdateUpdateKm.AsBoolean;
end;

end.
