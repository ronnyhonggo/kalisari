unit UserDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TUserDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    UserQ: TSDQuery;
    UserQKode: TStringField;
    UserQUsername: TStringField;
    UserQPassword: TStringField;
    UserQJenis: TStringField;
    UserQKodePegawai: TStringField;
    UserQMasterBKanibal: TBooleanField;
    UserQInsertMasterBKanibal: TBooleanField;
    UserQUpdateMasterBKanibal: TBooleanField;
    UserQDeleteMasterBKanibal: TBooleanField;
    UserQMasterKBarang: TBooleanField;
    UserQInsertMasterKBarang: TBooleanField;
    UserQUpdateMasterKBarang: TBooleanField;
    UserQDeleteMasterKBarang: TBooleanField;
    UserQMasterAC: TBooleanField;
    UserQInsertMasterAC: TBooleanField;
    UserQUpdateMasterAC: TBooleanField;
    UserQDeleteMasterAC: TBooleanField;
    UserQMasterArmada: TBooleanField;
    UserQInsertMasterArmada: TBooleanField;
    UserQUpdateMasterArmada: TBooleanField;
    UserQDeleteMasterArmada: TBooleanField;
    UserQMasterJenisKendaraan: TBooleanField;
    UserQInsertJenisKendaraan: TBooleanField;
    UserQUpdateJenisKendaraan: TBooleanField;
    UserQDeleteJenisKendaraan: TBooleanField;
    UserQMasterEkor: TBooleanField;
    UserQInsertMasterEkor: TBooleanField;
    UserQUpdateMasterEkor: TBooleanField;
    UserQDeleteMasterEkor: TBooleanField;
    UserQMasterPelanggan: TBooleanField;
    UserQInsertMasterPelanggan: TBooleanField;
    UserQUpdateMasterPelanggan: TBooleanField;
    UserQDeleteMasterPelanggan: TBooleanField;
    UserQMasterRute: TBooleanField;
    UserQInsertMasterRute: TBooleanField;
    UserQUpdateMasterRute: TBooleanField;
    UserQDeleteMasterRute: TBooleanField;
    UserQMasterSopir: TBooleanField;
    UserQInsertMasterSopir: TBooleanField;
    UserQUpdateMasterSopir: TBooleanField;
    UserQDeleteMasterSopir: TBooleanField;
    UserQMasterJarak: TBooleanField;
    UserQInsertMasterJarak: TBooleanField;
    UserQUpdateMasterJarak: TBooleanField;
    UserQDeleteMasterJarak: TBooleanField;
    UserQMasterBan: TBooleanField;
    UserQInsertMasterBan: TBooleanField;
    UserQUpdateMasterBan: TBooleanField;
    UserQDeleteMasterBan: TBooleanField;
    UserQMasterStandardPerawatan: TBooleanField;
    UserQInsertMasterStandardPerawatan: TBooleanField;
    UserQUpdateMasterStandardPerawatan: TBooleanField;
    UserQDeleteMasterStandardPerawatan: TBooleanField;
    UserQMasterBarang: TBooleanField;
    UserQInsertMasterBarang: TBooleanField;
    UserQUpdateMasterBarang: TBooleanField;
    UserQDeleteMasterBarang: TBooleanField;
    UserQMasterJenisPerbaikan: TBooleanField;
    UserQInsertMasterJenisPerbaikan: TBooleanField;
    UserQUpdateMasterJenisPerbaikan: TBooleanField;
    UserQDeleteMasterJenisPerbaikan: TBooleanField;
    UserQMasterSupplier: TBooleanField;
    UserQInsertMasterSupplier: TBooleanField;
    UserQUpdateMasterSupplier: TBooleanField;
    UserQDeleteMasterSupplier: TBooleanField;
    UserQMasterUser: TBooleanField;
    UserQInsertMasterUser: TBooleanField;
    UserQUpdateMasterUser: TBooleanField;
    UserQDeleteMasterUser: TBooleanField;
    UserQMasterMekanik: TBooleanField;
    UserQInsertMasterMekanik: TBooleanField;
    UserQUpdateMasterMekanik: TBooleanField;
    UserQDeleteMasterMekanik: TBooleanField;
    UserQMasterPegawai: TBooleanField;
    UserQInsertMasterPegawai: TBooleanField;
    UserQUpdateMasterPegawai: TBooleanField;
    UserQDeleteMasterPegawai: TBooleanField;
    UserQKontrak: TBooleanField;
    UserQInsertKontrak: TBooleanField;
    UserQUpdateKontrak: TBooleanField;
    UserQDeleteKontrak: TBooleanField;
    UserQSO: TBooleanField;
    UserQInsertSO: TBooleanField;
    UserQUpdateSO: TBooleanField;
    UserQDeleteSO: TBooleanField;
    UserQSetHargaSO: TBooleanField;
    UserQSetStatusSO: TBooleanField;
    UserQViewHargaSO: TBooleanField;
    UserQKeluhanPelanggan: TBooleanField;
    UserQInsertKeluhanPelanggan: TBooleanField;
    UserQUpdateKeluhanPelanggan: TBooleanField;
    UserQDeleteKeluhanPelanggan: TBooleanField;
    UserQDaftarPelanggan: TBooleanField;
    UserQJadwalArmada: TBooleanField;
    UserQPilihArmada: TBooleanField;
    UserQUpdatePilihArmada: TBooleanField;
    UserQSuratJalan: TBooleanField;
    UserQInsertSuratJalan: TBooleanField;
    UserQUpdateSuratJalan: TBooleanField;
    UserQDeleteSuratJalan: TBooleanField;
    UserQRealisasiSuratJalan: TBooleanField;
    UserQInsertRealisasiSuratJalan: TBooleanField;
    UserQUpdateRealisasiSuratJalan: TBooleanField;
    UserQDeleteRealisasiSuratJalan: TBooleanField;
    UserQStoring: TBooleanField;
    UserQInsertStoring: TBooleanField;
    UserQUpdateStoring: TBooleanField;
    UserQDeleteStoring: TBooleanField;
    UserQDaftarBeli: TBooleanField;
    UserQUpdateDaftarBeli: TBooleanField;
    UserQPilihSupplierDaftarBeli: TBooleanField;
    UserQPO: TBooleanField;
    UserQInsertPO: TBooleanField;
    UserQUpdatePO: TBooleanField;
    UserQDeletePO: TBooleanField;
    UserQApprovalPO: TBooleanField;
    UserQCashNCarry: TBooleanField;
    UserQInsertCashNCarry: TBooleanField;
    UserQUpdateCashNCarry: TBooleanField;
    UserQDeleteCashNCarry: TBooleanField;
    UserQKomplainNRetur: TBooleanField;
    UserQInsertKomplainNRetur: TBooleanField;
    UserQUpdateKomplainRetur: TBooleanField;
    UserQDeleteKomplainRetur: TBooleanField;
    UserQDaftarSupplier: TBooleanField;
    UserQBonBarang: TBooleanField;
    UserQInsertBonBarang: TBooleanField;
    UserQUpdateBonBarang: TBooleanField;
    UserQDeleteBonBarang: TBooleanField;
    UserQSetBeliBonBarang: TBooleanField;
    UserQSetMintaBonBarang: TBooleanField;
    UserQTambahStokBonBarang: TBooleanField;
    UserQPerbaikanBonBarang: TBooleanField;
    UserQPerawatanBonBarang: TBooleanField;
    UserQApprovalBonBarang: TBooleanField;
    UserQProcessBonBarang: TBooleanField;
    UserQApprovalMoneyBonBarang: TCurrencyField;
    UserQSetUrgentBonBarang: TBooleanField;
    UserQBonKeluarBarang: TBooleanField;
    UserQInsertBonKeluarBarang: TBooleanField;
    UserQUpdateBonKeluarBarang: TBooleanField;
    UserQDeleteBonKeluarBarang: TBooleanField;
    UserQLPB: TBooleanField;
    UserQInsertLPB: TBooleanField;
    UserQUpdateLPB: TBooleanField;
    UserQDeleteLPB: TBooleanField;
    UserQPemutihan: TBooleanField;
    UserQInsertPemutihan: TBooleanField;
    UserQUpdatePemutihan: TBooleanField;
    UserQDeletePemutihan: TBooleanField;
    UserQApprovalPemutihan: TBooleanField;
    UserQDaftarMinStok: TBooleanField;
    UserQDaftarStokBarang: TBooleanField;
    UserQLaporanRiwayatStokBarang: TBooleanField;
    UserQLaporanRiwayatHargaBarang: TBooleanField;
    UserQNotifikasiPerawatan: TBooleanField;
    UserQInsertNotifikasiPerawatan: TBooleanField;
    UserQPermintaanPerbaikan: TBooleanField;
    UserQInsertPermintaanPerbaikan: TBooleanField;
    UserQUpdatePermintaanPerbaikan: TBooleanField;
    UserQDeletePermintaanPerbaikan: TBooleanField;
    UserQPerbaikan: TBooleanField;
    UserQInsertPerbaikan: TBooleanField;
    UserQUpdatePerbaikan: TBooleanField;
    UserQDeletePerbaikan: TBooleanField;
    UserQPerawatan: TBooleanField;
    UserQInsertPerawatan: TBooleanField;
    UserQUpdatePerawatan: TBooleanField;
    UserQDeletePerawatan: TBooleanField;
    UserQLepasPasangBan: TBooleanField;
    UserQInsertLepasPasangBan: TBooleanField;
    UserQUpdateLepasPasangBan: TBooleanField;
    UserQDeleteLepasPasangBan: TBooleanField;
    UserQPerbaikanBan: TBooleanField;
    UserQInsertPerbaikanBan: TBooleanField;
    UserQUpdatePerbaikanBan: TBooleanField;
    UserQDeletePerbaikanBan: TBooleanField;
    UserQTukarKomponen: TBooleanField;
    UserQInsertTukarKomponen: TBooleanField;
    UserQUpdateTukarKomponen: TBooleanField;
    UserQDeleteTukarKomponen: TBooleanField;
    UserQKasBon: TBooleanField;
    UserQInsertKasBon: TBooleanField;
    UserQUpdateKasBon: TBooleanField;
    UserQDeleteKasBon: TBooleanField;
    UserQPenagihan: TBooleanField;
    UserQInsertPenagihan: TBooleanField;
    UserQUpdatePenagihan: TBooleanField;
    UserQDeletePenagihan: TBooleanField;
    UserQBayarPO: TBooleanField;
    UserQInsertBayarPO: TBooleanField;
    UserQUpdateBayarPO: TBooleanField;
    UserQDeleteBayarPO: TBooleanField;
    UserQLaporanSalesOrder: TBooleanField;
    UserQLaporanRealisasiOrder: TBooleanField;
    UserQLaporanOrderTidakSesuai: TBooleanField;
    UserQLaporanKeluhanPelanggan: TBooleanField;
    UserQLaporanPendapatan: TBooleanField;
    UserQLaporanPemenuhanKontrak: TBooleanField;
    UserQLaporanEfisiensiMuatan: TBooleanField;
    UserQLaporanPendapatanDanPoinSopir: TBooleanField;
    UserQLaporanPenggunaanBan: TBooleanField;
    UserQLaporanPenggunaanBarang: TBooleanField;
    UserQLaporanKegiatanMekanik: TBooleanField;
    UserQLaporanKegiatanTeknik: TBooleanField;
    UserQLaporanRiwayatKendaraan: TBooleanField;
    UserQLaporanPemenuhanBarang: TBooleanField;
    UserQLaporanPenerimaanNPembelian: TBooleanField;
    UserQLaporanPenambahanOli: TBooleanField;
    UserQLaporanRetur: TBooleanField;
    UserQLaporanPenjualanUser: TBooleanField;
    UserQLaporanProfitPerSO: TBooleanField;
    UserQSettingParameterNotifikasi: TBooleanField;
    UserQSettingWaktuNotifikasi: TBooleanField;
    UserQTampilkanNotifikasi: TBooleanField;
    UserQSettingNotifikasi: TBooleanField;
    UserQFollowUpCustomer: TBooleanField;
    UserQInactiveCustomer: TBooleanField;
    UserQPembuatanSuratJalan: TBooleanField;
    UserQNewBonBarang: TBooleanField;
    UserQNewDaftarBeli: TBooleanField;
    UserQNewPermintaanPerbaikan: TBooleanField;
    UserQNotifPilihArmada: TBooleanField;
    UserQNewLPB: TBooleanField;
    UserQMinimumStok: TBooleanField;
    UserQSTNKExpired: TBooleanField;
    UserQSTNKPajakExpired: TBooleanField;
    UserQKirExpired: TBooleanField;
    UserQSimExpired: TBooleanField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Username: TcxGridDBColumn;
    cxGrid1DBTableView1Jenis: TcxGridDBColumn;
    cxGrid1DBTableView1MasterBKanibal: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterBKanibal: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterBKanibal: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterBKanibal: TcxGridDBColumn;
    cxGrid1DBTableView1MasterKBarang: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterKBarang: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterKBarang: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterKBarang: TcxGridDBColumn;
    cxGrid1DBTableView1MasterAC: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterAC: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterAC: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterAC: TcxGridDBColumn;
    cxGrid1DBTableView1MasterArmada: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterArmada: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterArmada: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterArmada: TcxGridDBColumn;
    cxGrid1DBTableView1MasterJenisKendaraan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertJenisKendaraan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateJenisKendaraan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteJenisKendaraan: TcxGridDBColumn;
    cxGrid1DBTableView1MasterEkor: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterEkor: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterEkor: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterEkor: TcxGridDBColumn;
    cxGrid1DBTableView1MasterPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1MasterRute: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterRute: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterRute: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterRute: TcxGridDBColumn;
    cxGrid1DBTableView1MasterSopir: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterSopir: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterSopir: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterSopir: TcxGridDBColumn;
    cxGrid1DBTableView1MasterJarak: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterJarak: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterJarak: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterJarak: TcxGridDBColumn;
    cxGrid1DBTableView1MasterBan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterBan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterBan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterBan: TcxGridDBColumn;
    cxGrid1DBTableView1MasterStandardPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterStandardPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterStandardPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterStandardPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1MasterBarang: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterBarang: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterBarang: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterBarang: TcxGridDBColumn;
    cxGrid1DBTableView1MasterJenisPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterJenisPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterJenisPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterJenisPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1MasterSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1MasterUser: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterUser: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterUser: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterUser: TcxGridDBColumn;
    cxGrid1DBTableView1MasterMekanik: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterMekanik: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterMekanik: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterMekanik: TcxGridDBColumn;
    cxGrid1DBTableView1MasterPegawai: TcxGridDBColumn;
    cxGrid1DBTableView1InsertMasterPegawai: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateMasterPegawai: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteMasterPegawai: TcxGridDBColumn;
    cxGrid1DBTableView1Kontrak: TcxGridDBColumn;
    cxGrid1DBTableView1InsertKontrak: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateKontrak: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteKontrak: TcxGridDBColumn;
    cxGrid1DBTableView1SO: TcxGridDBColumn;
    cxGrid1DBTableView1InsertSO: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateSO: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteSO: TcxGridDBColumn;
    cxGrid1DBTableView1SetHargaSO: TcxGridDBColumn;
    cxGrid1DBTableView1SetStatusSO: TcxGridDBColumn;
    cxGrid1DBTableView1ViewHargaSO: TcxGridDBColumn;
    cxGrid1DBTableView1KeluhanPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertKeluhanPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateKeluhanPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteKeluhanPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1DaftarPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1JadwalArmada: TcxGridDBColumn;
    cxGrid1DBTableView1PilihArmada: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePilihArmada: TcxGridDBColumn;
    cxGrid1DBTableView1SuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1RealisasiSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertRealisasiSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateRealisasiSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteRealisasiSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1Storing: TcxGridDBColumn;
    cxGrid1DBTableView1InsertStoring: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateStoring: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteStoring: TcxGridDBColumn;
    cxGrid1DBTableView1DaftarBeli: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateDaftarBeli: TcxGridDBColumn;
    cxGrid1DBTableView1PilihSupplierDaftarBeli: TcxGridDBColumn;
    cxGrid1DBTableView1PO: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPO: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePO: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePO: TcxGridDBColumn;
    cxGrid1DBTableView1ApprovalPO: TcxGridDBColumn;
    cxGrid1DBTableView1CashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1InsertCashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateCashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteCashNCarry: TcxGridDBColumn;
    cxGrid1DBTableView1KomplainNRetur: TcxGridDBColumn;
    cxGrid1DBTableView1InsertKomplainNRetur: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateKomplainRetur: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteKomplainRetur: TcxGridDBColumn;
    cxGrid1DBTableView1DaftarSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1BonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1InsertBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1SetBeliBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1SetMintaBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1TambahStokBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1PerbaikanBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1PerawatanBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1ApprovalBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1ProcessBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1ApprovalMoneyBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1SetUrgentBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1BonKeluarBarang: TcxGridDBColumn;
    cxGrid1DBTableView1InsertBonKeluarBarang: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateBonKeluarBarang: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteBonKeluarBarang: TcxGridDBColumn;
    cxGrid1DBTableView1LPB: TcxGridDBColumn;
    cxGrid1DBTableView1InsertLPB: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateLPB: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteLPB: TcxGridDBColumn;
    cxGrid1DBTableView1Pemutihan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPemutihan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePemutihan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePemutihan: TcxGridDBColumn;
    cxGrid1DBTableView1ApprovalPemutihan: TcxGridDBColumn;
    cxGrid1DBTableView1DaftarMinStok: TcxGridDBColumn;
    cxGrid1DBTableView1DaftarStokBarang: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanRiwayatStokBarang: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanRiwayatHargaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1NotifikasiPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertNotifikasiPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1PermintaanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPermintaanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePermintaanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePermintaanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1Perbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1Perawatan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePerawatan: TcxGridDBColumn;
    cxGrid1DBTableView1LepasPasangBan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertLepasPasangBan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateLepasPasangBan: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteLepasPasangBan: TcxGridDBColumn;
    cxGrid1DBTableView1PerbaikanBan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPerbaikanBan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePerbaikanBan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePerbaikanBan: TcxGridDBColumn;
    cxGrid1DBTableView1TukarKomponen: TcxGridDBColumn;
    cxGrid1DBTableView1InsertTukarKomponen: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateTukarKomponen: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteTukarKomponen: TcxGridDBColumn;
    cxGrid1DBTableView1KasBon: TcxGridDBColumn;
    cxGrid1DBTableView1InsertKasBon: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateKasBon: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteKasBon: TcxGridDBColumn;
    cxGrid1DBTableView1Penagihan: TcxGridDBColumn;
    cxGrid1DBTableView1InsertPenagihan: TcxGridDBColumn;
    cxGrid1DBTableView1UpdatePenagihan: TcxGridDBColumn;
    cxGrid1DBTableView1DeletePenagihan: TcxGridDBColumn;
    cxGrid1DBTableView1BayarPO: TcxGridDBColumn;
    cxGrid1DBTableView1InsertBayarPO: TcxGridDBColumn;
    cxGrid1DBTableView1UpdateBayarPO: TcxGridDBColumn;
    cxGrid1DBTableView1DeleteBayarPO: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanSalesOrder: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanRealisasiOrder: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanOrderTidakSesuai: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanKeluhanPelanggan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPendapatan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPemenuhanKontrak: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanEfisiensiMuatan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPendapatanDanPoinSopir: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPenggunaanBan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPenggunaanBarang: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanKegiatanMekanik: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanKegiatanTeknik: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanRiwayatKendaraan: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPemenuhanBarang: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPenerimaanNPembelian: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPenambahanOli: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanRetur: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanPenjualanUser: TcxGridDBColumn;
    cxGrid1DBTableView1LaporanProfitPerSO: TcxGridDBColumn;
    cxGrid1DBTableView1SettingParameterNotifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1SettingWaktuNotifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1TampilkanNotifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1SettingNotifikasi: TcxGridDBColumn;
    cxGrid1DBTableView1FollowUpCustomer: TcxGridDBColumn;
    cxGrid1DBTableView1InactiveCustomer: TcxGridDBColumn;
    cxGrid1DBTableView1PembuatanSuratJalan: TcxGridDBColumn;
    cxGrid1DBTableView1NewBonBarang: TcxGridDBColumn;
    cxGrid1DBTableView1NewDaftarBeli: TcxGridDBColumn;
    cxGrid1DBTableView1NewPermintaanPerbaikan: TcxGridDBColumn;
    cxGrid1DBTableView1NotifPilihArmada: TcxGridDBColumn;
    cxGrid1DBTableView1NewLPB: TcxGridDBColumn;
    cxGrid1DBTableView1MinimumStok: TcxGridDBColumn;
    cxGrid1DBTableView1STNKExpired: TcxGridDBColumn;
    cxGrid1DBTableView1STNKPajakExpired: TcxGridDBColumn;
    cxGrid1DBTableView1KirExpired: TcxGridDBColumn;
    cxGrid1DBTableView1SimExpired: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  UserDropDownFm: TUserDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TUserDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TUserDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=UserQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TUserDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=UserQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TUserDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=UserQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TUserDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=UserQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

end.
