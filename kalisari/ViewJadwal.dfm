object ViewJadwalFm: TViewJadwalFm
  Left = 393
  Top = 35
  Width = 768
  Height = 641
  AutoSize = True
  Caption = 'ViewJadwalFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 584
    Width = 752
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 217
    Height = 584
    Align = alLeft
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 128
      Width = 24
      Height = 16
      Caption = 'Pagi'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 112
      Top = 128
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label3: TLabel
      Left = 8
      Top = 160
      Width = 32
      Height = 16
      Caption = 'Siang'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 192
      Width = 27
      Height = 16
      Caption = 'Sore'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 224
      Width = 38
      Height = 16
      Caption = 'Malam'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 112
      Top = 160
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label7: TLabel
      Left = 112
      Top = 192
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label8: TLabel
      Left = 112
      Top = 224
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label10: TLabel
      Left = 8
      Top = 8
      Width = 55
      Height = 16
      Caption = 'Tanggal :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 8
      Top = 56
      Width = 105
      Height = 16
      Caption = 'Tanggal (Range) :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel
      Left = 11
      Top = 344
      Width = 87
      Height = 13
      Caption = 'Total Perbaikan   :'
    end
    object Label13: TLabel
      Left = 100
      Top = 344
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label14: TLabel
      Left = 11
      Top = 368
      Width = 86
      Height = 13
      Caption = 'Total Perawatan :'
    end
    object Label15: TLabel
      Left = 100
      Top = 368
      Width = 4
      Height = 13
      Caption = '-'
    end
    object DateTimePicker1: TDateTimePicker
      Left = 8
      Top = 27
      Width = 186
      Height = 21
      Date = 41390.511596203700000000
      Time = 41390.511596203700000000
      TabOrder = 0
      OnChange = DateTimePicker1Change
    end
    object Button1: TButton
      Left = 8
      Top = 296
      Width = 97
      Height = 41
      Caption = 'Tampilkan Jadwal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = True
      OnClick = Button1Click
    end
    object SpinEdit2: TSpinEdit
      Left = 136
      Top = 472
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 0
      Visible = False
      OnChange = SpinEdit2Change
    end
    object SpinEdit3: TSpinEdit
      Left = 96
      Top = 392
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 0
      Visible = False
      OnChange = SpinEdit3Change
    end
    object SpinEdit4: TSpinEdit
      Left = 136
      Top = 392
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 0
      Visible = False
      OnChange = SpinEdit4Change
    end
    object SpinEdit5: TSpinEdit
      Left = 96
      Top = 416
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 0
      Visible = False
      OnChange = SpinEdit5Change
    end
    object SpinEdit6: TSpinEdit
      Left = 136
      Top = 416
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 6
      Value = 0
      Visible = False
      OnChange = SpinEdit6Change
    end
    object SpinEdit7: TSpinEdit
      Left = 96
      Top = 440
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 7
      Value = 0
      Visible = False
      OnChange = SpinEdit7Change
    end
    object SpinEdit8: TSpinEdit
      Left = 136
      Top = 440
      Width = 41
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 8
      Value = 0
      Visible = False
      OnChange = SpinEdit8Change
    end
    object Button2: TButton
      Left = 112
      Top = 296
      Width = 89
      Height = 41
      Caption = 'Reset Waktu'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      Visible = False
      WordWrap = True
      OnClick = Button2Click
    end
    object cxTimeEdit1: TcxTimeEdit
      Left = 49
      Top = 125
      EditValue = 0d
      Properties.TimeFormat = tfHour
      TabOrder = 10
      Width = 48
    end
    object cxTimeEdit2: TcxTimeEdit
      Left = 129
      Top = 125
      EditValue = 0.416666666666667d
      Properties.TimeFormat = tfHour
      TabOrder = 11
      Width = 48
    end
    object cxTimeEdit3: TcxTimeEdit
      Left = 49
      Top = 157
      EditValue = 0.416666666666667d
      Properties.TimeFormat = tfHour
      TabOrder = 12
      Width = 48
    end
    object cxTimeEdit4: TcxTimeEdit
      Left = 129
      Top = 157
      EditValue = 0.625d
      Properties.TimeFormat = tfHour
      TabOrder = 13
      Width = 48
    end
    object cxTimeEdit5: TcxTimeEdit
      Left = 49
      Top = 189
      EditValue = 0.625d
      Properties.TimeFormat = tfHour
      TabOrder = 14
      Width = 48
    end
    object cxTimeEdit6: TcxTimeEdit
      Left = 129
      Top = 189
      EditValue = 0.833333333333333d
      Properties.TimeFormat = tfHour
      TabOrder = 15
      Width = 48
    end
    object cxTimeEdit7: TcxTimeEdit
      Left = 49
      Top = 221
      EditValue = 0.833333333333333d
      Properties.TimeFormat = tfHour
      TabOrder = 16
      Width = 48
    end
    object cxTimeEdit8: TcxTimeEdit
      Left = 129
      Top = 221
      EditValue = 0.958333333333333d
      Properties.TimeFormat = tfHour
      TabOrder = 17
      Width = 48
    end
    object DateTimePicker2: TDateTimePicker
      Left = 8
      Top = 75
      Width = 186
      Height = 21
      Date = 41390.511596203700000000
      Time = 41390.511596203700000000
      TabOrder = 18
      Visible = False
      OnChange = DateTimePicker2Change
    end
  end
  object Panel2: TPanel
    Left = 217
    Top = 0
    Width = 535
    Height = 584
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 2
    object Label9: TLabel
      Left = 1
      Top = 1
      Width = 533
      Height = 24
      Align = alTop
      Caption = 'Jumlah Armada Ready :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object StringGrid1: TStringGrid
      Left = 1
      Top = 25
      Width = 533
      Height = 558
      Align = alClient
      DefaultColWidth = 100
      DefaultRowHeight = 30
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
      ParentFont = False
      TabOrder = 0
    end
  end
  object cxListBox1: TcxListBox
    Left = 512
    Top = 200
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 3
    Visible = False
  end
  object SpinEdit1: TSpinEdit
    Left = 88
    Top = 472
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
    Visible = False
    OnChange = SpinEdit1Change
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 126
    Top = 256
    TabOrder = 5
    Value = 2
    Width = 81
  end
  object cxLabel1: TcxLabel
    Left = 8
    Top = 256
    Caption = 'Batas Hari Kir Exp'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from Armada')
    Left = 40
    Top = 488
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object ArmadaQNoBody: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object ArmadaQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object ArmadaQJenisBBM: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object ArmadaQJenisAC: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object ArmadaQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object ArmadaQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQKapasitasTangkiBBM: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQLevelArmada: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object ArmadaQAC: TBooleanField
      FieldName = 'AC'
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from MasterSO')
    Left = 8
    Top = 488
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object JadwalQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      
        'select (select COUNT(*) from MasterSO left join Armada on Master' +
        'SO.Armada=Armada.Kode'
      'where '
      'Datepart(yy,JamJemput)=:year and'
      'Datepart(mm,JamJemput)=:month and'
      'Datepart(dd,JamJemput)=:date and'
      'Datepart(hh,JamJemput)>=0 and Datepart(hh,Tiba)<10'
      'and Datediff(day,JamJemput,Tiba)=0'
      'and KapasitasSeat=:seat) as Pagi,'
      
        '(select COUNT(*) from MasterSO left join Armada on MasterSO.Arma' +
        'da=Armada.Kode'
      'where '
      'Datepart(yy,JamJemput)=:year and'
      'Datepart(mm,JamJemput)=:month and'
      'Datepart(dd,JamJemput)=:date and'
      'Datepart(hh,JamJemput)>=10 and Datepart(hh,Tiba)<15'
      'and Datediff(day,JamJemput,Tiba)=0'
      'and KapasitasSeat=:seat) as Siang,'
      
        '(select COUNT(*) from MasterSO left join Armada on MasterSO.Arma' +
        'da=Armada.Kode'
      'where '
      'Datepart(yy,JamJemput)=:year and'
      'Datepart(mm,JamJemput)=:month and'
      'Datepart(dd,JamJemput)=:date and'
      'Datepart(hh,JamJemput)>=15 and Datepart(hh,Tiba)<18'
      'and Datediff(day,JamJemput,Tiba)=0'
      'and KapasitasSeat=:seat) as Sore,'
      
        '(select COUNT(*) from MasterSO left join Armada on MasterSO.Arma' +
        'da=Armada.Kode'
      'where '
      'Datepart(yy,JamJemput)=:year and'
      'Datepart(mm,JamJemput)=:month and'
      'Datepart(dd,JamJemput)=:date and'
      'Datepart(hh,JamJemput)>=18 and Datepart(hh,Tiba)<=23'
      'and Datepart(mm,Tiba)<=59 and Datepart(ss,Tiba)<=59'
      'and Datediff(day,JamJemput,Tiba)=0'
      'and KapasitasSeat=:seat) as Malam,'
      
        '(select COUNT(*) from MasterSO left join Armada on MasterSO.Arma' +
        'da=Armada.Kode'
      'where '
      'Datepart(yy,JamJemput)=:year and'
      'Datepart(mm,JamJemput)=:month and'
      'Datepart(dd,JamJemput)=:date and'
      'Datediff(day,JamJemput,Tiba)>=1'
      'and KapasitasSeat=:seat) as FullDay')
    UpdateObject = JadwalUS
    Left = 8
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'year'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'month'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'date'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'year'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'month'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'date'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'year'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'month'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'date'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'year'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'month'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'date'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'year'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'month'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'date'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalQPagi: TIntegerField
      FieldName = 'Pagi'
    end
    object JadwalQSiang: TIntegerField
      FieldName = 'Siang'
    end
    object JadwalQSore: TIntegerField
      FieldName = 'Sore'
    end
    object JadwalQMalam: TIntegerField
      FieldName = 'Malam'
    end
    object JadwalQFullDay: TIntegerField
      FieldName = 'FullDay'
    end
  end
  object JadwalDS: TDataSource
    DataSet = JadwalQ
    Left = 24
    Top = 520
  end
  object JadwalUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pe' +
        'mbayaranAwal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiP' +
        'embayaranAwal, NominalKwitansiPembayaranAwal, PenerimaPembayaran' +
        'Awal, Pelunasan, TglPelunasan, CaraPembayaranPelunasan, NoKwitan' +
        'siPelunasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend' +
        ', TglKembaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, T' +
        'oilet, AirSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJem' +
        'put, JamJemput, NoTelpPICJemput, AlamatJemput, Status, StatusPem' +
        'bayaran, ReminderPending, PenerimaPending, Keterangan, CreateDat' +
        'e, CreateBy, Operator, TglEntry, TglCetak'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  Berangkat = :Berangkat,'
      '  Tiba = :Tiba,'
      '  Harga = :Harga,'
      '  PPN = :PPN,'
      '  PembayaranAwal = :PembayaranAwal,'
      '  TglPembayaranAwal = :TglPembayaranAwal,'
      '  CaraPembayaranAwal = :CaraPembayaranAwal,'
      '  NoKwitansiPembayaranAwal = :NoKwitansiPembayaranAwal,'
      
        '  NominalKwitansiPembayaranAwal = :NominalKwitansiPembayaranAwal' +
        ','
      '  PenerimaPembayaranAwal = :PenerimaPembayaranAwal,'
      '  Pelunasan = :Pelunasan,'
      '  TglPelunasan = :TglPelunasan,'
      '  CaraPembayaranPelunasan = :CaraPembayaranPelunasan,'
      '  NoKwitansiPelunasan = :NoKwitansiPelunasan,'
      '  NominalKwitansiPelunasan = :NominalKwitansiPelunasan,'
      '  PenerimaPelunasan = :PenerimaPelunasan,'
      '  Extend = :Extend,'
      '  TglKembaliExtend = :TglKembaliExtend,'
      '  BiayaExtend = :BiayaExtend,'
      '  PPNExtend = :PPNExtend,'
      '  KapasitasSeat = :KapasitasSeat,'
      '  AC = :AC,'
      '  Toilet = :Toilet,'
      '  AirSuspension = :AirSuspension,'
      '  Rute = :Rute,'
      '  TglFollowUp = :TglFollowUp,'
      '  Armada = :Armada,'
      '  Kontrak = :Kontrak,'
      '  PICJemput = :PICJemput,'
      '  JamJemput = :JamJemput,'
      '  NoTelpPICJemput = :NoTelpPICJemput,'
      '  AlamatJemput = :AlamatJemput,'
      '  Status = :Status,'
      '  StatusPembayaran = :StatusPembayaran,'
      '  ReminderPending = :ReminderPending,'
      '  PenerimaPending = :PenerimaPending,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  TglCetak = :TglCetak'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, Berangkat, Tiba, Harga, PPN, Pembay' +
        'aranAwal, TglPembayaranAwal, CaraPembayaranAwal, NoKwitansiPemba' +
        'yaranAwal, NominalKwitansiPembayaranAwal, PenerimaPembayaranAwal' +
        ', Pelunasan, TglPelunasan, CaraPembayaranPelunasan, NoKwitansiPe' +
        'lunasan, NominalKwitansiPelunasan, PenerimaPelunasan, Extend, Tg' +
        'lKembaliExtend, BiayaExtend, PPNExtend, KapasitasSeat, AC, Toile' +
        't, AirSuspension, Rute, TglFollowUp, Armada, Kontrak, PICJemput,' +
        ' JamJemput, NoTelpPICJemput, AlamatJemput, Status, StatusPembaya' +
        'ran, ReminderPending, PenerimaPending, Keterangan, CreateDate, C' +
        'reateBy, Operator, TglEntry, TglCetak)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :Berangkat, :Tiba, :Harga, :PPN,' +
        ' :PembayaranAwal, :TglPembayaranAwal, :CaraPembayaranAwal, :NoKw' +
        'itansiPembayaranAwal, :NominalKwitansiPembayaranAwal, :PenerimaP' +
        'embayaranAwal, :Pelunasan, :TglPelunasan, :CaraPembayaranPelunas' +
        'an, :NoKwitansiPelunasan, :NominalKwitansiPelunasan, :PenerimaPe' +
        'lunasan, :Extend, :TglKembaliExtend, :BiayaExtend, :PPNExtend, :' +
        'KapasitasSeat, :AC, :Toilet, :AirSuspension, :Rute, :TglFollowUp' +
        ', :Armada, :Kontrak, :PICJemput, :JamJemput, :NoTelpPICJemput, :' +
        'AlamatJemput, :Status, :StatusPembayaran, :ReminderPending, :Pen' +
        'erimaPending, :Keterangan, :CreateDate, :CreateBy, :Operator, :T' +
        'glEntry, :TglCetak)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 8
    Top = 456
  end
  object JumSeatQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select distinct JumlahSeat from Armada where JumlahSeat<>'#39#39)
    Left = 40
    Top = 456
    object JumSeatQJumlahSeat: TIntegerField
      FieldName = 'JumlahSeat'
    end
  end
  object JadwalSiangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select ('
      '(select count(*) asTotal from armada'
      
        'where  Lokasi='#39'Surabaya'#39' and jumlahseat=:seat and ((kirselesai -' +
        ' :selisih) < GETDATE() or (kirselesai is NULL)) )'
      '-'
      '(select COUNT(*) as Total from MasterSO so'
      'left join armada a on a.kode=so.armada'
      'where so.tiba>:JS1 and so.jamjemput<:JS2'
      
        'and so.kapasitasseat=:seat and so.status='#39'DEAL'#39' and a.Lokasi='#39'Su' +
        'rabaya'#39') '
      '-'
      
        '(select count(distinct (pp.armada)) as Total from laporanperbaik' +
        'an lp, armada a, permintaanperbaikan pp'
      
        'where  Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wakt' +
        'uselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.pp=pp.kod' +
        'e and pp.armada=a.kode'
      'and a.jumlahseat=:seat)'
      '-'
      
        '(select count(distinct (lp.armada)) as Total from laporanperawat' +
        'an lp, armada a'
      
        'where  a.Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wa' +
        'ktuselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.armada=' +
        'a.kode'
      'and a.jumlahseat=:seat)'
      ')as Total'
      ''
      ''
      '')
    Left = 8
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'selisih'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalSiangQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object JadwalPagiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select ('
      '(select count(*) asTotal from armada'
      
        'where Lokasi='#39'Surabaya'#39' and jumlahseat=:seat and ( ((kirselesai ' +
        '- :selisih) < GETDATE()) or (kirselesai is NULL) ))'
      '-'
      '(select COUNT(*) as Total from MasterSO so'
      'left join armada a on a.kode=so.armada'
      'where so.tiba>:JS1 and so.jamjemput<:JS2'
      
        'and so.kapasitasseat=:seat and so.status='#39'DEAL'#39' and a.Lokasi='#39'Su' +
        'rabaya'#39') '
      '-'
      
        '(select count(distinct (pp.armada)) as Total from laporanperbaik' +
        'an lp, armada a, permintaanperbaikan pp'
      
        'where  Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wakt' +
        'uselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.pp=pp.kod' +
        'e and pp.armada=a.kode'
      'and a.jumlahseat=:seat)'
      '-'
      
        '(select count(distinct (lp.armada)) as Total from laporanperawat' +
        'an lp, armada a'
      
        'where  a.Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wa' +
        'ktuselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.armada=' +
        'a.kode'
      'and a.jumlahseat=:seat)'
      ')as Total')
    Left = 40
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'selisih'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalPagiQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object JadwalSoreQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select ('
      '(select count(*) asTotal from armada'
      
        'where  Lokasi='#39'Surabaya'#39' and jumlahseat=:seat and ((kirselesai -' +
        ' :selisih) < GETDATE() or (kirselesai is NULL)))'
      '-'
      '(select COUNT(*) as Total from MasterSO so'
      'left join armada a on a.kode=so.armada'
      'where so.tiba>:JS1 and so.jamjemput<:JS2'
      
        'and so.kapasitasseat=:seat and so.status='#39'DEAL'#39' and a.Lokasi='#39'Su' +
        'rabaya'#39') '
      '-'
      
        '(select count(distinct (pp.armada)) as Total from laporanperbaik' +
        'an lp, armada a, permintaanperbaikan pp'
      
        'where  Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wakt' +
        'uselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.pp=pp.kod' +
        'e and pp.armada=a.kode'
      'and a.jumlahseat=:seat)'
      '-'
      
        '(select count(distinct (lp.armada)) as Total from laporanperawat' +
        'an lp, armada a'
      
        'where  a.Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wa' +
        'ktuselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.armada=' +
        'a.kode'
      'and a.jumlahseat=:seat)'
      ')as Total')
    Left = 40
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'selisih'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalSoreQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object JadwalMalamQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select ('
      '(select count(*) asTotal from armada'
      
        'where  Lokasi='#39'Surabaya'#39' and jumlahseat=:seat and ((kirselesai -' +
        ' :selisih) < GETDATE() or (kirselesai is null)) )'
      '-'
      '(select COUNT(*) as Total from MasterSO so'
      'left join armada a on a.kode=so.armada'
      'where so.tiba>:JS1 and so.jamjemput<:JS2'
      
        'and so.kapasitasseat=:seat and so.status='#39'DEAL'#39' and a.Lokasi='#39'Su' +
        'rabaya'#39') '
      '-'
      
        '(select count(distinct (pp.armada)) as Total from laporanperbaik' +
        'an lp, armada a, permintaanperbaikan pp'
      
        'where  Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wakt' +
        'uselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.pp=pp.kod' +
        'e and pp.armada=a.kode'
      'and a.jumlahseat=:seat)'
      '-'
      
        '(select count(distinct (lp.armada)) as Total from laporanperawat' +
        'an lp, armada a'
      
        'where  a.Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wa' +
        'ktuselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.armada=' +
        'a.kode'
      'and a.jumlahseat=:seat)'
      ')as Total'
      '')
    Left = 8
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'selisih'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalMalamQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object JadwalFullDayQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select ('
      '(select count(*) asTotal from armada'
      'where jumlahseat=:seat)'
      '-'
      
        '(select COUNT(*) as Total from MasterSO left join Armada on Mast' +
        'erSO.Armada=Armada.Kode'
      'where tiba>:JS1 and jamjemput<:JS2'
      'and Armada.JumlahSeat=:seat)'
      '-'
      '(select count(*) as Total from armada'
      'where (kirselesai - :selisih) > GETDATE() )'
      '+'
      
        '(select count(*) as Total from Armada where Lokasi='#39'Surabaya'#39' an' +
        'd jumlahseat=:seat)'
      ') as Total')
    Left = 40
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'JS2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'selisih'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'seat'
        ParamType = ptInput
      end>
    object JadwalFullDayQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object SDQuery1: TSDQuery
    Options = []
    Left = 160
    Top = 496
  end
  object PerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select count(distinct (pp.armada)) as Total from laporanperbaika' +
        'n lp, armada a, permintaanperbaikan pp'
      
        'where  Lokasi='#39'surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wakt' +
        'uselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.pp=pp.kod' +
        'e and pp.armada=a.kode'
      '')
    Left = 104
    Top = 496
    ParamData = <
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
        Value = ' '
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end>
    object PerbaikanQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
  object PerawatanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select count(distinct (lp.armada)) as Total from laporanperawata' +
        'n lp, armada a'
      
        'where  a.Lokasi='#39'Surabaya'#39' and (lp.waktumulai<= :JS1 and ((lp.wa' +
        'ktuselesai>=:JS2) or (lp.waktuselesai is NULL)) ) and lp.armada=' +
        'a.kode')
    Left = 112
    Top = 536
    ParamData = <
      item
        DataType = ftString
        Name = 'JS1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'JS2'
        ParamType = ptInput
      end>
    object PerawatanQTotal: TIntegerField
      FieldName = 'Total'
    end
  end
end
