unit ViewJadwal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, StdCtrls, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, SDEngine, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ExtCtrls, Grids, cxContainer, cxListBox, Spin,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxTimeEdit, cxDropDownEdit,
  cxCalendar, cxLabel;

type
  TViewJadwalFm = class(TForm)
    StatusBar: TStatusBar;
    Panel1: TPanel;
    DateTimePicker1: TDateTimePicker;
    ArmadaQ: TSDQuery;
    SOQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQNoBody: TStringField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQJumlahSeat: TIntegerField;
    ArmadaQJenisBBM: TStringField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJenisAC: TStringField;
    ArmadaQToilet: TBooleanField;
    ArmadaQAirSuspension: TBooleanField;
    ArmadaQSopir: TStringField;
    ArmadaQKapasitasTangkiBBM: TIntegerField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQLevelArmada: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQAktif: TBooleanField;
    ArmadaQAC: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    Panel2: TPanel;
    JadwalQ: TSDQuery;
    JadwalQPagi: TIntegerField;
    JadwalQSiang: TIntegerField;
    JadwalQSore: TIntegerField;
    JadwalQMalam: TIntegerField;
    JadwalQFullDay: TIntegerField;
    JadwalDS: TDataSource;
    JadwalUS: TSDUpdateSQL;
    Button1: TButton;
    JumSeatQ: TSDQuery;
    JumSeatQJumlahSeat: TIntegerField;
    StringGrid1: TStringGrid;
    cxListBox1: TcxListBox;
    SpinEdit1: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    SpinEdit2: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    Label6: TLabel;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    JadwalSiangQ: TSDQuery;
    JadwalSiangQTotal: TIntegerField;
    JadwalPagiQ: TSDQuery;
    JadwalSoreQ: TSDQuery;
    JadwalMalamQ: TSDQuery;
    JadwalSoreQTotal: TIntegerField;
    JadwalPagiQTotal: TIntegerField;
    JadwalMalamQTotal: TIntegerField;
    JadwalFullDayQ: TSDQuery;
    JadwalFullDayQTotal: TIntegerField;
    Button2: TButton;
    cxTimeEdit1: TcxTimeEdit;
    cxTimeEdit2: TcxTimeEdit;
    cxTimeEdit3: TcxTimeEdit;
    cxTimeEdit4: TcxTimeEdit;
    cxTimeEdit5: TcxTimeEdit;
    cxTimeEdit6: TcxTimeEdit;
    cxTimeEdit7: TcxTimeEdit;
    cxTimeEdit8: TcxTimeEdit;
    cxSpinEdit1: TcxSpinEdit;
    cxLabel1: TcxLabel;
    Label9: TLabel;
    DateTimePicker2: TDateTimePicker;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    SDQuery1: TSDQuery;
    PerbaikanQ: TSDQuery;
    PerbaikanQTotal: TIntegerField;
    Label14: TLabel;
    Label15: TLabel;
    PerawatanQ: TSDQuery;
    PerawatanQTotal: TIntegerField;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure SpinEdit4Change(Sender: TObject);
    procedure SpinEdit5Change(Sender: TObject);
    procedure SpinEdit6Change(Sender: TObject);
    procedure SpinEdit7Change(Sender: TObject);
    procedure SpinEdit8Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ViewJadwalFm: TViewJadwalFm;
  i,JumData:integer;

implementation

uses DateUtils;

{$R *.dfm}

procedure TViewJadwalFm.Button1Click(Sender: TObject);
var tahun,bulan,tgl:string;
begin
  tahun:=FormatDateTime('YYYY',DateTimePicker1.Date);
  bulan:=FormatDateTime('MM',DateTimePicker1.Date);
  tgl:=FormatDateTime('DD',DateTimePicker1.Date);
  JumSeatQ.First;
  for i:=1 to JumSeatQ.RecordCount do
  begin
    cxListBox1.Items.Add(inttostr(JumSeatQJumlahSeat.AsVariant));
    StringGrid1.Cells[0,i]:='Armada ' +cxListBox1.Items.Strings[i-1]+' seat';
    JadwalPagiQ.Close;
    JadwalPagiQ.ParamByName('seat').AsInteger:=strtoint(cxListBox1.Items.Strings[i-1]);
    DateTimePicker1.Time:=cxTimeEdit1.Time;
    JadwalPagiQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    DateTimePicker1.Time:=cxTimeEdit2.Time;
    JadwalPagiQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    JadwalPagiQ.ParamByName('selisih').AsInteger:=cxSpinEdit1.Value;
    JadwalPagiQ.Open;

    JadwalSiangQ.Close;
    JadwalSiangQ.ParamByName('seat').AsInteger:=strtoint(cxListBox1.Items.Strings[i-1]);
    DateTimePicker1.Time:=cxTimeEdit3.Time;
    JadwalSiangQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    DateTimePicker1.Time:=cxTimeEdit4.Time;
    JadwalSiangQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    JadwalSiangQ.ParamByName('selisih').AsInteger:=cxSpinEdit1.Value;
    JadwalSiangQ.Open;

    JadwalSoreQ.Close;
    JadwalSoreQ.ParamByName('seat').AsInteger:=strtoint(cxListBox1.Items.Strings[i-1]);
    DateTimePicker1.Time:=cxTimeEdit5.Time;
    JadwalSoreQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    DateTimePicker1.Time:=cxTimeEdit6.Time;
    JadwalSoreQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    JadwalSoreQ.ParamByName('selisih').AsInteger:=cxSpinEdit1.Value;
    JadwalSoreQ.Open;

    JadwalMalamQ.Close;
    JadwalMalamQ.ParamByName('seat').AsInteger:=strtoint(cxListBox1.Items.Strings[i-1]);
    DateTimePicker1.Time:=cxTimeEdit7.Time;
    JadwalMalamQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    DateTimePicker1.Time:=cxTimeEdit8.Time;
    JadwalMalamQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    JadwalMalamQ.ParamByName('selisih').AsInteger:=cxSpinEdit1.Value;
    JadwalMalamQ.Open;

    {JadwalFullDayQ.Close;
    JadwalFullDayQ.ParamByName('seat').AsInteger:=strtoint(cxListBox1.Items.Strings[i-1]);
    DateTimePicker1.Time:=cxTimeEdit7.Time;
    JadwalFulldayQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    DateTimePicker1.Time:=cxTimeEdit2.Time;
    JadwalFulldayQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    JadwalFulldayQ.ParamByName('selisih').AsInteger:=cxSpinEdit1.Value;
    JadwalFullDayQ.Open; }

    //StringGrid1.Cells[1,i]:=inttostr(JadwalFullDayQTotal.AsInteger);
    StringGrid1.Cells[1,i]:=inttostr(JadwalPagiQTotal.AsInteger);
    StringGrid1.Cells[2,i]:=inttostr(JadwalSiangQTotal.AsInteger);
    StringGrid1.Cells[3,i]:=inttostr(JadwalSoreQTotal.AsInteger);
    StringGrid1.Cells[4,i]:=inttostr(JadwalMalamQTotal.AsInteger);
    JumSeatQ.Next;

  end;
    PerbaikanQ.Close;
    PerbaikanQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    PerbaikanQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    PerbaikanQ.Open;
    Label13.Caption:=inttostr(PerbaikanQTotal.AsInteger);
    PerawatanQ.Close;
    PerawatanQ.ParamByName('JS1').AsDateTime:=DateTimePicker1.DateTime;
    PerawatanQ.ParamByName('JS2').AsDateTime:=DateTimePicker1.DateTime;
    PerawatanQ.Open;
    Label15.Caption:=inttostr(PerawatanQTotal.AsInteger);
end;

procedure TViewJadwalFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TViewJadwalFm.FormCreate(Sender: TObject);
var tahun,bulan,tgl:string;
begin
JumSeatQ.Close;
JumSeatQ.Open;
JumData:=JumSeatQ.RecordCount+1;
StringGrid1.RowCount:=JumData;
StringGrid1.ColCount:=5;
StringGrid1.ColWidths[0]:=120;

  //StringGrid1.Cells[1,0]:='Full Day';
  StringGrid1.Cells[1,0]:='Pagi';
  StringGrid1.Cells[2,0]:='Siang';
  StringGrid1.Cells[3,0]:='Sore';
  StringGrid1.Cells[4,0]:='Malam';
  SpinEdit1.Value:=0;
  SpinEdit2.Value:=10;
  SpinEdit3.Value:=10;
  SpinEdit4.Value:=15;
  SpinEdit5.Value:=15;
  SpinEdit6.Value:=18;
  SpinEdit7.Value:=18;
  SpinEdit8.Value:=24;
end;

procedure TViewJadwalFm.SpinEdit1KeyPress(Sender: TObject; var Key: Char);
begin
if SpinEdit1.Value<0 then
begin
   SpinEdit1.Value:=24;
end;
end;

procedure TViewJadwalFm.SpinEdit1Change(Sender: TObject);
begin
if SpinEdit1.Value<0 then
begin
   SpinEdit1.Value:=24;
end;

if SpinEdit1.Value>24 then
begin
   SpinEdit1.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit2Change(Sender: TObject);
begin
if SpinEdit2.Value<0 then
begin
   SpinEdit2.Value:=24;
end;

if SpinEdit2.Value>24 then
begin
   SpinEdit2.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit3Change(Sender: TObject);
begin
if SpinEdit3.Value<0 then
begin
   SpinEdit3.Value:=24;
end;

if SpinEdit3.Value>24 then
begin
   SpinEdit3.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit4Change(Sender: TObject);
begin
if SpinEdit4.Value<0 then
begin
   SpinEdit4.Value:=24;
end;

if SpinEdit4.Value>24 then
begin
   SpinEdit4.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit5Change(Sender: TObject);
begin
if SpinEdit5.Value<0 then
begin
   SpinEdit5.Value:=24;
end;

if SpinEdit5.Value>24 then
begin
   SpinEdit5.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit6Change(Sender: TObject);
begin
if SpinEdit6.Value<0 then
begin
   SpinEdit6.Value:=24;
end;

if SpinEdit6.Value>24 then
begin
   SpinEdit6.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit7Change(Sender: TObject);
begin
if SpinEdit7.Value<0 then
begin
   SpinEdit7.Value:=24;
end;

if SpinEdit7.Value>24 then
begin
   SpinEdit7.Value:=0;
end;
end;

procedure TViewJadwalFm.SpinEdit8Change(Sender: TObject);
begin
if SpinEdit8.Value<0 then
begin
   SpinEdit8.Value:=24;
end;

if SpinEdit8.Value>24 then
begin
   SpinEdit8.Value:=0;
end;
end;

procedure TViewJadwalFm.Button2Click(Sender: TObject);
begin
  SpinEdit1.Value:=0;
  SpinEdit2.Value:=10;
  SpinEdit3.Value:=10;
  SpinEdit4.Value:=15;
  SpinEdit5.Value:=15;
  SpinEdit6.Value:=18;
  SpinEdit7.Value:=18;
  SpinEdit8.Value:=24;
end;

procedure TViewJadwalFm.FormShow(Sender: TObject);
begin
  DateTimePicker1.DateTime:=Today;
  DateTimePicker2.DateTime:=Today;
end;

procedure TViewJadwalFm.DateTimePicker1Change(Sender: TObject);
begin
  DateTimePicker2.DateTime:=DateTimePicker1.DateTime;
end;

procedure TViewJadwalFm.DateTimePicker2Change(Sender: TObject);
begin
  if DateTimePicker2.DateTime<DateTimePicker1.DateTime then
  begin
    ShowMessage('Tanggal (Range) harus pada hari sesudah Tanggal di atas');
    DateTimePicker2.DateTime:=DateTimePicker1.DateTime;
  end;
end;

end.
