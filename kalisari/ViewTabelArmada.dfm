object ViewTabelArmadaFm: TViewTabelArmadaFm
  Left = 45
  Top = 231
  Width = 1050
  Height = 631
  Caption = 'View Tabel Armada'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 1034
    Height = 479
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1032
      Height = 477
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        OnCustomDrawCell = cxGridDBTableView1CustomDrawCell
        DataController.DataSource = DSMTArmada
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGridDBTableView1cek: TcxGridDBColumn
          DataBinding.FieldName = 'cek'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = False
          Properties.ValueUnchecked = 'True'
        end
        object cxGridDBTableView1Kode: TcxGridDBColumn
          DataBinding.FieldName = 'Kode'
          Options.Editing = False
        end
        object cxGridDBTableView1NoPlat: TcxGridDBColumn
          DataBinding.FieldName = 'NoPlat'
          Options.Editing = False
        end
        object cxGridDBTableView1NoBody: TcxGridDBColumn
          DataBinding.FieldName = 'NoBody'
          Options.Editing = False
        end
        object cxGridDBTableView1Trayek: TcxGridDBColumn
          DataBinding.FieldName = 'Trayek'
          Options.Editing = False
        end
        object cxGridDBTableView1JamTrayek: TcxGridDBColumn
          DataBinding.FieldName = 'JamTrayek'
          Options.Editing = False
        end
        object cxGridDBTableView1Kontrak: TcxGridDBColumn
          DataBinding.FieldName = 'Kontrak'
          Options.Editing = False
        end
        object cxGridDBTableView1NamaSopir: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSopir'
          Options.Editing = False
          Width = 126
        end
        object cxGridDBTableView1NamaKondektur: TcxGridDBColumn
          DataBinding.FieldName = 'NamaKondektur'
          Options.Editing = False
          Width = 123
        end
      end
      object cxGrid2DBTableView1: TcxGridDBTableView
        DataController.DataSource = MasterDS
        DataController.DetailKeyFieldNames = 'TabelArmada'
        DataController.MasterKeyFieldNames = 'Kode'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1NamaSopir: TcxGridDBColumn
          DataBinding.FieldName = 'NamaSopir'
          Width = 143
        end
        object cxGrid2DBTableView1Kernet: TcxGridDBColumn
          DataBinding.FieldName = 'Kernet'
          Width = 143
        end
        object cxGrid2DBTableView1SO: TcxGridDBColumn
          DataBinding.FieldName = 'SO'
        end
        object cxGrid2DBTableView1Asal: TcxGridDBColumn
          DataBinding.FieldName = 'Asal'
          Width = 160
        end
        object cxGrid2DBTableView1Tujuan: TcxGridDBColumn
          DataBinding.FieldName = 'Tujuan'
          Width = 167
        end
        object cxGrid2DBTableView1TanggalSO: TcxGridDBColumn
          DataBinding.FieldName = 'TanggalSO'
        end
        object cxGrid2DBTableView1Keterangan: TcxGridDBColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 182
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 574
    Width = 1034
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 520
    Width = 1034
    Height = 54
    Align = alBottom
    TabOrder = 2
    object Button3: TButton
      Left = 5
      Top = 5
      Width = 121
      Height = 45
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button3Click
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1034
    Height = 41
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 56
      Height = 13
      Caption = 'TANGGAL : '
    end
    object cxDateEdit1: TcxDateEdit
      Left = 64
      Top = 13
      Properties.OnChange = cxDateEdit1PropertiesChange
      TabOrder = 0
      Width = 121
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Pegawai')
    Left = 744
    Top = 248
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 50
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 50
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PegawaiQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 50
    end
    object PegawaiQNoHP: TStringField
      FieldName = 'NoHP'
      Size = 50
    end
    object PegawaiQNoKTP: TStringField
      FieldName = 'NoKTP'
      Size = 50
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQGaji: TCurrencyField
      FieldName = 'Gaji'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 50
    end
    object PegawaiQMulaiBekerja: TDateTimeField
      FieldName = 'MulaiBekerja'
    end
    object PegawaiQNomorSIM: TStringField
      FieldName = 'NomorSIM'
      Size = 50
    end
    object PegawaiQExpiredSIM: TDateTimeField
      FieldName = 'ExpiredSIM'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object MasterTabelArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from MasterTabelArmada')
    UpdateObject = SDUMTArmada
    Left = 680
    Top = 248
    object MasterTabelArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterTabelArmadaQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterTabelArmadaQTrayek: TStringField
      FieldName = 'Trayek'
      Size = 10
    end
    object MasterTabelArmadaQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object MasterTabelArmadaQKondektur: TStringField
      FieldName = 'Kondektur'
      Size = 10
    end
    object MasterTabelArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterTabelArmadaQcek: TBooleanField
      FieldName = 'cek'
    end
    object MasterTabelArmadaQNoPlat: TStringField
      FieldKind = fkLookup
      FieldName = 'NoPlat'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'PlatNo'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterTabelArmadaQNoBody: TStringField
      FieldKind = fkLookup
      FieldName = 'NoBody'
      LookupDataSet = ArmadaQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NoBody'
      KeyFields = 'Armada'
      Size = 10
      Lookup = True
    end
    object MasterTabelArmadaQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Size = 50
      Lookup = True
    end
    object MasterTabelArmadaQNamaKondektur: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKondektur'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kondektur'
      Size = 50
      Lookup = True
    end
    object MasterTabelArmadaQJamTrayek: TMemoField
      FieldName = 'JamTrayek'
      BlobType = ftMemo
    end
  end
  object MasterDS: TDataSource
    DataSet = MasterQ
    Left = 680
    Top = 312
  end
  object SDUMaster: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, TabelArmada, Sopir, Kernet, SO, Keterangan'
      'from DetailTabelArmada'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    ModifySQL.Strings = (
      'update DetailTabelArmada'
      'set'
      '  Kode = :Kode,'
      '  TabelArmada = :TabelArmada,'
      '  Sopir = :Sopir,'
      '  Kernet = :Kernet,'
      '  SO = :SO,'
      '  Keterangan = :Keterangan'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    InsertSQL.Strings = (
      'insert into DetailTabelArmada'
      '  (Kode, TabelArmada, Sopir, Kernet, SO, Keterangan)'
      'values'
      '  (:Kode, :TabelArmada, :Sopir, :Kernet, :SO, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from DetailTabelArmada'
      'where'
      '  Kode = :OLD_Kode and'
      '  TabelArmada = :OLD_TabelArmada')
    Left = 712
    Top = 312
  end
  object DSMTArmada: TDataSource
    DataSet = MasterTabelArmadaQ
    Left = 648
    Top = 248
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Armada')
    Left = 744
    Top = 280
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Visible = False
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'PlatNo'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'NoBody'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
    object StringField6: TStringField
      FieldName = 'JenisKendaraan'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'JumlahSeat'
    end
    object StringField7: TStringField
      FieldName = 'JenisBBM'
      Size = 50
    end
    object StringField8: TStringField
      FieldName = 'TahunPembuatan'
      Size = 5
    end
    object StringField9: TStringField
      FieldName = 'JenisAC'
      Size = 10
    end
    object BooleanField1: TBooleanField
      FieldName = 'Toilet'
    end
    object BooleanField2: TBooleanField
      FieldName = 'AirSuspension'
    end
    object StringField10: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object IntegerField2: TIntegerField
      FieldName = 'KapasitasTangkiBBM'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object StringField11: TStringField
      FieldName = 'LevelArmada'
      Size = 50
    end
    object IntegerField3: TIntegerField
      FieldName = 'JumlahBan'
    end
    object StringField12: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object BooleanField3: TBooleanField
      FieldName = 'Aktif'
    end
    object BooleanField4: TBooleanField
      FieldName = 'AC'
    end
    object IntegerField4: TIntegerField
      FieldName = 'KmSekarang'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object StringField13: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object StringField14: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'KirMulai'
    end
  end
  object SDUMTArmada: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopi' +
        'r, cek'#13#10'from masterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update masterTabelArmada'
      'set'
      '  Kode = :Kode,'
      '  Armada = :Armada,'
      '  Trayek = :Trayek,'
      '  JamTrayek = :JamTrayek,'
      '  Kontrak = :Kontrak,'
      '  Kondektur = :Kondektur,'
      '  Sopir = :Sopir,'
      '  cek = :cek'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into masterTabelArmada'
      
        '  (Kode, Armada, Trayek, JamTrayek, Kontrak, Kondektur, Sopir, c' +
        'ek)'
      'values'
      
        '  (:Kode, :Armada, :Trayek, :JamTrayek, :Kontrak, :Kondektur, :S' +
        'opir, :cek)')
    DeleteSQL.Strings = (
      'delete from masterTabelArmada'
      'where'
      '  Kode = :OLD_Kode')
    Left = 712
    Top = 248
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from DetailTabelArmada dta, MasterSO so'
      'where dta.SO=so.kodenota'
      'and cast(so.berangkat as date) =:text2')
    UpdateObject = SDUMaster
    Left = 648
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTabelArmada: TStringField
      FieldName = 'TabelArmada'
      Required = True
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object MasterQKernet: TStringField
      FieldName = 'Kernet'
      Size = 50
    end
    object MasterQSO: TStringField
      FieldName = 'SO'
      Size = 10
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQNamaSopir: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSopir'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Sopir'
      Size = 50
      Lookup = True
    end
    object MasterQRute: TStringField
      FieldKind = fkLookup
      FieldName = 'Rute'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Rute'
      KeyFields = 'SO'
      Size = 10
      Lookup = True
    end
    object MasterQAsal: TStringField
      FieldKind = fkLookup
      FieldName = 'Asal'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Muat'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object MasterQTujuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Tujuan'
      LookupDataSet = RuteQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Bongkar'
      KeyFields = 'Rute'
      Size = 50
      Lookup = True
    end
    object MasterQTanggalSO: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'TanggalSO'
      LookupDataSet = SOQ
      LookupKeyFields = 'Kodenota'
      LookupResultField = 'Tgl'
      KeyFields = 'SO'
      Lookup = True
    end
  end
  object RuteQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from Rute')
    Left = 744
    Top = 312
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 250
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 250
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQKategori: TStringField
      FieldName = 'Kategori'
      Size = 50
    end
    object RuteQLevelRute: TStringField
      FieldName = 'LevelRute'
      Size = 50
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQPremiPengemudi: TCurrencyField
      FieldName = 'PremiPengemudi'
    end
    object RuteQPremiKernet: TCurrencyField
      FieldName = 'PremiKernet'
    end
    object RuteQPremiKondektur: TCurrencyField
      FieldName = 'PremiKondektur'
    end
    object RuteQMel: TCurrencyField
      FieldName = 'Mel'
    end
    object RuteQTol: TCurrencyField
      FieldName = 'Tol'
    end
    object RuteQUangJalanBesar: TCurrencyField
      FieldName = 'UangJalanBesar'
    end
    object RuteQUangJalanKecil: TCurrencyField
      FieldName = 'UangJalanKecil'
    end
    object RuteQUangBBM: TCurrencyField
      FieldName = 'UangBBM'
    end
    object RuteQUangMakan: TCurrencyField
      FieldName = 'UangMakan'
    end
    object RuteQWaktu: TIntegerField
      FieldName = 'Waktu'
    end
    object RuteQStandarHargaMax: TCurrencyField
      FieldName = 'StandarHargaMax'
    end
    object RuteQStandarHarga: TCurrencyField
      FieldName = 'StandarHarga'
    end
    object RuteQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object RuteQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object RuteQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object RuteQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object SOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from masterso')
    Left = 744
    Top = 344
    object SOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SOQBerangkat: TDateTimeField
      FieldName = 'Berangkat'
    end
    object SOQTiba: TDateTimeField
      FieldName = 'Tiba'
    end
    object SOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SOQPPN: TCurrencyField
      FieldName = 'PPN'
    end
    object SOQPembayaranAwal: TCurrencyField
      FieldName = 'PembayaranAwal'
    end
    object SOQTglPembayaranAwal: TDateTimeField
      FieldName = 'TglPembayaranAwal'
    end
    object SOQCaraPembayaranAwal: TStringField
      FieldName = 'CaraPembayaranAwal'
      Size = 50
    end
    object SOQKeteranganCaraPembayaranAwal: TStringField
      FieldName = 'KeteranganCaraPembayaranAwal'
      Size = 50
    end
    object SOQNoKwitansiPembayaranAwal: TStringField
      FieldName = 'NoKwitansiPembayaranAwal'
      Size = 50
    end
    object SOQNominalKwitansiPembayaranAwal: TCurrencyField
      FieldName = 'NominalKwitansiPembayaranAwal'
    end
    object SOQPenerimaPembayaranAwal: TStringField
      FieldName = 'PenerimaPembayaranAwal'
      Size = 10
    end
    object SOQPelunasan: TCurrencyField
      FieldName = 'Pelunasan'
    end
    object SOQTglPelunasan: TDateTimeField
      FieldName = 'TglPelunasan'
    end
    object SOQCaraPembayaranPelunasan: TStringField
      FieldName = 'CaraPembayaranPelunasan'
      Size = 50
    end
    object SOQKetCaraPembayaranPelunasan: TStringField
      FieldName = 'KetCaraPembayaranPelunasan'
      Size = 50
    end
    object SOQNoKwitansiPelunasan: TStringField
      FieldName = 'NoKwitansiPelunasan'
      Size = 50
    end
    object SOQNominalKwitansiPelunasan: TCurrencyField
      FieldName = 'NominalKwitansiPelunasan'
    end
    object SOQPenerimaPelunasan: TStringField
      FieldName = 'PenerimaPelunasan'
      Size = 10
    end
    object SOQExtend: TBooleanField
      FieldName = 'Extend'
    end
    object SOQTglKembaliExtend: TDateTimeField
      FieldName = 'TglKembaliExtend'
    end
    object SOQBiayaExtend: TCurrencyField
      FieldName = 'BiayaExtend'
    end
    object SOQPPNExtend: TCurrencyField
      FieldName = 'PPNExtend'
    end
    object SOQKapasitasSeat: TIntegerField
      FieldName = 'KapasitasSeat'
    end
    object SOQAC: TBooleanField
      FieldName = 'AC'
    end
    object SOQToilet: TBooleanField
      FieldName = 'Toilet'
    end
    object SOQAirSuspension: TBooleanField
      FieldName = 'AirSuspension'
    end
    object SOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SOQPICJemput: TMemoField
      FieldName = 'PICJemput'
      BlobType = ftMemo
    end
    object SOQJamJemput: TDateTimeField
      FieldName = 'JamJemput'
    end
    object SOQNoTelpPICJemput: TStringField
      FieldName = 'NoTelpPICJemput'
      Size = 50
    end
    object SOQAlamatJemput: TMemoField
      FieldName = 'AlamatJemput'
      BlobType = ftMemo
    end
    object SOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SOQStatusPembayaran: TStringField
      FieldName = 'StatusPembayaran'
      Size = 50
    end
    object SOQReminderPending: TDateTimeField
      FieldName = 'ReminderPending'
    end
    object SOQPenerimaPending: TStringField
      FieldName = 'PenerimaPending'
      Size = 10
    end
    object SOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SOQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SOQKomisiPelanggan: TCurrencyField
      FieldName = 'KomisiPelanggan'
    end
    object SOQKeteranganRute: TMemoField
      FieldName = 'KeteranganRute'
      BlobType = ftMemo
    end
    object SOQKeteranganHarga: TMemoField
      FieldName = 'KeteranganHarga'
      BlobType = ftMemo
    end
  end
end
