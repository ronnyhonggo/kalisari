unit ViewTabelArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxTimeEdit, cxCheckBox, Buttons, cxDropDownEdit,
  cxGroupBox;

type
  TViewTabelArmadaFm = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQNoTelp: TStringField;
    PegawaiQNoHP: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQGaji: TCurrencyField;
    PegawaiQJabatan: TStringField;
    PegawaiQMulaiBekerja: TDateTimeField;
    PegawaiQNomorSIM: TStringField;
    PegawaiQExpiredSIM: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQKeterangan: TMemoField;
    PegawaiQNoKTP: TStringField;
    MasterTabelArmadaQ: TSDQuery;
    Panel2: TPanel;
    Button3: TButton;
    MasterDS: TDataSource;
    SDUMaster: TSDUpdateSQL;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    DSMTArmada: TDataSource;
    ArmadaQ: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField1: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    StringField10: TStringField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    StringField11: TStringField;
    IntegerField3: TIntegerField;
    StringField12: TStringField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    IntegerField4: TIntegerField;
    DateTimeField3: TDateTimeField;
    StringField13: TStringField;
    StringField14: TStringField;
    DateTimeField4: TDateTimeField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    MasterTabelArmadaQKode: TStringField;
    MasterTabelArmadaQArmada: TStringField;
    MasterTabelArmadaQTrayek: TStringField;
    MasterTabelArmadaQKontrak: TStringField;
    MasterTabelArmadaQKondektur: TStringField;
    MasterTabelArmadaQSopir: TStringField;
    MasterTabelArmadaQcek: TBooleanField;
    cxGridDBTableView1Kode: TcxGridDBColumn;
    cxGridDBTableView1Trayek: TcxGridDBColumn;
    cxGridDBTableView1Kontrak: TcxGridDBColumn;
    cxGridDBTableView1cek: TcxGridDBColumn;
    MasterTabelArmadaQNoPlat: TStringField;
    MasterTabelArmadaQNoBody: TStringField;
    MasterTabelArmadaQNamaSopir: TStringField;
    MasterTabelArmadaQNamaKondektur: TStringField;
    cxGridDBTableView1NoPlat: TcxGridDBColumn;
    cxGridDBTableView1NoBody: TcxGridDBColumn;
    cxGridDBTableView1NamaSopir: TcxGridDBColumn;
    cxGridDBTableView1NamaKondektur: TcxGridDBColumn;
    SDUMTArmada: TSDUpdateSQL;
    MasterTabelArmadaQJamTrayek: TMemoField;
    cxGridDBTableView1JamTrayek: TcxGridDBColumn;
    MasterQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQTabelArmada: TStringField;
    MasterQSopir: TStringField;
    MasterQKernet: TStringField;
    MasterQSO: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQNamaSopir: TStringField;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQJarak: TIntegerField;
    RuteQKategori: TStringField;
    RuteQLevelRute: TStringField;
    RuteQPoin: TFloatField;
    RuteQPremiPengemudi: TCurrencyField;
    RuteQPremiKernet: TCurrencyField;
    RuteQPremiKondektur: TCurrencyField;
    RuteQMel: TCurrencyField;
    RuteQTol: TCurrencyField;
    RuteQUangJalanBesar: TCurrencyField;
    RuteQUangJalanKecil: TCurrencyField;
    RuteQUangBBM: TCurrencyField;
    RuteQUangMakan: TCurrencyField;
    RuteQWaktu: TIntegerField;
    RuteQStandarHargaMax: TCurrencyField;
    RuteQStandarHarga: TCurrencyField;
    RuteQCreateDate: TDateTimeField;
    RuteQCreateBy: TStringField;
    RuteQOperator: TStringField;
    RuteQTglEntry: TDateTimeField;
    SOQ: TSDQuery;
    SOQKodenota: TStringField;
    SOQTgl: TDateTimeField;
    SOQPelanggan: TStringField;
    SOQBerangkat: TDateTimeField;
    SOQTiba: TDateTimeField;
    SOQHarga: TCurrencyField;
    SOQPPN: TCurrencyField;
    SOQPembayaranAwal: TCurrencyField;
    SOQTglPembayaranAwal: TDateTimeField;
    SOQCaraPembayaranAwal: TStringField;
    SOQKeteranganCaraPembayaranAwal: TStringField;
    SOQNoKwitansiPembayaranAwal: TStringField;
    SOQNominalKwitansiPembayaranAwal: TCurrencyField;
    SOQPenerimaPembayaranAwal: TStringField;
    SOQPelunasan: TCurrencyField;
    SOQTglPelunasan: TDateTimeField;
    SOQCaraPembayaranPelunasan: TStringField;
    SOQKetCaraPembayaranPelunasan: TStringField;
    SOQNoKwitansiPelunasan: TStringField;
    SOQNominalKwitansiPelunasan: TCurrencyField;
    SOQPenerimaPelunasan: TStringField;
    SOQExtend: TBooleanField;
    SOQTglKembaliExtend: TDateTimeField;
    SOQBiayaExtend: TCurrencyField;
    SOQPPNExtend: TCurrencyField;
    SOQKapasitasSeat: TIntegerField;
    SOQAC: TBooleanField;
    SOQToilet: TBooleanField;
    SOQAirSuspension: TBooleanField;
    SOQRute: TStringField;
    SOQTglFollowUp: TDateTimeField;
    SOQArmada: TStringField;
    SOQKontrak: TStringField;
    SOQPICJemput: TMemoField;
    SOQJamJemput: TDateTimeField;
    SOQNoTelpPICJemput: TStringField;
    SOQAlamatJemput: TMemoField;
    SOQStatus: TStringField;
    SOQStatusPembayaran: TStringField;
    SOQReminderPending: TDateTimeField;
    SOQPenerimaPending: TStringField;
    SOQKeterangan: TMemoField;
    SOQCreateDate: TDateTimeField;
    SOQCreateBy: TStringField;
    SOQOperator: TStringField;
    SOQTglEntry: TDateTimeField;
    SOQTglCetak: TDateTimeField;
    SOQKomisiPelanggan: TCurrencyField;
    SOQKeteranganRute: TMemoField;
    SOQKeteranganHarga: TMemoField;
    MasterQRute: TStringField;
    MasterQAsal: TStringField;
    MasterQTujuan: TStringField;
    MasterQTanggalSO: TDateTimeField;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Kernet: TcxGridDBColumn;
    cxGrid2DBTableView1SO: TcxGridDBColumn;
    cxGrid2DBTableView1Keterangan: TcxGridDBColumn;
    cxGrid2DBTableView1NamaSopir: TcxGridDBColumn;
    cxGrid2DBTableView1Asal: TcxGridDBColumn;
    cxGrid2DBTableView1Tujuan: TcxGridDBColumn;
    cxGrid2DBTableView1TanggalSO: TcxGridDBColumn;
    Panel3: TPanel;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    procedure FormCreate(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxGridDBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ViewTabelArmadaFm: TViewTabelArmadaFm;
  MasterOriSQL: string;
  DetailBonBarangOriSQL: string;
  paramkode,paramkode2 :string;
  baru,index:integer;
  sopir,kernet,so,keterangan : string;
  SOhapus : array [1..100] of string;

implementation

uses DM, MenuUtama, DropDown, BarangDropDown, SPKDropDown, StoringDropDown, PegawaiDropDown,
  ArmadaDropDown, KontrakDropDown, SODropDown, SOTabelArmadaDropDown;

{$R *.dfm}

procedure TViewTabelArmadaFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
end;

procedure TViewTabelArmadaFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TViewTabelArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TViewTabelArmadaFm.FormShow(Sender: TObject);
begin
  index:=1;
  SOQ.Open;
  RuteQ.Open;
  ArmadaQ.Open;
  PegawaiQ.Open;
  cxDateEdit1.Date:=Date;
  MasterTabelArmadaQ.Open;
  MasterQ.Close;
  MasterQ.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  MasterQ.Open;
  cxGridDBTableView1.ViewData.Expand(true);
end;

procedure TViewTabelArmadaFm.Button3Click(Sender: TObject);
begin
  Release;
end;

procedure TViewTabelArmadaFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  MasterQ.Close;
  MasterQ.ParamByName('text2').AsDate:=cxDateEdit1.Date;
  MasterQ.Open;
end;

procedure TViewTabelArmadaFm.cxGridDBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if(AViewInfo.GridRecord.Values[0] = true) then
  begin
    ACanvas.Brush.Color := clRed;
  end
  else
  begin
  //ACanvas.Brush.Color := clWhite;
  end;
end;

end.
