unit Browse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, SDEngine,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TBrowseFm = class(TForm)
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    DataSourceBrowse: TDataSource;
    BrowseQ: TSDQuery;
    procedure cxGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:String;
    nama:String;
    dari:String;

  end;

var
  BrowseFm: TBrowseFm;

implementation
uses menuutama, BonBarang;

{$R *.dfm}

procedure TBrowseFm.cxGridDBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=13 then
  begin
    MenuUtamaFm.param_kodebon:= BrowseQ.Fields[0].AsString;
    //ShowMessage(BonBarangFm.param_kodebon);
    MenuUtamaFm.param_nama:=BrowseQ.Fields[1].AsString;
    Close;
  end;
end;

procedure TBrowseFm.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    MenuUtamaFm.param_kodebon:= BrowseQ.Fields[0].AsString;
    kode:=BrowseQ.Fields[0].AsString;
    nama:=BrowseQ.Fields[1].AsString;
    MenuUtamaFm.param_nama:=BrowseQ.Fields[1].AsString;
    if dari='po' then
        dari:=BrowseQ.Fields[3].AsString;
    if dari='retur' then
        dari:=BrowseQ.Fields[9].AsString;
    Close;
end;

procedure TBrowseFm.FormCreate(Sender: TObject);
var i:Integer;
    temp: String;
begin

end;

procedure TBrowseFm.FormActivate(Sender: TObject);
var i:Integer;
    temp: String;
begin
 try
    MenuUtamaFm.param_kodebon:='';
    MenuUtamaFm.param_kode:='';
    MenuUtamaFm.param_nama:='';
    kode:='';
    nama:='';
    dari:='';

    temp:=MenuUtamaFm.param_sql;
    if temp='' then
      temp:='select * from armada';

    BrowseQ.SQL.Text:=temp;
    DataSourceBrowse.DataSet:=BrowseQ;
    cxGridDBTableView1.DataController.DataSource:=DataSourceBrowse;

    BrowseQ.Open;
    cxGridDBTableView1.ClearItems;
    for i:=0 to DataSourceBrowse.DataSet.FieldCount-1 do
    begin
      cxGridDBTableView1.CreateColumn.DataBinding.FieldName:=DataSourceBrowse.DataSet.Fields[i].FieldName;
      cxGridDBTableView1.GetColumnByFieldName(DataSourceBrowse.DataSet.Fields[i].FieldName).Options.Editing:=true;
    end;
  except
    ShowMessage('gagal');
  end
end;

end.
