unit riwayat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxHyperLinkEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  SDEngine, cxButtonEdit, UCrpeClasses, UCrpe32, StdCtrls, ComCtrls,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  ExtCtrls, cxContainer, Menus, cxButtons, cxTextEdit;

type
  TRiwayatFm = class(TForm)
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SDQuery1Kode: TStringField;
    SDQuery1Nama: TStringField;
    SDQuery1Satuan: TStringField;
    SDQuery1MinimumStok: TIntegerField;
    SDQuery1MaximumStok: TIntegerField;
    SDQuery1StandardUmur: TIntegerField;
    SDQuery1Lokasi: TStringField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    cxGrid1DBTableView1MinimumStok: TcxGridDBColumn;
    cxGrid1DBTableView1MaximumStok: TcxGridDBColumn;
    cxGrid1DBTableView1StandardUmur: TcxGridDBColumn;
    cxGrid1DBTableView1Lokasi: TcxGridDBColumn;
    Crpe1: TCrpe;
    SDQuery1riwayat_harga: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    cxSearchText: TcxTextEdit;
    SDQuery1Jumlah: TFloatField;
    procedure cxGrid1DBTableView1NamaPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure cxSearchTextPropertiesChange(Sender: TObject);
    procedure cxSearchTextEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    kodebar,namaBarang:String;
    stokSekarang: integer;
  end;

var
  RiwayatFm: TRiwayatFm;
  OriSQL:string;

implementation
uses DateUtils, menuutama, StrUtils,RiwayatBarang;

{$R *.dfm}

procedure TRiwayatFm.cxGrid1DBTableView1NamaPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var jum_hari:integer;
begin
  kodebar:=SDQuery1Kode.AsString;
  namaBarang:=SDQuery1Nama.AsString;
  stokSekarang:=SDQuery1Jumlah.AsInteger;
  riwayatbarangFm:= Triwayatbarangfm.create(self);
    self.Visible:=false;
  {Crpe1.Refresh;
  Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Riwayat.rpt';
  Crpe1.ParamFields[0].CurrentValue:=SDQuery1Kode.AsString;
  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[3].CurrentValue:='date';
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', Now);
    end
    else
    begin
      Crpe1.ParamFields[3].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[2].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end;

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
  //ShowMessage(Crpe1.Connect.ServerName);

  Crpe1.Execute;
  Crpe1.WindowState:= wsMaximized;   }
end;

procedure TRiwayatFm.FormCreate(Sender: TObject);
begin
{DateTimePickerDay.DateTime:=now;
DateTimePickerMonth.DateTime:=now;
DateTimePickerMonth2.DateTime:=now;}
end;

procedure TRiwayatFm.cxSearchTextPropertiesChange(Sender: TObject);
begin
  SDQuery1.Close;
  SDQuery1.SQL.Text:='select *,'+QuotedStr('')+' as riwayat_harga from barang where nama like '+QuotedStr('%'+cxSearchText.Text+'%');
//  SDQuery1.ExecSQL;
  SDQuery1.Open;
end;

procedure TRiwayatFm.cxSearchTextEnter(Sender: TObject);
begin
  cxSearchText.Text:='';
end;

procedure TRiwayatFm.FormShow(Sender: TObject);
begin
  OriSQL:=SDQuery1.SQL.Text;
  cxSearchText.SetFocus;
  SDQuery1.Open;
end;

procedure TRiwayatFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SDQuery1.SQL.Text:=OriSQL;
end;

end.
