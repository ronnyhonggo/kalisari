USE [master]
GO
/****** Object:  Database [Kalisari]    Script Date: 04/08/2013 18:28:16 ******/
CREATE DATABASE [Kalisari] ON  PRIMARY 
( NAME = N'Kalisari', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\Kalisari.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Kalisari_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\Kalisari_log.ldf' , SIZE = 2816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Kalisari] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Kalisari].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Kalisari] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Kalisari] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Kalisari] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Kalisari] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Kalisari] SET ARITHABORT OFF
GO
ALTER DATABASE [Kalisari] SET AUTO_CLOSE ON
GO
ALTER DATABASE [Kalisari] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Kalisari] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Kalisari] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Kalisari] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Kalisari] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Kalisari] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Kalisari] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Kalisari] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Kalisari] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Kalisari] SET  DISABLE_BROKER
GO
ALTER DATABASE [Kalisari] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Kalisari] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Kalisari] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Kalisari] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Kalisari] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Kalisari] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Kalisari] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Kalisari] SET  READ_WRITE
GO
ALTER DATABASE [Kalisari] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Kalisari] SET  MULTI_USER
GO
ALTER DATABASE [Kalisari] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Kalisari] SET DB_CHAINING OFF
GO
USE [Kalisari]
GO
/****** Object:  Table [dbo].[Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pegawai](
	[Kode] [varchar](10) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Alamat] [varchar](50) NULL,
	[Kota] [varchar](50) NULL,
	[NoTelp] [varchar](50) NULL,
	[NoHP] [varchar](50) NULL,
	[TglLahir] [datetime] NULL,
	[Gaji] [money] NULL,
	[Jabatan] [varchar](50) NULL,
	[MulaiBekerja] [datetime] NULL,
	[NomorSIM] [varchar](50) NULL,
	[ExpiredSIM] [datetime] NULL,
	[Aktif] [bit] NULL,
	[Keterangan] [varchar](max) NULL,
	[NoKTP] [varchar](50) NULL,
 CONSTRAINT [PK_Pegawai] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Kode] [varchar](10) NOT NULL,
	[Username] [nchar](10) NOT NULL,
	[Password] [nchar](10) NOT NULL,
	[Jenis] [varchar](50) NULL,
	[KodePegawai] [varchar](10) NULL,
	[MasterBKanibal] [bit] NULL,
	[InsertMasterBKanibal] [bit] NULL,
	[UpdateMasterBKanibal] [bit] NULL,
	[DeleteMasterBKanibal] [bit] NULL,
	[MasterKBarang] [bit] NULL,
	[InsertMasterKBarang] [bit] NULL,
	[UpdateMasterKBarang] [bit] NULL,
	[DeleteMasterKBarang] [bit] NULL,
	[MasterAC] [bit] NULL,
	[InsertMasterAC] [bit] NULL,
	[UpdateMasterAC] [bit] NULL,
	[DeleteMasterAC] [bit] NULL,
	[MasterArmada] [bit] NULL,
	[InsertMasterArmada] [bit] NULL,
	[UpdateMasterArmada] [bit] NULL,
	[DeleteMasterArmada] [bit] NULL,
	[MasterJenisKendaraan] [bit] NULL,
	[InsertJenisKendaraan] [bit] NULL,
	[UpdateJenisKendaraan] [bit] NULL,
	[DeleteJenisKendaraan] [bit] NULL,
	[MasterEkor] [bit] NULL,
	[InsertMasterEkor] [bit] NULL,
	[UpdateMasterEkor] [bit] NULL,
	[DeleteMasterEkor] [bit] NULL,
	[MasterPelanggan] [bit] NULL,
	[InsertMasterPelanggan] [bit] NULL,
	[UpdateMasterPelanggan] [bit] NULL,
	[DeleteMasterPelanggan] [bit] NULL,
	[MasterRute] [bit] NULL,
	[InsertMasterRute] [bit] NULL,
	[UpdateMasterRute] [bit] NULL,
	[DeleteMasterRute] [bit] NULL,
	[MasterSopir] [bit] NULL,
	[InsertMasterSopir] [bit] NULL,
	[UpdateMasterSopir] [bit] NULL,
	[DeleteMasterSopir] [bit] NULL,
	[MasterJarak] [bit] NULL,
	[InsertMasterJarak] [bit] NULL,
	[UpdateMasterJarak] [bit] NULL,
	[DeleteMasterJarak] [bit] NULL,
	[MasterBan] [bit] NULL,
	[InsertMasterBan] [bit] NULL,
	[UpdateMasterBan] [bit] NULL,
	[DeleteMasterBan] [bit] NULL,
	[MasterStandardPerawatan] [bit] NULL,
	[InsertMasterStandardPerawatan] [bit] NULL,
	[UpdateMasterStandardPerawatan] [bit] NULL,
	[DeleteMasterStandardPerawatan] [bit] NULL,
	[MasterBarang] [bit] NULL,
	[InsertMasterBarang] [bit] NULL,
	[UpdateMasterBarang] [bit] NULL,
	[DeleteMasterBarang] [bit] NULL,
	[MasterJenisPerbaikan] [bit] NULL,
	[InsertMasterJenisPerbaikan] [bit] NULL,
	[UpdateMasterJenisPerbaikan] [bit] NULL,
	[DeleteMasterJenisPerbaikan] [bit] NULL,
	[MasterSupplier] [bit] NULL,
	[InsertMasterSupplier] [bit] NULL,
	[UpdateMasterSupplier] [bit] NULL,
	[DeleteMasterSupplier] [bit] NULL,
	[MasterUser] [bit] NULL,
	[InsertMasterUser] [bit] NULL,
	[UpdateMasterUser] [bit] NULL,
	[DeleteMasterUser] [bit] NULL,
	[MasterMekanik] [bit] NULL,
	[InsertMasterMekanik] [bit] NULL,
	[UpdateMasterMekanik] [bit] NULL,
	[DeleteMasterMekanik] [bit] NULL,
	[MasterPegawai] [bit] NULL,
	[InsertMasterPegawai] [bit] NULL,
	[UpdateMasterPegawai] [bit] NULL,
	[DeleteMasterPegawai] [bit] NULL,
	[Kontrak] [bit] NULL,
	[InsertKontrak] [bit] NULL,
	[UpdateKontrak] [bit] NULL,
	[DeleteKontrak] [bit] NULL,
	[SO] [bit] NULL,
	[InsertSO] [bit] NULL,
	[UpdateSO] [bit] NULL,
	[DeleteSO] [bit] NULL,
	[SetHargaSO] [bit] NULL,
	[SetStatusSO] [bit] NULL,
	[ViewHargaSO] [bit] NULL,
	[KeluhanPelanggan] [bit] NULL,
	[InsertKeluhanPelanggan] [bit] NULL,
	[UpdateKeluhanPelanggan] [bit] NULL,
	[DeleteKeluhanPelanggan] [bit] NULL,
	[DaftarPelanggan] [bit] NULL,
	[JadwalArmada] [bit] NULL,
	[PilihArmada] [bit] NULL,
	[UpdatePilihArmada] [bit] NULL,
	[SuratJalan] [bit] NULL,
	[InsertSuratJalan] [bit] NULL,
	[UpdateSuratJalan] [bit] NULL,
	[DeleteSuratJalan] [bit] NULL,
	[RealisasiSuratJalan] [bit] NULL,
	[InsertRealisasiSuratJalan] [bit] NULL,
	[UpdateRealisasiSuratJalan] [bit] NULL,
	[DeleteRealisasiSuratJalan] [bit] NULL,
	[Storing] [bit] NULL,
	[InsertStoring] [bit] NULL,
	[UpdateStoring] [bit] NULL,
	[DeleteStoring] [bit] NULL,
	[DaftarBeli] [bit] NULL,
	[UpdateDaftarBeli] [bit] NULL,
	[PilihSupplierDaftarBeli] [bit] NULL,
	[PO] [bit] NULL,
	[InsertPO] [bit] NULL,
	[UpdatePO] [bit] NULL,
	[DeletePO] [bit] NULL,
	[ApprovalPO] [bit] NULL,
	[CashNCarry] [bit] NULL,
	[InsertCashNCarry] [bit] NULL,
	[UpdateCashNCarry] [bit] NULL,
	[DeleteCashNCarry] [bit] NULL,
	[KomplainNRetur] [bit] NULL,
	[InsertKomplainNRetur] [bit] NULL,
	[UpdateKomplainRetur] [bit] NULL,
	[DeleteKomplainRetur] [bit] NULL,
	[DaftarSupplier] [bit] NULL,
	[BonBarang] [bit] NULL,
	[InsertBonBarang] [bit] NULL,
	[UpdateBonBarang] [bit] NULL,
	[DeleteBonBarang] [bit] NULL,
	[SetBeliBonBarang] [bit] NULL,
	[SetMintaBonBarang] [bit] NULL,
	[TambahStokBonBarang] [bit] NULL,
	[PerbaikanBonBarang] [bit] NULL,
	[PerawatanBonBarang] [bit] NULL,
	[ApprovalBonBarang] [bit] NULL,
	[ProcessBonBarang] [bit] NULL,
	[ApprovalMoneyBonBarang] [money] NULL,
	[SetUrgentBonBarang] [bit] NULL,
	[BonKeluarBarang] [bit] NULL,
	[InsertBonKeluarBarang] [bit] NULL,
	[UpdateBonKeluarBarang] [bit] NULL,
	[DeleteBonKeluarBarang] [bit] NULL,
	[LPB] [bit] NULL,
	[InsertLPB] [bit] NULL,
	[UpdateLPB] [bit] NULL,
	[DeleteLPB] [bit] NULL,
	[Pemutihan] [bit] NULL,
	[InsertPemutihan] [bit] NULL,
	[UpdatePemutihan] [bit] NULL,
	[DeletePemutihan] [bit] NULL,
	[ApprovalPemutihan] [bit] NULL,
	[DaftarMinStok] [bit] NULL,
	[DaftarStokBarang] [bit] NULL,
	[LaporanRiwayatStokBarang] [bit] NULL,
	[LaporanRiwayatHargaBarang] [bit] NULL,
	[NotifikasiPerawatan] [bit] NULL,
	[InsertNotifikasiPerawatan] [bit] NULL,
	[PermintaanPerbaikan] [bit] NULL,
	[InsertPermintaanPerbaikan] [bit] NULL,
	[UpdatePermintaanPerbaikan] [bit] NULL,
	[DeletePermintaanPerbaikan] [bit] NULL,
	[Perbaikan] [bit] NULL,
	[InsertPerbaikan] [bit] NULL,
	[UpdatePerbaikan] [bit] NULL,
	[DeletePerbaikan] [bit] NULL,
	[Perawatan] [bit] NULL,
	[InsertPerawatan] [bit] NULL,
	[UpdatePerawatan] [bit] NULL,
	[DeletePerawatan] [bit] NULL,
	[LepasPasangBan] [bit] NULL,
	[InsertLepasPasangBan] [bit] NULL,
	[UpdateLepasPasangBan] [bit] NULL,
	[DeleteLepasPasangBan] [bit] NULL,
	[PerbaikanBan] [bit] NULL,
	[InsertPerbaikanBan] [bit] NULL,
	[UpdatePerbaikanBan] [bit] NULL,
	[DeletePerbaikanBan] [bit] NULL,
	[TukarKomponen] [bit] NULL,
	[InsertTukarKomponen] [bit] NULL,
	[UpdateTukarKomponen] [bit] NULL,
	[DeleteTukarKomponen] [bit] NULL,
	[KasBon] [bit] NULL,
	[InsertKasBon] [bit] NULL,
	[UpdateKasBon] [bit] NULL,
	[DeleteKasBon] [bit] NULL,
	[Penagihan] [bit] NULL,
	[InsertPenagihan] [bit] NULL,
	[UpdatePenagihan] [bit] NULL,
	[DeletePenagihan] [bit] NULL,
	[BayarPO] [bit] NULL,
	[InsertBayarPO] [bit] NULL,
	[UpdateBayarPO] [bit] NULL,
	[DeleteBayarPO] [bit] NULL,
	[LaporanSalesOrder] [bit] NULL,
	[LaporanRealisasiOrder] [bit] NULL,
	[LaporanOrderTidakSesuai] [bit] NULL,
	[LaporanKeluhanPelanggan] [bit] NULL,
	[LaporanPendapatan] [bit] NULL,
	[LaporanPemenuhanKontrak] [bit] NULL,
	[LaporanEfisiensiMuatan] [bit] NULL,
	[LaporanPendapatanDanPoinSopir] [bit] NULL,
	[LaporanPenggunaanBan] [bit] NULL,
	[LaporanPenggunaanBarang] [bit] NULL,
	[LaporanKegiatanMekanik] [bit] NULL,
	[LaporanKegiatanTeknik] [bit] NULL,
	[LaporanRiwayatKendaraan] [bit] NULL,
	[LaporanPemenuhanBarang] [bit] NULL,
	[LaporanPenerimaanNPembelian] [bit] NULL,
	[LaporanPenambahanOli] [bit] NULL,
	[LaporanRetur] [bit] NULL,
	[LaporanPenjualanUser] [bit] NULL,
	[LaporanProfitPerSO] [bit] NULL,
	[SettingParameterNotifikasi] [bit] NULL,
	[SettingWaktuNotifikasi] [bit] NULL,
	[TampilkanNotifikasi] [bit] NULL,
	[SettingNotifikasi] [bit] NULL,
	[FollowUpCustomer] [bit] NULL,
	[InactiveCustomer] [bit] NULL,
	[PembuatanSuratJalan] [bit] NULL,
	[NewBonBarang] [bit] NULL,
	[NewDaftarBeli] [bit] NULL,
	[NewPermintaanPerbaikan] [bit] NULL,
	[NotifPilihArmada] [bit] NULL,
	[NewLPB] [bit] NULL,
	[MinimumStok] [bit] NULL,
	[STNKExpired] [bit] NULL,
	[STNKPajakExpired] [bit] NULL,
	[KirExpired] [bit] NULL,
	[SimExpired] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pelanggan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pelanggan](
	[Kode] [varchar](10) NOT NULL,
	[NamaPT] [varchar](50) NOT NULL,
	[Alamat] [varchar](100) NULL,
	[Kota] [varchar](50) NULL,
	[NoTelp] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[NoFax] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[NamaPIC1] [varchar](50) NULL,
	[TelpPIC1] [varchar](15) NULL,
	[JabatanPIC1] [varchar](20) NULL,
	[NamaPIC2] [varchar](50) NULL,
	[TelpPIC2] [varchar](15) NULL,
	[JabatanPIC2] [varchar](20) NULL,
	[NamaPIC3] [varchar](50) NULL,
	[TelpPIC3] [varchar](15) NULL,
	[JabatanPIC3] [varchar](20) NULL,
 CONSTRAINT [PK_Pelanggan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rute]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rute](
	[Kode] [varchar](10) NOT NULL,
	[Muat] [varchar](50) NOT NULL,
	[Bongkar] [varchar](50) NOT NULL,
	[Jarak] [int] NULL,
	[Kategori] [varchar](50) NULL,
	[LevelRute] [varchar](50) NULL,
	[Poin] [numeric](18, 0) NULL,
	[PremiPengemudi] [money] NULL,
	[PremiKernet] [money] NULL,
	[PremiKondektur] [money] NULL,
	[Mel] [money] NULL,
	[Tol] [money] NULL,
	[UangJalanBesar] [money] NULL,
	[UangJalanKecil] [money] NULL,
	[UangBBM] [money] NULL,
	[UangMakan] [money] NULL,
	[Waktu] [int] NULL,
	[StandarHargaMax] [money] NULL,
	[StandarHarga] [money] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_Rute] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kontrak](
	[Kode] [varchar](10) NOT NULL,
	[Pelanggan] [varchar](10) NOT NULL,
	[TglMulai] [datetime] NULL,
	[TglSelesai] [datetime] NULL,
	[StatusRute] [varchar](50) NULL,
	[Rute] [varchar](10) NULL,
	[AC] [bit] NULL,
	[Toilet] [bit] NULL,
	[AirSuspension] [bit] NULL,
	[KapasitasSeat] [int] NULL,
	[Harga] [money] NULL,
	[Status] [varchar](50) NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[IntervalPenagihan] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_Kontrak] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterAC]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterAC](
	[Kode] [varchar](10) NOT NULL,
	[Merk] [varchar](50) NULL,
	[Tipe] [varchar](50) NULL,
 CONSTRAINT [PK_MasterAC] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JenisKendaraan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JenisKendaraan](
	[Kode] [varchar](10) NOT NULL,
	[NamaJenis] [varchar](50) NOT NULL,
	[Tipe] [varchar](max) NOT NULL,
	[Kategori] [varchar](50) NULL,
	[Keterangan] [varchar](max) NULL,
 CONSTRAINT [PK_JenisKendaraan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Armada]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Armada](
	[Kode] [varchar](10) NOT NULL,
	[PlatNo] [varchar](10) NOT NULL,
	[JumlahSeat] [int] NOT NULL,
	[TahunPembuatan] [varchar](5) NOT NULL,
	[NoBody] [varchar](50) NULL,
	[JenisAC] [varchar](10) NULL,
	[JenisBBM] [varchar](50) NULL,
	[KapasitasTangkiBBM] [int] NULL,
	[LevelArmada] [varchar](50) NULL,
	[JumlahBan] [int] NOT NULL,
	[Aktif] [bit] NOT NULL,
	[AC] [bit] NOT NULL,
	[Toilet] [bit] NOT NULL,
	[AirSuspension] [bit] NOT NULL,
	[KmSekarang] [int] NULL,
	[Keterangan] [varchar](50) NULL,
	[Sopir] [varchar](10) NULL,
	[JenisKendaraan] [varchar](10) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[STNKPajakExpired] [datetime] NULL,
	[STNKPerpanjangExpired] [datetime] NULL,
	[KirMulai] [datetime] NULL,
	[KirSelesai] [datetime] NULL,
	[NoRangka] [varchar](50) NULL,
	[NoMesin] [varchar](50) NULL,
 CONSTRAINT [PK_Armada] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterSO]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterSO](
	[Kodenota] [varchar](10) NOT NULL,
	[Tgl] [datetime] NOT NULL,
	[Pelanggan] [varchar](10) NOT NULL,
	[Berangkat] [datetime] NULL,
	[Tiba] [datetime] NULL,
	[Harga] [money] NOT NULL,
	[PPN] [money] NULL,
	[PembayaranAwal] [money] NULL,
	[TglPembayaranAwal] [datetime] NULL,
	[CaraPembayaranAwal] [varchar](50) NULL,
	[NoKwitansiPembayaranAwal] [varchar](50) NULL,
	[NominalKwitansiPembayaranAwal] [money] NULL,
	[PenerimaPembayaranAwal] [varchar](10) NULL,
	[Pelunasan] [money] NULL,
	[TglPelunasan] [datetime] NULL,
	[CaraPembayaranPelunasan] [varchar](50) NULL,
	[NoKwitansiPelunasan] [varchar](50) NULL,
	[NominalKwitansiPelunasan] [money] NULL,
	[PenerimaPelunasan] [varchar](10) NULL,
	[Extend] [bit] NULL,
	[TglKembaliExtend] [datetime] NULL,
	[BiayaExtend] [money] NULL,
	[PPNExtend] [money] NULL,
	[KapasitasSeat] [int] NULL,
	[AC] [bit] NULL,
	[Toilet] [bit] NULL,
	[AirSuspension] [bit] NULL,
	[Rute] [varchar](10) NOT NULL,
	[TglFollowUp] [datetime] NULL,
	[Armada] [varchar](10) NULL,
	[Kontrak] [varchar](10) NULL,
	[PICJemput] [varchar](max) NULL,
	[JamJemput] [datetime] NULL,
	[NoTelpPICJemput] [varchar](50) NULL,
	[AlamatJemput] [varchar](max) NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusPembayaran] [varchar](50) NULL,
	[ReminderPending] [datetime] NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_MasterSO] PRIMARY KEY CLUSTERED 
(
	[Kodenota] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Jarak]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jarak](
	[kode] [varchar](10) NOT NULL,
	[Dari] [varchar](max) NOT NULL,
	[Ke] [varchar](max) NOT NULL,
	[JumlahKm] [int] NOT NULL,
 CONSTRAINT [PK_Jarak] PRIMARY KEY CLUSTERED 
(
	[kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterSJ]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterSJ](
	[Kodenota] [varchar](10) NOT NULL,
	[Tgl] [datetime] NOT NULL,
	[NoSO] [varchar](10) NOT NULL,
	[Sopir] [varchar](10) NULL,
	[Sopir2] [varchar](10) NULL,
	[Crew] [varchar](50) NULL,
	[TitipKwitansi] [bit] NULL,
	[NominalKwitansi] [money] NULL,
	[NoKwitansi] [varchar](50) NULL,
	[Keterangan] [varchar](100) NULL,
	[Kir] [bit] NOT NULL,
	[STNK] [bit] NOT NULL,
	[Pajak] [bit] NOT NULL,
	[TglKembali] [datetime] NULL,
	[Pendapatan] [money] NULL,
	[Pengeluaran] [money] NULL,
	[SisaDisetor] [money] NULL,
	[SPBUAYaniLiter] [numeric](18, 4) NULL,
	[SPBUAYaniUang] [money] NULL,
	[SPBUAYaniJam] [money] NULL,
	[SPBULuarLiter] [numeric](18, 4) NULL,
	[SPBULuarUang] [money] NULL,
	[SPBULuarUangDiberi] [money] NULL,
	[SPBULuarDetail] [varchar](max) NULL,
	[Status] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[Laporan] [varchar](max) NULL,
	[TglRealisasi] [datetime] NULL,
	[Awal] [varchar](10) NULL,
	[Akhir] [varchar](10) NULL,
	[TglCetak] [datetime] NULL,
	[ClaimSopir] [money] NULL,
	[KeteranganClaimSopir] [varchar](50) NULL,
	[PremiSopir] [money] NULL,
	[PremiSopir2] [money] NULL,
	[PremiKernet] [money] NULL,
	[TabunganSopir] [money] NULL,
	[TabunganSopir2] [money] NULL,
	[Tol] [money] NULL,
	[UangJalan] [money] NULL,
	[BiayaLainLain] [money] NULL,
	[KeteranganBiayaLainLain] [money] NULL,
	[UangMakan] [money] NULL,
	[Other] [money] NULL,
 CONSTRAINT [PK_MasterSJ] PRIMARY KEY CLUSTERED 
(
	[Kodenota] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Storing]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Storing](
	[Kode] [varchar](10) NOT NULL,
	[SuratJalan] [varchar](10) NULL,
	[Armada] [varchar](10) NULL,
	[Pengemudi] [varchar](10) NULL,
	[KategoriRute] [varchar](50) NULL,
	[Biaya] [money] NOT NULL,
	[PICJemput] [varchar](10) NOT NULL,
	[JenisStoring] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TindakanPerbaikan] [varchar](max) NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_Storing] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PermintaanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PermintaanPerbaikan](
	[Kode] [varchar](10) NOT NULL,
	[Tanggal] [datetime] NULL,
	[Armada] [varchar](10) NULL,
	[Peminta] [varchar](10) NOT NULL,
	[Keluhan] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_PermintaanPerbaikan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LaporanPerbaikan](
	[Kode] [varchar](10) NOT NULL,
	[PP] [varchar](10) NOT NULL,
	[Tanggal] [datetime] NULL,
	[AnalisaMasalah] [varchar](max) NOT NULL,
	[TindakanPerbaikan] [varchar](max) NOT NULL,
	[WaktuMulai] [datetime] NULL,
	[WaktuSelesai] [datetime] NULL,
	[Verifikator] [varchar](10) NULL,
	[TglSerahTerima] [datetime] NULL,
	[PICSerahTerima] [varchar](10) NULL,
	[KmArmadaSekarang] [int] NULL,
	[Status] [varchar](50) NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[ClaimSopir] [money] NULL,
	[SopirClaim] [varchar](10) NULL,
	[KeteranganClaimSopir] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_LaporanPerbaikan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier](
	[Kode] [varchar](10) NOT NULL,
	[NamaToko] [varchar](50) NOT NULL,
	[Alamat] [varchar](200) NOT NULL,
	[NoTelp] [varchar](50) NOT NULL,
	[Fax] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Kategori] [varchar](50) NULL,
	[StandarTermOfPayment] [varchar](50) NULL,
	[CashNCarry] [bit] NULL,
	[NPWP] [varchar](50) NULL,
	[NamaPIC1] [varchar](50) NOT NULL,
	[TelpPIC1] [varchar](15) NOT NULL,
	[JabatanPIC1] [varchar](20) NOT NULL,
	[NamaPIC2] [varchar](50) NULL,
	[TelpPIC2] [varchar](15) NULL,
	[JabatanPIC2] [varchar](20) NULL,
	[NamaPIC3] [varchar](50) NULL,
	[TelpPIC3] [varchar](15) NULL,
	[JabatanPIC3] [varchar](20) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KategoriBarang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KategoriBarang](
	[Kode] [varchar](10) NOT NULL,
	[Kategori] [varchar](50) NOT NULL,
	[MinStok] [int] NULL,
	[MaxStok] [int] NULL,
 CONSTRAINT [PK_KategoriBarang] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Barang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Barang](
	[Kode] [varchar](10) NOT NULL,
	[Nama] [varchar](50) NOT NULL,
	[Jumlah] [int] NOT NULL,
	[Satuan] [varchar](10) NOT NULL,
	[MinimumStok] [int] NULL,
	[MaximumStok] [int] NULL,
	[StandardUmur] [int] NULL,
	[Lokasi] [varchar](50) NOT NULL,
	[ClaimNWarranty] [bit] NULL,
	[DurasiClaimNWarranty] [int] NULL,
	[Kategori] [varchar](10) NULL,
	[SingleSupplier] [bit] NULL,
	[Harga] [money] NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_Barang] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rebuild]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rebuild](
	[Kode] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NULL,
	[DariArmada] [varchar](10) NULL,
	[TanggalMasuk] [datetime] NULL,
	[KeArmada] [varchar](10) NULL,
	[TanggalKeluar] [datetime] NULL,
	[AnalisaMasalah] [varchar](max) NULL,
	[JasaLuar] [bit] NULL,
	[Supplier] [varchar](10) NULL,
	[Harga] [money] NULL,
	[TanggalKirim] [datetime] NULL,
	[PICKirim] [varchar](10) NULL,
	[TanggalKembali] [datetime] NULL,
	[Penerima] [varchar](10) NULL,
	[PerbaikanInternal] [bit] NULL,
	[Verifikator] [varchar](10) NULL,
	[TglVerifikasi] [datetime] NULL,
	[Kanibal] [bit] NULL,
	[PersentaseCosting] [numeric](18, 4) NULL,
	[Status] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[TglEntry] [datetime] NULL,
	[Operator] [varchar](10) NULL,
	[CreateBy] [varchar](10) NULL,
 CONSTRAINT [PK_Rebuild] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LaporanPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LaporanPerawatan](
	[Kode] [varchar](10) NOT NULL,
	[Tanggal] [datetime] NULL,
	[WaktuMulai] [datetime] NULL,
	[WaktuSelesai] [datetime] NULL,
	[Verifikator] [varchar](10) NULL,
	[ButuhPerbaikan] [bit] NULL,
	[KmArmadaSekarang] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[Armada] [varchar](10) NULL,
 CONSTRAINT [PK_LaporanPerawatan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuratPerintahKerja]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuratPerintahKerja](
	[Kode] [varchar](10) NOT NULL,
	[Tanggal] [datetime] NULL,
	[LaporanPerbaikan] [varchar](10) NULL,
	[LaporanPerawatan] [varchar](10) NULL,
	[Rebuild] [varchar](10) NULL,
	[Mekanik] [varchar](10) NULL,
	[DetailTindakan] [varchar](max) NULL,
	[WaktuMulai] [datetime] NULL,
	[WaktuSelesai] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[Keterangan] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_SuratPerintahKerja] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BonBarang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonBarang](
	[Kode] [varchar](10) NOT NULL,
	[Peminta] [varchar](10) NOT NULL,
	[Penyetuju] [varchar](10) NOT NULL,
	[Penerima] [varchar](10) NULL,
	[LaporanPerbaikan] [varchar](10) NULL,
	[LaporanPerawatan] [varchar](10) NULL,
	[Storing] [varchar](10) NULL,
	[SPK] [varchar](10) NULL,
	[Status] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[Armada] [varchar](10) NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_BonBarang] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BonKeluarBarang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonKeluarBarang](
	[Kode] [varchar](10) NOT NULL,
	[BonBarang] [varchar](10) NOT NULL,
	[Penerima] [varchar](10) NOT NULL,
	[TglKeluar] [datetime] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[Operator] [varchar](10) NULL,
 CONSTRAINT [PK_BonKeluarBarang_1] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [BONKELUARBARANG_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BONKELUARBARANG_UPDATE] ON [dbo].[BonKeluarBarang]
for UPDATE
AS
BEGIN
	update BONKELUARBARANG set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BONKELUARBARANG_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BONKELUARBARANG_INSERT] ON [dbo].[BonKeluarBarang]
for INSERT
AS
BEGIN
	update BONKELUARBARANG set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BON_BARANG_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BON_BARANG_UPDATE] ON [dbo].[BonBarang]
for UPDATE
AS
BEGIN
	update BONBARANG set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BON_BARANG_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BON_BARANG_INSERT] ON [dbo].[BonBarang]
for INSERT
AS
BEGIN
	update BONBARANG set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[DaftarBeli]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DaftarBeli](
	[Kode] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[BonBarang] [varchar](10) NOT NULL,
	[HargaMin] [money] NULL,
	[HargaMax] [money] NULL,
	[HargaLast] [money] NULL,
	[LastSupplier] [varchar](10) NULL,
	[JumlahBeli] [int] NOT NULL,
	[HargaSatuan] [money] NULL,
	[Supplier] [varchar](10) NULL,
	[CashNCarry] [bit] NULL,
	[GrandTotal] [money] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Supplier1] [varchar](10) NULL,
	[Supplier2] [varchar](10) NULL,
	[Supplier3] [varchar](10) NULL,
	[Harga1] [money] NULL,
	[Harga2] [money] NULL,
	[Harga3] [money] NULL,
	[Termin1] [int] NULL,
	[Termin2] [int] NULL,
	[Termin3] [int] NULL,
	[Keterangan1] [varchar](max) NULL,
	[Keterangan2] [varchar](max) NULL,
	[Keterangan3] [varchar](max) NULL,
	[Termin] [int] NULL,
 CONSTRAINT [PK_DaftarBeli] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailBonBarang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailBonBarang](
	[KodeBonBarang] [varchar](10) NOT NULL,
	[KodeBarang] [varchar](10) NOT NULL,
	[JumlahDiminta] [int] NOT NULL,
	[JumlahBeli] [int] NULL,
	[Keterangan] [varchar](max) NULL,
	[StatusMinta] [varchar](50) NULL,
	[StatusBeli] [varchar](50) NULL,
	[Urgent] [bit] NOT NULL,
 CONSTRAINT [PK_DetailBonBarang] PRIMARY KEY CLUSTERED 
(
	[KodeBonBarang] ASC,
	[KodeBarang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [update_DetailBonBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[update_DetailBonBarang]
on [dbo].[DetailBonBarang]
for UPDATE 
as
declare @JumlahBeliLama int;
declare @JumlahBeliBaru int;
declare @KodeBonBarang varchar(10);
declare @KodeBarang varchar(10);
set @KodeBonBarang=(select i.KodeBonBarang from inserted i);
set @KodeBarang=(select i.KodeBarang from inserted i);
set @JumlahBeliBaru=(select i.JumlahBeli from inserted i);
update DaftarBeli set JumlahBeli=@JumlahBeliBaru where BonBarang=@KodeBonBarang and
Barang=@KodeBarang;
GO
/****** Object:  Trigger [trig_DaftarBeli]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[trig_DaftarBeli]  
on [dbo].[BonBarang]  
for UPDATE   
as  
declare @KodeBarang varchar(10);  
declare @kode integer;  
declare @kode2 integer;  
Declare @kodex integer;  
Declare @kodeakhir varchar(10);  
Declare @BonBarang varchar(10);  
Declare @kodes varchar(10);
Declare @JumBeli integer;  
Declare @StatusBaru varchar(50);  
Declare @StatusLama varchar(50); 
declare @looping int;
declare @downto int;
declare @stringPenambah varchar(9);
declare @counter int; 
DECLARE MyCursor CURSOR for  (select KodeBarang from DetailBonBarang, inserted i where KodeBonBarang=i.Kode)  
open MyCursor  
Fetch Next from MyCursor  
into @KodeBarang 
while @@FETCH_STATUS=0  
begin  
set @counter=0;
set @stringPenambah='0';
select @kodex=(select top 1 cast ( kode as INT) from DaftarBeli order by kode desc)  
if @kodex is NULL begin set @kodex=0 
end   
set @kode2=@kodex+1;   
set @kodes=(select CAST (@kode2 as varchar(10)));
set @looping=LEN(@kodes);
set @downto=10-@looping;
while (@counter<@downto-1)
begin
set @stringPenambah='0'+@stringPenambah;
set @counter=@counter+1;
end
set @kodeakhir=@stringPenambah + @kodes;  
select @BonBarang=(select i.Kode from inserted i);  
select @JumBeli=(select dbb.JumlahBeli from DetailBonBarang dbb, inserted i where  dbb.KodeBonBarang=@BonBarang and dbb.KodeBarang=@KodeBarang);  
select @StatusBaru=(select i.Status from inserted i);  
select @StatusLama=(select d.Status from deleted d);
if @StatusBaru='ON PROCESS' and @StatusLama<>'ON PROCESS' 
begin  
insert into DaftarBeli (Kode,Barang,BonBarang,JumlahBeli,GrandTotal,Status) values (@kodeakhir,@KodeBarang,@BonBarang,@JumBeli,0,'ON PROCESS') 
 end  
 Fetch Next from MyCursor  
 into @KodeBarang  
 end  
 Close MyCursor  
 Deallocate MyCursor
GO
/****** Object:  Table [dbo].[Notifikasi]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notifikasi](
	[Kode] [varchar](10) NOT NULL,
	[WaktuPengecekan] [int] NULL,
	[InactiveCustomer] [int] NULL,
	[PilihKendaraan] [int] NULL,
	[SuratJalan] [int] NULL,
	[Kontrak] [int] NULL,
 CONSTRAINT [PK_Notifikasi] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StandarPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StandarPerawatan](
	[Kode] [varchar](10) NOT NULL,
	[JenisKendaraan] [varchar](10) NOT NULL,
	[TipePerawatan] [varchar](50) NOT NULL,
	[PeriodeWaktu] [int] NULL,
	[PeriodeKm] [int] NULL,
	[StandardWaktu] [int] NULL,
 CONSTRAINT [PK_StandarPerawatan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailBKB]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailBKB](
	[Kode] [varchar](10) NOT NULL,
	[KodeBKB] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[JumlahKeluar] [int] NOT NULL,
	[Keterangan] [varchar](max) NULL,
 CONSTRAINT [PK_DetailBKB] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [Update_KeluarBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[Update_KeluarBarang]
on [dbo].[DetailBKB]
for UPDATE
as
declare @Barang varchar(10);
declare @JumlahKeluar int;
declare @JumlahKeluarLama int;
declare @Stok int;
declare @JumlahBaru int;
declare @BonBarang varchar(10);
declare @JumlahDiminta int;
declare @JumlahKeluarTotal int;
set @Barang=(select i.Barang from inserted i);
set @JumlahKeluar= (select i.JumlahKeluar from inserted i);
set @JumlahKeluarLama=(select d.JumlahKeluar from deleted d);
set @Stok=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahBaru=@Stok+@JumlahKeluarLama-@JumlahKeluar;
update Barang set Jumlah=@JumlahBaru where Kode=@Barang;
set @BonBarang=(select bkb.BonBarang from BonKeluarBarang bkb, inserted i where
i.KodeBKB=bkb.Kode);
set @JumlahDiminta=(select dbb.JumlahDiminta from DetailBonBarang dbb where
dbb.KodeBonBarang=@BonBarang and dbb.KodeBarang=@Barang);
set @JumlahKeluarTotal=(select SUM(JumlahKeluar) from DetailBKB dbkb, 
BonKeluarBarang bkb, DetailBonBarang dbb, BonBarang bb
where dbkb.KodeBKB=bkb.Kode and
bb.Kode=bkb.BonBarang and dbb.KodeBonBarang=bb.Kode
and dbb.KodeBarang=dbkb.Barang and dbkb.Barang=@Barang and bkb.BonBarang=@BonBarang)
if @JumlahDiminta<=@JumlahKeluarTotal
begin
update DetailBonBarang set StatusMinta='FINISHED' where KodeBonBarang=@BonBarang
and KodeBarang=@Barang
end
if @JumlahDiminta>@JumlahKeluarTotal
begin
update DetailBonBarang set StatusMinta='ON PROCESS' where KodeBonBarang=@BonBarang
and KodeBarang=@Barang
end
GO
/****** Object:  Trigger [Insert_KeluarBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[Insert_KeluarBarang]
on [dbo].[DetailBKB]
for insert 
as
declare @Barang varchar(10);
declare @JumlahKeluar int;
declare @Stok int;
declare @JumlahBaru int;
declare @BonBarang varchar(10);
declare @JumlahDiminta int;
declare @JumlahKeluarTotal int;
set @Barang=(select i.Barang from inserted i);
set @JumlahKeluar= (select i.JumlahKeluar from inserted i);
set @Stok=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahBaru=@Stok-@JumlahKeluar;
update Barang set Jumlah=@JumlahBaru where Kode=@Barang;
set @BonBarang=(select bkb.BonBarang from BonKeluarBarang bkb, inserted i where
i.KodeBKB=bkb.Kode);
set @JumlahDiminta=(select dbb.JumlahDiminta from DetailBonBarang dbb where
dbb.KodeBonBarang=@BonBarang and dbb.KodeBarang=@Barang);
set @JumlahKeluarTotal=(select SUM(JumlahKeluar) from DetailBKB dbkb, 
BonKeluarBarang bkb, DetailBonBarang dbb, BonBarang bb
where dbkb.KodeBKB=bkb.Kode and
bb.Kode=bkb.BonBarang and dbb.KodeBonBarang=bb.Kode
and dbb.KodeBarang=dbkb.Barang and dbkb.Barang=@Barang and bkb.BonBarang=@BonBarang)
if @JumlahDiminta<=@JumlahKeluarTotal
begin
update DetailBonBarang set StatusMinta='FINISHED' where KodeBonBarang=@BonBarang
and KodeBarang=@Barang
end
GO
/****** Object:  Trigger [Delete_KeluarBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[Delete_KeluarBarang]
on [dbo].[DetailBKB]
for DELETE
as
declare @Barang varchar(10);
declare @JumlahKeluarLama int;
declare @Stok int;
declare @JumlahBaru int;
declare @BonBarang varchar(10);
declare @JumlahDiminta int;
declare @JumlahKeluarTotal int;
set @Barang=(select d.Barang from deleted d);
set @JumlahKeluarLama=(select d.JumlahKeluar from deleted d);
set @Stok=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahBaru=@Stok+@JumlahKeluarLama;
update Barang set Jumlah=@JumlahBaru where Kode=@Barang;
set @BonBarang=(select bkb.BonBarang from BonKeluarBarang bkb, deleted d where
d.KodeBKB=bkb.Kode);
set @JumlahDiminta=(select dbb.JumlahDiminta from DetailBonBarang dbb where
dbb.KodeBonBarang=@BonBarang and dbb.KodeBarang=@Barang);
set @JumlahKeluarTotal=(select SUM(JumlahKeluar) from DetailBKB dbkb, 
BonKeluarBarang bkb, DetailBonBarang dbb, BonBarang bb
where dbkb.KodeBKB=bkb.Kode and
bb.Kode=bkb.BonBarang and dbb.KodeBonBarang=bb.Kode
and dbb.KodeBarang=dbkb.Barang and dbkb.Barang=@Barang and bkb.BonBarang=@BonBarang)
if @JumlahKeluarTotal is NULL
begin
set @JumlahKeluarTotal=0;
end
if @JumlahDiminta<=@JumlahKeluarTotal
begin
update DetailBonBarang set StatusMinta='FINISHED' where KodeBonBarang=@BonBarang
and KodeBarang=@Barang
end
if @JumlahDiminta>@JumlahKeluarTotal
begin
update DetailBonBarang set StatusMinta='ON PROCESS' where KodeBonBarang=@BonBarang
and KodeBarang=@Barang
end
GO
/****** Object:  Table [dbo].[JenisPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JenisPerbaikan](
	[Kode] [varchar](10) NOT NULL,
	[Nama] [varchar](50) NOT NULL,
	[LamaPengerjaan] [int] NULL,
	[LamaGaransi] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_JenisPerbaikan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CashNCarry]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CashNCarry](
	[Kode] [varchar](10) NOT NULL,
	[KasBon] [money] NOT NULL,
	[Penyetuju] [varchar](10) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_CashNCarry] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailCashNCarry]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailCashNCarry](
	[KodeCNC] [varchar](10) NOT NULL,
	[KodeDaftarBeli] [varchar](10) NOT NULL,
	[RealisasiHarga] [money] NULL,
	[TglBeli] [datetime] NULL,
	[RealisasiJumlah] [numeric](18, 0) NULL,
	[StatusCNC] [varchar](50) NULL,
 CONSTRAINT [PK_DetailCashNCarry] PRIMARY KEY CLUSTERED 
(
	[KodeCNC] ASC,
	[KodeDaftarBeli] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [SUPPLIER_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[SUPPLIER_UPDATE] ON [dbo].[Supplier]
for UPDATE
AS
BEGIN
	update SUPPLIER set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [SUPPLIER_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[SUPPLIER_INSERT] ON [dbo].[Supplier]
for INSERT
AS
BEGIN
	update SUPPLIER set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[DetailBarang]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailBarang](
	[KodeBarang] [varchar](10) NOT NULL,
	[KodeJenisKendaraan] [varchar](10) NOT NULL,
 CONSTRAINT [PK_DetailBarang] PRIMARY KEY CLUSTERED 
(
	[KodeBarang] ASC,
	[KodeJenisKendaraan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [CASHNCARRY_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[CASHNCARRY_UPDATE] ON [dbo].[CashNCarry]
for UPDATE
AS
BEGIN
	update CASHNCARRY set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [CASHNCARRY_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[CASHNCARRY_INSERT] ON [dbo].[CashNCarry]
for INSERT
AS
BEGIN
	update CASHNCARRY set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [JENISPERBAIKAN_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[JENISPERBAIKAN_UPDATE] ON [dbo].[JenisPerbaikan]
for UPDATE
AS
BEGIN
	update JENISPERBAIKAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [JENISPERBAIKAN_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[JENISPERBAIKAN_INSERT] ON [dbo].[JenisPerbaikan]
for INSERT
AS
BEGIN
	update JENISPERBAIKAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[BarangKanibal]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BarangKanibal](
	[Kode] [varchar](10) NOT NULL,
	[IDBarang] [varchar](50) NULL,
	[Barang] [varchar](10) NULL,
	[TglMasuk] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[TglEntry] [datetime] NULL,
	[Operator] [varchar](10) NULL,
	[CreateBy] [varchar](10) NULL,
 CONSTRAINT [PK_BarangKanibal] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [BARANG_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BARANG_UPDATE] ON [dbo].[Barang]
for UPDATE
AS
BEGIN
	update BARANG set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BARANG_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BARANG_INSERT] ON [dbo].[Barang]
for INSERT
AS
BEGIN
	update BARANG set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [ARMADA_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[ARMADA_UPDATE] ON [dbo].[Armada]
for UPDATE
AS
BEGIN
	update armada set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [ARMADA_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[ARMADA_INSERT] ON [dbo].[Armada]
for INSERT
AS
BEGIN
	update armada set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[Ban]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ban](
	[Kode] [varchar](10) NOT NULL,
	[KodeBarang] [varchar](10) NOT NULL,
	[KodeIDBan] [varchar](50) NULL,
	[Merk] [varchar](50) NOT NULL,
	[Jenis] [varchar](50) NOT NULL,
	[Ring] [varchar](50) NULL,
	[Velg] [varchar](50) NULL,
	[StandardUmur] [int] NULL,
	[StandardKm] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Ban] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ListSupplier]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListSupplier](
	[KodeBarang] [varchar](10) NOT NULL,
	[KodeSupplier] [varchar](10) NOT NULL,
 CONSTRAINT [PK_ListSupplier] PRIMARY KEY CLUSTERED 
(
	[KodeBarang] ASC,
	[KodeSupplier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pemutihan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pemutihan](
	[Kode] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[Saldo] [int] NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[Operator] [varchar](10) NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_Pemutihan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [PELANGGAN_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PELANGGAN_UPDATE] ON [dbo].[Pelanggan]
for UPDATE
AS
BEGIN
	update PELANGGAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PELANGGAN_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PELANGGAN_INSERT] ON [dbo].[Pelanggan]
for INSERT
AS
BEGIN
	update PELANGGAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[PO]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PO](
	[Kode] [varchar](10) NOT NULL,
	[Supplier] [varchar](10) NOT NULL,
	[Termin] [varchar](50) NOT NULL,
	[TglKirim] [datetime] NOT NULL,
	[GrandTotal] [money] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[StatusKirim] [varchar](50) NULL,
	[Diskon] [money] NULL,
	[Kirim] [bit] NULL,
	[Ambil] [bit] NULL,
 CONSTRAINT [PK_PO] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPO]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPO](
	[KodePO] [varchar](10) NOT NULL,
	[KodeDaftarBeli] [varchar](10) NOT NULL,
	[StatusPO] [varchar](50) NULL,
 CONSTRAINT [PK_DetailPO] PRIMARY KEY CLUSTERED 
(
	[KodePO] ASC,
	[KodeDaftarBeli] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [RUTE_UPDATE]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[RUTE_UPDATE] ON [dbo].[Rute]
for UPDATE
AS
BEGIN
	update RUTE set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [RUTE_INSERT]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[RUTE_INSERT] ON [dbo].[Rute]
for INSERT
AS
BEGIN
	update RUTE set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[RealisasiTrayek]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RealisasiTrayek](
	[Kode] [varchar](10) NOT NULL,
	[Kontrak] [varchar](10) NULL,
	[Tanggal] [datetime] NULL,
	[Armada] [varchar](10) NULL,
	[Pengemudi] [varchar](10) NULL,
	[Kondektur] [varchar](10) NULL,
	[Crew] [varchar](50) NULL,
	[KilometerAwal] [int] NULL,
	[KilometerAkhir] [int] NULL,
	[Rit1] [money] NULL,
	[Rit2] [money] NULL,
	[Rit3] [money] NULL,
	[Rit4] [money] NULL,
	[Pendapatan] [money] NULL,
	[Pengeluaran] [money] NULL,
	[SisaDisetor] [money] NULL,
	[SPBUAYaniLiter] [numeric](18, 4) NULL,
	[SPBUAYaniUang] [money] NULL,
	[SPBUAYaniJam] [datetime] NULL,
	[SPBULuarLiter] [numeric](18, 4) NULL,
	[SPBULuarUang] [money] NULL,
	[SPBULuarUangDiberi] [money] NULL,
	[SPBULuarDetail] [varchar](max) NULL,
	[UangMakan] [money] NULL,
	[PremiSopir] [money] NULL,
	[PremiKondektur] [money] NULL,
	[PremiKernet] [money] NULL,
	[TabunganSopir] [money] NULL,
	[TabunganKondektur] [money] NULL,
	[RetribusiTerminal] [money] NULL,
	[Parkir] [money] NULL,
	[Tol] [money] NULL,
	[BiayaLainLain] [money] NULL,
	[KeteranganBiayaLainLain] [varchar](max) NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_RealisasiTrayek] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [PO_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PO_UPDATE] ON [dbo].[PO]
for UPDATE
AS
BEGIN
	update PO set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PO_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PO_INSERT] ON [dbo].[PO]
for INSERT
AS
BEGIN
	update PO set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PEMUTIHAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PEMUTIHAN_UPDATE] ON [dbo].[Pemutihan]
for UPDATE
AS
BEGIN
	update PEMUTIHAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PEMUTIHAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PEMUTIHAN_INSERT] ON [dbo].[Pemutihan]
for INSERT
AS
BEGIN
	update PEMUTIHAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[Retur]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Retur](
	[Kode] [varchar](10) NOT NULL,
	[TglRetur] [datetime] NOT NULL,
	[Pelaksana] [varchar](10) NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[KodePO] [varchar](10) NULL,
	[Status] [varchar](10) NULL,
	[Retur] [bit] NULL,
	[Complaint] [bit] NULL,
 CONSTRAINT [PK_Retur] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [REBUILD_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REBUILD_UPDATE] ON [dbo].[Rebuild]
for UPDATE
AS
BEGIN
	update Rebuild set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [REBUILD_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REBUILD_INSERT] ON [dbo].[Rebuild]
for INSERT
AS
BEGIN
	update Rebuild set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PERMINTAANPERBAIKAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PERMINTAANPERBAIKAN_UPDATE] ON [dbo].[PermintaanPerbaikan]
for UPDATE
AS
BEGIN
	update PERMINTAANPERBAIKAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PERMINTAANPERBAIKAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PERMINTAANPERBAIKAN_INSERT] ON [dbo].[PermintaanPerbaikan]
for INSERT
AS
BEGIN
	update PERMINTAANPERBAIKAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[PerbaikanBan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PerbaikanBan](
	[Kode] [varchar](10) NOT NULL,
	[Ban] [varchar](10) NOT NULL,
	[PIC] [varchar](10) NOT NULL,
	[Tindakan] [varchar](max) NOT NULL,
	[Pemeriksa] [varchar](10) NULL,
	[TglPeriksa] [datetime] NULL,
	[Status] [varchar](50) NOT NULL,
	[CreateBy] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[StatusBan] [varchar](50) NOT NULL,
	[Keluhan] [varchar](max) NULL,
 CONSTRAINT [PK_PerbaikanBan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PenagihanAnjem]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PenagihanAnjem](
	[Kode] [varchar](10) NOT NULL,
	[Kontrak] [varchar](10) NOT NULL,
	[BiayaLain] [money] NULL,
	[KetBiayaLain] [varchar](50) NULL,
	[Claim] [money] NULL,
	[ketClaim] [varchar](50) NULL,
	[Terbayar] [money] NULL,
	[TglDibayar] [datetime] NULL,
	[CaraPembayaran] [varchar](50) NULL,
	[NoKwitansi] [varchar](50) NULL,
	[NominalKwitansiPenagihan] [money] NULL,
	[PenerimaPembayaran] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_PenagihanAnjem] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC,
	[Kontrak] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PasangBan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PasangBan](
	[Kode] [varchar](10) NOT NULL,
	[Armada] [varchar](10) NULL,
	[Peminta] [varchar](10) NOT NULL,
	[Pelaksana] [varchar](10) NULL,
	[Ban] [varchar](10) NOT NULL,
	[LokasiPasang] [varchar](50) NOT NULL,
	[TglPasang] [datetime] NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[StatusBan] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PasangBan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [LAPORANPERAWATAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LAPORANPERAWATAN_UPDATE] ON [dbo].[LaporanPerawatan]
for UPDATE
AS
BEGIN
	update LAPORANPERAWATAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [LAPORANPERAWATAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LAPORANPERAWATAN_INSERT] ON [dbo].[LaporanPerawatan]
for INSERT
AS
BEGIN
	update LAPORANPERAWATAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [KONTRAK_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[KONTRAK_UPDATE] ON [dbo].[Kontrak]
for UPDATE
AS
BEGIN
	update KONTRAK set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [KONTRAK_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[KONTRAK_INSERT] ON [dbo].[Kontrak]
for INSERT
AS
BEGIN
	update KONTRAK set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[KasBonAnjem]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KasBonAnjem](
	[Kode] [varchar](10) NOT NULL,
	[Kontrak] [varchar](10) NOT NULL,
	[Sopir] [varchar](10) NOT NULL,
	[Nominal] [money] NOT NULL,
	[Status] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_KasBonAnjem] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC,
	[Kontrak] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LepasBan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LepasBan](
	[Kode] [varchar](10) NOT NULL,
	[Armada] [varchar](10) NULL,
	[Ban] [varchar](10) NOT NULL,
	[Peminta] [varchar](10) NOT NULL,
	[Pelaksana] [varchar](10) NULL,
	[LokasiLepas] [varchar](50) NOT NULL,
	[TglLepas] [datetime] NOT NULL,
	[Permasalahan] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[StatusBan] [varchar](50) NOT NULL,
 CONSTRAINT [PK_LepasBan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [BAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BAN_UPDATE] ON [dbo].[Ban]
for UPDATE
AS
BEGIN
	update ban set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BAN_INSERT] ON [dbo].[Ban]
for INSERT
AS
BEGIN
	update BAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BARANGKANIBAL_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BARANGKANIBAL_UPDATE] ON [dbo].[BarangKanibal]
for UPDATE
AS
BEGIN
	update BarangKanibal set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BARANGKANIBAL_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BARANGKANIBAL_INSERT] ON [dbo].[BarangKanibal]
for INSERT
AS
BEGIN
	update BarangKanibal set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[ArmadaKontrak]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ArmadaKontrak](
	[Kontrak] [varchar](10) NOT NULL,
	[PlatNo] [varchar](50) NOT NULL,
	[TglExpired] [datetime] NULL,
 CONSTRAINT [PK_ArmadaKontrak_1] PRIMARY KEY CLUSTERED 
(
	[Kontrak] ASC,
	[PlatNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailStandard]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailStandard](
	[KodeStandard] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[JumlahDibutuhkan] [int] NOT NULL,
 CONSTRAINT [PK_DetailStandard_1] PRIMARY KEY CLUSTERED 
(
	[KodeStandard] ASC,
	[Barang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPerawatanMekanik]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPerawatanMekanik](
	[KodePerawatan] [varchar](10) NOT NULL,
	[Mekanik] [varchar](10) NOT NULL,
 CONSTRAINT [PK_DetailPerawatanMekanik_1] PRIMARY KEY CLUSTERED 
(
	[KodePerawatan] ASC,
	[Mekanik] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPerawatanJenis]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPerawatanJenis](
	[KodePerawatan] [varchar](10) NOT NULL,
	[JenisPerawatan] [varchar](10) NOT NULL,
 CONSTRAINT [PK_DetailPerawatanJenis] PRIMARY KEY CLUSTERED 
(
	[KodePerawatan] ASC,
	[JenisPerawatan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [insert_Pemutihan]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[insert_Pemutihan]
on [dbo].[Pemutihan]
for INSERT
as
declare @Barang varchar(10);
declare @Saldo int;
declare @Stok int;
declare @JumlahAktual int;
set @Barang=(select i.Barang from inserted i);
set @Saldo=(select i.Saldo from inserted i);
set @Stok=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahAktual=@Stok+@Saldo;
update Barang set Jumlah=@JumlahAktual where Kode=@Barang;
GO
/****** Object:  Table [dbo].[TukarKomponen]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TukarKomponen](
	[Kode] [varchar](10) NOT NULL,
	[DariArmada] [varchar](10) NOT NULL,
	[KeArmada] [varchar](10) NOT NULL,
	[BarangDari] [varchar](10) NOT NULL,
	[BarangKe] [varchar](10) NULL,
	[TglTukar] [datetime] NOT NULL,
	[Peminta] [varchar](10) NOT NULL,
	[LaporanPerbaikan] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_TukarKomponen] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPerbaikanMekanik]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPerbaikanMekanik](
	[KodePerbaikan] [varchar](10) NOT NULL,
	[Mekanik] [varchar](10) NOT NULL,
 CONSTRAINT [PK_DetailPerbaikanMekanik_1] PRIMARY KEY CLUSTERED 
(
	[KodePerbaikan] ASC,
	[Mekanik] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPerbaikanJenis]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPerbaikanJenis](
	[KodePerbaikan] [varchar](10) NOT NULL,
	[JenisPerbaikan] [varchar](10) NOT NULL,
 CONSTRAINT [PK_DetailPerbaikanJenis] PRIMARY KEY CLUSTERED 
(
	[KodePerbaikan] ASC,
	[JenisPerbaikan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailRetur]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailRetur](
	[KodeRetur] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[Jumlah] [int] NOT NULL,
	[Alasan] [varchar](max) NOT NULL,
	[TglTargetTukar] [datetime] NULL,
	[PotongPO] [bit] NULL,
	[StatusRetur] [varchar](50) NULL,
	[PesanUlang] [bit] NULL,
 CONSTRAINT [PK_DetailRetur] PRIMARY KEY CLUSTERED 
(
	[KodeRetur] ASC,
	[Barang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AmbilBarangKanibal]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AmbilBarangKanibal](
	[Kode] [varchar](10) NOT NULL,
	[DariBarang] [varchar](10) NULL,
	[KeArmada] [varchar](10) NULL,
	[Barang] [varchar](10) NULL,
	[TglTukar] [datetime] NULL,
	[Peminta] [varchar](10) NULL,
	[LaporanPerbaikan] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[TglEntry] [datetime] NULL,
	[Operator] [varchar](10) NULL,
	[CreateBy] [varchar](10) NULL,
 CONSTRAINT [PK_AmbilBarangKanibal] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [LAPORANPERBAIKAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LAPORANPERBAIKAN_UPDATE] ON [dbo].[LaporanPerbaikan]
for UPDATE
AS
BEGIN
	update LAPORANPERBAIKAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [LAPORANPERBAIKAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LAPORANPERBAIKAN_INSERT] ON [dbo].[LaporanPerbaikan]
for INSERT
AS
BEGIN
	update LAPORANPERBAIKAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[LPB]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LPB](
	[Kode] [varchar](10) NOT NULL,
	[Supplier] [varchar](10) NOT NULL,
	[Pemeriksa] [varchar](10) NOT NULL,
	[Penerima] [varchar](10) NOT NULL,
	[PO] [varchar](10) NULL,
	[Retur] [varchar](10) NULL,
	[CashNCarry] [varchar](10) NULL,
	[TglTerima] [datetime] NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_LPB] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailLPB]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailLPB](
	[KodeLPB] [varchar](10) NOT NULL,
	[Barang] [varchar](10) NOT NULL,
	[Jumlah] [int] NOT NULL,
	[JenisSesuai] [bit] NOT NULL,
	[KondisiBaik] [bit] NOT NULL,
	[JumlahSesuai] [bit] NOT NULL,
	[TepatWaktu] [bit] NOT NULL,
	[Diterima] [bit] NOT NULL,
	[Keterangan] [varchar](max) NULL,
 CONSTRAINT [PK_DetailLPB] PRIMARY KEY CLUSTERED 
(
	[KodeLPB] ASC,
	[Barang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [Update_TerimaBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[Update_TerimaBarang]
on [dbo].[DetailLPB]
for UPDATE
as
declare @Barang varchar(10);
declare @JumlahLama int;
declare @JumlahLPB int;
declare @JumlahBaru int;
declare @JumlahLPBLama int;
declare @PO varchar(10);
declare @Retur varchar(10);
declare @CashNCarry varchar(10);
declare @KodeLPB varchar(10);
declare @KodeDaftarBeli varchar(10);
declare @KodeBonBarang varchar(10);
declare @JumlahBeli int;
declare @JumlahDimintaLPB int;
set @Barang= (select i.Barang from inserted i);
set @JumlahLama=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahLPB=(select i.Jumlah from inserted i);
set @JumlahLPBLama= (select d.Jumlah from deleted d);
set @JumlahBaru=@JumlahLama-@JumlahLPBLama+@JumlahLPB;
update Barang set Jumlah=@JumlahBaru where Barang.Kode=@Barang
set @KodeLPB=(select i.KodeLPB from inserted i);
set @PO=(select PO from LPB where LPB.Kode=@KodeLPB);
set @CashNCarry=(select CashNCarry from LPB where LPB.Kode=@KodeLPB);
if @PO is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailPO dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodePO=@PO and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.PO=@PO and l.Kode=dl.KodeLPB);
if @JumlahBeli>@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='ON PROCESS' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='ON PROCESS' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
end
if @CashNCarry is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailCashNCarry dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodeCNC=@CashNCarry and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.CashNCarry=@CashNCarry and l.Kode=dl.KodeLPB);
if @JumlahBeli>@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='ON PROCESS' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='ON PROCESS' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
end
GO
/****** Object:  Trigger [Insert_TerimaBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[Insert_TerimaBarang]
on [dbo].[DetailLPB]
for INSERT
as
declare @Barang varchar(10);
declare @JumlahLama int;
declare @JumlahLPB int;
declare @JumlahBaru int;
declare @PO varchar(10);
declare @Retur varchar(10);
declare @CashNCarry varchar(10);
declare @KodeLPB varchar(10);
declare @KodeDaftarBeli varchar(10);
declare @KodeBonBarang varchar(10);
declare @JumlahBeli int;
declare @JumlahDimintaLPB int;
set @Barang= (select i.Barang from inserted i);
set @JumlahLama=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahLPB=(select i.Jumlah from inserted i);
set @JumlahBaru=@JumlahLama+@JumlahLPB;
update Barang set Jumlah=@JumlahBaru where Barang.Kode=@Barang
set @KodeLPB=(select i.KodeLPB from inserted i);
set @PO=(select PO from LPB where LPB.Kode=@KodeLPB);
set @CashNCarry=(select CashNCarry from LPB where LPB.Kode=@KodeLPB);
if @PO is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailPO dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodePO=@PO and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.PO=@PO and l.Kode=dl.KodeLPB);
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
end
else if @CashNCarry is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailCashNCarry dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodeCNC=@CashNCarry and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.CashNCarry=@CashNCarry and l.Kode=dl.KodeLPB);
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang
end
end
GO
/****** Object:  Trigger [Delete_TerimaBarang]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[Delete_TerimaBarang]
on [dbo].[DetailLPB]
for Delete
as
declare @Barang varchar(10);
declare @JumlahLama int;
declare @JumlahLPB int;
declare @JumlahBaru int;
declare @JumlahLPBLama int;
declare @PO varchar(10);
declare @Retur varchar(10);
declare @CashNCarry varchar(10);
declare @KodeLPB varchar(10);
declare @KodeDaftarBeli varchar(10);
declare @KodeBonBarang varchar(10);
declare @JumlahBeli int;
declare @JumlahDimintaLPB int;
set @Barang= (select d.Barang from deleted d);
set @JumlahLama=(select b.Jumlah from Barang b where b.Kode=@Barang);
set @JumlahLPB= (select d.Jumlah from deleted d);
set @JumlahBaru=@JumlahLama-@JumlahLPB;
update Barang set Jumlah=@JumlahBaru where Barang.Kode=@Barang
set @KodeLPB=(select d.KodeLPB from deleted d);
set @PO=(select PO from LPB where LPB.Kode=@KodeLPB);
set @CashNCarry=(select CashNCarry from LPB where LPB.Kode=@KodeLPB);
if @PO is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailPO dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodePO=@PO and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.PO=@PO and l.Kode=dl.KodeLPB);
if @JumlahBeli>@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='ON PROCESS' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='ON PROCESS' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
end
if @CashNCarry is not NULL
begin
set @KodeDaftarBeli=(select KodeDaftarBeli from DetailCashNCarry dp, DaftarBeli db where dp.KodeDaftarBeli=db.Kode and dp.KodeCNC=@CashNCarry and db.Barang=@Barang);
set @KodeBonBarang=(select BonBarang from DaftarBeli where Kode=@KodeDaftarBeli);
set @JumlahBeli= (select JumlahBeli from DetailBonBarang where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang);
set @JumlahDimintaLPB= (select SUM(jumlah) from DetailLPB dl,LPB l where dl.Barang=@Barang and l.CashNCarry=@CashNCarry and l.Kode=dl.KodeLPB);
if @JumlahBeli>@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='ON PROCESS' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='ON PROCESS' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
if @JumlahBeli<=@JumlahDimintaLPB
begin
update DetailBonBarang set StatusBeli='FINISHED' where KodeBonBarang=@KodeBonBarang and KodeBarang=@Barang;
update DaftarBeli set DaftarBeli.Status='FINISHED' where BonBarang=@KodeBonBarang and Barang=@Barang;
end
end
GO
/****** Object:  Trigger [LEPASBAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LEPASBAN_UPDATE] ON [dbo].[LepasBan]
for UPDATE
AS
BEGIN
	update LEPASBAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [LEPASBAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LEPASBAN_INSERT] ON [dbo].[LepasBan]
for INSERT
AS
BEGIN
	update LEPASBAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PASANGBAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PASANGBAN_UPDATE] ON [dbo].[PasangBan]
for UPDATE
AS
BEGIN
	update PASANGBAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PASANGBAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PASANGBAN_INSERT] ON [dbo].[PasangBan]
for INSERT
AS
BEGIN
	update PASANGBAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [MASTERSO_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[MASTERSO_UPDATE] ON [dbo].[MasterSO]
for UPDATE
AS
BEGIN
	update MASTERSO set TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
/****** Object:  Trigger [MASTERSO_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[MASTERSO_INSERT] ON [dbo].[MasterSO]
for INSERT
AS
BEGIN
	update MASTERSO set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
/****** Object:  Table [dbo].[Penagihan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Penagihan](
	[Kode] [varchar](10) NOT NULL,
	[SalesOrder] [varchar](10) NOT NULL,
	[BiayaLain] [money] NULL,
	[KetBiayaLain] [varchar](max) NULL,
	[Claim] [money] NULL,
	[ketClaim] [varchar](max) NULL,
	[Terbayar] [money] NULL,
	[TglDibayar] [datetime] NULL,
	[CaraPembayaran] [varchar](50) NULL,
	[NoKwitansi] [varchar](50) NULL,
	[NominalKwitansiPenagihan] [money] NULL,
	[PenerimaPembayaran] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_Penagihan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC,
	[SalesOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [RETUR_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[RETUR_UPDATE] ON [dbo].[Retur]
for UPDATE
AS
BEGIN
	update RETUR set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [RETUR_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[RETUR_INSERT] ON [dbo].[Retur]
for INSERT
AS
BEGIN
	update RETUR set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [REALISASITRAYEK_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REALISASITRAYEK_UPDATE] ON [dbo].[RealisasiTrayek]
for UPDATE
AS
BEGIN
	update RealisasiTrayek set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [REALISASITRAYEK_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REALISASITRAYEK_INSERT] ON [dbo].[RealisasiTrayek]
for INSERT
AS
BEGIN
	update RealisasiTrayek set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PERBAIKANBAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PERBAIKANBAN_UPDATE] ON [dbo].[PerbaikanBan]
for UPDATE
AS
BEGIN
	update PERBAIKANBAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PERBAIKANBAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PERBAIKANBAN_INSERT] ON [dbo].[PerbaikanBan]
for INSERT
AS
BEGIN
	update PERBAIKANBAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[RealisasiAnjem]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RealisasiAnjem](
	[Kode] [varchar](10) NOT NULL,
	[Kontrak] [varchar](10) NULL,
	[TanggalMulai] [datetime] NULL,
	[TanggalSelesai] [datetime] NULL,
	[Armada] [varchar](10) NULL,
	[Pengemudi] [varchar](10) NULL,
	[KilometerAwal] [int] NULL,
	[KilometerAkhir] [int] NULL,
	[Pendapatan] [money] NULL,
	[Pengeluaran] [money] NULL,
	[SisaDisetor] [money] NULL,
	[SPBUAYaniLiter] [numeric](18, 4) NULL,
	[SPBUAYaniUang] [money] NULL,
	[SPBUAYaniJam] [datetime] NULL,
	[SPBULuarLiter] [numeric](18, 4) NULL,
	[SPBULuarUang] [money] NULL,
	[SPBULuarUangDiberi] [money] NULL,
	[SPBULuarDetail] [varchar](max) NULL,
	[PremiSopir] [money] NULL,
	[PremiKernet] [money] NULL,
	[TabunganSopir] [money] NULL,
	[Tol] [money] NULL,
	[BiayaLainLain] [money] NULL,
	[KeteranganBiayaLainLain] [varchar](max) NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[CreateDate] [datetime] NULL,
	[TglEntry] [datetime] NULL,
 CONSTRAINT [PK_RealisasiAnjem] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [REALISASIANJEM_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REALISASIANJEM_UPDATE] ON [dbo].[RealisasiAnjem]
for UPDATE
AS
BEGIN
	update RealisasiAnjem set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [REALISASIANJEM_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[REALISASIANJEM_INSERT] ON [dbo].[RealisasiAnjem]
for INSERT
AS
BEGIN
	update RealisasiAnjem set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PENAGIHAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PENAGIHAN_UPDATE] ON [dbo].[Penagihan]
for UPDATE
AS
BEGIN
	update PENAGIHAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [PENAGIHAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PENAGIHAN_INSERT] ON [dbo].[Penagihan]
for INSERT
AS
BEGIN
	update PENAGIHAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
	declare @NoSO varchar(10);
	select @NoSO=i.SalesOrder from inserted i;
	update MasterSO set StatusPembayaran='LUNAS' where MasterSO.kodenota=@NoSO;
END
GO
/****** Object:  Trigger [PENAGIHAN_DELETE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[PENAGIHAN_DELETE] ON [dbo].[Penagihan]
for DELETE
AS
BEGIN
	declare @NoSO varchar(10);
	select @NoSO=i.SalesOrder from deleted i;
	update MasterSO set StatusPembayaran='BELUM LUNAS' where MasterSO.kodenota=@NoSO;
END
GO
/****** Object:  Trigger [LPB_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LPB_UPDATE] ON [dbo].[LPB]
for UPDATE
AS
BEGIN
	update LPB set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [LPB_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[LPB_INSERT] ON [dbo].[LPB]
for INSERT
AS
BEGIN
	update LPB set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [MASTERSJ_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[MASTERSJ_UPDATE] ON [dbo].[MasterSJ]
for UPDATE
AS
BEGIN
	update MASTERSJ set TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
/****** Object:  Trigger [MASTERSJ_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[MASTERSJ_INSERT] ON [dbo].[MasterSJ]
for INSERT
AS
BEGIN
	update MASTERSJ set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where KODENOTA in (select KODENOTA from inserted);
END
GO
/****** Object:  Table [dbo].[KeluhanPelanggan]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KeluhanPelanggan](
	[Kode] [varchar](10) NOT NULL,
	[Pelanggan] [varchar](10) NOT NULL,
	[Armada] [varchar](10) NOT NULL,
	[Sopir] [varchar](10) NOT NULL,
	[SuratJalan] [varchar](10) NOT NULL,
	[TglKejadian] [datetime] NOT NULL,
	[LokasiKejadian] [varchar](50) NOT NULL,
	[Perihal] [varchar](max) NOT NULL,
	[Analisa] [varchar](max) NULL,
	[TindakanPerbaikan] [varchar](max) NULL,
	[PelakuPerbaikan] [varchar](10) NULL,
	[TglPerbaikan] [datetime] NULL,
	[Verifikasi] [varchar](max) NULL,
	[Verifikator] [varchar](10) NULL,
	[TglVerifikasi] [datetime] NULL,
	[Pencegahan] [varchar](max) NULL,
	[PelakuPencegahan] [varchar](10) NULL,
	[TglPencegahan] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_KeluhanPelanggan] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [AMBILBARANGKANIBAL_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[AMBILBARANGKANIBAL_UPDATE] ON [dbo].[AmbilBarangKanibal]
for UPDATE
AS
BEGIN
	update ambilbarangkanibal set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [AMBILBARANGKANIBAL_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[AMBILBARANGKANIBAL_INSERT] ON [dbo].[AmbilBarangKanibal]
for INSERT
AS
BEGIN
	update ambilbarangkanibal set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Table [dbo].[BonSopir]    Script Date: 04/08/2013 18:28:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonSopir](
	[Kode] [varchar](10) NOT NULL,
	[SuratJalan] [varchar](10) NOT NULL,
	[Nominal] [money] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](10) NULL,
	[Operator] [varchar](10) NULL,
	[TglEntry] [datetime] NULL,
	[TglCetak] [datetime] NULL,
 CONSTRAINT [PK_BonSopir] PRIMARY KEY CLUSTERED 
(
	[Kode] ASC,
	[SuratJalan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [insert_RealisasiAnjem]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[insert_RealisasiAnjem]
on [dbo].[RealisasiAnjem]
for INSERT
as
declare @Kontrak varchar(10);
declare @Sopir varchar(10);
set @Kontrak=(select i.Kontrak from inserted i);
set @Sopir=(select i.Pengemudi from inserted i);
update KasBonAnjem set Status='FINISHED' where
Kontrak=@Kontrak and Sopir=@Sopir;
GO
/****** Object:  Trigger [TUKARKOMPONEN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TUKARKOMPONEN_UPDATE] ON [dbo].[TukarKomponen]
for UPDATE
AS
BEGIN
	update TUKARKOMPONEN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [TUKARKOMPONEN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TUKARKOMPONEN_INSERT] ON [dbo].[TukarKomponen]
for INSERT
AS
BEGIN
	update TUKARKOMPONEN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [SURATPERINTAHKERJA_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[SURATPERINTAHKERJA_UPDATE] ON [dbo].[SuratPerintahKerja]
for UPDATE
AS
BEGIN
	update SuratPerintahKerja set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [SURATPERINTAHKERJA_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[SURATPERINTAHKERJA_INSERT] ON [dbo].[SuratPerintahKerja]
for INSERT
AS
BEGIN
	update SuratPerintahKerja set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [upd_KasBon]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create trigger [dbo].[upd_KasBon]
on [dbo].[BonSopir]
for UPDATE
as
BEGIN
declare @Nominal money;
declare @NoSJ varchar(10);
select @Nominal=i.Nominal from inserted i
select @NoSJ=i.SuratJalan from inserted i
update MasterSJ set UangJalan=@Nominal where Kodenota=@NoSJ
END
GO
/****** Object:  Trigger [STORING_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[STORING_UPDATE] ON [dbo].[Storing]
for UPDATE
AS
BEGIN
	update STORING set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [STORING_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[STORING_INSERT] ON [dbo].[Storing]
for INSERT
AS
BEGIN
	update STORING set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [KELUHANPELANGGAN_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[KELUHANPELANGGAN_UPDATE] ON [dbo].[KeluhanPelanggan]
for UPDATE
AS
BEGIN
	update KELUHANPELANGGAN set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [KELUHANPELANGGAN_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[KELUHANPELANGGAN_INSERT] ON [dbo].[KeluhanPelanggan]
for INSERT
AS
BEGIN
	update KELUHANPELANGGAN set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [ins_KasBon]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create trigger [dbo].[ins_KasBon]
on [dbo].[BonSopir]
for INSERT
as
BEGIN
declare @Nominal money;
declare @NoSJ varchar(10);
select @Nominal=i.Nominal from inserted i
select @NoSJ=i.SuratJalan from inserted i
update MasterSJ set UangJalan=@Nominal where Kodenota=@NoSJ
END
GO
/****** Object:  Trigger [del_KasBon]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create trigger [dbo].[del_KasBon]
on [dbo].[BonSopir]
for DELETE
as
BEGIN
declare @NoSJ varchar(10);
select @NoSJ=d.SuratJalan from deleted d;
update MasterSJ set UangJalan=NULL where Kodenota=@NoSJ;
END
GO
/****** Object:  Trigger [BONSOPIR_UPDATE]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BONSOPIR_UPDATE] ON [dbo].[BonSopir]
for UPDATE
AS
BEGIN
	update BONSOPIR set TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Trigger [BONSOPIR_INSERT]    Script Date: 04/08/2013 18:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[BONSOPIR_INSERT] ON [dbo].[BonSopir]
for INSERT
AS
BEGIN
	update BONSOPIR set createdate=CURRENT_TIMESTAMP,TGLENTRY=CURRENT_TIMESTAMP where kode in (select KODE from inserted);
END
GO
/****** Object:  Default [DF_User_MasterArmada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterArmada]  DEFAULT ((0)) FOR [MasterArmada]
GO
/****** Object:  Default [DF_User_InsertMasterArmada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_InsertMasterArmada]  DEFAULT ((0)) FOR [InsertMasterArmada]
GO
/****** Object:  Default [DF_User_MasterJenisKendaraan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterJenisKendaraan]  DEFAULT ((0)) FOR [MasterJenisKendaraan]
GO
/****** Object:  Default [DF_User_MasterEkor]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterEkor]  DEFAULT ((0)) FOR [MasterEkor]
GO
/****** Object:  Default [DF_User_MasterPelanggan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterPelanggan]  DEFAULT ((0)) FOR [MasterPelanggan]
GO
/****** Object:  Default [DF_User_MasterRute]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterRute]  DEFAULT ((0)) FOR [MasterRute]
GO
/****** Object:  Default [DF_User_MasterSopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterSopir]  DEFAULT ((0)) FOR [MasterSopir]
GO
/****** Object:  Default [DF_User_MasterJarak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterJarak]  DEFAULT ((0)) FOR [MasterJarak]
GO
/****** Object:  Default [DF_User_MasterBan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterBan]  DEFAULT ((0)) FOR [MasterBan]
GO
/****** Object:  Default [DF_User_MasterStandardPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterStandardPerawatan]  DEFAULT ((0)) FOR [MasterStandardPerawatan]
GO
/****** Object:  Default [DF_User_MasterBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterBarang]  DEFAULT ((0)) FOR [MasterBarang]
GO
/****** Object:  Default [DF_User_MasterJenisPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterJenisPerbaikan]  DEFAULT ((0)) FOR [MasterJenisPerbaikan]
GO
/****** Object:  Default [DF_User_MasterSupplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterSupplier]  DEFAULT ((0)) FOR [MasterSupplier]
GO
/****** Object:  Default [DF_User_MasterUser]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_MasterUser]  DEFAULT ((0)) FOR [MasterUser]
GO
/****** Object:  ForeignKey [FK_BonKeluarBarang_BonBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonKeluarBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonKeluarBarang_BonBarang] FOREIGN KEY([BonBarang])
REFERENCES [dbo].[BonBarang] ([Kode])
GO
ALTER TABLE [dbo].[BonKeluarBarang] CHECK CONSTRAINT [FK_BonKeluarBarang_BonBarang]
GO
/****** Object:  ForeignKey [FK_BonKeluarBarang_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonKeluarBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonKeluarBarang_Pegawai] FOREIGN KEY([Penerima])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[BonKeluarBarang] CHECK CONSTRAINT [FK_BonKeluarBarang_Pegawai]
GO
/****** Object:  ForeignKey [FK_BonKeluarBarang_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonKeluarBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonKeluarBarang_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonKeluarBarang] CHECK CONSTRAINT [FK_BonKeluarBarang_User]
GO
/****** Object:  ForeignKey [FK_BonKeluarBarang_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonKeluarBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonKeluarBarang_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonKeluarBarang] CHECK CONSTRAINT [FK_BonKeluarBarang_User1]
GO
/****** Object:  ForeignKey [FK_User_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Pegawai] FOREIGN KEY([KodePegawai])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Pegawai]
GO
/****** Object:  ForeignKey [FK_User_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_User] FOREIGN KEY([Kode])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_User]
GO
/****** Object:  ForeignKey [FK_StandarPerawatan_JenisKendaraan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[StandarPerawatan]  WITH CHECK ADD  CONSTRAINT [FK_StandarPerawatan_JenisKendaraan] FOREIGN KEY([JenisKendaraan])
REFERENCES [dbo].[JenisKendaraan] ([Kode])
GO
ALTER TABLE [dbo].[StandarPerawatan] CHECK CONSTRAINT [FK_StandarPerawatan_JenisKendaraan]
GO
/****** Object:  ForeignKey [FK_Supplier_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Supplier] CHECK CONSTRAINT [FK_Supplier_User]
GO
/****** Object:  ForeignKey [FK_Supplier_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Supplier] CHECK CONSTRAINT [FK_Supplier_User1]
GO
/****** Object:  ForeignKey [FK_Rute_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rute]  WITH CHECK ADD  CONSTRAINT [FK_Rute_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Rute] CHECK CONSTRAINT [FK_Rute_User]
GO
/****** Object:  ForeignKey [FK_Pelanggan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Pelanggan]  WITH CHECK ADD  CONSTRAINT [FK_Pelanggan_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Pelanggan] CHECK CONSTRAINT [FK_Pelanggan_User]
GO
/****** Object:  ForeignKey [FK_Pelanggan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Pelanggan]  WITH CHECK ADD  CONSTRAINT [FK_Pelanggan_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Pelanggan] CHECK CONSTRAINT [FK_Pelanggan_User1]
GO
/****** Object:  ForeignKey [FK_JenisPerbaikan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[JenisPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_JenisPerbaikan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[JenisPerbaikan] CHECK CONSTRAINT [FK_JenisPerbaikan_User]
GO
/****** Object:  ForeignKey [FK_JenisPerbaikan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[JenisPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_JenisPerbaikan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[JenisPerbaikan] CHECK CONSTRAINT [FK_JenisPerbaikan_User1]
GO
/****** Object:  ForeignKey [FK_DetailCashNCarry_CashNCarry]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailCashNCarry]  WITH CHECK ADD  CONSTRAINT [FK_DetailCashNCarry_CashNCarry] FOREIGN KEY([KodeCNC])
REFERENCES [dbo].[CashNCarry] ([Kode])
GO
ALTER TABLE [dbo].[DetailCashNCarry] CHECK CONSTRAINT [FK_DetailCashNCarry_CashNCarry]
GO
/****** Object:  ForeignKey [FK_DetailCashNCarry_DaftarBeli]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailCashNCarry]  WITH CHECK ADD  CONSTRAINT [FK_DetailCashNCarry_DaftarBeli] FOREIGN KEY([KodeDaftarBeli])
REFERENCES [dbo].[DaftarBeli] ([Kode])
GO
ALTER TABLE [dbo].[DetailCashNCarry] CHECK CONSTRAINT [FK_DetailCashNCarry_DaftarBeli]
GO
/****** Object:  ForeignKey [FK_CashNCarry_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[CashNCarry]  WITH CHECK ADD  CONSTRAINT [FK_CashNCarry_Pegawai] FOREIGN KEY([Penyetuju])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[CashNCarry] CHECK CONSTRAINT [FK_CashNCarry_Pegawai]
GO
/****** Object:  ForeignKey [FK_CashNCarry_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[CashNCarry]  WITH CHECK ADD  CONSTRAINT [FK_CashNCarry_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[CashNCarry] CHECK CONSTRAINT [FK_CashNCarry_User]
GO
/****** Object:  ForeignKey [FK_CashNCarry_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[CashNCarry]  WITH CHECK ADD  CONSTRAINT [FK_CashNCarry_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[CashNCarry] CHECK CONSTRAINT [FK_CashNCarry_User1]
GO
/****** Object:  ForeignKey [FK_DetailBonBarang_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBonBarang]  WITH CHECK ADD  CONSTRAINT [FK_DetailBonBarang_Barang] FOREIGN KEY([KodeBarang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DetailBonBarang] CHECK CONSTRAINT [FK_DetailBonBarang_Barang]
GO
/****** Object:  ForeignKey [FK_DetailBonBarang_BonBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBonBarang]  WITH CHECK ADD  CONSTRAINT [FK_DetailBonBarang_BonBarang] FOREIGN KEY([KodeBonBarang])
REFERENCES [dbo].[BonBarang] ([Kode])
GO
ALTER TABLE [dbo].[DetailBonBarang] CHECK CONSTRAINT [FK_DetailBonBarang_BonBarang]
GO
/****** Object:  ForeignKey [FK_DetailBKB_BonBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBKB]  WITH CHECK ADD  CONSTRAINT [FK_DetailBKB_BonBarang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DetailBKB] CHECK CONSTRAINT [FK_DetailBKB_BonBarang]
GO
/****** Object:  ForeignKey [FK_DetailBKB_BonKeluarBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBKB]  WITH CHECK ADD  CONSTRAINT [FK_DetailBKB_BonKeluarBarang] FOREIGN KEY([KodeBKB])
REFERENCES [dbo].[BonKeluarBarang] ([Kode])
GO
ALTER TABLE [dbo].[DetailBKB] CHECK CONSTRAINT [FK_DetailBKB_BonKeluarBarang]
GO
/****** Object:  ForeignKey [FK_DaftarBeli_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DaftarBeli]  WITH CHECK ADD  CONSTRAINT [FK_DaftarBeli_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DaftarBeli] CHECK CONSTRAINT [FK_DaftarBeli_Barang]
GO
/****** Object:  ForeignKey [FK_DaftarBeli_BonBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DaftarBeli]  WITH CHECK ADD  CONSTRAINT [FK_DaftarBeli_BonBarang] FOREIGN KEY([BonBarang])
REFERENCES [dbo].[BonBarang] ([Kode])
GO
ALTER TABLE [dbo].[DaftarBeli] CHECK CONSTRAINT [FK_DaftarBeli_BonBarang]
GO
/****** Object:  ForeignKey [FK_DaftarBeli_Supplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DaftarBeli]  WITH CHECK ADD  CONSTRAINT [FK_DaftarBeli_Supplier] FOREIGN KEY([LastSupplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[DaftarBeli] CHECK CONSTRAINT [FK_DaftarBeli_Supplier]
GO
/****** Object:  ForeignKey [FK_DaftarBeli_Supplier1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DaftarBeli]  WITH CHECK ADD  CONSTRAINT [FK_DaftarBeli_Supplier1] FOREIGN KEY([Supplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[DaftarBeli] CHECK CONSTRAINT [FK_DaftarBeli_Supplier1]
GO
/****** Object:  ForeignKey [FK_Barang_KategoriBarang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Barang]  WITH CHECK ADD  CONSTRAINT [FK_Barang_KategoriBarang] FOREIGN KEY([Kategori])
REFERENCES [dbo].[KategoriBarang] ([Kode])
GO
ALTER TABLE [dbo].[Barang] CHECK CONSTRAINT [FK_Barang_KategoriBarang]
GO
/****** Object:  ForeignKey [FK_Barang_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Barang]  WITH CHECK ADD  CONSTRAINT [FK_Barang_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Barang] CHECK CONSTRAINT [FK_Barang_User]
GO
/****** Object:  ForeignKey [FK_Barang_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Barang]  WITH CHECK ADD  CONSTRAINT [FK_Barang_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Barang] CHECK CONSTRAINT [FK_Barang_User1]
GO
/****** Object:  ForeignKey [FK_Armada_JenisKendaraan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Armada]  WITH CHECK ADD  CONSTRAINT [FK_Armada_JenisKendaraan] FOREIGN KEY([JenisKendaraan])
REFERENCES [dbo].[JenisKendaraan] ([Kode])
GO
ALTER TABLE [dbo].[Armada] CHECK CONSTRAINT [FK_Armada_JenisKendaraan]
GO
/****** Object:  ForeignKey [FK_Armada_MasterAC]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Armada]  WITH CHECK ADD  CONSTRAINT [FK_Armada_MasterAC] FOREIGN KEY([JenisAC])
REFERENCES [dbo].[MasterAC] ([Kode])
GO
ALTER TABLE [dbo].[Armada] CHECK CONSTRAINT [FK_Armada_MasterAC]
GO
/****** Object:  ForeignKey [FK_Armada_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Armada]  WITH CHECK ADD  CONSTRAINT [FK_Armada_Pegawai] FOREIGN KEY([Sopir])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Armada] CHECK CONSTRAINT [FK_Armada_Pegawai]
GO
/****** Object:  ForeignKey [FK_Armada_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Armada]  WITH NOCHECK ADD  CONSTRAINT [FK_Armada_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Armada] NOCHECK CONSTRAINT [FK_Armada_User]
GO
/****** Object:  ForeignKey [FK_Armada_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Armada]  WITH NOCHECK ADD  CONSTRAINT [FK_Armada_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Armada] NOCHECK CONSTRAINT [FK_Armada_User1]
GO
/****** Object:  ForeignKey [FK_BarangKanibal_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_BarangKanibal_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[BarangKanibal] CHECK CONSTRAINT [FK_BarangKanibal_Barang]
GO
/****** Object:  ForeignKey [FK_BarangKanibal_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_BarangKanibal_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BarangKanibal] CHECK CONSTRAINT [FK_BarangKanibal_User]
GO
/****** Object:  ForeignKey [FK_BarangKanibal_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_BarangKanibal_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BarangKanibal] CHECK CONSTRAINT [FK_BarangKanibal_User1]
GO
/****** Object:  ForeignKey [FK_Ban_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Ban]  WITH CHECK ADD  CONSTRAINT [FK_Ban_Barang] FOREIGN KEY([KodeBarang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[Ban] CHECK CONSTRAINT [FK_Ban_Barang]
GO
/****** Object:  ForeignKey [FK_Ban_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Ban]  WITH CHECK ADD  CONSTRAINT [FK_Ban_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Ban] CHECK CONSTRAINT [FK_Ban_User]
GO
/****** Object:  ForeignKey [FK_Ban_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Ban]  WITH CHECK ADD  CONSTRAINT [FK_Ban_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Ban] CHECK CONSTRAINT [FK_Ban_User1]
GO
/****** Object:  ForeignKey [FK_DetailBarang_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBarang]  WITH CHECK ADD  CONSTRAINT [FK_DetailBarang_Barang] FOREIGN KEY([KodeBarang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DetailBarang] CHECK CONSTRAINT [FK_DetailBarang_Barang]
GO
/****** Object:  ForeignKey [FK_DetailBarang_JenisKendaraan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailBarang]  WITH CHECK ADD  CONSTRAINT [FK_DetailBarang_JenisKendaraan] FOREIGN KEY([KodeJenisKendaraan])
REFERENCES [dbo].[JenisKendaraan] ([Kode])
GO
ALTER TABLE [dbo].[DetailBarang] CHECK CONSTRAINT [FK_DetailBarang_JenisKendaraan]
GO
/****** Object:  ForeignKey [FK_Rebuild_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Armada] FOREIGN KEY([DariArmada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Armada]
GO
/****** Object:  ForeignKey [FK_Rebuild_Armada1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Armada1] FOREIGN KEY([KeArmada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Armada1]
GO
/****** Object:  ForeignKey [FK_Rebuild_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Barang]
GO
/****** Object:  ForeignKey [FK_Rebuild_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Pegawai] FOREIGN KEY([PICKirim])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Pegawai]
GO
/****** Object:  ForeignKey [FK_Rebuild_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Pegawai1] FOREIGN KEY([Penerima])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Pegawai1]
GO
/****** Object:  ForeignKey [FK_Rebuild_Pegawai2]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Pegawai2] FOREIGN KEY([Verifikator])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Pegawai2]
GO
/****** Object:  ForeignKey [FK_Rebuild_Supplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_Supplier] FOREIGN KEY([Supplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_Supplier]
GO
/****** Object:  ForeignKey [FK_Rebuild_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_User]
GO
/****** Object:  ForeignKey [FK_Rebuild_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Rebuild]  WITH CHECK ADD  CONSTRAINT [FK_Rebuild_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Rebuild] CHECK CONSTRAINT [FK_Rebuild_User1]
GO
/****** Object:  ForeignKey [FK_DetailPO_DaftarBeli]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPO]  WITH CHECK ADD  CONSTRAINT [FK_DetailPO_DaftarBeli] FOREIGN KEY([KodeDaftarBeli])
REFERENCES [dbo].[DaftarBeli] ([Kode])
GO
ALTER TABLE [dbo].[DetailPO] CHECK CONSTRAINT [FK_DetailPO_DaftarBeli]
GO
/****** Object:  ForeignKey [FK_DetailPO_PO]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPO]  WITH CHECK ADD  CONSTRAINT [FK_DetailPO_PO] FOREIGN KEY([KodePO])
REFERENCES [dbo].[PO] ([Kode])
GO
ALTER TABLE [dbo].[DetailPO] CHECK CONSTRAINT [FK_DetailPO_PO]
GO
/****** Object:  ForeignKey [FK_PO_Supplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PO]  WITH CHECK ADD  CONSTRAINT [FK_PO_Supplier] FOREIGN KEY([Supplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[PO] CHECK CONSTRAINT [FK_PO_Supplier]
GO
/****** Object:  ForeignKey [FK_PO_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PO]  WITH CHECK ADD  CONSTRAINT [FK_PO_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PO] CHECK CONSTRAINT [FK_PO_User]
GO
/****** Object:  ForeignKey [FK_PO_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PO]  WITH CHECK ADD  CONSTRAINT [FK_PO_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PO] CHECK CONSTRAINT [FK_PO_User1]
GO
/****** Object:  ForeignKey [FK_PermintaanPerbaikan_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PermintaanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_PermintaanPerbaikan_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[PermintaanPerbaikan] CHECK CONSTRAINT [FK_PermintaanPerbaikan_Armada]
GO
/****** Object:  ForeignKey [FK_PermintaanPerbaikan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PermintaanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_PermintaanPerbaikan_Pegawai] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PermintaanPerbaikan] CHECK CONSTRAINT [FK_PermintaanPerbaikan_Pegawai]
GO
/****** Object:  ForeignKey [FK_PermintaanPerbaikan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PermintaanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_PermintaanPerbaikan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PermintaanPerbaikan] CHECK CONSTRAINT [FK_PermintaanPerbaikan_User]
GO
/****** Object:  ForeignKey [FK_PermintaanPerbaikan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PermintaanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_PermintaanPerbaikan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PermintaanPerbaikan] CHECK CONSTRAINT [FK_PermintaanPerbaikan_User1]
GO
/****** Object:  ForeignKey [FK_LaporanPerawatan_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerawatan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerawatan_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerawatan] CHECK CONSTRAINT [FK_LaporanPerawatan_Armada]
GO
/****** Object:  ForeignKey [FK_LaporanPerawatan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerawatan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerawatan_Pegawai] FOREIGN KEY([Verifikator])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerawatan] CHECK CONSTRAINT [FK_LaporanPerawatan_Pegawai]
GO
/****** Object:  ForeignKey [FK_LaporanPerawatan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerawatan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerawatan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerawatan] CHECK CONSTRAINT [FK_LaporanPerawatan_User]
GO
/****** Object:  ForeignKey [FK_LaporanPerawatan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerawatan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerawatan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerawatan] CHECK CONSTRAINT [FK_LaporanPerawatan_User1]
GO
/****** Object:  ForeignKey [FK_Kontrak_Pelanggan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Kontrak]  WITH CHECK ADD  CONSTRAINT [FK_Kontrak_Pelanggan] FOREIGN KEY([Pelanggan])
REFERENCES [dbo].[Pelanggan] ([Kode])
GO
ALTER TABLE [dbo].[Kontrak] CHECK CONSTRAINT [FK_Kontrak_Pelanggan]
GO
/****** Object:  ForeignKey [FK_Kontrak_Rute]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Kontrak]  WITH CHECK ADD  CONSTRAINT [FK_Kontrak_Rute] FOREIGN KEY([Rute])
REFERENCES [dbo].[Rute] ([Kode])
GO
ALTER TABLE [dbo].[Kontrak] CHECK CONSTRAINT [FK_Kontrak_Rute]
GO
/****** Object:  ForeignKey [FK_Kontrak_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Kontrak]  WITH CHECK ADD  CONSTRAINT [FK_Kontrak_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Kontrak] CHECK CONSTRAINT [FK_Kontrak_User]
GO
/****** Object:  ForeignKey [FK_Kontrak_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Kontrak]  WITH CHECK ADD  CONSTRAINT [FK_Kontrak_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Kontrak] CHECK CONSTRAINT [FK_Kontrak_User1]
GO
/****** Object:  ForeignKey [FK_ListSupplier_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[ListSupplier]  WITH CHECK ADD  CONSTRAINT [FK_ListSupplier_Barang] FOREIGN KEY([KodeBarang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[ListSupplier] CHECK CONSTRAINT [FK_ListSupplier_Barang]
GO
/****** Object:  ForeignKey [FK_ListSupplier_Supplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[ListSupplier]  WITH CHECK ADD  CONSTRAINT [FK_ListSupplier_Supplier] FOREIGN KEY([KodeSupplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[ListSupplier] CHECK CONSTRAINT [FK_ListSupplier_Supplier]
GO
/****** Object:  ForeignKey [FK_Pemutihan_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Pemutihan]  WITH CHECK ADD  CONSTRAINT [FK_Pemutihan_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[Pemutihan] CHECK CONSTRAINT [FK_Pemutihan_Barang]
GO
/****** Object:  ForeignKey [FK_Pemutihan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Pemutihan]  WITH CHECK ADD  CONSTRAINT [FK_Pemutihan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Pemutihan] CHECK CONSTRAINT [FK_Pemutihan_User]
GO
/****** Object:  ForeignKey [FK_Pemutihan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Pemutihan]  WITH CHECK ADD  CONSTRAINT [FK_Pemutihan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Pemutihan] CHECK CONSTRAINT [FK_Pemutihan_User1]
GO
/****** Object:  ForeignKey [FK_Retur_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Retur]  WITH CHECK ADD  CONSTRAINT [FK_Retur_Pegawai] FOREIGN KEY([Pelaksana])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Retur] CHECK CONSTRAINT [FK_Retur_Pegawai]
GO
/****** Object:  ForeignKey [FK_Retur_PO]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Retur]  WITH CHECK ADD  CONSTRAINT [FK_Retur_PO] FOREIGN KEY([KodePO])
REFERENCES [dbo].[PO] ([Kode])
GO
ALTER TABLE [dbo].[Retur] CHECK CONSTRAINT [FK_Retur_PO]
GO
/****** Object:  ForeignKey [FK_Retur_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Retur]  WITH CHECK ADD  CONSTRAINT [FK_Retur_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Retur] CHECK CONSTRAINT [FK_Retur_User]
GO
/****** Object:  ForeignKey [FK_Retur_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Retur]  WITH CHECK ADD  CONSTRAINT [FK_Retur_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Retur] CHECK CONSTRAINT [FK_Retur_User1]
GO
/****** Object:  ForeignKey [FK_LepasBan_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_Armada]
GO
/****** Object:  ForeignKey [FK_LepasBan_Ban]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_Ban] FOREIGN KEY([Ban])
REFERENCES [dbo].[Ban] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_Ban]
GO
/****** Object:  ForeignKey [FK_LepasBan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_Pegawai] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_Pegawai]
GO
/****** Object:  ForeignKey [FK_LepasBan_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_Pegawai1] FOREIGN KEY([Pelaksana])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_Pegawai1]
GO
/****** Object:  ForeignKey [FK_LepasBan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_User]
GO
/****** Object:  ForeignKey [FK_LepasBan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LepasBan]  WITH CHECK ADD  CONSTRAINT [FK_LepasBan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LepasBan] CHECK CONSTRAINT [FK_LepasBan_User1]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_Pegawai] FOREIGN KEY([Verifikator])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_Pegawai]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_Pegawai1] FOREIGN KEY([PICSerahTerima])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_Pegawai1]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_Pegawai2]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_Pegawai2] FOREIGN KEY([SopirClaim])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_Pegawai2]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_PermintaanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_PermintaanPerbaikan] FOREIGN KEY([PP])
REFERENCES [dbo].[PermintaanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_PermintaanPerbaikan]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_User]
GO
/****** Object:  ForeignKey [FK_LaporanPerbaikan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LaporanPerbaikan]  WITH CHECK ADD  CONSTRAINT [FK_LaporanPerbaikan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LaporanPerbaikan] CHECK CONSTRAINT [FK_LaporanPerbaikan_User1]
GO
/****** Object:  ForeignKey [FK_KasBonAnjem_CreateBy]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KasBonAnjem]  WITH CHECK ADD  CONSTRAINT [FK_KasBonAnjem_CreateBy] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[KasBonAnjem] CHECK CONSTRAINT [FK_KasBonAnjem_CreateBy]
GO
/****** Object:  ForeignKey [FK_KasBonAnjem_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KasBonAnjem]  WITH CHECK ADD  CONSTRAINT [FK_KasBonAnjem_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[KasBonAnjem] CHECK CONSTRAINT [FK_KasBonAnjem_Kontrak]
GO
/****** Object:  ForeignKey [FK_KasBonAnjem_Operator]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KasBonAnjem]  WITH CHECK ADD  CONSTRAINT [FK_KasBonAnjem_Operator] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[KasBonAnjem] CHECK CONSTRAINT [FK_KasBonAnjem_Operator]
GO
/****** Object:  ForeignKey [FK_KasBonAnjem_Sopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KasBonAnjem]  WITH CHECK ADD  CONSTRAINT [FK_KasBonAnjem_Sopir] FOREIGN KEY([Sopir])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[KasBonAnjem] CHECK CONSTRAINT [FK_KasBonAnjem_Sopir]
GO
/****** Object:  ForeignKey [FK_MasterSO_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Armada]
GO
/****** Object:  ForeignKey [FK_MasterSO_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Kontrak]
GO
/****** Object:  ForeignKey [FK_MasterSO_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Pegawai] FOREIGN KEY([PenerimaPembayaranAwal])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Pegawai]
GO
/****** Object:  ForeignKey [FK_MasterSO_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Pegawai1] FOREIGN KEY([PenerimaPelunasan])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Pegawai1]
GO
/****** Object:  ForeignKey [FK_MasterSO_Pelanggan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Pelanggan] FOREIGN KEY([Pelanggan])
REFERENCES [dbo].[Pelanggan] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Pelanggan]
GO
/****** Object:  ForeignKey [FK_MasterSO_Rute]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_Rute] FOREIGN KEY([Rute])
REFERENCES [dbo].[Rute] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_Rute]
GO
/****** Object:  ForeignKey [FK_MasterSO_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_User]
GO
/****** Object:  ForeignKey [FK_MasterSO_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSO]  WITH CHECK ADD  CONSTRAINT [FK_MasterSO_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[MasterSO] CHECK CONSTRAINT [FK_MasterSO_User1]
GO
/****** Object:  ForeignKey [FK_PasangBan_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_Armada]
GO
/****** Object:  ForeignKey [FK_PasangBan_Ban]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_Ban] FOREIGN KEY([Ban])
REFERENCES [dbo].[Ban] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_Ban]
GO
/****** Object:  ForeignKey [FK_PasangBan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_Pegawai] FOREIGN KEY([Pelaksana])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_Pegawai]
GO
/****** Object:  ForeignKey [FK_PasangBan_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_Pegawai1] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_Pegawai1]
GO
/****** Object:  ForeignKey [FK_PasangBan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_User]
GO
/****** Object:  ForeignKey [FK_PasangBan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PasangBan]  WITH CHECK ADD  CONSTRAINT [FK_PasangBan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PasangBan] CHECK CONSTRAINT [FK_PasangBan_User1]
GO
/****** Object:  ForeignKey [FK_PerbaikanBan_Ban]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PerbaikanBan]  WITH CHECK ADD  CONSTRAINT [FK_PerbaikanBan_Ban] FOREIGN KEY([Ban])
REFERENCES [dbo].[Ban] ([Kode])
GO
ALTER TABLE [dbo].[PerbaikanBan] CHECK CONSTRAINT [FK_PerbaikanBan_Ban]
GO
/****** Object:  ForeignKey [FK_PerbaikanBan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PerbaikanBan]  WITH CHECK ADD  CONSTRAINT [FK_PerbaikanBan_Pegawai] FOREIGN KEY([PIC])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PerbaikanBan] CHECK CONSTRAINT [FK_PerbaikanBan_Pegawai]
GO
/****** Object:  ForeignKey [FK_PerbaikanBan_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PerbaikanBan]  WITH CHECK ADD  CONSTRAINT [FK_PerbaikanBan_Pegawai1] FOREIGN KEY([Pemeriksa])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PerbaikanBan] CHECK CONSTRAINT [FK_PerbaikanBan_Pegawai1]
GO
/****** Object:  ForeignKey [FK_PerbaikanBan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PerbaikanBan]  WITH CHECK ADD  CONSTRAINT [FK_PerbaikanBan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PerbaikanBan] CHECK CONSTRAINT [FK_PerbaikanBan_User]
GO
/****** Object:  ForeignKey [FK_PerbaikanBan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PerbaikanBan]  WITH CHECK ADD  CONSTRAINT [FK_PerbaikanBan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PerbaikanBan] CHECK CONSTRAINT [FK_PerbaikanBan_User1]
GO
/****** Object:  ForeignKey [FK_PenagihanAnjem_CreateBy]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PenagihanAnjem]  WITH CHECK ADD  CONSTRAINT [FK_PenagihanAnjem_CreateBy] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PenagihanAnjem] CHECK CONSTRAINT [FK_PenagihanAnjem_CreateBy]
GO
/****** Object:  ForeignKey [FK_PenagihanAnjem_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PenagihanAnjem]  WITH CHECK ADD  CONSTRAINT [FK_PenagihanAnjem_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[PenagihanAnjem] CHECK CONSTRAINT [FK_PenagihanAnjem_Kontrak]
GO
/****** Object:  ForeignKey [FK_PenagihanAnjem_Operator]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PenagihanAnjem]  WITH CHECK ADD  CONSTRAINT [FK_PenagihanAnjem_Operator] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[PenagihanAnjem] CHECK CONSTRAINT [FK_PenagihanAnjem_Operator]
GO
/****** Object:  ForeignKey [FK_PenagihanAnjem_PenerimaPembayaran]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[PenagihanAnjem]  WITH CHECK ADD  CONSTRAINT [FK_PenagihanAnjem_PenerimaPembayaran] FOREIGN KEY([PenerimaPembayaran])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[PenagihanAnjem] CHECK CONSTRAINT [FK_PenagihanAnjem_PenerimaPembayaran]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_Armada]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_Kontrak]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_Pegawai] FOREIGN KEY([Kondektur])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_Pegawai]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_Sopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_Sopir] FOREIGN KEY([Pengemudi])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_Sopir]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_User]
GO
/****** Object:  ForeignKey [FK_RealisasiTrayek_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiTrayek]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiTrayek_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiTrayek] CHECK CONSTRAINT [FK_RealisasiTrayek_User1]
GO
/****** Object:  ForeignKey [FK_DetailStandard_Ban]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailStandard]  WITH CHECK ADD  CONSTRAINT [FK_DetailStandard_Ban] FOREIGN KEY([Barang])
REFERENCES [dbo].[Ban] ([Kode])
GO
ALTER TABLE [dbo].[DetailStandard] CHECK CONSTRAINT [FK_DetailStandard_Ban]
GO
/****** Object:  ForeignKey [FK_DetailStandard_StandarPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailStandard]  WITH CHECK ADD  CONSTRAINT [FK_DetailStandard_StandarPerawatan] FOREIGN KEY([KodeStandard])
REFERENCES [dbo].[StandarPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[DetailStandard] CHECK CONSTRAINT [FK_DetailStandard_StandarPerawatan]
GO
/****** Object:  ForeignKey [FK_DetailPerawatanMekanik_LaporanPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerawatanMekanik]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerawatanMekanik_LaporanPerawatan] FOREIGN KEY([KodePerawatan])
REFERENCES [dbo].[LaporanPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerawatanMekanik] CHECK CONSTRAINT [FK_DetailPerawatanMekanik_LaporanPerawatan]
GO
/****** Object:  ForeignKey [FK_DetailPerawatanMekanik_Mekanik]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerawatanMekanik]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerawatanMekanik_Mekanik] FOREIGN KEY([Mekanik])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerawatanMekanik] CHECK CONSTRAINT [FK_DetailPerawatanMekanik_Mekanik]
GO
/****** Object:  ForeignKey [FK_DetailPerawatanJenis_LaporanPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerawatanJenis]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerawatanJenis_LaporanPerawatan] FOREIGN KEY([KodePerawatan])
REFERENCES [dbo].[LaporanPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerawatanJenis] CHECK CONSTRAINT [FK_DetailPerawatanJenis_LaporanPerawatan]
GO
/****** Object:  ForeignKey [FK_DetailPerawatanJenis_StandarPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerawatanJenis]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerawatanJenis_StandarPerawatan] FOREIGN KEY([JenisPerawatan])
REFERENCES [dbo].[StandarPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerawatanJenis] CHECK CONSTRAINT [FK_DetailPerawatanJenis_StandarPerawatan]
GO
/****** Object:  ForeignKey [FK_ArmadaKontrak_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[ArmadaKontrak]  WITH CHECK ADD  CONSTRAINT [FK_ArmadaKontrak_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[ArmadaKontrak] CHECK CONSTRAINT [FK_ArmadaKontrak_Kontrak]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_Armada] FOREIGN KEY([KeArmada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_Armada]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_Barang]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_BarangKanibal]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_BarangKanibal] FOREIGN KEY([DariBarang])
REFERENCES [dbo].[BarangKanibal] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_BarangKanibal]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_LaporanPerbaikan] FOREIGN KEY([LaporanPerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_Pegawai] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_Pegawai]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_User]
GO
/****** Object:  ForeignKey [FK_AmbilBarangKanibal_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[AmbilBarangKanibal]  WITH CHECK ADD  CONSTRAINT [FK_AmbilBarangKanibal_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[AmbilBarangKanibal] CHECK CONSTRAINT [FK_AmbilBarangKanibal_User1]
GO
/****** Object:  ForeignKey [FK_DetailPerbaikanMekanik_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerbaikanMekanik]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerbaikanMekanik_LaporanPerbaikan] FOREIGN KEY([KodePerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerbaikanMekanik] CHECK CONSTRAINT [FK_DetailPerbaikanMekanik_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_DetailPerbaikanMekanik_Mekanik]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerbaikanMekanik]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerbaikanMekanik_Mekanik] FOREIGN KEY([Mekanik])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerbaikanMekanik] CHECK CONSTRAINT [FK_DetailPerbaikanMekanik_Mekanik]
GO
/****** Object:  ForeignKey [FK_DetailPerbaikanJenis_JenisPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerbaikanJenis]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerbaikanJenis_JenisPerbaikan] FOREIGN KEY([JenisPerbaikan])
REFERENCES [dbo].[JenisPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerbaikanJenis] CHECK CONSTRAINT [FK_DetailPerbaikanJenis_JenisPerbaikan]
GO
/****** Object:  ForeignKey [FK_DetailPerbaikanJenis_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailPerbaikanJenis]  WITH CHECK ADD  CONSTRAINT [FK_DetailPerbaikanJenis_LaporanPerbaikan] FOREIGN KEY([KodePerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[DetailPerbaikanJenis] CHECK CONSTRAINT [FK_DetailPerbaikanJenis_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_DetailRetur_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailRetur]  WITH CHECK ADD  CONSTRAINT [FK_DetailRetur_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DetailRetur] CHECK CONSTRAINT [FK_DetailRetur_Barang]
GO
/****** Object:  ForeignKey [FK_DetailRetur_Retur]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailRetur]  WITH CHECK ADD  CONSTRAINT [FK_DetailRetur_Retur] FOREIGN KEY([KodeRetur])
REFERENCES [dbo].[Retur] ([Kode])
GO
ALTER TABLE [dbo].[DetailRetur] CHECK CONSTRAINT [FK_DetailRetur_Retur]
GO
/****** Object:  ForeignKey [FK_RealisasiAnjem_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiAnjem]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiAnjem_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiAnjem] CHECK CONSTRAINT [FK_RealisasiAnjem_Armada]
GO
/****** Object:  ForeignKey [FK_RealisasiAnjem_Kontrak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiAnjem]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiAnjem_Kontrak] FOREIGN KEY([Kontrak])
REFERENCES [dbo].[Kontrak] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiAnjem] CHECK CONSTRAINT [FK_RealisasiAnjem_Kontrak]
GO
/****** Object:  ForeignKey [FK_RealisasiAnjem_Sopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiAnjem]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiAnjem_Sopir] FOREIGN KEY([Pengemudi])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiAnjem] CHECK CONSTRAINT [FK_RealisasiAnjem_Sopir]
GO
/****** Object:  ForeignKey [FK_RealisasiAnjem_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiAnjem]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiAnjem_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiAnjem] CHECK CONSTRAINT [FK_RealisasiAnjem_User]
GO
/****** Object:  ForeignKey [FK_RealisasiAnjem_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[RealisasiAnjem]  WITH CHECK ADD  CONSTRAINT [FK_RealisasiAnjem_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[RealisasiAnjem] CHECK CONSTRAINT [FK_RealisasiAnjem_User1]
GO
/****** Object:  ForeignKey [FK_penagihan_MasterSO]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Penagihan]  WITH CHECK ADD  CONSTRAINT [FK_penagihan_MasterSO] FOREIGN KEY([SalesOrder])
REFERENCES [dbo].[MasterSO] ([Kodenota])
GO
ALTER TABLE [dbo].[Penagihan] CHECK CONSTRAINT [FK_penagihan_MasterSO]
GO
/****** Object:  ForeignKey [FK_Penagihan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Penagihan]  WITH CHECK ADD  CONSTRAINT [FK_Penagihan_Pegawai] FOREIGN KEY([PenerimaPembayaran])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Penagihan] CHECK CONSTRAINT [FK_Penagihan_Pegawai]
GO
/****** Object:  ForeignKey [FK_penagihan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Penagihan]  WITH CHECK ADD  CONSTRAINT [FK_penagihan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Penagihan] CHECK CONSTRAINT [FK_penagihan_User]
GO
/****** Object:  ForeignKey [FK_penagihan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Penagihan]  WITH CHECK ADD  CONSTRAINT [FK_penagihan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Penagihan] CHECK CONSTRAINT [FK_penagihan_User1]
GO
/****** Object:  ForeignKey [FK_MasterSJ_Jarak]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_Jarak] FOREIGN KEY([Awal])
REFERENCES [dbo].[Jarak] ([kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_Jarak]
GO
/****** Object:  ForeignKey [FK_MasterSJ_Jarak1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_Jarak1] FOREIGN KEY([Akhir])
REFERENCES [dbo].[Jarak] ([kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_Jarak1]
GO
/****** Object:  ForeignKey [FK_MasterSJ_MasterSO]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_MasterSO] FOREIGN KEY([NoSO])
REFERENCES [dbo].[MasterSO] ([Kodenota])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_MasterSO]
GO
/****** Object:  ForeignKey [FK_MasterSJ_Sopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_Sopir] FOREIGN KEY([Sopir])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_Sopir]
GO
/****** Object:  ForeignKey [FK_MasterSJ_Sopir1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_Sopir1] FOREIGN KEY([Sopir2])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_Sopir1]
GO
/****** Object:  ForeignKey [FK_MasterSJ_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_User]
GO
/****** Object:  ForeignKey [FK_MasterSJ_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[MasterSJ]  WITH CHECK ADD  CONSTRAINT [FK_MasterSJ_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[MasterSJ] CHECK CONSTRAINT [FK_MasterSJ_User1]
GO
/****** Object:  ForeignKey [FK_DetailLPB_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailLPB]  WITH CHECK ADD  CONSTRAINT [FK_DetailLPB_Barang] FOREIGN KEY([Barang])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[DetailLPB] CHECK CONSTRAINT [FK_DetailLPB_Barang]
GO
/****** Object:  ForeignKey [FK_DetailLPB_LPB]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[DetailLPB]  WITH CHECK ADD  CONSTRAINT [FK_DetailLPB_LPB] FOREIGN KEY([KodeLPB])
REFERENCES [dbo].[LPB] ([Kode])
GO
ALTER TABLE [dbo].[DetailLPB] CHECK CONSTRAINT [FK_DetailLPB_LPB]
GO
/****** Object:  ForeignKey [FK_LPB_CashNCarry]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_CashNCarry] FOREIGN KEY([CashNCarry])
REFERENCES [dbo].[CashNCarry] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_CashNCarry]
GO
/****** Object:  ForeignKey [FK_LPB_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_Pegawai] FOREIGN KEY([Pemeriksa])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_Pegawai]
GO
/****** Object:  ForeignKey [FK_LPB_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_Pegawai1] FOREIGN KEY([Penerima])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_Pegawai1]
GO
/****** Object:  ForeignKey [FK_LPB_PO]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_PO] FOREIGN KEY([PO])
REFERENCES [dbo].[PO] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_PO]
GO
/****** Object:  ForeignKey [FK_LPB_Retur]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_Retur] FOREIGN KEY([Retur])
REFERENCES [dbo].[Retur] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_Retur]
GO
/****** Object:  ForeignKey [FK_LPB_Supplier]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_Supplier] FOREIGN KEY([Supplier])
REFERENCES [dbo].[Supplier] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_Supplier]
GO
/****** Object:  ForeignKey [FK_LPB_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_User]
GO
/****** Object:  ForeignKey [FK_LPB_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[LPB]  WITH CHECK ADD  CONSTRAINT [FK_LPB_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[LPB] CHECK CONSTRAINT [FK_LPB_User1]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_Armada] FOREIGN KEY([DariArmada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_Armada]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_Armada1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_Armada1] FOREIGN KEY([KeArmada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_Armada1]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_Barang]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_Barang] FOREIGN KEY([BarangDari])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_Barang]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_Barang1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_Barang1] FOREIGN KEY([BarangKe])
REFERENCES [dbo].[Barang] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_Barang1]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_LaporanPerbaikan] FOREIGN KEY([LaporanPerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_Pegawai] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_Pegawai]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_User]
GO
/****** Object:  ForeignKey [FK_TukarKomponen_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[TukarKomponen]  WITH CHECK ADD  CONSTRAINT [FK_TukarKomponen_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[TukarKomponen] CHECK CONSTRAINT [FK_TukarKomponen_User1]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_LaporanPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_LaporanPerawatan] FOREIGN KEY([LaporanPerawatan])
REFERENCES [dbo].[LaporanPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_LaporanPerawatan]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_LaporanPerbaikan] FOREIGN KEY([LaporanPerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_Pegawai] FOREIGN KEY([Mekanik])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_Pegawai]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_Rebuild]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_Rebuild] FOREIGN KEY([Rebuild])
REFERENCES [dbo].[Rebuild] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_Rebuild]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_User] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_User]
GO
/****** Object:  ForeignKey [FK_SuratPerintahKerja_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[SuratPerintahKerja]  WITH CHECK ADD  CONSTRAINT [FK_SuratPerintahKerja_User1] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[SuratPerintahKerja] CHECK CONSTRAINT [FK_SuratPerintahKerja_User1]
GO
/****** Object:  ForeignKey [FK_BonBarang_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_Armada]
GO
/****** Object:  ForeignKey [FK_BonBarang_LaporanPerawatan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_LaporanPerawatan] FOREIGN KEY([LaporanPerawatan])
REFERENCES [dbo].[LaporanPerawatan] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_LaporanPerawatan]
GO
/****** Object:  ForeignKey [FK_BonBarang_LaporanPerbaikan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_LaporanPerbaikan] FOREIGN KEY([LaporanPerbaikan])
REFERENCES [dbo].[LaporanPerbaikan] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_LaporanPerbaikan]
GO
/****** Object:  ForeignKey [FK_BonBarang_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_Pegawai] FOREIGN KEY([Peminta])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_Pegawai]
GO
/****** Object:  ForeignKey [FK_BonBarang_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_Pegawai1] FOREIGN KEY([Penerima])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_Pegawai1]
GO
/****** Object:  ForeignKey [FK_BonBarang_Pegawai2]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_Pegawai2] FOREIGN KEY([Penyetuju])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_Pegawai2]
GO
/****** Object:  ForeignKey [FK_BonBarang_Storing]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_Storing] FOREIGN KEY([Storing])
REFERENCES [dbo].[Storing] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_Storing]
GO
/****** Object:  ForeignKey [FK_BonBarang_SuratPerintahKerja]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_SuratPerintahKerja] FOREIGN KEY([SPK])
REFERENCES [dbo].[SuratPerintahKerja] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_SuratPerintahKerja]
GO
/****** Object:  ForeignKey [FK_BonBarang_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_User]
GO
/****** Object:  ForeignKey [FK_BonBarang_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonBarang]  WITH CHECK ADD  CONSTRAINT [FK_BonBarang_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonBarang] CHECK CONSTRAINT [FK_BonBarang_User1]
GO
/****** Object:  ForeignKey [FK_Storing_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_Armada]
GO
/****** Object:  ForeignKey [FK_Storing_MasterSJ]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_MasterSJ] FOREIGN KEY([SuratJalan])
REFERENCES [dbo].[MasterSJ] ([Kodenota])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_MasterSJ]
GO
/****** Object:  ForeignKey [FK_Storing_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_Pegawai] FOREIGN KEY([PICJemput])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_Pegawai]
GO
/****** Object:  ForeignKey [FK_Storing_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_Pegawai1] FOREIGN KEY([Pengemudi])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_Pegawai1]
GO
/****** Object:  ForeignKey [FK_Storing_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_User]
GO
/****** Object:  ForeignKey [FK_Storing_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[Storing]  WITH CHECK ADD  CONSTRAINT [FK_Storing_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[Storing] CHECK CONSTRAINT [FK_Storing_User1]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Armada]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Armada] FOREIGN KEY([Armada])
REFERENCES [dbo].[Armada] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Armada]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_MasterSJ]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_MasterSJ] FOREIGN KEY([SuratJalan])
REFERENCES [dbo].[MasterSJ] ([Kodenota])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_MasterSJ]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Pegawai]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Pegawai] FOREIGN KEY([PelakuPencegahan])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Pegawai]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Pegawai1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Pegawai1] FOREIGN KEY([PelakuPerbaikan])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Pegawai1]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Pegawai2]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Pegawai2] FOREIGN KEY([Verifikator])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Pegawai2]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Pelanggan]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Pelanggan] FOREIGN KEY([Pelanggan])
REFERENCES [dbo].[Pelanggan] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Pelanggan]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_Sopir]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_Sopir] FOREIGN KEY([Sopir])
REFERENCES [dbo].[Pegawai] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_Sopir]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_User]
GO
/****** Object:  ForeignKey [FK_KeluhanPelanggan_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[KeluhanPelanggan]  WITH CHECK ADD  CONSTRAINT [FK_KeluhanPelanggan_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[KeluhanPelanggan] CHECK CONSTRAINT [FK_KeluhanPelanggan_User1]
GO
/****** Object:  ForeignKey [FK_BonSopir_MasterSJ]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonSopir]  WITH CHECK ADD  CONSTRAINT [FK_BonSopir_MasterSJ] FOREIGN KEY([SuratJalan])
REFERENCES [dbo].[MasterSJ] ([Kodenota])
GO
ALTER TABLE [dbo].[BonSopir] CHECK CONSTRAINT [FK_BonSopir_MasterSJ]
GO
/****** Object:  ForeignKey [FK_BonSopir_User]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonSopir]  WITH CHECK ADD  CONSTRAINT [FK_BonSopir_User] FOREIGN KEY([CreateBy])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonSopir] CHECK CONSTRAINT [FK_BonSopir_User]
GO
/****** Object:  ForeignKey [FK_BonSopir_User1]    Script Date: 04/08/2013 18:28:17 ******/
ALTER TABLE [dbo].[BonSopir]  WITH CHECK ADD  CONSTRAINT [FK_BonSopir_User1] FOREIGN KEY([Operator])
REFERENCES [dbo].[User] ([Kode])
GO
ALTER TABLE [dbo].[BonSopir] CHECK CONSTRAINT [FK_BonSopir_User1]
GO
